<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<link rel="stylesheet"
   href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!--  header -->
<!-- Top header start -->
<header id="tg-header" class="tg-header tg-haslayout">
   <nav class="navbar navbar-default">
      <!-- BRAND -->
      <div class="navbar-header">
         <button type="button" class="navbar-toggle collapsed"
            data-toggle="collapse" data-target="#alignment-example"
            aria-expanded="false">
         <span class="sr-only">Toggle navigation</span> <span
            class="icon-bar"></span> <span class="icon-bar"></span> <span
            class="icon-bar"></span>
         </button>
         <div class="cart-logo">
            <a href="userHome">
            <img src="assets/userAssets/imges/bglogo.png">
            </a>
         </div>
      </div>
      <div class="">
         <ul class="nav navbar-nav">
            <li class="clogo-img"></li>
            <li>
               <button class="btn dropdown-toggle" type="button"
                  data-toggle="dropdown">
               <img
                  src="assets/userAssets/imges/cart-img-2.png"> &nbsp;
               COURSES <span class="caret"></span>
               </button>
               <ul class="dropdown-menu">
                  <c:choose>
                     <c:when test="${ not empty categoryList }">
                        <c:forEach items="${ categoryList }" var="category">
                           <li><a href="#" onclick="filterByCategoty(${ category.categoryId })">${ category.categoryName }</a></li>
                        </c:forEach>
                     </c:when>
                  </c:choose>
                  <!--  <li><a href="#">Yoga</a></li>
                     <li><a href="#">Nutrition</a></li>
                     <li><a href="#">Meditation</a></li> -->
                  <!-- <li class="dropdown dropdown-submenu"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown Link 4</a>
                     <ul class="dropdown-menu">
                                           <li class="kopie"><a href="#">Dropdown Link 4</a></li>
                     <li><a href="#">Dropdown Link 4</a></li>
                     <li><a href="#">Dropdown Link 4</a></li>
                     <li><a href="#">Dropdown Link 4</a></li>
                     <li><a href="#">Dropdown Link 4</a></li>
                                                                             
                     </ul>
                     </li> -->
                  <!-- <li><a href="categories2.html">Health and Fitness</a></li> -->
               </ul>
            </li>
            <!-- <li>
               <form class="inline-form" action="#" method="get" style="width: 100%;">
                  <div class="input-group search-box mobile-search">
                     <input type="text" name="query" class="form-control" placeholder="Search for courses">
                     <div class="input-group-append">
                        <button class="btn" type="submit"><i class="fa fa-search"></i></button>
                     </div>
                  </div>
               </form>
            </li> -->
         </ul>
      </div>
      <div class="collapse navbar-collapse" id="alignment-example">
         <ul class="navbar-form navbar-right" role="search">

            <!-- <li class=""><a href="#">
              <div class="form-group">
					 
					  <select class="form-control sel-lang">
					    <option>English</option>
					    <option>German</option>
					   
					  </select>
            </div>
</a>
</li> -->
            
          
            <c:choose>
               <c:when test="${empty userSession}">
               		<li class=""><a href="becomeTrainer">Become A Trainer</a></li>
               		  <li class="searchNavDvider"></li>
                  <li class="log-user"><a href="userLogin">LOG IN</a></li>
                  <li class="search-navigation"><a href="userRegister"><span
                     class=""> SIGN UP</span> </a></li>
               </c:when>
               <c:otherwise>
                  <!-- <li><a href="#"><span class=""> <img src="assets/userAssets/imges/cart.png"></span> </a></li> -->
                  <li>
                     <a class="btn dropdown-toggle" type="button" data-toggle="dropdown"> 
                     <img src="assets/userAssets/imges/cart.png">  <span class="badge watchfav">${ myCartListCount }</span></a>
                     <ul class="dropdown-menu menu_cart">
                        <li>
                           <ul class="drop_scroll">
                              <c:choose>
                                 <c:when test="${ not empty myCartCourseList }">
                                    <c:forEach items="${ myCartCourseList  }" var="myCartCourse">
                                       <li>
                                          <div class="drop_cart">
                                             <div class="row">
                                                <div class="col-md-4 col-sm-6 col-xs-4 ">
                                                   <c:choose>
                                                      <c:when test="${ not empty myCartCourse.trainerCourseInfo.trainerCourseInfo.courseCoverImageInfo.courseCoverImageViewPath }">
                                                         <img src="${ myCartCourse.trainerCourseInfo.trainerCourseInfo.courseCoverImageInfo.courseCoverImageViewPath }">
                                                      </c:when>
                                                      <c:otherwise>
                                                         <img src="assets/userAssets/imges/cart-img-2.png">
                                                      </c:otherwise>
                                                   </c:choose>
                                                </div>
                                                <div class="col-md-8 col-sm-6 col-xs-8 pad0">
                                                   <h5>${ myCartCourse.trainerCourseInfo.trainerCourseInfo.courseName } </h5>
                                                   <h6>${ myCartCourse.trainerCourseInfo.trainerInfo.trainerFName } ${ myCartCourse.trainerCourseInfo.trainerInfo.trainerLName }</h6>
                                                   <h4>&#36; ${ myCartCourse.trainerCourseInfo.trainerCourseInfo.coursePrice }</h4>
                                                </div>
                                             </div>
                                          </div>
                                       </li>
                                    </c:forEach>
                                 </c:when>
                                 <c:otherwise>
                                    <li>Your Cart is Empty</li>
                                 </c:otherwise>
                              </c:choose>
                              <!-- <li>
                                 <div class="drop_cart">
                                    <div class="row">
                                       <div class="col-md-4 col-sm-6 col-xs-4 ">
                                          <img src="imges/cart-img-2.png">
                                       </div>
                                       <div class="col-md-8 col-sm-6 col-xs-8 pad0">
                                          <h5>Adobe Photoshop CC Advanced Training Course </h5>
                                          <h6>Daniel Walter Scott</h6>
                                          <h4>420</h4>
                                       </div>
                                    </div>
                                 </div>
                                 </li>
                                 <li>
                                 <div class="drop_cart">
                                    <div class="row">
                                       <div class="col-md-4 col-sm-6 col-xs-4 ">
                                          <img src="imges/cart-img-2.png">
                                       </div>
                                       <div class="col-md-8 col-sm-6 col-xs-8 pad0">
                                          <h5>Adobe Photoshop CC  Advanced Training Course </h5>
                                          <h6>Daniel Walter Scott</h6>
                                          <h4>420</h4>
                                       </div>
                                    </div>
                                 </div>
                                 </li> -->
                           </ul>
                        </li>
                        <li class="tot_amt">
                           <div class="tot_dis">
                              <p> Total amount  : <strong>&#36; ${ myCartListTotalPrice }</strong> <span class="pull-right red_part "><a href="userShopingCartList"class=" text-danger">Go to cart </a></span> </p>
                           </div>
                        </li>
                     </ul>
                  </li>
                  <li>
                     <a href="userPurchasedCourseList">
                     My Courses
                     </a>
                  </li>
                  <li>
                     <a href="userProfile">
                        <fmt:message key="label.profile" />
                     </a>
                  </li>
                  <li>
                     <a href="userFavouriteList">
                        <fmt:message key="label.userFavorite" />
                        <span class="badge watchfav">${ myFavouriteListCount }</span>
                     </a>
                  </li>
                  <li>
                     <a href="userLogout">
                        <fmt:message key="label.logout" />
                     </a>
                  </li>
               </c:otherwise>
            </c:choose>
         </ul>
      </div>
   </nav>
</header>
<!-- Top header end -->