<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!-- Left Panel -->
<aside id="left-panel" class="left-panel">
   <nav class="navbar navbar-expand-sm navbar-default">
      <div class="navbar-header">
         <button class="navbar-toggler" type="button" data-toggle="collapse"
            data-target="#main-menu" aria-controls="main-menu"
            aria-expanded="false" aria-label="Toggle navigation">
         <i class="fa fa-bars"></i>
         </button>
         <div class="novologo">
            <a href="trainerDashboard"> <img
               src="assets/adminAssets/images/Nova-fitnesslogo.png"
               class="img-responsive"></a>
         </div>
      </div>
      <div id="main-menu" class="main-menu collapse navbar-collapse">
         <ul class="nav navbar-nav">
            <li class="active"><a href="trainerDashboard"> <i
               class="menu-icon fa fa-dashboard"></i>Dashboard
               </a>
            </li>
            <li class=""><a href="trainerCourseManagement"> <i
               class="fa fa-user-o menu-icon" aria-hidden="true"></i>Course
               Management
               </a>
            </li>
            <li><a href="trainerPurchasedCourseManagement"> <i
               class="fa fa-taxi menu-icon" aria-hidden="true"></i> My Courses
               </a>
            </li>
            <li><a href="trainerCourseReviewRatingManagement"> <i
               class="menu-icon fa fa-table"></i>Reviews and Ratings
               </a>
            </li>
            <li><a href="trainerCourseUserPaymentSettlementManagement"> <i
               class="fa fa-credit-card-alt menu-icon" aria-hidden="true"></i>Payment
               Settlements
               </a>
            </li>
         </ul>
      </div>
      <!-- /.navbar-collapse -->
   </nav>
</aside>
<!-- /#left-panel -->
<!-- Left Panel -->