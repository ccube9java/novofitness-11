<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="utf-8">
<!-- External CSS libraries -->
<link rel="stylesheet" type="text/css"
	href="assets/userAssets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="assets/userAssets/css/animate.min.css">
<link rel="stylesheet" type="text/css"
	href="assets/userAssets/css/bootstrap-submenu.css">
<link rel="stylesheet" type="text/css"
	href="assets/userAssets/css/bootstrap-select.min.css">
<link rel="stylesheet" type="text/css"
	href="assets/userAssets/fonts/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css"
	href="assets/userAssets/fonts/flaticon/font/flaticon.css">
<link rel="stylesheet" type="text/css"
	href="assets/userAssets/fonts/linearicons/style.css">
<link rel="stylesheet" type="text/css"
	href="assets/userAssets/css/jquery.mCustomScrollbar.css">
<link rel="stylesheet" type="text/css"
	href="assets/userAssets/css/bootstrap-datepicker.min.css">
<!-- Custom stylesheet -->
<link rel="stylesheet" type="text/css"
	href="assets/userAssets/css/style.css">
<link rel="stylesheet" type="text/css"
	href="assets/userAssets/css/style2.css">
<link rel="stylesheet" type="text/css" id="style_sheet"
	href="assets/userAssets/css/skins/blue-light-2.css">
<!-- Favicon icon -->
<link rel="shortcut icon" href="assets/favicon.ico" type="image/x-icon">
<!-- Google fonts -->
<link rel="stylesheet" type="text/css"
	href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800%7CPlayfair+Display:400,700%7CRoboto:100,300,400,400i,500,700">
<link rel="stylesheet" type="text/css"
	href="assets/userAssets/css/ie10-viewport-bug-workaround.css">
<script src="assets/userAssets/js/ie-emulation-modes-warning.js"></script>
<!-- Sweet Alerts -->
   <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" rel="stylesheet"/>

<!-- Owl Stylesheets -->
<link rel="stylesheet"
	href="assets/userAssets/owlcarousel/assets/owl.carousel.min.css">
<link rel="stylesheet"
	href="assets/userAssets/owlcarousel/assets/owl.theme.default.min.css">