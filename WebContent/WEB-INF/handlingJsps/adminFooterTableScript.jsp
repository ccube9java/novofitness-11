 <script type="text/javascript">
$(document).ready(function() {
             var table = $('#example').DataTable( {
                 lengthChange: true,
                 "oLanguage": {
                     "sLengthMenu": "<fmt:message key="label.show" /> _MENU_ <fmt:message key="label.entries" />",
                     "sSearch": "<fmt:message key="label.search" /> : ",
                     "oPaginate": {
                         "sNext":     "<fmt:message key="label.next" />",
                         "sPrevious": "<fmt:message key="label.previous" />"
                     },
                     "sEmptyTable":     "<fmt:message key="label.noDataAvailableInTable" />",
                     "sInfo":           "<fmt:message key="label.showing" /> _START_ <fmt:message key="label.userLocationTo" /> _END_ <fmt:message key="label.of" /> _TOTAL_ <fmt:message key="label.entries" />",
                     "sInfoEmpty":      "<fmt:message key="label.showing" /> 0 <fmt:message key="label.userLocationTo" /> 0 <fmt:message key="label.of" /> 0 <fmt:message key="label.entries" />",
                     "sInfoFiltered":   "(<fmt:message key="label.filtered" /> <fmt:message key="label.userLocationFrom" /> _MAX_ <fmt:message key="label.totalEntries" />)",
                     "sZeroRecords":    "No matching records found",
                   }
               /*   buttons: [ 'excel', 'pdf' ] */
             } );
          
            /*  table.buttons().container()
                 .appendTo( '#example_wrapper .col-md-6:eq(0)' ); */
         } );
 </script>