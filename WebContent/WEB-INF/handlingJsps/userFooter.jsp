<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!-- Footer start -->
<footer class="main-footer clearfix">
   <div class="container">
      <!-- Footer info-->
      <div class="footer-info">
         <div class="row">
            <!-- About us -->
            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
               <div class="footer-item">
                  <ul class="pages-txt">
                     <li><a> Home </a></li>
                     <li><a> About Us </a></li>
                     <li><a> Contact Us </a></li>
                      <li><a href="#">English</a></li>
                       <li><a href="#">German</a></li>
                  </ul>
               </div>
            </div>
            <div class="col-md-1 col-sm-6 col-xs-6">
               <ul class="pages-txt">
                  <li><a>All</a></li>
                  <li><a>FITNESS</a></li>
                  <li><a>HEALTH</a></li>
               </ul>
            </div>
            <div class="col-md-2 col-sm-6 col-xs-6">
               <ul class="pages-txt text-center">
                  <li><a>ATHLETIC</a></li>
                  <li><a>DANCE</a></li>
                  <li><a>COACHING</a></li>
               </ul>
            </div>
            <div class="col-md-2 col-sm-6 col-xs-6">
               <ul class="pages-txt">
                  <li><a>Self Defense</a></li>
                  <li><a>TENNIS</a></li>
                  <li><a>FOOTBALL</a></li>
               </ul>
            </div>
            <div class="col-md-2 col-sm-6 col-xs-6">
               <ul class="pages-txt">
                  <li><a>Physio</a></li>
                  <li><a>hypnosis</a></li>
                  <li><a>Qi gong</a></li>
               </ul>
            </div>
            <div class="col-md-2 col-sm-6 col-xs-6">
               <ul class="pages-txt">
                  <li><a>Terms and conditions</a></li>
                  <li><a>Privacy policy</a></li>
                  <li><a>cookies</a></li>
               </ul>
            </div>
            <div class="col-md-1 col-sm-6 col-xs-6">
               <ul class="footer-icon">
                  <li><a href="#" class="fa fa-facebook footer-facebook-icons"></a></li>
                  <li><a href="#" class="fa fa-youtube footer-youtube-icons"></a></li>
                  <li><a href="#" class="fa fa-instagram footer-instagram-icons"></a></li>
                  <li><a href="#" class="fa fa-pinterest footer-pinterest-icons"></a></li>
               </ul>
            </div>
         </div>
         <!-- Gallery -->
         <!-- Newsletter -->
      </div>
      <hr>
      <div class="clearfix"></div>
      <div class="row">
         <div class="quick_newsletter">
            <div class="newsletter-info col-md-4 col-sm-4">
               <h3>
                  <!-- Novofitness E-Learning - purpose is to transform access to
                     sports education. -->Subscribe NewsLetter
               </h3>
               <p>Be the first to hear about our special offers and news</p>
            </div>
            <div class="newsletter-element">
               <form:form action="subscribeNewsLetter" method="post"
                  modelAttribute="newsLetterWrapper">
                  <p class="col-md-3 col-sm-3">
                     <form:input path="userName" name="name" placeholder="Your name" class="newsletter-firstname input-text"/>
                     <form:errors path="userName" class="error" />
                     <!-- <input class="newsletter-firstname input-text" type="text"
                        placeholder="Your Name"> -->
                  </p>
                  <p class="col-md-3 col-sm-3">
                     <form:input path="userEmail" name="email"
                        placeholder="Your email addresse" class="newsletter-email input-text" />
                     <form:errors path="userEmail" class="error" />
                     <!-- <input class="newsletter-email input-text" type="email"
                        placeholder="Enter email"> -->
                  </p>
                  <p class="col-md-2 col-sm-2">
                     <button type="submit" class="newsletter-submit btn" name="submit">
                        <i class="fa fa-paper-plane"></i>
                        <fmt:message key="label.subscribe" />
                     </button>
                     <!-- <button class="newsletter-submit btn" type="submit">
                        <i class="fa fa-paper-plane"></i> Subscribe
                        </button> -->
                  </p>
               </form:form>
            </div>
            <div class="clearfix"></div>
            
           <!--   <div class="row">
               <div class="col-md-4">
               </div>
               <div class="col-md-8">
                 <div class="languages">
                    <div class="form-group">
					 
					  <select class="form-control sel-lang">
					    <option>English</option>
					    <option>German</option>
					   
					  </select>
                      </div>
                 </div> languages
               </div> md8
               
             </div> row -->
             
             
               <div class="clearfix"></div>
             
            <div class="row">
               <div class="col-md-4">
               </div>
               <div class="col-md-8">
                 <%--  <c:if test="${ not empty alertSuccessMessage }">
                     <div class="alert alert-success alert-customizes">${ alertSuccessMessage }</div>
                  </c:if> --%>
                  <c:if
                     test="${ not empty alertSuccessMessagenewsletterAlreadySubscribed }">
                     <div class="alert alert-success alert-customizes">
                        <fmt:message key="label.newsletterAlreadySubscribed" />
                     </div>
                  </c:if>
                  <c:if
                     test="${ not empty alertSuccessMessagenewsletterSubscriptionSuccess }">
                     <div class="alert alert-success alert-customizes">
                        <fmt:message key="label.newsletterSubscriptionSuccess" />
                     </div>
                  </c:if>
                 <%--  <c:if test="${ not empty alertFailMessage }">
                     <div class="alert alert-danger alert-customizes">${ alertFailMessage }</div>
                  </c:if> --%>
                  <c:if
                     test="${ not empty alertFailMessagenewsletterSubscriptionFail }">
                     <div class="alert alert-danger alert-customizes">
                        <fmt:message key="label.newsletterSubscriptionFail" />
                     </div>
                  </c:if>
               </div>
            </div>
         </div>
      </div>
      <!-- rows -->
   </div>
</footer>
<!-- Footer end -->
<!-- Copy right start -->
<div class="copy-right">
   <div class="container">Copyright &copy; 2019 NovoFitness. All
      Rights Reserved.
   </div>
</div>
<!-- Copy end right-->