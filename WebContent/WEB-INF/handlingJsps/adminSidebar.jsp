<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!-- Left Panel -->
<aside id="left-panel" class="left-panel">
   <nav class="navbar navbar-expand-sm navbar-default">
      <div class="navbar-header">
         <button class="navbar-toggler" type="button" data-toggle="collapse"
            data-target="#main-menu" aria-controls="main-menu"
            aria-expanded="false" aria-label="Toggle navigation">
         <i class="fa fa-bars"></i>
         </button>
         <div class="novologo">
         	<a href="adminDashboard">
            <img src="assets/adminAssets/images/Nova-fitnesslogo.png"
               class="img-responsive"></a>
         </div>
      </div>
      <div id="main-menu" class="main-menu collapse navbar-collapse">
         <ul class="nav navbar-nav">
            <li class="active">
            	<a href="adminDashboard"> 
            		<i class="menu-icon fa fa-dashboard"></i>Dashboard
               </a>
            </li>
            
            <li class=""><a href="adminSubAdminManagement"> <i
               class="fa fa-user-o menu-icon" aria-hidden="true"></i>Sub-admin
               Management
               </a>
            </li>
           
            <li><a href="adminUserManagement"> <i
               class="fa fa fa-user menu-icon" aria-hidden="true"></i>User/Learner
               Management
               </a>
            </li>
            <li><a href="adminTrainerManagement"> <i
               class="menu-icon fa fa-table"></i>Trainer Management
               </a>
            </li>
            <li><a href="adminCourseManagement"> <i
               class="menu-icon fa fa-table"></i>Course Management
               </a>
            </li>
      <li><a href="adminTrainerCourseUserPaymentManagement"> <i
               class="fa fa-credit-card-alt menu-icon" aria-hidden="true"></i>Payment
               Management
               </a>
            </li>
            <li><a href="adminPaymentSettlementManagement"> <i
               class="fa fa-credit-card-alt menu-icon" aria-hidden="true"></i>Payment
               Settlement
               </a>
            </li>
            <li class="menu-item-has-children dropdown">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               	<i class="fa fa-pencil-square-o  menu-icon " aria-hidden="true"></i>
               	Content Management</a>
               <ul class="sub-menu children dropdown-menu">
                 <!--  <li><i class="fa fa-id-badge"></i><a href="slidermanagement.html">Slider Management</a></li> -->
                  <li><i class="fa fa-id-badge"></i><a href="adminFAQManagement">FAQ Management</a></li>
                  <!-- <li><i class="fa fa-id-badge"></i><a href="adminPromotionalOffersManagement">Promotional Management</a></li> -->
               </ul>
            </li>
            
            <li class="menu-item-has-children dropdown">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               	<i class="fa fa-pencil-square-o  menu-icon " aria-hidden="true"></i>
               	Category Management</a>
               <ul class="sub-menu children dropdown-menu">
                  <li><i class="fa fa-id-badge"></i><a href="adminMainCategoryManagement">Main Category</a></li>
                  <li><i class="fa fa-id-badge"></i><a href="adminSubCategoryManagement">Sub Category</a></li>
               </ul>
            </li>
            
             <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-pencil-square-o  menu-icon " aria-hidden="true"></i>General Management</a>
                        <ul class="sub-menu children dropdown-menu">
                              <li><i class="fa fa-id-badge"></i><a href="adminReviewManagement">Manage Reviews</a></li>
                               <li><i class="fa fa-id-badge"></i><a href="adminNewsLetterManagement">Manage Newsletter</a></li>
                           
                          </ul>
                        </li>
                        
                        
                   <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-pencil-square-o  menu-icon " aria-hidden="true"></i>Reports Management</a>
                        <ul class="sub-menu children dropdown-menu">
                              <li><i class="fa fa-id-badge"></i><a href="adminUserListManagement">List of Learners</a></li>
                               <li><i class="fa fa-id-badge"></i><a href="adminTrainerListManagent">List of Trainer</a></li>
                           
                          </ul>
                        </li>
                              
                        
                        
                        
           <!-- <li class="menu-item-has-children dropdown">
               <a href="#"
                  class="dropdown-toggle" data-toggle="dropdown"
                  aria-haspopup="true" aria-expanded="false"><i
                  class="fa fa-pencil-square-o  menu-icon " aria-hidden="true"></i>General
               Management</a>
               <ul class="sub-menu children dropdown-menu">
                  <li><i class="fa fa-id-badge"></i><a
                     href="Managereview.html">Manage Reviews</a></li>
                  <li><i class="fa fa-id-badge"></i><a
                     href="managenewsletter.html">Manage Newsletter</a></li>
               </ul>
            </li>
            <li><a href="paymentmanagement.html"> <i
               class="fa fa-credit-card-alt menu-icon" aria-hidden="true"></i>Payment
               Management
               </a>
            </li>
            <li class="menu-item-has-children dropdown">
               <a href="#"
                  class="dropdown-toggle" data-toggle="dropdown"
                  aria-haspopup="true" aria-expanded="false"><i
                  class="fa fa-pencil-square-o  menu-icon " aria-hidden="true"></i>Reports
               Management</a>
               <ul class="sub-menu children dropdown-menu">
                  <li><i class="fa fa-id-badge"></i><a href="listofuser.html">List
                     of Learners </a>
                  </li>
                  <li><i class="fa fa-id-badge"></i><a href="listoftrainer">List
                     of Trainer</a>
                  </li>
                  <li><i class="fa fa-id-badge"></i><a href="">Payment
                     Reports</a>
                  </li>
                  <li><i class="fa fa-id-badge"></i><a href=""> Course
                     Purchased</a>
                  </li>
               </ul>
            </li> -->
         </ul>
      </div>
      <!-- /.navbar-collapse -->
   </nav>
</aside>
<!-- /#left-panel -->
<!-- Left Panel -->