<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<c:choose>
	<c:when test="${ not empty adminSession}">
		<c:redirect url="adminDashboard" />
	</c:when>
</c:choose>