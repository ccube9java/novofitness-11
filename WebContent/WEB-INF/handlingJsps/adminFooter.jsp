<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>

<footer class="sticky-footer">
	<div class="container my-auto">
		<div class="copyright text-center my-auto">
			<span> <fmt:message key="label.copyright" /></span>
		</div>
	</div>
</footer>