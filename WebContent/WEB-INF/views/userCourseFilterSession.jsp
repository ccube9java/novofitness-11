<!DOCTYPE html>
<html lang="zxx">
<head>
<!-- top header -->
<title>NovoFitness</title>
<%@ include file="/WEB-INF/handlingJsps/userTopHeader.jsp"%>
 <%@ include file="/WEB-INF/handlingJsps/userSession.jsp"%>
<!-- close top header -->
<style>
.panel-group .panel {
	border: none;
	border-color: #EEEEEE;
}

.panel-title {
	font-size: 14px;
}

.panel-heading {
	padding: 0 !important;
}

.panel-heading:hover {
	color: #000000;
}

.panel-title>a {
	display: block;
	padding: 15px;
	text-decoration: none;
}

.panel-title>a:hover {
	color: #000000 !important;
}

.panel-body {
	padding: 10px 3px;
}

a, a:hover, a.active, a:active, a:visited, a:focus {
	color: #000000 !important;
}

.more-less {
	float: right;
	color: #212121;
}

.panel-default>.panel-heading+.panel-collapse>.panel-body {
	border-top-color: #EEEEEE;
}

#ex1Slider .slider-selection {
	background: #BABABA;
}

.category-items {
	margin: 10px 0px;
}

.panel-group {
	margin: 10px 0px;
}

footer li  a {
	color: #ffffff !important;
}

footer li .fa {
	color: #ffffff !important;
}
</style>
</head>
<body>
	<div class="page_loader"></div>
	<!-- Top header start -->
	<%@ include file="/WEB-INF/handlingJsps/userHeader.jsp"%>
	<!-- Top header end -->
	<!-- Sub banner start -->
	<div class="category-heading">
		<div class="container-fluid">
			<div class="categort-title">
				<h1>${ categoryName }</h1>
			</div>
		</div>
	</div>
	<div class="cart-details">
		<div class="container">
			<div class="row">
				<h2>Courses to get you started</h2>
			</div>
		</div>
	</div>
	<div class="category-elements">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
					<div class="demo">
						<div class="panel-group" id="accordion" role="tablist"
							aria-multiselectable="true">
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingTwo">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse"
											data-parent="#accordion" href="#collapseTwo"
											aria-expanded="false" aria-controls="collapseTwo"> <!--  <i class="more-less glyphicon glyphicon-plus"></i> -->
											Category
										</a>
									</h4>
								</div>
								<div id="collapseTwo" class="panel-collapse collapse"
									role="tabpanel" aria-labelledby="headingTwo">
									<div class="panel-body">
										<div class="form-group">
											<select onchange="selectedCourseCategory(this)"
												class="form-control">
												<option value="0">Select Course Category</option>
												<c:choose>
													<c:when test="${not empty categoryList}">
														<c:forEach items="${ categoryList }" var="category">
															<option value="${ category.categoryId }"
																${ category.categoryId == courseCategotyId ? 'selected="selected"' : ''}>${ category.categoryName }</option>
														</c:forEach>
													</c:when>
												</c:choose>
											</select>
										</div>
										<%-- <ul class="list-items">
                                    <c:forEach items="${ categoryList }" var="category">
                                    	<li><a href="#">
                                         <label class = "checkbox-inline">
                                         <input type = "checkbox" id = "inlineCheckbox1" value = "${ category.categoryId }">${ category.categoryName }
                                         </label>
                                         </a>
                                      </li>
                                    </c:forEach>
                                      
                                     <!--  <li>
                                         <label class = "checkbox-inline">
                                         <input type = "checkbox" id = "inlineCheckbox1" value = "option1">Nutrition
                                         </label>
                                      </li>
                                      <li>
                                         <label class = "checkbox-inline">
                                         <input type = "checkbox" id = "inlineCheckbox1" value = "option1">Meditation
                                         </label> 
                                      </li>
                                      <li>
                                         <label class = "checkbox-inline">
                                         <input type = "checkbox" id = "inlineCheckbox1" value = "option1">Health and Fitness
                                         </label> 
                                      </li> -->
                                    </ul> --%>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingThree">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse"
											data-parent="#accordion" href="#collapseThree"
											aria-expanded="false" aria-controls="collapseThree"> <!--  <i class="more-less glyphicon glyphicon-plus"></i> -->
											Price
										</a>
									</h4>
								</div>
								<div id="collapseThree" class="panel-collapse collapse"
									role="tabpanel" aria-labelledby="headingThree">
									<div class="panel-body">
										<div class="range-slider">
											<input class="range-slider__range" type="range" value="100"
												min="0" max="5000"> <span
												class="range-slider__value">0</span>
										</div>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingThree">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse"
											data-parent="#accordion" href="#collapseFour"
											aria-expanded="false" aria-controls="collapseFour"> <!--  <i class="more-less glyphicon glyphicon-plus"></i> -->
											Rating
										</a>
									</h4>
								</div>
								<div id="collapseFour" class="panel-collapse collapse"
									role="tabpanel" aria-labelledby="headingThree">
									<div class="panel-body">
										<div class="panels">
											<div class="form-group" id="rating-ability-wrapper">
												<label class="control-label" for="rating"> <span
													class="field-label-info"></span> <input type="hidden"
													id="selected_rating" name="selected_rating"
													value="${ not empty userLocationStarRatings?userLocationStarRatings:0 }"
													required="required">
												</label>
												<h2 class="bold rating-header" style="">
													<span class="selected-rating">0</span><small> / 5</small>
												</h2>
												<button type="button"
													class="btnrating btn btn-default btn-lg" data-attr="1"
													id="rating-star-1">
													<i class="fa fa-star" aria-hidden="true"></i>
												</button>
												<button type="button"
													class="btnrating btn btn-default btn-lg" data-attr="2"
													id="rating-star-2">
													<i class="fa fa-star" aria-hidden="true"></i>
												</button>
												<button type="button"
													class="btnrating btn btn-default btn-lg" data-attr="3"
													id="rating-star-3">
													<i class="fa fa-star" aria-hidden="true"></i>
												</button>
												<button type="button"
													class="btnrating btn btn-default btn-lg" data-attr="4"
													id="rating-star-4">
													<i class="fa fa-star" aria-hidden="true"></i>
												</button>
												<button type="button"
													class="btnrating btn btn-default btn-lg" data-attr="5"
													id="rating-star-5">
													<i class="fa fa-star" aria-hidden="true"></i>
												</button>
												<button type="button"
													class="btnrating btn btn-default btn-lg pull-right"
													data-attr="0" id="rating-star-0">
													<fmt:message key="label.clear" />
												</button>
											</div>
										</div>
										<!--  <ul class="list-items">
                                    <div class="form-group" id="rating-ability-wrapper">
                                       <label class="control-label" for="rating"> <span class="field-label-info"></span> 
                                       <input type="hidden" id="selected_rating" name="selected_rating" value="5" required="required">
                                       </label>
                                       <button type="button" class="btnrating btn btn-lg btn-warning" data-attr="1" id="rating-star-1">
                                       <i class="fa fa-star" aria-hidden="true"></i>
                                       </button>
                                       <button type="button" class="btnrating btn btn-lg btn-warning" data-attr="2" id="rating-star-2">
                                       <i class="fa fa-star" aria-hidden="true"></i>
                                       </button>
                                       <button type="button" class="btnrating btn btn-lg btn-warning" data-attr="3" id="rating-star-3">
                                       <i class="fa fa-star" aria-hidden="true"></i>
                                       </button>
                                       <button type="button" class="btnrating btn btn-lg btn-warning" data-attr="4" id="rating-star-4">
                                       <i class="fa fa-star" aria-hidden="true"></i>
                                       </button>
                                       <button type="button" class="btnrating btn btn-lg btn-warning" data-attr="5" id="rating-star-5">
                                       <i class="fa fa-star" aria-hidden="true"></i>
                                       </button>
                                       <button type="button" class="btnrating btn btn-default btn-lg pull-right" data-attr="0" id="rating-star-0"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                                       clear
                                       </font></font></button>
                                    </div>
                                    </ul> -->
									</div>
								</div>
							</div>
						</div>
						<!-- panel-group -->
					</div>
				</div>
				<!--end of the col-md-3 here--->
				<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
					<div class="row">
						<c:choose>
							<c:when test="${ not empty trainerCourseFilteredList }">
								<c:forEach items="${ trainerCourseFilteredList }"
									var="trainerCourseResponse">
									<a class="" href="dXNlckNvdXJzZURlcnRhaWxz?dHJhaW5lckNvdXJzZUlk=${ trainerCourseResponse.trainerCourseInfo.trainerCourseId }&dHJhaW5lcklk=${ trainerCourseResponse.trainerCourseInfo.trainerInfo.trainerId }">
									<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
										<div class="card-category category-items">
											<c:choose>
												<c:when
													test="${ not empty trainerCourseResponse.trainerCourseInfo.trainerCourseInfo.courseCoverImageInfo }">
													<img
														src="${ trainerCourseResponse.trainerCourseInfo.trainerCourseInfo.courseCoverImageInfo.courseCoverImageViewPath }"
														class="img-responsive">
												</c:when>
												<c:otherwise>
													<img src="assets/userAssets/imges/course11.jpg"
														class="img-responsive">
												</c:otherwise>
											</c:choose>
											<!-- <img src="imges/courses11.jpg" class="img-responsive"> -->
											<!-- <div class="special-info-details">
                                       <p>bestseller</p>
                                       </div> -->
											<div class="textinfo-details ">
												<h4>
													<b>${ trainerCourseResponse.trainerCourseInfo.trainerCourseInfo.courseName }</b>
												</h4>
												<div class="textinfo ">
													<h5>${ trainerCourseResponse.trainerCourseInfo.trainerCourseInfo.courseDescription }</h5>
												</div>
											</div>
											<div class="feedback-info-category ">
												<!-- <span class="fa fa-star checked"></span> <span
													class="fa fa-star checked"></span> <span
													class="fa fa-star checked"></span> <span class="fa fa-star"></span>
												<span class="fa fa-star"></span> -->
												<span class="stars" data-rating="${ trainerCourseResponse.averageRating }" data-num-stars="5" ></span>
												<%-- <div class="row">
                                                <div class="col-md-12">
                                                   <div class="star-divs rating">
                                                      <span class="stars" data-rating="${ trainerCourseResponse.averageRating }" data-num-stars="5" ></span>
                                                   </div>
                                                </div>
                                             </div> --%>
                                             <!--  rows -->
												<p style="float: right">
													<c:choose>
														<c:when test="${ empty userSession }">
															<%-- <fmt:message key="label.addToFav" var="addToFav"/> --%>
															<%-- <i class="fa fa-heart watch-list open-watchlistLogin"
                                                   aria-hidden="true"
                                                   data-id="${ trainerCourseResponse.trainerCourseInfo.trainerCourseId }"
                                                   title="<fmt:message key="label.addToFav"/>"
                                                   ></i> --%>
															<i class="fa fa-heart watch-list open-watchlistLogin"
																aria-hidden="true"
																data-id="${ trainerCourseResponse.trainerCourseInfo.trainerCourseId }"
																title="<fmt:message key="label.addToFav"/>"
																data-toggle="modal" data-target="#addFavLoginModal"></i>
														</c:when>
														<c:otherwise>
															<c:choose>
																<c:when
																	test="${ trainerCourseResponse.isWatchListed() }">
																	<c:set var="watchListClass" value="watch-list-in"></c:set>
																	<fmt:message key="label.removeFromFav"
																		var="removeFromFav" />
																	<c:set var="watchListTitle" value="${ removeFromFav }"></c:set>
																</c:when>
																<c:otherwise>
																	<c:set var="watchListClass" value="watch-list"></c:set>
																	<fmt:message key="label.addToFav" var="addToFav" />
																	<c:set var="watchListTitle" value="${ addToFav }"></c:set>
																</c:otherwise>
															</c:choose>
															<a
																onclick="userWatchListAddRemove(${ trainerCourseResponse.trainerCourseInfo.trainerCourseId }, ${ userSession.userId }, ${ trainerCourseResponse.isWatchListed() })">
																<i class="fa fa-heart ${ watchListClass }"
																aria-hidden="true" title="${ watchListTitle }"
																onclick=""></i>
															</a>
														</c:otherwise>
													</c:choose>
												</p>
											</div>
										</div>
									</div>
									 </a>
									<!---end of the col-md-3 here--->
								</c:forEach>
							</c:when>
							<c:otherwise>
								<div class="courseslogin">
									<div class="courseslogins">
									No Course available for this Category. 
										<a href="userHome"><button type="button"
												class="btn btn-default btnaddedcourses">find more</button></a>
									</div>

								</div>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				<!-- <a href="categories2.html">
                  <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                     <div class="card-category">
                        <img src="imges/courses11.jpg" class="img-responsive">
                        <div class="special-info-details">
                           <p>bestseller</p>
                        </div>
                        <div class="textinfo-details ">
                           <h4><b>Cognitive Behavioural Therapy (CBT) Practitioner Certificate</b></h4>
                           <p>Make your Concept Right and Beautiful</p>
                        </div>
                        <div class="feedback-info-category ">
                           <span class="fa fa-star checked"></span>
                           <span class="fa fa-star checked"></span>
                           <span class="fa fa-star checked"></span>
                           <span class="fa fa-star"></span>
                           <span class="fa fa-star"></span> 
                           <p style="float:right">4.6(7)</p>
                        </div>
                     </div>
                  </div>
                  </a>
                  end of the col-md-3 here-
                  <a href="categories2.html">
                  <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                     <div class="card-category">
                        <img src="imges/courses11.jpg" class="img-responsive">
                        <div class="special-info-details">
                           <p>bestseller</p>
                        </div>
                        <div class="textinfo-details ">
                           <h4><b>Cognitive Behavioural Therapy (CBT) Practitioner Certificate</b></h4>
                           <p>Make your Concept Right and Beautiful</p>
                        </div>
                        <div class="feedback-info-category ">
                           <span class="fa fa-star checked"></span>
                           <span class="fa fa-star checked"></span>
                           <span class="fa fa-star checked"></span>
                           <span class="fa fa-star"></span>
                           <span class="fa fa-star"></span> 
                           <p style="float:right">4.6(7)</p>
                        </div>
                     </div>
                  </div>
                  </a>
                  end of the col-md-3 here-
                  <a href="categories2.html">
                  <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                     <div class="card-category">
                        <img src="imges/courses11.jpg" class="img-responsive">
                        <div class="special-info-details">
                           <p>bestseller</p>
                        </div>
                        <div class="textinfo-details ">
                           <h4><b>Cognitive Behavioural Therapy (CBT) Practitioner Certificate</b></h4>
                           <p>Make your Concept Right and Beautiful</p>
                        </div>
                        <div class="feedback-info-category ">
                           <span class="fa fa-star checked"></span>
                           <span class="fa fa-star checked"></span>
                           <span class="fa fa-star checked"></span>
                           <span class="fa fa-star"></span>
                           <span class="fa fa-star"></span> 
                           <p style="float:right">4.6(7)</p>
                        </div>
                     </div>
                  </div>
                  </a> -->
				<!--end of the col-md-3 here--->
			</div>
		</div>
	</div>
	<!-- Modal for Login -->
	<div class="modal fade" id="addFavLoginModal" role="dialog">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">
						<%-- <fmt:message key="label.login" /> --%>
						Sign In to your account
					</h4>
				</div>
				<div class="modal-body">
					<div class="" id="showlog">
						<div class="details">
							<!-- Form start -->
							<div class="form-content-box ">
								<form:form action="" method="post"
									modelAttribute="userLoginWrapper">
									<div class="form-group">
										<form:hidden path="trainerCourseId" value="" />
										<fmt:message key="label.enterEmail" var="enterEmail" />
										<form:input path="userEmail" class="input-text"
											placeholder="${ enterEmail }" />
										<form:errors path="userEmail" class="error" />
									</div>
									<div class="form-group">
										<fmt:message key="label.enterPassword" var="enterPassword" />
										<form:password path="userPassword" class="input-text"
											placeholder="${ enterPassword }" />
										<form:errors path="userPassword" class="error" />
									</div>
									<div class="checkbox">
										<div class="ez-checkbox pull-left">
											<a href="userRegister"> <fmt:message key="label.register" />
											</a>
										</div>
										<a href="userForgetPassword"
											class="link-not-important pull-right"> Forgot password </a>
										<div class="clearfix"></div>
									</div>
									<div class="mb-0">
										<button type="submit" class="btn-md btn-theme btn-block">
											Login</button>
									</div>
								</form:form>
							</div>
							<!-- Form end -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Footer start -->
	<%@ include file="/WEB-INF/handlingJsps/userFooter.jsp"%>
	<!-- Footer end -->
	<%@ include file="/WEB-INF/handlingJsps/userLowerFooter.jsp"%>
	<script type="text/javascript">
         var courseSubCategories=[];
         var courseCategoryId = getQueryVariable("Y291cnNlTWFpbkNhdGVnb3J5SWQ");
         var courseStarRating = getQueryVariable("Y291cnNlU3RhclJhdGluZ3M");
         var courseMinPrice = getQueryVariable("Y291cnNlTWluUHJpY2U");
         var courseMaxPrice = getQueryVariable("Y291cnNlTWF4UHJpY2U");
         
         courseSubCategories = getQueryVariable("Y291cnNlU3ViQ2F0ZWdvcmllcw");
         
         
         //console.log("courseCategoryId::"+courseCategoryId);
         //console.log("courseStarRating::"+courseStarRating);
         //console.log("courseMinPrice::"+courseMinPrice);
         //console.log("courseMaxPrice::"+courseMaxPrice);
         //console.log("courseSubCategories::"+courseSubCategories);
         
         function getQueryVariable(variable) {
          
         	  var query = window.location.search.substring(1);
         	  var vars = query.split("&");
         	  for (var i=0;i<vars.length;i++) {
         	    var pair = vars[i].split("=");
         	    if (pair[0] == variable) {
         	      return pair[1];
         	    }
         	  } 
         	  //alert('Query Variable ' + variable + ' not found');
         	}
         	
         function selectedLocationCity(name){
          userLocationCity = name.value
         	filterSearch();
         }
         
         function selectedCourseCategory(id){
          courseCategoryId = id.value;
         	filterSearch();
         }
         
         function selectedLocationSurrounding(id){
         	userLocationSurroundingId = id.value;
         	filterSearch();
         }
         
         function selectedAllowedPeople(id) {
         	userLocationAllowedPeople = id.value;
         	filterSearch();
         }
         
         $("input[name='userSubCategories[]']").change(function () {
         	//alert("hui..");
         	userLocationSubCategoryList = $("input[name='userSubCategories[]']:checked").map(function (i) {
                 return $(this).val();
             }).get();
             //console.log("userLocationequipments::"+userLocationequipmentList); 
             filterSearch();
         });
         
         $("input[name='userEquipments[]']").change(function () {
         	//alert("hui..");
         	userLocationequipmentList = $("input[name='userEquipments[]']:checked").map(function (i) {
                 return $(this).val();
             }).get();
             //console.log("userLocationequipments::"+userLocationequipmentList); 
             filterSearch();
         });
         
         	function filterSearch() {
         	
         		if (courseCategoryId == undefined || courseCategoryId == 0) {
         			courseCategoryId='';
         		}
         		
          	if (courseStarRating == undefined || courseStarRating == 0) {
          		courseStarRating='';	
          	}
          	
          	if (courseMinPrice == undefined ) {
          		courseMinPrice=0;
          	}
          	
          	if (courseMaxPrice == undefined ) {
          		courseMaxPrice=''
          	}
         
          	window.location = "dHJhaW5lckNvdXJzZUJ5RmlsdGVyU2VhcmNo?Y291cnNlTWFpbkNhdGVnb3J5SWQ="
              		+ courseCategoryId
             		+ "&Y291cnNlU3RhclJhdGluZ3M="
             		+ courseStarRating
             		+ "&Y291cnNlTWluUHJpY2U="
             		+ courseMinPrice
             		+ "&Y291cnNlTWF4UHJpY2U="
             		+ courseMaxPrice
             		+ "&Y291cnNlU3ViQ2F0ZWdvcmllcw="
             		+ courseSubCategories;
         	}
      </script>
	<script>
         var rangeSlider = function() {
         var slider = $('.range-slider'),
            range = $('.range-slider__range'),
            value = $('.range-slider__value');
          	  slider.each(function() {
          
           value.each(function(){
             var value = $(this).prev().attr('value');
             $(this).html(value);
           });
          
           range.on('input', function() {
             $(this).next(value).html(this.value);
             console.log("range::",range.val());
             courseMaxPrice = range.val()
             filterSearch();
             
           });
          });
         };
         
         rangeSlider();
          
      </script>
	<!--  <script>
         $( function() {
           $( "#slider-range" ).slider({
             range: true,
             min: 0,
             max: 5000,
             values: [ courseMinPrice!=undefined?courseMinPrice:0, (courseMaxPrice==undefined||courseMaxPrice=='')?5000:courseMaxPrice ],
             slide: function( event, ui ) {
               $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
               courseMinPrice = ui.values[ 0 ];
               courseMaxPrice = ui.values[ 1 ];
               getvalues();
             }
           });
           $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
             " - $" + $( "#slider-range" ).slider( "values", 1 ) );
         });
          function getvalues(){
          console.log("minprice::"+courseMinPrice);
          console.log("maxprice::"+courseMaxPrice);
          filterSearch();
         } 
         </script> -->
	<script>
         jQuery(document).ready(function($) {
         	var previous_value1 = $("#selected_rating").val();
         	for (ix = 1; ix <= previous_value1; ++ix) {
         		$("#rating-star-" + ix).toggleClass('btn-warning');
         		$("#rating-star-" + ix).toggleClass('btn-default');
         	}
         	$(".selected-rating").html(previous_value1);
         
         	$(".btnrating").on('click', (function(e) {
         
         		var previous_value = $("#selected_rating").val();
         
         		var selected_value = $(this).attr("data-attr");
         		$("#selected_rating").val(selected_value);
         
         		$(".selected-rating").empty();
         		$(".selected-rating").html(selected_value);
         
         		for (i = 1; i <= selected_value; ++i) {
         			$("#rating-star-" + i).toggleClass('btn-warning');
         			$("#rating-star-" + i).toggleClass('btn-default');
         		}
         
         		for (ix = 1; ix <= previous_value; ++ix) {
         			$("#rating-star-" + ix).toggleClass('btn-warning');
         			$("#rating-star-" + ix).toggleClass('btn-default');
         		}
         		courseStarRating=selected_value;
         		console.log("courseStarRating::"+courseStarRating);
         		filterSearch();
         	}));
         });
      </script>
	<!--  <script>
         function openNav() {
           document.getElementById("mySidenav").style.display = "block";
           
         }
         
         function closeNav() {
           document.getElementById("mySidenav").style.display = "none";
           
         }
         </script> -->
	<!-- Custom javascript -->
	<script type="text/javascript">
         $(document).on("click", ".open-watchlistLogin", function() {
         	var trainerCourseId = $(this).data('id');
         	$(".modal-body #trainerCourseId").val(trainerCourseId);
         	// As pointed out in comments, 
         	// it is unnecessary to have to manually call the modal.
         	// $('#addBookDialog').modal('show');
         	$('#categoryWiseData').hide();
         });
      </script>
</body>
<script type="text/javascript">
      function userWatchListAddRemove(trainerCourseId, userId, watchListStatus) {
      	//alert("hi..");
      	var userWatchListData = { 
      			trainerCourseId : trainerCourseId,
      			userId : userId,
      			watchListStatus : watchListStatus
      		}
      	$.ajax({
              url: 'userWatchListAddRemove',
              type: 'post',
              data: userWatchListData,
              success: function(response) {
                  //alert('success::'+response);
                  if(response=='added'){
                 	 //alert("Course is added to your WatchList");
                	  swal({
            			  title: "Successfully Added!",
            			  text: "Course is added to your Favourite!",
            			  icon: "success",
            			});
                  } else if(response=='fail'){
                 	 //alert("Course is fail to update. Please try again");
                	  swal({
              			  title: "Failed!",
              			  text: "Course is fail to update. Please try again!",
              			  icon: "warning",
              			});
                  } else if(response=='removed'){
                	  swal({
            			  title: "Successfully Removed!",
            			  text: "Course is removed to your Favourite!",
            			  icon: "success",
            			});
                 	 //alert("Course is removed from your WatchList")
                  }
                 location.reload();
                }
          });      
      }
   </script>
<script type="text/javascript">
      $("#userLoginWrapper").submit(function(e) {
      	//alert("hi..");
      	$.ajax({
      		url : 'userLoginForFavouriteList',
      		type : 'post',
      		data : $(this).serialize(),
      		success : function(response) {
      			alert('success::'+response);
      			
      			if (response == 'error') {
      				//alert("Please Enter valid Data");
      				swal({
          			  title: "Failed!",
          			  text: "Please Enter valid Data!",
          			  icon: "warning",
          			});
      			} else if (response == 'fails') {
      				//alert("Login Details are not matched");
      				swal({
            			  title: "Failed!",
            			  text: "Login Details are not matched!",
            			  icon: "error",
            			});
      			} else if (response == 'success') {
      				//alert("Course added to your Favourites")
      				swal({
            			  title: "SuccessFully Added!",
            			  text: "Course added to your Favourite!",
            			  icon: "success",
            			});
      				location.reload();
      			}
      			location.reload();
      		}
      	})
      });
   </script>
    <script type="text/javascript">
         $(function(){
            $('.stars').stars();
         }); 
      </script>
</html>