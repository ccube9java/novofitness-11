<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>NovoFitness Trainer Details</title>
      <%@ include file="/WEB-INF/handlingJsps/adminTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/adminSession.jsp"%>
      <c:choose>
         <c:when test="${ adminSession.adminRole.roleId == 2 }">
            <c:redirect url="adminDashboard" />
         </c:when>
      </c:choose>
   </head>
   <body>
      <!-- Left Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminSidebar.jsp"%>
      <!-- Left Panel -->
      <!-- Right Panel -->
      <div id="right-panel" class="right-panel">
         <!-- Header-->
         <%@ include file="/WEB-INF/handlingJsps/adminHeader.jsp"%>
         <!-- /header -->
         <div class="breadcrumbs">
            <div class="col-sm-4">
               <div class="page-header float-left">
                  <div class="page-title">
                     <h1>Trainer Details</h1>
                  </div>
               </div>
            </div>
            <div class="col-sm-8">
               <div class="page-header float-right">
                  <div class="page-title">
                     <ol class="breadcrumb text-right">
                        <li><a href="adminTrainerManagement"><b>Back</b></a></li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
         <div class="content mt-3" id="Course-list-view">
            <div class="animated fadeIn">
               <div class="row">
                  <div class="col-xs-12 col-sm-12">
                     <div class="card">
                        <!--   <div class="card-header">
                           <strong> Trainer Details </strong>
                           </div> -->
                        <div class="card-body card-block">
                           <div class="col-xs-6 col-sm-6">
                              <div class="form-group">
                                 <label for="fName" class=" form-control-label">
                                    <fmt:message
                                       key="label.firstName" />
                                 </label>
                                 <div class="input-group">
                                    <input type="text" disabled class="form-control"
                                       id="first_name" name="first_name"
                                       value="${ trainerInfo.trainerFName}" />
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="email"  class=" form-control-label">
                                    <fmt:message key="label.email" />
                                 </label>
                                 <div class="input-group">
                                    <input type="text" disabled class="form-control"
                                       id="email_id" name="email_id"
                                       value="${ trainerInfo.trainerEmail}" />
                                 </div>
                              </div>
                           </div>
                           <div class="col-xs-6 col-sm-6">
                              <div class="form-group">
                                 <label for="lName" class=" form-control-label">
                                    <fmt:message
                                       key="label.lastName" />
                                 </label>
                                 <div class="input-group">
                                    <input type="text" disabled class="form-control"
                                       id="last_name" name="last_name"
                                       value="${ trainerInfo.trainerLName}" />
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="trainerDescription" class=" form-control-label">Description </label>
                                 <%-- <form:textarea path="trainerDescription" class="form-control" rows="2" value="${ trainerSession.trainerDescription }"/> --%>
                                 <textarea id="trainerDescription" disabled="disabled" name="trainerDescription" class="form-control" rows="2">${ trainerInfo.trainerDescription }</textarea>
                                 <!-- <textarea class="form-control" rows="2" id=""></textarea> -->
                              </div>
                              <%-- <div class="form-group">
                                 <label for="mobile" class=" form-control-label">
                                    <fmt:message key="label.mobile" />
                                 </label>
                                 <div class="input-group">
                                    <input type="text" disabled class="form-control" id="mobile_number" name="mobile_number" value=${ trainerInfo.trainerPhoneNo} />
                                 </div>
                                 </div> --%>
                           </div>
                        </div>
                     </div>
                     <div class="card">
                        <div class="card-body card-block">
                           <c:if test="${ not empty trainerInfo.trainerIDCopyInfo }">
                              <div class="col-xs-6 col-sm-6">
                                 <div class="form-group">
                                    <label class=" form-control-label">ID Copy</label>
                                    <div class="Certificates-img">
                                       <a
                                          href="${ trainerInfo.trainerIDCopyInfo.trainerIDCopyViewPath }"
                                          target="_blank"> <img
                                          src="assets/adminAssets/images/certificte-img.png"
                                          class="img-responsive"></a>
                                    </div>
                                 </div>
                              </div>
                           </c:if>
                           <c:if test="${ not empty trainerInfo.trainerCertificateInfo }">
                              <div class="col-xs-6 col-sm-6">
                                 <div class="form-group">
                                    <label for="company" class=" form-control-label">Certificates</label>
                                    <div class="Certificates-img">
                                       <a
                                          href="${ trainerInfo.trainerCertificateInfo.trainerCertificateViewPath }"
                                          target="_blank"> <img
                                          src="assets/adminAssets/images/certificte-img.png"
                                          class="img-responsive"></a>
                                    </div>
                                    <div></div>
                                 </div>
                              </div>
                           </c:if>
                           <c:if
                              test="${ empty trainerInfo.trainerIDCopyInfo && empty trainerInfo.trainerCertificateInfo }">
                              <div class="form-group">
                                 <label for="company" class=" form-control-label">No
                                 Documents Uploaded</label>
                              </div>
                           </c:if>
                           <div class="row">
                              <div class="col-md-12">
                                 <div class="approve-btn">
                                    <c:choose>
                                       <c:when test="${ trainerInfo.trainerIsApprovedByAdmin }">
                                          <button type="button" class="btn btn-danger btn-sm"
                                             onclick="approveTrainer(${ trainerInfo.trainerId }, ${ trainerInfo.trainerIsApprovedByAdmin })">
                                          Disapprove Trainer</button>
                                       </c:when>
                                       <c:when test="${ !trainerInfo.trainerIsApprovedByAdmin }">
                                          <button type="button" class="btn btn-success btn-sm"
                                             onclick="approveTrainer(${ trainerInfo.trainerId }, ${ trainerInfo.trainerIsApprovedByAdmin })">
                                          Approve Trainer</button>
                                       </c:when>
                                    </c:choose>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- .animated -->
         </div>
         <div class="content mt-3">
            <div class="animated fadeIn">
               <div class="row">
                  <div class="col-md-12">
                     <div class="card">
                        <div class="card-header">
                           <strong class="card-title">List Of Course</strong>
                           <div class="right-logo"></div>
                        </div>
                        <div class="card-body" id="userlist">
                           <table id="bootstrap-data-table-export"
                              class="table table-striped table-bordered">
                              <thead>
                                 <tr>
                                    <th>Sr No</th>
                                    <th>Course Name</th>
                                    <th>Category</th>
                                    <th>Price</th>
                                    <th>Action</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <c:choose>
                                    <c:when test="${ not empty trainerCourseList }">
                                       <c:forEach items="${ trainerCourseList }" var="trainerCourse" varStatus="loop">
                                          <tr>
                                             <td>${ loop.index+1 }</td>
                                             <td>${ trainerCourse.trainerCourseInfo.courseName }</td>
                                             <td>${ trainerCourse.trainerCourseInfo.courseCategoryInfo.categoryName }</td>
                                             <td>${ trainerCourse.trainerCourseInfo.coursePrice }</td>
                                             <td class="center-align"><a href="adminCourseView/${ trainerCourse.trainerCourseId }"><i class="fa fa-eye"></i></a> 
                                          </tr>
                                       </c:forEach>
                                    </c:when>
                                 </c:choose>
                                 <!-- <tr>
                                    <td>1</td>
                                    <td>ACROVINYASA</td>
                                    <td>Sports</td>
                                    <td>10000</td>
                                    <td class="center-align"><a href="trainercourseviw.html"><i class="fa fa-eye"></i></a></td>
                                    </tr>
                                    <tr>
                                    <td>2</td>
                                    <td>Pilates</td>
                                    <td>Sports</td>
                                    <td>10000</td>
                                    <td class="center-align"><a href="trainercourseviw.html"><i class="fa fa-eye"></i></a></td>
                                    </tr>
                                    <tr>
                                    <td>3</td>
                                    <td>BODYATTACK</td>
                                    <td>Sports</td>
                                    <td>11000</td>
                                    <td class="center-align"><a href="trainercourseviw.html"><i class="fa fa-eye"></i></a></td>
                                    </tr>
                                    <tr>
                                    <td>4</td>
                                    <td>BARRE Fitness</td>
                                    <td>Sports</td>
                                    <td>12000</td>
                                    <td class="center-align"><a href="trainercourseviw.html"><i class="fa fa-eye"></i></a></td>
                                    </tr>
                                    <tr>
                                    <td>5</td>
                                    <td>Aerial Yoga</td>
                                    <td>Sports</td>
                                    <td>40000</td>
                                    <td class="center-align"><a href="trainercourseviw.html"><i class="fa fa-eye"></i></a></td>
                                    </tr>
                                    <tr>
                                    <td>6</td>
                                    <td>ACTIVATE</td>
                                    <td>Sports</td>
                                    <td>10000</td>
                                    <td class="center-align"><a href="trainercourseviw.html"><i class="fa fa-eye"></i></a></td>
                                    </tr> -->
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- .content -->
      </div>
      <!-- Right Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminLowerFooter.jsp"%>
      <script>
         jQuery(document).ready(function() {
         	jQuery(".standardSelect").chosen({
         		disable_search_threshold : 10,
         		no_results_text : "Oops, nothing found!",
         		width : "100%"
         	});
         });
      </script>
      <script type="text/javascript">
         function approveTrainer(id,status) {
         	var trainerData = { 
         			trainerId : id,
         			trainerAdminApproveStatus : status
         		}
         	 $.ajax({
         	        type: "POST",
         	        url: "changeTrainerAdminApproveStatus",
         	        data: trainerData,
         	        success: function (result) {
         	        	if(result=="success"){
         	        		alert("Trainer's Admin approve status changed successfully.");
         	        	} 
         	        	if(result=="fail"){
         	        		alert("Fail to change Trainer status. Please try again ");
         	        	} 
         	        	if(result=="true"){
         	        		alert("Trainer approved successfully.");
         	        	} 
         	        	if(result=="false"){
         	        		alert("Trainer disapproved successfully.");
         	        	} 
         
         	        	location.reload();
         	        },
         	        error: function (result) {
         	        	location.reload();
         	        }
         	    });
         }
      </script>
   </body>
</html>