<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>NovoFitness Trainer Course Review</title>
      <%@ include file="/WEB-INF/handlingJsps/trainerTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/trainerSession.jsp"%>
   </head>
   <body>
      <!-- Left Panel Sidebar-->
      <%@ include file="/WEB-INF/handlingJsps/trainerSidebar.jsp"%>
      <div id="right-panel" class="right-panel">
         <!-- Right Panel Header-->
         <%@ include file="/WEB-INF/handlingJsps/trainerHeader.jsp"%>
         <div class="breadcrumbs">
            <div class="col-sm-4">
               <div class="page-header float-left">
                  <div class="page-title">
                     <h1>Course Review</h1>
                  </div>
               </div>
            </div>
            <div class="col-sm-8">
               <div class="page-header float-right">
                  <div class="page-title">
                     <ol class="breadcrumb text-right">
                        <li><a href="trainerCourseReviewRatingManagement"><b>Back</b></a></li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
            <div class="content mt-3">
            <div class="animated fadeIn">
               <div class="row">
                  <div class="col-md-12">
                     <div class="card">
                        <div class="card-header">
                           <strong>
                              <fmt:message key="label.userLocationReviewDescription" />
                           </strong>
                        </div>
                        <div class="card-body card-block">
                           <div class="col-xs-6 col-sm-6">
                              <div class="form-group">
                                 <label for="fName" class=" form-control-label">
                                    <fmt:message
                                       key="label.firstName" />
                                 </label>
                                 <div class="input-group">
                                    <input type="text" disabled class="form-control"
                                       id="first_name" name="first_name"
                                       value="${ reviewInfo.userInfo.userFName}" />
                                 </div>
                              </div>
                              <div class="form-group" class=" form-control-label">
                                 <label for="email">
                                    <fmt:message key="label.email" />
                                 </label>
                                 <div class="input-group">
                                    <input type="text" disabled class="form-control" id="email_id" name="email_id" value="${ reviewInfo.userInfo.userEmail }" />
                                 </div>
                              </div>
                              <div class="form-group" class=" form-control-label">
                                 <label for="email">
                                    Course Name
                                 </label>
                                 <div class="input-group">
                                    <input type="text" disabled class="form-control" id="email_id" name="email_id" value="${ reviewInfo.trainerCourseInfo.trainerCourseInfo.courseName }" />
                                 </div>
                              </div>
                               <div class="form-group" class=" form-control-label">
                                 <label for="email">
                                    Review Description
                                 </label>
                                 <div class="input-group">
                                   <textarea id="reviewDescription" name="reviewDescription" disabled="disabled" class="form-control" rows="2">${ reviewInfo.userCourseReviewDescription }</textarea>
                                 </div>
                              </div>
                           </div>
                           <div class="col-xs-6 col-sm-6">
                              <div class="form-group">
                                 <label for="lName" class=" form-control-label">
                                    <fmt:message key="label.lastName" />
                                 </label>
                                 <div class="input-group">
                                    <input type="text" disabled class="form-control" id="last_name" name="last_name" value="${ reviewInfo.userInfo.userLName}" />
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="mobile" class=" form-control-label">
                                    <fmt:message key="label.mobile" />
                                 </label>
                                 <div class="input-group">
                                    <input type="text" disabled class="form-control" id="mobile_number" name="mobile_number" value="${ reviewInfo.userInfo.userPhoneNo}" />
                                 </div>
                              </div>
                              <div class="form-group" class=" form-control-label">
                                 <label for="email">
                                    Review Title
                                 </label>
                                 <div class="input-group">
                                    <input type="text" disabled class="form-control" id="email_id" name="email_id" value="${ reviewInfo.userCourseReviewTitle }" />
                                 </div>
                              </div>
                              
                              <div class="form-group">
                                 <label for="rating" class=" form-control-label">
                                    <fmt:message key="label.userLocationStarRating" />
                                 </label>
                                 <div class="input-group">
                                     <span class="stars" data-rating="${ reviewInfo.userCourseStarRating }" data-num-stars="5" ></span>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- .animated -->
         </div>
         <!-- .content -->
      </div>
      <!-- /#right-panel -->
      <%@ include file="/WEB-INF/handlingJsps/trainerLowerFooter.jsp"%>
       <script type="text/javascript">
         $(function(){
            $('.stars').stars();
         }); 
      </script>
   </body>
</html>