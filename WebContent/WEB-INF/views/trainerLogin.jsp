<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>NovoFitness Trainer Login</title>
      <%@ include file="/WEB-INF/handlingJsps/adminTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/trainerSessionLogout.jsp"%>
      <style>
          section.login_content {
                  background: #ffffff;
                  padding: 30px 30px 20px;
                  border-radius: 2px;
                  }
                  button.btn.btn-default.submit.btn-log-in {
                  background: #272c33;
                  color: #fff;
                  margin-top: 16px;
                  }	
                  a.to_register {
                  color: #878787;
                  }
    
      
      </style>
   </head>	
   <body class="logindetail">
   
      <div class="sufee-login d-flex align-content-center flex-wrap">
         <div class="container">
          
            <div class="login-content-trainer">
               <div class="login-form" id="login-parts">
                 
                 <div class="novo-logo">
                         <a href="userHome" class="clearfix alpha-logo"> <img
                        src="assets/adminAssets/images/Nova-fitnesslogo.png" alt="white-logo">
                     </a>
                     </div>
                
                  <c:if test="${ not empty alertSuccessMessage }">
                     <div class="alert alert-success">${ alertSuccessMessage }</div>
                  </c:if>
                  <c:if
                     test="${ not empty alertSuccessMessagepasswordResetLinkMessage }">
                     <div class="alert alert-success">
                        <fmt:message key="label.passwordResetLinkMessage" />
                     </div>
                  </c:if>
                  <c:if test="${ not empty alertSuccessMessagepasswordResetSuccess }">
                     <div class="alert alert-success">
                        <fmt:message key="label.passwordResetSuccess" />
                     </div>
                  </c:if>
                  <c:if test="${ not empty alertFailMessage }">
                     <div class="alert alert-danger">${ alertFailMessage }</div>
                  </c:if>
                  <c:if test="${ not empty alertFailMessageemailNotPresent }">
                     <div class="alert alert-danger">
                        <fmt:message key="label.emailNotPresent" />
                     </div>
                  </c:if>
                  <c:if test="${ not empty alertFailMessageinvalidCredentials }">
                     <div class="alert alert-danger">
                        <fmt:message key="label.invalidCredentials" />
                     </div>
                  </c:if>
                  <c:if
                     test="${ not empty alertFailMessageuserAccountVerificationFailed }">
                     <div class="alert alert-danger">
                        <fmt:message key="label.userAccountVerificationFailed" />
                     </div>
                  </c:if>
                  <div class="login-txt">
                     <h2>Trainer Login</h2>
                  </div>
                  <form:form action="trainerLogin" method="post"
                     modelAttribute="trainerLoginWrapper">
                     <div class="form-group">
                        <%-- <label>
                                 <fmt:message key="label.enterEmail" />
                              </label> --%>
                              <label>E-Mail Address</label>
                        <fmt:message key="label.enterEmail" var="enterEmail" />
                        <form:input path="trainerEmail" class="form-control"
                           placeholder="${ enterEmail }" />
                        <form:errors path="trainerEmail" class="error" />
                     </div>
                     <div class="form-group">
                       <%--  <label>
                                 <fmt:message key="label.enterPassword" />
                              </label> --%>
                              <label>Password</label>
                        <fmt:message key="label.enterPassword" var="enterPassword" />
                        <form:password path="trainerPassword" class="form-control"
                           placeholder="${ enterPassword }" />
                        <form:errors path="trainerPassword" class="error" />
                     </div>
                     <div class="row">
                        <div class="col-md-6">
                           <!-- <div class="checkbox">
                              <label> <input type="checkbox"> Remember Me
                              </label>
                              </div> -->
                              <div class="form-group">
                              <div class="pwd-btn">
                                 <a href="trainerRegister" class="register-trainers">
                                    <fmt:message key="label.register" />
                                 </a>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <div class="pwd-btn">
                                 <a href="#" id="forgotBtn" class="forget_pwd_new">
                                    <fmt:message key="label.forgotPaasword" />
                                   
                                 </a>
                              </div>
                           </div>
                        </div>
                     </div>
                     <button type="submit"
                        class="btn btn-success btn-flat m-b-30 m-t-30">
                        <fmt:message key="label.login" />
                     </button>
                  </form:form>
               </div>
               <!-- login form -->
               <div id="retrive-password">
                  <section class="login_content">
                    <div class="novo-logo">
                         <a href="userHome" class="clearfix alpha-logo"> <img
                        src="assets/adminAssets/images/Nova-fitnesslogo.png" alt="white-logo">
                     </a>
                     </div>
                     <form:form action="trainerForgetPassword" method="post"
                        modelAttribute="trainerLoginWrapper">
                        <h3>Retrieve Password</h3>
                        <div class="form-group">
                           <div class="">
                              <fmt:message key="label.enterEmail" var="enterEmail" />
                              <form:input path="trainerEmail" class="form-control"
                                 placeholder="${ enterEmail }" />
                              <form:errors path="trainerEmail" class="error" />
                           </div>
                        </div>
                        <!--   <button type="submit" class="btn btn-send  btn-success">
                           <fmt:message key="label.submit" />
                           </button>-->
                        <a id="showlogin" class="to_register">
                           Already a member ? 
                           <fmt:message key="label.login" />
                        </a>
                        <button type="submit" class="btn btn-submit  btn-success">
                           <fmt:message key="label.submit" />
                        </button>
                        <%-- <input type="submit" class="form-control loginsub" value="<fmt:message key="label.submit" />"> --%>
                        <div class="clearfix"></div>
                        <div class="separator">
                           <div class="clearfix"></div>
                           <br />
                        </div>
                     </form:form>
                  </section>
               </div>
               <!-- retrive password -->
            </div>
         </div>
      </div>
     
      <script src="assets/adminAssets/vendors/jquery/dist/jquery.min.js"></script>
      <script type="text/javascript">
         $("#retrive-password").hide();
         $("#forgotBtn").click(function() {
         	$("#login-parts").hide();
         	$("#retrive-password").show();
         
         });
      </script>
      <script type="text/javascript">
         $("#retrive-password").hide();
         $("#showlogin").click(function() {
         	$("#login-parts").show();
         	$("#retrive-password").hide();
         });
      </script>
      <script
         src="assets/adminAssets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
       
<script type="text/javascript">javascript:window.history.forward(1);</script>
   </body>
</html>