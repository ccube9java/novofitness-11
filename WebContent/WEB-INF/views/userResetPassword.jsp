<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!DOCTYPE html>
<html lang="zxx">
   <head>
      <!-- top header -->
      <title>
         NovoFitness 
         <fmt:message key="label.resetPassword" />
      </title>
      <%@ include file="/WEB-INF/handlingJsps/userTopHeader.jsp"%>
      <!-- close top header -->
      <style>
         .form-content-box form{
         height:auto;
         }
         .form-content-box .details {
         padding: 30px 30px;
         border-radius: 5px 5px 0 0;
         background: #f3f3fb;
         }
         .footer span a{
         color: #535353;
         }
         .btn-theme{
         background-color: #f59e01;
         }
         
         
      </style>
   </head>
   <body>
      <!-- Content area start -->
      <%@ include file="/WEB-INF/handlingJsps/userHeader.jsp"%>
      <div class="container">
         <div class="row">
            <div class="col-lg-12">
               <!-- Form content box start -->
               <div class="form-content-box">
                  <!-- logo -->
                  <!-- details -->
                  <div class="details">
                     <a href="index.html" class="clearfix alpha-logo">
                     <img src="assets/userAssets/imges/bglogo.png" alt="white-logo">
                     </a>
                      <h3 class="logintitle">
                        <fmt:message key="label.resetPassword" />
                     </h3>
                     <c:if test="${ not empty alertSuccessMessage }">
                        <div class="alert alert-success">${ alertSuccessMessage }</div>
                     </c:if>
                     <c:if test="${ not empty alertFailMessage }">
                        <div class="alert alert-danger">${ alertFailMessage }</div>
                     </c:if>
                     <c:if test="${ not empty alertFailMessageuserAccountVerificationFailed }">
                        <div class="alert alert-danger"><fmt:message key="label.userAccountVerificationFailed" /></div>
                     </c:if>
                     <!-- Form start -->
                      <form:form action="resetUserPassword" method="post" modelAttribute="userResetPasswordWrapper">
                        <div class="form-group">
                        <fmt:message key="label.enterPassword" var="enterPassword"/>
                                                         <form:password path="userPassword" class="input-text" placeholder="Enter New Password " />
                                 <form:errors path="userPassword" class="error" />
                        
                           <!-- <input type="email" name="email" class="input-text" placeholder="Email Address"> -->
                        </div>
                        <div class="form-group">
                                 <fmt:message key="label.enterConfirmPassword" var="enterConfirmPassword"/>
                                 <form:password path="userConfirmPassword" class="input-text" placeholder="${ enterConfirmPassword }" />
                                 <form:errors path="userConfirmPassword" class="error" />
                              </div>
                        <div class="mb-0">
                           <button type="submit" class="btn-md btn-theme btn-block">Reset Password</button>
                        </div>
                    </form:form>
                     <!-- Form end -->
                  </div>
                  <!-- Footer -->
                  <div class="footer">
                  </div>
               </div>
               <!-- Form content box end -->
            </div>
         </div>
      </div>
      <!-- Content area end -->
      <!-- Footer start -->
  <%@ include file="/WEB-INF/handlingJsps/userFooter.jsp"%>
	<%@ include file="/WEB-INF/handlingJsps/userLowerFooter.jsp"%>
      <!-- Footer end -->
   </body>
</html>