<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>NovoFitness Sub Admin Management</title>
      <%@ include file="/WEB-INF/handlingJsps/adminTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/adminSession.jsp"%>
      <c:choose>
         <c:when test="${ adminSession.adminRole.roleId == 2 }">
            <c:redirect url="adminDashboard" />
         </c:when>
      </c:choose>
      
      <style type="text/css">
         .fa-edit {
         margin-left: 10px;
         }
      </style>
   </head>
   <body>
      <!-- Left Panel Sidebar-->
      <%@ include file="/WEB-INF/handlingJsps/adminSidebar.jsp"%>
      <div id="right-panel" class="right-panel">
         <!-- Right Panel Header-->
         <%@ include file="/WEB-INF/handlingJsps/adminHeader.jsp"%>
         <div class="breadcrumbs">
            <div class="col-sm-4">
               <div class="page-header float-left">
                  <div class="page-title">
                     <h1>Sub-admin Management</h1>
                  </div>
               </div>
            </div>
            <div class="col-sm-4">
            </div>
         </div>
         <div class="content mt-3">
            <div class="animated fadeIn">
               <div class="row">
                  <div class="col-md-12">
                     <div class="card">
                        <div class="card-header">
                           <strong class="card-title"><fmt:message key="label.subAdminList" /></strong>
                           
                           <div class="right-logo">
                              <a href="addNewSubAdminManagement">
                              <button type="submit" class="btn btn-success btn-sm">
                              <fmt:message key="label.addNewSubAdmin" />
                              </button></a>
                           
                           </div>                                              
                           
                        </div>
                        <div class="card-body" id="userlist" >
                           <table id="trainers" class="table table-striped table-bordered">
                              <thead>
                                 <tr>
                                    <th><fmt:message key="label.srNo" /></th>
                                    <th><fmt:message key="label.firstName" /></th>
                                    <th><fmt:message key="label.lastName" /></th>
                                    <th><fmt:message key="label.email" /></th>
                                    <th><fmt:message key="label.mobile" /></th>
                                    <th><fmt:message key="label.status" /></th>
                                    <th><fmt:message key="label.action" /></th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <c:choose>
                                 <c:when test="${not empty subAdminAccessRoleList}">
                                    <c:forEach items="${ subAdminAccessRoleList }" var="subAdmin" varStatus="loop">
                                       <tr>
                                          <td>${ loop.index+1 }</td>
                                          <td>${ subAdmin.subAdminInfo.adminFName }</td>
                                          <td>${ subAdmin.subAdminInfo.adminLName }</td>
                                          <td>${ subAdmin.subAdminInfo.adminEmail }</td>
                                          <td>${ subAdmin.subAdminInfo.adminPhoneNo }</td>
                                         
                                          <td>
                                             <label class="switch">
                                                 <input type="checkbox" 
                                                 ${ subAdmin.subAdminStatus?'checked':'' }
                                                onchange="changeSubAdminStatus(this,${ subAdmin.subAdminAccessRoleId }, ${ subAdmin.subAdminStatus })" />
         				                        <span class="slider round"></span>
                                             </label>
                                          </td>
                                                
                                          <td class="center-align">
                                          <a href="adminSubAdminView/${ subAdmin.subAdminAccessRoleId }"><i class="fa fa-eye"></i></a>
                                          <a href="adminSubAdminUpdate/${ subAdmin.subAdminAccessRoleId }"><i class="fa fa-edit"></i></a>
                                          <a href=""><i class="fa fa-trash" title="Delete" onclick="deleteAdminSubAdmin(${ subAdmin.subAdminAccessRoleId})" ></i></a>
                                             </td>
                                             
                                             
                                         <%--  <td>
                                          
                                             <span title="Edit" class="Edit"> 
                                             <a href="adminSubAdminViewUpdate/${ subAdminAccessRole.subAdminInfo.adminId }">
                                             <button class="btn btn-primary btn-xs">
                                             <i class="fa fa-pencil" aria-hidden="true"></i>
                                             </button>
                                             </a>
                                             </span> 
                                             <span title="Delete" class="Edit">
                                             <button class="btn btn-danger btn-xs" data-title="Delete"
                                               onclick="deleteSubAdmin(${ subAdminAccessRole.subAdminInfo.adminId })">
                                             <i class="fa fa-trash-o" aria-hidden="true"></i>
                                             </button>
                                             </span>
                                          </td> --%>
                                          
                                          
                                          
                                       </tr>
                                    </c:forEach>
                                 </c:when>
                              </c:choose>
                                <!--  <tr>
                                    <td>1</td>
                                    <td>Brielle </td>
                                    <td> Williamson</td>
                                    <td>Edi@gmail.com</td>
                                    <td>9191919191</td>
                                    <td><label class="switch"> <input type="checkbox" checked><span class="slider round"></span></label></td>
                                    <td class="">
                                       <a href="subadmin-view.html"><i class="fa fa-eye"></i></a>
                                       <a href="Subadmin-edit.html"><i class="fa fa-edit"></i></a>
                                       <a href=""><i class="fa fa-trash"></i></a>
                                    </td>
                                 </tr> -->
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- .animated -->
         </div>
      </div>
      <!-- /#right-panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminLowerFooter.jsp"%>
      
      <script>
         (function($) {
         	"use strict";
         
         	jQuery('#vmap').vectorMap({
         		map : 'world_en',
         		backgroundColor : null,
         		color : '#ffffff',
         		hoverOpacity : 0.7,
         		selectedColor : '#1de9b6',
         		enableZoom : true,
         		showTooltip : true,
         		values : sample_data,
         		scaleColors : [ '#1de9b6', '#03a9f5' ],
         		normalizeFunction : 'polynomial'
         	});
         })(jQuery);
      </script>
      <script>
         $('.myDiv').click(function() { //button click class name is myDiv
         	e.stopPropagation();
         })
         
         $(function() {
         	$('.openDiv').click(function() {
         		$('.myDiv').show();
         
         	});
         	$(document).click(function() {
         		$('.myDiv').hide(); //hide the button
         
         	});
         });
      </script>
      
      <script>
         $(document).ready(function () {
             $('#datepicker2').datepicker({
               uiLibrary: 'bootstrap'
             });
         	
         	$('#fa-calendar2').click(function() {
             $("#datepicker2").focus();
           });
         });
      </script>
      <script>
         $(document).ready(function () {
             $('#datepicker3').datepicker({
               uiLibrary: 'bootstrap'
             });
         	
         	$('#fa-calendar3').click(function() {
             $("#datepicker3").focus();
           });
         });
      </script>
      
       <script type="text/javascript">
         function changeSubAdminStatus(checkboxElem,id,status) {
         	var subAdminData = { 
         			subAdminAccessRoleId : id,
         			subAdminStatus : status
         		}
         	 $.ajax({
         	        type: "POST",
         	        url: "changeSubAdminStatus",
         	        data: subAdminData,
         	        success: function (result) {
         	        	if(result=="success"){
         	        		alert("Sub-Admin status changed successfully.");
         	        	} 
         	        	if(result=="fail"){
         	        		alert("Fail to change Sub-Admin status. Please try again ");
         	        	} 
         	        	location.reload();
         	        },
         	        error: function (result) {
         	        	location.reload();
         	        }
         	    });
         }
      </script>
        
     
      
      <script type="text/javascript">
         function deleteAdminSubAdmin(id) {
         	var subAdminData = { 
         			subAdminAccessRoleId : id,
         	}
         	$.ajax({
                 type: "POST",
                 url: "deleteAdminSubAdmin",
                 data: subAdminData,
                 success: function (result) {
                	//alert(result);
                	 if(result=="success"){
               		alert("subAdmin deleted successfully.");
                 }
                 if(result=="fail"){
                		alert("Fail to delete subAdmin. Please try again ");
                	} 
                  	location.reload();
                  },
                 error: function (result) {
                 	location.reload();
                 }
             });
         }
      </script>
      
      
      
       
   </body>
</html>