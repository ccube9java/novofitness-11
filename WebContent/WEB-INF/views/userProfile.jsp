<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!DOCTYPE html>
<html lang="zxx">
   <head>
      <!-- top header -->
      <title>
         NovoFitness 
         <fmt:message key="label.userProfile" />
      </title>
      <%@ include file="/WEB-INF/handlingJsps/userTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/userSession.jsp"%>
      <!-- close top header -->
   </head>
   <body>
      <!-- Top header start -->
      <%@ include file="/WEB-INF/handlingJsps/userHeader.jsp"%>
      <!-- Top header end -->
      <div class="elearning-elements-3" id="tg-dashboardbox">
         <div class="container">
            <div class="row learning-elements">
               <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 tg-lgcolwidthhalf">
                  <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-lgcolwidthhalf">
                  <div class="tg-dashboardbox-profile tg-profile">
                     <div class="tg-dashboardboxtitle">
                        <h2>Profile Photo</h2>
                     </div>
                     <div class="upload-profilesection">
                        <!-- <input type="file" name="file" id="profile-img">  -->
                        <c:choose>
                           <c:when test="${ not empty userProfileImageSession.userProfileImageInfo }">
                              <img
                                 src="${ userProfileImageSession.userProfileImageInfo.userProfileImageViewPath }"
                                 id="profile-img-tag" />
                              <button class="btn btn-danger btn-xs delete-profile" data-title="Delete"
                                 onclick="deleteUserProfileImage(${ userProfileImageSession.userProfileImageInfo.userProfileImageId }, ${ userProfileImageSession.userId })">
                              <i class="fa fa-trash" aria-hidden="true" title="Delete"></i>
                              </button>
                           </c:when>
                           <c:otherwise>
                              <img src="assets/adminAssets/images/profile.png"
                                 id="profile-img-tag" width="200px">
                           </c:otherwise>
                        </c:choose>
                       <%--  <form action="uploadUserProfileImage" id="userProfileImageForm"
                           method="post" enctype="multipart/form-data">
                           <input type=hidden class="form-control" name="userEmail"
                              value="${ userSession.userEmail }" />
                           <div class="btn btn-upload ">
                              <span>Upload</span>
                              <input type="file" name="userProfileImage" id="profile-img" />
                              <!-- onchange="uploadUserProfileImage();" -->
                           </div>
                        </form> --%>
                         <form action="uploadUserProfileImage"
                                 id="userProfileImageForm" method="post"
                                 enctype="multipart/form-data">
                                 <input
                                    type=hidden class="form-control" name="userEmail"
                                    value="${ userSession.userEmail }" />
                                 
                                 <input type="file" accept="image/*" name="userProfileImage"
                                    id="userProfileImage" class="form-control" />
                                 <br>
                                 <div class="form-group">
                                    <button class="btn btn-upload" type="submit">Upload</button>
                                 </div>
                              </form>
                     </div>
                  </div>
                  </div>
                  <!-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tg-lgcolwidthhalf">
                  <div class="tg-dashboardbox-profile tg-profile tg-refer-friends">
                  	<div class="btn btn-upload btn-refer">
                  		<a href="userReferalInvitation">Refer your Friend</a>
                  	</div>
                  </div>
                  </div> -->
                  </div>
               </div>
               
               <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 tg-lgcolwidthhalf" id="profileform">
                  <form:form action="updateUserProfile" method="post"
                     modelAttribute="userProfileWrapper">
                     <div class="tg-dashboardbox-profile tg-profile-details">
                        <div class="tg-dashboardboxtitle">
                           <h2>Profile Details</h2>
                        </div>
                        <c:if test="${ not empty alertSuccessMessage }">
                           <div class="alert alert-success">${ alertSuccessMessage }</div>
                        </c:if>
                        <c:if
                           test="${ not empty alertSuccessMessageUserProfileUpdateSuccess }">
                           <div class="alert alert-success">
                              <fmt:message key="label.userProfileUpdateSuccess" />
                           </div>
                        </c:if>
                        <c:if test="${ not empty alertFailMessage }">
                           <div class="alert alert-danger">${ alertFailMessage }</div>
                        </c:if>
                        <c:if
                           test="${ not empty alertFailMessageuserAccountVerificationFailed }">
                           <div class="alert alert-danger">
                              <fmt:message key="label.userAccountVerificationFailed" />
                           </div>
                        </c:if>
                        <c:if test="${ not empty alertFailMessageUserProfileUpdateFail }">
                           <div class="alert alert-danger">
                              <fmt:message key="label.userProfileUpdateFail" />
                           </div>
                        </c:if>
                        <div class="tg-dashboardholder">
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <form:input path="userFName" class="form-control"
                                       value="${ userSession.userFName }" placeholder="First Name" />
                                    <form:errors path="userFName" class="error" />
                                 </div>
                                 <div class="form-group">
                                    <form:input path="userEmail" class="form-control"
                                       value="${ userSession.userEmail }" readonly="true" placeholder="Enter Email" />
                                    <form:errors path="userEmail" class="error" />
                                 </div>
                                 <div class="form-group">
                                    <form:input path="userLocation" class="form-control"
                                       value="${ userSession.userLocation }" placeholder="Location" />
                                    <form:errors path="userLocation" class="error" />
                                 </div>
                                 <div class="form-group">
                                    <form:input path="userState" class="form-control"
                                       value="${ userSession.userState }" placeholder="State"/>
                                    <form:errors path="userState" class="error" />
                                 </div>
                                 <div class="form-group radio-element">
                                    <c:choose>
                                       <c:when test="${ not empty genderList }">
                                          <c:forEach items="${ genderList }" var="gender">
                                             <div class="radio-inline">
                                                <form:radiobutton path="genderId" value="${ gender.genderId }" checked="${ gender.genderId == userSession.genderInfo.genderId ? 'checked' : '' }" label="${ gender.genderTitle }" />
                                                <%-- <input type="radio"
                                                   name="optradio">${ gender.genderTitle } --%>
                                             </div>
                                          </c:forEach>
                                       </c:when>
                                    </c:choose>
                                 </div>
                                 <form:errors path="genderId" cssClass="error" />
                              </div>
                              <!-- end of the col md 6 here -->
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <form:input path="userLName" class="form-control"
                                       value="${ userSession.userLName }"  placeholder="Last Name" />
                                    <form:errors path="userLName" class="error" />
                                 </div>
                                 <div class="form-group">
                                    <form:input path="userPhoneNumber" class="form-control"
                                       value="${ userSession.userPhoneNo }" placeholder="Phone No" />
                                    <form:errors path="userPhoneNumber" class="error" />
                                 </div>
                                 <div class="form-group">
                                    <form:input path="userCity" class="form-control"
                                       value="${ userSession.userCity }" placeholder="City"/>
                                    <form:errors path="userCity" class="error" />
                                 </div>
                                 <div class="form-group">
                                    <form:input path="userCountry" class="form-control"
                                       value="${ userSession.userCountry }" placeholder="Country"/>
                                    <form:errors path="userCountry" class="error" />
                                 </div>
                              </div>
                              <!-- end of the col md 6 here -->
                           </div>
                           <!-- end of the row  here -->
                            </div>
                           <!-- end of the tg-dashboardholder  here -->
                           <!-- -<div class="form-group radio-element">
                              <c:choose>
                              	<c:when test="${ not empty genderList }">
                              		<c:forEach items="${ genderList }" var="gender">
                              			<label class="radio-inline">
                              <form:radiobutton path="genderId" value="${ gender.genderId }" checked="${ gender.genderId == userSession.genderInfo.genderId ? 'checked' : '' }" label="${ gender.genderTitle }" /> 
                               <%-- <input type="radio"
                                 name="optradio">${ gender.genderTitle } --%>
                              </label> 
                              		</c:forEach>
                              	</c:when>
                              </c:choose>
                              <form:errors path="genderId" cssClass="error" />
                              </div>--->
                           <button class="tg-btn" type="submit">Submit</button>
                           </div>
                           </form:form>
                           <div class="row">
                              <form:form action="changeUserPassword" method="post"
                                 modelAttribute="userChangePasswordWrapper">
                                 <div class="">
                                    <div class="tg-dashboardboxtitle">
                                       <h2>
                                          <fmt:message key="label.changePassword" />
                                       </h2>
                                       <c:if
                                          test="${ not empty alertSuccessMessagechangePasswordSuccess }">
                                          <div class="alert alert-success">
                                             <fmt:message key="label.changePasswordSuccess" />
                                          </div>
                                       </c:if>
                                       <c:if test="${ not empty alertFailMessageoldPasswordFail }">
                                          <div class="alert alert-danger">
                                             <fmt:message key="label.oldPasswordFail" />
                                          </div>
                                       </c:if>
                                       <c:if test="${ not empty alertFailMessagechangePasswordFail }">
                                          <div class="alert alert-danger">
                                             <fmt:message key="label.changePasswordFail" />
                                          </div>
                                       </c:if>
                                    </div>
                                    <div class="tg-dashboardholder">
                                       <div class="col-md-4">
                                          <div class="form-group">
                                             <fmt:message key="label.enterPassword" var="enterPassword" />
                                             <form:password path="userOldPassword" class="form-control"
                                                placeholder="Enter Old  Password" />
                                             <form:errors path="userOldPassword" class="error" />
                                          </div>
                                       </div>
                                       <div class="col-md-4">
                                          <div class="form-group">
                                             <fmt:message key="label.enterPassword" var="enterPassword" />
                                             <form:password path="userPassword" class="form-control"
                                                placeholder="Enter New Password" />
                                             <form:errors path="userPassword" class="error" />
                                          </div>
                                       </div>
                                       <div class="col-md-4">
                                          <div class="form-group">
                                             <fmt:message key="label.enterPassword"
                                                var="enterPassword" />
                                             <form:password path="userConfirmPassword" class="form-control"
                                                placeholder="${ enterPassword } again" />
                                             <form:errors path="userConfirmPassword" class="error" />
                                          </div>
                                       </div>
                                       <button class="tg-btn btn-pass" type="submit">Submit</button>
                                    </div>
                                 </div>
                              </form:form>
                           </div>
                        </div>
                     </div>
               </div>
               <%--         <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 tg-lgcolwidthhalf" id="tgforgotpwd">
                  <form:form action="changeUserPassword" method="post"
                     modelAttribute="userChangePasswordWrapper">
                     <div class="tg-dashboardbox">
                        <div class="tg-dashboardboxtitle">
                           <h2>
                              <fmt:message key="label.changePassword" />
                           </h2>
                           <c:if
                              test="${ not empty alertSuccessMessagechangePasswordSuccess }">
                              <div class="alert alert-success">
                                 <fmt:message key="label.changePasswordSuccess" />
                              </div>
                           </c:if>
                           <c:if test="${ not empty alertFailMessageoldPasswordFail }">
                              <div class="alert alert-danger">
                                 <fmt:message key="label.oldPasswordFail" />
                              </div>
                           </c:if>
                           <c:if test="${ not empty alertFailMessagechangePasswordFail }">
                              <div class="alert alert-danger">
                                 <fmt:message key="label.changePasswordFail" />
                              </div>
                           </c:if>
                        </div>
                        <div class="tg-dashboardholder">
                           <div class="form-group">
                              <fmt:message key="label.enterPassword" var="enterPassword" />
                              <form:password path="userOldPassword" class="form-control"
                                 placeholder="${ enterPassword }" />
                              <form:errors path="userOldPassword" class="error" />
                           </div>
                           <div class="form-group">
                              <fmt:message key="label.enterPassword" var="enterPassword" />
                              <form:password path="userPassword" class="form-control"
                                 placeholder="${ enterPassword }" />
                              <form:errors path="userPassword" class="error" />
                           </div>
                           <div class="form-group">
                              <fmt:message key="label.enterConfirmPassword"
                                 var="enterConfirmPassword" />
                              <form:password path="userConfirmPassword" class="form-control"
                                 placeholder="${ enterConfirmPassword }" />
                              <form:errors path="userConfirmPassword" class="error" />
                           </div>
                           <button class="tg-btn" type="submit">Submit</button>
                        </div>
                     </div>
                  </form:form>
                  </div> --%> 
            </div>
            <%--          <div class="row" id="forgot-pass">
               <div class="col-md-12">
               
                             <form:form action="changeUserPassword" method="post"
                     modelAttribute="userChangePasswordWrapper">
                     <div class="tg-dashboardbox mt-0">
                        <div class="tg-dashboardboxtitle">
                           <h2>
                              <fmt:message key="label.changePassword" />
                           </h2>
                           <c:if
                              test="${ not empty alertSuccessMessagechangePasswordSuccess }">
                              <div class="alert alert-success">
                                 <fmt:message key="label.changePasswordSuccess" />
                              </div>
                           </c:if>
                           <c:if test="${ not empty alertFailMessageoldPasswordFail }">
                              <div class="alert alert-danger">
                                 <fmt:message key="label.oldPasswordFail" />
                              </div>
                           </c:if>
                           <c:if test="${ not empty alertFailMessagechangePasswordFail }">
                              <div class="alert alert-danger">
                                 <fmt:message key="label.changePasswordFail" />
                              </div>
                           </c:if>
                        </div>
                        <div class="tg-dashboardholder">
                        
                        <div class="col-md-4">
                           <div class="form-group">
                              <fmt:message key="label.enterPassword" var="enterPassword" />
                              <form:password path="userOldPassword" class="form-control"
                                 placeholder="${ enterPassword }" />
                              <form:errors path="userOldPassword" class="error" />
                           </div>
                           </div>
                             <div class="col-md-4">
                           <div class="form-group">
                              <fmt:message key="label.enterPassword" var="enterPassword" />
                              <form:password path="userPassword" class="form-control"
                                 placeholder="${ enterPassword }" />
                              <form:errors path="userPassword" class="error" />
                           </div>
                           </div>
                             <div class="col-md-4">
                           <div class="form-group">
                              <fmt:message key="label.enterConfirmPassword"
                                 var="enterConfirmPassword" />
                              <form:password path="userConfirmPassword" class="form-control"
                                 placeholder="${ enterConfirmPassword }" />
                              <form:errors path="userConfirmPassword" class="error" />
                           </div>
                           </div>
                          
                           <button class="tg-btn btn-pass" type="submit">Submit</button>
                        </div>
                     </div>
                  </form:form>
               
               </div>
               
               </div> --%>
        <!--  </div>
      </div> -->
      <!-- Footer start -->
      <%@ include file="/WEB-INF/handlingJsps/userFooter.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/userLowerFooter.jsp"%>
      <!-- Footer end -->
      <script>
         function openNav() {
         	/* var x = document.getElementById("mySidenav");
         	if (x.style.display === "none") {
         	 x.style.display = "block";
         	 x.classList.add('active');
         	
         	} else {
         	 x.style.display = "none";
         	  
         	}*/
         	document.getElementById("mySidenav").style.display = "block";
         id
         }
         
         function closeNav() {
         	document.getElementById("mySidenav").style.display = "none";
         
         }
      </script>
      <script type="text/javascript">
         function readURL(input) {
         	if (input.files && input.files[0]) {
         		var reader = new FileReader();
         
         		reader.onload = function(e) {
         			$('#profile-img-tag').attr('src', e.target.result);
         		}
         		reader.readAsDataURL(input.files[0]);
         	}
         }
        /*  $("#profile-img").change(function() {
         	//readURL(this);
         	$("#userProfileImageForm").submit();
         	location.reload();
         }); */
      </script>
       <script type="text/javascript">
       function deleteUserProfileImage(userProfileImageId, userId) {
          swal({
              	  title: 'Are you sure?',
              	  text: "Do you really want to remove your Profile Image ?",
              	  type: 'warning',
              	  showCancelButton: true,
              	  confirmButtonColor: '#3085d6',
              	  cancelButtonColor: '#d33',
              	  confirmButtonText: 'Yes, Remove it!',
              	  closeOnConfirm: false
             	},
            		function() {
              	$.ajax({
                      type: "POST",
                      url: "deleteUserProfileImage",
                      data: "userProfileImageId="+userProfileImageId+"&userId="+userId,
                      success: function (result) {
                      }
                  })
                  .done(function(data) {
                 swal("Removed!", "Your Profile Image removed successfully!", "success");
                 location.reload();
               })
               .error(function(data) {
            	   console.log(data);
                 swal("Oops", "We couldn't connect to the server!", "error");
               });
            	});
         }
      </script>
     <!--  <script type="text/javascript">
         function deleteUserProfileImage(userProfileImageId, userId) {
          var userProfileImageData = { 
         			userProfileImageId : userProfileImageId,
         			userId : userId
         	}
         	$.ajax({
                 type: "POST",
                 url: "deleteUserProfileImage",
                 data: userProfileImageData,
                 success: function (result) {
                	 location.reload();
                 },
                 error: function (result) {
                 	location.reload();
                 }
             });
         }
      </script> -->
      <!-- Custom javascript -->
   </body>
</html>