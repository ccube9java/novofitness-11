<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!DOCTYPE html>
<html lang="zxx">
<head>
<!-- top header -->
<title>NovoFitness</title>
<%@ include file="/WEB-INF/handlingJsps/userTopHeader.jsp"%>
<c:choose>
	<c:when test="${ not empty userSession }">
		<c:redirect url="userTrainerDetailsSession" />
	</c:when>
</c:choose>
<!-- close top header -->
<style>
.addReadMore.showlesscontent .SecSec, .addReadMore.showlesscontent .readLess
	{
	display: none;
}

.addReadMore.showmorecontent .readMore {
	display: none;
}

.addReadMore .readMore, .addReadMore .readLess {
	font-weight: bold;
	margin-left: 2px;
	color: blue;
	cursor: pointer;
}

.addReadMoreWrapTxt.showmorecontent .SecSec, .addReadMoreWrapTxt.showmorecontent .readLess
	{
	display: block;
}
</style>
</head>
<body>
	<div class="page_loader"></div>
	<!-- Top header start -->
	<%@ include file="/WEB-INF/handlingJsps/userHeader.jsp"%>
	<!-- Top header end -->
	<!-- Sub banner start -->
	<section class="places-trainersdetails">
		<div class="places-indicate-users">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<div class="trainner-photo">
							<c:choose>
								<c:when
									test="${ not empty trainerDetails.trainerProfileImageInfo }">
									<img
										src="${ trainerDetails.trainerProfileImageInfo.trainerProfileImageViewPath }">
								</c:when>
								<c:otherwise>
									<img src="assets/adminAssets/images/profile.png">
								</c:otherwise>
							</c:choose>
						</div>
						<div class="instructor">${ trainerDetails.trainerFName }${ trainerDetails.trainerLName }
						</div>
						<div class="learnes-courses">
							<div class="learnes">
								<h3>9.281</h3>
								<h4>Learner's</h4>
							</div>
							<div class="learnes">
								<h3>${ trainerSCourseList.size() }</h3>
								<h4>Courses</h4>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="social-media">
							<a id="websites-btn"
								href="${ trainerDetails.trainerWebsiteLink }" target="_blank"><i
								class="fa fa-globe"></i><span class="web-txts">Website</span></a> <a
								id="facebook-btn" href="${ trainerDetails.trainerFacebookLink }"
								target="_blank"><i class="fa fa-facebook"></i><span
								class="web-txts">Facebook</span></a>
							<!--  <a id="twitter-btn" href="#"><i class="fa fa-twitter"></i>Twitter</a> -->
							<a id="instagram-btn"
								href="${ trainerDetails.trainerInstagramLink }" target="_blank"><i
								class="fa fa-instagram"></i><span class="web-txts">Instagram</span></a>
							<a id="youtube-btn" href="${ trainerDetails.trainerYoutubeLink }"
								target="_blank"><i class="fa fa-youtube"></i><span
								class="web-txts">Youtube</span></a>
						</div>
					</div>
					<!--  md-3  -->
					<div class="col-md-9">
						<div class="about-writors">
							<h3 class="myabout">About Me</h3>
							<%--  <p>${ trainerDetails.trainerDescription  }</p> --%>
							<%-- <p class="addReadMore showlesscontent">${ trainerDetails.trainerDescription  }
							</p> --%>
							 <div class="trainer-course-info">
                                                ${ trainerDetails.trainerDescription }
                                             </div>
						</div>
						<div class="row">
							<h3 class="mycoursestxts">My Courses</h3>
							<c:choose>
								<c:when test="${ not empty trainerSCourseList }">
									<c:forEach items="${ trainerSCourseList }" var="trainerSCourse">
										<div class="col-md-4 col-sm-4 col-xs-12 course-details">
											<div class="cart-over">
												<div class="card">
													<div class="course-img">
														<c:choose>
															<c:when
																test="${ not empty trainerSCourse.trainerCourseInfo.trainerCourseInfo.courseCoverImageInfo }">
																<img
																	src="${ trainerSCourse.trainerCourseInfo.trainerCourseInfo.courseCoverImageInfo.courseCoverImageViewPath }"
																	class="img-responsive">
															</c:when>
															<c:otherwise>
																<img src="assets/userAssets/imges/course11.jpg"
																	class="img-responsive">
															</c:otherwise>
														</c:choose>
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="course-name">
																<h5>
																	<b>${ trainerSCourse.trainerCourseInfo.trainerCourseInfo.courseName }</b>
																</h5>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="textinfo ">
																<h5>${ trainerSCourse.trainerCourseInfo.trainerCourseInfo.courseDescription }</h5>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="textinfo ">
																<h5>
																	<b>${ trainerSCourse.trainerCourseInfo.trainerInfo.trainerFName }
																		${ trainerSCourse.trainerCourseInfo.trainerInfo.trainerLName }</b>
																</h5>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="star-divs rating">
																<span class="stars" data-rating="0.0" data-num-stars="5"><i
																	class="fa fa-star"></i><i class="fa fa-star"></i><i
																	class="fa fa-star"></i><i class="fa fa-star"></i><i
																	class="fa fa-star"></i></span>
															</div>
														</div>
													</div>
													<!--  rows -->
													<div class="row">
														<div class="col-md-12">
															<div class="pricing-parts">
																<p>$ ${ trainerSCourse.trainerCourseInfo.trainerCourseInfo.coursePrice }</p>
															</div>
														</div>
													</div>
													<!--  rows -->
												</div>
											</div>
										</div>
									</c:forEach>
								</c:when>
								<c:otherwise>
                        	No Courses added yet.
                        </c:otherwise>
							</c:choose>
							<
							<!-- div class="col-md-4 col-sm-4 col-xs-12 course-details">
                           <div class="cart-over">
                              <div class="card">
                                 <div class="course-img">
                                    <img src="http://13.233.151.143:8090/dHJhaW5lckNvdXJzZU1lZGlhSW5mb1ZpZXdQYXRo/MnlvZ2l0YXJhdGhpMjIxMkBnbWFpbC5jb20zSU1HXzIwMjAwMzAxXzEyMDk1NmpwZw==/Y292ZXJJbWFnZQ/IMG_20200301_120956.jpg" class="img-responsive">
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="course-name">
                                          <h5>
                                             <b>Circuit Training</b>
                                          </h5>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="textinfo ">
                                          <h5>Test course 6th March 2020</h5>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="textinfo ">
                                          <h5>
                                             <b>Yogita
                                             Rathi</b>
                                          </h5>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="star-divs rating">
                                          <span class="stars" data-rating="0.0" data-num-stars="5"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
                                       </div>
                                    </div>
                                 </div>
                                  rows
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="pricing-parts">
                                          <p>$ 100</p>
                                       </div>
                                    </div>
                                 </div>
                                  rows
                              </div>
                           </div>
                        </div> -->
							<!--  sm-6  -->
							<!--   <div class="col-md-4 col-sm-4 col-xs-12 course-details">
                           <div class="cart-over">
                              <div class="card">
                                 <div class="course-img">
                                    <img src="http://13.233.151.143:8090/dHJhaW5lckNvdXJzZU1lZGlhSW5mb1ZpZXdQYXRo/MnlvZ2l0YXJhdGhpMjIxMkBnbWFpbC5jb20zSU1HXzIwMjAwMzAxXzEyMDk1NmpwZw==/Y292ZXJJbWFnZQ/IMG_20200301_120956.jpg" class="img-responsive">
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="course-name">
                                          <h5>
                                             <b>Circuit Training</b>
                                          </h5>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="textinfo ">
                                          <h5>Test course 6th March 2020</h5>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="textinfo ">
                                          <h5>
                                             <b>Yogita
                                             Rathi</b>
                                          </h5>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="star-divs rating">
                                          <span class="stars" data-rating="0.0" data-num-stars="5"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
                                       </div>
                                    </div>
                                 </div>
                                  rows
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="pricing-parts">
                                          <p>$ 100</p>
                                       </div>
                                    </div>
                                 </div>
                                  rows
                              </div>
                           </div>
                        </div>
                        md-6  -->
						</div>
						<!--  rows -->
					</div>
					<!--  md-9  -->
				</div>
				<%-- <div class="row">
                  <div class="col-lg-2 col-md-2 col-xs-2 col-sm-2 link1-padds ">
                  </div>
                  
                  <div class="col-lg-8 col-md-8 col-xs-12 col-sm-12 link1-padds ">
                  
                  <div class="row">
                     <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 ">
                        <div class="instructions-details">
                       
                       
                       <div class="instructor">
                       TRAINER
                       </div>
                       
                        <div class="name-instructor">
                       <h4>  ${ trainerDetails.trainerFName } ${ trainerDetails.trainerLName }</h4>
                       </div>
                       
                       <!-- <div class="instructor-details">
                          <b>BestSelling Personal Development Instructor 250,000Students!</b>
                       </div>
                       
                       <div class="course-details">
                        286,600 total students143 courses26,740 reviews
                       </div> -->
                       
                       <div class="about-writors">
                       <h4>About TRAINER</h4>
                        <p>${ trainerDetails.trainerDescription  }</p>
                          
                        
                          
                       </div>
                       </div>
                        
                        
                        
                  
                     </div>
                   </div>
                  
                  
                  </div>
                  <div class="col-lg-2 col-md-2 col-xs-2 col-sm-2 set-long">
                  
                  <div class="instructor-details">
                    <div class="instructor-profile">
                  
                    <div class="trainner-photo">
                    <c:choose>
                                                           		<c:when test="${ not empty trainerDetails.trainerProfileImageInfo }">
                                                           			<img src="${ trainerDetails.trainerProfileImageInfo.trainerProfileImageViewPath }">
                                                           		</c:when>
                                                           		<c:otherwise>
                                                           			 <img src="assets/adminAssets/images/profile.png">
                                                           		</c:otherwise>
                                                           	</c:choose>
                       </div>
                  
                  </div>
                  
                  </div>
                  
                  
                  </div>
                  </div>
                  </div> --%>
			</div>
		</div>
		<!-- creating module dialoge here-->
	</section>
	<!-- Footer start -->
	<%@ include file="/WEB-INF/handlingJsps/userFooter.jsp"%>
	<!-- Footer end -->
	<%@ include file="/WEB-INF/handlingJsps/userLowerFooter.jsp"%>
	<script>
		function openNav() {
			/* var x = document.getElementById("mySidenav");
			if (x.style.display === "none") {
			 x.style.display = "block";
			 x.classList.add('active');
			
			} else {
			 x.style.display = "none";
			  
			}*/
			document.getElementById("mySidenav").style.display = "block";

		}

		function closeNav() {
			document.getElementById("mySidenav").style.display = "none";

		}
	</script>
	<script>
		function AddReadMore() {
			//This limit you can set after how much characters you want to show Read More.
			var carLmt = 1000;
			// Text to show when text is collapsed
			var readMoreTxt = " ... Read More";
			// Text to show when text is expanded
			var readLessTxt = " Read Less";

			//Traverse all selectors with this class and manupulate HTML part to show Read More
			$(".addReadMore")
					.each(
							function() {
								if ($(this).find(".firstSec").length)
									return;

								var allstr = $(this).text();
								if (allstr.length > carLmt) {
									var firstSet = allstr.substring(0, carLmt);
									var secdHalf = allstr.substring(carLmt,
											allstr.length);
									var strtoadd = firstSet
											+ "<span class='SecSec'>"
											+ secdHalf
											+ "</span><span class='readMore' title='Click to Show More'>"
											+ readMoreTxt
											+ "</span><span class='readLess' title='Click to Show Less'>"
											+ readLessTxt + "</span>";
									$(this).html(strtoadd);
								}

							});
			//Read More and Read Less Click Event binding
			$(document).on(
					"click",
					".readMore,.readLess",
					function() {
						$(this).closest(".addReadMore").toggleClass(
								"showlesscontent showmorecontent");
					});
		}
		$(function() {
			//Calling function after Page Load
			AddReadMore();
		});
	</script>
	<!-- Custom javascript -->
</body>
</html>