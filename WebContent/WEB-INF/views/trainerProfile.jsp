<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!DOCTYPE html>
<html lang="en">
   <head>
      <title>
         NovoFitness Trainer 
         <fmt:message key="label.profile" />
      </title>
      <%@ include file="/WEB-INF/handlingJsps/trainerTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/trainerSession.jsp"%>
      <script src="assets/ckeditor/ckeditor.js"></script>
   </head>
   <body id="userprofile">
      <!-- Left Panel Sidebar-->
      <%@ include file="/WEB-INF/handlingJsps/trainerSidebar.jsp"%>
      <div id="right-panel" class="right-panel">
         <!-- Right Panel Header-->
         <%@ include file="/WEB-INF/handlingJsps/trainerHeader.jsp"%>
         <div class="breadcrumbs">
            <div class="row">
               <div class="col-sm-4">
                  <div class="page-header float-left">
                     <div class="page-title">
                        <h1>Update Profile</h1>
                     </div>
                  </div>
               </div>
               <div class="col-sm-8">
                  <div class="page-header float-right">
                     <div class="page-title">
                        <ol class="breadcrumb text-right">
                           <!-- <li><a href="#">Forms</a></li>
                              <li class="active">Advanced</li>>-->
                        </ol>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
             <div class="col-xs-12 col-sm-12">
               <div class="card">
                  <div class="card-body card-block" id="upload-section">
                     <div class="col-xs-5 col-sm-5">
                     </div>
                     <div class="col-xs-4 col-sm-4">
                        <%-- <div class="profile-txt">
                           <div class="update-img">
                              <c:choose>
                                 <c:when
                                    test="${ not empty trainerSession.trainerProfileImageInfo }">
                                    <img 
                                       src="${ trainerSession.trainerProfileImageInfo.trainerProfileImageViewPath }" />
                                    <button class="btn btn-danger btn-xs" data-title="Delete"
                                       onclick="deleteTrainerProfileImage(${ trainerSession.trainerProfileImageInfo.trainerProfileImageId }, ${ trainerSession.trainerId })">
                                    <i class="fa fa-trash" aria-hidden="true" title="Delete"></i>
                                    </button>
                                 </c:when>
                                 <c:otherwise>
                                    <img src="assets/adminAssets/images/profile.png">
                                 </c:otherwise>
                              </c:choose>
                           </div>
                        <!--  <form action="uploadTrainerProfileImage"
                              id="trainerProfileImageForm" method="post" 
                              enctype="multipart/form-data">
                              <input type=hidden class="form-control" name="trainerEmail"
                                 value="${ trainerSession.trainerEmail }" />
                              <div class="input-file-container"> 
                                 <input type="file" name="trainerProfileImage"
                                    id="trainerProfileImage" class="form-control input-file"
                                    onchange="uploadTrainerProfileImage();" />
                                     <i class="fa fa-edit"> </i>
                                    
                              </div>
                           </form>-->
                          <form action="uploadTrainerProfileImage"
                              id="trainerProfileImageForm" method="post" 
                              enctype="multipart/form-data">
                              <input type=hidden class="form-control" name="trainerEmail"
                                 value="${ trainerSession.trainerEmail }" />
                              <div class="input-file-container">  
                                 <input  class="input-file" id="my-file" type="file">
                                 <label tabindex="0" onchange="uploadimg(this)" for="my-file" class="input-file-trigger">Upload</label>
                                 <!-- <label tabindex="0" for="my-file"  name="trainerProfileImage"
                                    id="trainerProfileImage" onchange="uploadTrainerProfileImage();" class="input-file-trigger">Upload</label> -->
                              </div>
                              <div id="file-upload-filename"></div>
                          </form>
                             <!--  <div class="btn btn-upload "><span>Upload</span>
					               <input type="file" name="userProfileImage" id="profile-img" />
					               onchange="uploadUserProfileImage();"
					               </div> -->
                        </div> --%>
                     </div>
                  </div>
               </div>
               </div>
            <div class="content mt-3" >
               <div class="animated fadeIn">
                  <div class="row">
                     <div class="col-xs-12 col-sm-12">
                        <div class="card">
                           <div class="card-body card-block">
                              <c:if test="${ not empty alertSuccessMessage }">
                                 <div class="alert alert-success">${ alertSuccessMessage }</div>
                              </c:if>
                              <c:if test="${ not empty alertSuccessMessagechangePasswordSuccess }">
                                 <div class="alert alert-success">
                                    <fmt:message key="label.changePasswordSuccess" />
                                 </div>
                              </c:if>
                              <c:if test="${ not empty alertSuccessMessageTrainerProfileUpdateSuccess }">
                                 <div class="alert alert-success">
                                    <fmt:message key="label.trainerProfileUpdateSuccess" />
                                 </div>
                              </c:if>
                              <c:if test="${ not empty alertFailMessage }">
                                 <div class="alert alert-danger">${ alertFailMessage }</div>
                              </c:if>
                              <c:if test="${ not empty alertFailMessagechangePasswordFail }">
                                 <div class="alert alert-danger">
                                    <fmt:message key="label.changePasswordFail" />
                                 </div>
                              </c:if>
                              <c:if test="${ not empty alertFailMessageoldPasswordFail }">
                                 <div class="alert alert-danger">
                                    <fmt:message key="label.oldPasswordFail" />
                                 </div>
                              </c:if>
                              <c:if test="${ not empty alertFailMessageTrainerProfileUpdateFail }">
                                 <div class="alert alert-danger">
                                    <fmt:message key="label.trainerProfileUpdateFail" />
                                 </div>
                              </c:if>
                              <form:form action="updateTrainerProfile" method="post" modelAttribute="trainerProfileWrapper">
                                 <div class="col-sm-6 col-md-6 col-xs-12">
                                    <div class="form-group">
                                       <label for="fname">
                                          <fmt:message key="label.firstName" />
                                          <span class="error">*</span>
                                       </label>
                                       <div class="input-group">
                                          <form:input path="trainerFName" class="form-control" value="${ trainerSession.trainerFName }"/>
                                       </div>
                                    </div>
                                    <form:errors path="trainerFName" class="error" />
                                 </div>
                                 
                                 <div class="col-sm-6 col-md-6 col-xs-12">
                                    <div class="form-group">
                                       <label for="lName">
                                          <fmt:message key="label.lastName" />
                                          <span class="error">*</span>
                                       </label>
                                       <div class="input-group">
                                          <form:input path="trainerLName" class="form-control" value="${ trainerSession.trainerLName }"/>
                                       </div>
                                    </div>
                                    <form:errors path="trainerLName" class="error" />
                                 </div>
                                 
                                 <div class="col-sm-6 col-md-6 col-xs-12">
                                    <div class="form-group">
                                       <label for="email">
                                          <fmt:message key="label.email" />
                                          <span class="error">*</span>
                                       </label>
                                       <div class="input-group">
                                          <form:input path="trainerEmail" class="form-control" value="${ trainerSession.trainerEmail }" readonly="true"/>
                                       </div>
                                    </div>
                                    <form:errors path="trainerEmail" class="error" />
                                 </div>
                                 
                                 
                                  <div class="col-sm-6 col-md-6 col-xs-12">
                                    <div class="form-group">
                                       <label for="email">
                                         Paypal Id
                                          <span class="error">*</span>
                                       </label>
                                        <div class="input-group">
                                          <form:input path="trainerPayPalId" class="form-control" value="${ trainerSession.trainerPayPalId }"/>
                                       </div>
                                    </div>
                                    <form:errors path="trainerPayPalId" class="error" />
                                       <!-- <div class="input-group">
                                           <input type="text" class="form-control">
                                       </div> -->
                                    </div>
                                    
                                
                                 
                                 
                                 <label class="socia-txts">Social Media <span class="error">*</span></label>
                                 <div class="clearfix"></div>
                                 <div class="col-sm-3 col-md-3 col-xs-12">
                                    <div class="row fb_connect_img">
                                       <div class="col-md-2 label_img">
                                          <a href="#" class="fa fa-globe fa-customizes fa-websites"></a>
                                       </div>
                                       <div class="col-md-10 label_connect_txt">
                                          <!-- <input class="form-control" value="fb" placeholder="Website" name="facebook"> -->
                                          <form:input path="trainerWebsiteLink" class="form-control" value="${ trainerSession.trainerWebsiteLink }"/>
                                       </div>
                                    </div>
                                    <!-- crow fb_connect_img" -->
                                 </div>
                                 <div class="col-sm-3 col-md-3 col-xs-12">
                                    <div class="row fb_connect_img">
                                       <div class="col-md-2 label_img">
                                          <a href="#" class="fa fa-facebook fa-customizes"></a>
                                       </div>
                                       <div class="col-md-10 label_connect_txt">
                                          <!-- <input class="form-control" value="fb" placeholder="Facebook" name="facebook"> -->
                                          <form:input path="trainerFacebookLink" class="form-control" value="${ trainerSession.trainerFacebookLink }"/>
                                       </div>
                                    </div>
                                    <!-- crow fb_connect_img" -->
                                 </div>
                                 <!-- col-sm-4 col-md-4 col-xs-12" -->
                                 <%-- <div class="col-sm-4 col-md-4 col-xs-12">
                                    <div class="row fb_connect_img">
                                       <div class="col-md-2 label_img">
                                          <a href="#" class="fa fa-twitter fa-customizes-twitter"></a>
                                       </div>
                                       <div class="col-md-10 label_connect_txt">
                                          <!-- <input class="form-control" value="Twitter" placeholder="Twitter" name="Twitter"> -->
                                          <form:input path="trainerLName" class="form-control" value="${ trainerSession.trainerLName }"/>
                                       </div>
                                    </div>
                                    <!-- crow fb_connect_img" -->
                                 </div> --%>
                                 <!-- col-sm-4 col-md-4 col-xs-12" -->
                                 <div class="col-sm-3 col-md-3 col-xs-12">
                                    <div class="row fb_connect_img">
                                       <div class="col-md-2 label_img">
                                          <a href="#" class="fa fa-instagram fa-customizes-instagram"></a>
                                       </div>
                                       <div class="col-md-10 label_connect_txt">
                                          <!-- <input class="form-control" value="Instagram" placeholder="Instagram" name="Instagram"> -->
                                          <form:input path="trainerInstagramLink" class="form-control" value="${ trainerSession.trainerInstagramLink }"/>
                                       </div>
                                    </div>
                                    <!-- crow fb_connect_img" -->
                                 </div>
                                 <div class="col-sm-3 col-md-3 col-xs-12">
                                    <div class="row fb_connect_img">
                                       <div class="col-md-2 label_img">
                                          <a href="#" class="fa fa-youtube fa-customizes-youtubes"></a>
                                       </div>
                                       <div class="col-md-10 label_connect_txt">
                                          <!-- <input class="form-control" value="Twitter" placeholder="Twitter" name="Twitter"> -->
                                          <form:input path="trainerYoutubeLink" class="form-control" value="${ trainerSession.trainerYoutubeLink }"/>
                                       </div>
                                    </div>
                                    <!-- crow fb_connect_img" -->
                                 </div>
                                 <!-- col-sm-4 col-md-4 col-xs-12" -->
                                 <div class="clearfix"></div>
                                 <div class="col-md-12 desc-boxs">
                                    <label for="trainerDescription" class=" form-control-label">Description <span class="error">*</span></label>
                                    <textarea id="description-trainers" name="trainerDescription" style="width:100%; ">"${ trainerSession.trainerDescription }</textarea>
                                    <%-- <form:textarea path="trainerDescription" id="description-trainers"  style="width:100%; " class="form-control" value="${ trainerSession.trainerDescription }"/>
                                    <form:errors path="trainerDescription" class="error" /> --%>
                                    <script>
                                       CKEDITOR.replace( 'description-trainers', {
                                       	fullPage: true,
                                       	allowedContent: true,
                                       	extraPlugins: 'wysiwygarea',
                                       	width: '100%'
                                       });
                                    </script>
                                 </div>
                                 <!-- textarea row desc complete -->
                                 <%--   <div class="form-group">
                                    <label for="trainerDescription" class=" form-control-label">Description <span class="error">*</span></label>
                                    	<form:textarea path="trainerDescription" class="form-control" rows="2" value="${ trainerSession.trainerDescription }"/>
                                         <textarea id="trainerDescription" name="trainerDescription" class="form-control" rows="2">${ trainerSession.trainerDescription }</textarea>
                                    
                                    </div>
                                    <form:errors path="trainerDescription" class="error" /> --%>
                                 <div class="col-md-12 updatesspaces">
                                    <button class="btn btn-success btn-flat m-b-30 m-t-30" type="submit">Update Profile</button>
                                 </div>
                                 <!-- </div> --> <!-- first row complete -->
                                 <!--  <div class="row">
                                    </div>  --><!-- second row complete -->
                              </form:form>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-xs-12 col-sm-12">
                        <div class="card">
                           <div class="card-body card-block">
                              <div class="col-xs-6 col-sm-6">
                                 <div class="form-group">
                                    <label class=" form-control-label">ID Copy</label>
                                    <div class="Certificates-img">
                                       <a href="${ trainerSession.trainerIDCopyInfo.trainerIDCopyViewPath }" target="_blank"> 
                                       <img src="assets/adminAssets/images/certificte-img.png" class="img-responsive">
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-xs-6 col-sm-6">
                                 <div class="form-group">
                                    <label for="company" class=" form-control-label">Certificates</label>
                                    <div class="Certificates-img">
                                       <a href="${ trainerSession.trainerCertificateInfo.trainerCertificateViewPath }" target="_blank"> 
                                       <img src="assets/adminAssets/images/certificte-img.png" class="img-responsive">
                                       </a>
                                    </div>
                                    <div></div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-12">
                                    <div class="approve-btn">
                                       <%-- <c:choose>
                                          <c:when test="${ trainerInfo.trainerIsApprovedByAdmin }">
                                          	<button type="button" class="btn btn-danger btn-sm"
                                              onclick="approveTrainer(${ trainerInfo.trainerId }, ${ trainerInfo.trainerIsApprovedByAdmin })">
                                           Disapprove Trainer</button>
                                          </c:when>
                                          <c:when test="${ !trainerInfo.trainerIsApprovedByAdmin }">
                                          	<button type="button" class="btn btn-success btn-sm"
                                              onclick="approveTrainer(${ trainerInfo.trainerId }, ${ trainerInfo.trainerIsApprovedByAdmin })">
                                           Approve Trainer</button>
                                          </c:when>
                                          </c:choose> --%>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="breadcrumbs-2">
                     <div class="col-sm-8">
                        <div class="float-left">
                           <div class="page-title">
                              <h5>Change Password</h5>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-xs-12 col-sm-12">
                        <form:form action="changeTrainerPassword" method="post" modelAttribute="trainerChangePasswordWrapper">
                           <div class="card">
                              <div class="card-body">
                                 <div class="col-xs-4 col-sm-4">
                                    <div class="form-group">
                                       <label for="oldpwd">
                                          <fmt:message key="label.oldPassword" />
                                          <span class="error">*</span>
                                       </label>
                                       <div class="input-group">
                                          <fmt:message key="label.enterPassword" var="enterPassword"/>
                                          <form:password path="trainerOldPassword" class="form-control" placeholder="${ enterPassword }" />
                                       </div>
                                    </div>
                                    <!-- <div class="form-group">
                                       <label class=" form-control-label"></label>
                                       <div class="input-group">
                                          <input class="form-control" placeholder="old password"   type="text">
                                       </div>
                                       </div> -->
                                    <form:errors path="trainerOldPassword" class="error" />
                                 </div>
                                 <div class="col-xs-4 col-sm-4">
                                    <div class="form-group">
                                       <label for="newpwd">
                                          <fmt:message key="label.newPassword" />
                                          <span class="error">*</span>
                                       </label>
                                       <div class="input-group">
                                          <fmt:message key="label.enterPassword" var="enterPassword"/>
                                          <form:password path="trainerPassword" class="form-control" placeholder="${ enterPassword }" />
                                       </div>
                                    </div>
                                    <form:errors path="trainerPassword" class="error" />
                                 </div>
                                 <div class="col-xs-4 col-sm-4">
                                    <div class="form-group">
                                       <label for="conpwd">
                                          <fmt:message key="label.confirmPassword" />
                                          <span class="error">*</span>
                                       </label>
                                       <div class="input-group">
                                          <fmt:message key="label.enterPassword" var="enterPassword"/>
                                          <form:password path="trainerConfirmPassword" class="form-control" placeholder="${ enterPassword }" />
                                       </div>
                                    </div>
                                    <form:errors path="trainerConfirmPassword" class="error" />
                                 </div>
                                 <div class="col-md-12">
                                    <button class="btn btn-success btn-flat m-b-30 m-t-30 m-update" type="submit">Change Password</button>
                                 </div>
                              </div>
                           </div>
                        </form:form>
                     <!-- </div> -->
                  </div>
               </div>
            </div>
            <!-- .animated -->
         </div>
         <!-- .content -->
      </div>
      <!-- /#right-panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminLowerFooter.jsp"%>
      <script>
         //JASVSCRIPT CODE FOR UPLOADED IMAGE NAME AND ITS THUMNAILS
         function uploadimg(input){
          console.log(input);
          var input = document.getElementById( 'my-file' );
         var infoArea = document.getElementById( 'file-upload-filename' );
         
         if (input.files && input.files[0]) {
                      var reader = new FileReader();
         
                      reader.onload = function (e) {
                          $('#blah')
                              .attr('src', e.target.result);
                      };
                       document.getElementById("blah").style.display = "block";
                      
                      reader.readAsDataURL(input.files[0]);
                  }
         
         input.addEventListener( 'change', showFileName );
         
         function showFileName( event ) {
         
         
         var input = event.srcElement;
         
         
         var fileName = input.files[0].name;
         
         
         infoArea.textContent = fileName;
         }
         }
         
      </script>
      <script>
         var loadFile = function(event) {
          var image = document.getElementById('output');
          image.src = URL.createObjectURL(event.target.files[0]);
         };
      </script>
      <script>
         function readURL(input) {
           if (input.files && input.files[0]) {
             var reader = new FileReader();
             
             reader.onload = function(e) {
               $('#blah').attr('src', e.target.result);
             }
             
             reader.readAsDataURL(input.files[0]);
           }
         }
         
         $("#imgInp").change(function() {
           readURL(this);
         });
      </script>
      <script>
         function uploadTrainerProfileImage() {
          alert("hi.........")
         	 $("#trainerProfileImageForm").submit();
         }
      </script>
      <script type="text/javascript">
         function deleteTrainerProfileImage(id, profileId) {
         	var trainerProfileImageData = { 
         			trainerProfileImageId : id,
         			trainerId : profileId
         	}
         	$.ajax({
                 type: "POST",
                 url: "deleteTrainerProfileImage",
                 data: trainerProfileImageData,
                 success: function (result) {
                	 location.reload();
                 },
                 error: function (result) {
                 	location.reload();
                 }
             });
         }
      </script>
   </body>
</html>