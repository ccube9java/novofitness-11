<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
<head>
<title>NovoFitness Trainer Courses</title>
<%@ include file="/WEB-INF/handlingJsps/trainerTopHeader.jsp"%>
<%@ include file="/WEB-INF/handlingJsps/trainerSession.jsp"%>
<script src="assets/ckeditor/ckeditor.js"></script>
</head>
<body>
	<!-- Left Panel Sidebar-->
	<%@ include file="/WEB-INF/handlingJsps/trainerSidebar.jsp"%>
	<div id="right-panel" class="right-panel">
		<!-- Right Panel Header-->
		<%@ include file="/WEB-INF/handlingJsps/trainerHeader.jsp"%>
		<div class="breadcrumbs">
			<div class="col-sm-4">
				<div class="page-header float-left">
					<div class="page-title">
						<h1>Course Chapter Update</h1>
					</div>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="page-header float-right">
					<div class="page-title">
						<ol class="breadcrumb text-right">
							<li><a href="trainersCourseView/${ trainerCourseChapterDetails.trainerCourseInfo.trainerCourseId }"><b>Back</b></a></li>
						</ol>
					</div>
				</div>
			</div>
		</div>
		<div class="content mt-3">
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						<strong>Course Chapter</strong>
						<c:if test="${ not empty alertSuccessMessage }">
							<div class="alert alert-success">${ alertSuccessMessage }</div>
						</c:if>
						<c:if test="${ not empty alertSuccessMessageForUpdatingCourseChapter }">
							<div class="alert alert-success">${ alertSuccessMessageForUpdatingCourseChapter }</div>
						</c:if>
						<c:if test="${ not empty alertFailMessage }">
							<div class="alert alert-danger">${ alertFailMessage }</div>
						</c:if>
						<c:if test="${ not empty alertFailMessageForUpdatingCourseChapter }">
							<div class="alert alert-danger">${ alertFailMessageForUpdatingCourseChapter }
							</div>
						</c:if>
					</div>
					<div class="card-body card-block">
						<form:form action="updateTrainerCourseChapter" method="post"
							modelAttribute="trainerCourseChapterWrapper">
							<div class="row">
								<div class="col-lg-6 col-md-6 ">
									<div class="form-group">
										<label for="courseName" class=" form-control-label">
											Chapter Name <span class="error">*</span>
										</label>
										<form:hidden path="trainerCourseChapterId"
											value="${ trainerCourseChapterDetails.trainerCourseChapterId }" />
										<form:input path="trainerCourseChapterName"
											class="form-control" id="company"
											value="${ trainerCourseChapterDetails.trainerCourseChapterName }" />
										<form:errors path="trainerCourseChapterName" class="error" />
									</div>
								</div>
							</div>
							<!-- rows -->
							<div class="row">
								<div class="col-md-12">
									<button type="submit" class="btn btn-success btn-sm">Update Chapter</button>
								</div>
							</div>
						</form:form>
						<!-- rows -->
					</div>
				</div>
				<!-- .animated -->
			</div>
			<!-- .content -->
		</div>
		<!-- .content -->
	</div>
	<!-- /#right-panel -->
	<%@ include file="/WEB-INF/handlingJsps/trainerLowerFooter.jsp"%>
</body>
</html>