<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>
         NovoFitness 
         <fmt:message key="label.adminCategoryManagement" />
      </title>
      <%@ include file="/WEB-INF/handlingJsps/adminTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/adminSession.jsp"%>
      <c:choose>
         <c:when test="${ adminSession.adminRole.roleId == 2 }">
            <c:redirect url="adminDashboard" />
         </c:when>
      </c:choose>
   </head>
   <body>
      <!-- Left Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminSidebar.jsp"%>
      <!-- Left Panel -->
      <!-- Right Panel -->
      <div id="right-panel" class="right-panel">
         <!-- Header-->
         <%@ include file="/WEB-INF/handlingJsps/adminHeader.jsp"%>
         <!-- /header -->
         <div class="breadcrumbs">
            <div class="col-sm-4">
               <div class="page-header float-left">
                  <div class="page-title">
                     <h1>Main Category</h1>
                  </div>
               </div>
            </div>
         </div>
         <div class="content mt-3">
            <div class="animated fadeIn">
               <div class="row">
                  <div class="col-md-12">
                     <div class="card">
                        <div class="card-header">
                           <strong class="card-title">Category List</strong>
                           <c:if test="${ not empty alertSuccessMessage }">
                              <div class="alert alert-success">${ alertSuccessMessage }</div>
                           </c:if>
                           <c:if test="${ not empty alertSuccessMessagecategoryCreationSuccess }">
                              <div class="alert alert-success">
                                 <fmt:message key="label.categoryCreationSuccess" />
                              </div>
                           </c:if>
                           <c:if test="${ not empty alertFailMessage }">
                              <div class="alert alert-danger">${ alertFailMessage }</div>
                           </c:if>
                           <c:if test="${ not empty alertFailMessagecategoryCreationFail }">
                              <div class="alert alert-danger">
                                 <fmt:message key="label.categoryCreationFail" />
                              </div>
                           </c:if>
                           <div class="right-logo">
                              <a href="addNewMainCategoryManagement"><button type="submit" class="btn btn-success btn-sm">
                              Add Category
                              </button></a>
                              
                           </div>
                        </div>
                        <div class="card-body" id="userlist">
                           <table id="trainers"
                              class="table table-striped table-bordered">
                              <thead>
                                 <tr>
                                    <th>
                                       <fmt:message key="label.srNo" />
                                    </th>
                                    <th>
                                       <fmt:message key="label.titleName" />
                                    </th>
                                    <th>
                                       <fmt:message key="label.description" />
                                    </th>
                                    <th>
                                       <fmt:message key="label.status" />
                                    </th>
                                    <th>
                                       <fmt:message key="label.action" />
                                    </th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <c:choose>
                                    <c:when test="${ not empty categoryList }">
                                       <c:forEach items="${ categoryList }" var="category" varStatus="loop">
                                          <tr>
                                             <td>${ loop.index+1 }</td>
                                             <td>${ category.categoryName }</td>
                                             <td>${ category.categoryDescription }</td>
                                             <td><label class="switch"> 
                                                <input type="checkbox" ${ category.categoryIsActive ? 'checked':'' } onchange="changeCategoryStatus(this,${ category.categoryId },${ category.categoryIsActive })" />
                                                <span class="slider round"></span></label>
                                             </td>
                                             <td class="center-align">
                                                <a href="adminMainCategoryUpdate/${ category.categoryId }"><i class="fa fa-edit"></i></a>
                                                <a href=""><i class="fa fa-trash" title="Delete" onclick="deleteCategory(${ category.categoryId })" ></i></a>
                                             </td>
                                          </tr>
                                       </c:forEach>
                                    </c:when>
                                 </c:choose>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- .animated -->
         </div>
         <!-- .content -->
      </div>
      <!-- Right Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminLowerFooter.jsp"%>
      <script type="text/javascript">
         function changeCategoryStatus(checkboxElem,id,status) {
         	var categoryData = { 
         			categoryId : id,
         			categoryStatus : status
         		}
         	 $.ajax({
         	        type: "POST",
         	        url: "changeMainCategoryStatus",
         	        data: categoryData,
         	        success: function (result) {
         	        	//alert(result);
         	        	if(result=="success"){
         	        		alert("Category status changed successfully.");
         	        	} 
         	        	if(result=="fail"){
         	        		alert("Fail to change Category status. Please try again ");
         	        	} 
         	        	location.reload();
         	        },
         	        error: function (result) {
         	        	location.reload();
         	        }
         	    });
         }
      </script>
      <script type="text/javascript">
         function deleteCategory(id) {
         	var categoryData = { 
         			categoryId : id,
         	}
         	$.ajax({
                 type: "POST",
                 url: "deleteMainCategory",
                 data: categoryData,
                 success: function (result) {
                	 //alert(result);
                	 if(result=="success"){
               		alert("Category deleted successfully.");
               	} 
               	if(result=="fail"){
               		alert("Fail to delete Category. Please try again ");
               	} 
                 	location.reload();
                 },
                 error: function (result) {
                	 //alert(result);
                 	location.reload();
                 }
             });
         }
      </script>
   </body>
</html>