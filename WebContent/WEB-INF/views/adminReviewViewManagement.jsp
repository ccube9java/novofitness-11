<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>
         NovoFitness 
         <fmt:message key="label.adminUserDetails" />
      </title>
      <%@ include file="/WEB-INF/handlingJsps/adminTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/adminSession.jsp"%>
      <c:choose>
         <c:when test="${ adminSession.adminRole.roleId == 2 }">
            <c:redirect url="adminDashboard" />
         </c:when>
      </c:choose>
   </head>
   <body>
      <!-- Left Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminSidebar.jsp"%>
      <!-- Left Panel -->
      <!-- Right Panel -->
      <div id="right-panel" class="right-panel">
         <!-- Header-->
         <%@ include file="/WEB-INF/handlingJsps/adminHeader.jsp"%>
         <!-- /header -->
         <div class="breadcrumbs">
            <div class="col-sm-4">
               <div class="page-header float-left">
                  <div class="page-title">
                     <h1>
                        <fmt:message key="label.userLocationReviewDescription" />
                     </h1>
                  </div>
               </div>
            </div>
            <div class="col-sm-8">
               <div class="page-header float-right">
                  <div class="page-title">
                     <ol class="breadcrumb text-right">
                        <li><a href="adminReviewManagement"><b>Back</b></a></li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
         <div class="content mt-3">
            <div class="animated fadeIn">
               <div class="row">
                  <div class="col-md-12">
                     <div class="card">
                        <div class="card-header">
                           <strong>
                              <fmt:message key="label.userLocationReviewDescription" />
                           </strong>
                        </div>
                        <div class="card-body card-block">
                           <div class="col-xs-6 col-sm-6">
                              <div class="form-group">
                                 <label for="fName" class=" form-control-label">
                                    <fmt:message
                                       key="label.firstName" />
                                 </label>
                                 <div class="input-group">
                                    <input type="text" disabled class="form-control"
                                       id="first_name" name="first_name"
                                       value="${ reviewInfo.userInfo.userFName}" />
                                 </div>
                              </div>
                              <div class="form-group" class=" form-control-label">
                                 <label for="email">
                                    <fmt:message key="label.email" />
                                 </label>
                                 <div class="input-group">
                                    <input type="text" disabled class="form-control" id="email_id" name="email_id" value="${ reviewInfo.userInfo.userEmail }" />
                                 </div>
                              </div>
                              <div class="form-group" class=" form-control-label">
                                 <label for="email">
                                    Course Name
                                 </label>
                                 <div class="input-group">
                                    <input type="text" disabled class="form-control" id="email_id" name="email_id" value="${ reviewInfo.trainerCourseInfo.trainerCourseInfo.courseName }" />
                                 </div>
                              </div>
                               <div class="form-group" class=" form-control-label">
                                 <label for="email">
                                    Review Description
                                 </label>
                                 <div class="input-group">
                                   <textarea id="reviewDescription" name="reviewDescription" disabled="disabled" class="form-control" rows="2">${ reviewInfo.userCourseReviewDescription }</textarea>
                                 </div>
                              </div>
                           </div>
                           <div class="col-xs-6 col-sm-6">
                              <div class="form-group">
                                 <label for="lName" class=" form-control-label">
                                    <fmt:message key="label.lastName" />
                                 </label>
                                 <div class="input-group">
                                    <input type="text" disabled class="form-control" id="last_name" name="last_name" value="${ reviewInfo.userInfo.userLName}" />
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="mobile" class=" form-control-label">
                                    <fmt:message key="label.mobile" />
                                 </label>
                                 <div class="input-group">
                                    <input type="text" disabled class="form-control" id="mobile_number" name="mobile_number" value="${ reviewInfo.userInfo.userPhoneNo}" />
                                 </div>
                              </div>
                              <div class="form-group" class=" form-control-label">
                                 <label for="email">
                                    Review Title
                                 </label>
                                 <div class="input-group">
                                    <input type="text" disabled class="form-control" id="email_id" name="email_id" value="${ reviewInfo.userCourseReviewTitle }" />
                                 </div>
                              </div>
                              
                              <div class="form-group">
                                 <label for="rating" class=" form-control-label">
                                    <fmt:message key="label.userLocationStarRating" />
                                 </label>
                                 <div class="input-group">
                                     <span class="stars" data-rating="${ reviewInfo.userCourseStarRating }" data-num-stars="5" ></span>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- .animated -->
         </div>
         <!-- .content -->
      </div>
      <!-- Right Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminLowerFooter.jsp"%>
       <script type="text/javascript">
         $(function(){
            $('.stars').stars();
         }); 
      </script>
      <script>
         jQuery(document).ready(function() {
         	jQuery(".standardSelect").chosen({
         		disable_search_threshold : 10,
         		no_results_text : "Oops, nothing found!",
         		width : "100%"
         	});
         });
      </script>
   </body>
</html>