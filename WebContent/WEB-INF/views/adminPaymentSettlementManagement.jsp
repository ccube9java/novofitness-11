<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
<head>
<title>NovoFitness Payment Settlement Management</title>
<%@ include file="/WEB-INF/handlingJsps/adminTopHeader.jsp"%>
<%@ include file="/WEB-INF/handlingJsps/adminSession.jsp"%>
<c:choose>
	<c:when test="${ adminSession.adminRole.roleId == 2 }">
		<c:redirect url="adminDashboard" />
	</c:when>
</c:choose>
</head>
<body>
	<!-- Left Panel -->
	<%@ include file="/WEB-INF/handlingJsps/adminSidebar.jsp"%>
	<!-- Left Panel -->
	<!-- Right Panel -->
	<div id="right-panel" class="right-panel">
		<!-- Header-->
		<%@ include file="/WEB-INF/handlingJsps/adminHeader.jsp"%>
		<!-- /header -->
		<div class="breadcrumbs">
			<div class="col-sm-4">
				<div class="page-header float-left">
					<div class="page-title">
						<h1>Set Payment Contribution</h1>
					</div>
				</div>
			</div>
			<!-- <div class="col-sm-8">
               <div class="page-header float-right">
                  <div class="page-title">
                     <ol class="breadcrumb text-right">
                        <li><a href="adminMainCategoryManagement"><b>Back</b></a></li>
                     </ol>
                  </div>
               </div>
            </div> -->
		</div>
		<div class="content mt-3">
			<div class="animated fadeIn">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<strong>Set Payment Contribution</strong>
								<c:if test="${ not empty alertSuccessMessage }">
									<div class="alert alert-success">${ alertSuccessMessage }</div>
								</c:if>
								<c:if
									test="${ not empty alertSuccessMessagePaymentContribution }">
									<div class="alert alert-success">
										<fmt:message key="label.categoryCreationSuccess" />
									</div>
								</c:if>
								<c:if test="${ not empty alertFailMessage }">
									<div class="alert alert-danger">${ alertFailMessage }</div>
								</c:if>
								<c:if test="${ not empty alertFailPaymentContribution }">
									<div class="alert alert-danger">
										<fmt:message key="label.categoryCreationFail" />
									</div>
								</c:if>
							</div>

							<div class="card-body card-block">
								<form:form action="addPaymentSettlementContribution" method="post"
									modelAttribute="adminPaymentSettlementWrapper">
									<div class="row">
										<div class="col-lg-6 col-md-6 ">
											<div class="form-group">
												<label for="categoryName" class="form-control-label">
													Trainer Payment Percentage Contribution  <span class="error">*</span>
												</label>
												<form:input path="percentageContribution" class="form-control"
													id="company" placeholder="Enter Contribution Percentage" />
												<form:errors path="percentageContribution" class="error" />
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-md-12">
											<button type="submit" class="btn btn-success btn-sm">
												Submit
											</button>
										</div>
									</div>
								</form:form>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<strong class="card-title">Payment Settlement Contribution</strong>
							</div>
							<div class="card-body" id="userlist">
								<table id="trainers" class="table table-striped table-bordered">
									<thead>
										<tr>
											<th><fmt:message key="label.srNo" /></th>
											<th>Current Trainer Contribution</th>
										</tr>
									</thead>
									<tbody>
										<c:choose>
											<c:when test="${ not empty courseContributionList }">
												<c:forEach items="${ courseContributionList }"
													var="courseContribution" varStatus="loop">
													<tr>
														<td>${ loop.index+1 }</td>
														<td>${ courseContribution.courseContributionPercentage }</td>
													</tr>
												</c:forEach>
											</c:when>
										</c:choose>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- .animated -->
		</div>
		<!-- .content -->
	</div>
	<!-- Right Panel -->
	<%@ include file="/WEB-INF/handlingJsps/adminLowerFooter.jsp"%>
	<script>
		jQuery(document).ready(function() {
			jQuery(".standardSelect").chosen({
				disable_search_threshold : 10,
				no_results_text : "Oops, nothing found!",
				width : "100%"
			});
		});
	</script>
</body>
</html>