<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!DOCTYPE html>
<html lang="en">
   <head>
      <title>
         NovoFitness Admin 
         <fmt:message key="label.profile" />
      </title>
      <%@ include file="/WEB-INF/handlingJsps/adminTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/adminSession.jsp"%>
      
      <style>
      #my-file{
      display:none;}
      </style>
   </head>
   <body>
      <!-- Left Panel Sidebar-->
      <%@ include file="/WEB-INF/handlingJsps/adminSidebar.jsp"%>
      <div id="right-panel" class="right-panel">
         <!-- Right Panel Header-->
         <%@ include file="/WEB-INF/handlingJsps/adminHeader.jsp"%>
         <div class="breadcrumbs">
            <div class="row">
               <div class="col-sm-4">
                  <div class="page-header float-left">
                     <div class="page-title">
                        <h1>Update Profile</h1>
                     </div>
                  </div>
               </div>
               <div class="col-sm-8">
                  <div class="page-header float-right">
                     <div class="page-title">
                        <ol class="breadcrumb text-right">
                           <!-- <li><a href="#">Forms</a></li>
                              <li class="active">Advanced</li>>-->
                        </ol>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-xs-12 col-sm-12">
               <div class="card">
                  <div class="card-body card-block" id="upload-section">
                     <div class="col-xs-5 col-sm-5">
                     </div>
                     <div class="col-xs-4 col-sm-4">
                        <div class="profile-txt">
                           <div class="update-img">
                              <c:choose>
                                 <c:when
                                    test="${ not empty adminSession.adminProfileImageInfo }">
                                    <img 
                                       src="${ adminSession.adminProfileImageInfo.adminProfileImageViewPath }" />
                                    <button class="btn btn-danger btn-xs" data-title="Delete"
                                       onclick="deleteAdminProfileImage(${ adminSession.adminProfileImageInfo.adminProfileImageId }, ${ adminSession.adminId })">
                                    <i class="fa fa-trash" aria-hidden="true" title="Delete"></i>
                                    </button>
                                 </c:when>
                                 <c:otherwise>
                                    <img src="assets/adminAssets/images/profile.png">
                                 </c:otherwise>
                              </c:choose>
                           </div>
                           <!-- <form action="uploadAdminProfileImage"
                              id="adminProfileImageForm" method="post" 
                              enctype="multipart/form-data">
                              <input type=hidden class="form-control" name="adminEmail"
                                 value="${ adminSession.adminEmail }" />
                              <div class="input-file-container"> 
                                 <input type="file" name="adminProfileImage"
                                    id="adminProfileImage" class="form-control input-file"
                                    onchange="preview_image();" />
                                     <i class="fa fa-edit"> </i>
                                    
                              </div>
                           </form>---->
                           <form action="#">
                              <div class="input-file-container">  
                                 <input  class="input-file" id="my-file" type="file">
                                 <label tabindex="0" onchange="uploadimg(this)" onfocus="uploadimg(this)" for="my-file" class="input-file-trigger">Upload </label>
                              </div>
                              <div id="file-upload-filename"></div>
                              
                              </form> 
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="content mt-3" >
               <div class="animated fadeIn">
                  <div class="row">
                     <div class="col-xs-12 col-sm-12">
                        <div class="card">
                           <div class="card-body card-block">
                              <c:if test="${ not empty alertSuccessMessage }">
                                 <div class="alert alert-success">${ alertSuccessMessage }</div>
                              </c:if>
                              <c:if test="${ not empty alertSuccessMessagechangePasswordSuccess }">
                                 <div class="alert alert-success">
                                    <fmt:message key="label.changePasswordSuccess" />
                                 </div>
                              </c:if>
                              <c:if test="${ not empty alertSuccessMessageadminProfileUpdateSuccess }">
                                 <div class="alert alert-success">
                                    <fmt:message key="label.adminProfileUpdateSuccess" />
                                 </div>
                              </c:if>
                              <c:if test="${ not empty alertFailMessage }">
                                 <div class="alert alert-danger">${ alertFailMessage }</div>
                              </c:if>
                              <c:if test="${ not empty alertFailMessagechangePasswordFail }">
                                 <div class="alert alert-danger">
                                    <fmt:message key="label.changePasswordFail" />
                                 </div>
                              </c:if>
                              <c:if test="${ not empty alertFailMessageoldPasswordFail }">
                                 <div class="alert alert-danger">
                                    <fmt:message key="label.oldPasswordFail" />
                                 </div>
                              </c:if>
                              <c:if test="${ not empty alertFailMessageadminProfileUpdateFail }">
                                 <div class="alert alert-danger">
                                    <fmt:message key="label.adminProfileUpdateFail" />
                                 </div>
                              </c:if>
                              <form:form action="updateAdminProfile" method="post" modelAttribute="adminProfile">
                                 <div class="col-xs-6 col-sm-6">
                                    <div class="form-group">
                                       <label for="fname">
                                          <fmt:message key="label.firstName" />
                                          <span class="error">*</span>
                                       </label>
                                       <div class="input-group">
                                          <form:input path="adminFName" class="form-control" value="${ adminSession.adminFName }"/>
                                          <form:errors path="adminFName" class="error" />
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label for="email">
                                          <fmt:message key="label.email" />
                                          <span class="error">*</span>
                                       </label>
                                       <div class="input-group">
                                          <form:input path="adminEmail" class="form-control" value="${ adminSession.adminEmail }" readonly="true"/>
                                          <form:errors path="adminEmail" class="error" />
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-xs-6 col-sm-6">
                                    <div class="form-group">
                                       <label for="lName">
                                          <fmt:message key="label.lastName" />
                                          <span class="error">*</span>
                                       </label>
                                       <div class="input-group">
                                          <form:input path="adminLName" class="form-control" value="${ adminSession.adminLName }"/>
                                          <form:errors path="adminLName" class="error" />
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label for="mobile">
                                          <fmt:message key="label.mobile" />
                                          <span class="error">*</span>
                                       </label>
                                       <div class="input-group">
                                          <form:input path="adminPhoneNumber" class="form-control" value="${ adminSession.adminPhoneNo }"/>
                                          <form:errors path="adminPhoneNumber" class="error" />
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <button class="btn btn-success btn-flat m-b-30 m-t-30" type="submit">Update Profile</button>
                                 </div>
                              </form:form>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="breadcrumbs-2">
                     <div class="col-sm-8">
                        <div class="float-left">
                           <div class="page-title">
                              <h5>Change Password</h5>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                  <div class="col-xs-12 col-sm-12">
                     <form:form action="changeAdminPassword" method="post" modelAttribute="adminChangePassword">
                           <div class="card">
                              <div class="card-body">
                                 <div class="col-xs-4 col-sm-4">
                                    <div class="form-group">
                                       <label for="oldpwd">
                                          <fmt:message key="label.oldPassword" />
                                          <span class="error">*</span>
                                       </label>
                                       <div class="input-group">
                                          <fmt:message key="label.enterPassword" var="enterPassword"/>
                                          <form:password path="adminOldPassword" class="form-control" placeholder="${ enterPassword }" />
                                          <form:errors path="adminOldPassword" class="error" />
                                       </div>
                                    </div>
                                    <!-- <div class="form-group">
                                       <label class=" form-control-label"></label>
                                       <div class="input-group">
                                          <input class="form-control" placeholder="old password"   type="text">
                                       </div>
                                       </div> -->
                                 </div>
                                 <div class="col-xs-4 col-sm-4">
                                    <div class="form-group">
                                       <label for="newpwd">
                                          <fmt:message key="label.newPassword" />
                                          <span class="error">*</span>
                                       </label>
                                       <div class="input-group">
                                          <fmt:message key="label.enterPassword" var="enterPassword"/>
                                          <form:password path="adminPassword" class="form-control" placeholder="${ enterPassword }" />
                                          <form:errors path="adminPassword" class="error" />
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-xs-4 col-sm-4">
                                    <div class="form-group">
                                       <label for="conpwd">
                                          <fmt:message key="label.confirmPassword" />
                                          <span class="error">*</span>
                                       </label>
                                       <div class="input-group">
                                          <fmt:message key="label.enterPassword" var="enterPassword"/>
                                          <form:password path="adminConfirmPassword" class="form-control" placeholder="${ enterPassword }" />
                                          <form:errors path="adminConfirmPassword" class="error" />
                                       </div>
                                    </div>
                                 </div>
                                 <!-- <div class="col-xs-4 col-sm-4">
                                    <div class="form-group">
                                       <label class=" form-control-label"></label>
                                       <div class="input-group">
                                          <input class="form-control" placeholder="new password"  type="text">
                                       </div>
                                    </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-4">
                                    <div class="form-group">
                                       <label class=" form-control-label">  </label>
                                       <div class="input-group">
                                          <input class="form-control" placeholder="confirm password" type="text">
                                       </div>
                                    </div>
                                    </div> -->
                                 <div class="col-md-12">
                                    <button class="btn btn-success btn-flat m-b-30 m-t-30" type="submit">Change Password</button>
                                 </div>
                              </div>
                           </div>
                           </form:form>
                        </div>
                     
                  </div>
               </div>
            </div>
            <!-- .animated -->
         </div>
         <!-- .content -->
      </div>
      <!-- /#right-panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminLowerFooter.jsp"%>
      <script>
         //JASVSCRIPT CODE FOR UPLOADED IMAGE NAME AND ITS THUMNAILS
         function uploadimg(input){
          var input = document.getElementById( 'my-file' );
         var infoArea = document.getElementById( 'file-upload-filename' );
         
         if (input.files && input.files[0]) {
                      var reader = new FileReader();
         
                      reader.onload = function (e) {
                          $('#blah')
                              .attr('src', e.target.result);
                      };
                       document.getElementById("blah").style.display = "block";
                      
                      reader.readAsDataURL(input.files[0]);
                  }
         
         input.addEventListener( 'change', showFileName );
         
         function showFileName( event ) {
         
         
         var input = event.srcElement;
         
         
         var fileName = input.files[0].name;
         
         
         infoArea.textContent = fileName;
         }
         }
         
      </script>
      <script>
         var loadFile = function(event) {
          var image = document.getElementById('output');
          image.src = URL.createObjectURL(event.target.files[0]);
         };
      </script>
      <script>
         function readURL(input) {
           if (input.files && input.files[0]) {
             var reader = new FileReader();
             
             reader.onload = function(e) {
               $('#blah-2').attr('src', e.target.result);
             }
             
             reader.readAsDataURL(input.files[0]);
           }
         }
         
         $("#imgInp").change(function() {
           readURL(this);
         });
      </script>
      <script>
         function preview_image() {
         /* $('#image_preview2').empty();
         	var total_file = document.getElementById("adminProfileImage").files.length;
         	for (var i = 0; i < total_file; i++) {
         		$('#image_preview2').append(
         				"<div class='col-xs-6 col-sm-3 col-md-3 col-lg-3'><div class='img_thumb'><img class='img-responsives' src='"
         						+ URL.createObjectURL(event.target.files[i])
         						+ "'></div></div>");
         	} */
         	 $("#adminProfileImageForm").submit();
         }
      </script>
      <script type="text/javascript">
         function deleteAdminProfileImage(id, profileId) {
         	var adminProfileImageData = { 
         			adminProfileImageId : id,
         			adminId : profileId
         	}
         	$.ajax({
                 type: "POST",
                 url: "deleteAdminProfileImage",
                 data: adminProfileImageData,
                 success: function (result) {
                	 location.reload();
                 },
                 error: function (result) {
                 	location.reload();
                 }
             });
         }
      </script>
   </body>
</html>