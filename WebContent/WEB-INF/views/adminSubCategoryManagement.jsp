<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>NovoFitness <fmt:message key="label.subCategory" /></title>
      <%@ include file="/WEB-INF/handlingJsps/adminTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/adminSession.jsp"%>
      <c:choose>
         <c:when test="${ adminSession.adminRole.roleId == 2 }">
            <c:redirect url="adminDashboard" />
         </c:when>
      </c:choose>
   </head>
   <body>
      <!-- Left Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminSidebar.jsp"%>
      <!-- Left Panel -->
      <!-- Right Panel -->
      <div id="right-panel" class="right-panel">
         <!-- Header-->
         <%@ include file="/WEB-INF/handlingJsps/adminHeader.jsp"%>
         <!-- /header -->
         <div class="breadcrumbs">
            <div class="col-sm-4">
               <div class="page-header float-left">
                  <div class="page-title">
                     <h1> <fmt:message key="label.subCategory" /></h1>
                  </div>
               </div>
            </div>
         </div>
         <div class="content mt-3">
            <div class="animated fadeIn">
               <div class="row">
                  <div class="col-md-12">
                     <div class="card">
                        <div class="card-header">
                           <strong class="card-title">Sub-Category List</strong>
                           <c:if test="${ not empty alertSuccessMessage }">
                     <div class="alert alert-success">${ alertSuccessMessage }</div>
                  </c:if>
                  <c:if test="${ not empty alertSuccessMessageaddNewSubCategorySuccess }">
                     <div class="alert alert-success"><fmt:message key="label.addNewSubCategorySuccess" /></div>
                  </c:if>
                  <c:if test="${ not empty alertFailMessage }">
                     <div class="alert alert-danger">${ alertFailMessage }</div>
                  </c:if>
                  <c:if test="${ not empty alertFailMessageaddNewSubCategoryFail }">
                     <div class="alert alert-danger"><fmt:message key="label.addNewSubCategoryFail" /></div>
                  </c:if>
                           <div class="right-logo">
                           	   <a href="addNewSubCategoryManagement"><button type="submit" class="btn btn-success btn-sm">
                                    Add SubCategory
                                      </button></a>
                              <a href=""><img src="assets/adminAssets/images/pdf-512.png" class="src-img"></a> 
                              <a href=""><img class="src-img-2" src="assets/adminAssets/images/icons8-microsoft-excel-48.png"></a>
                           </div>
                        </div>
                        <div class="card-body" id="userlist">
                           <table id="bootstrap-data-table-export"
                              class="table table-striped table-bordered">
                              <thead>
                                 <tr>
                                    <th>
                                       <fmt:message key="label.srNo" />
                                    </th>
                                    <th><fmt:message key="label.mainCategoryName" /></th>
                                 <th><fmt:message key="label.subCategoryName" /></th>
                                 <th>
                                    <fmt:message key="label.description" />
                                 </th>
                                    <th>
                                       <fmt:message key="label.status" />
                                    </th>
                                    <th>
                                       <fmt:message key="label.action" />
                                    </th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <c:choose>
                                    <c:when test="${ not empty subCategoryList }">
                                       <c:forEach items="${ subCategoryList }" var="subCategory" varStatus="loop">
                                          <tr>
                                             <td>${ loop.index+1 }</td>
                                             <td>${ subCategory.categoryInfo.categoryName }</td>
                                             <td>${ subCategory.subcategoryName }</td>
                                             <td>${ subCategory.subcategoryDescription }</td>
                                             <td><label class="switch"> 
                                                <input type="checkbox" ${ subCategory.subcategoryIsActive ? 'checked':'' } onchange="changeSubCategoryStatus(this,${ subCategory.subcategoryId },${ subCategory.subcategoryIsActive })" />
                                                <span class="slider round"></span></label>
                                             </td>
                                             <td class="center-align">
                                               <a href="adminSubCategoryUpdate/${ subCategory.subcategoryId }"><i class="fa fa-edit"></i></a>
                                                <a href=""><i class="fa fa-trash" title="Delete" onclick="deleteSubCategory(${ subCategory.subcategoryId })" ></i></a>
                                            </td>
                                          </tr>
                                       </c:forEach>
                                    </c:when>
                                 </c:choose>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- .animated -->
         </div>
         <!-- .content -->
      </div>
      <!-- /#right-panel -->
      <!-- Right Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminLowerFooter.jsp"%>
      <script>
         $(document).ready(function() {
         	$('#datepicker2').datepicker({
         		uiLibrary : 'bootstrap'
         	});
         
         	$('#fa-calendar2').click(function() {
         		$("#datepicker2").focus();
         	});
         });
      </script>
      <script>
         $(document).ready(function() {
         	$('#datepicker3').datepicker({
         		uiLibrary : 'bootstrap'
         	});
         
         	$('#fa-calendar3').click(function() {
         		$("#datepicker3").focus();
         	});
         });
      </script>
       <script type="text/javascript">
         function changeSubCategoryStatus(checkboxElem,id,status) {
         	var subCategoryData = { 
         			subcategoryId : id,
         			subcategoryStatus : status
         		}
         	 $.ajax({
         	        type: "POST",
         	        url: "changeSubCategoryStatus",
         	        data: subCategoryData,
         	        success: function (result) {
         	        	if(result=="success"){
         	        		alert("Sub Category status changed successfully.");
         	        	} 
         	        	if(result=="fail"){
         	        		alert("Fail to change Sub Category status. Please try again ");
         	        	} 

         	        	location.reload();
         	        },
         	        error: function (result) {
         	        	location.reload();
         	        }
         	    });
         }
      </script>
      <script type="text/javascript">
         function deleteSubCategory(id) {
         	var subCategoryData = { 
         			subcategoryId : id,
         	}
         	$.ajax({
                 type: "POST",
                 url: "deleteSubCategory",
                 data: subCategoryData,
                 success: function (result) {
                	 if(result=="success"){
                    		alert("Sub Category deleted successfully.");
                    	} 
                    	if(result=="fail"){
                    		alert("Fail to delete Sub Category. Please try again ");
                    	} 
                 	location.reload();
                 },
                 error: function (result) {
                 	location.reload();
                 }
             });
         }
      </script>
   </body>
</html>