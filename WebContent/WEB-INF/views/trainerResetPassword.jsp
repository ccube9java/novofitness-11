<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!DOCTYPE html>
<html lang="en">
   <head>
      <title>
         NovoFitness Trainer 
         <fmt:message key="label.resetPassword" />
      </title>
      <%@ include file="/WEB-INF/handlingJsps/trainerTopHeader.jsp"%>
      <style>
      .card.card-registerpassword.mx-auto.mt-5 {
    width: 500px;
}
    input.form-control.loginsub.resetpwd {
    background: #f49e00;
    border-color: #f49e00 !important;
    color: #fff !important;
    font-size: 14px;
    font-weight: 600;
    text-transform: uppercase;
}  
.form-pwd {
    font-size: 14px;
    color: #000;
}
      </style>
   </head>
   <body class="">
      <div class="container">
         <div class="logos">
            <!-- <h3>NovaFitness Trainer</h3> -->
         </div>
         <div class="card card-registerpassword mx-auto mt-5">
            <div class="card-header">
               <fmt:message key="label.resetPassword" />
            </div>
            <div class="card-body">
               <c:if test="${ not empty alertSuccessMessage }">
                  <div class="alert alert-success">${ alertSuccessMessage }</div>
               </c:if>
               <c:if test="${ not empty alertFailMessage }">
                  <div class="alert alert-danger">${ alertFailMessage }</div>
               </c:if>
               <c:if test="${ not empty alertFailMessageuserAccountVerificationFailed }">
					<div class="alert alert-danger"><fmt:message key="label.userAccountVerificationFailed" /></div>
				</c:if>
               <form:form action="resetTrainerPassword" method="post" modelAttribute="trainerResetPasswordWrapper">
                  <div class="form-group">
                     <div class="form-row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <form:password path="trainerPassword" class="form-control form-pwd" placeholder="New Password" />
                              <form:errors path="trainerPassword" class="error" />
                           </div>
                        </div>
                        <div class="col-md-12">
                           <div class="form-group">
                              <form:password path="trainerConfirmPassword" class="form-control form-pwd" placeholder="Confirm Password" />
                              <form:errors path="trainerConfirmPassword" class="error" />
                           </div>
                        </div>
                     </div>
                  </div>
                  <input type="submit" class="form-control loginsub resetpwd" value="<fmt:message key="label.submit" />">
               </form:form>
               <div class="text-center">
                  <a class="d-block small mt-3 logindetails" href="trainerLogin"><%-- <fmt:message key="label.login" /> --%>Already a member? Login here</a>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>