<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>NovoFitness <fmt:message key="label.adminUserManagement" /></title>
      <%@ include file="/WEB-INF/handlingJsps/adminTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/adminSession.jsp"%>
      <c:choose>
         <c:when test="${ adminSession.adminRole.roleId == 2 }">
            <c:redirect url="adminDashboard" />
         </c:when>
      </c:choose>
   </head>
   <body>
      <!-- Left Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminSidebar.jsp"%>
      <!-- Left Panel -->
      <!-- Right Panel -->
      <div id="right-panel" class="right-panel">
         <!-- Header-->
         <%@ include file="/WEB-INF/handlingJsps/adminHeader.jsp"%>
         <!-- /header -->
         <div class="breadcrumbs">
            <div class="col-sm-4">
               <div class="page-header float-left">
                  <div class="page-title">
                     <h1>User Management</h1>
                  </div>
               </div>
            </div>
            <div class="col-sm-4"></div>
         </div>
         <div class="content mt-3">
            <div class="animated fadeIn">
               <div class="row">
                  <div class="col-md-12">
                     <div class="card">
                        <div class="card-header">
                           <strong class="card-title">List Of User</strong>
                           
                        </div>
                        <div class="card-body" id="userlist">
                           <!-- <table id="bootstrap-data-table-export"
                              class="table table-striped table-bordered"> -->
                                <table id="trainers" class="table table-striped table-bordered" style="width:100%">
                              <thead>
                                 <tr>
                                    <th>
                                       <fmt:message key="label.srNo" />
                                    </th>
                                    <th>
                                       <fmt:message key="label.firstName" />
                                    </th>
                                    <th>
                                       <fmt:message key="label.lastName" />
                                    </th>
                                    <th>
                                       <fmt:message key="label.email" />
                                    </th>
                                    <th>
                                    <fmt:message key="label.mobile" />
                                 </th>
                                    <th>
                                       <fmt:message key="label.status" />
                                    </th>
                                    <th>
                                       <fmt:message key="label.action" />
                                    </th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <c:choose>
                                    <c:when test="${ not empty userList }">
                                       <c:forEach items="${ userList }" var="user" varStatus="loop">
                                          <tr>
                                             <td>${ loop.index+1 }</td>
                                             <td>${ user.userFName }</td>
                                             <td>${ user.userLName }</td>
                                             <td>${ user.userEmail }</td>
                                             <td>${ user.userPhoneNo }</td>
                                             <td><label class="switch">
                                             	<input type="checkbox" 
                                                ${ user.userIsActive?'checked':'' } 
                                                onchange="changeUserStatus(this,${ user.userId },${ user.userIsActive })" /> 
                                                <span class="slider round"></span></label>
                                             </td>
                                             <td class="center-align">
                                                <a href="adminUsersView/${ user.userId }"><i class="fa fa-eye"></i></a> 
                                                <!-- <a href=""><i class="fa fa-trash"></i></a> -->
                                             </td>
                                          </tr>
                                       </c:forEach>
                                    </c:when>
                                 </c:choose>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- .animated -->
         </div>
         <!-- .content -->
      </div>
      <!-- /#right-panel -->
      <!-- Right Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminLowerFooter.jsp"%>
      <!-- <script type="text/javascript">
         $(document).ready(function() {
             var table = $('#example').DataTable( {
                 lengthChange: true,
                 buttons: [ 'excel', 'pdf' ],
                /*  "oLanguage": {
                     "sLengthMenu": "Show _MENU_ entries",
                     "sSearch": "<fmt:message key="label.search" /> : ",
                     "oPaginate": {
                         "sNext":     "<fmt:message key="label.next" />",
                         "sPrevious": "<fmt:message key="label.previous" />"
                     },
                     "sEmptyTable":     "No data available in table",
                     "sInfo":           "Showing _START_ to _END_ of _TOTAL_ entries",
                     "sInfoEmpty":      "Showing 0 to 0 of 0 entries",
                     "sInfoFiltered":   "(filtered from _MAX_ total entries)",
                     "sZeroRecords":    "No matching records found",
                   } */
             } );
          
             table.buttons().container()
                 .appendTo( '#example_wrapper .col-md-6:eq(0)' );
         } );
         
      </script> -->
      <script>
         $(document).ready(function() {
         	$('#datepicker2').datepicker({
         		uiLibrary : 'bootstrap'
         	});
         
         	$('#fa-calendar2').click(function() {
         		$("#datepicker2").focus();
         	});
         });
      </script>
      <script>
         $(document).ready(function() {
         	$('#datepicker3').datepicker({
         		uiLibrary : 'bootstrap'
         	});
         
         	$('#fa-calendar3').click(function() {
         		$("#datepicker3").focus();
         	});
         });
      </script>
        <script type="text/javascript">
         function changeUserStatus(checkboxElem,id,status) {
         	var userData = { 
         			userId : id,
         			userStatus : status
         		}
         	 $.ajax({
         	        type: "POST",
         	        url: "changeUserStatus",
         	        data: userData,
         	        success: function (result) {
         	        	if(result=="success"){
         	        		alert("User status changed successfully.");
         	        	} 
         	        	if(result=="fail"){
         	        		alert("Fail to change User status. Please try again ");
         	        	} 

         	        	location.reload();
         	        },
         	        error: function (result) {
         	        	location.reload();
         	        }
         	    });
         }
      </script>
   </body>
</html>