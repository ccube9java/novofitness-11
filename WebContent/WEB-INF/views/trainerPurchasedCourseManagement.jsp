<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>NovoFitness Trainer Purchased Courses</title>
      <%@ include file="/WEB-INF/handlingJsps/trainerTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/trainerSession.jsp"%>
   </head>
   <body>
      <!-- Left Panel Sidebar-->
      <%@ include file="/WEB-INF/handlingJsps/trainerSidebar.jsp"%>
      <div id="right-panel" class="right-panel">
         <!-- Right Panel Header-->
         <%@ include file="/WEB-INF/handlingJsps/trainerHeader.jsp"%>
         <div class="breadcrumbs">
            <div class="col-sm-4">
               <div class="page-header float-left">
                  <div class="page-title">
                     <h1>My Purchased Course Management</h1>
                  </div>
               </div>
            </div>
            <div class="col-sm-4"></div>
         </div>
            <div class="content mt-3">
            <div class="animated fadeIn">
               <div class="row">
                  <div class="col-md-12">
                     <div class="card">
                        <c:choose>
                           <c:when test="${ trainerSession.trainerIsApprovedByAdmin }">
                              <div class="card-header">
                                 <strong class="card-title">List Of Purchased Courses</strong>
                                 <c:if test="${ not empty alertSuccessMessage }">
                                    <div class="alert alert-success">${ alertSuccessMessage }</div>
                                 </c:if>
                                 <c:if test="${ not empty alertSuccessMessageForAddingNewCourse }">
                                    <div class="alert alert-success">${ alertSuccessMessageForAddingNewCourse }</div>
                                 </c:if>
                                 <c:if test="${ not empty alertFailMessage }">
                                    <div class="alert alert-danger">${ alertFailMessage }</div>
                                 </c:if>
                                 <c:if test="${ not empty alertFailMessageForAddingNewCourseOnlyCoverImageFailure }">
                                    <div class="alert alert-danger">
                                       ${ alertFailMessageForAddingNewCourseOnlyCoverImageFailure }
                                    </div>
                                 </c:if>
                                <!--  <div class="right-logo">
                                    <a href=""><img
                                       src="assets/adminAssets/images/pdf-512.png" class="src-img"></a>
                                    <a href=""><img class="src-img-2"
                                       src="assets/adminAssets/images/icons8-microsoft-excel-48.png"></a>
                                 </div> -->
                              </div>
                              <div class="card-body" id="userlist">
                                 <table id="trainerpanels"
                                    class="table table-striped table-bordered">
                                     <thead>
                                 <tr>
                                 <th>
                                             <fmt:message key="label.srNo" />
                                          </th>
                                    <th>Course Name</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email id</th>
                                    <th>Payment Status</th>
                                    <th>View Detail</th>
                                 </tr>
                              </thead>
                              <tbody>
                              	<c:choose>
                              		<c:when test="${ not empty trainerCourseUserPaymentList }">
                              			<c:forEach items="${ trainerCourseUserPaymentList }" var="trainerCourseUserPayment" varStatus="loop">
                              					<tr>
                              					 <td>${ loop.index+1 }</td>
                              					 <td>${ trainerCourseUserPayment.trainerCourseInfo.trainerCourseInfo.courseName }</td>
                              					<td>${ trainerCourseUserPayment.userInfo.userFName }</td>
                                    <td>${ trainerCourseUserPayment.userInfo.userLName } </td>
                                    <td>${ trainerCourseUserPayment.userInfo.userEmail }</td>
                                    <td>Completed</td>
                                    <td class="center-align"><a href="trainerCourseUserPaymentViewManagement/${ trainerCourseUserPayment.userCoursePaymentId }"><i class="fa fa-eye"></i></a></td>
                                 </tr>
                              			</c:forEach>
                              		</c:when>
                              	</c:choose>
                              </tbody>
                                    <%-- <thead>
                                       <tr>
                                          <th>
                                             <fmt:message key="label.srNo" />
                                             </th>
                                             <th>
                                             <fmt:message key="label.courseName" />
                                             </th>
                                             <th>
                                             Category
                                             </th>
                                             <th>
                                             Price
                                             </th>
                                             <th>
                                             <fmt:message key="label.action" />
                                             </th>
                                          <th>First Name</th>
                                          <th>Last Name</th>
                                          <th>Course Name</th>
                                          <th>Email id </th>
                                          <th>Price</th>
                                          <th>Payment Status</th>
                                          <th>Action</th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <c:choose>
                                          <c:when test="${ not empty trainerCourseList1 }">
                                             <c:forEach items="${ trainerCourseList1 }" var="trainerCourse" varStatus="loop">
                                                <tr>
                                                   <td>${ loop.index+1 }</td>
                                                   <td>${ trainerCourse.trainerCourseInfo.courseName }</td>
                                                   <td>${ trainerCourse.trainerCourseInfo.courseCategoryInfo.categoryName }</td>
                                                   <td>${ trainerCourse.trainerCourseInfo.coursePrice }</td>
                                                   <td class="center-align">
                                                      <a href="trainersCourseView/${ trainerCourse.trainerCourseId }"><i class="fa fa-eye"></i></a> 
                                                      <a href="trainersCourseView/${ trainerCourse.trainerCourseId }"><i class="fa fa-edit"></i></a>
                                                      <a href=""><i class="fa fa-trash" title="Delete" onclick="deleteTrainerCourse(${trainerCourse.trainerCourseId})" ></i></a>
                                                   </td>
                                                </tr>
                                             </c:forEach>
                                          </c:when>
                                       </c:choose>
                                    </tbody> --%>
                                 </table>
                              </div>
                           </c:when>
                           <c:when test="${ !trainerSession.trainerIsApprovedByAdmin }">
                              <div class="card-header">
                                 <strong class="card-title">Wait still Admin will approved your account.</strong>
                              </div>
                           </c:when>
                        </c:choose>
                     </div>
                  </div>
               </div>
            </div>
            <!-- .animated -->
         </div>
         <!-- .content -->
      </div>
      <!-- /#right-panel -->
      <%@ include file="/WEB-INF/handlingJsps/trainerLowerFooter.jsp"%>
      <!-- <script>
         (function($) {
         	"use strict";
         
         	jQuery('#vmap').vectorMap({
         		map : 'world_en',
         		backgroundColor : null,
         		color : '#ffffff',
         		hoverOpacity : 0.7,
         		selectedColor : '#1de9b6',
         		enableZoom : true,
         		showTooltip : true,
         		values : sample_data,
         		scaleColors : [ '#1de9b6', '#03a9f5' ],
         		normalizeFunction : 'polynomial'
         	});
         })(jQuery);
         </script> -->
      <!--  <script>
         $('.myDiv').click(function() { //button click class name is myDiv
         	e.stopPropagation();
         })
         
         $(function() {
         	$('.openDiv').click(function() {
         		$('.myDiv').show();
         
         	});
         	$(document).click(function() {
         		$('.myDiv').hide(); //hide the button
         
         	});
         });
         </script> -->
   </body>
</html>