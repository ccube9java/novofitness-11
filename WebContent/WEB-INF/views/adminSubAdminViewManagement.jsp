<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>
         NovoFitness 
         <fmt:message key="label.adminSubAdminDetails" />
      </title>
      <%@ include file="/WEB-INF/handlingJsps/adminTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/adminSession.jsp"%>
      <c:choose>
         <c:when test="${ adminSession.adminRole.roleId == 2 }">
            <c:redirect url="adminDashboard" />
         </c:when>
      </c:choose>
   </head>
   <body>
      <!-- Left Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminSidebar.jsp"%>
      <!-- Left Panel -->
      <!-- Right Panel -->
      <div id="right-panel" class="right-panel">
         <!-- Header-->
         <%@ include file="/WEB-INF/handlingJsps/adminHeader.jsp"%>
         <!-- /header -->
         <div class="breadcrumbs">
            <div class="col-sm-4">
               <div class="page-header float-left">
                  <div class="page-title">
                     <h1>
                        <fmt:message key="label.adminSubAdminDetails" />
                     </h1>
                  </div>
               </div>
            </div>
            <div class="col-sm-8">
               <div class="page-header float-right">
                  <div class="page-title">
                     <ol class="breadcrumb text-right">
                        <li><a href="adminSubAdminManagement"><b>Back</b></a></li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
         <div class="content mt-3">
            <div class="animated fadeIn">
               <div class="row">
                  <div class="col-md-12">
                     <div class="card">
                        <div class="card-header">
                           <strong>
                              <fmt:message key="label.adminSubAdminDetails" />
                           </strong>
                        </div>
                        <div class="card-body card-block">
                           <div class="col-xs-6 col-sm-6">
                              <div class="form-group">
                                 <label for="fName" class=" form-control-label">
                                    <fmt:message
                                       key="label.firstName" />
                                 </label>
                                 <div class="input-group">
                                    <input type="text" disabled class="form-control" id="first_name" name="first_name" value="${ subAdminInfo.subAdminInfo.adminFName}" />
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="lName" class=" form-control-label">
                                    <fmt:message key="label.lastName" />
                                 </label>
                                 <div class="input-group">
                                    <input type="text" disabled class="form-control" id="last_name" name="last_name" value="${ subAdminInfo.subAdminInfo.adminLName}" />
                                 </div>
                              </div>
                           </div>
                           <div class="col-xs-6 col-sm-6">
                              <div class="form-group" class=" form-control-label">
                                 <label for="email">
                                    <fmt:message key="label.email" />
                                 </label>
                                 <div class="input-group">
                                    <input type="text" disabled class="form-control" id="email_id" name="email_id" value="${ subAdminInfo.subAdminInfo.adminEmail}" />
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="mobile" class=" form-control-label">
                                    <fmt:message key="label.mobile" />
                                 </label>
                                 <div class="input-group">
                                    <input type="text" disabled class="form-control" id="mobile_number" name="mobile_number" value="${ subAdminInfo.subAdminInfo.adminPhoneNo}" />
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="card">
                           <div class="card-header"><strong>Assign Rolls</strong></div>
                           <div class="card-body card-block">
                              <div class="row">
                                 <div class="col-lg-4 col-md-4 ">
                                    <input type="checkbox" value="${ subAdminInfo.subAdminAccessForUserManagement}" >
                                    <label class="checkbox-inline">
                                    User/Learner Management
                                    </label>
                                 </div>
                                 <div class="col-lg-4 col-md-4 ">
                                    <input type="checkbox" value="${ subAdminInfo.subAdminAccessForTrainerManagement}" >
                                    <label class="checkbox-inline">
                                    Trainer Management
                                    </label>
                                 </div>
                                 <div class="col-lg-4 col-md-4 ">
                                    <input type="checkbox" value="${ subAdminInfo.subAdminAccessForContentManagement}" >
                                    <label class="checkbox-inline">
                                    Content Management
                                    </label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-4 col-md-4 ">
                                    <input type="checkbox" value="${ subAdminInfo.subAdminAccessForCategoryManagement}" >
                                    <label class="checkbox-inline">
                                    Category Management
                                    </label>
                                 </div>
                                 <div class="col-lg-4 col-md-4 ">
                                    <input type="checkbox" value="${ subAdminInfo.subAdminAccessForNewsletterManagement}" >
                                    <label class="checkbox-inline">
                                    Newsletter Management
                                    </label>
                                 </div>
                                 <div class="col-lg-4 col-md-4 ">
                                    <input type="checkbox" value="${ subAdminInfo.subAdminAccessForReviewsRatingsManagement}" >
                                    <label class="checkbox-inline">
                                    Reviews Ratings Management
                                    </label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-4 col-md-4 ">
                                    <input type="checkbox" value="${ subAdminInfo.subAdminAccessForFAQsManagement}" >
                                    <label class="checkbox-inline">
                                    FAQs Management
                                    </label>
                                 </div>
                                 <div class="col-lg-4 col-md-4 ">
                                    <input type="checkbox" value="${ subAdminInfo.subAdminAccessForSubAdminManagement}" >
                                    <label class="checkbox-inline">
                                    Sub-Admin Management
                                    </label>
                                 </div>
                                 <div class="col-lg-4 col-md-4 ">
                                    <input type="checkbox" value="${ subAdminInfo.subAdminAccessForGeneralManagement}" >
                                    <label class="checkbox-inline">
                                    General Management
                                    </label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-4 col-md-4 ">
                                    <input type="checkbox" value="${ subAdminInfo.subAdminAccessForCourseManagement}" >
                                    <label class="checkbox-inline">
                                    Courses Management
                                    </label>
                                 </div>
                                 <div class="col-lg-4 col-md-4 ">
                                    <input type="checkbox" value="${ subAdminInfo.subAdminAccessForPaymentManagement}" >
                                    <label class="checkbox-inline">
                                    Payment Management
                                    </label>
                                 </div>
                                 <div class="col-lg-4 col-md-4 ">
                                    <input type="checkbox" value="${ subAdminInfo.subAdminAccessForReportsManagement}" >
                                    <label class="checkbox-inline">
                                    Reports Management
                                    </label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- .animated -->
         </div>
         <!-- .content -->
      </div>
      <!-- Right Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminLowerFooter.jsp"%>
      <script>
         jQuery(document).ready(function() {
         	jQuery(".standardSelect").chosen({
         		disable_search_threshold : 10,
         		no_results_text : "Oops, nothing found!",
         		width : "100%"
         	});
         });
      </script>
   </body>
</html>