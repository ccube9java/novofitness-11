<!DOCTYPE html>
<html lang="zxx">
<head>
<!-- top header -->
<title>NovoFitness</title>
<%@ include file="/WEB-INF/handlingJsps/userTopHeader.jsp"%>
<!-- close top header -->
</head>
<body>
	<div class="page_loader"></div>
	<!-- Top header start -->
	<%@ include file="/WEB-INF/handlingJsps/userHeader.jsp"%>
	<!-- Top header end -->
	<!-- Sub banner images start -->
	<section class="details-banners"
		style="background: url(${ not empty trainerCourseDetils.trainerCourseInfo.courseCoverImageInfo ? trainerCourseDetils.trainerCourseInfo.courseCoverImageInfo.courseCoverImageViewPath :'assets/userAssets/imges/novofitnessbg2.jpg' })">
		<div class="menuslider-in1"></div>
	</section>
	<div class="trainers-details">
		<h3>BECOME A TRAINER</h3>
		<p>EARN MONEY. SHARE YOUR EXPERTISE. BUILD YOUR FOLLOWING.</p>
		<a href="trainerLogin"><button type="button"
				class="btn btn-default btn-trainers">Become a trainer</button></a>
	</div>
	<section class="becometrainers">
		<div class="container">
			<h2 class="title-discovers">Discover your potential</h2>
			<div class="row">
				<div class="col-sm-4 text-maincontent">
					<span class="icons-txts-images"> <i class="fa fa-money"
						aria-hidden="true"></i>
					</span>
					<h3 class="earns-subs">Earn money</h3>
					<div class="subearntexts">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt ut labore</p>
					</div>
				</div>
				<!-- col-sm-4 -->
				<div class="col-sm-4 text-maincontent">
					<span class="icons-txts-images"> <i class="fa fa-user"
						aria-hidden="true"></i>
					</span>
					<h3 class="earns-subs">Share Your Expertise</h3>
					<div class="subearntexts">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt ut labore</p>
					</div>
				</div>
				<!-- col-sm-4 -->
				<div class="col-sm-4 text-maincontent">
					<span class="icons-txts-images"> <i class="fa fa-thumbs-up"
						aria-hidden="true"></i>
					</span>
					<h3 class="earns-subs">Build Your Following</h3>
					<div class="subearntexts">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt ut labore</p>
					</div>
				</div>
				<!-- col-sm-4 -->
			</div>
			<!--row -->
		</div>
		<!-- container -->
	</section>

	<!-- courses sections -->

	<section class="shares-text">

		<div class="container">

			<h2 class="title-discovers">Expand your reach as a coach.</h2>
			<div class="educations">

				<p>We have changed lives by connecting educators with
					participants worldwide</p>

			</div>

		</div>
	</section>





	<section class="course-details-trainers">
		<div class="container">
			<h2 class="title-discovers">Envision your success</h2>
			<div class="col-sm-4 courses-parts">
				<span class="course-images"> <i class="fa fa-graduation-cap"></i>
				</span>

				<div class="course-heading">
					<span class="course-texts">1</span><span class="course-subtitles">Create
						Your Course</span>
				</div>
				<!--course-heading -->

				<div class="course-descriptions">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
						do eiusmod tempor incididunt ut labores</p>
				</div>
				<!--course-heading -->

			</div>
			<!-- col-sm-4 -->


			<div class="col-sm-4 courses-parts">
				<span class="course-images"> <i class="fa fa-video-camera"></i>
				</span>

				<div class="course-heading">
					<span class="course-texts">2</span><span class="course-subtitles">Upload
						Your Video</span>
				</div>
				<!--course-heading -->

				<div class="course-descriptions">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
						do eiusmod tempor incididunt ut labores</p>
				</div>
				<!--course-heading -->

			</div>
			<!-- col-sm-4 -->



			<div class="col-sm-4 courses-parts">
				<span class="course-images"> <i class="fa fa-comment"></i>
				</span>

				<div class="course-heading">
					<span class="course-texts">3</span><span class="course-subtitles">Share
						Your Course</span>
				</div>
				<!--course-heading -->

				<div class="course-descriptions">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
						do eiusmod tempor incididunt ut labores</p>
				</div>
				<!--course-heading -->

			</div>
			<!-- col-sm-4 -->

		</div>
		<!-- container -->

	</section>

	<section class="become-instructors">
		<div class="container">
			<h2 class="title-discovers">Become a trainer today</h2>
			<p class="trainer-sub-txts">EARN MONEY. SHARE YOUR EXPERTISE.
				BUILD YOUR FOLLOWING.</p>

			<div class="become-instructors-btns">
				<a href="trainerRegister">
					<button type="button" class="btn btn-default btn-trainers-lower">Become
						a trainer</button>
				</a>

			</div>
		</div>
		<!-- container  -->

	</section>


	<!---sports elearning section end here--->
	<!-- Footer start -->
	<%@ include file="/WEB-INF/handlingJsps/userFooter.jsp"%>
	<!-- Footer end -->
	<%@ include file="/WEB-INF/handlingJsps/userLowerFooter.jsp"%>
	<!-- Custom javascript -->
</body>
</html>