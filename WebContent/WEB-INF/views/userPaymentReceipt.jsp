<%@page import="com.cube9.novafitness.config.UserDefinedKeyWords"%>
<!DOCTYPE html>
<html lang="zxx">
   <head>
      <!-- top header -->
      <title>NovoFitness</title>
      <%@ include file="/WEB-INF/handlingJsps/userTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/userSession.jsp"%>
      <c:choose>
         <c:when test="${empty userOderSummaryList}">
            <c:redirect url="userShopingCartList" />
         </c:when>
      </c:choose>
      <link rel="stylesheet" type="text/css"
         href="assets/userAssets/css/userPayment.css">
      <!-- close top header -->
   </head>
   <body>
      <div class="page_loader"></div>
      <!-- Top header start -->
      <%@ include file="/WEB-INF/handlingJsps/userHeader.jsp"%>
      <!-- Top header end -->
      <!-- Sub banner start -->
      <div class="category-heading">
         <div class="container-fluid">
            <div class="categort-title">
               <h1>Payment Receipt</h1>
            </div>
         </div>
      </div>
      <section class="login_payment">
         <div class="invoice">
            <div class="container">
               <div class="row">
                  <div class="col-xs-12">
                     <div class="toolbar hidden-print">
                        <div class="text-right">
                           <button id="printInvoice" class="btn btn-info"><i class="fa fa-print"></i> Print</button>
                           <button id="exportInvoice" class="btn btn-info"><i class="fa fa-file-pdf-o"></i> Export as PDF</button>
                        </div>
                        <hr>
                     </div>
                     <div class="row">
                        <div class="col-xs-6">
                           <h2>Invoice</h2>
                        </div>
                        <div class="col-xs-6">
                           <h3 class="orders-txts">Order # ${ userOrderDetails.userOrderNumber }</h3>
                        </div>
                     </div>
                     <hr>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-12">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h3 class="panel-title"><strong>Order summary</strong></h3>
                        </div>
                        <div class="panel-body">
                           <div class="table-responsive">
                              <table class="table table-condensed">
                                 <thead>
                                    <tr>
                                       <td><strong>Item</strong></td>
                                       <td class="text-center"><strong>Price</strong></td>
                                       <td class="text-center"><strong>Quantity</strong></td>
                                       <td class="text-center"></td>
                                       <td class="text-right"><strong>Total</strong></td>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <c:choose>
                                       <c:when test="${ not empty userOderSummaryList }">
                                          <c:forEach items="${ userOderSummaryList }" var="userOderSummary">
                                             <tr>
                                                <td>${ userOderSummary.trainerCourseInfo.trainerCourseInfo.courseName }</td>
                                                <td class="text-center">$ ${ userOderSummary.trainerCourseInfo.trainerCourseInfo.coursePrice }</td>
                                                <td class="text-center">1</td>
                                                <td class="text-center"></td>
                                                <td class="text-right">$ ${ userOderSummary.trainerCourseInfo.trainerCourseInfo.coursePrice }</td>
                                             </tr>
                                          </c:forEach>
                                       </c:when>
                                    </c:choose>
                                    <c:choose>
                                       <c:when test="${ not empty userOrderDetails.couponCodeInfo }">
                                          <tr>
                                             <td class="thick-line"></td>
                                             <td class="thick-line"></td>
                                             <td class="thick-line"></td>
                                             <td class="thick-line text-center"><strong>Sub Total </strong></td>
                                             <td class="thick-line text-right"> $ ${ userOrderTotalAmount.details.subtotal }</td>
                                          </tr>
                                          <tr>
                                             <td class="no-line"></td>
                                             <td class="no-line"></td>
                                             <td class="no-line"></td>
                                             <td class="no-line text-center"><strong>Discount</strong></td>
                                             <td class="no-line text-right">- $ ${ userOrderDetails.couponCodeInfo.couponCodeAmount }</td>
                                          </tr>
                                       </c:when>
                                    </c:choose>
                                    <tr>
                                       <td class="thick-line"></td>
                                       <td class="thick-line"></td>
                                       <td class="thick-line"></td>
                                       <td class="thick-line text-center"><strong>Total</strong></td>
                                       <td class="thick-line text-right">$ ${ userOrderTotalAmount.total }</td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!---sports elearning section end here--->
      <!-- Footer start -->
      <%@ include file="/WEB-INF/handlingJsps/userFooter.jsp"%>
      <!-- Footer end -->
      <%@ include file="/WEB-INF/handlingJsps/userLowerFooter.jsp"%>
      <!-- Custom javascript -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js"></script>
      <script type="text/javascript">
         $('#printInvoice').click(function(){
             Popup($('.invoice')[0].outerHTML);
             function Popup(data) 
             {
                 window.print();
                 return true;
             }
         });
      </script>
      <script type="text/javascript">
         var specialElementHandlers = {
                '#editor': function (element,renderer) {
                    return true;
                }
            };
         $('#exportInvoice').click(function () {
             var doc = new jsPDF();
             doc.fromHTML(
                 $('.invoice').html(), 15, 15, 
                 { 'width': 170, 'elementHandlers': specialElementHandlers }, 
                 function(){ doc.save('sample-file.pdf'); }
             );
         
         });  
      </script>
   </body>
</html>