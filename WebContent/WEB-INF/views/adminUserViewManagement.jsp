<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>
         NovoFitness 
         <fmt:message key="label.adminUserDetails" />
      </title>
      <%@ include file="/WEB-INF/handlingJsps/adminTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/adminSession.jsp"%>
      <c:choose>
         <c:when test="${ adminSession.adminRole.roleId == 2 }">
            <c:redirect url="adminDashboard" />
         </c:when>
      </c:choose>
   </head>
   <body>
      <!-- Left Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminSidebar.jsp"%>
      <!-- Left Panel -->
      <!-- Right Panel -->
      <div id="right-panel" class="right-panel">
         <!-- Header-->
         <%@ include file="/WEB-INF/handlingJsps/adminHeader.jsp"%>
         <!-- /header -->
         <div class="breadcrumbs">
            <div class="col-sm-4">
               <div class="page-header float-left">
                  <div class="page-title">
                     <h1>
                        <fmt:message key="label.adminUserDetails" />
                     </h1>
                  </div>
               </div>
            </div>
            <div class="col-sm-8">
               <div class="page-header float-right">
                  <div class="page-title">
                     <ol class="breadcrumb text-right">
                        <li><a href="adminUserManagement"><b>Back</b></a></li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
         <div class="content mt-3">
            <div class="animated fadeIn">
               <div class="row">
                  <div class="col-md-12">
                     <div class="card">
                        <%-- <div class="card-header">
                           <strong>
                              <fmt:message key="label.adminUserDetails" />
                           </strong>
                           </div> --%>
                        <div class="card-body card-block">
                           <div class="col-xs-6 col-sm-6">
                              <div class="form-group">
                                 <label for="fName" class=" form-control-label">
                                    <fmt:message
                                       key="label.firstName" />
                                 </label>
                                 <div class="input-group">
                                    <input type="text" disabled class="form-control"
                                       id="first_name" name="first_name"
                                       value="${ userInfo.userFName}" />
                                 </div>
                              </div>
                              <div class="form-group" class=" form-control-label">
                                 <label for="email">
                                    <fmt:message key="label.email" />
                                 </label>
                                 <div class="input-group">
                                    <input type="text" disabled class="form-control" id="email_id" name="email_id" value="${ userInfo.userEmail}" />
                                 </div>
                              </div>
                           </div>
                           <div class="col-xs-6 col-sm-6">
                              <div class="form-group">
                                 <label for="lName" class=" form-control-label">
                                    <fmt:message key="label.lastName" />
                                 </label>
                                 <div class="input-group">
                                    <input type="text" disabled class="form-control" id="last_name" name="last_name" value="${ userInfo.userLName}" />
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="mobile" class=" form-control-label">
                                    <fmt:message key="label.mobile" />
                                 </label>
                                 <div class="input-group">
                                    <input type="text" disabled class="form-control" id="mobile_number" name="mobile_number" value="${ userInfo.userPhoneNo}" />
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- .animated -->
         </div>
         <!-- .content -->
      </div>
      <!-- Right Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminLowerFooter.jsp"%>
      <script>
         jQuery(document).ready(function() {
         	jQuery(".standardSelect").chosen({
         		disable_search_threshold : 10,
         		no_results_text : "Oops, nothing found!",
         		width : "100%"
         	});
         });
      </script>
   </body>
</html>