<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<html lang="zxx">
   <head>
      <!-- top header -->
      <title>
         NovoFitness 
         <fmt:message key="label.register" />
      </title>
      <%@ include file="/WEB-INF/handlingJsps/userTopHeader.jsp"%>
      <!-- close top header -->
      <style>
         .fa {
         padding: 10px 32px;
         font-size: 30px;
         text-align: center;
         text-decoration: none;
         margin: 5px 2px;
         }
         .fa-facebook {
         background: #3B5998;
         color: white;
         }
         .fa-twitter {
         background: #55ACEE;
         color: white;
         }
         .fa-instagram {
         background: #125688;
         color: white;
         }
         .form-content-box .details {
         padding: 30px 30px;
         border-radius: 5px 5px 0 0;
         background: #f3f3fb;
         }
         .socila-button button{
         display:inline;
         }
         p{
         margin: 19px 0px 5px 0px;
         }
         .form-content-box .footer {
         font-size: 13px;
         padding: 27px;
         border-radius: 0 0 5px 5px;
         }
         .form-content-box form{
         height:auto;
         }
         .footer span a{
         color: #535353;
         }
         .btn-theme{
         background-color: #f59e01;
         }
         .main-footer .footer-icon li{
         padding: 0px 0px!important;
         }
         ul li.active a {
         background: #f89e24!important;
         color: white;
         }
         .nav-tabs{
         text-align: center!important;
         } 
         .nav-tabs>li {
         display: inline-block!important;
         margin-top: 30px!important;
         float:none;
         border-radius: 0;
         }
         .nav-tabs>li a{
         color:#ffffff;
         border-radius: 0!important;
         }
         .nav-tabs>li>a:hover {
         background: transparent;
         border:none;
         border-radius: none!important;
         }
         .nav-tabs > li.active > a:hover {
         border-radius: 0px;
         background-color: transparent;
         }
      </style>
   </head>
   <body>
      <!-- Top header start -->
      <%@ include file="/WEB-INF/handlingJsps/userHeader.jsp"%>
      <!-- Top header end -->
      <div class="main-page">
         <div class="page_loader"></div>
         <!-- Content bg area start -->
         <div class="container">
            <div class="row">
               <!--    <div class="col-lg-4 col-md-4">
                  </div>
                   <div class="col-lg-4 col-md-4">
                     <ul id="registerTab" class="nav nav-tabs">
                        <li class="active"><a href="#userregister" data-toggle="tab">User Register</a></li>
                        <li class=""><a href="#trainerregister" data-toggle="tab">Trainer Register</a></li>
                      </ul>
                    </div>
                    
                     <div class="col-lg-4 col-md-4">
                     </div> -->
               <div id="myTabContent" class="tab-content">
                  <div class="tab-pane fade active in" id="userregister">
                     <div class="col-lg-12">
                        <!-- Form content box start -->
                        <div class="form-content-box">
                           <!-- logo -->
                           <!-- details -->
                           <div class="details">
                              <a href="userHome" class="clearfix alpha-logo">
                              <img src="assets/userAssets/imges/bglogo.png" alt="white-logo">
                              </a>
                              <h3>Create an user account</h3>
                              <c:if test="${ not empty alertSuccessMessage }">
                                 <div class="alert alert-success">
                                    <fmt:message key="label.successFullRegister" />
                                 </div>
                              </c:if>
                              <c:if test="${ not empty alertFailMessage }">
                                 <div class="alert alert-danger">
                                    <fmt:message key="label.userEmailAlreadyPresent" />
                                 </div>
                              </c:if>
                              <!-- Form start-->
                              <form:form action="registerUser" method="post" modelAttribute="userRegisterWrapper">
                                 <div class="form-group">
                                 	<label>
                                 <fmt:message key="label.firstName" />
                                 <span class="error" style="float: right; margin-top: 0px;">*</span>
                              </label>
                                    <fmt:message key="label.enterFirstName" var="enterFirstName"/>
                                    <form:input path="userFName" class="input-text" placeholder="${ enterFirstName }" />
                                    <form:errors path="userFName" class="error" />
                                 </div>
                                 <div class="form-group">
                                 	<label>
                                 <fmt:message key="label.lastName" />
                                 <span class="error" style="float: right; margin-top: 0px;">*</span>
                              </label>
                                    <fmt:message key="label.enterLastName" var="enterLastName"/>
                                    <form:input path="userLName" class="input-text" placeholder="${ enterLastName }" />
                                    <form:errors path="userLName" class="error" />
                                 </div>
                                 <div class="form-group">
                                 <label>
                                 <fmt:message key="label.phone" />
                                 <span class="error" style="float: right; margin-top: 0px;">*</span>
                              </label>
                                    <fmt:message key="label.enterMobile" var="enterMobile"/>
                                    <form:input path="userPhoneNumber" class="input-text" placeholder="${ enterMobile }" />
                                    <form:errors path="userPhoneNumber" class="error" />
                                 </div>
                                 <div class="form-group">
                                 	<label>
                                 <fmt:message key="label.email" />
                                 <span class="error" style="float: right; margin-top: 0px;">*</span>
                              </label>
                                    <fmt:message key="label.enterEmail" var="enterEmail"/>
                                    <form:input path="userEmail"  class="input-text" placeholder="${ enterEmail }" />
                                    <form:errors path="userEmail" class="error" />
                                 </div>
                                 <div class="form-group">
                                  <label>
                                 <fmt:message key="label.password" />
                                 <span class="error" style="float: right; margin-top: 0px;">*</span>
                              </label>
                                    <fmt:message key="label.enterPassword" var="enterPassword"/>
                                    <form:password path="userPassword"  class="input-text" placeholder="${ enterPassword }" />
                                    <form:errors path="userPassword" class="error" />
                                 </div>
                                 <div class="form-group">
                                 <label>
                                 <fmt:message key="label.confirmPassword" />
                                 <span class="error" style="float: right; margin-top: 0px;">*</span>
                              </label>
                                    <fmt:message key="label.enterConfirmPassword" var="enterConfirmPassword"/>
                                    <form:password path="userConfirmPassword" class="input-text" placeholder="${ enterConfirmPassword }" />
                                    <form:errors path="userConfirmPassword" class="error" />
                                 </div>
                                 <div class="form-group frmelements-2">
                                    <label class="checkbox-inline">
                                       <form:checkbox path="userAcceptNewsletterSubscription" />
                                       Do you agree to receive our news, special offers by e-mailing
                                       <br>
                                       <form:errors path="userAcceptNewsletterSubscription" class="error" />
                                    </label>
                                 </div>
                                 <div class="form-group frmelements-2">
                                    <label class="checkbox-inline">
                                       <form:checkbox path="userAcceptTermsCondition" />
                                       <fmt:message key="label.acceptTermsCondition" /><span class="error" style="float: right; margin-top: 0px;">*</span>
                                       <br>
                                       <form:errors path="userAcceptTermsCondition" class="error" />
                                    </label>
                                 </div>
                                 <div class="mb-0">
                                    <button type="submit" class="btn-md btn-theme btn-block">
                                       <fmt:message key="label.register" />
                                    </button>
                                 </div>
                              </form:form>
                              <!-- Form end-->
                              <div class="socila-button">
                                 <p class="social-txt">
                                    Social media for sign up
                                 </p>
                                 <div class="social-login">
                                    <a href="#" class="fa fa-facebook"></a>
                                    <a href="#" class="fa fa-twitter"></a>
                                    <a href="#" class="fa fa-instagram"></a>
                                 </div>
                              </div>
                              <div class="footer">
                                 <span>
                                    Already a member? 
                                    <a href="userLogin">
                                       <fmt:message key="label.login" />
                                       here
                                    </a>
                                 </span>
                              </div>
                           </div>
                           <!-- Footer -->
                        </div>
                        <!-- Form content box end -->
                     </div>
                  </div>
                  <!-- end of the user Registration tab here-- -->
                  <%--   <div class="tab-pane fade" id="trainerregister">
                     <div class="col-lg-12">
                     <!-- Form content box start -->
                     <div class="form-content-box">
                     <!-- logo -->
                     <!-- details -->
                     <div class="details">
                        <a href="userHome" class="clearfix alpha-logo">
                        <img src="assets/userAssets/imges/bglogo.png" alt="white-logo">
                        </a>
                        <h3>Create an trainer account</h3>
                        <c:if test="${ not empty alertSuccessMessage }">
                           <div class="alert alert-success">
                              <fmt:message key="label.successFullRegister" />
                           </div>
                        </c:if>
                        <c:if test="${ not empty alertFailMessage }">
                           <div class="alert alert-danger">
                              <fmt:message key="label.userEmailAlreadyPresent" />
                           </div>
                        </c:if>
                     
                      <form:form action="registerTrainer" method="post" modelAttribute="trainerRegisterWrapper" enctype="multipart/form-data">
                        <div class="row">
                           <div class="col-md-12 col-lg-12">
                              <div class="form-group">
                                 <label>
                                    <fmt:message key="label.firstName" />
                                 </label>
                                 <fmt:message key="label.enterFirstName" var="enterFirstName"/>
                                 <form:input path="trainerFName" class="form-control" placeholder="${ enterFirstName }" />
                                 <form:errors path="trainerFName" class="error" />
                              </div>
                          
                              <div class="form-group">
                                 <label>
                                    <fmt:message key="label.lastName" />
                                 </label>
                                 <fmt:message key="label.enterLastName" var="enterLastName"/>
                                 <form:input path="trainerLName" class="form-control" placeholder="${ enterLastName }" />
                                 <form:errors path="trainerLName" class="error" />
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-12 col-lg-12">
                              <div class="form-group">
                                 <label>
                                    <fmt:message key="label.email" />
                                 </label>
                                 <fmt:message key="label.enterEmail" var="enterEmail"/>
                                 <form:input path="trainerEmail" class="form-control" placeholder="${ enterEmail }" />
                                 <form:errors path="trainerEmail" class="error" />
                              </div>
                           </div>
                        </div>
                        <!---end of the row here--->
                        <div class="row">
                           <div class="col-md-12 col-lg-12">
                              <div class="form-group">
                                 <label>
                                    <fmt:message key="label.password" />
                                 </label>
                                
                                 <fmt:message key="label.enterPassword" var="enterPassword"/>
                                 <form:password path="trainerPassword" class="form-control" placeholder="${ enterPassword }" />
                                 <form:errors path="trainerPassword" class="error" />
                              </div>
                           
                              <div class="form-group">
                                 <label>
                                    <fmt:message key="label.confirmPassword" />
                                 </label>
                                 <fmt:message key="label.enterConfirmPassword" var="enterConfirmPassword"/>
                                 <form:password path="trainerConfirmPassword" class="form-control" placeholder="${ enterConfirmPassword }" />
                                 <form:errors path="trainerConfirmPassword" class="error" />
                              </div>
                           </div>
                        </div>
                        <!--end of the row here-->
                        <div class="row">
                           <div class="col-md-12 col-lg-12">
                              <div class="form-group">
                                <label>
                                    <fmt:message key="label.idCopy" />
                                 </label>
                                  <form:input type="file" path="trainerIDCopyInfo" class="form-control file" data-browse-on-zone-click="true" />
                                  <form:errors path="trainerIDCopyInfo" class="error" />
                                 <!-- <input id="input-b2" name="input-b1" type="file" class="file" data-browse-on-zone-click="true"> -->
                              </div>
                           
                              <div class="form-group">
                                <label>
                                    <fmt:message key="label.certificates" />
                                 </label>
                                  <form:input type="file" path="trainerCertificateInfo" class="form-control file" data-browse-on-zone-click="true" />
                                  <form:errors path="trainerCertificateInfo" class="error" />
                                 <!-- <input id="input-b1" name="input-b1" type="file" class="file" data-browse-on-zone-click="true"> -->
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-9">
                              <div class="checkbox">
                                 <!-- <input type="checkbox"> -->
                                   <form:checkbox path="trainerAcceptTermsCondition" />  
                                 <fmt:message key="label.acceptTermsCondition" /><br>
                                 <form:errors path="trainerAcceptTermsCondition" class="error" />
                              </div>
                           </div>
                          <!-- ---- <div class="col-md-3">
                              <div class="form-group">
                                 <div class="pwd-btn">
                                    <a href="trainerLogin" id="forgotBtn" class="forget_pwd_new">
                                       <fmt:message key="label.login" />
                                       
                                    </a>
                                 </div>
                              </div>
                           </div>----->
                        </div>
                        <button type="submit" class="btn-md btn-theme btn-block">
                           <fmt:message key="label.register" />
                           <!-- Trainer -->
                        </button>
                     </form:form>
                     
                     <div class="socila-button">
                           <p class="social-txt">
                              Social media for sign up
                           </p>
                           <div class="social-login">
                              <a href="#" class="fa fa-facebook"></a>
                              <a href="#" class="fa fa-twitter"></a>
                              <a href="#" class="fa fa-instagram"></a>
                           </div>
                        </div>
                        <div class="footer">
                           <span>
                              Already a member? 
                              <a href="userLogin">
                                 <fmt:message key="label.login" />
                                 here
                              </a>
                           </span>
                        </div>
                     </div>
                     <!-- Footer -->
                     </div>
                     <!-- Form content box end -->
                     </div>
                     
                     
                     </div> --%>
                  <!-- end of the Trainer Registration tab here-- -->
               </div>
               <!-- end of the main  tab chnage here here-- -->
            </div>
         </div>
      </div>
      <!-- Footer start -->
      <%@ include file="/WEB-INF/handlingJsps/userFooter.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/userLowerFooter.jsp"%>
      <!-- Footer end -->
   </body>
</html>