<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>NovoFitness <fmt:message key="label.adminFAQsManagement" /></title>
      <%@ include file="/WEB-INF/handlingJsps/adminTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/adminSession.jsp"%>
      <c:choose>
         <c:when test="${ adminSession.adminRole.roleId == 2 }">
            <c:redirect url="adminDashboard" />
         </c:when>
      </c:choose>
   </head>
   <body>
      <!-- Left Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminSidebar.jsp"%>
      <!-- Left Panel -->
      <!-- Right Panel -->
      <div id="right-panel" class="right-panel">
         <!-- Header-->
         <%@ include file="/WEB-INF/handlingJsps/adminHeader.jsp"%>
         <!-- /header -->
         <div class="breadcrumbs">
            <div class="col-sm-4">
               <div class="page-header float-left">
                  <div class="page-title">
                      <h1>Promotional Offer</h1>
                  </div>
               </div>
            </div>
            <div class="col-sm-4"></div>
         </div>
         <div class="content mt-3">
            <div class="animated fadeIn">
               <div class="row">
                  <div class="col-md-12">
                     <div class="card">
                        <div class="card-header">
                           <strong class="card-title">Promotional Offer List</strong>

                           <div class="right-logo">
                           		 <a href="addpramotional.html"><button type="submit" class="btn btn-success btn-sm">
                                    Add Promotion
                                      </button></a>
                              <a href=""><img src="assets/adminAssets/images/pdf-512.png" class="src-img"></a> 
                              <a href=""><img class="src-img-2" src="assets/adminAssets/images/icons8-microsoft-excel-48.png"></a>
                           </div>
                        </div>
                        <div class="card-body" id="userlist">
                           <table id="bootstrap-data-table-export"
                              class="table table-striped table-bordered">
                              <thead>
                                  <tr>
                                 <th>
                                    <fmt:message key="label.srNo" />
                                 </th>
                                 <th><fmt:message key="label.question" /></th>
                                 <th><fmt:message key="label.answer" /></th>
                                 <th>
                                    <fmt:message key="label.status" />
                                 </th>
                                 <th>
                                    <fmt:message key="label.action" />
                                 </th>
                              </tr>
                              </thead>
                              <tbody>
                                 <c:choose>
                                    <c:when test="${ not empty faqList }">
                                       <c:forEach items="${ faqList }" var="faq" varStatus="loop">
                                          <tr>
                                             <td>${ loop.index+1 }</td>
                                             <td>${ user.faqQuestion }</td>
                                             <td>${ user.faqAnswer }</td>
                                             <td><label class="switch"> 
                                                <input type="checkbox" checked><span class="slider round"></span></label>
                                             </td>
                                             <td class="center-align">
                                                <a href="User-view.html"><i class="fa fa-eye"></i></a> 
                                                <a href=""><i class="fa fa-trash"></i></a>
                                             </td>
                                          </tr>
                                       </c:forEach>
                                    </c:when>
                                 </c:choose>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- .animated -->
         </div>
         <!-- .content -->
      </div>
      <!-- /#right-panel -->
      <!-- Right Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminLowerFooter.jsp"%>
      <script>
         $(document).ready(function() {
         	$('#datepicker2').datepicker({
         		uiLibrary : 'bootstrap'
         	});
         
         	$('#fa-calendar2').click(function() {
         		$("#datepicker2").focus();
         	});
         });
      </script>
      <script>
         $(document).ready(function() {
         	$('#datepicker3').datepicker({
         		uiLibrary : 'bootstrap'
         	});
         
         	$('#fa-calendar3').click(function() {
         		$("#datepicker3").focus();
         	});
         });
      </script>
   </body>
</html>