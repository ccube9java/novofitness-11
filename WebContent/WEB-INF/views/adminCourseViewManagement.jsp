<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>NovoFitness Course Details</title>
      <%@ include file="/WEB-INF/handlingJsps/adminTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/adminSession.jsp"%>
      <c:choose>
         <c:when test="${ adminSession.adminRole.roleId == 2 }">
            <c:redirect url="adminDashboard" />
         </c:when>
      </c:choose>
   </head>
   <body>
      <!-- Left Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminSidebar.jsp"%>
      <!-- Left Panel -->
      <!-- Right Panel -->
      <div id="right-panel" class="right-panel">
         <!-- Header-->
         <%@ include file="/WEB-INF/handlingJsps/adminHeader.jsp"%>
         <!-- /header -->
         <div class="breadcrumbs">
            <div class="col-sm-4">
               <div class="page-header float-left">
                  <div class="page-title">
                     <h1>Course Details</h1>
                  </div>
               </div>
            </div>
            <div class="col-sm-8">
               <div class="page-header float-right">
                  <div class="page-title">
                     <ol class="breadcrumb text-right">
                        <li><a href="adminCourseManagement"><b>Back</b></a></li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
         <div class="content mt-3" id="Course-list-view">
            <div class="animated fadeIn">
               <div class="row">
                  <div class="col-xs-12 col-sm-12">
                     <div class="card">
                        <div class="card-body card-block">
                           <div class="col-xs-6 col-sm-6">
                              <div class="form-group">
                                 <label for="courseName" class=" form-control-label">
                                    <fmt:message
                                       key="label.courseName" />
                                 </label>
                                 <div class="input-group">
                                    <input type="text" disabled class="form-control"
                                       id="first_name" name="first_name"
                                       value="${ trainerCourseInfo.trainerCourseInfo.courseName}" />
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="form-control-label">Description</label>
                                 <div class="input-group">
                                    <!-- <input class="form-control"  value="Description" type="text"> -->
                                    <textarea id="courseDescription" name="courseDescription" disabled="disabled" class="form-control" rows="2">${ trainerCourseInfo.trainerCourseInfo.courseDescription }</textarea>
                                 </div>
                              </div>
                              <div class="form-group"><label for="company" class=" form-control-label"> Price</label>
                                 <input type="text" id="company" disabled="disabled" value="${ trainerCourseInfo.trainerCourseInfo.coursePrice}" class="form-control">
                              </div>
                              <div class="form-group">
                                 <label for="vat" class=" form-control-label">Cover Image</label><br>
                                 <c:choose>
                                    <c:when test="${ not empty trainerCourseInfo.trainerCourseInfo.courseCoverImageInfo }">
                                       <img class="thumnail-img-2 img-thumnail" src="${ trainerCourseInfo.trainerCourseInfo.courseCoverImageInfo.courseCoverImageViewPath }">
                                    </c:when>
                                    <c:otherwise>
                                       No Cover Image Available
                                    </c:otherwise>
                                 </c:choose>
                              </div>
                              <div class="form-group">
                                 <label for="vat" class=" form-control-label">Other Images</label><br>
                                 <c:choose>
                                    <c:when test="${ not empty courseOtherImagesList }">
                                       <c:forEach items="${ courseOtherImagesList }" var="courseOtherImage">
                                          <img class="thumnail-img-2 img-thumnail" src="${ courseOtherImage.courseImagesInfo.courseImagesViewPath }">
                                       </c:forEach>
                                    </c:when>
                                    <c:otherwise>
                                       No Other Images Available
                                    </c:otherwise>
                                 </c:choose>
                              </div>
                              <div class="form-group">
                                 <label for="company" class=" form-control-label">Videos</label><br>
                                 <c:choose>
                                    <c:when test="${ not empty courseOtherVideosList }">
                                       <c:forEach items="${ courseOtherVideosList }" var="courseOtherVideo">
                                          <video controls>
                                             <source src="${ courseOtherVideo.courseVideoInfo.courseVideoViewPath }" type="video/mp4">
                                             <source src="${ courseOtherVideo.courseVideoInfo.courseVideoViewPath }" type="video/ogg">
                                          </video>
                                       </c:forEach>
                                    </c:when>
                                    <c:otherwise>
                                       No Other Videos Available
                                    </c:otherwise>
                                 </c:choose>
                              </div>
                              <br>
                              <div class="form-group">
                                 <label for="vat" class=" form-control-label"> Documents</label><br>
                                 <c:choose>
                                    <c:when test="${ not empty courseOtherPDFsList }">
                                       <c:forEach items="${ courseOtherPDFsList }" var="courseOtherPDF">
                                          <div class="Certificates-img">
                                             <a href="${ courseOtherPDF.coursePDFInfo.coursePDFViewPath }" title="${ courseOtherPDF.coursePDFInfo.coursePDFName }" target="_blank"> 
                                             <img src="assets/adminAssets/images/certificte-img.png" class="img-responsive">
                                             </a>
                                          </div>
                                       </c:forEach>
                                    </c:when>
                                    <c:otherwise>
                                       No Documents Available.
                                    </c:otherwise>
                                 </c:choose>
                              </div>
                           </div>
                           <div class="col-xs-6 col-sm-6">
                              <div class="form-group">
                                 <label class=" form-control-label">Category</label>
                                 <div class="input-group">
                                    <input type="text" disabled class="form-control"
                                       id="email_id" name="email_id"
                                       value=${ trainerCourseInfo.trainerCourseInfo.courseCategoryInfo.categoryName} />
                                    <!-- <input class="form-control" value="Category #1" type="text"> -->
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="vat" class=" form-control-label">Course Content </label>
                                 <!-- <input type="text" id="vat" value="Content" class="form-control"> -->
                                 <textarea id="courseContent" name="courseContent" disabled="disabled" class="form-control" rows="2">${ trainerCourseInfo.trainerCourseInfo.courseContent }</textarea>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <%--          <div class="content mt-3" id="Course-list-view">
            <div class="animated fadeIn">
               <div class="row">
                  <div class="col-xs-12 col-sm-12">
                     <div class="card">
                        <div class="card-body card-block">
                           <div class="col-xs-6 col-sm-6">
                              <div class="form-group">
                                 <label for="courseName" class=" form-control-label">
                                    <fmt:message
                                       key="label.courseName" />
                                 </label>
                                 <div class="input-group">
                                    <input type="text" disabled class="form-control"
                                       id="first_name" name="first_name"
                                       value=${ trainerCourseInfo.trainerCourseInfo.courseName} />
                                 </div>
                              </div>
                              <div class="form-group" class=" form-control-label">
                                 <label for="email">
                                    <fmt:message key="label.category" />
                                 </label>
                                 <div class="input-group">
                                    <input type="text" disabled class="form-control"
                                       id="email_id" name="email_id"
                                       value=${ trainerCourseInfo.trainerCourseInfo.courseCategoryInfo.categoryName} />
                                 </div>
                              </div>
                           </div>
                           <div class="col-xs-6 col-sm-6">
                              <div class="form-group">
                                 <label for="lName" class=" form-control-label">
                                    <fmt:message
                                       key="label.coursePrice" />
                                 </label>
                                 <div class="input-group">
                                    <input type="text" disabled class="form-control"
                                       id="last_name" name="last_name"
                                       value=${ trainerCourseInfo.trainerCourseInfo.coursePrice} />
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="mobile" class=" form-control-label">
                                    <fmt:message key="label.mobile" />
                                 </label>
                                 <div class="input-group">
                                    <input type="text" disabled class="form-control" id="mobile_number" name="mobile_number" value=${ trainerInfo.trainerPhoneNo} />
                                 </div>
                                 </div>
                           </div>
                        </div>
                     </div>
                     <div class="card">
                        <div class="card-body card-block">
                           <c:if test="        ">
                              <div class="col-xs-6 col-sm-6">
                                 <div class="form-group">
                                    <label class=" form-control-label">ID Copy</label>
                                    <div class="Certificates-img">
                                       <a
                                          href="       "
                                          target="_blank"> <img
                                          src="assets/adminAssets/images/certificte-img.png"
                                          class="img-responsive"></a>
                                    </div>
                                 </div>
                              </div>
                           </c:if>
                           <c:if test="            ">
                              <div class="col-xs-6 col-sm-6">
                                 <div class="form-group">
                                    <label for="company" class=" form-control-label">Certificates</label>
                                    <div class="Certificates-img">
                                       <a
                                          href="            "
                                          target="_blank"> <img
                                          src="assets/adminAssets/images/certificte-img.png"
                                          class="img-responsive"></a>
                                    </div>
                                    <div></div>
                                 </div>
                              </div>
                           </c:if>
                           <c:if
                              test=" ">
                              <div class="form-group">
                                 <label for="company" class=" form-control-label">No
                                 Documents Uploaded</label>
                              </div>
                           </c:if>
                           
                           
                           
                           <div class="row">
                              <div class="col-md-12">
                                 <div class="approve-btn">
                                 	<c:choose>
                                 		<c:when test="${ trainerInfo.trainerIsApprovedByAdmin }">
                                 			<button type="button" class="btn btn-danger btn-sm"
                                       onclick="approveTrainer(${ trainerInfo.trainerId }, ${ trainerInfo.trainerIsApprovedByAdmin })">
                                    DisApprove Trainer</button>
                                		</c:when>
                                 		<c:when test="${ !trainerInfo.trainerIsApprovedByAdmin }">
                                			<button type="button" class="btn btn-success btn-sm"
                                       onclick="approveTrainer(${ trainerInfo.trainerId }, ${ trainerInfo.trainerIsApprovedByAdmin })">
                                    Approve Trainer</button>
                                		</c:when>
                                	</c:choose>
                                    
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- .animated -->
            </div> --%>
         <div class="content mt-3">
            <div class="animated fadeIn">
               <div class="row">
                  <div class="col-md-12">
                     <div class="card">
                        <div class="card-header">
                           <strong class="card-title">List Of Users</strong>
                           <div class="right-logo">
                           </div>
                        </div>
                        <div class="card-body" id="userlist">
                           <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                              <thead>
                                 <tr>
                                 <tr>
                                    <th>
                                       <fmt:message key="label.srNo" />
                                    </th>
                                    <th>
                                       <fmt:message key="label.firstName" />
                                    </th>
                                    <th>
                                       <fmt:message key="label.lastName" />
                                    </th>
                                    <th>
                                       <fmt:message key="label.email" />
                                    </th>
                                     <th>Price</th>
                                    <th>Purchased Date</th>
                                    <th>
                                       <fmt:message key="label.action" />
                                    </th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <c:choose>
                                    <c:when test="${ not empty coursePurchasedUserList }">
                                       <c:forEach items="${ coursePurchasedUserList }" var="coursePurchasedUser" varStatus="loop">
                                          <tr>
                                             <td>${ loop.index+1 }</td>
                                             <td>${ coursePurchasedUser.userInfo.userFName }</td>
                                             <td>${ coursePurchasedUser.userInfo.userLName }</td>
                                             <td>${ coursePurchasedUser.userInfo.userEmail }</td>
                                             <td>${ coursePurchasedUser.trainerCourseInfo.trainerCourseInfo.coursePrice }</td>
                                             <td>${ coursePurchasedUser.userCoursePaymentCreatedDateTime }
                                             </td>
                                             <td><a href="adminUsersView/${ coursePurchasedUser.userInfo.userId }"><i class="fa fa-eye"></i></a></td>
                                          </tr>
                                       </c:forEach>
                                    </c:when>
                                 </c:choose>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- .animated -->
         </div>
      </div>
      <!-- .content -->
     
         <!-- Right Panel -->
         <%@ include file="/WEB-INF/handlingJsps/adminLowerFooter.jsp"%>
       <%--      <script>
            jQuery(document).ready(function() {
            	jQuery(".standardSelect").chosen({
            		disable_search_threshold : 10,
            		no_results_text : "Oops, nothing found!",
            		width : "100%"
            	});
            });
         </script>
         <script type="text/javascript">
            function approveTrainer(id,status) {
            	var trainerData = { 
            			trainerId : id,
            			trainerAdminApproveStatus : status
            		}
            	 $.ajax({
            	        type: "POST",
            	        url: "changeTrainerAdminApproveStatus",
            	        data: trainerData,
            	        success: function (result) {
            	        	if(result=="success"){
            	        		alert("Trainer's Admin approve status changed successfully.");
            	        	} 
            	        	if(result=="fail"){
            	        		alert("Fail to change Trainer status. Please try again ");
            	        	} 
            	        	if(result=="true"){
            	        		alert("Trainer approved successfully.");
            	        	} 
            	        	if(result=="false"){
            	        		alert("Trainer disapproved successfully.");
            	        	} 
            
            	        	location.reload();
            	        },
            	        error: function (result) {
            	        	location.reload();
            	        }
            	    });
            } 
         </script>--%>
   </body>
</html>