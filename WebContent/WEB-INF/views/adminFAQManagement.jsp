<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>
         NovoFitness 
         <fmt:message key="label.adminFAQsManagement" />
      </title>
      <%@ include file="/WEB-INF/handlingJsps/adminTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/adminSession.jsp"%>
      <c:choose>
         <c:when test="${ adminSession.adminRole.roleId == 2 }">
            <c:redirect url="adminDashboard" />
         </c:when>
      </c:choose>
   </head>
   <body>
      <!-- Left Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminSidebar.jsp"%>
      <!-- Left Panel -->
      <!-- Right Panel -->
      <div id="right-panel" class="right-panel">
         <!-- Header-->
         <%@ include file="/WEB-INF/handlingJsps/adminHeader.jsp"%>
         <!-- /header -->
         <div class="breadcrumbs">
            <div class="col-sm-4">
               <div class="page-header float-left">
                  <div class="page-title">
                     <h1>
                        <fmt:message key="label.adminFAQsManagement" />
                     </h1>
                  </div>
               </div>
            </div>
            <div class="col-sm-4"></div>
         </div>
         <div class="content mt-3">
            <div class="animated fadeIn">
               <div class="row">
                  <div class="col-md-12">
                     <div class="card">
                        <div class="card-header">
                           <strong class="card-title">FAQ List</strong>
                           <c:if test="${ not empty alertSuccessMessage }">
                              <div class="alert alert-success">${ alertSuccessMessage }</div>
                           </c:if>
                           <c:if test="${ not empty alertSuccessMessageaddNewFAQsSuccess }">
                              <div class="alert alert-success">
                                 <fmt:message key="label.addNewFAQsSuccess" />
                              </div>
                           </c:if>
                           <c:if test="${ not empty alertFailMessage }">
                              <div class="alert alert-danger">${ alertFailMessage }</div>
                           </c:if>
                           <c:if test="${ not empty alertFailMessageaddNewFAQsFail }">
                              <div class="alert alert-danger">
                                 <fmt:message key="label.addNewFAQsFail" />
                              </div>
                           </c:if>
                           <div class="right-logo">
                              <a href="addNewFAQManagement">
                                 <button type="submit" class="btn btn-success btn-sm">
                                    <fmt:message key="label.adminAddNewFAQs" />
                                 </button>
                              </a>
                              
                           </div>
                        </div>
                        <div class="card-body" id="userlist">
                           <table id="trainers"
                              class="table table-striped table-bordered">
                              <thead>
                                 <tr>
                                    <th>
                                       <fmt:message key="label.srNo" />
                                    </th>
                                    <th>
                                       <fmt:message key="label.question" />
                                    </th>
                                    <th>
                                       <fmt:message key="label.answer" />
                                    </th>
                                    <th>
                                       <fmt:message key="label.status" />
                                    </th>
                                    <th>
                                       <fmt:message key="label.action" />
                                    </th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <c:choose>
                                    <c:when test="${ not empty faqList }">
                                       <c:forEach items="${ faqList }" var="faq" varStatus="loop">
                                          <tr>
                                             <td>${ loop.index+1 }</td>
                                             <td>${ faq.faqQuestion }</td>
                                             <td>${ faq.faqAnswer }</td>
                                             <td><label class="switch"> 
                                                <input type="checkbox" 
                                                ${ faq.faqIsActive?'checked':'' } 
                                                onchange="changeFAQsStatus(this,${ faq.faqId },${ faq.faqIsActive })" />
                                                <span class="slider round"></span></label>
                                             </td>
                                             <td class="center-align">
                                                <a href="adminFAQsViewUpdate/${ faq.faqId }"><i class="fa fa-edit"></i></a> 
                                                <a href=""><i class="fa fa-trash" title="Delete" onclick="deleteFAQs(${ faq.faqId })"></i></a>
                                             </td>
                                          </tr>
                                       </c:forEach>
                                    </c:when>
                                 </c:choose>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- .animated -->
         </div>
         <!-- .content -->
      </div>
      <!-- /#right-panel -->
      <!-- Right Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminLowerFooter.jsp"%>
      <script>
         $(document).ready(function() {
         	$('#datepicker2').datepicker({
         		uiLibrary : 'bootstrap'
         	});
         
         	$('#fa-calendar2').click(function() {
         		$("#datepicker2").focus();
         	});
         });
      </script>
      <script>
         $(document).ready(function() {
         	$('#datepicker3').datepicker({
         		uiLibrary : 'bootstrap'
         	});
         
         	$('#fa-calendar3').click(function() {
         		$("#datepicker3").focus();
         	});
         });
      </script>
      <script type="text/javascript">
         function changeFAQsStatus(checkboxElem,id,status) {
         	var faqsData = { 
         			faqsId : id,
         			faqsStatus : status
         		}
         	 $.ajax({
         	        type: "POST",
         	        url: "changeFAQsStatus",
         	        data: faqsData,
         	        success: function (result) {
         	        	if(result=="success"){
         	        		alert("FAQ status changed successfully.");
         	        	} 
         	        	if(result=="fail"){
         	        		alert("Fail to change FAQ status. Please try again ");
         	        	} 
         	        	location.reload();
         	        },
         	        error: function (result) {
         	        	location.reload();
         	        }
         	    });
         }
      </script>
      <script type="text/javascript">
         function deleteFAQs(id) {
         	var faqsData = { 
         			faqsId : id,
         	}
         	$.ajax({
                 type: "POST",
                 url: "deleteFAQs",
                 data: faqsData,
                 success: function (result) {
                	 if(result=="success"){
                    		alert("FAQ deleted successfully.");
                    	} 
                    	if(result=="fail"){
                    		alert("Fail to delete FAQ. Please try again ");
                    	} 
                 	location.reload();
                 },
                 error: function (result) {
                 	location.reload();
                 }
             });
         }
      </script>
   </body>
</html>