<%@page import="com.cube9.novafitness.config.UserDefinedKeyWords"%>
<!DOCTYPE html>
<html lang="zxx">
   <head>
      <!-- top header -->
      <title>NovoFitness</title>
      <%@ include file="/WEB-INF/handlingJsps/userTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/userSession.jsp"%>
      <!-- close top header -->
   </head>
   <body>
      <div class="page_loader"></div>
      <!-- Top header start -->
      <%@ include file="/WEB-INF/handlingJsps/userHeader.jsp"%>
      <!-- Top header end -->
      <!-- Sub banner start -->
      <div class="category-heading">
         <div class="container-fluid">
            <div class="categort-title">
               <h1>Checkout</h1>
            </div>
         </div>
      </div>
      <section class="login_payment">
         <div class="container">
            <div class="row">
               <div class="col-md-2"></div>
               <div class="col-md-8">
                  <div class="paymentbox">
                     <div class="row">
                        <div class="col-md-4">
                           <div class="payment_menu">
                              <h3>Payment Options</h3>
                              <ul class="nav debittab">
                                 <!-- <li class="active"><a data-toggle="tab" href="#home" aria-expanded="true">Debit / Credit card</a></li> 
                                    <li class=""><a data-toggle="tab" href="#menu1" aria-expanded="false">Net Banking</a></li>
                                    <li class=""><a data-toggle="tab" href="#menu4" aria-expanded="false">Google pay</a></li>
                                    -->
                                 <li class="active"><a data-toggle="tab" href="#home"
                                    aria-expanded="true">PayPal</a></li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-md-8">
                           <div class="payment_tab">
                              <div class="amt_payment">
                                 <h3>
                                    Amount to pay : <span class="amtpay">
                                    <%-- &#36; ${ userOrderDetails.userOrderTotal } --%>
                                    <c:choose>
									<c:when
										test="${ not empty myCartListTotalPriceAfterCouponCode }">
										
										&#36; ${ myCartListTotalPriceAfterCouponCode }
									</c:when>
									<c:otherwise>
										&#36; ${ myCartListTotalPrice }
									</c:otherwise>
								</c:choose>
                                    
                                    </span>
                                 </h3>
                              </div>
                              <div class="tab-content">
                                 <div id="home" class="tab-pane fade active in">
                                    <div class="creditbal">
                                       <div class="panel-body">
                                          <div class="row">
                                          <div class="col-xs-12">
                                             <form:form action="authorizeUserPayment"
                                                modelAttribute="userPaymentCheckoutWraspper">
                                                <c:choose>
									<c:when
										test="${ not empty myCartListTotalPriceAfterCouponCode }">
										 <form:hidden path="myCartListTotalPrice"
                                                   value="${ myCartListTotalPriceAfterCouponCode }" />
									</c:when>
									<c:otherwise>
									<form:hidden path="myCartListTotalPrice"
                                                   value="${ myCartListTotalPrice }" />
									</c:otherwise>
								</c:choose>
                                               
                                                <form:hidden path="couponCodeId"
									value="${ couponCodeId }" />
                                                <form:hidden path="userId"
                                                   value="${ userSession.userId }" />
                                                <c:choose>
                                                   <c:when test="${ not empty myCartCourseList }">
                                                      <div class="col-xs-12">
                                                         <button
                                                            class="subscribe btn btn-success btn-lg btn-block"
                                                            type="submit">
                                                         <img alt="PayPal"
                                                            src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwIiBoZWlnaHQ9IjMyIiB2aWV3Qm94PSIwIDAgMTAwIDMyIiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJ4TWluWU1pbiBtZWV0IiB4bWxucz0iaHR0cDomI3gyRjsmI3gyRjt3d3cudzMub3JnJiN4MkY7MjAwMCYjeDJGO3N2ZyI+PHBhdGggZmlsbD0iIzAwMzA4NyIgZD0iTSAxMi4yMzcgMi40NDQgTCA0LjQzNyAyLjQ0NCBDIDMuOTM3IDIuNDQ0IDMuNDM3IDIuODQ0IDMuMzM3IDMuMzQ0IEwgMC4yMzcgMjMuMzQ0IEMgMC4xMzcgMjMuNzQ0IDAuNDM3IDI0LjA0NCAwLjgzNyAyNC4wNDQgTCA0LjUzNyAyNC4wNDQgQyA1LjAzNyAyNC4wNDQgNS41MzcgMjMuNjQ0IDUuNjM3IDIzLjE0NCBMIDYuNDM3IDE3Ljc0NCBDIDYuNTM3IDE3LjI0NCA2LjkzNyAxNi44NDQgNy41MzcgMTYuODQ0IEwgMTAuMDM3IDE2Ljg0NCBDIDE1LjEzNyAxNi44NDQgMTguMTM3IDE0LjM0NCAxOC45MzcgOS40NDQgQyAxOS4yMzcgNy4zNDQgMTguOTM3IDUuNjQ0IDE3LjkzNyA0LjQ0NCBDIDE2LjgzNyAzLjE0NCAxNC44MzcgMi40NDQgMTIuMjM3IDIuNDQ0IFogTSAxMy4xMzcgOS43NDQgQyAxMi43MzcgMTIuNTQ0IDEwLjUzNyAxMi41NDQgOC41MzcgMTIuNTQ0IEwgNy4zMzcgMTIuNTQ0IEwgOC4xMzcgNy4zNDQgQyA4LjEzNyA3LjA0NCA4LjQzNyA2Ljg0NCA4LjczNyA2Ljg0NCBMIDkuMjM3IDYuODQ0IEMgMTAuNjM3IDYuODQ0IDExLjkzNyA2Ljg0NCAxMi42MzcgNy42NDQgQyAxMy4xMzcgOC4wNDQgMTMuMzM3IDguNzQ0IDEzLjEzNyA5Ljc0NCBaIj48L3BhdGg+PHBhdGggZmlsbD0iIzAwMzA4NyIgZD0iTSAzNS40MzcgOS42NDQgTCAzMS43MzcgOS42NDQgQyAzMS40MzcgOS42NDQgMzEuMTM3IDkuODQ0IDMxLjEzNyAxMC4xNDQgTCAzMC45MzcgMTEuMTQ0IEwgMzAuNjM3IDEwLjc0NCBDIDI5LjgzNyA5LjU0NCAyOC4wMzcgOS4xNDQgMjYuMjM3IDkuMTQ0IEMgMjIuMTM3IDkuMTQ0IDE4LjYzNyAxMi4yNDQgMTcuOTM3IDE2LjY0NCBDIDE3LjUzNyAxOC44NDQgMTguMDM3IDIwLjk0NCAxOS4zMzcgMjIuMzQ0IEMgMjAuNDM3IDIzLjY0NCAyMi4xMzcgMjQuMjQ0IDI0LjAzNyAyNC4yNDQgQyAyNy4zMzcgMjQuMjQ0IDI5LjIzNyAyMi4xNDQgMjkuMjM3IDIyLjE0NCBMIDI5LjAzNyAyMy4xNDQgQyAyOC45MzcgMjMuNTQ0IDI5LjIzNyAyMy45NDQgMjkuNjM3IDIzLjk0NCBMIDMzLjAzNyAyMy45NDQgQyAzMy41MzcgMjMuOTQ0IDM0LjAzNyAyMy41NDQgMzQuMTM3IDIzLjA0NCBMIDM2LjEzNyAxMC4yNDQgQyAzNi4yMzcgMTAuMDQ0IDM1LjgzNyA5LjY0NCAzNS40MzcgOS42NDQgWiBNIDMwLjMzNyAxNi44NDQgQyAyOS45MzcgMTguOTQ0IDI4LjMzNyAyMC40NDQgMjYuMTM3IDIwLjQ0NCBDIDI1LjAzNyAyMC40NDQgMjQuMjM3IDIwLjE0NCAyMy42MzcgMTkuNDQ0IEMgMjMuMDM3IDE4Ljc0NCAyMi44MzcgMTcuODQ0IDIzLjAzNyAxNi44NDQgQyAyMy4zMzcgMTQuNzQ0IDI1LjEzNyAxMy4yNDQgMjcuMjM3IDEzLjI0NCBDIDI4LjMzNyAxMy4yNDQgMjkuMTM3IDEzLjY0NCAyOS43MzcgMTQuMjQ0IEMgMzAuMjM3IDE0Ljk0NCAzMC40MzcgMTUuODQ0IDMwLjMzNyAxNi44NDQgWiI+PC9wYXRoPjxwYXRoIGZpbGw9IiMwMDMwODciIGQ9Ik0gNTUuMzM3IDkuNjQ0IEwgNTEuNjM3IDkuNjQ0IEMgNTEuMjM3IDkuNjQ0IDUwLjkzNyA5Ljg0NCA1MC43MzcgMTAuMTQ0IEwgNDUuNTM3IDE3Ljc0NCBMIDQzLjMzNyAxMC40NDQgQyA0My4yMzcgOS45NDQgNDIuNzM3IDkuNjQ0IDQyLjMzNyA5LjY0NCBMIDM4LjYzNyA5LjY0NCBDIDM4LjIzNyA5LjY0NCAzNy44MzcgMTAuMDQ0IDM4LjAzNyAxMC41NDQgTCA0Mi4xMzcgMjIuNjQ0IEwgMzguMjM3IDI4LjA0NCBDIDM3LjkzNyAyOC40NDQgMzguMjM3IDI5LjA0NCAzOC43MzcgMjkuMDQ0IEwgNDIuNDM3IDI5LjA0NCBDIDQyLjgzNyAyOS4wNDQgNDMuMTM3IDI4Ljg0NCA0My4zMzcgMjguNTQ0IEwgNTUuODM3IDEwLjU0NCBDIDU2LjEzNyAxMC4yNDQgNTUuODM3IDkuNjQ0IDU1LjMzNyA5LjY0NCBaIj48L3BhdGg+PHBhdGggZmlsbD0iIzAwOWNkZSIgZD0iTSA2Ny43MzcgMi40NDQgTCA1OS45MzcgMi40NDQgQyA1OS40MzcgMi40NDQgNTguOTM3IDIuODQ0IDU4LjgzNyAzLjM0NCBMIDU1LjczNyAyMy4yNDQgQyA1NS42MzcgMjMuNjQ0IDU1LjkzNyAyMy45NDQgNTYuMzM3IDIzLjk0NCBMIDYwLjMzNyAyMy45NDQgQyA2MC43MzcgMjMuOTQ0IDYxLjAzNyAyMy42NDQgNjEuMDM3IDIzLjM0NCBMIDYxLjkzNyAxNy42NDQgQyA2Mi4wMzcgMTcuMTQ0IDYyLjQzNyAxNi43NDQgNjMuMDM3IDE2Ljc0NCBMIDY1LjUzNyAxNi43NDQgQyA3MC42MzcgMTYuNzQ0IDczLjYzNyAxNC4yNDQgNzQuNDM3IDkuMzQ0IEMgNzQuNzM3IDcuMjQ0IDc0LjQzNyA1LjU0NCA3My40MzcgNC4zNDQgQyA3Mi4yMzcgMy4xNDQgNzAuMzM3IDIuNDQ0IDY3LjczNyAyLjQ0NCBaIE0gNjguNjM3IDkuNzQ0IEMgNjguMjM3IDEyLjU0NCA2Ni4wMzcgMTIuNTQ0IDY0LjAzNyAxMi41NDQgTCA2Mi44MzcgMTIuNTQ0IEwgNjMuNjM3IDcuMzQ0IEMgNjMuNjM3IDcuMDQ0IDYzLjkzNyA2Ljg0NCA2NC4yMzcgNi44NDQgTCA2NC43MzcgNi44NDQgQyA2Ni4xMzcgNi44NDQgNjcuNDM3IDYuODQ0IDY4LjEzNyA3LjY0NCBDIDY4LjYzNyA4LjA0NCA2OC43MzcgOC43NDQgNjguNjM3IDkuNzQ0IFoiPjwvcGF0aD48cGF0aCBmaWxsPSIjMDA5Y2RlIiBkPSJNIDkwLjkzNyA5LjY0NCBMIDg3LjIzNyA5LjY0NCBDIDg2LjkzNyA5LjY0NCA4Ni42MzcgOS44NDQgODYuNjM3IDEwLjE0NCBMIDg2LjQzNyAxMS4xNDQgTCA4Ni4xMzcgMTAuNzQ0IEMgODUuMzM3IDkuNTQ0IDgzLjUzNyA5LjE0NCA4MS43MzcgOS4xNDQgQyA3Ny42MzcgOS4xNDQgNzQuMTM3IDEyLjI0NCA3My40MzcgMTYuNjQ0IEMgNzMuMDM3IDE4Ljg0NCA3My41MzcgMjAuOTQ0IDc0LjgzNyAyMi4zNDQgQyA3NS45MzcgMjMuNjQ0IDc3LjYzNyAyNC4yNDQgNzkuNTM3IDI0LjI0NCBDIDgyLjgzNyAyNC4yNDQgODQuNzM3IDIyLjE0NCA4NC43MzcgMjIuMTQ0IEwgODQuNTM3IDIzLjE0NCBDIDg0LjQzNyAyMy41NDQgODQuNzM3IDIzLjk0NCA4NS4xMzcgMjMuOTQ0IEwgODguNTM3IDIzLjk0NCBDIDg5LjAzNyAyMy45NDQgODkuNTM3IDIzLjU0NCA4OS42MzcgMjMuMDQ0IEwgOTEuNjM3IDEwLjI0NCBDIDkxLjYzNyAxMC4wNDQgOTEuMzM3IDkuNjQ0IDkwLjkzNyA5LjY0NCBaIE0gODUuNzM3IDE2Ljg0NCBDIDg1LjMzNyAxOC45NDQgODMuNzM3IDIwLjQ0NCA4MS41MzcgMjAuNDQ0IEMgODAuNDM3IDIwLjQ0NCA3OS42MzcgMjAuMTQ0IDc5LjAzNyAxOS40NDQgQyA3OC40MzcgMTguNzQ0IDc4LjIzNyAxNy44NDQgNzguNDM3IDE2Ljg0NCBDIDc4LjczNyAxNC43NDQgODAuNTM3IDEzLjI0NCA4Mi42MzcgMTMuMjQ0IEMgODMuNzM3IDEzLjI0NCA4NC41MzcgMTMuNjQ0IDg1LjEzNyAxNC4yNDQgQyA4NS43MzcgMTQuOTQ0IDg1LjkzNyAxNS44NDQgODUuNzM3IDE2Ljg0NCBaIj48L3BhdGg+PHBhdGggZmlsbD0iIzAwOWNkZSIgZD0iTSA5NS4zMzcgMi45NDQgTCA5Mi4xMzcgMjMuMjQ0IEMgOTIuMDM3IDIzLjY0NCA5Mi4zMzcgMjMuOTQ0IDkyLjczNyAyMy45NDQgTCA5NS45MzcgMjMuOTQ0IEMgOTYuNDM3IDIzLjk0NCA5Ni45MzcgMjMuNTQ0IDk3LjAzNyAyMy4wNDQgTCAxMDAuMjM3IDMuMTQ0IEMgMTAwLjMzNyAyLjc0NCAxMDAuMDM3IDIuNDQ0IDk5LjYzNyAyLjQ0NCBMIDk2LjAzNyAyLjQ0NCBDIDk1LjYzNyAyLjQ0NCA5NS40MzcgMi42NDQgOTUuMzM3IDIuOTQ0IFoiPjwvcGF0aD48L3N2Zz4="
                                                            title="Pay Now">
                                                         </button>
                                                      </div>
                                                   </c:when>
                                                </c:choose>
                                             </form:form>
                                             </div>
                                          </div>
                                          <%-- <script src="https://www.paypal.com/sdk/js?client-id=<%=UserDefinedKeyWords.PAYPAL_CLIENT_ID%>&commit=true"> </script>
                                             <div id="paypal-button-container"></div> --%>
                                          <!-- <script>
                                             /* paypal.Buttons().render('#paypal-button-container'); */
                                             paypal.Buttons({
                                             	enableStandardCardFields: false,
                                             }).render('#paypal-button-container');
                                             //This function displays Smart Payment Buttons on your web page.
                                             </script> -->
                                          <!-- <form role="form" id="payment-form" method="POST" action="javascript:void(0);">
                                             <div class="row">
                                                <div class="col-xs-12">
                                                   <div class="form-group">
                                                      <label for="cardNumber">CARD NUMBER</label>
                                                      <div class="">
                                                         <input type="tel" class="form-control" name="cardNumber" placeholder="Valid Card Number" autocomplete="cc-number" required="" autofocus="">
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-xs-12">
                                                   <div class="form-group">
                                                      <label for="cardNumber">CARD HOLDER NAME</label>
                                                      <input type="text" class="form-control" name=" " placeholder="Valid Card Holder Name" autocomplete="cc-number" required="" autofocus="">
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-xs-7 col-md-7">
                                                   <div class="form-group">
                                                      <label for="cardExpiry"><span class="hidden-xs">EXPIRATION</span><span class="visible-xs-inline">EXP</span> DATE</label>
                                                      <input type="tel" class="form-control" name="cardExpiry" placeholder="MM / YY" autocomplete="cc-exp" required="">
                                                   </div>
                                                </div>
                                                <div class="col-xs-5 col-md-5 pull-right">
                                                   <div class="form-group">
                                                      <label for="cardCVC">CV CODE</label>
                                                      <input type="tel" class="form-control" name="cardCVC" placeholder="CVC" autocomplete="cc-csc" required="">
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-xs-12">
                                                   <div class="form-group">
                                                      <div class="display-td">                            
                                                         <img class="img-responsiveS" src="http://i76.imgup.net/accepted_c22e0.png">
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-xs-12">
                                                   <button class="subscribe btn btn-success btn-lg btn-block" type="button">Pay</button>
                                                </div>
                                             </div>
                                             </form> -->
                                          <%--  <h1>Please Review Before Paying</h1>
                                             <form action="executeUserPayment" method="post">
                                             <table>
                                             	<tr>
                                             		<td colspan="2"><b>Transaction Details:</b></td>
                                             		<td>
                                             			<input type="hidden" name="paymentId" value="${param.paymentId}" />
                                             			<input type="hidden" name="PayerID" value="${param.PayerID}" />
                                             		</td>
                                             	</tr>
                                             	<tr>
                                             		<td>Description:</td>
                                             		<td>${transaction.description}</td>
                                             	</tr>
                                             	<tr>
                                             		<td>Subtotal:</td>
                                             		<td>${transaction.amount.details.subtotal} USD</td>
                                             	</tr>
                                             	<tr>
                                             		<td>Shipping:</td>
                                             		<td>${transaction.amount.details.shipping} USD</td>
                                             	</tr>
                                             	<tr>
                                             		<td>Tax:</td>
                                             		<td>${transaction.amount.details.tax} USD</td>
                                             	</tr>
                                             	<tr>
                                             		<td>Total:</td>
                                             		<td>${transaction.amount.total} USD</td>
                                             	</tr>	
                                             	<tr><td><br/></td></tr>
                                             	<tr>
                                             		<td colspan="2"><b>Payer Information:</b></td>
                                             	</tr>
                                             	<tr>
                                             		<td>First Name:</td>
                                             		<td>${payer.firstName}</td>
                                             	</tr>
                                             	<tr>
                                             		<td>Last Name:</td>
                                             		<td>${payer.lastName}</td>
                                             	</tr>
                                             	<tr>
                                             		<td>Email:</td>
                                             		<td>${payer.email}</td>
                                             	</tr>
                                             	<tr><td><br/></td></tr>
                                             	<tr>
                                             		<td colspan="2"><b>Shipping Address:</b></td>
                                             	</tr>
                                             	<tr>
                                             		<td>Recipient Name:</td>
                                             		<td>${shippingAddress.recipientName}</td>
                                             	</tr>
                                             	<tr>
                                             		<td>Line 1:</td>
                                             		<td>${shippingAddress.line1}</td>
                                             	</tr>
                                             	<tr>
                                             		<td>City:</td>
                                             		<td>${shippingAddress.city}</td>
                                             	</tr>
                                             	<tr>
                                             		<td>State:</td>
                                             		<td>${shippingAddress.state}</td>
                                             	</tr>
                                             	<tr>
                                             		<td>Country Code:</td>
                                             		<td>${shippingAddress.countryCode}</td>
                                             	</tr>
                                             	<tr>
                                             		<td>Postal Code:</td>
                                             		<td>${shippingAddress.postalCode}</td>
                                             	</tr>
                                             	<tr>
                                             		<td colspan="2" align="center">
                                             			<input type="submit" value="Pay Now" />
                                             		</td>
                                             	</tr>		
                                             </table>
                                             </form> --%>
                                          <!--  <form action="authorize_payment" method="post">
                                             <table>
                                             	<tr>
                                             		<td>Product/Service:</td>
                                             		<td><input type="text" name="product" value="Next iPhone" /></td>
                                             	</tr>
                                             	<tr>
                                             		<td>Sub Total:</td>
                                             		<td><input type="text" name="subtotal" value="100" /></td>
                                             	</tr>
                                             	<tr>
                                             		<td>Shipping:</td>
                                             		<td><input type="text" name="shipping" value="10" /></td>
                                             	</tr>		
                                             	<tr>
                                             		<td>Tax:</td>
                                             		<td><input type="text" name="tax" value="10" /></td>
                                             	</tr>		
                                             	<tr>
                                             		<td>Total Amount:</td>
                                             		<td><input type="text" name="total" value="120" /></td>
                                             	</tr>
                                             	<tr>
                                             		<td colspan="2" align="center">
                                             			<input type="submit" value="Checkout" />
                                             		</td>
                                             	</tr>
                                             </table>
                                             </form> -->
                                       </div>
                                    </div>
                                 </div>
                                 <!--  <div id="menu1" class="tab-pane fade">
                                    <div class="creditbal">
                                       <div class="panel-body">
                                          <form role="form" id="payment-form" method="POST" action="javascript:void(0);">
                                             <div class="row">
                                                <div class="col-xs-12">
                                                   <div class="form-group">
                                                      <label for="cardNumber">Search for your bank:</label>
                                                      <input list="browsers" class="form-control" name="browser">
                                                      <datalist id="browsers">
                                                         <option value="Axis Bank ">
                                                         </option>
                                                         <option value="Kotak Mahindra ">
                                                         </option>
                                                         <option value="HDFC ">
                                                         </option>
                                                         <option value="ICICI Bank ">
                                                         </option>
                                                         <option value="State Bank of India ">
                                                         </option>
                                                      </datalist>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-xs-12">
                                                   <button class="subscribe btn btn-success btn-lg btn-block" type="button">Pay</button>
                                                </div>
                                             </div>
                                          </form>
                                       </div>
                                    </div>
                                    </div> -->
                                 <!-- <div id="menu4" class="tab-pane fade">
                                    <div class="creditbal">
                                       <div class="panel-body">
                                          <form role="form" id="payment-form" method="POST" action="javascript:void(0);">
                                             <div class="row">
                                                <div class="col-xs-7 col-md-7">
                                                   <div class="form-group">
                                                      <label for="cardExpiry"><span class="hidden-xs">Google Pay UPI ID</span><span class="visible-xs-inline">EXP</span> DATE</label>
                                                      <input type="tel" class="form-control" name="cardExpiry" placeholder=" " required="">
                                                   </div>
                                                </div>
                                                <div class="col-xs-5 col-md-5 pull-right">
                                                   <div class="form-group">
                                                      <label for="cardCVC">Bankname</label>
                                                      <input type="tel" class="form-control" name="cardCVC" placeholder=" " required="">
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-xs-12">
                                                   <button class="subscribe btn btn-success btn-lg btn-block" type="button">Pay</button>
                                                </div>
                                             </div>
                                          </form>
                                       </div>
                                    </div>
                                    </div> -->
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-2"></div>
               </div>
            </div>
         </div>
      </section>
      
      
      <!---sports elearning section end here--->
      <!-- Footer start -->
      <%@ include file="/WEB-INF/handlingJsps/userFooter.jsp"%>
      <!-- Footer end -->
      <%@ include file="/WEB-INF/handlingJsps/userLowerFooter.jsp"%>
      <!-- Custom javascript -->
      <script type="text/javascript">
      $(document).ready(function(){
       	
       	$("#voucherCardDisplay").hide();
       	//var data = eval('('+'${displayStatus}'+')'); 
       
         $("#voucherCard").click(function(){
           $("#voucherCardDisplay").toggle();
           
         });
         
         
       });
      </script>
   </body>
</html>