<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>NovoFitness Trainer Courses</title>
      <%@ include file="/WEB-INF/handlingJsps/trainerTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/trainerSession.jsp"%>
   </head>
   <body>
      <!-- Left Panel Sidebar-->
      <%@ include file="/WEB-INF/handlingJsps/trainerSidebar.jsp"%>
      <div id="right-panel" class="right-panel">
         <!-- Right Panel Header-->
         <%@ include file="/WEB-INF/handlingJsps/trainerHeader.jsp"%>
         <div class="breadcrumbs">
            <div class="col-sm-4">
               <div class="page-header float-left">
                  <div class="page-title">
                     <h1>Course Management</h1>
                  </div>
               </div>
            </div>
            <div class="col-sm-4"></div>
         </div>
         <div class="content mt-3">
            <div class="animated fadeIn">
               <div class="row">
                  <div class="col-md-12">
                     <div class="card">
                        <c:choose>
                           <c:when test="${ trainerSession.trainerIsApprovedByAdmin }">
                              <div class="card-header">
                                 <strong class="card-title">List of Courses </strong>
                                 <c:if test="${ not empty alertSuccessMessage }">
                                    <div class="alert alert-success">${ alertSuccessMessage }</div>
                                 </c:if>
                                 <c:if test="${ not empty alertSuccessMessageForAddingNewCourse }">
                                    <div class="alert alert-success">${ alertSuccessMessageForAddingNewCourse }</div>
                                 </c:if>
                                 <c:if test="${ not empty alertFailMessage }">
                                    <div class="alert alert-danger">${ alertFailMessage }</div>
                                 </c:if>
                                 <c:if test="${ not empty alertFailMessageForAddingNewCourseOnlyCoverImageFailure }">
                                    <div class="alert alert-danger">
                                       ${ alertFailMessageForAddingNewCourseOnlyCoverImageFailure }
                                    </div>
                                 </c:if>
                                 <div class="right-logo">
                                    <a href="trainerAddCourseManagement"><button type="button"
                                       class="btn btn-success btn-sm">Add Course</button></a> 
                                    <!-- <a href=""><img
                                       src="assets/adminAssets/images/pdf-512.png" class="src-img"></a>
                                       <a href=""><img class="src-img-2"
                                       src="assets/adminAssets/images/icons8-microsoft-excel-48.png"></a> -->
                                 </div>
                              </div>
                              <div class="card-body" id="userlist">
                                 <table id="trainerpanels"
                                    class="table table-striped table-bordered">
                                    <thead>
                                       <tr>
                                          <th>
                                             <fmt:message key="label.srNo" />
                                          </th>
                                          <th>
                                             <fmt:message key="label.courseName" />
                                          </th>
                                          <th>
                                             Category
                                          </th>
                                          <th>
                                             Price
                                          </th>
                                          <th>
                                             <fmt:message key="label.action" />
                                          </th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <c:choose>
                                          <c:when test="${ not empty trainerCourseList }">
                                             <c:forEach items="${ trainerCourseList }" var="trainerCourse" varStatus="loop">
                                                <tr>
                                                   <td>${ loop.index+1 }</td>
                                                   <td>${ trainerCourse.trainerCourseInfo.courseName }</td>
                                                   <td>${ trainerCourse.trainerCourseInfo.courseCategoryInfo.categoryName }</td>
                                                   <td>${ trainerCourse.trainerCourseInfo.coursePrice }</td>
                                                   <td class="center-align">
                                                      <%-- <a href="trainersCourseView/${ trainerCourse.trainerCourseId }"><i class="fa fa-eye"></i></a> --%> 
                                                      <a href="trainersCourseView/${ trainerCourse.trainerCourseId }"><i class="fa fa-edit"></i></a>
                                                      <i class="fa fa-trash" title="Delete" onclick="deleteTrainerCourse(${trainerCourse.trainerCourseId})" ></i>
                                                   </td>
                                                </tr>
                                             </c:forEach>
                                          </c:when>
                                       </c:choose>
                                    </tbody>
                                 </table>
                              </div>
                           </c:when>
                           <c:when test="${ !trainerSession.trainerIsApprovedByAdmin }">
                              <div class="card-header">
                                 <strong class="card-title">Wait still Admin will approved your account.</strong>
                              </div>
                           </c:when>
                        </c:choose>
                     </div>
                  </div>
               </div>
            </div>
            <!-- .animated -->
         </div>
         <!-- .content -->
      </div>
      <!-- /#right-panel -->
      <%@ include file="/WEB-INF/handlingJsps/trainerLowerFooter.jsp"%>
      <script type="text/javascript">
         function deleteTrainerCourse(id) {
         	 swal({
              	  title: 'Are you sure?',
              	  text: "Course will permanently deleted !",
              	  type: 'warning',
              	  showCancelButton: true,
              	  confirmButtonColor: '#3085d6',
              	  cancelButtonColor: '#d33',
              	  confirmButtonText: 'Yes, delete it!',
              	  closeOnConfirm: false
             	} ,
            		function() {
              	$.ajax({
                      type: "POST",
                      url: "deleteTrainerCourse",
                      data: "trainerCourseId="+id,
                      success: function (result) {
                      }
                  })
                  .done(function(data) {
                 swal("Deleted!", "Course deleted successfully!", "success");
                 location.reload();
               })
               .error(function(data) {
                 swal("Oops", "We couldn't connect to the server!", "error");
               });
            	});
          }
      </script>
   </body>
</html>