<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>NovoFitness Course Details</title>
      <%@ include file="/WEB-INF/handlingJsps/adminTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/adminSession.jsp"%>
      <c:choose>
         <c:when test="${ adminSession.adminRole.roleId == 2 }">
            <c:redirect url="adminDashboard" />
         </c:when>
      </c:choose>
   </head>
   <body>
      <!-- Left Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminSidebar.jsp"%>
      <!-- Left Panel -->
      <!-- Right Panel -->
      <div id="right-panel" class="right-panel">
         <!-- Header-->
         <%@ include file="/WEB-INF/handlingJsps/adminHeader.jsp"%>
         <!-- /header -->
         <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Payment Management View</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                       <ol class="breadcrumb text-right">
                            <li><a href="adminTrainerCourseUserPaymentManagement"><b>Back</b></a></li>
                          
                        </ol>
                    </div>
                </div>
            </div>
        </div>
         <div class="content mt-3" id="Course-list-view">
            <div class="animated fadeIn">
               <div class="row">
                  <div class="col-xs-12 col-sm-12">
                     <div class="card">
                        <div class="card-body card-block">
                           <div class="col-xs-6 col-sm-6">
                              <div class="form-group">
                                 <div class="form-group">
                                    <label class=" form-control-label"> First Name</label>
                                    <div class="input-group">
                                       <input class="form-control" type="text" disabled="disabled" value="${ userCoursePaymentInfo.userInfo.userFName }" />
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label for="company" class=" form-control-label">Email id</label>
                                    <input type="text" id="company" disabled="disabled" value="${ userCoursePaymentInfo.userInfo.userEmail }"  class="form-control">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="form-control-label">Course Name</label>
                                 <div class="input-group">
                                    <input class="form-control" disabled="disabled" value="${ userCoursePaymentInfo.trainerCourseInfo.trainerCourseInfo.courseName}"  type="text">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="form-control-label">Payment Status</label>
                                 <div class="input-group">
                                    <input class="form-control" disabled="disabled" value="Completed" type="text">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="vat" class=" form-control-label">Payment Date</label>
                                 <input type="text" id="vat" disabled="disabled" value="${ userCoursePaymentInfo.userCoursePaymentCreatedDateTime }"  class="form-control">
                              </div>
                           </div>
                           <div class="col-xs-6 col-sm-6">
                              <div class="form-group">
                                 <label class=" form-control-label">Last Name</label>
                                 <div class="input-group">
                                    <input class="form-control" type="text" disabled="disabled" value="${ userCoursePaymentInfo.userInfo.userLName }" />
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="vat" class=" form-control-label">Mobile No</label>
                                 <input type="text" id="vat" disabled="disabled" value="${ userCoursePaymentInfo.userInfo.userPhoneNo }" class="form-control">
                              </div>
                              <div class="form-group">
                                 <label class="form-control-label">Trainer Name</label>
                                 <div class="input-group">
                                    <input class="form-control" disabled="disabled" value="${ userCoursePaymentInfo.trainerCourseInfo.trainerInfo.trainerFName } ${ userCoursePaymentInfo.trainerCourseInfo.trainerInfo.trainerFName }"  type="text">
                                 </div>
                              </div>
                              <!-- <div class="form-group">
                                 <label for="vat" class=" form-control-label">Payment Mode</label>
                                 <input type="text" id="vat" value="Paypal" class="form-control">
                              </div> -->
                              <div class="form-group">
                                 <label for="vat" class=" form-control-label">Total Payment</label>
                                 <input type="text" id="vat" disabled="disabled" value="${ userCoursePaymentInfo.trainerCourseInfo.trainerCourseInfo.coursePrice }" class="form-control">
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- .content -->
      <!-- Right Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminLowerFooter.jsp"%>
   </body>
</html>