<%@page import="com.cube9.novafitness.config.UserDefinedKeyWords"%>
<!DOCTYPE html>
<html lang="zxx">
   <head>
      <!-- top header -->
      <title>NovoFitness</title>
      <%@ include file="/WEB-INF/handlingJsps/userTopHeader.jsp"%>
       <%@ include file="/WEB-INF/handlingJsps/userSession.jsp"%>
      <!-- close top header -->
      <style>
         .justify-content-center {
         -webkit-box-pack: center;
         -ms-flex-pack: center;
         justify-content: center;
         }
         .align-items-center {
         -ms-flex-align: center!important;
         align-items: center!important;
         }
         .justify-content-center {
         -ms-flex-pack: center!important;
         justify-content: center!important;
         }
         button.btn.btn-primary.btns-joins-links {
         padding: 11px 30px;
         letter-spacing: 1px;
         font-size: 17px;
         font-weight: 600;
         text-transform: uppercase;
         color: #fff;
         transition: .5s;
         border-radius: 2px;
         border: none;
         background-color: #f59e01;
         height: 56px;
         width: 250px;
         }
         .joins-btns {
         text-align: center;
         }
         .referralportions.justify-content-center.align-items-center {
         background-color: #fff;
         box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
         border-radius: 5px;
         position: relative;
         /* top: -161px; */
         padding: 45px;
         }
         .referralportions.justify-content-center.align-items-center {
         background-color: #fff;
         box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
         border-radius: 5px;
         position: relative;
         /* top: -161px; */
         padding: 45px;
         width: 600px;
         margin: 0 auto;
         }
         button.btn.btn-default.btn-emails-submit {
         cursor: pointer;
         padding: 11px 30px;
         letter-spacing: 1px;
         font-size: 13px;
         font-weight: 600;
         text-transform: uppercase;
         color: #fff;
         transition: .5s;
         border-radius: 2px;
         border: none;
         background-color: #f59e01;
         width: 200px;
         }
         textarea#comment {
         box-shadow: none;
         border-radius: 0px;
         }
      </style>
   </head>
   <body>
      <div class="page_loader"></div>
      <!-- Top header start -->
      <%@ include file="/WEB-INF/handlingJsps/userHeader.jsp"%>
      <!-- Top header end -->
      <!-- Sub banner start -->
      <div class="category-heading">
         <div class="container-fluid">
            <div class="categort-title">
               <h1>Invitation/Referral Friend</h1>
            </div>
         </div>
      </div>
      <section class="login_payment">
         <div class="container">
            <div class="referralportions justify-content-center align-items-center">
               <div class="joins-btns">
                  <!-- <button type="button" class="btn btn-primary btns-joins-links" disabled>Join Link</button> -->
                  <div class="form-group">
                     <input type="text" class="form-control join-txts" placeholder="Join Link" id="usr" disabled>
                  </div>
               </div>
               <hr>
               <form action="#">
                  <div class="form-group">
                     <label for="email">Email</label>
                     <textarea class="form-control form-referrals" rows="3" id="comment"></textarea>
                  </div>
                  <p>Note:<b> Add more Email by separating comma</b></p>
                  <button type="submit" class="btn btn-default btn-emails-submit">Submit</button>
               </form>
            </div>
         </div>
      </section>
      <!---sports elearning section end here--->
      <!-- Footer start -->
      <%@ include file="/WEB-INF/handlingJsps/userFooter.jsp"%>
      <!-- Footer end -->
      <%@ include file="/WEB-INF/handlingJsps/userLowerFooter.jsp"%>
      <!-- Custom javascript -->
   </body>
</html>