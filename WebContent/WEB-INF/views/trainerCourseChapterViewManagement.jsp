<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>NovoFitness Trainer Courses</title>
      <%@ include file="/WEB-INF/handlingJsps/trainerTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/trainerSession.jsp"%>
      <script src="assets/ckeditor/ckeditor.js"></script>
   </head>
   <body>
      <!-- Left Panel Sidebar-->
      <%@ include file="/WEB-INF/handlingJsps/trainerSidebar.jsp"%>
      <div id="right-panel" class="right-panel">
         <!-- Right Panel Header-->
         <%@ include file="/WEB-INF/handlingJsps/trainerHeader.jsp"%>
         <div class="breadcrumbs">
            <div class="col-sm-4">
               <div class="page-header float-left">
                  <div class="page-title">
                     <h1>Course Chapter View Update</h1>
                  </div>
               </div>
            </div>
            <div class="col-sm-8">
               <div class="page-header float-right">
                  <div class="page-title">
                     <ol class="breadcrumb text-right">
                        <li><a href="trainersCourseView/${ trainerCourseChapterDetails.trainerCourseInfo.trainerCourseId }"><b>Back</b></a></li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
         <div class="content mt-3">
            <div class="animated fadeIn">
               <div class="card">
                  <div class="card-header">
                     <strong>Course Chapter</strong>
                     <c:if test="${ not empty alertSuccessMessage }">
                        <div class="alert alert-success">${ alertSuccessMessage }</div>
                     </c:if>
                     <c:if test="${ not empty alertSuccessMessageForAddingCourseChapterLesson }">
                        <div class="alert alert-success">${ alertSuccessMessageForAddingCourseChapterLesson }</div>
                     </c:if>
                     <c:if test="${ not empty alertFailMessage }">
                        <div class="alert alert-danger">${ alertFailMessage }</div>
                     </c:if>
                     <c:if test="${ not empty alertFailMessageForAddingCourseChapterLesson }">
                        <div class="alert alert-danger">${ alertFailMessageForAddingCourseChapterLesson }
                        </div>
                     </c:if>
                  </div>
                  <div class="card-body card-block">
                     <form:form action="addNewTrainerCourseChapterLesson" method="post"
                        modelAttribute="trainerCourseChapterLessonWrapper"
                        enctype="multipart/form-data">
                        <div class="row">
                           <div class="col-lg-6 col-md-6 ">
                              <div class="form-group">
                                 <label for="courseName" class=" form-control-label">
                                 Chapter Name <span class="error">*</span>
                                 </label>
                                 <form:hidden path="trainerCourseChapterId"
                                    value="${ trainerCourseChapterDetails.trainerCourseChapterId }" />
                                 <form:input path="trainerCourseChapterName"
                                    class="form-control" id="company" disabled="true"
                                    value="${ trainerCourseChapterDetails.trainerCourseChapterName }" />
                                 <form:errors path="trainerCourseChapterName" class="error" />
                              </div>
                           </div>
                           <div class="col-lg-6 col-md-6 ">
                              <div class="form-group">
                                 <label for="courseName" class=" form-control-label">
                                 Lesson Name <span class="error">*</span>
                                 </label>
                                 <form:input path="trainerCourseChapterLessonName"
                                    class="form-control" id="company" />
                                 <form:errors path="trainerCourseChapterLessonName"
                                    class="error" />
                              </div>
                           </div>
                        </div>
                        <!-- rows -->
                        <hr>
                        <%-- <div class="row">
                           <div class="col-lg-6 col-md-6 ">
                              <div class="form-group">
                                 <label for="company" class=" form-control-label">Upload
                                 Preview Video <span class="error">*</span>
                                 </label><br>
                                 <div class="row">
                                    <c:choose>
                                       <c:when
                                          test="${ not empty trainerCourseDetils.trainerCourseInfo.courseCoverVideoInfo }">
                                          <div class="col-md-4">
                                             <iframe
                                                src="https://player.vimeo.com/video/${ trainerCourseDetils.trainerCourseInfo.courseCoverVideoInfo.courseCoverVideoVimeoName }"
                                                width="100%" height="100px" frameborder="0"
                                                webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                             <button class="btn delete-button" data-title="Delete"
                                                onclick="deleteCoverImage(${ userCompleteProfile.userProfileCoverImageInfo.userProfileCoverImageId }, ${ userCompleteProfile.userProfileId })">
                                             <i class="fa fa-trash" aria-hidden="true" title="Delete"></i>
                                             </button>
                                          </div>
                                       </c:when>
                                       <c:otherwise>
                                          <div class="col-md-4">No Preview Video Available.
                                             Please upload Preview Video.
                                          </div>
                                       </c:otherwise>
                                       </c:choose>
                                 </div>
                                 <!--  rows -->
                                 <br>
                                 <form:input type="file" accept="video/mp4,video/x-m4v,video/*"
                                    path="trainerCourseChapterLessonPreviewVideo" id="file-input"
                                    name="file-input" class="form-control-file" />
                                 <form:errors path="trainerCourseChapterLessonPreviewVideo"
                                    class="error" />
                              </div>
                           </div>
                           <div class="col-lg-6 col-md-6 ">
                              <div class="form-group">
                                 <label for="company" class=" form-control-label">Upload
                                 Actual Video <span class="error">*</span>
                                 </label><br>
                                 <div class="row">
                                     <c:choose>
                                       <c:when test="${ not empty trainerCourseOtherVideosList }">
                                          <c:forEach items="${ trainerCourseOtherVideosList }"
                                             var="trainerCourseOtherVideos">
                                             <c:choose>
                                                <c:when
                                                   test="${ not empty trainerCourseOtherVideos.courseVideoInfo.courseVideoVimeoName }">
                                                   <div class="col-md-4">
                                                      <iframe
                                                         src="https://player.vimeo.com/video/${ trainerCourseOtherVideos.courseVideoInfo.courseVideoVimeoName }"
                                                         width="100%" height="100px" frameborder="0"
                                                         webkitallowfullscreen mozallowfullscreen
                                                         allowfullscreen></iframe>
                                                      <button class="btn delete-button" data-title="Delete"
                                                         onclick="deleteCoverImage(${ userCompleteProfile.userProfileCoverImageInfo.userProfileCoverImageId }, ${ userCompleteProfile.userProfileId })">
                                                      <i class="fa fa-trash" aria-hidden="true"
                                                         title="Delete"></i>
                                                      </button>
                                                      <video controls>
                                                         <source src="${ trainerCourseOtherVideos.courseVideoInfo.courseVideoViewPath }" type="video/mp4">
                                                         <source src="${ trainerCourseOtherVideos.courseVideoInfo.courseVideoViewPath }" type="video/ogg">
                                                         </video>
                                                   </div>
                                                </c:when>
                                             </c:choose>
                                          </c:forEach>
                                       </c:when>
                                       <c:otherwise>
                                          <div class="col-md-4">No Videos Available. Please
                                             upload more Videos.
                                          </div>
                                       </c:otherwise>
                                       </c:choose>
                                    <!-- <div class="col-md-4">
                                       </div> -->
                                 </div>
                                 <!-- rows -->
                                 <br>
                                 <form:input type="file" accept="video/mp4,video/x-m4v,video/*"
                                    path="trainerCourseChapterLessonMainVideo" id="file-input"
                                    name="file-input" class="form-control-file" />
                                 <form:errors path="trainerCourseChapterLessonMainVideo"
                                    class="error" />
                              </div>
                           </div>
                        </div> --%>
                        <%-- <hr>
                        <div class="row">
                           <div class="col-lg-6 col-md-6 ">
                              <div class="form-group">
                                 <label for="company" class=" form-control-label">Upload
                                 PDF <span class="error">*</span></label><br>
                                 <form:input type="file" accept="application/pdf"
                                    path="trainerCourseChapterLessonMainPdf" id="file-input" name="file-input"
                                    class="form-control-file" multiple="multiple" />
                                 <form:errors path="trainerCourseChapterLessonMainPdf" class="error" />
                              </div>
                           </div>
                           </div>
                           <hr> --%>
                        <div class="row">
                           <div class="col-md-12">
                              <button type="submit" class="btn btn-success btn-sm">Add
                              New Lesson</button>
                           </div>
                        </div>
                     </form:form>
                     <hr>
                     <!-- rows -->
                     <!-- Lessons List  -->
                     <div class="col-md-12">
                        <div class="card">
                           <div class="card-header">
                              <strong class="card-title">Lessons List</strong>
                           </div>
                           <div class="card-body">
                              <table id="trainers" class="table table-striped table-bordered">
                                 <thead>
                                    <tr>
                                       <th>
                                          <fmt:message key="label.srNo" />
                                       </th>
                                       <th>Chapter Name</th>
                                       <th>Lesson Name</th>
                                       <th>Preview Video</th>
                                       <th>Actual Video</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <c:choose>
                                       <c:when test="${ not empty trainerCourseChapterLessonList }">
                                          <c:forEach items="${ trainerCourseChapterLessonList }"
                                             var="trainerCourseChapterLesson" varStatus="loop">
                                             <tr>
                                                <td>${ loop.index+1 }</td>
                                                <td>${ trainerCourseChapterLesson.trainerCourseChapterInfo.trainerCourseChapterName }</td>
                                                <td>${ trainerCourseChapterLesson.trainerCourseChapterLessonName }</td>
                                                <td>
                                                   <c:choose>
                                                      <c:when
                                                         test="${ not empty trainerCourseChapterLesson.trainerCourseChapterLessonPreviewVideoVimeoName }">
                                                         <iframe
                                                            src="https://player.vimeo.com/video/${ trainerCourseChapterLesson.trainerCourseChapterLessonPreviewVideoVimeoName }"
                                                            width="100%" height="100px" frameborder="0"
                                                            webkitallowfullscreen mozallowfullscreen
                                                            allowfullscreen></iframe>
                                                         <%-- <button class="btn delete-button" data-title="Delete"
                                                            onclick="deleteCoverImage(${ userCompleteProfile.userProfileCoverImageInfo.userProfileCoverImageId }, ${ userCompleteProfile.userProfileId })">
                                                            <i class="fa fa-trash" aria-hidden="true"
                                                            	title="Delete"></i>
                                                            </button> --%>
                                                      </c:when>
                                                      <c:otherwise>
                                                         <div class="col-md-4">No Preview Video
                                                            Available. Please upload Preview Video.
                                                         </div>
                                                      </c:otherwise>
                                                   </c:choose>
                                                </td>
                                                <td>
                                                   <c:choose>
                                                      <c:when
                                                         test="${ not empty trainerCourseChapterLesson.trainerCourseChapterLessonMainVideoVimeoName }">
                                                         <iframe
                                                            src="https://player.vimeo.com/video/${ trainerCourseChapterLesson.trainerCourseChapterLessonMainVideoVimeoName }"
                                                            width="100%" height="100px" frameborder="0"
                                                            webkitallowfullscreen mozallowfullscreen
                                                            allowfullscreen></iframe>
                                                         <%-- <button class="btn delete-button" data-title="Delete"
                                                            onclick="deleteCoverImage(${ userCompleteProfile.userProfileCoverImageInfo.userProfileCoverImageId }, ${ userCompleteProfile.userProfileId })">
                                                            <i class="fa fa-trash" aria-hidden="true"
                                                            	title="Delete"></i>
                                                            </button> --%>
                                                      </c:when>
                                                      <c:otherwise>
                                                         <div class="col-md-4">No Actual Video Available.
                                                            Please upload Actual Video.
                                                         </div>
                                                      </c:otherwise>
                                                   </c:choose>
                                                </td>
                                                <td class="center-align">
                                                   <a href="trainersCourseChapterLessonUpdate/${ trainerCourseChapterLesson.trainerCourseChapterLessonId }"><i class="fa fa-edit"></i></a> 
                                                   <a href="#" onclick="deleteTrainersCourseChapterLesson(${ trainerCourseChapterLesson.trainerCourseChapterLessonId })"><i class="fa fa-trash" title="Delete"></i></a>
                                                </td>
                                             </tr>
                                          </c:forEach>
                                       </c:when>
                                    </c:choose>
                                 </tbody>
                              </table>
                           </div>
                           <!-- card body -->
                        </div>
                        <!-- Card -->
                     </div>
                     <!-- md12  -->
                  </div>
               </div>
               <!-- .animated -->
            </div>
            <!-- .content -->
         </div>
         <!-- .content -->
      </div>
      <!-- /#right-panel -->
      <%@ include file="/WEB-INF/handlingJsps/trainerLowerFooter.jsp"%>
      <script type="text/javascript">
         function deleteTrainersCourseChapterLesson(id) {
        	 swal({
           	  title: 'Are you sure?',
           	  text: "Course Chapter Lesson will permanently deleted !",
           	  type: 'warning',
           	  showCancelButton: true,
           	  confirmButtonColor: '#3085d6',
           	  cancelButtonColor: '#d33',
           	  confirmButtonText: 'Yes, delete it!',
           	  closeOnConfirm: false
          	},
         		function() {
           	$.ajax({
                   type: "POST",
                   url: "deleteTrainersCourseChapterLesson",
                   data: "trainerCourseChapterLessonId="+id,
                   success: function (result) {
                   }
               })
               .done(function(data) {
              swal("Deleted!", "Course Chapter Lesson deleted successfully!", "success");
              location.reload();
            })
            .error(function(data) {
              swal("Oops", "We couldn't connect to the server!", "error");
            });
         	});
         }
      </script>
   </body>
</html>