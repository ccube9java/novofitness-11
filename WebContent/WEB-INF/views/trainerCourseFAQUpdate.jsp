<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
<head>
<title>NovoFitness Trainer Courses</title>
<%@ include file="/WEB-INF/handlingJsps/trainerTopHeader.jsp"%>
<%@ include file="/WEB-INF/handlingJsps/trainerSession.jsp"%>
<script src="assets/ckeditor/ckeditor.js"></script>
</head>
<body>
	<!-- Left Panel Sidebar-->
	<%@ include file="/WEB-INF/handlingJsps/trainerSidebar.jsp"%>
	<div id="right-panel" class="right-panel">
		<!-- Right Panel Header-->
		<%@ include file="/WEB-INF/handlingJsps/trainerHeader.jsp"%>
		<div class="breadcrumbs">
			<div class="col-sm-4">
				<div class="page-header float-left">
					<div class="page-title">
						<h1>Course FAQ Update</h1>
					</div>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="page-header float-right">
					<div class="page-title">
						<ol class="breadcrumb text-right">
							<li><a href="trainersCourseView/${ trainerCourseFAQDetails.trainerCourseInfo.trainerCourseId }"><b>Back</b></a></li>
						</ol>
					</div>
				</div>
			</div>
		</div>
		<div class="content mt-3">
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						<strong>Course FAQ</strong>
						<c:if test="${ not empty alertSuccessMessage }">
							<div class="alert alert-success">${ alertSuccessMessage }</div>
						</c:if>
						<c:if test="${ not empty alertSuccessMessageForUpdatingCourseFAQ }">
							<div class="alert alert-success">${ alertSuccessMessageForUpdatingCourseFAQ }</div>
						</c:if>
						<c:if test="${ not empty alertFailMessage }">
							<div class="alert alert-danger">${ alertFailMessage }</div>
						</c:if>
						<c:if test="${ not empty alertFailMessageForUpdatingCourseFAQ }">
							<div class="alert alert-danger">${ alertFailMessageForUpdatingCourseFAQ }
							</div>
						</c:if>
					</div>
					<div class="card-body card-block">
						<form:form action="updateTrainerCourseFAQ"
							modelAttribute="trainerCourseFAQWrapper">
							<div class="row">
								<div class="col-md-5">
                              <div class="form-group">
                                 <label for="courseName" class=" form-control-label">
                                 FAQ Question <span class="error">*</span>
                                 </label>
                                 <!-- <textarea class="form-control" rows="2" id="comment"></textarea> -->
                                 <form:hidden path="trainerCourseFAQId"
                                    value="${ trainerCourseFAQDetails.trainerCourseFAQId }" />
                                    <textarea class="form-control" name="trainerCourseFAQQuestion" rows="2" id="comment">${ trainerCourseFAQDetails.trainerCourseFAQQuestion }</textarea>
                               <%--   <form:textarea path="trainerCourseFAQQuestion" class="form-control" rows="2" /> --%>
                                 <form:errors path="trainerCourseFAQQuestion" class="error" />
                              </div>
                           </div>
                           <div class="col-md-5">
                              <div class="form-group">
                                 <label for="courseName" class=" form-control-label">
                                 FAQ Answer <span class="error">*</span>
                                 </label>
                                 <textarea class="form-control" name="trainerCourseFAQAnswer" rows="2" id="comment">${ trainerCourseFAQDetails.trainerCourseFAQAnswer }</textarea>
                                 <%-- <form:textarea path="trainerCourseFAQAnswer" class="form-control" rows="2" /> --%>
                                 <form:errors path="trainerCourseFAQAnswer" class="error" />
                                 <!-- <textarea class="form-control" rows="2" id="comment"></textarea> -->
                              </div>
                           </div>
                           <div class="col-md-2">
                              <div class="form-group">
                                 <button type="submit"
                                    class="btn btn-success btn-sm btn-add-questions">Update FAQ</button>
                              </div>
                           </div>
							</div>
							<!-- rows -->
						</form:form>
					</div>
				</div>
				<!-- .animated -->
			</div>
			<!-- .content -->
		</div>
		<!-- .content -->
	</div>
	<!-- /#right-panel -->
	<%@ include file="/WEB-INF/handlingJsps/trainerLowerFooter.jsp"%>
</body>
</html>