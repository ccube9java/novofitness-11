<!DOCTYPE html>
<html lang="zxx">
<head>
<!-- top header -->
<title>NovoFitness</title>
<%@ include file="/WEB-INF/handlingJsps/userTopHeader.jsp"%>
<%@ include file="/WEB-INF/handlingJsps/userSession.jsp"%>
<!-- close top header -->
</head>
<body>
	<div class="page_loader"></div>
	<!-- Top header start -->
	<%@ include file="/WEB-INF/handlingJsps/userHeader.jsp"%>
	<!-- Top header end -->
	<!-- Sub banner start -->
	<section class="sub-banner">
		<div class="container">
			<div class="row">
				<div class="breadcrumb-area">
					<div class="headet-txt">
						<h1>
							THE FUTURE OF<br> <span> FITNESS </span> and <span>HEALTH</span>
						</h1>
					</div>
					<p class="banner-txt">FIND your EXPERT now</p>
				</div>
			</div>
		</div>
	</section>
	<section id="tabs">
		<div class="about-institute">
			<h6 class="section-title h1">My Courses List</h6>
			<div class="row tab-details">
				<div class="navbar navbar-light bg-faded"></div>
			</div>
			<div class="tab-content">
				<div class="tab-pane active" id="nav-All">
					<div class="about-institute content-area-userpurchase">
						<div class="container-fluid" id="allCategories">
							<!-- <div class="list-heading-tabs">
                           <h3>All</h3>
                           </div> -->
							<c:choose>
								<c:when test="${ not empty userPurchasedCourseList }">
									<c:forEach items="${ userPurchasedCourseList }"
										var="userPurchasedCourse">
										<a class=""
											href="dXNlckNvdXJzZURlcnRhaWxz?dHJhaW5lckNvdXJzZUlk=${ userPurchasedCourse.trainerCourseInfo.trainerCourseId }&dHJhaW5lcklk=${ userPurchasedCourse.trainerCourseInfo.trainerInfo.trainerId }">
											<div
												class="col-md-3 col-lg-3 col-sm-6 col-xs-6 course-details">
												<div class="cart-over">
													<div class="card">
														<div class="course-img">
															<c:choose>
																<c:when
																	test="${ not empty userPurchasedCourse.trainerCourseInfo.trainerCourseInfo.courseCoverImageInfo }">
																	<img
																		src="${ userPurchasedCourse.trainerCourseInfo.trainerCourseInfo.courseCoverImageInfo.courseCoverImageViewPath }"
																		class="img-responsive">
																</c:when>
																<c:otherwise>
																	<img src="assets/userAssets/imges/course11.jpg"
																		class="img-responsive">
																</c:otherwise>
															</c:choose>
														</div>
														<!-- <div class="row">
                                                <div class="col-md-8">
                                                   <div class="textinfo ">
                                                      <h5>
                                                         <b>YOGA BOOTCAMP :</b>
                                                      </h5>
                                                   </div>
                                                </div>
                                                <div class="col-md-4">
                                                   <div class="time-show">
                                                      <p>3h 20min</p>
                                                   </div>
                                                </div>
                                                </div> -->
														<!--end of the row here--->
														<div class="row">
															<div class="col-md-12">
																<div class="course-name">
																	<h5>
																		<b>${ userPurchasedCourse.trainerCourseInfo.trainerCourseInfo.courseName }</b>
																	</h5>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<div class="textinfo ">
																	<h5>${ userPurchasedCourse.trainerCourseInfo.trainerCourseInfo.courseDescription }</h5>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<div class="textinfo ">
																	<h5>
																		Trainer : <b>${ userPurchasedCourse.trainerCourseInfo.trainerInfo.trainerFName }
																			${ userPurchasedCourse.trainerCourseInfo.trainerInfo.trainerLName }</b>
																	</h5>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-3">
																<!-- <div class="textinfo-2">
                                                      <h5>Rating :</h5>
                                                   </div> -->
															</div>
															<div class="col-md-5">
																<!-- <div class="feedback-info ">
                                                      <ul class="rating list-inline">
                                                         <li><span class="stars" data-rating="3.0"
                                                            data-num-stars="5"> <i
                                                            class="fa fa-star review-rating-star"> </i><i
                                                            class="fa fa-star review-rating-star"> </i><i
                                                            class="fa fa-star review-rating-star"> </i><i
                                                            class="fa fa-star"></i> <i class="fa fa-star"> </i></span></li>
                                                      </ul>
                                                   </div> -->
															</div>
															<div class="col-md-4">
																<div class="ratio-show">
																	<!--  <p>4,3/5 (20)</p> -->
																	<c:choose>
																		<c:when test="${ empty userSession }">
																			<%-- <fmt:message key="label.addToFav" var="addToFav"/> --%>
																			<i class="fa fa-heart watch-list open-watchlistLogin"
																				aria-hidden="true"
																				data-id="${ userPurchasedCourse.trainerCourseInfo.trainerCourseId }"
																				title="<fmt:message key="label.addToFav"/>"></i>
																			<%-- <i class="fa fa-heart watch-list open-watchlistLogin"
                                                               aria-hidden="true"
                                                               data-id="${ userfavouriteTrainerCourse.trainerCourseInfo.trainerCourseId }"
                                                               title="<fmt:message key="label.addToFav"/>"
                                                               data-toggle="modal"
                                                               data-target="#addFavLoginModal"></i> --%>
																		</c:when>
																		<%-- <c:otherwise>
                                                            <c:choose>
                                                               <c:when
                                                                  test="${ userPurchasedCourse.favouriteTrainerCourseUserIsActive }">
                                                                  <c:set var="watchListClass" value="fa-trash watch-list-in"></c:set>
                                                                  <fmt:message key="label.removeFromFav"
                                                                     var="removeFromFav" />
                                                                  <c:set var="watchListTitle"
                                                                     value="${ removeFromFav }"></c:set>
                                                               </c:when>
                                                               <c:otherwise>
                                                                  <c:set var="watchListClass" value="fa-heart watch-list"></c:set>
                                                                  <fmt:message key="label.addToFav" var="addToFav" />
                                                                  <c:set var="watchListTitle" value="${ addToFav }"></c:set>
                                                               </c:otherwise>
                                                            </c:choose>
                                                             <a onclick="userWatchListAddRemove(${ userPurchasedCourse.trainerCourseInfo.trainerCourseId }, ${ userSession.userId }, ${ userPurchasedCourse.favouriteTrainerCourseUserIsActive })">
                                                            <i class="fa fa-heart ${ watchListClass }"
                                                               aria-hidden="true" title="${ watchListTitle }"
                                                               onclick=""></i>
                                                            </a>
                                                         </c:otherwise> --%>
																	</c:choose>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</a>
										<!---end of the col-md-3 here--->
									</c:forEach>
								</c:when>
								<c:otherwise>
									<!-- <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6 course-details"> -->
									<div class="courseslogin">
										<div class="courseslogins">
											<a href="userHome"><button type="button"
													class="btn btn-default btnaddedcourses">You do not have any courses in this list, go here</button></a>
										</div>

									</div>
									<!-- </div> -->
								</c:otherwise>
							</c:choose>
						</div>
						<!--end of container-fluid here--->
					</div>
				</div>
				<!--end of the nav-All here--->
			</div>
		</div>
		<!-- ./Tabs -->
	</section>
	<!---sports elearning section end here--->
	<!-- Footer start -->
	<%@ include file="/WEB-INF/handlingJsps/userFooter.jsp"%>
	<!-- Footer end -->
	<%@ include file="/WEB-INF/handlingJsps/userLowerFooter.jsp"%>
	<!--  <script>
         function openNav() {
         	/* var x = document.getElementById("mySidenav");
         	if (x.style.display === "none") {
         	 x.style.display = "block";
         	 x.classList.add('active');
         	
         	} else {
         	 x.style.display = "none";
         	  
         	}*/
         	document.getElementById("mySidenav").style.display = "block";
         
         }
         
         function closeNav() {
         	document.getElementById("mySidenav").style.display = "none";
         
         }
      </script> -->
	<!--      <script type="text/javascript">
         $("#userLoginWrapper").submit(function(e) {
         	//alert("hi..");
         	$.ajax({
         		url : 'userLoginForFavouriteList',
         		type : 'post',
         		data : $(this).serialize(),
         		success : function(response) {
         			//alert('success::'+response);
         			if (response == 'error') {
         				alert("Please Enter valid Data");
         			} else if (response == 'fails') {
         				alert("Login Details are not matched");
         			} else if (response == 'success') {
         				alert("Location added to your Favourites")
         			}
         			/*  $('#watchlistLoginModal').dialog({modal:true});    */
         			/*  $( "#watchlistLoginModal" ).dialog( "open" ); */
         			/* location.reload(); */
         		}
         	})
         });
      </script> -->
	<!-- <script type="text/javascript">
         $(document).on("click", ".open-watchlistLogin", function() {
         	var trainerCourseId = $(this).data('id');
         	$(".modal-body #trainerCourseId").val(trainerCourseId);
         	// As pointed out in comments, 
         	// it is unnecessary to have to manually call the modal.
         	// $('#addBookDialog').modal('show');
         	$('#categoryWiseData').hide();
         });
      </script> -->
	<script type="text/javascript">
		function userWatchListAddRemove(trainerCourseId, userId,
				watchListStatus) {
			//alert("hi..");
			var userWatchListData = {
				trainerCourseId : trainerCourseId,
				userId : userId,
				watchListStatus : watchListStatus
			}
			$
					.ajax({
						url : 'userWatchListAddRemove',
						type : 'post',
						data : userWatchListData,
						success : function(response) {
							//alert('success::'+response);
							if (response == 'added') {
								//alert("Course is added to your WatchList");
								swal({
									title : "Successfully Added!",
									text : "Course is added to your Favourite!",
									icon : "success",
								});
							} else if (response == 'fail') {
								//alert("Course is fail to update. Please try again");
								swal({
									title : "Failed!",
									text : "Course is fail to update. Please try again!",
									icon : "warning",
								});
							} else if (response == 'removed') {
								swal({
									title : "Successfully Removed!",
									text : "Course is removed to your Favourite!",
									icon : "success",
								});
								//alert("Course is removed from your WatchList")
							}
							location.reload();
						}
					});
		}
	</script>
	<!-- Custom javascript -->
</body>
</html>