<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>NovoFitness Trainer Dashboard</title>
      <%@ include file="/WEB-INF/handlingJsps/trainerTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/trainerSession.jsp"%>
   </head>
   <body>
   		 <!-- Left Panel Sidebar-->
      <%@ include file="/WEB-INF/handlingJsps/trainerSidebar.jsp"%>
      <div id="right-panel" class="right-panel">
      <!-- Right Panel Header-->
         <%@ include file="/WEB-INF/handlingJsps/trainerHeader.jsp"%>
         <div class="breadcrumbs">
            <div class="col-sm-4">
               <div class="page-header float-left">
                  <div class="page-title">
                     <h1>Dashboard</h1>
                  </div>
               </div>
            </div>
            <div class="col-sm-8">
               <div class="page-header float-right">
                  <div class="page-title"></div>
               </div>
            </div>
         </div>
         <div class="content mt-3">
            <%-- <div class="col-sm-6 col-lg-3">
               <div class="card text-white bg-flat-color-1">
                  <div class="card-body pb-0">
                     <h4 class="mb-0">
                        <span class="count">${ courseCount }</span>
                     </h4>
                     <p class="text-light">Courses</p>
                     <div class="chart-wrapper px-0" style="height:70px;" height="70">
                        <canvas id="widgetChart1"></canvas>
                     </div>
                  </div>
               </div>
            </div> --%>
            <!--/.col-->
            <div class="col-sm-6 col-lg-3">
               <div class="card text-white bg-flat-color-2">
                  <div class="card-body pb-0">
                     <h4 class="mb-0">
                        <span class="count">${ courseCount }</span>
                     </h4>
                     <p class="text-light"> My Courses </p>
                     <div class="chart-wrapper px-0" style="height:70px;" height="70">
                        <canvas id="widgetChart2"></canvas>
                     </div>
                  </div>
               </div>
            </div>
            <!--/.col-->
            <div class="col-sm-6 col-lg-3">
               <div class="card text-white bg-flat-color-3">
                  <div class="card-body pb-0">
                     <h4 class="mb-0">
                        <span class="count">${ trainerCourseReviewCount }</span>
                     </h4>
                     <p class="text-light"> Reviews and Ratings  </p>
                  </div>
                  <div class="chart-wrapper px-0" style="height:70px;" height="70">
                     <canvas id="widgetChart3"></canvas>
                  </div>
               </div>
            </div>
            <!--/.col-->
            <div class="col-sm-6 col-lg-3">
               <div class="card text-white bg-flat-color-4">
                  <div class="card-body pb-0">
                     <h4 class="mb-0">
                        <span class="count">${ trainerCoursePaymentCount }</span>
                     </h4>
                     <p class="text-light">Payment Settlements </p>
                     <div class="chart-wrapper px-3" style="height:70px;" height="70">
                        <canvas id="widgetChart4"></canvas>
                     </div>
                  </div>
               </div>
            </div>
            <!--/.col-->
         </div>
         <!-- .content -->
      </div>
      <!-- /#right-panel -->
      <%@ include file="/WEB-INF/handlingJsps/trainerLowerFooter.jsp"%>
      <script>
         (function($) {
         	"use strict";
         
         	jQuery('#vmap').vectorMap({
         		map : 'world_en',
         		backgroundColor : null,
         		color : '#ffffff',
         		hoverOpacity : 0.7,
         		selectedColor : '#1de9b6',
         		enableZoom : true,
         		showTooltip : true,
         		values : sample_data,
         		scaleColors : [ '#1de9b6', '#03a9f5' ],
         		normalizeFunction : 'polynomial'
         	});
         })(jQuery);
      </script>
      <script>
         $('.myDiv').click(function() { //button click class name is myDiv
         	e.stopPropagation();
         })
         
         $(function() {
         	$('.openDiv').click(function() {
         		$('.myDiv').show();
         
         	});
         	$(document).click(function() {
         		$('.myDiv').hide(); //hide the button
         
         	});
         });
      </script>
   </body>
</html>