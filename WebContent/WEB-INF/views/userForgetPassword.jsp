<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!DOCTYPE html>
<html lang="zxx">
   <head>
      <!-- top header -->
      <title>
         NovoFitness 
         <fmt:message key="label.login" />
      </title>
      <%@ include file="/WEB-INF/handlingJsps/userTopHeader.jsp"%>
      <!-- close top header -->
      <style>
         .form-content-box form{
         height:auto;
         }
         .form-content-box .details {
         padding: 30px 30px;
         border-radius: 5px 5px 0 0;
         background: #f3f3fb;
         }
         .footer span a{
         color: #535353;
         }
         .btn-theme{
         background-color: #f59e01;
         }
      </style>
   </head>
   <body>
    <!-- Top header start -->
	<%@ include file="/WEB-INF/handlingJsps/userHeader.jsp"%>
	<!-- Top header end -->
      <!-- Content area start -->
      <div class="main-page">
      <div class="container">
         <div class="row">
            <div class="col-lg-12">
               <!-- Form content box start -->
               <div class="form-content-box">
                  <!-- logo -->
                  <!-- details -->
                  <div class="details">
                     <a href="index.html" class="clearfix alpha-logo">
                     <img src="assets/userAssets/imges/bglogo.png" alt="white-logo">
                     </a>
                     <h3>Forgot Password ?</h3>
                     <p>You can reset your password here.</p>
                     <c:if test="${ not empty alertSuccessMessage }">
                        <div class="alert alert-success">${ alertSuccessMessage }</div>
                     </c:if>
                     <c:if test="${ not empty alertSuccessMessagepasswordResetSuccess }">
                        <div class="alert alert-success"><fmt:message key="label.passwordResetSuccess" /></div>
                     </c:if>
                     <c:if test="${ not empty alertSuccessMessagepasswordResetLinkMessage }">
                        <div class="alert alert-success"><fmt:message key="label.passwordResetLinkMessage" /></div>
                     </c:if>
                     <c:if test="${ not empty alertFailMessageinvalidCredentials }">
                        <div class="alert alert-danger"><fmt:message key="label.invalidCredentials" /></div>
                     </c:if>
                     <c:if test="${ not empty alertFailMessageUserAccountVerificationFailed }">
                        <div class="alert alert-danger"><fmt:message key="label.userAccountVerificationFailed" /></div>
                     </c:if>
                     <c:if test="${ not empty alertFailMessageemailNotPresent }">
                        <div class="alert alert-danger"><fmt:message key="label.emailNotPresent" /></div>
                     </c:if>
                     <!-- Form start -->
                     <form:form action="forgotUserPassword" method="post" modelAttribute="userLoginWrapper">
                        <div class="form-group">
                         <fmt:message key="label.enterEmail" var="enterEmail"/>
                        	<form:input path="userEmail" class="input-text" placeholder="${ enterEmail }" />
                           <form:errors path="userEmail" class="error" />
                        </div>
                        <div class="mb-0">
                           <button type="submit" class="btn-md btn-theme btn-block"><fmt:message key="label.submit" /></button>
                        </div>
                     </form:form>
                     <!-- Form end -->
                  </div>
                  <!-- Footer -->
                  <div class="footer">
                     <span> Already member ? <a href="userLogin"><fmt:message key="label.login" /></a>
                     </span>
                  </div>
               </div>
               <!-- Form content box end -->
            </div>
         </div>
      </div>
      </div>
      <!-- Content area end -->
      <!-- Footer start -->
	<%@ include file="/WEB-INF/handlingJsps/userFooter.jsp"%>
	<%@ include file="/WEB-INF/handlingJsps/userLowerFooter.jsp"%>
	<!-- Footer end -->
   </body>
</html>