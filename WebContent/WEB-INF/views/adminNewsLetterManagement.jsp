<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>NovoFitness <fmt:message key="label.adminNewsLetterManagement" /></title>
      <%@ include file="/WEB-INF/handlingJsps/adminTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/adminSession.jsp"%>
      <c:choose>
         <c:when test="${ adminSession.adminRole.roleId == 2 }">
            <c:redirect url="adminDashboard" />
         </c:when>
      </c:choose>
   </head>
   <body>
      <!-- Left Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminSidebar.jsp"%>
      <!-- Left Panel -->
      <!-- Right Panel -->
      <div id="right-panel" class="right-panel">
         <!-- Header-->
         <%@ include file="/WEB-INF/handlingJsps/adminHeader.jsp"%>
         <!-- /header -->
         <div class="breadcrumbs">
            <div class="col-sm-4">
               <div class="page-header float-left">
                  <div class="page-title">
                     <h1>Manage Newsletter</h1>
                  </div>
               </div>
            </div>
            <div class="col-sm-4"></div>
         </div>
         <div class="content mt-3">
            <div class="animated fadeIn">
               <div class="row">
                  <div class="col-md-12">
                     <div class="card">
                        <div class="card-header">
                           <strong class="card-title">List Of Newsletter</strong>
                           <div class="right-logo">
                              <a href=""><img src="assets/adminAssets/images/pdf-512.png" class="src-img"></a> 
                              <a href=""><img class="src-img-2" src="assets/adminAssets/images/icons8-microsoft-excel-48.png"></a>
                           </div>
                        </div>
                        <div class="card-body" id="userlist">
                           <table id="bootstrap-data-table-export"
                              class="table table-striped table-bordered">
                              <thead>
                                 <tr>
                                    <th>
                                       <fmt:message key="label.srNo" />
                                    </th>
                                    <th>
                                       User Name
                                    </th>
                                    <th>
                                       <fmt:message key="label.email" />
                                    </th>
                                    <th>
                                       <fmt:message key="label.status" />
                                    </th>
                                    <th>
                                       <fmt:message key="label.action" />
                                    </th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <c:choose>
                                    <c:when test="${ not empty newsLetterList }">
                                       <c:forEach items="${ newsLetterList }" var="newsLetter" varStatus="loop">
                                          <tr>
                                             <td>${ loop.index+1 }</td>
                                             <td>${ newsLetter.newsLetterUserFName }</td>
                                             <td>${ newsLetter.newsLetterUserEmail }</td>
                                             <td><label class="switch">
                                             	<input type="checkbox" 
                                                ${ newsLetter.newsLetterUserIsActive?'checked':'' }
                                                onchange="changeNewsLetterStatus(this,${ newsLetter.newsLetterId }, ${ newsLetter.newsLetterUserIsActive} )" /> 
                                                <span class="slider round"></span></label>
                                             </td>
                                             
                                             <td>
                                             <i class="fa fa-newspaper-o" aria-hidden="true" data-toggle="modal" data-target="#myModal${ newsLetter.newsLetterId }"></i>
                                            <%--  <i class="fa fa-newspaper-o" onclick="openSendModal(${ newsLetter.newsLetterId }, '${ newsLetter.newsLetterUserEmail }')"></i> --%>
                                             </td>
                                          </tr>
                                       </c:forEach>
                                    </c:when>
                                 </c:choose>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- .animated -->
         </div>
         <!-- .content -->
      </div>
      
      <!-- Modal -->
  <c:choose>
                                    <c:when test="${ not empty newsLetterList }">
                                       <c:forEach items="${ newsLetterList }" var="newsLetter" varStatus="loop">
      <div class="modal fade" id="myModal${ newsLetter.newsLetterId }" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
           <h4 class="modal-title">Send Newsletter Notification</h4>
           <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
         <input type="hidden" value="${ newsLetter.newsLetterId }" id="newsletterId"/>
         <input type="text" value="${ newsLetter.newsLetterUserEmail }" disabled="disabled" id="newsletterEmail"/>
        <textarea class="form-control" rows="5" id="comment"></textarea>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sucess" data-dismiss="modal">Send</button>
        </div>
      </div>
    </div>
  </div>
  </c:forEach>
  </c:when>
  </c:choose>
      <!-- /#right-panel -->
      <!-- Right Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminLowerFooter.jsp"%>
      <!-- <script type="text/javascript">
         $(document).on("click", ".open-watchlistLogin", function() {
         	var trainerCourseId = $(this).data('id');
         	$(".modal-body #trainerCourseId").val(trainerCourseId);
         	// As pointed out in comments, 
         	// it is unnecessary to have to manually call the modal.
         	// $('#addBookDialog').modal('show');watch-list
         });
        function openSendModal(newsletterId, newsletterEmail) {
        	alert("hi."+ newsletterEmail);
        	$(".modal-body #newsletterId").val(newsletterId);
        	//$(".modal-body #newsletterEmail").val(newsletterEmail);
        	$("#myModal").modal();
		}
      </script> -->
      <script>
         $(document).ready(function() {
         	$('#datepicker2').datepicker({
         		uiLibrary : 'bootstrap'
         	});
         
         	$('#fa-calendar2').click(function() {
         		$("#datepicker2").focus();
         	});
         });
      </script>
      <script>
         $(document).ready(function() {
         	$('#datepicker3').datepicker({
         		uiLibrary : 'bootstrap'
         	});
         
         	$('#fa-calendar3').click(function() {
         		$("#datepicker3").focus();
         	});
         });
      </script>
        <script type="text/javascript">
         function changeNewsLetterStatus(checkboxElem,id,status) {
         	var userData = { 
         			newsLetterId : id,
         			newsLetterStatus : status
         		}
         	 $.ajax({
         	        type: "POST",
         	        url: "changeNewsLetterStatus",
         	        data: userData,
         	        success: function (result) {
         	        	if(result=="success"){
         	        		alert("NewsLetter status changed successfully.");
         	        	} 
         	        	if(result=="fail"){
         	        		alert("Fail to change NewsLetter status. Please try again.");
         	        	} 
         	        	location.reload();
         	        },
         	        error: function (result) {
         	        	location.reload();
         	        }
         	    });
         }
      </script>
   </body>
</html>