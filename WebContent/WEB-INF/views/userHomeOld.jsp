<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!DOCTYPE html>
<html lang="zxx">
   <head>
      <!-- top header -->
      <title>NovoFitness</title>
      <%@ include file="/WEB-INF/handlingJsps/userTopHeader.jsp"%>
      <!-- close top header -->
   </head>
   <body>
      <div class="page_loader"></div>
      <!-- Top header start -->
      <%@ include file="/WEB-INF/handlingJsps/userHeader.jsp"%>
      <!-- Top header end -->
      <!-- Sub banner start -->
      <section class="sub-banner">
         <div class="container">
            <div class="row">
               <div class="breadcrumb-area">
                  <div class="headet-txt">
                     <h1>search online top <span>courses</span></h1>
                  </div>
                  <div class="img-ares">
                     <img src="assets/userAssets/imges/play-video.png">
                  </div>
                  <div class="inline-buttons">
                     <p>
                        <button class="one-third first button">BROWSE COURSES</button>
                        <button class="one-third second button">BECOME A TRAINER</button>
                     </p>
                  </div>
                  <p class="banner-txt">Already a novofitness member? sign in now</p>
               </div>
            </div>
         </div>
      </section>
      <!-- Sub Banner end -->
      <!-- About Institute start -->
      <div class="about-institute content-area-6">
         <div class="container" >
            <div class="list-heading">
               <h2>Most Popular Training Courses</h2>
            </div>
            <ul class="total-cards">
               <li class="card-elements-2">
                  <div class="card">
                     <div class="special-info">
                        <p><b>Strength building</b></p>
                     </div>
                     <img  src="assets/userAssets/imges/popular1.png" class="img-responsive">
                     <div class="textinfo ">
                        <h5>STRENGTH BUILDING</h5>
                     </div>
                     <div class="feedback-info ">
                        <img src="assets/userAssets/imges/rating.png">
                     </div>
                     <div class="textinfo ">
                        <h5>Price: $149</h5>
                     </div>
                  </div>
                  <!-- Inside video end -->
               </li>
               <li class="card-elements-2">
                  <!-- Inside video start  -->
                  <div class="card">
                     <div class="special-info">
                        <p><b>Balance training</b></p>
                     </div>
                     <img  src="assets/userAssets/imges/popular2.png" class="img-responsive">
                     <div class="textinfo ">
                        <h5>BALANCE TRAINING</h5>
                     </div>
                     <div class="feedback-info ">
                        <img src="assets/userAssets/imges/rating.png">
                     </div>
                     <div class="textinfo ">
                        <h5>Price: $179</h5>
                     </div>
                  </div>
                  <!-- Inside video end -->
               </li>
               <li class="card-elements-2">
                  <!-- Inside video start  -->
                  <div class="card">
                     <div class="special-info">
                        <p class="left-tem"><b>Flexibility training</b></p>
                     </div>
                     <img  src="assets/userAssets/imges/popular3.png" class="img-responsive">
                     <div class="textinfo ">
                        <h5>FLEXIBILITY TRAINING</h5>
                     </div>
                     <div class="feedback-info ">
                        <img src="assets/userAssets/imges/rating.png">
                     </div>
                     <div class="textinfo ">
                        <h5>Price: $199</h5>
                     </div>
                  </div>
                  <!-- Inside video end -->
               </li>
               <li class="card-elements-2">
                  <!-- Inside video start  -->
                  <div class="card">
                     <div class="special-info">
                        <p><b>Aerobic training</b></p>
                     </div>
                     <img  src="assets/userAssets/imges/popular4.png" class="img-responsive">
                     <div class="textinfo ">
                        <h5>AEROBIC TRAINING</h5>
                     </div>
                     <div class="feedback-info ">
                        <img src="assets/userAssets/imges/rating.png">
                     </div>
                     <div class="textinfo ">
                        <h5>Price: $219</h5>
                     </div>
                  </div>
                  <!-- Inside video end -->
               </li>
            <!--    <li class="card-elements-2">
                  Inside video start 
                  <div class="card">
                     <div class="special-info">
                        <p><b>Endurance  exercise</b></p>
                     </div>
                     <img  src="assets/userAssets/imges/popular5.png" class="img-responsive">
                     <div class="textinfo ">
                        <h5>ENDURANCE EXERCISE</h5>
                     </div>
                     <div class="feedback-info ">
                        <img src="assets/userAssets/imges/rating.png">
                        <header id="tg-header" class="tg-header tg-haslayout">
                           <nav class="navbar navbar-default">
                              <div class="container">
                                 <div class="row">
                                    BRAND
                                    <div class="col-md-3">
                                       <div class="navbar-header">
                                          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#alignment-example" aria-expanded="false">
                                          <span class="sr-only">Toggle navigation</span>
                                          <span class="icon-bar"></span>
                                          <span class="icon-bar"></span>
                                          <span class="icon-bar"></span>
                                          </button>
                                          <a class="navbar-brand" href="#">
                                          <img src="assets/userAssets/imges/bglogo.png">
                                          </a>
                                       </div>
                                    </div>
                                    <div class="" id="mySidenav">
                                       <div class="collapse navbar-collapse" id="alignment-example">
                                          <div class="col-md-4">
                                             <form class="" role="search">
                                                <div class="form-group">
                                                   <input type="text" class="form-control">
                                                   <i class="fa fa-search" aria-hidden="true"></i>
                                                </div>
                                             </form>
                                          </div>
                                          <div class="col-md-3">
                                             <ul class="nav navbar-nav">
                                                <li>
                                                   <button type="button" class="btn  dropdown-toggle" data-toggle="dropdown">
                                                      <span id="search_elements">CATEGORIES</span>  <span class="caret"></span>
                                                       </button>
                                                   <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">CATEGORIES
                                                   <span class="caret"></span></button>
                                                   <ul class="dropdown-menu">
                                                      <li><a href="#">Yoga</a></li>
                                                      <li><a href="#">Nutrition</a></li>
                                                      <li><a href="#">Meditation</a></li>
                                                      <li><a href="#">Health and Fitness</a></li>
                                                   </ul>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="col-md-4">
                                             <ul class="navbar-form navbar-right" role="search">
                                                <li class="">
                                                   <a href>LOG IN</a>
                                                </li>
                                                <li class="searchNavDvider"></li>
                                                <li class="search-navigation">
                                                   <a href="#"><span class="">
                                                   SIGN UP</span>
                                                   </a>
                                                </li>
                                                <li>
                                                   <a href="#"><span class="">
                                                   <img src="assets/userAssets/imges/cart.png"></span>
                                                   </a>
                                                </li>
                                                <li class="search-navigation">
                                                   <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                                                </li>
                                             </ul>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                    </div>
                                    <div class="col-md-3">
                                       <ul class="caret-elements">
                                          <li >
                                             <a href="#"><span class="">
                                             <img src="assets/userAssets/imges/cart.png"></span>
                                             </a> 
                                          </li>
                                          <li ><span  onclick="openNav()"><img class="opennav" src="assets/userAssets/imges/caretimg.png"></span></li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </nav>
                        </header>
                     </div>
                     <div class="textinfo ">
                        <h5>Price: $219</h5>
                     </div>
                  </div>
                  Inside video end
               </li> -->
            </ul>
         </div>
      </div>
      <!---end of the popular training course section here-->
      <!---sports elearning section here--->
      <!-- Our facilties section start -->
      <div class="elearning-elements">
         <div class="container">
            <div class="row learning-elements">
               <!-- Main title -->
               <div class="">
                  <div class="sportelearning ">
                     <h2>Why Sports E-Learning?</h2>
                     <p>Four Reasons you should join Sports E-Learning</p>
                  </div>
                  <div class="col-md-3 col-sm-12">
                     <!-- Inside video start  -->
                     <div class="card">
                        <div class="row">
                           <div class="col-md-4 sideimg">
                              <img src="assets/userAssets/imges/ico1.png" class="img-responsive">
                           </div>
                           <div class="col-md-8">
                              <div class=" " id="textinfo">
                                 <h4><b>Workout anytime, anywehere</b></h4>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- Inside video end -->
                  </div>
                  <div class="col-md-3 col-sm-12">
                     <!-- Inside video start  -->
                     <div class="card">
                        <div class="row">
                           <div class="col-md-4 sideimg">
                              <img src="assets/userAssets/imges/ico2.png" class="img-responsive">
                           </div>
                           <div class="col-md-8">
                              <div class=" " id="textinfo">
                                 <h4><b>Guidance by our master trainers</b></h4>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- Inside video end -->
                  </div>
                  <div class="col-md-3 col-sm-12">
                     <!-- Inside video start  -->
                     <div class="card">
                        <div class="row">
                           <div class="col-md-4 sideimg">
                              <img src="assets/userAssets/imges/ico3.png" class="img-responsive">
                           </div>
                           <div class="col-md-8">
                              <div class=" " id="textinfo">
                                 <h4><b>Goal based Workouts</b></h4>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- Inside video end -->
                  </div>
                  <div class="col-md-3 col-sm-12">
                     <!-- Inside video start  -->
                     <div class="card">
                        <div class="row">
                           <div class="col-md-4 sideimg">
                              <img src="assets/userAssets/imges/ico4.png" class="img-responsive">
                           </div>
                           <div class="col-md-8">
                              <div class=" " id="textinfo">
                                 <h4><b>Quick Workouts,<br>at home</b></h4>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- Inside video end -->
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!---sports elearning section end here--->
      <div class="Workouts-sections">
         <div class="container">
            <div class="row">
               <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                  <div class="Workouts-captions">
                     <h1>WORKOUT</h1>
                     <h2><span>ANYTIME, ANWHERE</span></h2>
                     <button class="one-third first button">BROWSE COURSES</button>
                  </div>
               </div>
               <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                  <div class="fitness-img">
                     <img src="assets/userAssets/imges/welcome-pic.png" class="img-responsive">
                  </div>
               </div>
            </div>
            <!--end of the row here--->
         </div>
      </div>
      <!-- Our facilties section start -->
      <div class="view-courseelements" id="view-course">
         <div class="container">
            <div class="mostview-course">
               <h2 >Most Viewed Courses</h2>
               <div class="row">
                  <div class="col-md-6 col-lg-6 ">
                     <div class="row card-elements">
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                           <div class="avatar">
                              <img src="assets/userAssets/imges/most1.jpg" alt="avatar-2" class="">
                           </div>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                           <div class="yoga-txt">
                              <h2>Yoga</h2>
                           </div>
                           <div class="testimonials-info">
                              <div class="text">
                                 <p>
                                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque lauatium, totam rem aperiam. eaqesa quae ab illo inventore veritatis et quasi architecto 
                                    beatae.
                                 </p>
                                 <h5><b>More about yoga</b></h5>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6 col-lg-6 ">
                     <div class="row card-elements">
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                           <div class="avatar">
                              <img src="assets/userAssets/imges/most2.jpg" alt="avatar-2" class="">
                           </div>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                           <div class="yoga-txt">
                              <h2>Nutrition</h2>
                           </div>
                           <div class="testimonials-info">
                              <div class="text">
                                 <p>
                                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque lauatium, totam rem aperiam. eaqesa quae ab illo inventore veritatis et quasi architecto 
                                    beatae.
                                 </p>
                                 <h5><b>More about Nutrition</b></h5>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-6 col-lg-6 ">
                     <div class="row card-elements">
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                           <div class="avatar">
                              <img src="assets/userAssets/imges/most3.jpg" alt="avatar-2" class="">
                           </div>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                           <div class="yoga-txt">
                              <h2>Meditation</h2>
                           </div>
                           <div class="testimonials-info">
                              <div class="text">
                                 <p>
                                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque lauatium, totam rem aperiam. eaqesa quae ab illo inventore veritatis et quasi architecto 
                                    beatae.
                                 </p>
                                 <h5><b>More about Meditation</b></h5>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6 col-lg-6 ">
                     <div class="row card-elements">
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                           <div class="avatar">
                              <img src="assets/userAssets/imges/most4.jpg" alt="avatar-2" class="">
                           </div>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                           <div class="yoga-txt">
                              <h2>Health and Fitness</h2>
                           </div>
                           <div class="testimonials-info">
                              <div class="text">
                                 <p>
                                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque lauatium, totam rem aperiam. eaqesa quae ab  illo inventore veritatis et quasi architecto 
                                    beatae.
                                 </p>
                                 <h5><b>More about Health and Fitness</b></h5>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <button class="one-third viewbtn button">View More</button>
            </div>
         </div>
      </div>
      <div class="blog-elements3">
         <div class="container">
            <div class="allcourses">
               <h2 style="">All Courses</h2>
            </div>
            <div class="row">
               <div class="col-md-3 col-sm-6 col-xs-6">
                  <div class="course-img">
                     <img src="assets/userAssets/imges/courses1.png">
                  </div>
                  <div class="course-caption">
                     <h5><b>YOGA</b></h5>
                  </div>
               </div>
               <div class="col-md-3 col-sm-6 col-xs-6">
                  <div class="course-img">
                     <img src="assets/userAssets/imges/courses2.png">
                  </div>
                  <div class="course-caption">
                     <h5><b>AEROBIC TRAINING</b></h5>
                  </div>
               </div>
               <div class="col-md-3 col-sm-6 col-xs-6">
                  <div class="course-img">
                     <img src="assets/userAssets/imges/courses3.png">
                  </div>
                  <div class="course-caption">
                     <h5><b>BALANCE TRAINING</b></h5>
                  </div>
               </div>
               <div class="col-md-3 col-sm-6 col-xs-6">
                  <div class="course-img">
                     <img src="assets/userAssets/imges/courses4.png">
                  </div>
                  <div class="course-caption">
                     <h5><b>FLEXIBILITY TRAINING</b></h5>
                  </div>
               </div>
            </div>
            <!---end of the trainig here--->
            <div class="row detail-course">
               <div class="col-md-3 col-sm-6 col-xs-6">
                  <div class="course-img">
                     <img src="assets/userAssets/imges/courses5.png">
                  </div>
                  <div class="course-caption">
                     <h5><b>MEDITATION</b></h5>
                  </div>
               </div>
               <div class="col-md-3 col-sm-6 col-xs-6">
                  <div class="course-img">
                     <img src="assets/userAssets/imges/courses6.png">
                  </div>
                  <div class="course-caption">
                     <h5><b>MARTIAL ARTS</b></h5>
                  </div>
               </div>
               <div class="col-md-3 col-sm-6 col-xs-6">
                  <div class="course-img">
                     <img src="assets/userAssets/imges/courses7.png">
                  </div>
                  <div class="course-caption">
                     <h5><b>STRENGTH BUILDING</b></h5>
                  </div>
               </div>
               <div class="col-md-3 col-sm-6 col-xs-6">
                  <div class="course-img">
                     <img src="assets/userAssets/imges/courses8.png">
                  </div>
                  <div class="course-caption">
                     <h5><b>NUTRITION</b></h5>
                  </div>
               </div>
            </div>
            <!---end of the trainig here--->
         </div>
      </div>
      <!-- Our Courses related section start -->
      <section class="elearning-elements">
         <div class="container">
            <div class="row learning-elements">
               <!-- Main title -->
               <div class="text-center">
                  <div class="becomeateacher">
                     <h2 >Became  a teacher on novofitness </h2>
                  </div>
                  <div class="col-md-3 col-sm-12">
                     <!-- Inside video start  -->
                     <div class="card">
                        <div class="row">
                           <div class="col-md-4 sideimg">
                              <img src="assets/userAssets/imges/become1.png" class="img-responsive">
                           </div>
                           <div class="col-md-8">
                              <div class=" " id="textinfo">
                                 <h4><b>Teach on Novofitness</b></h4>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- Inside video end -->
                  </div>
                  <div class="col-md-3 col-sm-12">
                     <!-- Inside video start  -->
                     <div class="card">
                        <div class="row">
                           <div class="col-md-4 sideimg">
                              <img src="assets/userAssets/imges/become2.png" class="img-responsive">
                           </div>
                           <div class="col-md-8">
                              <div class=" " id="textinfo">
                                 <h4><b>Earn Money</b></h4>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- Inside video end -->
                  </div>
                  <div class="col-md-3 col-sm-12">
                     <!-- Inside video start  -->
                     <div class="card">
                        <div class="row">
                           <div class="col-md-4 sideimg">
                              <img src="assets/userAssets/imges/become3.png" class="img-responsive">
                           </div>
                           <div class="col-md-8">
                              <div class=" " id="textinfo">
                                 <h4><b>Share your Expertise</b></h4>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- Inside video end -->
                  </div>
                  <div class="col-md-3 col-sm-12">
                     <!-- Inside video start  -->
                     <div class="card">
                        <div class="row">
                           <div class="col-md-4 sideimg">
                              <img src="assets/userAssets/imges/become4.png" class="img-responsive">
                           </div>
                           <div class="col-md-8">
                              <div class=" " id="textinfo">
                                 <h4><b>Build your Following</b></h4>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- Inside video end -->
                  </div>
                  <button class="one-third trainerbtn button">Become a triner today</button>
               </div>
            </div>
         </div>
      </section>
      <!-- staff section start -->
      <div class="staff-section content-area">
         <div class="container">
            <!-- Main title -->
            <div class="partners-block">
               <div class="container">
                  <div class="student-speaking">
                     <h2>What Our Student Speak</h2>
                  </div>
                  <div class="row">
                     <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                        <div class="feedback-txt">
                           <p><span class="quotearro"><i class="fa fa-quote-left"></i></span>I believe in lifelong learning and Sports-Learning Online is a Great place to learn from experts,i've learned a lot and recommend it to all my friends.</p>
                           <div class="student-feedback">
                              <p>Robert Reoch(Yoga student)</p>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                        <div class="feedback-txt-2">
                           <p><span class="quotearro"><i class="fa fa-quote-left"></i></span>I believe in lifelong learning and Sports-Learning Online is a Great place to learn from experts,i've learned a lot and recommend it to all my friends.</p>
                           <div class="student-feedback">
                              <p>Gionee Di Noto (Yoga student)</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <section class="signing-elements">
         <div class="container">
            <div class="row learning-elements">
               <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                  <div class="list-heading">
                     <h2>Sports E-Learning-purpose is to transform access to sports education.</h2>
                  </div>
               </div>
               <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                  <div class="sign-area">
                     <div class="list-heading">
                        <p>Be the first to hear about our latest courses by signing up to our mailing list</p>
                     </div>
                     <fieldset>
                        <input type="text" name="email" placeholder="Your email addresse" class="form-control-sign">
                        <button type="submit" class="btnsign" name="submit">Sign up</button>
                     </fieldset>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Footer start -->
      <%@ include file="/WEB-INF/handlingJsps/userFooter.jsp"%>
      <!-- Footer end -->
      <%@ include file="/WEB-INF/handlingJsps/userLowerFooter.jsp"%>
      <script>
         function openNav() {
            /* var x = document.getElementById("mySidenav");
           if (x.style.display === "none") {
             x.style.display = "block";
             x.classList.add('active');
          
           } else {
             x.style.display = "none";
              
           }*/
           document.getElementById("mySidenav").style.display = "block";
           
         }
         
         function closeNav() {
           document.getElementById("mySidenav").style.display = "none";
           
         }
      </script>
      <!-- Custom javascript -->
   </body>
</html>