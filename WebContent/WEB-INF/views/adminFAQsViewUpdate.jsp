<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>
         NovoFitness 
         <fmt:message key="label.adminFAQsManagement" />
      </title>
      <%@ include file="/WEB-INF/handlingJsps/adminTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/adminSession.jsp"%>
      <c:choose>
         <c:when test="${ adminSession.adminRole.roleId == 2 }">
            <c:redirect url="adminDashboard" />
         </c:when>
      </c:choose>
   </head>
   <body>
      <!-- Left Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminSidebar.jsp"%>
      <!-- Left Panel -->
      <!-- Right Panel -->
      <div id="right-panel" class="right-panel">
         <!-- Header-->
         <%@ include file="/WEB-INF/handlingJsps/adminHeader.jsp"%>
         <!-- /header -->
         <div class="breadcrumbs">
            <div class="col-sm-4">
               <div class="page-header float-left">
                  <div class="page-title">
                     <h1>Edit FAQ</h1>
                  </div>
               </div>
            </div>
            <div class="col-sm-8">
               <div class="page-header float-right">
                  <div class="page-title">
                     <ol class="breadcrumb text-right">
                        <li><a href="adminFAQManagement"><b>Back</b></a></li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
         <div class="content mt-3">
            <div class="animated fadeIn">
               <div class="row">
                  <div class="col-md-12">
                     <div class="card">
                        <div class="card-header">
                          <!--  <strong>Edit FAQ</strong> -->
                           <c:if test="${ not empty alertSuccessMessage }">
                              <div class="alert alert-success">${ alertSuccessMessage }</div>
                           </c:if>
                           <c:if test="${ not empty alertSuccessMessageupdateFAQsSuccess }">
                              <div class="alert alert-success">
                                 <fmt:message key="label.updateFAQsSuccess" />
                              </div>
                           </c:if>
                           <c:if test="${ not empty alertFailMessage }">
                              <div class="alert alert-danger">${ alertFailMessage }</div>
                           </c:if>
                           <c:if test="${ not empty alertFailMessageupdateFAQsFail }">
                              <div class="alert alert-danger">
                                 <fmt:message key="label.updateFAQsFail" />
                              </div>
                           </c:if>
                        </div>
                        <div class="card-body card-block">
                           <form:form action="updateFAQs" method="post" modelAttribute="adminFAQsWrapper" >
                              <div class="row">
                                 <div class="col-lg-6 col-md-6 ">
                                    <div class="form-group">
                                       <label for="faqsQuestion" class="form-control-label">
                                          <fmt:message key="label.question" />
                                          <span class="error">*</span>
                                       </label>
                                       <form:hidden path="faqId" class="form-control"
                                          value="${ faqInfo.faqId }" />
                                       <form:textarea path="faqsQuestion" class="form-control" id="company" value="${ faqInfo.faqQuestion }" />
                                       <form:errors path="faqsQuestion" class="error" />
                                    </div>
                                 </div>
                                 <div class="col-lg-6 col-md-6 ">
                                    <div class="form-group">
                                       <label for="faqsAnswer" class=" form-control-label">
                                          <fmt:message key="label.answer" />
                                          <span class="error">*</span>
                                       </label>
                                       <form:textarea path="faqsAnswer" class="form-control" id="company" value="${ faqInfo.faqAnswer }" />
                                       <form:errors path="faqsAnswer" class="error" />
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-12">
                                    <button type="submit" class="btn btn-success btn-sm">
                                       <fmt:message key="label.adminUpdateFAQs" />
                                    </button>
                                 </div>
                              </div>
                           </form:form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- .animated -->
         </div>
         <!-- .content -->
      </div>
      <!-- Right Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminLowerFooter.jsp"%>
      <script>
         jQuery(document).ready(function() {
         	jQuery(".standardSelect").chosen({
         		disable_search_threshold : 10,
         		no_results_text : "Oops, nothing found!",
         		width : "100%"
         	});
         });
      </script>
   </body>
</html>