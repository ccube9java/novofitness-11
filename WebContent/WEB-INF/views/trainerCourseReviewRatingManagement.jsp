<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>NovoFitness Trainer Courses</title>
      <%@ include file="/WEB-INF/handlingJsps/trainerTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/trainerSession.jsp"%>
   </head>
   <body>
      <!-- Left Panel Sidebar-->
      <%@ include file="/WEB-INF/handlingJsps/trainerSidebar.jsp"%>
      <div id="right-panel" class="right-panel">
         <!-- Right Panel Header-->
         <%@ include file="/WEB-INF/handlingJsps/trainerHeader.jsp"%>
         <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1> Reviews and Ratings  </h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
            </div>
        </div>
         <div class="content mt-3">
            <div class="animated fadeIn">
               <div class="row">
                  <div class="col-md-12">
                     <div class="card">
                        <c:choose>
                           <c:when test="${ trainerSession.trainerIsApprovedByAdmin }">
                              <div class="card-header">
                                 <strong class="card-title">List Of Reviews and Ratings </strong>
                                 <c:if test="${ not empty alertSuccessMessage }">
                                    <div class="alert alert-success">${ alertSuccessMessage }</div>
                                 </c:if>
                                 <c:if test="${ not empty alertSuccessMessageForAddingNewCourse }">
                                    <div class="alert alert-success">${ alertSuccessMessageForAddingNewCourse }</div>
                                 </c:if>
                                 <c:if test="${ not empty alertFailMessage }">
                                    <div class="alert alert-danger">${ alertFailMessage }</div>
                                 </c:if>
                                 <c:if test="${ not empty alertFailMessageForAddingNewCourseOnlyCoverImageFailure }">
                                    <div class="alert alert-danger">
                                       ${ alertFailMessageForAddingNewCourseOnlyCoverImageFailure }
                                    </div>
                                 </c:if>
                                
                              </div>
                              <div class="card-body" id="userlist">
                                 <table id="trainerpanels"
                                    class="table table-striped table-bordered">
                                    <thead>
                                       <tr>
                                          <th>
                                             <fmt:message key="label.srNo" />
                                          </th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Course Name</th>
                                            <th>Email id </th>
                                            <th>Rating</th>
                                           <!-- <th>Action</th>-->
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                       <c:choose>
                                          <c:when test="${ not empty trainerCourseListWithReviewAndRating }">
                                             <c:forEach items="${ trainerCourseListWithReviewAndRating }" var="trainerCourseWithReviewAndRating" varStatus="loop">
                                                <tr>
                                           <td>${ loop.index+1 }</td>
                                            <td>${ trainerCourseWithReviewAndRating.userInfo.userFName }</td>
                                             <td>${ trainerCourseWithReviewAndRating.userInfo.userLName }</td>
                                             <td>${ trainerCourseWithReviewAndRating.trainerCourseInfo.trainerCourseInfo.courseName } </td>
                                            <td>${ trainerCourseWithReviewAndRating.userInfo.userEmail }</td>
                                            <td>
                                            <span class="stars" data-rating="${ trainerCourseWithReviewAndRating.userCourseStarRating }" data-num-stars="5" ></span> </td>
                                            <!---<td><button type="submit" class="btn btn-success btn-sm btn-aproved">Approve</button></td>-->
                                            <td class="center-align"><a href="trainerCourseReviewViewManagement/${ trainerCourseWithReviewAndRating.userCourseReviewId }"><i class="fa fa-eye"></i></a></td>
                                        </tr>
                                                
                                             </c:forEach>
                                          </c:when>
                                       </c:choose>
                                    </tbody>
                                 </table>
                              </div>
                           </c:when>
                           <c:when test="${ !trainerSession.trainerIsApprovedByAdmin }">
                              <div class="card-header">
                                 <strong class="card-title">Wait still Admin will approved your account.</strong>
                              </div>
                           </c:when>
                        </c:choose>
                     </div>
                  </div>
               </div>
            </div>
            <!-- .animated -->
         </div>
         <!-- .content -->
      </div>
      <!-- /#right-panel -->
      <%@ include file="/WEB-INF/handlingJsps/trainerLowerFooter.jsp"%>
      <script type="text/javascript">
         $(function(){
            $('.stars').stars();
         }); 
      </script>
      <!-- <script>
         (function($) {
         	"use strict";
         
         	jQuery('#vmap').vectorMap({
         		map : 'world_en',
         		backgroundColor : null,
         		color : '#ffffff',
         		hoverOpacity : 0.7,
         		selectedColor : '#1de9b6',
         		enableZoom : true,
         		showTooltip : true,
         		values : sample_data,
         		scaleColors : [ '#1de9b6', '#03a9f5' ],
         		normalizeFunction : 'polynomial'
         	});
         })(jQuery);
         </script> -->
      <!--  <script>
         $('.myDiv').click(function() { //button click class name is myDiv
         	e.stopPropagation();
         })
         
         $(function() {
         	$('.openDiv').click(function() {
         		$('.myDiv').show();
         
         	});
         	$(document).click(function() {
         		$('.myDiv').hide(); //hide the button
         
         	});
         });
         </script> -->
      <script type="text/javascript">
         function deleteTrainerCourse(id) {
          var courseData = { 
         			trainerCourseId : id,
         	}
         	$.ajax({
                 type: "POST",
                 url: "deleteTrainerCourse",
                 data: courseData,
                 success: function (result) {
                	 /* alert(result); */
                	if(result=="success") {
                		swal({
                			  title: "Course deleted successfully!",
                			  text: "",
                			  icon: "success",
                			});
                		//swal("Course deleted successfully.", " ",  "success");
               		} 
               		if(result=="fail")  {
               			swal({
              			  title: "Fail to delete Course!",
              			  text: "Please try again",
              			  icon: "error",
              			});
               			//swal("Fail to delete Course.", "Please try again",  "error");
               			//alert("Fail to delete Course. Please try again ");
               		} 
                 	//location.reload();
                 },
                 error: function (result) {
                	// alert(result);
                 	//location.reload();
                 }
             }); 
         }
      </script>
   </body>
</html>