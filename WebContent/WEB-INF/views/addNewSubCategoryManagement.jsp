<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>
         NovoFitness 
         <fmt:message key="label.subCategory" />
      </title>
      <%@ include file="/WEB-INF/handlingJsps/adminTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/adminSession.jsp"%>
      <c:choose>
         <c:when test="${ adminSession.adminRole.roleId == 2 }">
            <c:redirect url="adminDashboard" />
         </c:when>
      </c:choose>
   </head>
   <body>
      <!-- Left Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminSidebar.jsp"%>
      <!-- Left Panel -->
      <!-- Right Panel -->
      <div id="right-panel" class="right-panel">
         <!-- Header-->
         <%@ include file="/WEB-INF/handlingJsps/adminHeader.jsp"%>
         <!-- /header -->
         <div class="breadcrumbs">
            <div class="col-sm-4">
               <div class="page-header float-left">
                  <div class="page-title">
                     <h1>
                        <fmt:message key="label.addNewSubCategory" />
                     </h1>
                  </div>
               </div>
            </div>
            <div class="col-sm-8">
               <div class="page-header float-right">
                  <div class="page-title">
                     <ol class="breadcrumb text-right">
                        <li><a href="adminSubCategoryManagement"><b>Back</b></a></li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
         <div class="content mt-3">
            <div class="animated fadeIn">
               <div class="row">
                  <div class="col-md-12">
                     <div class="card">
                        <div class="card-header">
                           <strong>
                              <fmt:message key="label.addNewSubCategory" />
                           </strong>
                           <c:if test="${ not empty alertSuccessMessage }">
                              <div class="alert alert-success">${ alertSuccessMessage }</div>
                           </c:if>
                           <c:if
                              test="${ not empty alertSuccessMessageaddNewSubCategorySuccess }">
                              <div class="alert alert-success">
                                 <fmt:message key="label.addNewSubCategorySuccess" />
                              </div>
                           </c:if>
                           <c:if test="${ not empty alertFailMessage }">
                              <div class="alert alert-danger">${ alertFailMessage }</div>
                           </c:if>
                           <c:if
                              test="${ not empty alertFailMessageaddNewSubCategoryFail }">
                              <div class="alert alert-danger">
                                 <fmt:message key="label.addNewSubCategoryFail" />
                              </div>
                           </c:if>
                        </div>
                        <div class="card-body card-block">
                           <form:form action="createNewSubCategory" method="post"
                              modelAttribute="adminSubCategoryWrapper">
                              <div class="row">
                                 <div class="col-lg-6 col-md-6 ">
                                    <div class="form-group">
                                       <label for="MainCategory" class=" form-control-label">
                                          <fmt:message
                                             key="label.selectMainCategory" />
                                          <span class="error">*</span>
                                       </label>
                                       <form:select path="mainCategoryId" id="selectLg"
                                          class="form-control-lg form-control">
                                          <form:option value="">
                                             <fmt:message key="label.selectMainCategory" />
                                          </form:option>
                                          <c:choose>
                                             <c:when test="${not empty categoryList}">
                                                <c:forEach items="${ categoryList }" var="category">
                                                   <form:option value="${ category.categoryId }" >${ category.categoryName }</form:option>
                                                </c:forEach>
                                             </c:when>
                                          </c:choose>
                                       </form:select>
                                       <form:errors path="mainCategoryId" class="error" />
                                    </div>
                                 </div>
                                 <div class="col-lg-6 col-md-6 ">
                                    <div class="form-group">
                                       <label for="subCategoryName" class=" form-control-label">
                                          <fmt:message
                                             key="label.subCategoryName" />
                                          <span class="error">*</span>
                                       </label>
                                       <form:input path="subCategoryName" class="form-control"
                                          id="company" placeholder="Enter Sub Category Name" />
                                       <form:errors path="subCategoryName" class="error" />
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-6 col-md-6 ">
                                    <div class="form-group">
                                       <label for="subcategoryDescription"
                                          class="form-control-label">
                                          <fmt:message
                                             key="label.description" />
                                          <span class="error">*</span>
                                       </label>
                                       <form:input path="subCategoryDescription"
                                          class="form-control" id="company"
                                          placeholder="Enter Description" />
                                       <form:errors path="subCategoryDescription" class="error" />
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-12">
                                    <button type="submit" class="btn btn-success btn-sm">
                                       <fmt:message key="label.addNewSubCategory" />
                                    </button>
                                 </div>
                              </div>
                           </form:form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- .animated -->
         </div>
         <!-- .content -->
      </div>
      <!-- Right Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminLowerFooter.jsp"%>
      <script>
         jQuery(document).ready(function() {
         	jQuery(".standardSelect").chosen({
         		disable_search_threshold : 10,
         		no_results_text : "Oops, nothing found!",
         		width : "100%"
         	});
         });
      </script>
   </body>
</html>