<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>NovoFitness Trainer Courses</title>
      <%@ include file="/WEB-INF/handlingJsps/trainerTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/trainerSession.jsp"%>
      <script src="assets/ckeditor/ckeditor.js"></script>
   </head>
   <body>
      <!-- Left Panel Sidebar-->
      <%@ include file="/WEB-INF/handlingJsps/trainerSidebar.jsp"%>
      <div id="right-panel" class="right-panel">
         <!-- Right Panel Header-->
         <%@ include file="/WEB-INF/handlingJsps/trainerHeader.jsp"%>
         <div class="breadcrumbs">
            <div class="col-sm-4">
               <div class="page-header float-left">
                  <div class="page-title">
                     <h1>Course Chapter View Update</h1>
                  </div>
               </div>
            </div>
            <div class="col-sm-8">
               <div class="page-header float-right">
                  <div class="page-title">
                     <ol class="breadcrumb text-right">
                        <li><a href="trainersCourseChapterView/${ trainerCourseChapterLessonDetails.trainerCourseChapterInfo.trainerCourseChapterId }"><b>Back</b></a></li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
         <div class="content mt-3">
            <div class="animated fadeIn">
               <div class="card">
                  <div class="card-header">
                     <strong>Course Chapter</strong>
                     <c:if test="${ not empty alertSuccessMessage }">
                        <div class="alert alert-success">${ alertSuccessMessage }</div>
                     </c:if>
                     <c:if test="${ not empty alertSuccessMessageForUpdatingCourse }">
                        <div class="alert alert-success">${ alertSuccessMessageForUpdatingCourse }</div>
                     </c:if>
                     <c:if test="${ not empty alertFailMessage }">
                        <div class="alert alert-danger">${ alertFailMessage }</div>
                     </c:if>
                     <c:if test="${ not empty alertFailMessageForUpdatingCourse }">
                        <div class="alert alert-danger">${ alertFailMessageForUpdatingCourse }
                        </div>
                     </c:if>
                  </div>
                  <div class="card-body card-block">
                     <form:form action="updateTrainerCourseChapterLesson" method="post"
                        modelAttribute="trainerCourseChapterLessonWrapper"
                        enctype="multipart/form-data">
                        <div class="row">
                           <div class="col-lg-4 col-md-4">
                              <div class="form-group">
                                 <label for="courseName" class=" form-control-label">
                                 Chapter Name <span class="error">*</span>
                                 </label>
                                 <form:hidden path="trainerCourseChapterLessonId"
                                    value="${ trainerCourseChapterLessonDetails.trainerCourseChapterLessonId }" />
                                 <form:input path="trainerCourseChapterName"
                                    class="form-control" id="company" disabled="true"
                                    value="${ trainerCourseChapterLessonDetails.trainerCourseChapterInfo.trainerCourseChapterName }" />
                                 <form:errors path="trainerCourseChapterName" class="error" />
                              </div>
                           </div>
                           <div class="col-lg-4 col-md-4">
                              <div class="form-group">
                                 <label for="courseName" class=" form-control-label">
                                 Lesson Name <span class="error">*</span>
                                 </label>
                                 <form:input path="trainerCourseChapterLessonName"
                                    class="form-control" id="company"
                                    value="${ trainerCourseChapterLessonDetails.trainerCourseChapterLessonName }"  />
                                 <form:errors path="trainerCourseChapterLessonName"
                                    class="error" />
                              </div>
                           </div>
                           <div class="col-lg-4 col-md-4">
                              <button type="submit" class="btn btn-success btn-sm">Update Lesson</button>
                           </div>
                        </div>
                     </form:form>
                     <!-- rows -->
                     <hr>
                     <div class="row">
                        <div class="col-lg-6 col-md-6 ">
                           <div class="form-group">
                              <label for="company" class=" form-control-label">Upload
                              Preview Video <span class="error">*</span>
                              </label><br>
                              <div class="row">
                                 <c:choose>
                                    <c:when
                                       test="${ not empty trainerCourseChapterLessonDetails.trainerCourseChapterLessonPreviewVideoVimeoName }">
                                       <div class="col-md-4">
                                          <iframe
                                             src="https://player.vimeo.com/video/${ trainerCourseChapterLessonDetails.trainerCourseChapterLessonPreviewVideoVimeoName }"
                                             width="100%" height="100px" frameborder="0"
                                             webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                          <button class="btn delete-button" data-title="Delete"
                                             onclick="deletePreviewVideo(${ trainerCourseChapterLessonDetails.trainerCourseChapterLessonId })">
                                          <i class="fa fa-trash" aria-hidden="true" title="Delete"></i>
                                          </button>
                                       </div>
                                    </c:when>
                                    <c:otherwise>
                                       <div class="col-md-4">No Preview Video Available.
                                          Please upload Preview Video.
                                       </div>
                                    </c:otherwise>
                                 </c:choose>
                              </div>
                              <!--  rows -->
                              <br>
                              <form action="uploadCourseChapterLessonPreviewVideo"
                                 id="uploadCourseChapterLessonPreviewVideoForm" method="post"
                                 enctype="multipart/form-data">
                                 <input
                                    type=hidden class="form-control" name="trainerCourseChapterLessonId"
                                    value="${ trainerCourseChapterLessonDetails.trainerCourseChapterLessonId }" />
                                 <input type="file" accept="video/mp4,video/x-m4v,video/*" name="trainerCourseChapterLessonPreviewVideo"
                                    id="trainerCourseChapterLessonPreviewVideo" class="form-control-file" />
                                 <br>
                                 <div class="form-group">
                                    <button class="btn btn-primary" type="submit" id="uploadCourseChapterLessonPreviewVideoButton">Upload</button>
                                 </div>
                              </form>
                              <!-- Bootstrap Progress bar -->
                              <div class="progress" id="progressBars" style="display: none;">
                                 <div id="progressBar" class="progress-bar progress-bar-success" role="progressbar"
                                    aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">0%</div>
                              </div>
                              <!-- Alert -->
                              <div id="alertMsg" style="color: green;font-size: 18px; display: none;"></div>
                              <%-- <form:input type="file" accept="video/mp4,video/x-m4v,video/*"
                                 path="trainerCourseChapterLessonPreviewVideo" id="file-input"
                                 name="file-input" class="form-control-file" />
                                 <form:errors path="trainerCourseChapterLessonPreviewVideo"
                                 class="error" /> --%>
                           </div>
                        </div>
                        <div class="col-lg-6 col-md-6 ">
                           <div class="form-group">
                              <label for="company" class=" form-control-label">Upload
                              Actual Video <span class="error">*</span>
                              </label><br>
                              <div class="row">
                                 <c:choose>
                                    <c:when
                                       test="${ not empty trainerCourseChapterLessonDetails.trainerCourseChapterLessonMainVideoVimeoName }">
                                       <div class="col-md-4">
                                          <iframe
                                             src="https://player.vimeo.com/video/${ trainerCourseChapterLessonDetails.trainerCourseChapterLessonMainVideoVimeoName }"
                                             width="100%" height="100px" frameborder="0"
                                             webkitallowfullscreen mozallowfullscreen
                                             allowfullscreen></iframe>
                                          <button class="btn delete-button" data-title="Delete"
                                             onclick="deleteActualVideo(${ trainerCourseChapterLessonDetails.trainerCourseChapterLessonId })">
                                          <i class="fa fa-trash" aria-hidden="true"
                                             title="Delete"></i>
                                          </button>
                                          <%-- <video controls>
                                             <source src="${ trainerCourseOtherVideos.courseVideoInfo.courseVideoViewPath }" type="video/mp4">
                                             <source src="${ trainerCourseOtherVideos.courseVideoInfo.courseVideoViewPath }" type="video/ogg">
                                             </video> --%>
                                       </div>
                                    </c:when>
                                    <c:otherwise>
                                       <div class="col-md-4">No Actual Video Available. Please
                                          upload Actual Video.
                                       </div>
                                    </c:otherwise>
                                 </c:choose>
                              </div>
                              <!-- rows -->
                              <br>
                              <form action="uploadCourseChapterLessonMainVideo"
                                 id="uploadCourseChapterLessonMainVideoForm" method="post"
                                 enctype="multipart/form-data">
                                 <input
                                    type=hidden class="form-control" name="trainerCourseChapterLessonId"
                                    value="${ trainerCourseChapterLessonDetails.trainerCourseChapterLessonId }" />
                                 <input type="file" accept="video/mp4,video/x-m4v,video/*" name="trainerCourseChapterLessonMainVideo"
                                    id="trainerCourseChapterLessonMainVideo" class="form-control-file" />
                                 <br>
                                 <div class="form-group">
                                    <button class="btn btn-primary" type="submit" id="uploadCourseChapterLessonMainVideoButton">Upload</button>
                                 </div>
                              </form>
                              <!-- Bootstrap Progress bar -->
                              <div class="progress" id="progressBars1" style="display: none;">
                                 <div id="progressBar1" class="progress-bar progress-bar-success" role="progressbar"
                                    aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">0%</div>
                              </div>
                              <!-- Alert -->
                              <div id="alertMsg1" style="color: green;font-size: 18px; display: none;"></div>
                              <%-- <form:input type="file" accept="video/mp4,video/x-m4v,video/*"
                                 path="trainerCourseChapterLessonMainVideo" id="file-input"
                                 name="file-input" class="form-control-file" />
                                 <form:errors path="trainerCourseChapterLessonMainVideo"
                                 class="error" /> --%>
                           </div>
                        </div>
                     </div>
                     <hr>
                     <div class="row">
                        <div class="col-lg-6 col-md-6 ">
                           <div class="form-group">
                              <label for="company" class=" form-control-label">Upload
                              PDF <span class="error">*</span></label><br>
                              <div class="row">
                                 <c:choose>
                                    <c:when test="${ not empty trainerCourseChapterLessonDetails.trainerCourseChapterLessonMainPdfViewPath }">
                                       <div class="col-md-4">
                                          <div class="Certificates-img">
                                             <a
                                                href="${ trainerCourseChapterLessonDetails.trainerCourseChapterLessonMainPdfViewPath }"
                                                target="_blank"> <img
                                                src="assets/adminAssets/images/certificte-img.png"
                                                class="img-responsive">
                                             </a>
                                             <button class="btn delete-button-pdf"
                                                data-title="Delete"
                                                onclick="deleteMainPdf(${ trainerCourseChapterLessonDetails.trainerCourseChapterLessonId })">
                                             <i class="fa fa-trash" aria-hidden="true"
                                                title="Delete"></i>
                                             </button>
                                          </div>
                                       </div>
                                    </c:when>
                                    <c:otherwise>
                                       <div class="col-md-4">No PDFs Available. Please
                                          upload more PDFs.
                                       </div>
                                    </c:otherwise>
                                 </c:choose>
                              </div>
                              <br>
                              <form action="uploadCourseChapterLessonMainPdf"
                                 id="uploadCourseChapterLessonMainPdfForm" method="post"
                                 enctype="multipart/form-data">
                                 <input
                                    type=hidden class="form-control" name="trainerCourseChapterLessonId"
                                    value="${ trainerCourseChapterLessonDetails.trainerCourseChapterLessonId }" />
                                 <input type="file" accept="application/pdf" name="trainerCourseChapterLessonMainPdf"
                                    id="trainerCourseChapterLessonMainPdf" class="form-control-file" />
                                 <br>
                                 <div class="form-group">
                                    <button class="btn btn-primary" type="submit" id="uploadCourseChapterLessonMainPdfButton">Upload</button>
                                 </div>
                              </form>
                              <!-- Bootstrap Progress bar -->
                              <div class="progress" id="progressBars2" style="display: none;">
                                 <div id="progressBar2" class="progress-bar progress-bar-success" role="progressbar"
                                    aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">0%</div>
                              </div>
                              <!-- Alert -->
                              <div id="alertMsg2" style="color: green;font-size: 18px;display: none;"></div>
                              <%--  <form:input type="file" accept="application/pdf"
                                 path="trainerCourseChapterLessonMainPdf" id="file-input" name="file-input"
                                 class="form-control-file" multiple="multiple" />
                                 <form:errors path="trainerCourseChapterLessonMainPdf" class="error" /> --%>
                           </div>
                        </div>
                     </div>
                     <hr>
                     <!-- rows -->
                  </div>
               </div>
               <!-- .animated -->
            </div>
            <!-- .content -->
         </div>
         <!-- .content -->
      </div>
      <!-- /#right-panel -->
      <%@ include file="/WEB-INF/handlingJsps/trainerLowerFooter.jsp"%>
      <script type="text/javascript">
         $(function() {
         	$('#uploadCourseChapterLessonPreviewVideoButton').click(function(e) {
         		$('#progressBars').show();
         		$('#alertMsg').show();
         		e.preventDefault();
         		//Disable submit button
         		$(this).prop('disabled',true);
         		//alert("hi..");
         		var form = document.forms[1];
         		//console.log(form);
         		var formData = new FormData(form);
         		//console.log(formData)
         		// Ajax call for file uploaling
         		var ajaxReq = $.ajax({
         			url : 'uploadCourseChapterLessonPreviewVideo',
         			type : 'POST',
         			data : formData,
         			cache : false,
         			contentType : false,
         			processData : false,
         			xhr: function(){
         				//Get XmlHttpRequest object
         				 var xhr = $.ajaxSettings.xhr() ;
         				
         				//Set onprogress event handler 
         				 xhr.upload.onprogress = function(event){
         					var perc = Math.round((event.loaded / event.total) * 100);
         					$('#progressBar').text(perc + '%');
         					$('#progressBar').css('width',perc + '%');
         				 };
         				 return xhr ;
         			},
         			beforeSend: function( xhr ) {
         				//Reset alert message and progress bar
         				$('#alertMsg').text('');
         				$('#progressBar').text('');
         				$('#progressBar').css('width','0%');
                        }
         		});
         
         		// Called on success of file upload
         		ajaxReq.done(function(msg) {
         			//alert("success");
         			$('#alertMsg').text(msg);
         			$('#trainerCourseChapterLessonPreviewVideo').val('');
         			$('button[type=submit]').prop('disabled',false);
         			location.reload();
         		});
         		
         		// Called on failure of file upload
         		ajaxReq.fail(function(jqXHR) {
         			//alert("fail");
         			$('#alertMsg').text(jqXHR.responseText+'('+jqXHR.status+
         					' - '+jqXHR.statusText+')');
         			$('button[type=submit]').prop('disabled',false);
         		});
         	});
         });
      </script>
      <script type="text/javascript">
         $(function() {
         	$('#uploadCourseChapterLessonMainVideoButton').click(function(e) {
         		$('#progressBars1').show();
         		$('#alertMsg1').show();
         		e.preventDefault();
         		//Disable submit button
         		$(this).prop('disabled',true);
         		//alert("hi..");
         		var form = document.forms[2];
         		//console.log(form);
         		var formData = new FormData(form);
         		//console.log(formData)
         		// Ajax call for file uploaling
         		var ajaxReq = $.ajax({
         			url : 'uploadCourseChapterLessonMainVideo',
         			type : 'POST',
         			data : formData,
         			cache : false,
         			contentType : false,
         			processData : false,
         			xhr: function(){
         				//Get XmlHttpRequest object
         				 var xhr = $.ajaxSettings.xhr() ;
         				
         				//Set onprogress event handler 
         				 xhr.upload.onprogress = function(event){
         					var perc = Math.round((event.loaded / event.total) * 100);
         					$('#progressBar1').text(perc + '%');
         					$('#progressBar1').css('width',perc + '%');
         				 };
         				 return xhr ;
         			},
         			beforeSend: function( xhr ) {
         				//Reset alert message and progress bar
         				$('#alertMsg1').text('');
         				$('#progressBar1').text('');
         				$('#progressBar1').css('width','0%');
                        }
         		});
         
         		// Called on success of file upload
         		ajaxReq.done(function(msg) {
         			//alert("success");
         			$('#alertMsg1').text(msg);
         			$('#trainerCourseChapterLessonMainVideo').val('');
         			$('button[type=submit]').prop('disabled',false);
         			location.reload();
         		});
         		
         		// Called on failure of file upload
         		ajaxReq.fail(function(jqXHR) {
         			//alert("fail");
         			$('#alertMsg1').text(jqXHR.responseText+'('+jqXHR.status+
         					' - '+jqXHR.statusText+')');
         			$('button[type=submit]').prop('disabled',false);
         		});
         	});
         });
      </script>
      <script type="text/javascript">
         $(function() {
         	$('#uploadCourseChapterLessonMainPdfButton').click(function(e) {
         		$('#progressBars2').show();
         		$('#alertMsg2').show();
         		e.preventDefault();
         		//Disable submit button
         		$(this).prop('disabled',true);
         		//alert("hi..");
         		var form = document.forms[3];
         		//console.log(form);
         		var formData = new FormData(form);
         		//console.log(formData)
         		// Ajax call for file uploaling
         		var ajaxReq = $.ajax({
         			url : 'uploadCourseChapterLessonMainPdf',
         			type : 'POST',
         			data : formData,
         			cache : false,
         			contentType : false,
         			processData : false,
         			xhr: function(){
         				//Get XmlHttpRequest object
         				 var xhr = $.ajaxSettings.xhr() ;
         				
         				//Set onprogress event handler 
         				 xhr.upload.onprogress = function(event){
         					var perc = Math.round((event.loaded / event.total) * 100);
         					$('#progressBar2').text(perc + '%');
         					$('#progressBar2').css('width',perc + '%');
         				 };
         				 return xhr ;
         			},
         			beforeSend: function( xhr ) {
         				//Reset alert message and progress bar
         				$('#alertMsg2').text('');
         				$('#progressBar2').text('');
         				$('#progressBar2').css('width','0%');
                        }
         		});
         
         		// Called on success of file upload
         		ajaxReq.done(function(msg) {
         			//alert("success");
         			$('#alertMsg2').text(msg);
         			$('#trainerCourseChapterLessonMainPdf').val('');
         			$('button[type=submit]').prop('disabled',false);
         			location.reload();
         		});
         		
         		// Called on failure of file upload
         		ajaxReq.fail(function(jqXHR) {
         			//alert("fail");
         			$('#alertMsg2').text(jqXHR.responseText+'('+jqXHR.status+
         					' - '+jqXHR.statusText+')');
         			$('button[type=submit]').prop('disabled',false);
         		});
         	});
         });
      </script>
      <script type="text/javascript">
         function deletePreviewVideo(id, courseId) {
          swal({
              	  title: 'Are you sure?',
              	  text: "Lesson Preview Video will permanently deleted !",
              	  type: 'warning',
              	  showCancelButton: true,
              	  confirmButtonColor: '#3085d6',
              	  cancelButtonColor: '#d33',
              	  confirmButtonText: 'Yes, delete it!',
              	  closeOnConfirm: false
             	},
            		function() {
              	$.ajax({
                      type: "POST",
                      url: "deletePreviewVideo",
                      data: "trainerCourseChapterLessonId="+id,
                      success: function (result) {
                      }
                  })
                  .done(function(data) {
                 swal("Deleted!", "Lesson Preview Video deleted successfully!", "success");
                 location.reload();
               })
               .error(function(data) {
                 swal("Oops", "We couldn't connect to the server!", "error");
               });
            	});
         }
      </script>
      <script type="text/javascript">
         function deleteActualVideo(id, courseId) {
          swal({
              	  title: 'Are you sure?',
              	  text: "Lesson Actual Video will permanently deleted !",
              	  type: 'warning',
              	  showCancelButton: true,
              	  confirmButtonColor: '#3085d6',
              	  cancelButtonColor: '#d33',
              	  confirmButtonText: 'Yes, delete it!',
              	  closeOnConfirm: false
             	},
            		function() {
              	$.ajax({
                      type: "POST",
                      url: "deleteActualVideo",
                      data: "trainerCourseChapterLessonId="+id,
                      success: function (result) {
                      }
                  })
                  .done(function(data) {
                 swal("Deleted!", "Lesson Actual Video deleted successfully!", "success");
                 location.reload();
               })
               .error(function(data) {
                 swal("Oops", "We couldn't connect to the server!", "error");
               });
            	});
         }
      </script>
      <script type="text/javascript">
         function deleteMainPdf(id, courseId) {
          swal({
              	  title: 'Are you sure?',
              	  text: "Lesson PDF will permanently deleted !",
              	  type: 'warning',
              	  showCancelButton: true,
              	  confirmButtonColor: '#3085d6',
              	  cancelButtonColor: '#d33',
              	  confirmButtonText: 'Yes, delete it!',
              	  closeOnConfirm: false
             	},
            		function() {
              	$.ajax({
                      type: "POST",
                      url: "deleteMainPdf",
                      data: "trainerCourseChapterLessonId="+id,
                      success: function (result) {
                      }
                  })
                  .done(function(data) {
                 swal("Deleted!", "Lesson PDF deleted successfully!", "success");
                 location.reload();
               })
               .error(function(data) {
                 swal("Oops", "We couldn't connect to the server!", "error");
               });
            	});
         }
      </script>
   </body>
</html>