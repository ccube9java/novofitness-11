<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!DOCTYPE html>
<html lang="zxx">
<head>
<!-- top header -->
<title>NovoFitness <fmt:message key="label.login" />
</title>
<%@ include file="/WEB-INF/handlingJsps/userTopHeader.jsp"%>
<!-- close top header -->
<style>
.form-content-box form {
	height: auto;
}

.form-content-box .details {
	padding: 30px 30px;
	border-radius: 5px 5px 0 0;
	background: #f3f3fb;
}

.footer span a {
	color: #535353;
}

.btn-theme {
	background-color: #f59e01;
}
</style>
</head>
<body>
	<!-- Top header start -->
	<%@ include file="/WEB-INF/handlingJsps/userHeader.jsp"%>
	<!-- Top header end -->
	<div class="main-page">
		<div class="page_loader"></div>
		<!-- Content area start -->
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<!-- Form content box start -->
					<div class="form-content-box">
						<!-- logo -->
						<!-- details -->
						<div class="details">
							<a href="userHome" class="clearfix alpha-logo"> <img
								src="assets/userAssets/imges/bglogo.png" alt="white-logo">
							</a>
							<h3>Sign In to your account</h3>
							<c:if test="${ not empty alertSuccessMessage }">
								<div class="alert alert-success">${ alertSuccessMessage }</div>
							</c:if>
							<c:if
								test="${ not empty alertSuccessMessagepasswordResetSuccess }">
								<div class="alert alert-success">
									<fmt:message key="label.passwordResetSuccess" />
								</div>
							</c:if>
							<c:if
								test="${ not empty alertSuccessMessagepasswordResetLinkMessage }">
								<div class="alert alert-success">
									<fmt:message key="label.passwordResetLinkMessage" />
								</div>
							</c:if>
							<c:if test="${ not empty alertFailMessageinvalidCredentials }">
								<div class="alert alert-danger">
									<fmt:message key="label.invalidCredentials" />
								</div>
							</c:if>
							<c:if
								test="${ not empty alertFailMessageUserAccountVerificationFailed }">
								<div class="alert alert-danger">
									<fmt:message key="label.userAccountVerificationFailed" />
								</div>
							</c:if>
							<c:if test="${ not empty alertFailMessageemailNotPresent }">
								<div class="alert alert-danger">
									<fmt:message key="label.emailNotPresent" />
								</div>
							</c:if>
							<!-- Form start -->
							<form:form action="userLogin" method="post"
								modelAttribute="userLoginWrapper">
								<div class="form-group">
									<fmt:message key="label.enterEmail" var="enterEmail" />
									<form:input path="userEmail" class="input-text"
										placeholder="${ enterEmail }" />
									<form:errors path="userEmail" class="error" />
								</div>
								<div class="form-group">
									<fmt:message key="label.enterPassword" var="enterPassword" />
									<form:password path="userPassword" class="input-text"
										placeholder="${ enterPassword }" />
									<form:errors path="userPassword" class="error" />
								</div>
								<div class="checkbox">
									<div class="ez-checkbox pull-left">
										<label> <input type="checkbox" class="ez-hide">
											Remember me
										</label>
									</div>
									<a href="userForgetPassword"
										class="link-not-important pull-right"> <fmt:message
											key="label.forgotPaasword" />
									</a>
									<div class="clearfix"></div>
								</div>
								<div class="mb-0">
									<button type="submit" class="btn-md btn-theme btn-block">
										<fmt:message key="label.login" />
									</button>
								</div>
							</form:form>
							<!-- Form end -->
						
						<!-- Footer -->
						<%--  <div class="footer">
                  	<span class="row">
                  		<span class="col-md-12">
                        Login as trainer ? <a href="trainerLogin">
                           Click
                           here
                        </a>
                     </span>
                  		<!-- <span class="col-md-12">
                        Want to register as trainer ? <a href="trainerRegister">
                           Click
                           here
                        </a>
                     </span> -->
                    <span class="col-md-12">
                        Don't have an account ? 
                        <a href="userRegister">
                           <fmt:message key="label.register" />
                           here
                        </a>
                     </span>
                  	</span>
                  </div> --%>
						<div class="footer_login_register">
							<span class="row trainer_register"> <span
								class="col-md-12 trainer_span"> Login as trainer ? <a
									href="trainerLogin"> Click here </a>
							</span> <!-- <span class="col-md-12">
                        Want to register as trainer ? <a href="trainerRegister">
                           Click
                           here
                        </a>
                     </span> --> <span class="col-md-12"> Don't
									have an account ? <a href="userRegister"> Register here </a>
							</span>
							</span>
						</div>
						</div>
					</div>
					<!-- Form content box end -->
				</div>
			</div>
		</div>
	</div>
	<!-- Content area end -->
	<!-- Footer start -->
	<%@ include file="/WEB-INF/handlingJsps/userFooter.jsp"%>
	<%@ include file="/WEB-INF/handlingJsps/userLowerFooter.jsp"%>
	<!-- Footer end -->
</body>
</html>