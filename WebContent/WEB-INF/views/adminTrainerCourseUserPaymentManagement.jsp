<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
<head>
<title>NovoFitness <fmt:message
		key="label.adminNewsLetterManagement" /></title>
<%@ include file="/WEB-INF/handlingJsps/adminTopHeader.jsp"%>
<%@ include file="/WEB-INF/handlingJsps/adminSession.jsp"%>
<c:choose>
	<c:when test="${ adminSession.adminRole.roleId == 2 }">
		<c:redirect url="adminDashboard" />
	</c:when>
</c:choose>
</head>
<body>
	<!-- Left Panel -->
	<%@ include file="/WEB-INF/handlingJsps/adminSidebar.jsp"%>
	<!-- Left Panel -->
	<!-- Right Panel -->
	<div id="right-panel" class="right-panel">
		<!-- Header-->
		<%@ include file="/WEB-INF/handlingJsps/adminHeader.jsp"%>
		<!-- /header -->
		<div class="breadcrumbs">
			<div class="col-sm-4">
				<div class="page-header float-left">
					<div class="page-title">
						<h1>Payment Management</h1>
					</div>
				</div>
			</div>
			<div class="col-sm-4"></div>
		</div>
		<div class="content mt-3">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="col-md-3 payment-status" id="payment">
							<p>Payment Status</p>
							<select class="form-control dest">
								<option>Payment status</option>
								<option>Daily</option>
								<option>Weekly</option>
								<option>Monthly</option>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content mt-3">
			<div class="animated fadeIn">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<strong class="card-title">List Of Payment Management</strong>

							</div>
							<div class="card-body" id="userlist">
								<table id="trainers" class="table table-striped table-bordered">
									<thead>
										<tr>
											<th><fmt:message key="label.srNo" /></th>
											<th><fmt:message key="label.firstName" /></th>
											<th><fmt:message key="label.lastName" /></th>
											<th><fmt:message key="label.email" /></th>
											<th>Course Name</th>
											<th>Trainer Name</th>
											<th>Total Amount</th>
											<th>Trainer Contribution</th>
											<th>Payment Status</th>
											<th><fmt:message key="label.action" /></th>
										</tr>
									</thead>
									<tbody>
										<c:choose>
											<c:when test="${ not empty trainerCourseUserPaymentList }">
												<c:forEach items="${ trainerCourseUserPaymentList }"
													var="trainerCourseUserPayment" varStatus="loop">
													<tr>
														<td>${ loop.index+1 }</td>
														<td>${ trainerCourseUserPayment.userInfo.userFName }</td>
														<td>${ trainerCourseUserPayment.userInfo.userLName }
														</td>
														<td>${ trainerCourseUserPayment.userInfo.userEmail }</td>
														<td>${ trainerCourseUserPayment.trainerCourseInfo.trainerCourseInfo.courseName }</td>
														<td>${ trainerCourseUserPayment.trainerCourseInfo.trainerInfo.trainerFName }
															${ trainerCourseUserPayment.trainerCourseInfo.trainerInfo.trainerLName }</td>
														<td>$ ${ trainerCourseUserPayment.trainerCourseInfo.trainerCourseInfo.courseCost }</td>
														<td>$ ${ trainerCourseUserPayment.userPaymentTrainerContribution }</td>
														<td>
															<c:choose>
													<c:when test="${ trainerCourseUserPayment.userCoursePaymentIsAdminPaidTrainerContrubution }">
														Done
													</c:when>
													<c:otherwise>
														Pending <a onclick="makeTrainerCoursePayment(${ trainerCourseUserPayment.userCoursePaymentId })"><button type="button" 
																class="btn btn-primary btn-round-sm btn-sm btn-paypals">Pay</button></a>
													</c:otherwise>
												</c:choose>
														</td>
														<td class="center-align"><a
															href="adminTrainerCourseUserPaymentViewManagement/${ trainerCourseUserPayment.userCoursePaymentId }"><i
																class="fa fa-eye"></i></a></td>
													</tr>
												</c:forEach>
											</c:when>
										</c:choose>


									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- .animated -->
		</div>
		<!-- .content -->
	</div>


	<!-- /#right-panel -->
	<!-- Right Panel -->
	<%@ include file="/WEB-INF/handlingJsps/adminLowerFooter.jsp"%>
	<!-- <script type="text/javascript">
         $(document).on("click", ".open-watchlistLogin", function() {
         	var trainerCourseId = $(this).data('id');
         	$(".modal-body #trainerCourseId").val(trainerCourseId);
         	// As pointed out in comments, 
         	// it is unnecessary to have to manually call the modal.
         	// $('#addBookDialog').modal('show');watch-list
         });
        function openSendModal(newsletterId, newsletterEmail) {
        	alert("hi."+ newsletterEmail);
        	$(".modal-body #newsletterId").val(newsletterId);
        	//$(".modal-body #newsletterEmail").val(newsletterEmail);
        	$("#myModal").modal();
		}
      </script> -->
	<script>
		$(document).ready(function() {
			$('#datepicker2').datepicker({
				uiLibrary : 'bootstrap'
			});

			$('#fa-calendar2').click(function() {
				$("#datepicker2").focus();
			});
		});
	</script>
	<script>
		$(document).ready(function() {
			$('#datepicker3').datepicker({
				uiLibrary : 'bootstrap'
			});

			$('#fa-calendar3').click(function() {
				$("#datepicker3").focus();
			});
		});
	</script>
	<script type="text/javascript">
		function makeTrainerCoursePayment(id) {
			var userData = {
					userCoursePaymentId : id,
			}
			$.ajax({
						type : "POST",
						url : "payTrainerCoursePaymentContribution",
						data : userData,
						success : function(result) {
							if (result == "success") {
								alert("Paid to Trainer successfully.");
							}
							if (result == "fail") {
								alert("Fail to pay the Trainer. Please try again.");
							}
							location.reload();
						},
						error : function(result) {
							location.reload();
						}
					});
		}
	</script>
</body>
</html>