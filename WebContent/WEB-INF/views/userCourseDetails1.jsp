<!DOCTYPE html>
<html lang="zxx">
   <head>
      <!-- top header -->
      <title>NovoFitness</title>
      <%@ include file="/WEB-INF/handlingJsps/userTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/userSession.jsp"%>
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
      <style type="text/css">
         .videos-course-details {
         height: 100px;
         width: 100px;
         text-align: center;
         width: 100%;
         max-width: 200px;
         margin: 0 auto;
         }
      </style>
      <!-- close top header -->
   </head>
   <body>
      <div class="page_loader"></div>
      <!-- Top header start -->
      <%@ include file="/WEB-INF/handlingJsps/userHeader.jsp"%>
      <!-- Top header end -->
      <!-- Sub banner video start -->
      <section class="videosections">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container">
                  <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                     <c:choose>
                        <c:when test="${ not empty trainerCourseUserChapterLessonList }">
                           <c:forEach items="${ trainerCourseUserChapterLessonList }" var="trainerCourseUserChapterLesson" varStatus="loop">
                              <!-- flight section -->
                              <div class="bhoechie-tab-content ${ loop.index==0?'active':'' }">
                                 <div class="video-parts">
                                    <iframe width="100%" height="500"
                                       src="https://player.vimeo.com/video/${ trainerCourseUserChapterLesson.trainerCourseChapterLessonMainVideoVimeoName }"
                                       frameborder="0" allowfullscreen></iframe>
                                 </div>
                              </div>
                           </c:forEach>
                        </c:when>
                     </c:choose>
                  </div>
                  <!-- lg-9  -->
                  <div
                     class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
                     <div class="list-group">
                        <c:choose>
                           <c:when test="${ not empty trainerCourseUserChapterLessonList }">
                              <c:forEach items="${ trainerCourseUserChapterLessonList }" var="trainerCourseUserChapterLesson" varStatus="loop">
                                 <a href="#" class="list-group-item text-center ${ loop.index==0?'active':'' }">
                                 ${ trainerCourseUserChapterLesson.trainerCourseChapterLessonName } </a> 
                              </c:forEach>
                           </c:when>
                        </c:choose>
                     </div>
                  </div>
                  <!-- lg-3 -->
               </div>
            </div>
            <!-- rows -->
         </div>
         <!-- end .videoWrapper -->
         <!-- rightssidebars  -->
      </section>
      <!-- videosections end -->
      <section class="tab-sections">
         <div class="container">
            <ul class="nav nav-tabs nav-customizetabs">
               <li class="active"><a data-toggle="tab" href="#home">Course
                  Description </a>
               </li>
               <li><a data-toggle="tab" href="#menu1">Trainer Details</a></li>
               <li><a data-toggle="tab" href="#menu2">Reviews</a></li>
               <li><a data-toggle="tab" href="#menu3">FAQs</a></li>
               <li><a data-toggle="tab" href="#menu4">Images</a></li>
               <li><a data-toggle="tab" href="#menu5">PDF</a></li>
            </ul>
            <div class="tab-content">
               <div id="home" class="tab-pane fade tabs-customize-panels active in">
                  <div class="tabspacing"></div>
                  <%-- <c:choose>
                     <c:when test="${ not empty trainerCourseUserChapterList }">
                        <div class="panel-group" id="accordion" role="tablist"
                           aria-multiselectable="true">
                           <div class="panel panel-default">
                              <c:forEach items="${ trainerCourseUserChapterList }"
                                 var="trainerCourseUserChapter">
                                 <div class="panel-heading panel-intro-heading" role="tab"
                                    id="heading">
                                    <h4 class="panel-title">
                                       <a class="collapsed" data-toggle="collapse"
                                          data-parent="#accordion"
                                          href="#collapseheading${ trainerCourseUserChapter.trainerCourseChapterId }"
                                          aria-expanded="false" aria-controls="collapseheading">
                                       ${ trainerCourseUserChapter.trainerCourseChapterName } </a>
                                    </h4>
                                    <!-- <div class="font-text-xs">
                                       <span class="mr-space-xxs">3 / 3</span>|<span
                                       	class="ml-space-xxs">7min</span>
                                       </div> -->
                                 </div>
                                 <div
                                    id="collapseheading${ trainerCourseUserChapter.trainerCourseChapterId }"
                                    class="panel-collapse collapse" role="tabpanel"
                                    aria-labelledby="heading">
                                    <c:choose>
                                       <c:when
                                          test="${ not empty trainerCourseUserChapterLessonList }">
                                          <div class="panel-body panelfaqs">
                                             <ul class="section-list-video">
                                                <c:forEach
                                                   items="${ trainerCourseUserChapterLessonList }"
                                                   var="trainerCourseUserChapterLesson">
                                                   <c:choose>
                                                      <c:when
                                                         test="${ trainerCourseUserChapter.trainerCourseChapterId == trainerCourseUserChapterLesson.trainerCourseChapterInfo.trainerCourseChapterId }">
                                                         <li>${ trainerCourseUserChapterLesson.trainerCourseChapterLessonName }</li>
                                                      </c:when>
                                                   </c:choose>
                                                   <!-- <li>2. Who should take this course</li> -->
                                                </c:forEach>
                                             </ul>
                                             <!-- section-list-video   -->
                                          </div>
                                       </c:when>
                                    </c:choose>
                                    <!-- panel-body panelfaqs   -->
                                 </div>
                                 <!-- collapseheading   -->
                              </c:forEach>
                           </div>
                        </div>
                     </c:when>
                     </c:choose> --%>
                  <!--  main accordian -->
                  ${ trainerCourseDetails.trainerCourseInfo.courseDescription }
               </div>
               <!--  homes -->
               <div id="menu1" class="tab-pane fade tabs-customize-panels">
                  <div class="tabspacing"></div>
                  <div class="container">
                     <div class="row">
                        <div class="col-md-12 col-xs-12 col-sm-12 return-data2 sets-atraction"
                           id="section2">
                           <div class="sets-atraction3">
                              <h4>Trainer Details</h4>
                           </div>
                           <div class="tours-deta">
                              <div class="row">
                                 <div class="col-md-3 col-xs-3 col-sm-3">
                                    <div class="trainner-photo">
                                       <c:choose>
                                          <c:when
                                             test="${ not empty trainerCourseDetails.trainerInfo.trainerProfileImageInfo }">
                                             <img
                                                src="${ trainerCourseDetails.trainerInfo.trainerProfileImageInfo.trainerProfileImageViewPath }">
                                          </c:when>
                                          <c:otherwise>
                                             <img src="assets/adminAssets/images/profile.png">
                                          </c:otherwise>
                                       </c:choose>
                                    </div>
                                    <!-- <div class="trainer-rating">
                                       <p> <span class="fa fa-star checked"></span><span class="trainer-feed"> 4.4 Instructor Rating</span></p>
                                       </div>
                                       <div class="trainer-rating">
                                       <p> <i class="fa fa-comment" aria-hidden="true"></i><span class="trainer-feed"> <b>26,740</b> Reviews</span></p>
                                       </div> -->
                                 </div>
                                 <div class="col-md-9 col-xs-9 col-sm-9">
                                    <ul class="generalair">
                                       <li>
                                          <a
                                             href="dHJhaW5lckRldGFpbHNieVRyYWluZXJJZA?dHJhaW5lcklk=${ trainerCourseDetails.trainerInfo.trainerId }">
                                             <strong>
                                                <!-- Name : --> ${ trainerCourseDetails.trainerInfo.trainerFName }
                                                ${ trainerCourseDetails.trainerInfo.trainerLName }
                                             </strong>
                                          </a>
                                       </li>
                                       <li></li>
                                    </ul>
                                    <div class="trainer-info">
                                       <p>${ trainerCourseDetails.trainerInfo.trainerDescription }
                                       </p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- <div class="course-acc-heading">
                           <h4>About this course</h4>
                           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                              sed do
                           </p>
                           </div>
                           course heading
                           <hr>
                           <div class="row">
                           <div class="col-md-3">
                              <div class="coursepartss">
                                 <p>By the numbers</p>
                              </div>
                              coursepartss
                           </div>
                           md4
                           <div class="col-md-3">
                              <div class="coursepartss">
                                 <p>Skill level: Expert Level</p>
                                 <p>Students: 7577</p>
                                 <p>Languages: English</p>
                                 <p>Captions: No</p>
                              </div>
                           </div>
                           md4
                           <div class="col-md-3">
                              <div class="coursepartss">
                                 <p>Lectures: 30</p>
                                 <p>Video: 2 total hours</p>
                              </div>
                           </div>
                           md4
                           </div>
                           rows
                           <hr>
                           <div class="row">
                           <div class="col-md-2">
                              <div class="coursepartss">
                                 <p>Certificates</p>
                              </div>
                           </div>
                           md2
                           <div class="col-md-10">
                              <div class="coursepartss">
                                 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                    sed do
                                 </p>
                              </div>
                           </div>
                           md10
                           </div>
                           rows
                           <div class="row">
                           <div class="col-md-2">
                              <div class="coursepartss">
                                 <p>Features</p>
                              </div>
                           </div>
                           md2
                           <div class="col-md-7">
                              <div class="coursepartss">
                                 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                    sed do
                                 </p>
                              </div>
                           </div>
                           md10
                           </div>
                           rows
                           <hr>
                           <div class="row">
                           <div class="col-md-2">
                              <div class="coursepartss">
                                 <p>Description</p>
                              </div>
                           </div>
                           md2
                           <div class="col-md-7">
                              <div class="coursepartss">
                                 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                    sed do Lorem ipsum dolor sit amet, consectetur adipiscing
                                    elit, sed do Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do Lorem ipsum dolor sit amet,
                                    consectetur adipiscing elit, sed do Lorem ipsum dolor sit
                                    amet, consectetur adipiscing elit, sed do
                                 </p>
                              </div>
                           </div>
                           md10
                           </div> -->
                        <!-- rows -->
                     </div>
                  </div>
                  <!--  container -->
               </div>
               <!-- menu1 -->
               <div id="menu2" class="tab-pane fade tabs-customize-panels">
                  <div class="tabspacing"></div>
                  <div class="container">
                     <!-- <div class="row">
                        <div class="col-md-3">
                           <div class="form-group">
                              <select class="form-control form-details" id="">
                                 <option>All lectures</option>
                                 <option>Current lecture</option>
                              </select>
                           </div>
                           form-group 
                        </div>
                        md-3 
                        <div class="col-md-3">
                           <div class="form-group">
                              <select class="form-control form-details">
                                 <option>Sort by most recent</option>
                                 <option>Sort by most upvoted</option>
                              </select>
                           </div>
                           form-group 
                        </div>
                         col-md-3
                        <div class="col-md-3">
                           <div class="form-group">
                              <select class="form-control form-details">
                                 <option>Filter question</option>
                              </select>
                           </div>
                           form-group 
                        </div>
                         col-md-3
                        </div>
                        rows
                        question
                        <p class="questions-head-answers">2 Questions in this course</p>
                        <hr>
                        <div class="row questions-types">
                        <div class="col-md-2">
                           <div class="user-avatars">
                              <span class="users-names">VN</span>
                           </div>
                           user-avatars
                        </div>
                        col-md2
                        <div class="col-md-7">
                           <div class="questions-heading">Great course</div>
                           <div class="questions-desc">
                              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                 sed do Lorem ipsum dolor sit amet
                              </p>
                           </div>
                           <div class="questions-names">
                              <p>Vinay k</p>
                           </div>
                        </div>
                        col-md7
                        </div>
                        rows
                        <hr>
                        <div class="row questions-types">
                        <div class="col-md-2">
                           <div class="user-avatars">
                              <span class="users-names">VN</span>
                           </div>
                           user-avatars
                        </div>
                        col-md2
                        <div class="col-md-7">
                           <div class="questions-heading">Great course</div>
                           <div class="questions-desc">
                              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                 sed do Lorem ipsum dolor sit amet
                              </p>
                           </div>
                           <div class="questions-names">
                              <p>Vinay k</p>
                           </div>
                        </div>
                        col-md7
                        </div> -->
                     <div class="review-contents">
                        <div class="review-contents234">
                           <div class="row">
                              <div class="col-md-6 col-xs-6 col-sm-6 review-point "
                                 id="section6">
                                 <h4>
                                    Reviews<span> ${ not empty reviewsCountAverage ? reviewsCountAverage:0.0 }</span>
                                 </h4>
                              </div>
                              <!-- <div class="col-md-6 col-xs-6 col-sm-6 review-point1"> -->
                              <!-- <h4><a href="#review">Write a Reviews</a> </h4> -->
                              <!-- </div>   -->
                           </div>
                        </div>
                        <div class="review-contents2">
                           <c:choose>
                              <c:when test="${ not empty userCourseReviews }">
                                 <c:forEach items="${ userCourseReviews }"
                                    var="userCourseReview">
                                    <div class="row">
                                       <div class="col-md-2 col-xs-12 col-sm-12 profile-imgs">
                                          <div class="profile-file">
                                             <c:choose>
                                                <c:when
                                                   test="${ not empty userCourseReview.userInfo.userProfileImageInfo }">
                                                   <img
                                                      src="${ userCourseReview.userInfo.userProfileImageInfo.userProfileImageViewPath }">
                                                </c:when>
                                                <c:otherwise>
                                                   <img src="assets/adminAssets/images/profile.png">
                                                </c:otherwise>
                                             </c:choose>
                                             <h4>${ userCourseReview.userInfo.userFName }${ userCourseReview.userInfo.userLName }
                                             </h4>
                                          </div>
                                       </div>
                                       <div class="col-md-10 col-xs-12 col-sm-12 stayalon">
                                          <div class="col-md-12 col-xs-12 col-sm-12">
                                             <div class="prof-review-aals">
                                                <div class="prof-review">
                                                   <h4>${ userCourseReview.userCourseReviewTitle }</h4>
                                                </div>
                                                <ul class="resors-details sect-scroll2 pull-right">
                                                   <li class="park-star yellowstar"><span
                                                      class="stars"
                                                      data-rating="${ userCourseReview.userCourseStarRating }"
                                                      data-num-stars="5"></span></li>
                                                </ul>
                                             </div>
                                          </div>
                                          <div class="col-md-12 col-xs-12 col-sm-12">
                                             <div class="profile-file-desc yellowstar_cont">
                                                <p>${ userCourseReview.userCourseReviewDescription }</p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </c:forEach>
                              </c:when>
                              <c:otherwise>
                                 <fmt:message key="label.userLocationNoReviewAvailable" />
                              </c:otherwise>
                           </c:choose>
                        </div>
                        <c:choose>
                           <c:when test="${ not empty myPurchasedCourseInfo }">
                              <div class="get-writer" id="review">
                                 <c:choose>
                                    <c:when test="${ not empty userSession }">
                                       <form:form action="userReviewForTrainerCourse"
                                          method="post" modelAttribute="userCourseReviewWrapper">
                                          <h3 class="logintitle">
                                             <fmt:message key="label.userLocationWriteAReiew" />
                                          </h3>
                                          <div class="get-writereviw">
                                             <div class="row">
                                                <div class="col-md-12 reviewrate">
                                                   <div class="form-group" id="rating-ability-wrapper">
                                                      <label class="control-label" for="rating">
                                                         <span
                                                            class="field-label-info"></span> 
                                                         <form:hidden
                                                            path="trainerCourseId"
                                                            value="${ trainerCourseDetails.trainerCourseId }" />
                                                         <form:hidden path="reviewedUserId"
                                                            value="${ userSession.userId }" />
                                                         <form:hidden
                                                            path="trainerId"
                                                            value="${ trainerCourseDetails.trainerInfo.trainerId }" />
                                                         <form:hidden path="userCourseReviewStarRating"
                                                            value="" />
                                                         <!-- <input type="hidden" id="selected_rating" name="selected_rating" value="" required="required"> -->
                                                      </label>
                                                      <button type="button"
                                                         class="btnrating btn btn-default btn-lg"
                                                         data-attr="1" id="rating-star-1">
                                                      <i class="fa fa-star" aria-hidden="true"></i>
                                                      </button>
                                                      <button type="button"
                                                         class="btnrating btn btn-default btn-lg"
                                                         data-attr="2" id="rating-star-2">
                                                      <i class="fa fa-star" aria-hidden="true"></i>
                                                      </button>
                                                      <button type="button"
                                                         class="btnrating btn btn-default btn-lg"
                                                         data-attr="3" id="rating-star-3">
                                                      <i class="fa fa-star" aria-hidden="true"></i>
                                                      </button>
                                                      <button type="button"
                                                         class="btnrating btn btn-default btn-lg"
                                                         data-attr="4" id="rating-star-4">
                                                      <i class="fa fa-star" aria-hidden="true"></i>
                                                      </button>
                                                      <button type="button"
                                                         class="btnrating btn btn-default btn-lg"
                                                         data-attr="5" id="rating-star-5">
                                                      <i class="fa fa-star" aria-hidden="true"></i>
                                                      </button>
                                                   </div>
                                                   <form:errors path="userCourseReviewStarRating"
                                                      class="error" />
                                                </div>
                                                <div class="col-md-12 sets-input">
                                                   <p>
                                                      <fmt:message key="label.userLocationReviewTitle" />
                                                   </p>
                                                   <form:input path="userCourseReviewTitle"
                                                      class="form-control" placeholder="Enter Review Title" />
                                                   <form:errors path="userCourseReviewTitle" class="error" />
                                                </div>
                                                <div class="col-md-12 sets-input">
                                                   <p>
                                                      <fmt:message key="label.userLocationReviewComments" />
                                                   </p>
                                                   <form:textarea path="userCourseReviewDescription"
                                                      class="form-control"
                                                      placeholder="Enter Review Description"></form:textarea>
                                                   <form:errors path="userCourseReviewDescription"
                                                      class="error" />
                                                </div>
                                             </div>
                                             <div class="post-cmt">
                                                <button type="submit">
                                                   <fmt:message key="label.userLocationPostComment" />
                                                </button>
                                             </div>
                                          </div>
                                       </form:form>
                                    </c:when>
                                 </c:choose>
                              </div>
                           </c:when>
                        </c:choose>
                     </div>
                     <!-- rows -->
                  </div>
                  <!--  container -->
               </div>
               <!-- question and answer -->
               <div id="menu3" class="tab-pane fade tabs-customize-panels">
                  <div class="tabspacing"></div>
                  <div class="bookmarks-part col-md-12">
                     <c:choose>
                        <c:when test="${ not empty trainerCourseUserFAQList }">
                           <div class="faqs">
                              <h6>FAQ</h6>
                              <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                 <div class="panel panel-default">
                                    <c:forEach items="${ trainerCourseUserFAQList }" var="trainerCourseUserFAQ">
                                       <div class="panel panel-default">
                                          <div class="panel-heading" role="tab" id="heading">
                                             <h4 class="panel-title">
                                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse${ trainerCourseUserFAQ.trainerCourseFAQId }" aria-expanded="false" aria-controls="collapseheading">
                                                ${ trainerCourseUserFAQ.trainerCourseFAQQuestion }
                                                </a>
                                             </h4>
                                          </div>
                                          <div id="collapse${ trainerCourseUserFAQ.trainerCourseFAQId }" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading">
                                             <div class="panel-body panelfaqs">${ trainerCourseUserFAQ.trainerCourseFAQAnswer }</div>
                                          </div>
                                       </div>
                                    </c:forEach>
                                 </div>
                                 <!-- panel group -->
                              </div>
                              <!-- FAQ ends -->
                           </div>
                        </c:when>
                     </c:choose>
                     <!-- <h4>No bookmarks created yet</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                           sed do Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet,
                           consectetur adipiscing elit, sed do Lorem ipsum dolor sit amet
                        </p> -->
                  </div>
                  <!--  bookmarks-part-->
               </div>
               <!--  Bookmarks menu3 -->
               <div id="menu4" class="tab-pane fade tabs-customize-panels">
                  <div class="tabspacing"></div>
                  <div class="row">
                     <div
                        class="col-md-12 col-xs-12 col-sm-12 return-data2 sets-atraction"
                        id="section2">
                        <div class="sets-atraction3">
                           <h4>Course Images</h4>
                        </div>
                        <div class="tours-detas ">
                           <div class="row">
                              <c:choose>
                                 <c:when test="${ not empty myCourseImageList }">
                                    <c:forEach items="${ myCourseImageList }" var="trainerCourseOtherImages">
                                       <div class="col-md-3 gallss">
                                          <a data-fancybox="gallery" href="${ trainerCourseOtherImages.courseImagesInfo.courseImagesViewPath }">
                                          <img src="${ trainerCourseOtherImages.courseImagesInfo.courseImagesViewPath }" alt=""></a>
                                       </div>
                                       <%-- <div class="col-md-3 galls">
                                          <img src="${ trainerCourseOtherImages.courseImagesInfo.courseImagesViewPath }" alt="">
                                          </div> --%>
                                    </c:forEach>
                                 </c:when>
                                 <c:otherwise>
                                    <div class="col-md-3 gallss">
                                       No Images Available.
                                    </div>
                                    
                                 </c:otherwise>
                              </c:choose>
                              <!-- <div class="col-md-3 galls">
                                 <a data-fancybox="gallery" href="http://13.233.151.143:8090/dHJhaW5lckNvdXJzZU1lZGlhSW5mb1ZpZXdQYXRo/MnlvZ2l0YXJhdGhpMjIxMkBnbWFpbC5jb20zSU1HXzIwMjAwMzAxXzEyMDk1NmpwZw==/Y292ZXJJbWFnZQ/IMG_20200301_120956.jpg">
                                 <img src="http://13.233.151.143:8090/dHJhaW5lckNvdXJzZU1lZGlhSW5mb1ZpZXdQYXRo/MnlvZ2l0YXJhdGhpMjIxMkBnbWFpbC5jb20zSU1HXzIwMjAwMzAxXzEyMDk1NmpwZw==/Y292ZXJJbWFnZQ/IMG_20200301_120956.jpg" alt=""></a>
                                 </div>
                                 <div class="col-md-3 galls">
                                 <a data-fancybox="gallery" href="assets/userAssets/imges/courses1.png">
                                 <img src="assets/userAssets/imges/courses1.png" alt=""></a>
                                 </div> -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- <div class="row">
                     <div class="col-md-2">
                        <div class="personimages">
                           <img src="" alt=".....">
                        </div>
                     </div>
                     <div class="col-md-7">
                        <div class="announcetxts">
                           <h5 class="announace-head">John Dizoa</h5>
                           <p class="posted-txts">Posted an announcement:18 days Ago</p>
                        </div>
                        announcetxts 
                     </div>
                     md7 
                     </div> -->
                  <!-- rows -->
                  <!-- <div class="announacement-desc">
                     <h4>Lorem Ipsum</h4>
                     <p class="announace-subtxts">Lorem ipsum dolor sit amet,
                        consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                        nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                        commodo consequat. Duis aute irure dolor in reprehenderit in
                        voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                        Excepteur sint occaecat cupidatat non proident
                     </p>
                     <p class="announace-subtxts">Lorem ipsum dolor sit amet,
                        consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                        nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                        commodo consequat. Duis aute irure dolor in reprehenderit in
                        voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                        Excepteur sint occaecat cupidatat non proident
                     </p>
                     </div> -->
                  <!-- rows -->
               </div>
               <!-- menu 4  -->
               
               <!-- menu 5  -->
                  <div id="menu5" class="tab-pane fade tabs-customize-panels">
                      <div class="tabspacing"></div>
                    <div class="pdfheading">
                      <h4>PDF</h4>
                    </div>
                    <c:choose>
                    <c:when test="${ not empty trainerCourseUserChapterLessonList  }">
                    	<c:forEach items="${ trainerCourseUserChapterLessonList }" var="trainerCourseUserChapterLesson">
                    	<c:choose>
                    	<c:when test="${ not empty trainerCourseUserChapterLesson.trainerCourseChapterLessonMainPdfViewPath }">
                    		<div class="col-md-12">
                    <div class="mainpdfsections">
	                     <div class="row">
	                       <div class="col-md-8">
	                          <div class="pdficons">
                                   <svg height="33pt" viewBox="-79 0 512 512" width="33pt"  
                                   xmlns="http://www.w3.org/2000/svg">
                                   <path d="m353.101562 485.515625h-353.101562v-485.515625h273.65625l79.445312 79.449219zm0 0" fill="#e3e4d8"></path><path d="m273.65625 0v79.449219h79.445312zm0 0" fill="#d0cebd"></path><path d="m0 353.101562h353.101562v158.898438h-353.101562zm0 0" fill="#b53438"></path><g fill="#fff"><path d="m52.964844 485.515625c-4.871094 0-8.828125-3.953125-8.828125-8.824219v-88.277344c0-4.875 3.957031-8.828124 8.828125-8.828124 4.875 0 8.828125 3.953124 8.828125 8.828124v88.277344c0 4.871094-3.953125 8.824219-8.828125 8.824219zm0 0"></path>
                                   <path d="m300.136719 397.242188h-52.964844c-4.871094 0-8.828125-3.957032-8.828125-8.828126 0-4.875 3.957031-8.828124 8.828125-8.828124h52.964844c4.875 0 8.828125 3.953124 8.828125 8.828124 0 4.871094-3.953125 8.828126-8.828125 8.828126zm0 0"></path><path d="m300.136719 441.378906h-52.964844c-4.871094 0-8.828125-3.953125-8.828125-8.828125 0-4.871093 3.957031-8.828125 8.828125-8.828125h52.964844c4.875 0 8.828125 3.957032 8.828125 8.828125 0 4.875-3.953125 8.828125-8.828125 8.828125zm0 0"></path><path d="m247.171875 485.515625c-4.871094 0-8.828125-3.953125-8.828125-8.824219v-88.277344c0-4.875 3.957031-8.828124 8.828125-8.828124 4.875 0 8.828125 3.953124 8.828125 8.828124v88.277344c0 4.871094-3.953125 8.824219-8.828125 8.824219zm0 0"></path></g><path d="m170.203125 95.136719c-.863281.28125-11.695313 15.261719.847656 27.9375 8.351563-18.371094-.464843-28.054688-.847656-27.9375m5.34375 73.523437c-6.296875 21.496094-14.601563 44.703125-23.527344 65.710938 18.378907-7.042969 38.375-13.195313 57.140625-17.546875-11.871094-13.621094-23.738281-30.632813-33.613281-48.164063m65.710937 57.175782c7.167969 5.445312 8.914063 8.199218 13.613282 8.199218 2.054687 0 7.925781-.085937 10.636718-3.828125 1.316407-1.820312 1.828126-2.984375 2.019532-3.59375-1.074219-.574219-2.515625-1.710937-10.335938-1.710937-4.449218 0-10.027344.191406-15.933594.933594m-119.957031 38.601562c-18.804687 10.425781-26.464843 19-27.011719 23.835938-.089843.804687-.328124 2.90625 3.785157 6.011718 1.316406-.414062 8.96875-3.859375 23.226562-29.847656m-23.421875 44.527344c-3.0625 0-6-.980469-8.507812-2.832032-9.15625-6.796874-10.390625-14.347656-9.808594-19.492187 1.597656-14.132813 19.304688-28.945313 52.648438-44.03125 13.230468-28.636719 25.820312-63.921875 33.324218-93.398437-8.773437-18.871094-17.3125-43.351563-11.097656-57.714844 2.179688-5.03125 4.910156-8.894532 9.976562-10.566406 2.011719-.652344 7.078126-1.480469 8.941407-1.480469 4.617187 0 9.050781 5.507812 11.183593 9.089843 3.972657 6.648438 3.992188 14.390626 3.363282 21.859376-.609375 7.253906-1.84375 14.46875-3.265625 21.601562-1.039063 5.242188-2.214844 10.460938-3.46875 15.660156 11.855469 24.175782 28.644531 48.816406 44.746093 65.683594 11.539063-2.054688 21.460938-3.097656 29.546876-3.097656 13.761718 0 22.121093 3.167968 25.519531 9.691406 2.828125 5.402344 1.660156 11.726562-3.433594 18.769531-4.898437 6.769531-11.640625 10.34375-19.523437 10.34375-10.710938 0-23.15625-6.671875-37.050782-19.851562-24.957031 5.15625-54.097656 14.34375-77.65625 24.515625-7.355468 15.410156-14.398437 27.824218-20.964844 36.933594-8.996093 12.5-16.773437 18.316406-24.472656 18.316406" fill="#b53438"></path><path d="m79.449219 450.207031h-26.484375c-4.871094 0-8.828125-3.953125-8.828125-8.828125v-52.964844c0-4.875 3.957031-8.828124 8.828125-8.828124h26.484375c19.472656 0 35.308593 15.835937 35.308593 35.3125 0 19.472656-15.835937 35.308593-35.308593 35.308593zm-17.65625-17.65625h17.65625c9.734375 0 17.652343-7.917969 17.652343-17.652343 0-9.738282-7.917968-17.65625-17.652343-17.65625h-17.65625zm0 0" fill="#fff"></path><path d="m158.898438 485.515625h-8.828126c-4.875 0-8.828124-3.953125-8.828124-8.824219v-88.277344c0-4.875 3.953124-8.828124 8.828124-8.828124h8.828126c29.199218 0 52.964843 23.753906 52.964843 52.964843 0 29.210938-23.765625 52.964844-52.964843 52.964844zm0-17.652344h.085937zm0-70.621093v70.621093c19.472656 0 35.308593-15.839843 35.308593-35.3125 0-19.472656-15.835937-35.308593-35.308593-35.308593zm0 0" fill="#fff"></path>
                                   </svg>
                              </div>
	                          <h3 class="course-dtls">${ trainerCourseUserChapterLesson.trainerCourseChapterLessonMainPdfName }</h3>
	                       </div> <!-- md8 -->
	                        
	                       <div class="col-md-4">
	                          <div class="downloadpdf">
	                          <a class="btn btn-primary btns-download" href="${ trainerCourseUserChapterLesson.trainerCourseChapterLessonMainPdfViewPath }" download="${ trainerCourseUserChapterLesson.trainerCourseChapterLessonMainPdfViewPath }">Download</a>
	                           <!-- <button type="button" class="btn btn-primary btns-download">Download</button> -->  
	                           </div>
	                       </div> <!-- md4 -->
	                     </div> <!-- rows -->
                     </div> <!--mainpdfsections-->
                   </div> <!-- md12 -->
                    	</c:when>
                    	</c:choose>
                    	
                    	</c:forEach>
                    </c:when>
                    </c:choose>
                  </div> <!-- pdf div -->
            </div>
            <!--  tab-content -->
         </div>
         <!--  container -->
      </section>
      <!-- tab-sections end -->
      <!-- Footer start -->
      <%@ include file="/WEB-INF/handlingJsps/userFooter.jsp"%>
      <!-- Footer end -->
      <%@ include file="/WEB-INF/handlingJsps/userLowerFooter.jsp"%>
      <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
      <script type="text/javascript">
         $(function(){
            $('.stars').stars();
         }); 
      </script>
      <script>
         $(document).ready(function() {
             $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
                 e.preventDefault();
                 $(this).siblings('a.active').removeClass("active");
                 $(this).addClass("active");
                 var index = $(this).index();
                 $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
                 $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
             });
         });
      </script>
      <script>
         //open and close tab menu
         $('.nav-tabs-dropdown').on("click", "li:not('.active') a",
         		function(event) {
         			$(this).closest('ul').removeClass("open");
         		}).on("click", "li.active a", function(event) {
         	$(this).closest('ul').toggleClass("open");
         });
      </script>
      <script>
         jQuery(document).ready(function($) {
         
         	$(".btnrating").on('click', (function(e) {
         
         		var previous_value = $("#userCourseReviewStarRating").val();
         
         		var selected_value = $(this).attr("data-attr");
         		$("#userCourseReviewStarRating").val(selected_value);
         
         		$(".selected-rating").empty();
         		$(".selected-rating").html(selected_value);
         
         		for (i = 1; i <= selected_value; ++i) {
         			$("#rating-star-" + i).toggleClass('btn-warning');
         			$("#rating-star-" + i).toggleClass('btn-default');
         		}
         
         		for (ix = 1; ix <= previous_value; ++ix) {
         			$("#rating-star-" + ix).toggleClass('btn-warning');
         			$("#rating-star-" + ix).toggleClass('btn-default');
         		}
         
         	}));
         
         });
      </script>
   </body>
</html>