<!DOCTYPE html>
<html lang="zxx">
<head>
<!-- top header -->
<title>NovoFitness</title>
<%@ include file="/WEB-INF/handlingJsps/userTopHeader.jsp"%>
 <%@ include file="/WEB-INF/handlingJsps/userSession.jsp"%>
<!-- close top header -->
</head>
<body>
	<div class="page_loader"></div>
	<!-- Top header start -->
	<%@ include file="/WEB-INF/handlingJsps/userHeader.jsp"%>
	<!-- Top header end -->
	<!-- Sub banner start -->
	<section class="sub-banner">
		<div class="container">
			<div class="row">
				<div class="breadcrumb-area">
					<div class="headet-txt">
						<h1>
							THE FUTURE OF<br> <span> FITNESS </span> and <span>HEALTH</span>
						</h1>
					</div>
					<p class="banner-txt">FIND your EXPERT now</p>
				</div>
			</div>
		</div>
	</section>
	<section id="tabs">
		<div class="about-institute content-area-6">
			<h6 class="section-title h1">Choose Your category</h6>
			<div class="row tab-details">
				<div class="navbar navbar-light bg-faded">
					<div class="col-xs-12 ">
						<nav>
							<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
								<a class="nav-item nav-link active" id="nav-home-tab"
									data-toggle="tab" href="userHome" role="tab"
									aria-controls="nav-home" aria-selected="true">All</a>
								<c:choose>
									<c:when test="${ not empty categoryList }">
										<c:forEach items="${ categoryList }" var="category">
											<a class="nav-item nav-link" id="nav-home-tab"
												data-toggle="tab" href="#nav-All" role="tab"
												aria-controls="nav-home" aria-selected="true"
												onclick="searchByCategoty(${ category.categoryId })">${ category.categoryName }</a>
										</c:forEach>
									</c:when>
								</c:choose>
							</div>
						</nav>
					</div>
				</div>
			</div>
			<div class="tab-content">
				<div class="tab-pane active" id="nav-All">
					<div class="about-institute content-area-6">
						<div class="container" id="allCategories">
							<!-- <div class="list-heading-tabs">
                           <h3>All</h3>
                           </div> -->
							<!-- Wrapper for slides -->
							<!--  <div id="users-demo" class="carousel slide" data-ride="carousel"> -->
							<!-- <div class="carousel-inner"> -->
							<!-- <div class="item active"> -->
							<div class="owl-carousel owl-theme">
								<c:choose>
									<c:when test="${ not empty trainerCourseResponseList }">
										<c:forEach items="${ trainerCourseResponseList }"
											var="trainerCourseResponse">
											<div class="item">
												<a class=""
													href="dXNlckNvdXJzZURlcnRhaWxz?dHJhaW5lckNvdXJzZUlk=${ trainerCourseResponse.trainerCourseInfo.trainerCourseId }&dHJhaW5lcklk=${ trainerCourseResponse.trainerCourseInfo.trainerInfo.trainerId }">
													<!-- <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6 course-details"> -->
													<div class="cart-over">
														<div class="card">
															<div class="course-img">
																<c:choose>
																	<c:when
																		test="${ not empty trainerCourseResponse.trainerCourseInfo.trainerCourseInfo.courseCoverImageInfo }">
																		<img
																			src="${ trainerCourseResponse.trainerCourseInfo.trainerCourseInfo.courseCoverImageInfo.courseCoverImageViewPath }"
																			class="img-responsive">
																	</c:when>
																	<c:otherwise>
																		<img src="assets/userAssets/imges/course11.jpg"
																			class="img-responsive">
																	</c:otherwise>
																</c:choose>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="course-name">
																		<h5>
																			<b>${ trainerCourseResponse.trainerCourseInfo.trainerCourseInfo.courseName }</b>
																		</h5>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="textinfo ">
																		<h5>${ trainerCourseResponse.trainerCourseInfo.trainerCourseInfo.courseDescription }</h5>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="textinfo ">
																		<h5>
																			<!-- Trainer : -->
																			<b>${ trainerCourseResponse.trainerCourseInfo.trainerInfo.trainerFName }
																				${ trainerCourseResponse.trainerCourseInfo.trainerInfo.trainerLName }</b>
																		</h5>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="star-divs rating">
																		<span class="stars"
																			data-rating="${ trainerCourseResponse.averageRating }"
																			data-num-stars="5"></span>
																	</div>
																</div>
															</div>
															<!--  rows -->
															<div class="row">
																<div class="col-md-12">
																	<div class="pricing-parts">
																		<p>$ ${ trainerCourseResponse.trainerCourseInfo.trainerCourseInfo.coursePrice }</p>
																	</div>
																</div>
															</div>
															<!--  rows -->
														</div>
													</div> <!--  </div> -->
												</a>
											</div>
											<!---end of the col-md-3 here--->
										</c:forEach>
									</c:when>
								</c:choose>
							</div>
							<!-- </div> -->

							<!-- <div class="item">
                     <a href="#1">
                        <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6 course-details">
                           <div class="cart-over">
                              <div class="card">
                                 <div class="course-img">
                                    <img src="http://13.233.151.143:8090/dHJhaW5lckNvdXJzZU1lZGlhSW5mb1ZpZXdQYXRo/MXZhaWJoYXZkZXNoNzAyNkBnbWFpbC5jb200aW1hZ2VzanBn/Y292ZXJJbWFnZQ/images.jpg" class="img-responsive">
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="course-name">
                                          <h5>
                                             <b>Internationally Accredited Diploma Certificate in Fitness</b>
                                          </h5>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="textinfo ">
                                          <h5>Now earn your Internationally Accredited Diploma in Fitness accredited by CPD Certification Service, which is an independent body that ensures qualifications are in line with the most current professional standards.
                                          </h5>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="textinfo ">
                                          <h5>
                                             Trainer : <b>vaibhav1
                                             desh1</b>
                                          </h5>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="star-divs rating">
                                          <span class="stars" data-rating="0.0" data-num-stars="5"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
                                       </div>
                                    </div>
                                 </div>
                                  rows
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="pricing-parts">
                                          <p>$ 707</p>
                                       </div>
                                    </div>
                                 </div>
                                  rows
                              </div>
                           </div>
                        </div>
                     </a>
                  </div> -->
							<!--  </div> -->

							<!-- Left and right controls -->
							<!-- <a class="left carousel-control left-users-customizes" href="#users-demo" data-slide="prev">
               <span class="glyphicon glyphicon-chevron-left"></span>
               <span class="sr-only">Previous</span>
               </a>
               <a class="right carousel-control right-users-customizes" href="#users-demo" data-slide="next">
               <span class="glyphicon glyphicon-chevron-right"></span>
               <span class="sr-only">Next</span>
               </a> -->
							<!-- </div> -->
						</div>
						<!--end of container-fluid here--->
					</div>
				</div>
				<!--end of the nav-All here--->
			</div>
		</div>
		<!-- ./Tabs -->
		<!-- dummy carousel start sliders -->
		<!--          <div class="container">
            <h2>Carousel Example</h2>
            <div id="users-demosss" class="carousel slide" data-ride="carousel">
               Indicators
               Wrapper for slides
               <div class="carousel-inner">
                  <div class="item active">
                     <a href="#1">
                        <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6 course-details">
                           <div class="cart-over">
                              <div class="card">
                                 <div class="course-img">
                                    <img src="http://13.233.151.143:8090/dHJhaW5lckNvdXJzZU1lZGlhSW5mb1ZpZXdQYXRo/MXZhaWJoYXZkZXNoNzAyNkBnbWFpbC5jb200aW1hZ2VzanBn/Y292ZXJJbWFnZQ/images.jpg" class="img-responsive">
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="course-name">
                                          <h5>
                                             <b>Internationally Accredited Diploma Certificate in Fitness</b>
                                          </h5>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="textinfo ">
                                          <h5>Now earn your Internationally Accredited Diploma in Fitness accredited by CPD Certification Service, which is an independent body that ensures qualifications are in line with the most current professional standards.
                                          </h5>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="textinfo ">
                                          <h5>
                                             Trainer : <b>vaibhav1
                                             desh1</b>
                                          </h5>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="star-divs rating">
                                          <span class="stars" data-rating="0.0" data-num-stars="5"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
                                       </div>
                                    </div>
                                 </div>
                                  rows
                                 <div class="row">
                                    <div class="col-md-12"><script src="assets/owlcarousel/owl.carousel.js"></script>
                                       <div class="pricing-parts">
                                          <p>$ 707</p>
                                       </div>
                                    </div>
                                 </div>
                                  rows
                              </div>
                           </div>
                        </div>
                     </a>
                     <a href="#1">
                        <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6 course-details">
                           <div class="cart-over">
                              <div class="card">
                                 <div class="course-img">
                                    <img src="http://13.233.151.143:8090/dHJhaW5lckNvdXJzZU1lZGlhSW5mb1ZpZXdQYXRo/MXZhaWJoYXZkZXNoNzAyNkBnbWFpbC5jb200aW1hZ2VzanBn/Y292ZXJJbWFnZQ/images.jpg" class="img-responsive">
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="course-name">
                                          <h5>
                                             <b>Internationally Accredited Diploma Certificate in Fitness</b>
                                          </h5>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="textinfo ">
                                          <h5>Now earn your Internationally Accredited Diploma in Fitness accredited by CPD Certification Service, which is an independent body that ensures qualifications are in line with the most current professional standards.
                                          </h5>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="textinfo ">
                                          <h5>
                                             Trainer : <b>vaibhav1
                                             desh1</b>
                                          </h5>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="star-divs rating">
                                          <span class="stars" data-rating="0.0" data-num-stars="5"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
                                       </div>
                                    </div>
                                 </div>
                                  rows
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="pricing-parts">
                                          <p>$ 707</p>
                                       </div>
                                    </div>
                                 </div>
                                  rows
                              </div>
                           </div>
                        </div>
                     </a>
                     <a href="#1">
                        <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6 course-details">
                           <div class="cart-over">
                              <div class="card">
                                 <div class="course-img">
                                    <img src="http://13.233.151.143:8090/dHJhaW5lckNvdXJzZU1lZGlhSW5mb1ZpZXdQYXRo/MXZhaWJoYXZkZXNoNzAyNkBnbWFpbC5jb200aW1hZ2VzanBn/Y292ZXJJbWFnZQ/images.jpg" class="img-responsive">
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="course-name">
                                          <h5>
                                             <b>Internationally Accredited Diploma Certificate in Fitness</b>
                                          </h5>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="textinfo ">
                                          <h5>Now earn your Internationally Accredited Diploma in Fitness accredited by CPD Certification Service, which is an independent body that ensures qualifications are in line with the most current professional standards.
                                          </h5>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="textinfo ">
                                          <h5>
                                             Trainer : <b>vaibhav1
                                             desh1</b>
                                          </h5>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="star-divs rating">
                                          <span class="stars" data-rating="0.0" data-num-stars="5"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
                                       </div>
                                    </div>
                                 </div>
                                  rows
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="pricing-parts">
                                          <p>$ 707</p>
                                       </div>
                                    </div>
                                 </div>
                                  rows
                              </div>
                           </div>
                        </div>
                     </a>
                     <a href="#1">
                        <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6 course-details">
                           <div class="cart-over">
                              <div class="card">
                                 <div class="course-img">
                                    <img src="http://13.233.151.143:8090/dHJhaW5lckNvdXJzZU1lZGlhSW5mb1ZpZXdQYXRo/MXZhaWJoYXZkZXNoNzAyNkBnbWFpbC5jb200aW1hZ2VzanBn/Y292ZXJJbWFnZQ/images.jpg" class="img-responsive">
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="course-name">
                                          <h5>
                                             <b>Internationally Accredited Diploma Certificate in Fitness</b>
                                          </h5>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="textinfo ">
                                          <h5>Now earn your Internationally Accredited Diploma in Fitness accredited by CPD Certification Service, which is an independent body that ensures qualifications are in line with the most current professional standards.
                                          </h5>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="textinfo ">
                                          <h5>
                                             Trainer : <b>vaibhav1
                                             desh1</b>
                                          </h5>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="star-divs rating">
                                          <span class="stars" data-rating="0.0" data-num-stars="5"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
                                       </div>
                                    </div>
                                 </div>
                                  rows
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="pricing-parts">
                                          <p>$ 707</p>
                                       </div>
                                    </div>
                                 </div>
                                  rows
                              </div>
                           </div>
                        </div>
                     </a>
                  </div>
                  Firset item
                  <div class="item">
                     <a href="#1">
                        <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6 course-details">
                           <div class="cart-over">
                              <div class="card">
                                 <div class="course-img">
                                    <img src="http://13.233.151.143:8090/dHJhaW5lckNvdXJzZU1lZGlhSW5mb1ZpZXdQYXRo/MXZhaWJoYXZkZXNoNzAyNkBnbWFpbC5jb200aW1hZ2VzanBn/Y292ZXJJbWFnZQ/images.jpg" class="img-responsive">
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="course-name">
                                          <h5>
                                             <b>Internationally Accredited Diploma Certificate in Fitness</b>
                                          </h5>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="textinfo ">
                                          <h5>Now earn your Internationally Accredited Diploma in Fitness accredited by CPD Certification Service, which is an independent body that ensures qualifications are in line with the most current professional standards.
                                          </h5>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="textinfo ">
                                          <h5>
                                             Trainer : <b>vaibhav1
                                             desh1</b>
                                          </h5>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="star-divs rating">
                                          <span class="stars" data-rating="0.0" data-num-stars="5"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
                                       </div>
                                    </div>
                                 </div>
                                  rows
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="pricing-parts">
                                          <p>$ 707</p>
                                       </div>
                                    </div>
                                 </div>
                                  rows
                              </div>
                           </div>
                        </div>
                     </a>
                  </div>
                  Second item
               </div>
               carousel-inner
               Left and right controls
               <a class="left carousel-control left-users-customizes" href="#users-demosss" data-slide="prev">
               <span class="glyphicon glyphicon-chevron-left"></span>
               <span class="sr-only">Previous</span>
               </a>
               <a class="right carousel-control right-users-customizes" href="#users-demosss" data-slide="next">
               <span class="glyphicon glyphicon-chevron-right"></span>
               <span class="sr-only">Next</span>
               </a>
            </div>
         </div> -->
		<!-- dummy carousel end sliders -->
		<div class="elearning-elements">
			<!--  </a> -->
			<div class="container">
				<div class="row learning-elements">
					<!-- Main title -->
					<div class="">
						<div class="sportelearning ">
							<h2>Why Novofitness ?</h2>
						</div>
						<div class="col-md-4 col-sm-12">
							<div class="first-sections">
								<img src="assets/userAssets/imges/icon2.png" alt="">
								<h3>Learning from experts</h3>
							</div>
						</div>
						<div class="col-md-4 col-sm-12">
							<div class="first-sections">
								<img src="assets/userAssets/imges/icon3.png" alt="">
								<h3>Anytime, Anywhere</h3>
							</div>
						</div>
						<div class="col-md-4 col-sm-12">
							<div class="first-sections">
								<img src="assets/userAssets/imges/icon1.png" alt="">
								<h3>Interact with teachers and students</h3>
							</div>
						</div>
					</div>
				</div>
				<!-- rows -->
			</div>
			<!-- containers -->
		</div>
	</section>
	<!---sports elearning section end here--->
	<div class="Workouts-sections">
		<div class="container">
			<div class="row">
				<div class="sportelearning-student">
					<h2>What our Student Speak</h2>
				</div>
				<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
					<div class="students-first">
						<div class="row">
							<div class="col-md-4">
								<div class="fitnessss-img">
									<img src="assets/userAssets/imges/jesica.png"
										class="img-responsive">
								</div>
							</div>
							<div class="col-md-8">
								<h6>JESSICA,24</h6>
							</div>
						</div>
						<!-- rows -->
						<div class="er-txts">
							<p>Eroritibusto cum et vendessit eostis apedica turiandus et
								volorro blam endi dis ut aut latis et quam nimustotate
								conessitia sa</p>
						</div>
					</div>
				</div>
				<!-- md-4 -->
				<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
					<div class="students-first">
						<div class="row">
							<div class="col-md-4">
								<div class="fitnessss-img">
									<img src="assets/userAssets/imges/jesica.png"
										class="img-responsive">
								</div>
							</div>
							<div class="col-md-8">
								<h6>JESSICA,24</h6>
							</div>
						</div>
						<!-- rows -->
						<div class="er-txts">
							<p>Eroritibusto cum et vendessit eostis apedica turiandus et
								volorro blam endi dis ut aut latis et quam nimustotate
								conessitia sa</p>
						</div>
					</div>
				</div>
				<!-- md-4 -->
				<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
					<div class="students-first">
						<div class="row">
							<div class="col-md-4">
								<div class="fitnessss-img">
									<img src="assets/userAssets/imges/jesica.png"
										class="img-responsive">
								</div>
							</div>
							<div class="col-md-8">
								<h6>JESSICA,24</h6>
							</div>
						</div>
						<!-- rows -->
						<div class="er-txts">
							<p>Eroritibusto cum et vendessit eostis apedica turiandus et
								volorro blam endi dis ut aut latis et quam nimustotate
								conessitia sa</p>
						</div>
					</div>
				</div>
				<!-- md-4 -->
			</div>
			<!--end of the row here--->
		</div>
	</div>
	<!-- BECOME A TEACHER start -->
	<div class="become-elements3">
		<div class="container">
			<div class="allcourses-txts">
				<h2>BECOME A TEACHER</h2>
			</div>
			<div class="earn-txtxs">
				<p>Earn money. Share your expertise. Build your following.</p>
				<a href="becomeTrainer"><button type="button"
						class="btn btn-default btn-laern">Learn More</button></a>
			</div>
		</div>
	</div>
	<!-- Our BECOME A TEACHER start -->
	<section class="signing-elements">
		<%--   <div class="container">
            <div class="row learning-elements">
               <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                  <div class="list-heading">
                     <h2>Novofitness E-Learning - purpose is to transform access
                        to sports education.
                     </h2>
                  </div>
               </div>
               <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                  <div class="sign-area">
                     <div class="list-heading">
                        <p>Be the first to hear about our special offers and news</p>
                     </div>
                     <fieldset>
                        <c:if test="${ not empty alertSuccessMessage }">
                           <div class="alert alert-success">${ alertSuccessMessage }</div>
                        </c:if>
                        <c:if test="${ not empty alertSuccessMessagenewsletterAlreadySubscribed }">
                           <div class="alert alert-success">
                              <fmt:message key="label.newsletterAlreadySubscribed" />
                           </div>
                        </c:if>
                        <c:if test="${ not empty alertSuccessMessagenewsletterSubscriptionSuccess }">
                           <div class="alert alert-success">
                              <fmt:message key="label.newsletterSubscriptionSuccess" />
                           </div>
                        </c:if>
                        <c:if test="${ not empty alertFailMessage }">
                           <div class="alert alert-danger">${ alertFailMessage }</div>
                        </c:if>
                        <c:if test="${ not empty alertFailMessagenewsletterSubscriptionFail }">
                           <div class="alert alert-danger">
                              <fmt:message key="label.newsletterSubscriptionFail" />
                           </div>
                        </c:if>
                        <form:form action="subscribeNewsLetter" method="post" modelAttribute="newsLetterWrapper">
                           <!--  <input type="text" name="name" placeholder="Your name" class="form-control-sign">
                              <input type="text" name="email" placeholder="Your email addresse" class="form-control-sign"> -->
                           <form:input path="userName" name="name" placeholder="Your name" class="form-control-sign"/>
                           <form:errors path="userName" class="error" />
                           <form:input path="userEmail"  name="email" placeholder="Your email addresse" class="form-control-sign"/>
                           <form:errors path="userEmail" class="error" />
                           <button type="submit" class="btnsign" name="submit">
                              <fmt:message key="label.subscribe" />
                           </button>
                        </form:form>
                     </fieldset>
                  </div>
               </div>
            </div>
            </div> --%>
		<!-- Modal for Login -->
		<%-- <div class="modal fade" id="addFavLoginModal" role="dialog">
            <div class="modal-dialog modal-md">
               <div class="modal-content">
                  <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                     <h4 class="modal-title">
                        <fmt:message key="label.login" />
                        Sign In to your account
                     </h4>
                  </div>
                  <div class="modal-body">
                     <div class="" id="showlog">
                        <div class="details">
                           <!-- Form start -->
                           <div class="form-content-box ">
                              <form:form action="" method="post" modelAttribute="userLoginWrapper">
                                 <div class="form-group">
                                    <form:hidden path="trainerCourseId" value=""/>
                                    <fmt:message key="label.enterEmail" var="enterEmail"/>
                                    <form:input path="userEmail" class="input-text" placeholder="${ enterEmail }" />
                                    <form:errors path="userEmail" class="error" />
                                 </div>
                                 <div class="form-group">
                                    <fmt:message key="label.enterPassword" var="enterPassword"/>
                                    <form:password path="userPassword" class="input-text" placeholder="${ enterPassword }" />
                                    <form:errors path="userPassword" class="error" />
                                 </div>
                                 <div class="checkbox">
                                    <div class="ez-checkbox pull-left">
                                       <a href="userRegister">
                                          <fmt:message
                                             key="label.register" />
                                       </a>
                                    </div>
                                    <a href="userForgetPassword"
                                       class="link-not-important pull-right"> Forgot password </a>
                                    <div class="clearfix"></div>
                                 </div>
                                 <div class="mb-0">
                                    <button type="submit" class="btn-md btn-theme btn-block">
                                    Login</button>
                                 </div>
                              </form:form>
                           </div>
                           <!-- Form end -->
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            </div> --%>
	</section>
	<!-- Footer start -->
	<%@ include file="/WEB-INF/handlingJsps/userFooter.jsp"%>
	<!-- Footer end -->
	<%@ include file="/WEB-INF/handlingJsps/userLowerFooter.jsp"%>
	<script type="text/javascript">
         $(function(){
            $('.stars').stars();
         }); 
      </script>
	<!--  <script>
         $('#dummy').carousel({
         interval: 5000
         });
         
      </script> -->
	<!--  <script type="text/javascript">
         $(document).ready(function() {
                var owl = $('.owl-carousel');
                owl.owlCarousel({
                 	items:3,
                 	/* loop:true, */
                    margin:10,
                    autoplay:true,
                    autoplayTimeout:5000,
                    autoplayHoverPause:true
                  
                })
              });
      </script> -->

	<script type="text/javascript">
         $(document).ready(function() {
                var owl = $('.owl-carousel');
               
                owl.owlCarousel({
                	 loop:true,
                	    margin:10,
                	    responsiveClass:true,
                	    responsive:{
                	        0:{
                	            items:1,
                	            nav:false
                	        },
                	        600:{
                	            items:3,
                	            nav:false
                	        },
                	        1000:{
                	            items:3,
                	            nav:false,
                	            loop:true
                	        }
                	    }
                
                
                  
                })
              });
      </script>

	<!--  <script>
         function openNav() {
         	/* var x = document.getElementById("mySidenav");
         	if (x.style.display === "none") {
         	 x.style.display = "block";
         	 x.classList.add('active');
         	
         	} else {
         	 x.style.display = "none";
         	  
         	}*/
         	document.getElementById("mySidenav").style.display = "block";
         
         }
         
         function closeNav() {
         	document.getElementById("mySidenav").style.display = "none";
         
         }
         </script> -->
	<!--        <script type="text/javascript">
         function userWatchListAddRemove(trainerCourseId, userId, watchListStatus) {
         	//alert("hi..");
         	var userWatchListData = { 
         			trainerCourseId : trainerCourseId,
         			userId : userId,
         			watchListStatus : watchListStatus
         		}
         	$.ajax({
                 url: 'userWatchListAddRemove',
                 type: 'post',
                 data: userWatchListData,
                 success: function(response) {
                     //alert('success::'+response);
                     if(response=='added'){
                    	 alert("Course is added to your WatchList");
                     } else if(response=='fail'){
                    	 alert("Course is fail to update. Please try again");
                     } else if(response=='removed'){
                    	 alert("Course is removed from your WatchList")
                     }
                    /*  $('#watchlistLoginModal').dialog({modal:true});    */
                   /*  $( "#watchlistLoginModal" ).dialog( "open" ); */
                   location.reload();
                   }
             });      
         }
         </script>
         <script type="text/javascript">
            $("#userLoginWrapper").submit(function(e) {
            	//alert("hi..");
            	$.ajax({
            		url : 'userLoginForFavouriteList',
            		type : 'post',
            		data : $(this).serialize(),
            		success : function(response) {
            			//alert('success::'+response);
            			if (response == 'error') {
            				alert("Please Enter valid Data");
            			} else if (response == 'fails') {
            				alert("Login Details are not matched");
            			} else if (response == 'success') {
            				alert("Location added to your Favourites")
            			}
            			/*  $('#watchlistLoginModal').dialog({modal:true});    */
            			/*  $( "#watchlistLoginModal" ).dialog( "open" ); */
            			location.reload();
            		}
            	})
            });
         </script> -->
	<!-- <script type="text/javascript">
         $(document).on("click", ".open-watchlistLogin", function() {
         	var trainerCourseId = $(this).data('id');
         	$(".modal-body #trainerCourseId").val(trainerCourseId);
         	// As pointed out in comments, 
         	// it is unnecessary to have to manually call the modal.
         	// $('#addBookDialog').modal('show');
         	$('#categoryWiseData').hide();
         });
         </script> -->
	<script type="text/javascript">
         function searchByCategoty(categoryId) {
         // alert(categoryId);
          /* var courseData = { 
         		categoryId : categoryId,
             	}
             	$.ajax({
                     type: "POST",
                     url: "searchByCategoty",
                     data: courseData,
                     success: function (result) {
                    	 console.log("sccess",result)
                    	 $('#allCategories').hide();
                    	 $('#categoryWiseData').show();
                    	 
                    	 window.location = "/NovaFitness/bWFpblBhZ2VDb3Vyc2VTZWFyY2hCeUNhdGVnb3J5?Y291cnNlTWFpbkNhdGVnb3J5SWQ="
                        		+ categoryId;
                    	 //location.reload();
                     },
                     error: function (result) {
                    	 console.log("error",result)
                    	 //location.reload();
                     }
                 });  */
          window.location = "bWFpblBhZ2VDb3Vyc2VTZWFyY2hCeUNhdGVnb3J5?Y291cnNlTWFpbkNhdGVnb3J5SWQ="
            		+ categoryId;
         }
      </script>
	<!-- Custom javascript -->
</body>
</html>