<!DOCTYPE html>
<html lang="zxx">
   <head>
      <!-- top header -->
      <title>NovoFitness</title>
      <%@ include file="/WEB-INF/handlingJsps/userTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/userSession.jsp"%>
      <!-- close top header -->
   </head>
   <body>
      <div class="page_loader"></div>
      <!-- Top header start -->
      <%@ include file="/WEB-INF/handlingJsps/userHeader.jsp"%>
      <!-- Top header end -->
      <!-- Sub banner start -->
      <div class="category-heading">
         <div class="container-fluid">
            <div class="categort-title">
               <h1>User Payment Cancelled</h1>
            </div>
         </div>
      </div>
      <section class="category-cart">
         <div class="container">
              <div class="cancelsdivs">
              
                <h3>User Payment Cancelled</h3>
              
              </div>
         </div> <!-- container -->
      </section>
               
      <!---sports elearning section end here--->
      <!-- Footer start -->
      <%@ include file="/WEB-INF/handlingJsps/userFooter.jsp"%>
      <!-- Footer end -->
      <%@ include file="/WEB-INF/handlingJsps/userLowerFooter.jsp"%>
      <!-- Custom javascript -->
   </body>
</html>