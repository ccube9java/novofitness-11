<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!DOCTYPE html>
<html lang="zxx">
<head>
<!-- top header -->
<title>NovoFitness</title>
<%@ include file="/WEB-INF/handlingJsps/userTopHeader.jsp"%>
<c:choose>
	<c:when test="${ not empty userSession }">
		<c:redirect url="userCourseDetailsSession" />
	</c:when>
</c:choose>
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
<style type="text/css">
.videos-course-details {
	height: 100px;
	width: 100px;
	text-align: center;
	width: 100%;
	max-width: 200px;
	margin: 0 auto;
}
</style>
<!-- close top header -->
</head>
<body>
	<div class="page_loader"></div>
	<!-- Top header start -->
	<%@ include file="/WEB-INF/handlingJsps/userHeader.jsp"%>
	<!-- Top header end -->
	<!-- Sub banner start -->
	<section class="details-banners"
		style="background: url(${ not empty trainerCourseDetails.trainerCourseInfo.courseCoverImageInfo ? trainerCourseDetails.trainerCourseInfo.courseCoverImageInfo.courseCoverImageViewPath :'assets/userAssets/imges/novofitnessbg2.jpg' })">
		<div class="menuslider-in1"></div>
	</section>
	<section class="places-details">
		<div class="places-indicate">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-md-8 col-xs-12 col-sm-12 link1-padds ">
						<div class="offer-details">
							<h2>${ trainerCourseDetails.trainerCourseInfo.courseName }</h2>
							<%--  <p class="subtitle"> ${ trainerCourseDetails.trainerCourseInfo.courseDescription }</p> --%>
							<ul class="resors-details sect-scroll2">
								<li class="park-star"><span
									class="tourmaster-tour-rating-text">${ not empty reviewsCountAverage ? reviewsCountAverage:0.0 }</span>
									<span class="stars"
									data-rating="${ not empty reviewsCountAverage ? reviewsCountAverage:0 }"
									data-num-stars="5"></span></li>
								<li class="park-review"><a href="#section6"
									class="park-photos"> <i class="fa fa-commenting-o"
										aria-hidden="true"></i> <c:choose>
											<c:when test="${ not empty userCourseReviews }">
												<span> ${ userCourseReviews.size() } <fmt:message
														key="label.reviews" />
												</span>
											</c:when>
											<c:otherwise>
												<span> <fmt:message key="label.userLocationNoReviews" />
												</span>
											</c:otherwise>
										</c:choose>
								</a></li>
								<li class="active2"><a class="park-photos" href="#section2"><i
										class="fa fa-camera-retro" aria-hidden="true"></i>View Photos</a></li>
								<li class="active2"><a class="park-photos" href="#"> <i
										class="fa fa-graduation-cap" aria-hidden="true"></i>71,467
										students enrolled
								</a></li>
								<li><c:choose>
										<c:when test="${ empty userSession }">
											<i
												class="fa fa-heart watch-list-details-course-page open-watchlistLogin"
												aria-hidden="true"
												title="<fmt:message key="label.addToFav"/>"
												data-id="${ trainerCourseDetails.trainerCourseId }"
												data-toggle="modal" data-target="#addFavLoginModal"></i>
										</c:when>
										<c:otherwise>
											<c:choose>
												<c:when test="${ favouriteListStatusCourseDetails }">
													<c:set var="watchListClass" value="watch-list-in"></c:set>
													<fmt:message key="label.removeFromFav" var="removeFromFav" />
													<c:set var="watchListTitle" value="${ removeFromFav }"></c:set>
												</c:when>
												<c:otherwise>
													<c:set var="watchListClass"
														value="watch-list-details-course-page"></c:set>
													<fmt:message key="label.addToFav" var="addToFav" />
													<c:set var="watchListTitle" value="${ addToFav }"></c:set>
												</c:otherwise>
											</c:choose>
											<a
												onclick="userWatchListAddRemove(${ trainerCourseDetails.trainerCourseId }, ${ userSession.userId }, ${ favouriteListStatusCourseDetails })">
												<i class="fa fa-heart ${ watchListClass }"
												aria-hidden="true" title="${ watchListTitle }" onclick=""></i>
											</a>
										</c:otherwise>
									</c:choose></li>
							</ul>
							<div class="clearfix"></div>
							<p class="traindesc">
								<!-- Trainer: -->
								<span class="training-subtxtxs">${ trainerCourseDetails.trainerInfo.trainerFName }
									${ trainerCourseDetails.trainerInfo.trainerLName }</span>
								<!-- <span class="updatesd-dates">Last updated 11/2017</span>   -->
							</p>
							<div class="clearfix"></div>
							<ul class="resors-details sect-scroll2">
								<li class="active2"><i
										class="fa fa-comments" aria-hidden="true"></i> ${ trainerCourseDetails.trainerCourseInfo.courseLanguage }
								</li>
								<li>
							</ul>
						</div>
						<!-- banners divs  -->
						<div class="clearfix"></div>
						<article>
							<div class="tours-tabs farmistyd">
								<div class="tours-titles">
									<h3 id="section1">Description</h3>
									<p>
										<!-- <span>Lorem Ipsum is simply dummy</span> -->
										${ trainerCourseDetails.trainerCourseInfo.courseDescription }
									</p>
									<div class="tours-deta1">
										<div class="row">
											<div
												class="col-md-12 col-xs-12 col-sm-12 return-data2 sets-atraction">
												<div class="sets-atraction2">
													<h4>Course content</h4>
												</div>
												<p>${ trainerCourseDetails.trainerCourseInfo.courseContent }
												</p>
											</div>
											<!-- Course Chapters Start -->
											<div
												class="col-md-12 col-xs-12 col-sm-12 return-data2 sets-atraction">
												<c:choose>
													<c:when test="${ not empty trainerCourseUserChapterList }">
														<div class="faqs">
															<h6>Chapters</h6>
															<div class="panel-group" id="accordion" role="tablist"
																aria-multiselectable="true">
																<div class="panel panel-default">
																	<c:forEach items="${ trainerCourseUserChapterList }"
																		var="trainerCourseUserChapter">
																		<div class="panel panel-default">
																			<div class="panel-heading" role="tab" id="heading">
																				<h4 class="panel-title">
																					<a class="collapsed" data-toggle="collapse"
																						data-parent="#accordion"
																						href="#collapse${ trainerCourseUserChapter.trainerCourseChapterId }"
																						aria-expanded="false"
																						aria-controls="collapseheading"> ${ trainerCourseUserChapter.trainerCourseChapterName }
																					</a>
																				</h4>
																			</div>
																			<div
																				id="collapse${ trainerCourseUserChapter.trainerCourseChapterId }"
																				class="panel-collapse collapse" role="tabpanel"
																				aria-labelledby="heading">
																				<c:choose>
																					<c:when
																						test="${ not empty trainerCourseUserChapterLessonList }">
																						<c:forEach
																							items="${ trainerCourseUserChapterLessonList }"
																							var="trainerCourseUserChapterLesson">
																							<c:choose>
																								<c:when
																									test="${ trainerCourseUserChapter.trainerCourseChapterId == trainerCourseUserChapterLesson.trainerCourseChapterInfo.trainerCourseChapterId }">
																									<div class="panel-body panelfaqs">${ trainerCourseUserChapterLesson.trainerCourseChapterLessonName }
																										<a data-fancybox
																											href="https://vimeo.com/${ trainerCourseUserChapterLesson.trainerCourseChapterLessonPreviewVideoVimeoName }">
																											<%-- <iframe src="https://player.vimeo.com/video/${ trainerCourseUserChapterLesson.trainerCourseChapterLessonPreviewVideoVimeoName }" width="10%" height="50px" style="float: right; cursor: pointer;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> --%>
																											<span style="float: right;">preview</span>
																										</a>
																									</div>
																								</c:when>
																							</c:choose>
																						</c:forEach>
																					</c:when>
																				</c:choose>
																			</div>
																		</div>
																	</c:forEach>
																</div>
																<!-- panel group -->
															</div>
															<!-- Chapters ends -->
														</div>
													</c:when>
												</c:choose>
											</div>
											<div
												class="col-md-12 col-xs-12 col-sm-12 return-data2 sets-atraction"
												id="section2">
												<div class="sets-atraction2">
													<h4>Trainer Details</h4>
												</div>
												<div class="tours-deta">
													<div class="row">
														<div class="col-md-3 col-xs-3 col-sm-3">
															<div class="trainner-photo">
																<c:choose>
																	<c:when
																		test="${ not empty trainerCourseDetails.trainerInfo.trainerProfileImageInfo }">
																		<img
																			src="${ trainerCourseDetails.trainerInfo.trainerProfileImageInfo.trainerProfileImageViewPath }">
																	</c:when>
																	<c:otherwise>
																		<img src="assets/adminAssets/images/profile.png">
																	</c:otherwise>
																</c:choose>
															</div>
															<!-- <div class="trainer-rating">
                                                <p> <span class="fa fa-star checked"></span><span class="trainer-feed"> 4.4 Instructor Rating</span></p>
                                                </div>
                                                <div class="trainer-rating">
                                                <p> <i class="fa fa-comment" aria-hidden="true"></i><span class="trainer-feed"> <b>26,740</b> Reviews</span></p>
                                                </div> -->
														</div>
														<div class="col-md-9 col-xs-9 col-sm-9">
															<ul class="generalair">
																<li><a
																	href="dHJhaW5lckRldGFpbHNieVRyYWluZXJJZA?dHJhaW5lcklk=${ trainerCourseDetails.trainerInfo.trainerId }">
																		<strong> <!-- Name : --> ${ trainerCourseDetails.trainerInfo.trainerFName }
																			${ trainerCourseDetails.trainerInfo.trainerLName }
																	</strong>
																</a></li>
																<li></li>
															</ul>
															<div class="trainer-course-info">
																${ trainerCourseDetails.trainerInfo.trainerDescription }
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="tours-deta1">
										<div class="row">
											<div
												class="col-md-12 col-xs-12 col-sm-12 return-data2 sets-atraction"
												id="section2">
												<div class="sets-atraction2">
													<h4>Course Images</h4>
												</div>
												<div class="tours-deta ">
													<div class="row">
														<c:choose>
															<c:when test="${ not empty myCourseImageList }">
																<c:forEach items="${ myCourseImageList }"
																	var="trainerCourseOtherImages">
																	<div class="col-md-3 galls">
																		<a data-fancybox="gallery"
																			href="${ trainerCourseOtherImages.courseImagesInfo.courseImagesViewPath }">
																			<img
																			src="${ trainerCourseOtherImages.courseImagesInfo.courseImagesViewPath }"
																			alt="">
																		</a>
																	</div>
																	<%-- <div class="col-md-3 galls">
                                                      <img src="${ trainerCourseOtherImages.courseImagesInfo.courseImagesViewPath }" alt="">
                                                      </div> --%>
																</c:forEach>
															</c:when>
															<c:otherwise>
																<div class="col-md-3 galls">No Images Available.</div>
															</c:otherwise>
														</c:choose>
														<!-- <div class="col-md-3 galls">
                                             <a data-fancybox="gallery" href="http://13.233.151.143:8090/dHJhaW5lckNvdXJzZU1lZGlhSW5mb1ZpZXdQYXRo/MnlvZ2l0YXJhdGhpMjIxMkBnbWFpbC5jb20zSU1HXzIwMjAwMzAxXzEyMDk1NmpwZw==/Y292ZXJJbWFnZQ/IMG_20200301_120956.jpg">
                                             <img src="http://13.233.151.143:8090/dHJhaW5lckNvdXJzZU1lZGlhSW5mb1ZpZXdQYXRo/MnlvZ2l0YXJhdGhpMjIxMkBnbWFpbC5jb20zSU1HXzIwMjAwMzAxXzEyMDk1NmpwZw==/Y292ZXJJbWFnZQ/IMG_20200301_120956.jpg" alt=""></a>
                                             </div>
                                             <div class="col-md-3 galls">
                                             <a data-fancybox="gallery" href="assets/userAssets/imges/courses1.png">
                                             <img src="assets/userAssets/imges/courses1.png" alt=""></a>
                                             </div> -->
													</div>
												</div>
											</div>
										</div>
									</div>
									<c:choose>
										<c:when test="${ not empty myPurchasedCourseInfo }">
											<div class="tours-deta1">
												<div class="row">
													<div
														class="col-md-12 col-xs-12 col-sm-12 return-data2 sets-atraction"
														id="section2">
														<div class="sets-atraction2">
															<h4>Course Pdfs</h4>
														</div>
														<div class="tours-deta ">
															<div class="row">
																<c:choose>
																	<c:when test="${ not empty myPurchasedCoursePdfList }">
																		<c:forEach items="${ myPurchasedCoursePdfList }"
																			var="trainerCourseOtherPDFs">
																			<div class="col-md-3 galls">
																				<div class="Certificates-img">
																					<a
																						href="${ trainerCourseOtherPDFs.coursePDFInfo.coursePDFViewPath }"
																						target="_blank"> <img
																						src="assets/adminAssets/images/certificte-img.png"
																						class="img-responsive">
																					</a>
																				</div>
																			</div>
																		</c:forEach>
																	</c:when>
																	<c:otherwise>
																		<div class="col-md-3 galls">No PDFs Available.</div>
																	</c:otherwise>
																</c:choose>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="tours-deta1">
												<div class="row">
													<div
														class="col-md-12 col-xs-12 col-sm-12 return-data2 sets-atraction"
														id="section2">
														<div class="sets-atraction2">
															<h4>Course Videos</h4>
														</div>
														<div class="tours-deta ">
															<div class="row">
																<c:choose>
																	<c:when
																		test="${ not empty myPurchasedCourseVideoList }">
																		<c:forEach items="${ myPurchasedCourseVideoList }"
																			var="trainerCourseOtherVideos">
																			<div class="col-md-4">
																				<%-- <video controls class="in videos-course-details">
                                                               <source src="${ trainerCourseOtherVideos.courseVideoInfo.courseVideoViewPath }" type="video/mp4">
                                                               <source src="${ trainerCourseOtherVideos.courseVideoInfo.courseVideoViewPath }" type="video/ogg">
                                                               </video> --%>
																				<iframe
																					src="https://player.vimeo.com/video/${ trainerCourseOtherVideos.courseVideoInfo.courseVideoVimeoName }"
																					width="100%" height="100px" frameborder="0"
																					webkitallowfullscreen mozallowfullscreen
																					allowfullscreen></iframe>
																			</div>
																		</c:forEach>
																	</c:when>
																	<c:otherwise>
																		<div class="col-md-3 galls">No Videos Available.
																		</div>
																	</c:otherwise>
																</c:choose>
															</div>
														</div>
													</div>
												</div>
											</div>
										</c:when>
									</c:choose>
									<div class="review-contents">
										<div class="review-contents23">
											<div class="row">
												<div class="col-md-6 col-xs-6 col-sm-6 review-point "
													id="section6">
													<h4>
														Reviews<span> ${ not empty reviewsCountAverage ? reviewsCountAverage:0.0 }</span>
													</h4>
												</div>
												<!-- <div class="col-md-6 col-xs-6 col-sm-6 review-point1"> -->
												<!-- <h4><a href="#review">Write a Reviews</a> </h4> -->
												<!-- </div>   -->
											</div>
										</div>
										<div class="review-contents2">
											<c:choose>
												<c:when test="${ not empty userCourseReviews }">
													<c:forEach items="${ userCourseReviews }"
														var="userCourseReview">
														<div class="row">
															<div class="col-md-2 col-xs-12 col-sm-12 profile-imgs">
																<div class="profile-file">
																	<c:choose>
																		<c:when
																			test="${ not empty userCourseReview.userInfo.userProfileImageInfo }">
																			<img
																				src="${ userCourseReview.userInfo.userProfileImageInfo.userProfileImageViewPath }">
																		</c:when>
																		<c:otherwise>
																			<img src="assets/adminAssets/images/profile.png">
																		</c:otherwise>
																	</c:choose>
																	<h4>${ userCourseReview.userInfo.userFName }${ userCourseReview.userInfo.userLName }
																	</h4>
																</div>
															</div>
															<div class="col-md-10 col-xs-12 col-sm-12 stayalon">
																<div class="col-md-12 col-xs-12 col-sm-12">
																	<div class="prof-review-aals">
																		<div class="prof-review">
																			<h4>${ userCourseReview.userCourseReviewTitle }</h4>
																		</div>
																		<ul class="resors-details sect-scroll2 pull-right">
																			<li class="park-star yellowstar"><span
																				class="stars"
																				data-rating="${ userCourseReview.userCourseStarRating }"
																				data-num-stars="5"></span></li>
																		</ul>
																	</div>
																</div>
																<div class="col-md-12 col-xs-12 col-sm-12">
																	<div class="profile-file-desc yellowstar_cont">
																		<p>${ userCourseReview.userCourseReviewDescription }</p>
																	</div>
																</div>
															</div>
														</div>
													</c:forEach>
												</c:when>
												<c:otherwise>
													<fmt:message key="label.userLocationNoReviewAvailable" />
												</c:otherwise>
											</c:choose>
										</div>
										<c:choose>
											<c:when test="${ not empty myPurchasedCourseInfo }">
												<div class="get-writer" id="review">
													<c:choose>
														<c:when test="${ not empty userSession }">
															<form:form action="userReviewForTrainerCourse"
																method="post" modelAttribute="userCourseReviewWrapper">
																<h3 class="logintitle">
																	<fmt:message key="label.userLocationWriteAReiew" />
																</h3>
																<div class="get-writereviw">
																	<div class="row">
																		<div class="col-md-12 reviewrate">
																			<div class="form-group" id="rating-ability-wrapper">
																				<label class="control-label" for="rating"> <span
																					class="field-label-info"></span> <form:hidden
																						path="trainerCourseId"
																						value="${ trainerCourseDetails.trainerCourseId }" />
																					<form:hidden path="reviewedUserId"
																						value="${ userSession.userId }" /> <form:hidden
																						path="trainerId"
																						value="${ trainerCourseDetails.trainerInfo.trainerId }" />
																					<form:hidden path="userCourseReviewStarRating"
																						value="" /> <!-- <input type="hidden" id="selected_rating" name="selected_rating" value="" required="required"> -->
																				</label>
																				<button type="button"
																					class="btnrating btn btn-default btn-lg"
																					data-attr="1" id="rating-star-1">
																					<i class="fa fa-star" aria-hidden="true"></i>
																				</button>
																				<button type="button"
																					class="btnrating btn btn-default btn-lg"
																					data-attr="2" id="rating-star-2">
																					<i class="fa fa-star" aria-hidden="true"></i>
																				</button>
																				<button type="button"
																					class="btnrating btn btn-default btn-lg"
																					data-attr="3" id="rating-star-3">
																					<i class="fa fa-star" aria-hidden="true"></i>
																				</button>
																				<button type="button"
																					class="btnrating btn btn-default btn-lg"
																					data-attr="4" id="rating-star-4">
																					<i class="fa fa-star" aria-hidden="true"></i>
																				</button>
																				<button type="button"
																					class="btnrating btn btn-default btn-lg"
																					data-attr="5" id="rating-star-5">
																					<i class="fa fa-star" aria-hidden="true"></i>
																				</button>
																			</div>
																			<form:errors path="userCourseReviewStarRating"
																				class="error" />
																		</div>
																		<div class="col-md-12 sets-input">
																			<p>
																				<fmt:message key="label.userLocationReviewTitle" />
																			</p>
																			<form:input path="userCourseReviewTitle"
																				class="form-control"
																				placeholder="Enter Review Title" />
																			<form:errors path="userCourseReviewTitle"
																				class="error" />
																		</div>
																		<div class="col-md-12 sets-input">
																			<p>
																				<fmt:message key="label.userLocationReviewComments" />
																			</p>
																			<form:textarea path="userCourseReviewDescription"
																				class="form-control"
																				placeholder="Enter Review Description"></form:textarea>
																			<form:errors path="userCourseReviewDescription"
																				class="error" />
																		</div>
																	</div>
																	<div class="post-cmt">
																		<button type="submit">
																			<fmt:message key="label.userLocationPostComment" />
																		</button>
																	</div>
																</div>
															</form:form>
														</c:when>
														<%-- <c:otherwise>
                                             <form:form action="userLoginForReview" method="post" modelAttribute="userLogin">
                                                <h3 class="logintitle"><fmt:message key="label.login" /></h3>
                                                <div class="form-group">
                                                	<form:hidden path="userLocationId" value="${ userLocationReviewInfo.userLocationId }"/>
                                                   <label><fmt:message key="label.email" /></label>
                                             <fmt:message key="label.enterEmail" var="enterEmail"/>
                                             <form:input path="userEmail" class="form-control" placeholder="${ enterEmail }" />
                                             <form:errors path="userEmail" class="error" />
                                                </div>
                                                <div class="form-group">
                                                   <label><fmt:message key="label.password" /></label>
                                             <fmt:message key="label.enterPassword" var="enterPassword"/>
                                             <form:password path="userPassword" class="form-control" placeholder="${ enterPassword }" />
                                             <form:errors path="userPassword" class="error" />
                                                </div>
                                                <div class="row">
                                                   <div class="col-md-6">
                                                      <p class="logregist">
                                                         <a href="register"><fmt:message key="label.register" /></a>
                                                      </p>
                                                   </div>
                                                   <div class="col-md-6">
                                                      <p class="logforgot" id="forgot"><fmt:message key="label.forgotPaasword" /></p>
                                                   </div>
                                                </div>
                                                <div class="form-group">
                                                   <input type="submit" class="form-control loginsub" value="<fmt:message key="label.login" />">
                                                </div>
                                             </form:form>
                                             </c:otherwise> --%>
													</c:choose>
												</div>
											</c:when>
										</c:choose>
										<%-- <div class="get-writer" id="review">
                                 <h4><fmt:message key="label.userLocationWriteAReiew" /></h4>
                                 <form:form action="userReviewForTrainerCourse" method="post" modelAttribute="userCourseReviewWrapper">
                                    <div class="get-writereviw">
                                       <div class="row">
                                          <div class="col-md-12 sets-input">
                                             <p>Title</p>
                                             <input type="text" id="user">
                                          </div>
                                          <div class="col-md-12 sets-input">
                                             <p>Comments</p>
                                             <textarea rows="4" cols="50"></textarea>
                                          </div>
                                       </div>
                                       <div class="post-cmt">
                                          <button type="button">Post Comment</button>
                                       </div>
                                    </div>
                                 </form:form>
                                 </div> --%>
										<%--  <c:choose>
                                 <c:when test="${ empty trainerCourseUserQuestionInfo }">
                                    <div class="get-writer" id="review">
                                       <h4>Write A Question</h4>
                                       <form:form action="uploadTrainerCourseUserQuestion" method="post"  class="review_service" modelAttribute="trainerCourseUserQuestionWrapper">
                                          <!-- <form class="review_service"> -->
                                          <form:hidden path="trainerCourseId" value="${ trainerCourseDetails.trainerCourseId }"/>
                                          <form:hidden path="trainerId" value="${ trainerCourseDetails.trainerInfo.trainerId }"/>
                                          <form:hidden path="userId" value="${ userSession.userId }"/>
                                          <div class="get-writereviw">
                                             <div class="row">
                                                <div class="col-md-12 sets-input">
                                                   <p>Question</p>
                                                   <!-- <input type="text" id="user"> -->
                                                   <form:input path="trainerCourseUserQuestionDetails" />
                                                   <form:errors path="trainerCourseUserQuestionDetails" class="error"/>
                                                </div>
                                                <!-- <div class="col-md-12 sets-input">
                                                   <p>Comments</p>
                                                   <textarea rows="4" cols="50"></textarea>
                                                   </div> -->
                                             </div>
                                             <div class="post-cmt">
                                                <c:choose>
                                                   <c:when test="${ empty userSession }">
                                                      <button type="button" data-id="${ trainerCourseDetails.trainerCourseId }"
                                                         data-toggle="modal"
                                                         data-target="#discussionRoomLoginModal">Submit</button>
                                                   </c:when>
                                                   <c:otherwise>
                                                      <button type="submit">Submit</button>
                                                   </c:otherwise>
                                                </c:choose>
                                             </div>
                                          </div>
                                       </form:form>
                                    </div>
                                 </c:when>
                                 <c:when test="${ not empty trainerCourseUserQuestionInfo }">
                                    <div class="review-contents2">
                                       <div class="row">
                                          <div class="col-md-12 col-xs-12 col-sm-12">
                                             <div class="prof-review-aals">
                                                <div class="prof-review">
                                                   <h4>Discussion Room</h4>
                                                   <h5><strong>Question : </strong> ${ trainerCourseUserQuestionInfo.trainerCourseUserQuestionDetails }</h5>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <c:choose>
                                       <c:when test="${ not empty trainerCourseUserQuestionAnswerList }">
                                          <c:forEach items="${ trainerCourseUserQuestionAnswerList }" var="trainerCourseUserQuestionAnswer" >
                                             <div class="row border-top">
                                                <div class="col-md-10 col-xs-12 col-sm-12 stayalon">
                                                   <div class="col-md-1 col-xs-1 col-sm-1">
                                                      <div class="profile-photo-discussion">
                                                         <c:choose>
                                                            <c:when test="${ not empty trainerCourseUserQuestionAnswer.answeredUserInfo.userProfileImageInfo }">
                                                               <img src = "${ trainerCourseUserQuestionAnswer.answeredUserInfo.userProfileImageInfo.userProfileImageViewPath }" />
                                                            </c:when>
                                                            <c:otherwise>
                                                               <img src="assets/adminAssets/images/profile.png">
                                                            </c:otherwise>
                                                         </c:choose>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-11 col-xs-11 col-sm-11">
                                                      <div class="profile-details-desc">
                                                         <p><b>${ trainerCourseUserQuestionAnswer.answeredUserInfo.userFName } ${ trainerCourseUserQuestionAnswer.answeredUserInfo.userLName }</b></p>
                                                         <div class="description-txt">
                                                            <p>${ trainerCourseUserQuestionAnswer.trainerCourseUserQuestionAnswerDetails }</p>
                                                         </div>
                                                         <div class="button-popup">
                                                            <i class="fa fa-reply" id="showcomment"></i>
                                                            <!-- <button type="button" class="btn" data-toggle="modal" data-target="#myModal">Reply</button> -->
                                                         </div>
                                                         <div class="submit-input" id="submit-input">
                                                            <textarea class="replay"  row="2"></textarea>
                                                            <button type="submit">Post</button>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </c:forEach>
                                       </c:when>
                                    </c:choose>
                                    <!-- <div class="row border-top">
                                       <div class="col-md-10 col-xs-12 col-sm-12 stayalon">
                                          <div class="col-md-1 col-xs-1 col-sm-1">
                                             <div class="profile-photo-discussion">
                                                <img src="assets/userAssets/imges/most4.jpg">
                                             </div>
                                          </div>
                                          <div class="col-md-11 col-xs-11 col-sm-11">
                                             <div class="profile-details-desc">
                                                <p><b>Maxim Demidov</b></p>
                                                <div class="rating-dtails">
                                                   <span class="fa fa-star checked"></span>
                                                   <span class="fa fa-star checked"></span>
                                                   <span class="fa fa-star checked"></span>
                                                   <span class="fa fa-star checked"></span>
                                                   <span class="fa fa-star checked"></span>
                                                   </div>
                                                <div class="description-txt">
                                                   <p>Curso muito bem explicado. Muito bom.</p>
                                                </div>
                                                <div class="button-popup">
                                                   <i class="fa fa-reply" aria-hidden="true" data-toggle="modal" data-target="#myModal"></i>
                                                   <button type="button" class="btn" data-toggle="modal" data-target="#myModal">Reply</button>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       </div> -->
                                    <div class="get-writer" id="review">
                                       <h4>Post Your Answer</h4>
                                       <form:form action="postTrainerCourseUserQuestionAnswer" method="post"  class="review_service" modelAttribute="trainerCourseUserQuestionAnswerWrapper">
                                          <!-- <form class="review_service"> -->
                                          <form:hidden path="trainerCourseId" value="${ trainerCourseDetails.trainerCourseId }"/>
                                          <form:hidden path="trainerId" value="${ trainerCourseDetails.trainerInfo.trainerId }"/>
                                          <form:hidden path="trainerCourseUserQuestionId" value="${ trainerCourseUserQuestionInfo.trainerCourseUserQuestionId }"/>
                                          <form:hidden path="userId" value="${ userSession.userId }"/>
                                          <div class="get-writereviw">
                                             <div class="row">
                                                <div class="col-md-12 sets-input">
                                                   <p>Answer</p>
                                                   <!-- <input type="text" id="user"> -->
                                                   <form:textarea rows="5" path="trainerCourseUserQuestionAnswerDetails" />
                                                   <form:errors path="trainerCourseUserQuestionAnswerDetails" class="error"/>
                                                </div>
                                                <!-- <div class="col-md-12 sets-input">
                                                   <p>Comments</p>
                                                   <textarea rows="4" cols="50"></textarea>
                                                   </div> -->
                                             </div>
                                             <div class="post-cmt">
                                                <c:choose>
                                                   <c:when test="${ empty userSession }">
                                                      <button type="button" data-id="${ trainerCourseDetails.trainerCourseId }"
                                                         data-toggle="modal"
                                                         data-target="#discussionRoomLoginModal">Post</button>
                                                   </c:when>
                                                   <c:otherwise>
                                                      <button type="submit">Post</button>
                                                   </c:otherwise>
                                                </c:choose>
                                             </div>
                                          </div>
                                       </form:form>
                                    </div>
                                 </c:when>
                                 </c:choose> --%>
									</div>
								</div>
								<!-- FAQ Start -->
								<c:choose>
									<c:when test="${ not empty trainerCourseUserFAQList }">
										<div class="faqs">
											<h6>FAQ</h6>
											<div class="panel-group" id="accordion" role="tablist"
												aria-multiselectable="true">
												<div class="panel panel-default">
													<c:forEach items="${ trainerCourseUserFAQList }"
														var="trainerCourseUserFAQ">
														<div class="panel panel-default">
															<div class="panel-heading" role="tab" id="heading">
																<h4 class="panel-title">
																	<a class="collapsed" data-toggle="collapse"
																		data-parent="#accordion"
																		href="#collapse${ trainerCourseUserFAQ.trainerCourseFAQId }"
																		aria-expanded="false" aria-controls="collapseheading">
																		${ trainerCourseUserFAQ.trainerCourseFAQQuestion } </a>
																</h4>
															</div>
															<div
																id="collapse${ trainerCourseUserFAQ.trainerCourseFAQId }"
																class="panel-collapse collapse" role="tabpanel"
																aria-labelledby="heading">
																<div class="panel-body panelfaqs">${ trainerCourseUserFAQ.trainerCourseFAQAnswer }</div>
															</div>
														</div>
													</c:forEach>
												</div>
												<!-- panel group -->
											</div>
											<!-- FAQ ends -->
										</div>
									</c:when>
								</c:choose>
								<!--  <div class="panel panel-default">
                           <div class="panel-heading" role="tab" id="headingTwo">
                              <h4 class="panel-title">
                                 <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                 Lorem ipsum dolor sit amet #2
                                 </a>
                              </h4>
                           </div>
                           <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                              <div class="panel-body panelfaqs">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.</div>
                           </div>
                           </div> -->
								<!-- <div class="panel panel-default">
                           <div class="panel-heading" role="tab" id="headingFour">
                              <h4 class="panel-title">
                                 <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                 Lorem ipsum dolor sit amet #3
                                 </a>
                              </h4>
                           </div>
                           <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                              <div class="panel-body panelfaqs">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </div>
                           </div>
                           </div> -->
						</article>
					</div>
					<div class="col-lg-4 col-md-4 col-xs-12 col-sm-12 set-long">
						<div class="video_tab">
							<c:choose>
								<c:when
									test="${ not empty trainerCourseDetails.trainerCourseInfo.courseCoverVideoInfo }">
									<a data-fancybox="gallery"
										href="https://vimeo.com/${ trainerCourseDetails.trainerCourseInfo.courseCoverVideoInfo.courseCoverVideoVimeoName }">
										<%--  <iframe src="https://player.vimeo.com/video/${ trainerCourseDetails.trainerCourseInfo.courseCoverVideoInfo.courseCoverVideoVimeoName }"
                               width="100%" height="100px" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen>
                               </iframe> --%> <input type="hidden"
										id="vimeoid"
										value="${ trainerCourseDetails.trainerCourseInfo.courseCoverVideoInfo.courseCoverVideoVimeoName }">
										<div id="output"></div>
										<div class="play">
											<i class="fa fa-play" aria-hidden="true"></i>
										</div> <%-- <video class="usersvideos" controls>
                                       <source src="https://player.vimeo.com/video/${ trainerCourseDetails.trainerCourseInfo.courseCoverVideoInfo.courseCoverVideoVimeoName }" type="video/mp4">
                                      
                                    </video> --%>
									</a>
								</c:when>
								<c:otherwise>
									<!-- <a data-fancybox="gallery" href="https://www.youtube.com/embed/ldLeBMj3q3g">
                               <input type="hidden" id="vimeoid" placeholder="257314493" value="257314493">
                               <div id="output">
									</div>
									 <div id="output">
									 <img alt="" src="https://ctt.trains.com/sitefiles/images/no-preview-available.png">
									</div>
									
                                  <iframe class="cours_video"
                                 src="https://www.youtube.com/embed/ldLeBMj3q3g" frameborder="0" width="100%" height="200"
                                 allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                 allowfullscreen></iframe>
                              </a> -->
									<div id="output">
										<img alt=""
											src="assets/userAssets/imges/noPreviewAvailable.png">
									</div>

								</c:otherwise>
							</c:choose>
							<!-- <iframe class="cours_video"
                           src="https://www.youtube.com/embed/ldLeBMj3q3g" frameborder="0" width="100%" height="200"
                           allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                           allowfullscreen></iframe> -->
						</div>
						<c:choose>
							<c:when test="${ empty myPurchasedCourseInfo }">
								<div class="set-books" id="sticker">
									<h4>PURCHASE NOW</h4>
									<div class=" books-cmts">
										<h5>
											<span><i class="fa fa-usd" aria-hidden="true"></i>${ trainerCourseDetails.trainerCourseInfo.coursePrice }</span><span><i
												class="fa fa-info-circl" aria-hidden="true"></i></span>
										</h5>
										<div class="tabs-submitss">
											<c:choose>
												<c:when test="${ empty userSession }">
													<button class="btn btn-proced open-addToCartLogin"
														data-id="${ trainerCourseDetails.trainerCourseId }"
														data-toggle="modal" data-target="#addCartLoginModal">Add
														to cart</button>
													<button class="btn btn-procednow"
														data-id="${ trainerCourseDetails.trainerCourseId }"
														data-toggle="modal" data-target="#addCartLoginModal">Buy
														Now</button>
												</c:when>
												<c:when test="${ not empty userSession }">
													<c:choose>
														<c:when test="${ !cartListStatusCourseDetails }">
															<a href="#"
																onclick="userCartListAddRemove(${ trainerCourseDetails.trainerCourseId }, ${ userSession.userId }, ${ cartListStatusCourseDetails })">
																<button class="btn btn-proced">Add to cart</button>
															</a>
														</c:when>
													</c:choose>
													<a href="#"
														onclick="userBuyNow(${ trainerCourseDetails.trainerCourseId }, ${ userSession.userId }, ${ cartListStatusCourseDetails })">
														<button class="btn btn-procednow">Buy Now</button>
													</a>
												</c:when>
											</c:choose>
										</div>
									</div>
								</div>
							</c:when>
						</c:choose>
						<div class="tabs-share">
							<div class="submits-share">
								<div class="row">
									<div class="col-md-12 wish-share">
										<!-- <div class="wish-sharelist">
                                    <a href="#"><i class="fa fa-envelope-o"
                                       aria-hidden="true"></i> <span>novafitness@gmail.com</span></a>
                                    </div> -->
									</div>
								</div>
							</div>
						</div>
						<!-- <div class="tabs-share-info">
                        <div class="submits-share-info">
                           <div class="row">
                              <div class="col-md-12 wish-share">
                                 <div class="wish-sharelist-info">
                                    <h3>This course includes</h3>
                                 </div>
                                 <p class="icon-details">
                                    <i class="fa fa-life-ring" aria-hidden="true"></i>4 hours on-demand video
                                 </p>
                                 <p class="icon-details">
                                    <i class="fa fa-file-pdf-o"></i>downloadable resources
                                 </p>
                                 <p class="icon-details">
                                    <i class="fa fa-file-excel-o" aria-hidden="true"></i>11 downloadable resources
                                 </p>
                                 <p class="icon-details">
                                    <i class="fas fa-tv"></i>Access on mobile and TV
                                 </p>
                              </div>
                               wish shares
                           </div>
                        </div>
                     </div> -->
					</div>
				</div>
			</div>
		</div>
		<!-- Modal for Login FavouriteList -->
		<div class="modal fade modal-favouritelist" id="addFavLoginModal"
			role="dialog">
			<div class="modal-dialog modal-md modals-accounts-details">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">
							<%-- <fmt:message key="label.login" /> --%>
							Sign In to your account
						</h4>
					</div>
					<div class="modal-body">
						<div class="coursesaccountsdetails" id="showlog">
							<div class="details">
								<!-- Form start -->
								<div class="form-content-box contentdetailsform">
									<form:form action="" method="post"
										modelAttribute="userLoginWrapper">
										<div class="form-group">
											<form:hidden path="trainerCourseId" value="" />
											<fmt:message key="label.enterEmail" var="enterEmail" />
											<form:input path="userEmail" class="input-text"
												placeholder="${ enterEmail }" />
											<form:errors path="userEmail" class="error" />
										</div>
										<div class="form-group">
											<fmt:message key="label.enterPassword" var="enterPassword" />
											<form:password path="userPassword" class="input-text"
												placeholder="${ enterPassword }" />
											<form:errors path="userPassword" class="error" />
										</div>
										<div class="checkbox">
											<div class="ez-checkbox pull-left">
												<a href="userRegister"> <fmt:message
														key="label.register" />
												</a>
											</div>
											<a href="userForgetPassword"
												class="link-not-important pull-right"> Forgot password </a>
											<div class="clearfix"></div>
										</div>
										<div class="mb-0">
											<button type="submit" class="btn-md btn-theme btn-block">
												Login</button>
										</div>
									</form:form>
								</div>
								<!-- Form end -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal for Login Add to cart -->
		<div class="modal fade" id="addCartLoginModal" role="dialog">
			<div class="modal-dialog modal-md">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">
							<%-- <fmt:message key="label.login" /> --%>
							Sign In to your account
						</h4>
					</div>
					<div class="modal-body">
						<div class="" id="showlog">
							<div class="details">
								<!-- Form start -->
								<div class="form-content-box ">
									<form:form action="" method="post" id="addToCartForm"
										modelAttribute="userLoginWrapper">
										<div class="form-group">
											<form:hidden path="trainerCourseId" value="" />
											<fmt:message key="label.enterEmail" var="enterEmail" />
											<form:input path="userEmail" class="input-text"
												placeholder="${ enterEmail }" />
											<form:errors path="userEmail" class="error" />
										</div>
										<div class="form-group">
											<fmt:message key="label.enterPassword" var="enterPassword" />
											<form:password path="userPassword" class="input-text"
												placeholder="${ enterPassword }" />
											<form:errors path="userPassword" class="error" />
										</div>
										<div class="checkbox">
											<div class="ez-checkbox pull-left">
												<a href="userRegister"> <fmt:message
														key="label.register" />
												</a>
											</div>
											<a href="userForgetPassword"
												class="link-not-important pull-right"> Forgot password </a>
											<div class="clearfix"></div>
										</div>
										<div class="mb-0">
											<button type="submit" class="btn-md btn-theme btn-block">
												Login</button>
										</div>
									</form:form>
								</div>
								<!-- Form end -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal for Login Discussion Room -->
		<div class="modal fade" id="discussionRoomLoginModal" role="dialog">
			<div class="modal-dialog modal-md">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">
							<%-- <fmt:message key="label.login" /> --%>
							Sign In to your account
						</h4>
					</div>
					<div class="modal-body">
						<div class="" id="showlog">
							<div class="details">
								<!-- Form start -->
								<div class="form-content-box ">
									<form:form action="" method="post"
										modelAttribute="discussionRoomUserLoginWrapper">
										<div class="form-group">
											<%-- <form:hidden path="trainerCourseId" value=""/> --%>
											<fmt:message key="label.enterEmail" var="enterEmail" />
											<form:input path="userEmail" class="input-text"
												placeholder="${ enterEmail }" />
											<form:errors path="userEmail" class="error" />
										</div>
										<div class="form-group">
											<fmt:message key="label.enterPassword" var="enterPassword" />
											<form:password path="userPassword" class="input-text"
												placeholder="${ enterPassword }" />
											<form:errors path="userPassword" class="error" />
										</div>
										<div class="checkbox">
											<div class="ez-checkbox pull-left">
												<a href="userRegister"> <fmt:message
														key="label.register" />
												</a>
											</div>
											<a href="userForgetPassword"
												class="link-not-important pull-right"> Forgot password </a>
											<div class="clearfix"></div>
										</div>
										<div class="mb-0">
											<button type="submit" class="btn-md btn-theme btn-block">
												Login</button>
										</div>
									</form:form>
								</div>
								<!-- Form end -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal For Replying in Discussion Room -->
		<div class="modal fade" id="myModal" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Submit Feedback</h4>
					</div>
					<div class="modal-body">
						<textarea class="form-control" rows="5" id="comment"></textarea>
					</div>
					<div class="modal-footer">
						<!-- <button type="button" class="btn btn-default" >Close</button> -->
						<div class="post-cmt">
							<button class="post-cmt" data-dismiss="modal">Submit</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Footer start -->
	<%@ include file="/WEB-INF/handlingJsps/userFooter.jsp"%>
	<!-- Footer end -->
	<%@ include file="/WEB-INF/handlingJsps/userLowerFooter.jsp"%>
	<script
		src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
	<script type="text/javascript">
         $(function(){
            $('.stars').stars();
         }); 
      </script>
	<!--  <script>
         function openNav() {
         	/* var x = document.getElementById("mySidenav");
         	if (x.style.display === "none") {
         	 x.style.display = "block";
         	 x.classList.add('active');
         	
         	} else {
         	 x.style.display = "none";
         	  
         	}*/
         	document.getElementById("mySidenav").style.display = "block";
         
         }
         
         function closeNav() {
         	document.getElementById("mySidenav").style.display = "none";
         
         }
         </script> -->
	<script type="text/javascript">
         $(document).on("click", ".open-watchlistLogin", function() {
         	var trainerCourseId = $(this).data('id');
         	$(".modal-body #trainerCourseId").val(trainerCourseId);
         	// As pointed out in comments, 
         	// it is unnecessary to have to manually call the modal.
         	// $('#addBookDialog').modal('show');watch-list
         });
         $(document).on("click", ".open-addToCartLogin", function() {
          	var trainerCourseId = $(this).data('id');
          	$(".modal-body #trainerCourseId").val(trainerCourseId);
          	// As pointed out in comments, 
          	// it is unnecessary to have to manually call the modal.
          	// $('#addBookDialog').modal('show');watch-list
          });
      </script>
	<script type="text/javascript">
         function userWatchListAddRemove(trainerCourseId, userId, watchListStatus) {
         	//alert("hi..");
         	var userWatchListData = { 
         			trainerCourseId : trainerCourseId,
         			userId : userId,
         			watchListStatus : watchListStatus
         		}
         	 $.ajax({
                 url: 'userWatchListAddRemove',
                 type: 'post',
                 data: userWatchListData,
                 success: function(response) {
                     //alert('success::'+response);
                     if(response=='added') {
                    	 //alert("Course is added to your WatchList");
                   	  swal({
               			  title: "Successfully Added!",
               			  text: "Course is added to your Favourite!",
               			  icon: "success",
               			});
                     } else if(response=='fail'){
                    	 //alert("Course is fail to update. Please try again");
                   	  swal({
                 			  title: "Failed!",
                 			  text: "Course is fail to update. Please try again!",
                 			  icon: "warning",
                 			});
                     } else if(response=='removed'){
                   	  swal({
               			  title: "Successfully Removed!",
               			  text: "Course is removed to your Favourite!",
               			  icon: "success",
               			});
                    	 //alert("Course is removed from your WatchList")
                     }
                   location.reload();
                   }
             });      
         }
      </script>
	<script type="text/javascript">
         function userCartListAddRemove(trainerCourseId, userId, watchListStatus) {
         	//alert("hi..");
         	var userCartListData = { 
         			trainerCourseId : trainerCourseId,
         			userId : userId,
         			watchListStatus : watchListStatus
         		}
         	 $.ajax({
                 url: 'userCartListAddRemove',
                 type: 'post',
                 data: userCartListData,
                 success: function(response) {
                     //alert('success::'+response);
                     if(response=='added') {
                    	 //alert("Course is added to your WatchList");
                   	  swal({
               			  title: "Successfully Added!",
               			  text: "Course is added to your Cart!",
               			  icon: "success",
               			});
                     } else if(response=='fail'){
                    	 //alert("Course is fail to update. Please try again");
                   	  swal({
                 			  title: "Failed!",
                 			  text: "Course is fail to add your Cart. Please try again!",
                 			  icon: "warning",
                 			});
                     } else if(response=='removed'){
                   	  swal({
               			  title: "Successfully Removed!",
               			  text: "Course is removed to your Cart!",
               			  icon: "success",
               			});
                    	 //alert("Course is removed from your WatchList")
                     }
                   location.reload();
                   }
             });      
         }
      </script>
	<script type="text/javascript">
         function userBuyNow(trainerCourseId, userId, watchListStatus) {
         	//alert("hi..");
         	var userCartListData = { 
         			trainerCourseId : trainerCourseId,
         			userId : userId,
         			watchListStatus : watchListStatus
         		}
         	 $.ajax({
                 url: 'userBuyNow',
                 type: 'post',
                 data: userCartListData,
                 success: function(response) {
                     //alert('success::'+response);
                     if(response=='added') {
                    	 //alert("Course is added to your WatchList");
                    		window.location = "userShopingCartList";
                     } else if(response=='fail') {
                    	 //alert("Course is fail to update. Please try again");
                    	 location.reload();
                     }/*  else if(response=='removed') {
                   	  swal({
               			  title: "Successfully Removed!",
               			  text: "Course is removed to your Cart!",
               			  icon: "success",
               			});
                    	 //alert("Course is removed from your WatchList")
                     } */
                 
                   //location.reload();
                   }
             });      
         }
      </script>
	<script type="text/javascript">
         $("#userLoginWrapper").submit(function(e) {
         	//alert("hi..");
         	$.ajax({
         		url : 'userLoginForFavouriteList',
         		type : 'post',
         		data : $(this).serialize(),
         		success : function(response) {
         			//alert('success::'+response);
         			
         			if (response == 'error') {
         				//alert("Please Enter valid Data");
         				swal({
             			  title: "Failed!",
             			  text: "Please Enter valid Data!",
             			  icon: "warning",
             			});
         			} else if (response == 'fails') {
         				//alert("Login Details are not matched");
         				swal({
               			  title: "Failed!",
               			  text: "Login Details are not matched!",
               			  icon: "error",
               			});
         			} else if (response == 'success') {
         				//alert("Course added to your Favourites")
         				swal({
               			  title: "SuccessFully Added!",
               			  text: "Course added to your Favourite!",
               			  icon: "success",
               			});
         				location.reload();
         			}
         			location.reload();
         		}
         	})
         });
      </script>
	<script type="text/javascript">
         $("#discussionRoomUserLoginWrapper").submit(function(e) {
         	//alert("hi..");
         	$.ajax({
         		url : 'userLoginForDiscussionRoom',
         		type : 'post',
         		data : $(this).serialize(),
         		success : function(response) {
         			//alert('success::'+response);
         			
         			if (response == 'error') {
         				//alert("Please Enter valid Data");
         				swal({
             			  title: "Failed!",
             			  text: "Please Enter valid Data!",
             			  icon: "warning",
             			});
         			} 
         			if (response == 'fails') {
         				//alert("Login Details are not matched");
         				swal({
               			  title: "Failed!",
               			  text: "Login Details are not matched!",
               			  icon: "error",
               			});
         			} 
         			if (response == 'success1') {
         				alert("Course added to your Favourites")
         				/* swal({
               			  title: "Successfully",
               			  text: "Question generated!",
               			  icon: "success",
               			}); */
         			}
         			location.reload();
         		}
         	})
         });
      </script>
	<script type="text/javascript">
         $("#addToCartForm").submit(function(e) {
         	//alert("hi..");
         	$.ajax({
         		url : 'userLoginForAddToCart',
         		type : 'post',
         		data : $(this).serialize(),
         		success : function(response) {
         			//alert('success::'+response);
         			
         			if (response == 'error') {
         				//alert("Please Enter valid Data");
         				swal({
             			  title: "Failed!",
             			  text: "Please Enter valid Data!",
             			  icon: "warning",
             			});
         			} else if (response == 'fails') {
         				//alert("Login Details are not matched");
         				swal({
               			  title: "Failed!",
               			  text: "Login Details are not matched!",
               			  icon: "error",
               			});
         			} else if (response == 'success') {
         				//alert("Course added to your Favourites")
         				swal({
               			  title: "SuccessFully Added!",
               			  text: "Course added to your Cart!",
               			  icon: "success",
               			});
         				location.reload();
         			}
         			location.reload();
         		}
         	})
         });
      </script>
	<script>
         $(document).ready(function(){
           $("#showcomment").click(function(){
             $("#submit-input").toggle();
           });
         /*   $("#showcomment").click(function(){
             $("#submit-input").show();
           }); */
         });
      </script>
	<script>
         jQuery(document).ready(function($) {
         
         	$(".btnrating").on('click', (function(e) {
         
         		var previous_value = $("#userCourseReviewStarRating").val();
         
         		var selected_value = $(this).attr("data-attr");
         		$("#userCourseReviewStarRating").val(selected_value);
         
         		$(".selected-rating").empty();
         		$(".selected-rating").html(selected_value);
         
         		for (i = 1; i <= selected_value; ++i) {
         			$("#rating-star-" + i).toggleClass('btn-warning');
         			$("#rating-star-" + i).toggleClass('btn-default');
         		}
         
         		for (ix = 1; ix <= previous_value; ++ix) {
         			$("#rating-star-" + ix).toggleClass('btn-warning');
         			$("#rating-star-" + ix).toggleClass('btn-default');
         		}
         	}));
         });
      </script>
	<!-- Custom javascript -->
	<script type="text/javascript">
      $( document ).ready(function() {
    	    console.log( "ready!" );
    	    const videoIdInput = document.getElementById('vimeoid');
    	     // const getVideo = document.getElementById('getVideo');
    	      const output = document.getElementById('output');
    	      console.log(videoIdInput.value);
    	      const videoId = videoIdInput.value;
    	      getVideoThumbnails(videoIdInput.value);
    	});
      function getVideoThumbnails(videoid) {
	        fetch("https://vimeo.com/api/v2/video/"+videoid+".json")
	        .then(response => {
	          return response.text();
	        })
	        .then(data => {
	          const { thumbnail_large, thumbnail_medium, thumbnail_small } = JSON.parse(data)[0];
	          const thumb = JSON.parse(data)[0];
	          console.log(thumb.thumbnail_small);
	         console.log({ thumbnail_large, thumbnail_medium, thumbnail_small });
	         const thumbnailurl = "${thumbnail_small}";
	         console.log(thumbnailurl);
	          const small = "<img src="+thumb.thumbnail_large+">";
	         // const medium = `<img src="${thumbnail_medium}"/>`;
	         // const large = `<img src="${thumbnail_large}"/>`;
	          output.innerHTML = small;
	        })
	        .catch(error => {
	          console.log(error);
	        });
	      }

     /*  getVideo.addEventListener('click', e => {
        if (!isNaN(videoIdInput.value)) {
          getVideoThumbnails(videoIdInput.value);
        }
      }); */
      </script>
</body>
</html>