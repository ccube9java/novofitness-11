<!DOCTYPE html>
<html lang="zxx">
   <head>
      <!-- top header -->
      <title>NovoFitness</title>
      <%@ include file="/WEB-INF/handlingJsps/userTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/userSession.jsp"%>
      <!-- close top header -->
   </head>
   <body>
      <div class="page_loader"></div>
      <!-- Top header start -->
      <%@ include file="/WEB-INF/handlingJsps/userHeader.jsp"%>
      <!-- Top header end -->
      <!-- Sub banner start -->
      <div class="category-heading">
         <div class="container-fluid">
            <div class="categort-title">
               <h1>Shopping Cart</h1>
            </div>
         </div>
      </div>
      <section class="category-cart">
         <div class="container">
            <div class="row ">
               <div class="col-md-9 col-xs-12">
                  <div class="row nov_detcourse">
                     <div class="col-md-12 col-xs-6 pad0">
                        <h2>Courses in Cart</h2>
                     </div>
                  </div>
                  <c:choose>
                     <c:when test="${ not empty myCartCourseList }">
                        <c:forEach items="${ myCartCourseList  }" var="myCartCourse">
                           <div class="row nov_detcart">
                              <div class="col-md-2 col-xs-6">
                                 <c:choose>
                                    <c:when
                                       test="${ not empty myCartCourse.trainerCourseInfo.trainerCourseInfo.courseCoverImageInfo.courseCoverImageViewPath }">
                                       <img
                                          src="${ myCartCourse.trainerCourseInfo.trainerCourseInfo.courseCoverImageInfo.courseCoverImageViewPath }">
                                    </c:when>
                                    <c:otherwise>
                                       <img src="assets/userAssets/imges/cart-img-2.png">
                                    </c:otherwise>
                                 </c:choose>
                              </div>
                              <div class="col-md-8 col-xs-6">
                                 <h4>${ myCartCourse.trainerCourseInfo.trainerCourseInfo.courseName }
                                    <br> <small>${ myCartCourse.trainerCourseInfo.trainerInfo.trainerFName }
                                    ${ myCartCourse.trainerCourseInfo.trainerInfo.trainerLName }
                                    </small>
                                 </h4>
                              </div>
                              <div class="col-md-2 col-xs-6">
                                 <h3>&#36; ${ myCartCourse.trainerCourseInfo.trainerCourseInfo.coursePrice }</h3>
                                 <h6 class="text-center">
                                    <button class="btn btn-remove btn-sm"
                                       onclick="userCartListRemove(${ myCartCourse.cartTrainerCourseUserId }, ${ myCartCourse.cartTrainerCourseUserIsActive })">Remove
                                    </button>
                                 </h6>
                              </div>
                           </div>
                        </c:forEach>
                     </c:when>
                     <c:otherwise>
                        <div class="row nov_detcart">Your Cart is Empty</div>
                     </c:otherwise>
                  </c:choose>
               </div>
               <div class="col-md-3 col-xs-12">
                  <div class="cart_shop">
                     <c:if test="${ not empty alertSuccessMessage }">
                        <div class="alert alert-success">${ alertSuccessMessage }</div>
                     </c:if>
                     <c:if test="${ not empty alertFailMessage }">
                        <div class="alert alert-danger">
                           <fmt:message key="label.voucherCardInvalid" />
                        </div>
                     </c:if>
                     <h4>Total amount</h4>
                     <div class="row">
                        <div class="col-xs-12">
                           <div class="check_page">
                              <c:choose>
                                 <c:when
                                    test="${ not empty myCartListTotalPriceAfterCouponCode }">
                                    <div class="col-xs-12">
                                       <h3>&#36; ${ myCartListTotalPrice }</h3>
                                    </div>
                                    <div class="col-xs-12">
                                       <h3>- &#36; ${ couponCodeAmount }</h3>
                                    </div>
                                    <div class="col-xs-12">
                                       <hr>
                                    </div>
                                    <div class="col-xs-12">
                                       <h3>&#36; ${ myCartListTotalPriceAfterCouponCode }</h3>
                                    </div>
                                 </c:when>
                                 <c:otherwise>
                                    <div class="col-xs-12">
                                       <h3>&#36; ${ myCartListTotalPrice }</h3>
                                    </div>
                                 </c:otherwise>
                              </c:choose>
                              <c:choose>
                                 <c:when test="${ not empty myCartCourseList }">
                                    <a href="userPaymentCheckout"><button
                                       class="btn btn-danger">Checkout</button></a>
                                 </c:when>
                              </c:choose>
                              <%-- <form:form action="moveUserToPaymentCheckout" modelAttribute="userPaymentCheckoutWraspper">
                                 <div class="row">
                                 <c:choose>
                                 	<c:when
                                 		test="${ not empty myCartListTotalPriceAfterCouponCode }">
                                 		<div class="col-xs-12">
                                 		<h3>&#36; ${ myCartListTotalPrice }</h3>
                                 		</div>
                                 		<div class="col-xs-12">
                                 		<h3>- &#36; ${ couponCodeAmount }</h3>
                                 		</div>
                                 		<div class="col-xs-12"><hr></div>
                                 		<div class="col-xs-12"><h3>&#36; ${ myCartListTotalPriceAfterCouponCode }</h3></div>
                                 	</c:when>
                                 	<c:otherwise>
                                 		<div class="col-xs-12"><h3>&#36; ${ myCartListTotalPrice }</h3></div>
                                 	</c:otherwise>
                                 </c:choose>
                                 
                                 <form:hidden path="myCartListTotalPrice"
                                 	value="${ myCartListTotalPrice }" />
                                 <form:hidden path="myCartCourseList"
                                 	value="${ myCartCourseList }" />
                                 <form:hidden path="couponCodeId"
                                 	value="${ couponCodeId }" />
                                 	
                                 <form:hidden path="userId"
                                 	value="${ userSession.userId }" />
                                 
                                 <div class="col-xs-12">
                                 <c:choose>
                                 	<c:when test="${ not empty myCartCourseList }">
                                 		<button class="btn btn-danger" type="submit">Checkout</button>
                                 	</c:when>
                                 </c:choose>
                                 </div>
                                 </div>
                                 </form:form> --%>
                           </div>
                        </div>
                        <div class="col-xs-12">
                           <div class="check_page">
                              <form:form action="applyCouponCode" method="post"
                                 modelAttribute="couponCodePaymentWrapper">
                                 <div class="row">
                                 <div class="col-xs-12">
                                 <label class="couponApply">Coupon Code</label>
                                 </div>
                                    <div class="col-xs-8">
                                       <div class="form-group">
                                          <form:hidden path="myCartListTotalPrice"
                                             value="${ myCartListTotalPrice }" />
                                          <form:hidden path="userId" value="${ userSession.userId }" />
                                          <form:input path="couponCodeValue" id="voucherCardCodeInput"
                                             class="form-control" placeholder="Apply Coupon"/>
                                          <!-- <input type="text"> -->
                                       </div>
                                    </div>
                                    <div class="col-xs-4">
                                       <div class="form-group">
                                          <button type="submit" id="voucherCardCodeButton"
                                             class="btn btn-warning">Apply</button>
                                       </div>
                                    </div>
                                 </div>
                                 <%-- <div class="row">
                                    <form:errors style="margin: 0px 15px;"
                                                                                      path="voucherCodeValue" class="error" />
                                    </div> --%>
                              </form:form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <%--   <div class="col-xs-12 user-coupon-code">
                  <div class="row">
                   <div class="col-xs-12">
                  	<h5>Do you have Coupon Code ?</h5>
                  </div>
                  <div class="col-xs-3">
                  
                  <!-- <button type="button" class="btn btn-success btn-lg btn-block"
                  value="Voucher" id="voucherCard">
                  Coupon Code
                  </button>
                  </div>
                  <div class="col-xs-9"> -->
                  <div id="voucherCardDisplay">
                    <form:form action="applyVoucherCard"
                       method="post"
                       modelAttribute="voucherCardPaymentWrapper">
                       <div class="row">
                          <div
                             class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                             <div class="form-group">
                                <form:hidden
                                   path="userSubscriptionPlanOrderNumber"
                                   value="${ userSubscriptionPlanDetails.userSubscriptionPlanOrderNumber }" />
                                <form:hidden path="userId"
                                   value="${ userSession.userId }" />
                                <form:input path="voucherCodeValue"
                                   id="voucherCardCodeInput"
                                   class="form-control" />
                                   <input type="text">
                             </div>
                          </div>
                          <div
                             class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                             <div class="form-group">
                                <button
                                   type="submit" id="voucherCardCodeButton"
                                   class="btn btn-warning">
                                   Apply
                                </button>
                             </div>
                          </div>
                       </div>
                       <div class="row">
                          <form:errors style="margin: 0px 15px;"
                             path="voucherCodeValue" class="error" />
                       </div>
                    </form:form>
                  </div>
                  </div>
                  </div>
                  </div> --%>
            </div>
         </div>
      </section>
      <!---sports elearning section end here--->
      <!-- Footer start -->
      <%@ include file="/WEB-INF/handlingJsps/userFooter.jsp"%>
      <!-- Footer end -->
      <%@ include file="/WEB-INF/handlingJsps/userLowerFooter.jsp"%>
      <script type="text/javascript">
         function userCartListRemove(cartTrainerCourseUserId, cartListStatus) {
         	//alert("hi..");
         	var userCartListData = { 
         			cartTrainerCourseUserId : cartTrainerCourseUserId,
         			cartListStatus : cartListStatus
         		}
         	 $.ajax({
                 url: 'userCartListRemove',
                 type: 'post',
                 data: userCartListData,
                 success: function(response) {
                     //alert('success::'+response);
                     if(response=='added') {
                    	 //alert("Course is added to your WatchList");
                   	  swal({
               			  title: "Successfully Added!",
               			  text: "Course is added to your Cart!",
               			  icon: "success",
               			});
                     } else if(response=='fail'){
                    	 //alert("Course is fail to update. Please try again");
                   	  swal({
                 			  title: "Failed!",
                 			  text: "Course is fail to update. Please try again!",
                 			  icon: "warning",
                 			});
                     } else if(response=='removed'){
                   	  swal({
               			  title: "Successfully Removed!",
               			  text: "Course is removed to your Cart!",
               			  icon: "success",
               			});
                    	 //alert("Course is removed from your WatchList")
                     }
                   location.reload();
                   }
             });      
         }
      </script>
      <!-- Custom javascript -->
   </body>
</html>