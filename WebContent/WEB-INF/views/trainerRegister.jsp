<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>NovoFitness Trainer Register</title>
      <%@ include file="/WEB-INF/handlingJsps/adminTopHeader.jsp"%>
       <%@ include file="/WEB-INF/handlingJsps/trainerSessionLogout.jsp"%>
      <style>
         section.login_content {
         background: #ffffff;
         padding: 30px 30px 20px;
         border-radius: 2px;
         }
         button.btn.btn-default.submit.btn-log-in {
         background: #272c33;
         color: #fff;
         margin-top: 16px;
         }	
         a.to_register {
         color: #007bff;
         }
      </style>
   </head>
   <body class="logindetail">
      <div class="sufee-login d-flex align-content-center flex-wrap">
         <div class="container">
            <div class="login-content-trainer">
               <div class="login-form" id="Register-parts">
                   <div class="novo-logo">
                         <a href="userHome" class="clearfix alpha-logo"> <img
                        src="assets/adminAssets/images/Nova-fitnesslogo.png" alt="white-logo">
                     </a>
                     </div>
                  <div class="login-txt">
                     <h2>
                        Trainer 
                        <fmt:message key="label.register" />
                     </h2>
                  </div>
                  <form:form action="registerTrainer" method="post" modelAttribute="trainerRegisterWrapper" enctype="multipart/form-data">
                     <div class="row">
                        <div class="col-md-12 col-lg-12">
                           <div class="form-group">
                              <label>
                                 <fmt:message key="label.firstName" />
                                 <span class="error" style="float: right; margin-top: 0px;">*</span>
                              </label>
                              <fmt:message key="label.enterFirstName" var="enterFirstName"/>
                              <form:input path="trainerFName" class="form-control" placeholder="${ enterFirstName }" />
                              <form:errors path="trainerFName" class="error" />
                           </div>
                       
                           <div class="form-group">
                               <label>
                                 <fmt:message key="label.lastName" />
                                 <span class="error" style="float: right; margin-top: 0px;">*</span>
                              </label>
                             
                              <fmt:message key="label.enterLastName" var="enterLastName"/>
                              <form:input path="trainerLName" class="form-control" placeholder="${ enterLastName }" />
                              <form:errors path="trainerLName" class="error" />
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12 col-lg-12">
                           <div class="form-group">
                             <label>
                                 <fmt:message key="label.email" />
                                 <span class="error" style="float: right; margin-top: 0px;">*</span>
                              </label>
                            
                              <fmt:message key="label.enterEmail" var="enterEmail"/>
                              <form:input path="trainerEmail" class="form-control" placeholder="${ enterEmail }" />
                              <form:errors path="trainerEmail" class="error" />
                           </div>
                        </div>
                     </div>
                     <!---end of the row here--->
                     <div class="row">
                        <div class="col-md-12 col-lg-12">
                           <div class="form-group">
                              <label>
                                 <fmt:message key="label.password" />
                                 <span class="error" style="float: right; margin-top: 0px;">*</span>
                              </label>
                              <fmt:message key="label.enterPassword" var="enterPassword"/>
                              <form:password path="trainerPassword" class="form-control" placeholder="${ enterPassword }" />
                              <form:errors path="trainerPassword" class="error" />
                           </div>
                        
                           <div class="form-group">
                              <label>
                                 <fmt:message key="label.confirmPassword" />
                                 <span class="error" style="float: right; margin-top: 0px;">*</span>
                              </label>
                              <fmt:message key="label.enterConfirmPassword" var="enterConfirmPassword"/>
                              <form:password path="trainerConfirmPassword" class="form-control" placeholder="${ enterConfirmPassword }" />
                              <form:errors path="trainerConfirmPassword" class="error" />
                           </div>
                        </div>
                     </div>
                     <!--end of the row here-->
                     <div class="row">
                        <div class="col-md-12 col-lg-12">
                           <div class="form-group">
                              <label>
                                 <fmt:message key="label.idCopy" />
                              </label>
                               <form:input type="file" path="trainerIDCopyInfo" class="form-control file" data-browse-on-zone-click="true" />
                               <form:errors path="trainerIDCopyInfo" class="error" />
                              <!-- <input id="input-b2" name="input-b1" type="file" class="file" data-browse-on-zone-click="true"> -->
                           </div>
                        
                           <div class="form-group">
                               <label>
                                 <fmt:message key="label.certificates" />
                              </label>
                               <form:input type="file" path="trainerCertificateInfo" class="form-control file" data-browse-on-zone-click="true" />
                               <form:errors path="trainerCertificateInfo" class="error" />
                              <!-- <input id="input-b1" name="input-b1" type="file" class="file" data-browse-on-zone-click="true"> -->
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-9">
                           <div class="checkbox accept-check">
                              <!-- <input type="checkbox"> -->
                                <form:checkbox path="trainerAcceptTermsCondition" />  
                              <fmt:message key="label.acceptTermsCondition" /><span class="error" style="margin-top: 0px;">*</span><br>
                              <form:errors path="trainerAcceptTermsCondition" class="error" />
                           </div>
                        </div>
                        
                        <%-- <div class="col-md-3">
                           <div class="form-group">
                              <div class="pwd-btn">
                                 <a href="trainerLogin" id="forgotBtn" class="forget_pwd_new">
                                    <fmt:message key="label.login" />
                                    
                                 </a>
                              </div>
                           </div>
                        </div> --%>
                        
                     </div>
                     <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30 btns-registerss">
                        <fmt:message key="label.register" />
                        <!-- Trainer -->
                     </button>
                            <div class="pwd-btn-registers">
                                 <a href="trainerLogin" id="forgotBtn" class="forget_pwd_new forgets-registers ">
                                    Already a member ? Login here
                                 </a>
                              </div>
                  </form:form>
               </div>
            </div>
         </div>
      </div>
      <script src="assets/adminAssets/vendors/jquery/dist/jquery.min.js"></script>
      <script
         src="assets/adminAssets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
   </body>
</html>