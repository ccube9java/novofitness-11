<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>NovoFitness Trainer Management</title>
      <%@ include file="/WEB-INF/handlingJsps/adminTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/adminSession.jsp"%>
      <c:choose>
         <c:when test="${ adminSession.adminRole.roleId == 2 }">
            <c:redirect url="adminDashboard" />
         </c:when>
      </c:choose>
   </head>
   <body>
      <!-- Left Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminSidebar.jsp"%>
      <!-- Left Panel -->
      <!-- Right Panel -->
      <div id="right-panel" class="right-panel">
         <!-- Header-->
         <%@ include file="/WEB-INF/handlingJsps/adminHeader.jsp"%>
         <!-- /header -->
         <div class="breadcrumbs">
            <div class="col-sm-4">
               <div class="page-header float-left">
                  <div class="page-title">
                     <h1>Course Management</h1>
                  </div>
               </div>
            </div>
            <div class="col-sm-4"></div>
         </div>
         <div class="content mt-3">
            <div class="animated fadeIn">
               <div class="row">
                  <div class="col-md-12">
                     <div class="card">
                        <div class="card-header">
                           <strong class="card-title">List Of Course</strong>
                          
                        </div>
                        <div class="card-body" id="userlist">
                           <table id="trainers"
                              class="table table-striped table-bordered">
                              <thead>
                                 <tr>
                                    <th>
                                       <fmt:message key="label.srNo" />
                                    </th>
                                    <th>
                                       <fmt:message key="label.courseName" />
                                    </th>
                                    <th>
                                       <fmt:message key="label.category" />
                                    </th>
                                   <%--  <th>
                                       <fmt:message key="    " />
                                       Image
                                    </th> --%>
                                    <th>
                                      Price
                                    </th>
                                    
                                    <th>
                                       <fmt:message key="label.action" />
                                    </th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <c:choose>
                                    <c:when test="${ not empty courseList }">
                                       <c:forEach items="${ courseList }" var="course" varStatus="loop">
                                          <tr>
                                             <td>${ loop.index+1 }</td>
                                             <td>${ course.trainerCourseInfo.courseName }</td>
                                             <td>${ course.trainerCourseInfo.courseCategoryInfo.categoryName }</td>
                                            <!--  <td> </td> -->
                                             <td>${ course.trainerCourseInfo.coursePrice }</td>
                                             
                                              <td class="center-align">
                                              
                                                <a href="adminCourseView/${course.trainerCourseId}"><i class="fa fa-eye"></i></a> 
                                                
                                               <a href=""><i class="fa fa-trash" title="Delete" onclick="deleteTrainerCourse(${course.trainerCourseId})" ></i></a>
                                             </td>
                                          </tr>
                                       </c:forEach>
                                    </c:when>
                                 </c:choose>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- .animated -->
         </div>
         <!-- .content -->
      </div>
      <!-- /#right-panel -->
      <!-- Right Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminLowerFooter.jsp"%>
      <script>
         $(document).ready(function() {
         	$('#datepicker2').datepicker({
         		uiLibrary : 'bootstrap'
         	});
         
         	$('#fa-calendar2').click(function() {
         		$("#datepicker2").focus();
         	});
         });
      </script>
      <script>
         $(document).ready(function() {
         	$('#datepicker3').datepicker({
         		uiLibrary : 'bootstrap'
         	});
         
         	$('#fa-calendar3').click(function() {
         		$("#datepicker3").focus();
         	});
         });
      </script>
       <script type="text/javascript">
         function changeTrainerStatus(checkboxElem,id,status) {
         	var trainerData = { 
         			trainerId : id,
         			trainerStatus : status
         		}
         	 $.ajax({
         	        type: "POST",
         	        url: "changeTrainerStatus",
         	        data: trainerData,
         	        success: function (result) {
         	        	if(result=="success"){
         	        		alert("Trainer status changed successfully.");
         	        	} 
         	        	if(result=="fail"){
         	        		alert("Fail to change Trainer status. Please try again ");
         	        	} 

         	        	location.reload();
         	        },
         	        error: function (result) {
         	        	location.reload();
         	        }
         	    });
         }
      </script>
      <script type="text/javascript">
         function deleteTrainerCourse(id) {
         	var courseData = { 
         			trainerCourseId : id,
         	}
         	$.ajax({
                 type: "POST",
                 url: "deleteTrainerCourse",
                 data: courseData,
                 success: function (result) {
                	 //alert(result);
                	 if(result=="success"){
               		alert("Course deleted successfully.");
               	} 
               	if(result=="fail"){
               		alert("Fail to delete Course. Please try again ");
               	} 
                 	location.reload();
                 },
                 error: function (result) {
                	 //alert(result);
                 	location.reload();
                 }
             });
         }
      </script>
   </body>
</html>