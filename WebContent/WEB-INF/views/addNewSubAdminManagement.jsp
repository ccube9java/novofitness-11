<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>
         NovoFitness 
         <fmt:message
            key="label.subAdminManagement" />
      </title>
      <%@ include file="/WEB-INF/handlingJsps/adminTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/adminSession.jsp"%>
      <c:choose>
         <c:when test="${ adminSession.adminRole.roleId == 2 }">
            <c:redirect url="adminDashboard" />
         </c:when>
      </c:choose>
   </head>
   <body>
      <!-- Left Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminSidebar.jsp"%>
      <!-- Left Panel -->
      <!-- Right Panel -->
      <div id="right-panel" class="right-panel">
         <!-- Header-->
         <%@ include file="/WEB-INF/handlingJsps/adminHeader.jsp"%>
         <!-- /header -->
         <div class="breadcrumbs">
            <div class="col-sm-4">
               <div class="page-header float-left">
                  <div class="page-title">
                     <h1>Add Sub-admin</h1>
                  </div>
               </div>
            </div>
            <div class="col-sm-8">
               <div class="page-header float-right">
                  <div class="page-title">
                     <ol class="breadcrumb text-right">
                        <li><a href="adminSubAdminManagement"><b>Back</b></a></li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
         <div class="content mt-3">
            <div class="animated fadeIn">
               <div class="row">
                  <div class="col-md-12">
                     <div class="card">
                        <div class="card-header">
                           <strong>Add Sub-admin</strong>
                           <c:if test="${ not empty alertSuccessMessage }">
                              <div class="alert alert-success">${ alertSuccessMessage }</div>
                           </c:if>
                           <c:if test="${ not empty alertSuccessMessagecategoryCreationSuccess }">
                              <div class="alert alert-success">
                                 <fmt:message key="label.subAdminAddSuccess" />
                              </div>
                           </c:if>
                           <c:if test="${ not empty alertFailMessage }">
                              <div class="alert alert-danger">${ alertFailMessage }</div>
                           </c:if>
                           <c:if test="${ not empty alertFailMessagecategoryCreationFail }">
                              <div class="alert alert-danger">
                                 <fmt:message key="label.subAdminCreationFail" />
                              </div>
                           </c:if>
                        </div>
                        
                        
                        <div class="card-body card-block">
                           <form:form action="createNewSubAdmin" method="post" modelAttribute="adminCreateNewSubAdminWrapper" >
                             
                              <div class="row">
                              
                                 <div class="col-lg-6 col-md-6 ">
                                    <div class="form-group">
                                       <label for="subAdminFName" class="form-control-label">
                                          <fmt:message key="label.firstName" />
                                          <span class="error">*</span>
                                       </label>
                                       <form:input path="subAdminFName" class="form-control" id="company"
                                          placeholder="Enter First Name"/>
                                       <form:errors path="subAdminFName" class="error" />
                                    </div>
                                 </div>
                                 
                                  <div class="col-lg-6 col-md-6 ">
                                    <div class="form-group">
                                       <label for="subAdminLName" class="form-control-label">
                                          <fmt:message key="label.lastName" />
                                          <span class="error">*</span>
                                       </label>
                                       <form:input path="subAdminLName" class="form-control" id="company"
                                          placeholder="Enter Last Name"/>
                                       <form:errors path="subAdminLName" class="error" />
                                    </div>
                                 </div>
                              </div>
                              
                               <div class="row">
                              
                                 <div class="col-lg-6 col-md-6 ">
                                    <div class="form-group">
                                       <label for="subAdminEmail" class="form-control-label">
                                          <fmt:message key="label.enterEmail" />
                                          <span class="error">*</span>
                                       </label>
                                       <form:input path="subAdminEmail" class="form-control" id="company"
                                          placeholder="Enter Email"/>
                                       <form:errors path="subAdminEmail" class="error" />
                                    </div>
                                 </div>
                                 
                                 <div class="col-lg-6 col-md-6 ">
                                    <div class="form-group">
                                       <label for="subAdminPhoneNumber" class="form-control-label">
                                          <fmt:message key="label.ContactNumber" />
                                          <span class="error">*</span>
                                       </label>
                                       <form:input path="subAdminPhoneNumber" class="form-control" id="company"
                                          placeholder="Enter Contact Number"/>
                                       <form:errors path="subAdminPhoneNumber" class="error" />
                                    </div>
                                 </div>
                                 
                                  <!-- <div class="col-lg-6 col-md-6 ">
                              <div class="form-group">
                              <label for="vat" class=" form-control-label">
                              Upload Images</label>
                              <input type="file" id="file-input" name="file-input" class="form-control-file">
                              </div>
                        </div> -->
                        </div>
                                 
                                  <div class="row">
                              
                                 <div class="col-lg-6 col-md-6 ">
                                    <div class="form-group">
                                       <label for="subAdminPassword" class="form-control-label">
                                          <fmt:message key="label.enterPassword" />
                                          <span class="error">*</span>
                                       </label>
                                       <form:password path="subAdminPassword" class="form-control" id="company"
                                          placeholder="Password"/>
                                       <form:errors path="subAdminPassword" class="error" />
                                    </div>
                                 </div>
                                 
                                  <div class="col-lg-6 col-md-6 ">
                                    <div class="form-group">
                                       <label for="subAdminConfirmPassword" class="form-control-label">
                                          <fmt:message key="label.confirmPassword" />
                                          <span class="error">*</span>
                                       </label>
                                       <form:password path="subAdminConfirmPassword" class="form-control" id="company"
                                          placeholder="Confirm Password"/>
                                       <form:errors path="subAdminConfirmPassword" class="error" />
                                    </div>
                                 </div>
                              </div> 
                              
                               <div class="card">
                  <div class="card-header"><strong>Assign Rolls</strong></div>
                  <div class="card-body card-block">
                   
                   <div class="row">
                   
                        <div class="col-lg-4 col-md-4 ">                      
                         <form:checkbox path="subAdminAccessForUserManagement"/>
                          <label class="checkbox-inline">User/Learner Management
                          </label>
                        </div>
                        
                         <div class="col-lg-4 col-md-4 ">                      
                        <form:checkbox path="subAdminAccessForTrainerManagement"/>
                        <label class="checkbox-inline">Trainer Management
                        </label>
                        </div>
                        
						<div class="col-lg-4 col-md-4 ">
                         <form:checkbox path="subAdminAccessForContentManagement"/>
                            <label class="checkbox-inline">Content Management
                          </label>
                        </div>

                                          
                     </div>
                     
                     <div class="row">
                   
                        <div class="col-lg-4 col-md-4 ">  
                        <form:checkbox path="subAdminAccessForCategoryManagement"/>   
                          <label class="checkbox-inline">Category Management
                          </label>
                        </div>
                        
                         <div class="col-lg-4 col-md-4 ">                      
                       <form:checkbox path="subAdminAccessForNewsletterManagement"/>
                        <label class="checkbox-inline">Newsletter Management
                        </label>
                        </div>
                        
						<div class="col-lg-4 col-md-4 ">
						<form:checkbox path="subAdminAccessForReviewsRatingsManagement"/>
                         
                            <label class="checkbox-inline">Reviews Ratings Management
                          </label>
                        </div>

                                           
                     </div>
                     
                       <div class="row">
                   
                        <div class="col-lg-4 col-md-4 ">                      
                         <form:checkbox path="subAdminAccessForFAQsManagement"/>
                          <label class="checkbox-inline">FAQs Management
                          </label>
                        </div>
                        
                         <div class="col-lg-4 col-md-4 ">                      
                       <form:checkbox path="subAdminAccessForSubAdminManagement"/>
                        <label class="checkbox-inline">Sub-Admin Management
                        </label>
                        </div>
                        
						<div class="col-lg-4 col-md-4 ">
                         <form:checkbox path="subAdminAccessForGeneralManagement"/>
                            <label class="checkbox-inline">General Management
                          </label>
                        </div>

                              
                                   
                     </div>  
                     
                     <div class="row">
                         <div class="col-lg-4 col-md-4 ">
                         <form:checkbox path="subAdminAccessForCourseManagement"/>
                            <label class="checkbox-inline">Courses Management
                          </label>
                        </div> 
                        
                         <div class="col-lg-4 col-md-4 ">
                          <form:checkbox path="subAdminAccessForPaymentManagement"/>
                            <label class="checkbox-inline">Payment Management
                          </label>
                        </div>
                        
                         <div class="col-lg-4 col-md-4 ">
                          <form:checkbox path="subAdminAccessForReportsManagement"/>
                            <label class="checkbox-inline">Reports Management
                          </label>
                        </div>  
                     
                     </div>
                     
                     
                                     
         </div>
         </div>
    
    <div class="row">
            <div class="col-md-12">
               <button type="submit" class=" btn-save btn btn-success btn-sm">
               <fmt:message key="label.addNewSubAdmin" />
               </button>
            </div>
          </div>
                        
       </form:form>                  
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- .animated -->
         </div>
         <!-- .content -->
      </div>
      <!-- Right Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminLowerFooter.jsp"%>
      <script>
         jQuery(document).ready(function() {
             jQuery(".standardSelect").chosen({
                 disable_search_threshold: 10,
                 no_results_text: "Oops, nothing found!",
                 width: "100%"
             });
         });
      </script>
   </body>
</html>

