<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>NovoFitness Trainer Courses</title>
      <%@ include file="/WEB-INF/handlingJsps/trainerTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/trainerSession.jsp"%>
      <script src="assets/ckeditor/ckeditor.js"></script>
      <style type="text/css">
         #progressBars1 {
         display: none!important;
         }
      </style>
   </head>
   <body>
      <!-- Left Panel Sidebar-->
      <%@ include file="/WEB-INF/handlingJsps/trainerSidebar.jsp"%>
      <div id="right-panel" class="right-panel">
         <!-- Right Panel Header-->
         <%@ include file="/WEB-INF/handlingJsps/trainerHeader.jsp"%>
         <div class="breadcrumbs">
            <div class="col-sm-4">
               <div class="page-header float-left">
                  <div class="page-title">
                     <h1>Edit Course</h1>
                  </div>
               </div>
            </div>
            <div class="col-sm-8">
               <div class="page-header float-right">
                  <div class="page-title">
                     <ol class="breadcrumb text-right">
                        <li><a href="trainerCourseManagement"><b>Back</b></a></li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
         <div class="content mt-3">
            <div class="animated fadeIn">
               <div class="card">
                  <div class="card-header">
                     <strong>Edit Course</strong>
                     <c:if test="${ not empty alertSuccessMessage }">
                        <div class="alert alert-success">${ alertSuccessMessage }</div>
                     </c:if>
                     <c:if test="${ not empty alertSuccessMessageForUpdatingCourse }">
                        <div class="alert alert-success">${ alertSuccessMessageForUpdatingCourse }</div>
                     </c:if>
                     <c:if test="${ not empty alertSuccessMessageForAddingCourseFAQ }">
                        <div class="alert alert-success">${ alertSuccessMessageForAddingCourseFAQ }</div>
                     </c:if>
                     <c:if
                        test="${ not empty alertSuccessMessageForAddingCourseChapter }">
                        <div class="alert alert-success">${ alertSuccessMessageForAddingCourseChapter }</div>
                     </c:if>
                     <c:if test="${ not empty alertFailMessage }">
                        <div class="alert alert-danger">${ alertFailMessage }</div>
                     </c:if>
                     <c:if test="${ not empty alertFailMessageForUpdatingCourse }">
                        <div class="alert alert-danger">${ alertFailMessageForUpdatingCourse }
                        </div>
                     </c:if>
                     <c:if test="${ not empty alertFailMessageForAddingCourseFAQ }">
                        <div class="alert alert-danger">${ alertFailMessageForAddingCourseFAQ }
                        </div>
                     </c:if>
                     <c:if test="${ not empty alertFailMessageForAddingCourseChapter }">
                        <div class="alert alert-danger">${ alertFailMessageForAddingCourseChapter }
                        </div>
                     </c:if>
                     <!-- <div class="chapter-parts">
                        <button class="btn warning btn-addchapters" data-toggle="modal" data-target="#myModalChapter">
                        Add Chapter</button>
                        </div> -->
                  </div>
                  <div class="card-body card-block">
                     <form:form action="updateCourseInfo" method="post"
                        modelAttribute="trainerCourseWrapper"
                        enctype="multipart/form-data">
                        <div class="row">
                           <div class="col-lg-6 col-md-6 ">
                              <div class="form-group">
                                 <label for="courseName" class=" form-control-label">
                                    <fmt:message
                                       key="label.courseName" />
                                    <span class="error">*</span>
                                 </label>
                                 <form:hidden path="trainerCourseId"
                                    value="${ trainerCourseDetails.trainerCourseId }" />
                                 <form:hidden path="courseId"
                                    value="${ trainerCourseDetails.trainerCourseInfo.courseId }" />
                                 <form:hidden path="trainerId"
                                    value="${ trainerCourseDetails.trainerInfo.trainerId }" />
                                 <form:input path="courseName" class="form-control"
                                    id="company"
                                    value="${ trainerCourseDetails.trainerCourseInfo.courseName }" />
                                 <form:errors path="courseName" class="error" />
                              </div>
                           </div>
                           <div class="col-lg-6 col-md-6 ">
                              <label for="selectLg" class=" form-control-label">Course
                              Category <span class="error">*</span>
                              </label>
                              <form:select path="categoryId" id="selectLg"
                                 class="form-control">
                                 <form:option value="">
                                    <fmt:message key="label.selectMainCategory" />
                                 </form:option>
                                 <c:choose>
                                    <c:when test="${not empty categoryList}">
                                       <c:forEach items="${ categoryList }" var="category">
                                          <form:option value="${ category.categoryId }"
                                             selected="${ category.categoryId == trainerCourseDetails.trainerCourseInfo.courseCategoryInfo.categoryId ? 'selected':'' }">${ category.categoryName }</form:option>
                                       </c:forEach>
                                    </c:when>
                                 </c:choose>
                              </form:select>
                              <form:errors path="categoryId" class="error" />
                           </div>
                        </div>
                        <div class="row">
                           <!-- <div class="col-lg-6 col-md-6 ">
                              <div class="form-group">
                                 <label for="sel1"> Subcategory</label> 
                                 <select
                                    class="form-control">
                                    <option>select</option>
                                 </select>
                              </div>
                              </div> -->
                           <div class="col-lg-6 col-md-6 ">
                              <div class="form-group">
                                 <label for="coursePrice" class=" form-control-label">Course
                                 Price <span class="error">*</span>
                                 </label>
                                 <form:input path="coursePrice" class="form-control"
                                    id="company"
                                    value="${ trainerCourseDetails.trainerCourseInfo.coursePrice }" />
                                 <form:errors path="coursePrice" class="error" />
                              </div>
                           </div>
                           <div class="col-lg-6 col-md-6 ">
                              <div class="form-group">
                                 <label for="coursePrice" class=" form-control-label">Course
                                 Language <span class="error">*</span>
                                 </label>
                                 <form:input path="courseLanguage" class="form-control"
                                    id="company" value="${ trainerCourseDetails.trainerCourseInfo.courseLanguage }" />
                                 <form:errors path="courseLanguage" class="error" />
                              </div>
                           </div>
                        </div>
                        <!-- rows -->
                        <div class="row">
                           <div class="col-lg-6 col-md-6 ">
                              <div class="form-group">
                                 <label for="courseDescription" class=" form-control-label">
                                    Course Description <!-- <span class="error">*</span> -->
                                 </label>
                                 <%-- <form:input path="courseDescription" class="form-control"
                                    id="company" value="${ trainerCourseDetails.trainerCourseInfo.courseDescription }" /> --%>
                                 <%-- <textarea id="courseDescription" name="courseDescription" class="form-control" rows="2">${ trainerCourseDetails.trainerCourseInfo.courseDescription }</textarea>
                                    <form:errors path="courseDescription" class="error" /> --%>
                                 <textarea id="description" name="courseDescription"
                                    style="width: 100%;">${ trainerCourseDetails.trainerCourseInfo.courseDescription }</textarea>
                                 <script>
                                    CKEDITOR.replace( 'description', {
                                    	fullPage: true,
                                    	allowedContent: true,
                                    	extraPlugins: 'wysiwygarea',
                                    	width: '100%'
                                    });
                                 </script>
                              </div>
                           </div>
                           <div class="col-lg-6 col-md-6 ">
                              <div class="form-group">
                                 <label for="courseContent" class=" form-control-label">Course
                                 Content <span class="error">*</span>
                                 </label>
                                 <%-- <form:input path="courseContent" class="form-control"
                                    id="company" value="${ trainerCourseDetails.trainerCourseInfo.courseContent }" /> --%>
                                 <%-- <textarea id="courseContent" name="courseContent" class="form-control" rows="2">${ trainerCourseDetails.trainerCourseInfo.courseContent }</textarea>
                                    <form:errors path="courseContent" class="error" /> --%>
                                 <textarea id="coursecontents" name="courseContent"
                                    style="width: 100%;">${ trainerCourseDetails.trainerCourseInfo.courseContent }</textarea>
                                 <script>
                                    CKEDITOR.replace( 'coursecontents', {
                                    	fullPage: true,
                                    	allowedContent: true,
                                    	extraPlugins: 'wysiwygarea',
                                    	width: '100%'
                                    });
                                 </script>
                              </div>
                           </div>
                        </div>
                        <hr>
                        <div class="row">
                           <div class="col-md-12">
                              <button type="submit" class="btn btn-success btn-sm">Update</button>
                           </div>
                        </div>
                     </form:form>
                     <hr>
                     <div class="row"></div>
                     <hr>
                     <div class="row">
                        <div class="col-lg-6 col-md-6 ">
                           <div class="form-group">
                              <label for="courseCoverImage" class=" form-control-label">
                              Cover Image <span class="error">*</span>
                              </label> <br>
                              <div class="row">
                                 <c:choose>
                                    <c:when
                                       test="${ not empty trainerCourseDetails.trainerCourseInfo.courseCoverImageInfo }">
                                       <div class="col-md-4">
                                          <img class="thumnail-img-2 img-thumnail"
                                             src="${ trainerCourseDetails.trainerCourseInfo.courseCoverImageInfo.courseCoverImageViewPath }">
                                          <button class="btn delete-button" data-title="Delete"
                                             onclick="deleteCoverImage(${ trainerCourseDetails.trainerCourseInfo.courseCoverImageInfo.courseCoverImageId }, ${ trainerCourseDetails.trainerCourseInfo.courseId })">
                                          <i class="fa fa-trash" aria-hidden="true" title="Delete"></i>
                                          </button>
                                       </div>
                                    </c:when>
                                    <c:otherwise>
                                       <div class="col-md-4">No Cover Image Available. Please
                                          upload Cover Image
                                       </div>
                                    </c:otherwise>
                                 </c:choose>
                              </div>
                              <br>
                              <form action="uploadCourseCoverImage"
                                 id="uploadCourseCoverImageForm" method="post"
                                 enctype="multipart/form-data">
                                 <input
                                    type=hidden class="form-control" name="trainerId"
                                    value="${ trainerCourseDetails.trainerInfo.trainerId }" />
                                 <input
                                    type=hidden class="form-control" name="courseId"
                                    value="${ trainerCourseDetails.trainerCourseInfo.courseId }" />
                                 <input type="file" accept="image/*" name="courseCoverImage"
                                    id="courseCoverImage" class="form-control-file" />
                                 <br>
                                 <div class="form-group">
                                    <button class="btn btn-primary" type="submit" id="uploadCourseCoverImageButton">Upload</button>
                                 </div>
                              </form>
                              <!-- Bootstrap Progress bar -->
                              <div class="progress" id="progressBars" style="display: none;">
                                 <div id="progressBar" class="progress-bar progress-bar-success" role="progressbar"
                                    aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">0%</div>
                              </div>
                              <!-- Alert -->
                              <div id="alertMsg" style="color: green;font-size: 18px; display: none;"></div>
                           </div>
                        </div>
                        <div class="col-lg-6 col-md-6 ">
                           <div class="form-group">
                              <label for="company" class=" form-control-label">Upload
                              More Images <span class="error">*</span>
                              </label> <br>
                              <div class="row">
                                 <c:choose>
                                    <c:when test="${ not empty trainerCourseOtherImagesList }">
                                       <c:forEach items="${ trainerCourseOtherImagesList }"
                                          var="trainerCourseOtherImages">
                                          <div class="col-md-4">
                                             <img class="thumnail-img-2 img-thumnail thumbmoreimages"
                                                src="${ trainerCourseOtherImages.courseImagesInfo.courseImagesViewPath }">
                                             <button class="btn delete-button" data-title="Delete"
                                                onclick="deleteOtherImage(${ trainerCourseOtherImages.courseOtherImagesId })">
                                             <i class="fa fa-trash" aria-hidden="true" title="Delete"></i>
                                             </button>
                                          </div>
                                       </c:forEach>
                                    </c:when>
                                    <c:otherwise>
                                       <div class="col-md-4">No Images Available. Please
                                          upload more Images.
                                       </div>
                                    </c:otherwise>
                                 </c:choose>
                              </div>
                              <br>
                              <%-- 	<form:input type="file" path="courseOtherImages"
                                 id="file-input" name="file-input" class="form-control-file"
                                 multiple="multiple" />
                                 <form:errors path="courseOtherImages" class="error" /> --%>
                              <form action="uploadCourseOtherImages"
                                 id="uploadCourseOtherImagesForm" method="post"
                                 enctype="multipart/form-data">
                                 <input
                                    type=hidden class="form-control" name="trainerId"
                                    value="${ trainerCourseDetails.trainerInfo.trainerId }" />
                                 <input
                                    type=hidden class="form-control" name="courseId"
                                    value="${ trainerCourseDetails.trainerCourseInfo.courseId }" />
                                 <input type="file" accept="image/*" name="courseOtherImages"
                                    id="courseOtherImages" class="form-control-file" multiple="multiple"/>
                                 <br>
                                 <div class="form-group">
                                    <button class="btn btn-primary" type="submit" id="uploadCourseOtherImagesButton">Upload</button>
                                 </div>
                              </form>
                              <!-- Bootstrap Progress bar -->
                              <div class="progress" id="progressBars1">
                                 <div id="progressBar1" class="progress-bar progress-bar-success" role="progressbar"
                                    aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">0%</div>
                              </div>
                              <!-- Alert -->
                              <div id="alertMsg1" style="color: green; font-size: 18px; display: none;"></div>
                           </div>
                        </div>
                     </div>
                     <hr>
                     <div class="row">
                        <div class="col-lg-6 col-md-6 ">
                           <div class="form-group">
                              <label for="company" class=" form-control-label">Upload
                              Cover Video <span class="error">*</span>
                              </label><br>
                              <div class="row">
                                 <c:choose>
                                    <c:when
                                       test="${ not empty trainerCourseDetails.trainerCourseInfo.courseCoverVideoInfo }">
                                       <div class="col-md-4">
                                          <iframe
                                             src="https://player.vimeo.com/video/${ trainerCourseDetails.trainerCourseInfo.courseCoverVideoInfo.courseCoverVideoVimeoName }"
                                             width="100%" height="100px" frameborder="0"
                                             webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                          <button class="btn delete-button" data-title="Delete"
                                             onclick="deleteCoverVideo(${ trainerCourseDetails.trainerCourseInfo.courseCoverVideoInfo.courseCoverVideoId }, ${ trainerCourseDetails.trainerCourseInfo.courseId })">
                                          <i class="fa fa-trash" aria-hidden="true" title="Delete"></i>
                                          </button>
                                       </div>
                                    </c:when>
                                    <c:otherwise>
                                       <div class="col-md-4">No Preview Video Available.
                                          Please upload Preview Video.
                                       </div>
                                    </c:otherwise>
                                 </c:choose>
                              </div>
                              <!--  rows -->
                              <%-- 	<form:input type="file" accept="video/mp4,video/x-m4v,video/*"
                                 path="courseCoverVideo" id="file-input" name="file-input"
                                 class="form-control-file" />
                                 <form:errors path="courseCoverVideo" class="error" /> --%>
                              <br>
                              <form action="uploadCourseCoverVideo"
                                 id="uploadCourseCoverVideoForm" method="post"
                                 enctype="multipart/form-data">
                                 <input
                                    type=hidden class="form-control" name="trainerId"
                                    value="${ trainerCourseDetails.trainerInfo.trainerId }" />
                                 <input
                                    type=hidden class="form-control" name="courseId"
                                    value="${ trainerCourseDetails.trainerCourseInfo.courseId }" />
                                 <input type="file" accept="video/mp4,video/x-m4v,video/*" name="courseCoverVideo"
                                    id="courseCoverVideo" class="form-control-file" />
                                 <br>
                                 <div class="form-group">
                                    <button class="btn btn-primary" type="submit" id="uploadCourseCoverVideoButton">Upload</button>
                                 </div>
                              </form>
                              <!-- Bootstrap Progress bar -->
                              <div class="progress" id="progressBars2" style="display: none;">
                                 <div id="progressBar2" class="progress-bar progress-bar-success" role="progressbar"
                                    aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">0%</div>
                              </div>
                              <!-- Alert -->
                              <div id="alertMsg2" style="color: green;font-size: 18px;display: none;"></div>
                           </div>
                        </div>
                        <%-- <div class="col-lg-6 col-md-6 ">
                           <div class="form-group">
                              <label for="company" class=" form-control-label">Upload
                              Videos <span class="error">*</span>
                              </label><br>
                              <div class="row">
                                 <c:choose>
                                    <c:when test="${ not empty trainerCourseOtherVideosList }">
                                       <c:forEach items="${ trainerCourseOtherVideosList }"
                                          var="trainerCourseOtherVideos">
                                          <c:choose>
                                             <c:when
                                                test="${ not empty trainerCourseOtherVideos.courseVideoInfo.courseVideoVimeoName }">
                                                <div class="col-md-4">
                                                   <iframe
                                                      src="https://player.vimeo.com/video/${ trainerCourseOtherVideos.courseVideoInfo.courseVideoVimeoName }"
                                                      width="100%" height="100px" frameborder="0"
                                                      webkitallowfullscreen mozallowfullscreen
                                                      allowfullscreen></iframe>
                                                   <button class="btn delete-button" data-title="Delete"
                                                      onclick="deleteCoverImage(${ userCompleteProfile.userProfileCoverImageInfo.userProfileCoverImageId }, ${ userCompleteProfile.userProfileId })">
                                                   <i class="fa fa-trash" aria-hidden="true"
                                                      title="Delete"></i>
                                                   </button>
                                                   <video controls>
                                                      <source src="${ trainerCourseOtherVideos.courseVideoInfo.courseVideoViewPath }" type="video/mp4">
                                                      <source src="${ trainerCourseOtherVideos.courseVideoInfo.courseVideoViewPath }" type="video/ogg">
                                                      </video>
                                                </div>
                                             </c:when>
                                          </c:choose>
                                       </c:forEach>
                                    </c:when>
                                    <c:otherwise>
                                       <div class="col-md-4">No Videos Available. Please
                                          upload more Videos.
                                       </div>
                                    </c:otherwise>
                                 </c:choose>
                                 <!-- <div class="col-md-4">
                                    </div> -->
                              </div>
                              <!-- rows -->
                              <br>
                              <form:input type="file" accept="video/mp4,video/x-m4v,video/*"
                                 path="courseOtherVideos" id="file-input" name="file-input"
                                 class="form-control-file" multiple="multiple" />
                              <form:errors path="courseOtherVideos" class="error" />
                           </div>
                           </div> --%>
                     </div>
                     <hr>
                     <%-- <div class="row">
                        <div class="col-lg-6 col-md-6 ">
                           <div class="form-group">
                              <label for="company" class=" form-control-label">Upload
                              PDF <span class="error">*</span>
                              </label><br>
                              <div class="row">
                                 <c:choose>
                                    <c:when test="${ not empty trainerCourseOtherPDFsList }">
                                       <c:forEach items="${ trainerCourseOtherPDFsList }"
                                          var="trainerCourseOtherPDFs">
                                          <div class="col-md-4">
                                             <div class="Certificates-img">
                                                <a
                                                   href="${ trainerCourseOtherPDFs.coursePDFInfo.coursePDFViewPath }"
                                                   target="_blank"> <img
                                                   src="assets/adminAssets/images/certificte-img.png"
                                                   class="img-responsive">
                                                </a>
                                                <button class="btn delete-button-pdf"
                                                   data-title="Delete"
                                                   onclick="deleteCoverImage(${ userCompleteProfile.userProfileCoverImageInfo.userProfileCoverImageId }, ${ userCompleteProfile.userProfileId })">
                                                <i class="fa fa-trash" aria-hidden="true"
                                                   title="Delete"></i>
                                                </button>
                                             </div>
                                          </div>
                                       </c:forEach>
                                    </c:when>
                                    <c:otherwise>
                                       <div class="col-md-4">No PDFs Available. Please
                                          upload more PDFs.
                                       </div>
                                    </c:otherwise>
                                 </c:choose>
                                 <!-- <input id="input-2" name="input2[]" type="file" class="file" multiple data-show-upload="true" data-show-caption="true"> -->
                              </div>
                              <!--  rows -->
                              <br>
                              <form:input type="file" accept="application/pdf"
                                 path="courseOtherPdfs" id="file-input" name="file-input"
                                 class="form-control-file" multiple="multiple" />
                              <form:errors path="courseOtherPdfs" class="error" />
                           </div>
                        </div>
                        </div>
                        <hr> --%>
                     <%-- <div class="row">
                        <div class="col-md-12">
                           <button type="submit" class="btn btn-success btn-sm">Update</button>
                        </div>
                        </div>
                        </form:form> --%>
                     <hr>
                     <!-- Chapter section start -->
                     <div class="row">
                        <form:form action="addNewTrainerCourseChapter" method="post"
                           modelAttribute="trainerCourseChapterWrapper">
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label for="courseChapterName" class=" form-control-label">
                                 Chapter Name <span class="error">*</span>
                                 </label>
                              </div>
                           </div>
                           <div class="col-md-5">
                              <div class="form-group">
                                 <!-- <textarea class="form-control" rows="2" id="comment"></textarea> -->
                                 <form:hidden path="trainerCourseId"
                                    value="${ trainerCourseDetails.trainerCourseId }" />
                                 <form:input path="trainerCourseChapterName"
                                    class="form-control" rows="2" />
                                 <form:errors path="trainerCourseChapterName" class="error" />
                              </div>
                           </div>
                           <div class="col-md-2">
                              <div class="form-group">
                                 <button type="submit" class="btn btn-success btn-sm ">Add
                                 Chapter</button>
                              </div>
                           </div>
                        </form:form>
                     </div>
                     <!-- rows -->
                     <hr>
                     <!-- FAQ List  -->
                     <div class="col-md-12">
                        <div class="card">
                           <div class="card-header">
                              <strong class="card-title">Chapter List</strong>
                           </div>
                           <div class="card-body">
                              <table id="trainers" class="table table-striped table-bordered">
                                 <thead>
                                    <tr>
                                       <th>
                                          <fmt:message key="label.srNo" />
                                       </th>
                                       <th>Chapter Name</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <c:choose>
                                       <c:when test="${ not empty trainerCourseChapterList }">
                                          <c:forEach items="${ trainerCourseChapterList }"
                                             var="trainerCourseChapter" varStatus="loop">
                                             <tr>
                                                <td>${ loop.index+1 }</td>
                                                <td>${ trainerCourseChapter.trainerCourseChapterName }</td>
                                                <td class="center-align">
                                                   <a
                                                      href="trainersCourseChapterUpdate/${ trainerCourseChapter.trainerCourseChapterId }"><i
                                                      class="fa fa-edit" title="Edit Chapter Name"></i></a> <a
                                                      href="trainersCourseChapterView/${ trainerCourseChapter.trainerCourseChapterId }"><i
                                                      class="fa fa-plus" title="Add New Lesson"></i></a> 
                                                   <!-- <a href="#" > -->
                                                   <i class="fa fa-trash" title="Delete" onclick="deleteTrainersCourseChapter(${ trainerCourseChapter.trainerCourseChapterId })"></i>
                                                   <!-- </a> -->
                                                </td>
                                             </tr>
                                          </c:forEach>
                                       </c:when>
                                    </c:choose>
                                 </tbody>
                              </table>
                           </div>
                           <!-- card body -->
                        </div>
                        <!-- Card -->
                     </div>
                     <!-- md12  -->
                     <!--  <hr> -->
                     <!--  FAQ section start -->
                     <div class="row">
                        <form:form action="addNewTrainerCourseFAQ" method="post"
                           modelAttribute="trainerCourseFAQWrapper">
                           <div class="col-md-5">
                              <div class="form-group">
                                 <label for="courseName" class=" form-control-label">
                                 FAQ Question <span class="error">*</span>
                                 </label>
                                 <!-- <textarea class="form-control" rows="2" id="comment"></textarea> -->
                                 <form:hidden path="trainerCourseId"
                                    value="${ trainerCourseDetails.trainerCourseId }" />
                                 <form:textarea path="trainerCourseFAQQuestion"
                                    class="form-control" rows="2" />
                                 <form:errors path="trainerCourseFAQQuestion" class="error" />
                              </div>
                           </div>
                           <div class="col-md-5">
                              <div class="form-group">
                                 <label for="courseName" class=" form-control-label">
                                 FAQ Answer <span class="error">*</span>
                                 </label>
                                 <form:textarea path="trainerCourseFAQAnswer"
                                    class="form-control" rows="2" />
                                 <form:errors path="trainerCourseFAQAnswer" class="error" />
                                 <!-- <textarea class="form-control" rows="2" id="comment"></textarea> -->
                              </div>
                           </div>
                           <div class="col-md-2">
                              <div class="form-group">
                                 <button type="submit"
                                    class="btn btn-success btn-sm btn-add-questions">Add
                                 Question</button>
                              </div>
                           </div>
                        </form:form>
                     </div>
                     <!-- rows -->
                     <hr>
                     <!-- FAQ List  -->
                     <div class="col-md-12">
                        <div class="card">
                           <div class="card-header">
                              <strong class="card-title">FAQ List</strong>
                           </div>
                           <div class="card-body">
                              <table id="trainers" class="table table-striped table-bordered">
                                 <thead>
                                    <tr>
                                       <th>
                                          <fmt:message key="label.srNo" />
                                       </th>
                                       <th>Question</th>
                                       <th>Answer</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <c:choose>
                                       <c:when test="${ not empty trainerCourseFAQList }">
                                          <c:forEach items="${ trainerCourseFAQList }"
                                             var="trainerCourseFAQ" varStatus="loop">
                                             <tr>
                                                <td>${ loop.index+1 }</td>
                                                <td>${ trainerCourseFAQ.trainerCourseFAQQuestion }</td>
                                                <td>${ trainerCourseFAQ.trainerCourseFAQAnswer }</td>
                                                <td class="center-align"><a
                                                   href="trainersCourseFAQUpdate/${ trainerCourseFAQ.trainerCourseFAQId }"><i
                                                   class="fa fa-edit"></i></a> <a href="#"
                                                   onclick="deleteTrainersCourseFAQ(${ trainerCourseFAQ.trainerCourseFAQId })"><i
                                                   class="fa fa-trash" title="Delete"></i></a></td>
                                             </tr>
                                          </c:forEach>
                                       </c:when>
                                    </c:choose>
                                 </tbody>
                              </table>
                           </div>
                           <!-- card body -->
                        </div>
                        <!-- Card -->
                     </div>
                     <!-- md12  -->
                     <!--  FAQ section ends -->
                  </div>
               </div>
               <!-- .animated -->
            </div>
            <!-- .content -->
         </div>
         <!-- .content -->
      </div>
      <!-- /#right-panel -->
      <%@ include file="/WEB-INF/handlingJsps/trainerLowerFooter.jsp"%>
      <script type="text/javascript">
         $(function() {
         	$('#uploadCourseCoverImageButton').click(function(e) {
         		$('#progressBars').show();
         		$('#alertMsg').show();
         		e.preventDefault();
         		//Disable submit button
         		$(this).prop('disabled',true);
         		//alert("hi..");
         		var form = document.forms[1];
         		///console.log(form);
         		var formData = new FormData(form);
         		//console.log(formData)
         		// Ajax call for file uploaling
         		var ajaxReq = $.ajax({
         			url : 'uploadCourseCoverImage',
         			type : 'POST',
         			data : formData,
         			cache : false,
         			contentType : false,
         			processData : false,
         			xhr: function(){
         				//Get XmlHttpRequest object
         				 var xhr = $.ajaxSettings.xhr() ;
         				
         				//Set onprogress event handler 
         				 xhr.upload.onprogress = function(event){
         					var perc = Math.round((event.loaded / event.total) * 100);
         					$('#progressBar').text(perc + '%');
         					$('#progressBar').css('width',perc + '%');
         				 };
         				 return xhr ;
         			},
         			beforeSend: function( xhr ) {
         				//Reset alert message and progress bar
         				$('#alertMsg').text('');
         				$('#progressBar').text('');
         				$('#progressBar').css('width','0%');
                        }
         		});
         
         		// Called on success of file upload
         		ajaxReq.done(function(msg) {
         			//alert("success");
         			$('#alertMsg').text(msg);
         			$('#courseCoverImage').val('');
         			$('button[type=submit]').prop('disabled',false);
         			location.reload();
         		});
         		
         		// Called on failure of file upload
         		ajaxReq.fail(function(jqXHR) {
         			//alert("fail");
         			$('#alertMsg').text(jqXHR.responseText+'('+jqXHR.status+
         					' - '+jqXHR.statusText+')');
         			$('button[type=submit]').prop('disabled',false);
         		});
         	});
         });
      </script>
      <script type="text/javascript">
         $(function() {
         	$('#progressBars1').show();
         	$('#alertMsg1').show();
         	$('#uploadCourseOtherImagesButton').click(function(e) {
         		e.preventDefault();
         		//Disable submit button
         		$(this).prop('disabled',true);
         		//alert("hi..");
         		var form = document.forms[2];
         		//console.log(form);
         		var formData = new FormData(form);
         		//console.log(formData)
         		// Ajax call for file uploaling
         		var ajaxReq = $.ajax({
         			url : 'uploadCourseOtherImages',
         			type : 'POST',
         			data : formData,
         			cache : false,
         			contentType : false,
         			processData : false,
         			xhr: function(){
         				//Get XmlHttpRequest object
         				 var xhr = $.ajaxSettings.xhr() ;
         				
         				//Set onprogress event handler 
         				 xhr.upload.onprogress = function(event){
         					var perc = Math.round((event.loaded / event.total) * 100);
         					$('#progressBar1').text(perc + '%');
         					$('#progressBar1').css('width',perc + '%');
         				 };
         				 return xhr ;
         			},
         			beforeSend: function( xhr ) {
         				//Reset alert message and progress bar
         				$('#alertMsg1').text('');
         				$('#progressBar1').text('');
         				$('#progressBar1').css('width','0%');
                        }
         		});
         
         		// Called on success of file upload
         		ajaxReq.done(function(msg) {
         			//alert("success");
         			$('#alertMsg1').text(msg);
         			$('#courseOtherImages').val('');
         			$('button[type=submit]').prop('disabled',false);
         			location.reload();
         		});
         		
         		// Called on failure of file upload
         		ajaxReq.fail(function(jqXHR) {
         			//alert("fail");
         			$('#alertMsg1').text(jqXHR.responseText+'('+jqXHR.status+
         					' - '+jqXHR.statusText+')');
         			$('button[type=submit]').prop('disabled',false);
         		});
         	});
         });
      </script>
      <script type="text/javascript">
         $(function() {
         	$('#uploadCourseCoverVideoButton').click(function(e) {
         		$('#progressBars2').show();
         		$('#alertMsg2').show();
         		e.preventDefault();
         		//Disable submit button
         		$(this).prop('disabled',true);
         		//alert("hi..");
         		var form = document.forms[3];
         		//console.log(form);
         		var formData = new FormData(form);
         		//console.log(formData)
         		// Ajax call for file uploaling
         		var ajaxReq = $.ajax({
         			url : 'uploadCourseCoverVideo',
         			type : 'POST',
         			data : formData,
         			cache : false,
         			contentType : false,
         			processData : false,
         			xhr: function(){
         				//Get XmlHttpRequest object
         				 var xhr = $.ajaxSettings.xhr() ;
         				
         				//Set onprogress event handler 
         				 xhr.upload.onprogress = function(event){
         					var perc = Math.round((event.loaded / event.total) * 100);
         					$('#progressBar2').text(perc + '%');
         					$('#progressBar2').css('width',perc + '%');
         				 };
         				 return xhr ;
         			},
         			beforeSend: function( xhr ) {
         				//Reset alert message and progress bar
         				$('#alertMsg2').text('');
         				$('#progressBar2').text('');
         				$('#progressBar2').css('width','0%');
                        }
         		});
         
         		// Called on success of file upload
         		ajaxReq.done(function(msg) {
         			//alert("success");
         			$('#alertMsg2').text(msg);
         			$('#courseVideoImage').val('');
         			$('button[type=submit]').prop('disabled',false);
         			location.reload();
         		});
         		
         		// Called on failure of file upload
         		ajaxReq.fail(function(jqXHR) {
         			//alert("fail");
         			$('#alertMsg2').text(jqXHR.responseText+'('+jqXHR.status+
         					' - '+jqXHR.statusText+')');
         			$('button[type=submit]').prop('disabled',false);
         		});
         	});
         });
      </script>
      <script type="text/javascript">
         function deleteTrainersCourseChapter(id) {
          swal({
             	  title: 'Are you sure?',
             	  text: "Course Chapter will permanently deleted !",
             	  type: 'warning',
             	  showCancelButton: true,
             	  confirmButtonColor: '#3085d6',
             	  cancelButtonColor: '#d33',
             	  confirmButtonText: 'Yes, delete it!',
             	  closeOnConfirm: false
            	},
           		function() {
             	$.ajax({
                     type: "POST",
                     url: "deleteTrainersCourseChapter",
                     data: "trainerCourseChapterId="+id,
                     success: function (result) {
                     }
                 })
                 .done(function(data) {
                swal("Deleted!", "Course Chapter deleted successfully!", "success");
                location.reload();
              })
              .error(function(data) {
                swal("Oops", "We couldn't connect to the server!", "error");
              });
           	});
         }
      </script>
      <script type="text/javascript">
         function deleteTrainersCourseFAQ(id) {
          swal({
            	  title: 'Are you sure?',
            	  text: "Course FAQ will permanently deleted !",
            	  type: 'warning',
            	  showCancelButton: true,
            	  confirmButtonColor: '#3085d6',
            	  cancelButtonColor: '#d33',
            	  confirmButtonText: 'Yes, delete it!',
            	  closeOnConfirm: false
           	},
          		function() {
            	$.ajax({
                    type: "POST",
                    url: "deleteTrainersCourseFAQ",
                    data: "trainerCourseFAQId="+id,
                    success: function (result) {
                    }
                })
                .done(function(data) {
               swal("Deleted!", "Course FAQ deleted successfully!", "success");
               location.reload();
             })
             .error(function(data) {
               swal("Oops", "We couldn't connect to the server!", "error");
             });
          	});
         }
      </script>
      <script type="text/javascript">
         function deleteCoverImage(id, courseId) {
          swal({
           	  title: 'Are you sure?',
           	  text: "Course Cover Image will permanently deleted !",
           	  type: 'warning',
           	  showCancelButton: true,
           	  confirmButtonColor: '#3085d6',
           	  cancelButtonColor: '#d33',
           	  confirmButtonText: 'Yes, delete it!',
           	  closeOnConfirm: false
          	},
         		function() {
           	$.ajax({
                   type: "POST",
                   url: "deleteCoverImage",
                   data: "coverImageId="+id+"&courseId="+courseId,
                   success: function (result) {
                   }
               })
               .done(function(data) {
              swal("Deleted!", "Course Cover Image deleted successfully!", "success");
              location.reload();
            })
            .error(function(data) {
              swal("Oops", "We couldn't connect to the server!", "error");
            });
         	});
         }
      </script>
      <script type="text/javascript">
         function deleteOtherImage(id) {
          swal({
              	  title: 'Are you sure?',
              	  text: "Course Image will permanently deleted !",
              	  type: 'warning',
              	  showCancelButton: true,
              	  confirmButtonColor: '#3085d6',
              	  cancelButtonColor: '#d33',
              	  confirmButtonText: 'Yes, delete it!',
              	  closeOnConfirm: false
             	},
            		function() {
              	$.ajax({
                      type: "POST",
                      url: "deleteOtherImage",
                      data: "otherImageId="+id,
                      success: function (result) {
                      }
                  })
                  .done(function(data) {
                 swal("Deleted!", "Course Image deleted successfully!", "success");
                 location.reload();
               })
               .error(function(data) {
                 swal("Oops", "We couldn't connect to the server!", "error");
               });
            	});
         }
      </script>
      <script type="text/javascript">
         function deleteCoverVideo(id, courseId) {
          swal({
              	  title: 'Are you sure?',
              	  text: "Course Cover Video will permanently deleted !",
              	  type: 'warning',
              	  showCancelButton: true,
              	  confirmButtonColor: '#3085d6',
              	  cancelButtonColor: '#d33',
              	  confirmButtonText: 'Yes, delete it!',
              	  closeOnConfirm: false
             	},
            		function() {
              	$.ajax({
                      type: "POST",
                      url: "deleteCoverVideo",
                      data: "coverVideoId="+id+"&courseId="+courseId,
                      success: function (result) {
                      }
                  })
                  .done(function(data) {
                 swal("Deleted!", "Course Cover Video deleted successfully!", "success");
                 location.reload();
               })
               .error(function(data) {
                 swal("Oops", "We couldn't connect to the server!", "error");
               });
            	});
         }
      </script>
   </body>
</html>