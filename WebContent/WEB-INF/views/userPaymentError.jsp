<!DOCTYPE html>
<html lang="zxx">
   <head>
      <!-- top header -->
      <title>NovoFitness</title>
      <%@ include file="/WEB-INF/handlingJsps/userTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/userSession.jsp"%>
      <!-- close top header -->
   </head>
   <body>
      <div class="page_loader"></div>
      <!-- Top header start -->
      <%@ include file="/WEB-INF/handlingJsps/userHeader.jsp"%>
      <!-- Top header end -->
      <!-- Sub banner start -->
      <div class="category-heading">
         <div class="container-fluid">
            <div class="categort-title">
               <h1>User Payment Error</h1>
               <%--  <h3>${userPaymentErrorMessage}</h3> --%>
            </div>
         </div>
      </div>
      <section class="category-cart-errors">
         <div class="container">
            <div id="notfound">
               <div class="notfound">
                  <div class="notfound-404">
                     <h1>404</h1>
                  </div>
                  <h2>Oops, The Page you are looking for can't be found!</h2>
                  <form class="notfound-search">
                     <input type="text" placeholder="Search...">
                     <button type="button">Search</button>
                  </form>
                  <a href="#"><span class="arrow"></span>Go Back</a>
               </div>
            </div>
         </div>
      </section>
      <!---sports elearning section end here--->
      <!-- Footer start -->
      <%@ include file="/WEB-INF/handlingJsps/userFooter.jsp"%>
      <!-- Footer end -->
      <%@ include file="/WEB-INF/handlingJsps/userLowerFooter.jsp"%>
      <!-- Custom javascript -->
   </body>
</html>