<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>NovoFitness Admin Dashboard</title>
      <%@ include file="/WEB-INF/handlingJsps/adminTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/adminSession.jsp"%>
   </head>
   <body>
   		 <!-- Left Panel Sidebar-->
      <%@ include file="/WEB-INF/handlingJsps/adminSidebar.jsp"%>
      <div id="right-panel" class="right-panel">
      <!-- Right Panel Header-->
         <%@ include file="/WEB-INF/handlingJsps/adminHeader.jsp"%>
         <div class="breadcrumbs">
            <div class="col-sm-4">
               <div class="page-header float-left">
                  <div class="page-title">
                     <h1>Dashboard</h1>
                  </div>
               </div>
            </div>
            <div class="col-sm-8">
               <div class="page-header float-right">
                  <div class="page-title"></div>
               </div>
            </div>
         </div>
         <div class="content mt-3">
            <div class="col-sm-6 col-lg-3">
               <div class="card text-white bg-flat-color-1">
                  <div class="card-body pb-0">
                     <h4 class="mb-0">
                        <span class="count">${ userCount }</span>
                     </h4>
                     <p class="text-light">Total Users </p>
                     <div class="chart-wrapper px-0" style="height: 70px;" >
                        <canvas id="widgetChart1"></canvas>
                     </div>
                  </div>
               </div>
            </div>
            <!--/.col-->
            <div class="col-sm-6 col-lg-3">
               <div class="card text-white bg-flat-color-2">
                  <div class="card-body pb-0">
                     <h4 class="mb-0">
                        <span class="count">${ trainerCount }</span>
                     </h4>
                     <p class="text-light">Total Trainers</p>
                     <div class="chart-wrapper px-0" style="height: 70px;" >
                        <canvas id="widgetChart2"></canvas>
                     </div>
                  </div>
               </div>
            </div>
            <!--/.col-->
            <div class="col-sm-6 col-lg-3">
               <div class="card text-white bg-flat-color-3">
                  <div class="card-body pb-0">
                     <h4 class="mb-0">
                        <span class="count">${ trainerCourseCount }</span>
                     </h4>
                     <p class="text-light">Total Courses Purchased</p>
                  </div>
                  <div class="chart-wrapper px-0" style="height: 70px;" >
                     <canvas id="widgetChart3"></canvas>
                  </div>
               </div>
            </div>
            <!--/.col-->
           <!--  <div class="col-sm-6 col-lg-3">
               <div class="card text-white bg-flat-color-4">
                  <div class="card-body pb-0">
                     <h4 class="mb-0">
                        <span class="count">10468</span>
                     </h4>
                     <p class="text-light">Content</p>
                     <div class="chart-wrapper px-3" style="height: 70px;" height="70">
                        <canvas id="widgetChart4"></canvas>
                     </div>
                  </div>
               </div>
            </div> -->
            <!--/.col-->
            <div class="col-xl-6"></div>
            <div class="col-xl-6">
               <!-- /# card -->
            </div>
<!--             <div class="col-xl-6 col-lg-6">
               <div class="card">
                  <div class="card-body">
                     <div class="stat-widget-one">
                        <div class="stat-icon dib">
                           <i class="ti-money text-success border-success"></i>
                        </div>
                        <div class="stat-content dib">
                           <div class="stat-text">Total User</div>
                           <div class="stat-digit">1,012</div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-xl-3 col-lg-6">
               <div class="card">
                  <div class="card-body">
                     <div class="stat-widget-one">
                        <div class="stat-icon dib">
                           <i class="ti-user text-primary border-primary"></i>
                        </div>
                        <div class="stat-content dib">
                           <div class="stat-text">New User</div>
                           <div class="stat-digit">961</div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-xl-3 col-lg-6">
               <div class="card">
                  <div class="card-body">
                     <div class="stat-widget-one">
                        <div class="stat-icon dib">
                           <i class="ti-layout-grid2 text-warning border-warning"></i>
                        </div>
                        <div class="stat-content dib">
                           <div class="stat-text">Total Trainer</div>
                           <div class="stat-digit">770</div>
                        </div>
                     </div>
                  </div>
               </div>
            </div> -->
         </div>
         <!-- .content -->
      </div>
      <!-- /#right-panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminLowerFooter.jsp"%>
      <script>
         (function($) {
         	"use strict";
         
         	jQuery('#vmap').vectorMap({
         		map : 'world_en',
         		backgroundColor : null,
         		color : '#ffffff',
         		hoverOpacity : 0.7,
         		selectedColor : '#1de9b6',
         		enableZoom : true,
         		showTooltip : true,
         		values : sample_data,
         		scaleColors : [ '#1de9b6', '#03a9f5' ],
         		normalizeFunction : 'polynomial'
         	});
         })(jQuery);
      </script>
      <script>
         $('.myDiv').click(function() { //button click class name is myDiv
         	e.stopPropagation();
         })
         
         $(function() {
         	$('.openDiv').click(function() {
         		$('.myDiv').show();
         
         	});
         	$(document).click(function() {
         		$('.myDiv').hide(); //hide the button
         
         	});
         });
      </script>
   </body>
</html>