<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>NovoFitness Trainer Courses</title>
      <%@ include file="/WEB-INF/handlingJsps/trainerTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/trainerSession.jsp"%>
   </head>
   <body>
      <!-- Left Panel Sidebar-->
      <%@ include file="/WEB-INF/handlingJsps/trainerSidebar.jsp"%>
      <div id="right-panel" class="right-panel">
         <!-- Right Panel Header-->
         <%@ include file="/WEB-INF/handlingJsps/trainerHeader.jsp"%>
         <div class="breadcrumbs">
            <div class="col-sm-4">
               <div class="page-header float-left">
                  <div class="page-title">
                     <h1> Payment Settlements   </h1>
                  </div>
               </div>
            </div>
            <div class="col-sm-4">
            </div>
         </div>
         <div class="content mt-3">
            <div class="animated fadeIn">
               <div class="row">
                  <div class="col-md-12">
                     <div class="card">
                        <div class="card-header">
                           <strong class="card-title">List Of Payment Settlements</strong>
                           <div class="right-logo">
                           </div>
                        </div>
                        <div class="card-body" id="userlist">
                           <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                              <thead>
                                 <tr>
                                  <th>
                                             <fmt:message key="label.srNo" />
                                          </th>
                                    <th>Course Name</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email id</th>
                                    <th>Total Amount</th>
                                    <th>My Contribution</th>
                                    <th>Payment Status</th>
                                    <th>View Detail</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <c:choose>
                                    <c:when test="${ not empty trainerCourseUserPaymentList }">
                                       <c:forEach items="${ trainerCourseUserPaymentList }" var="trainerCourseUserPayment" varStatus="loop">
                                          <tr>
                                          <td>${ loop.index+1 }</td>
                                             <td>${ trainerCourseUserPayment.trainerCourseInfo.trainerCourseInfo.courseName }</td>
                                             <td>${ trainerCourseUserPayment.userInfo.userFName }</td>
                                             <td>${ trainerCourseUserPayment.userInfo.userLName } </td>
                                             <td>${ trainerCourseUserPayment.userInfo.userEmail }</td>
                                             <td>$ ${ trainerCourseUserPayment.trainerCourseInfo.trainerCourseInfo.courseCost }</td>
                                             <td>$ ${ trainerCourseUserPayment.userPaymentTrainerContribution }</td>
                                             <td>
												<c:choose>
													<c:when test="${ trainerCourseUserPayment.userCoursePaymentIsAdminPaidTrainerContrubution }">
														Done
													</c:when>
													<c:otherwise>
														Pending
													</c:otherwise>
												</c:choose>
											</td>
                                             <td class="center-align"><a href="trainerCourseUserPaymentSettlementViewManagement/${ trainerCourseUserPayment.userCoursePaymentId }"><i class="fa fa-eye"></i></a></td>
                                          </tr>
                                       </c:forEach>
                                    </c:when>
                                 </c:choose>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- .animated -->
         </div>
         <!-- .content -->
      </div>
      <%@ include file="/WEB-INF/handlingJsps/trainerLowerFooter.jsp"%>
      <!-- /#right-panel -->
      <!-- <script>
         (function($) {
         	"use strict";
         
         	jQuery('#vmap').vectorMap({
         		map : 'world_en',
         		backgroundColor : null,
         		color : '#ffffff',
         		hoverOpacity : 0.7,
         		selectedColor : '#1de9b6',
         		enableZoom : true,
         		showTooltip : true,
         		values : sample_data,
         		scaleColors : [ '#1de9b6', '#03a9f5' ],
         		normalizeFunction : 'polynomial'
         	});
         })(jQuery);
         </script> -->
      <!--  <script>
         $('.myDiv').click(function() { //button click class name is myDiv
         	e.stopPropagation();
         })
         
         $(function() {
         	$('.openDiv').click(function() {
         		$('.myDiv').show();
         
         	});
         	$(document).click(function() {
         		$('.myDiv').hide(); //hide the button
         
         	});
         });
         </script> -->
      <script type="text/javascript">
         function deleteTrainerCourse(id) {
          var courseData = { 
         			trainerCourseId : id,
         	}
         	$.ajax({
                 type: "POST",
                 url: "deleteTrainerCourse",
                 data: courseData,
                 success: function (result) {
                	 /* alert(result); */
                	if(result=="success") {
                		swal({
                			  title: "Course deleted successfully!",
                			  text: "",
                			  icon: "success",
                			});
                		//swal("Course deleted successfully.", " ",  "success");
               		} 
               		if(result=="fail")  {
               			swal({
              			  title: "Fail to delete Course!",
              			  text: "Please try again",
              			  icon: "error",
              			});
               			//swal("Fail to delete Course.", "Please try again",  "error");
               			//alert("Fail to delete Course. Please try again ");
               		} 
                 	//location.reload();
                 },
                 error: function (result) {
                	// alert(result);
                 	//location.reload();
                 }
             }); 
         }
      </script>
   </body>
</html>