<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!DOCTYPE html>
<html lang="en">
   <head>
      <title>
         NovoFitness Admin 
         <fmt:message key="label.resetPassword" />
      </title>
      <%@ include file="/WEB-INF/handlingJsps/adminTopHeader.jsp"%>
   </head>
   <body class="bg-dark">
      <div class="container">
         <div class="logos">
            <h3>NovaFitness</h3>
         </div>
         <div class="card card-register mx-auto mt-5">
            <div class="card-header">
               <fmt:message key="label.resetPassword" />
            </div>
            <div class="card-body">
               <c:if test="${ not empty alertSuccessMessage }">
                  <div class="alert alert-success">${ alertSuccessMessage }</div>
               </c:if>
               <c:if test="${ not empty alertFailMessage }">
                  <div class="alert alert-danger">${ alertFailMessage }</div>
               </c:if>
               <c:if test="${ not empty alertFailMessageuserAccountVerificationFailed }">
					<div class="alert alert-danger"><fmt:message key="label.userAccountVerificationFailed" /></div>
				</c:if>
               <form:form action="resetAdminPassword" method="post" modelAttribute="adminResetPassword">
                  <div class="form-group">
                     <div class="form-row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <form:password path="adminPassword" class="form-control" placeholder="" />
                              <form:errors path="adminPassword" class="error" />
                           </div>
                        </div>
                        <div class="col-md-12">
                           <div class="form-group">
                              <form:password path="adminConfirmPassword" class="form-control" placeholder="" />
                              <form:errors path="adminConfirmPassword" class="error" />
                           </div>
                        </div>
                     </div>
                  </div>
                  <input type="submit" class="form-control loginsub" value="<fmt:message key="label.submit" />">
               </form:form>
               <div class="text-center">
                  <a class="d-block small mt-3" href="admin"><fmt:message key="label.login" /></a>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>