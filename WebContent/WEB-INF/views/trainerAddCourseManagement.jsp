<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>NovoFitness Trainer Courses</title>
      <%@ include file="/WEB-INF/handlingJsps/trainerTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/trainerSession.jsp"%>
      <script src="assets/ckeditor/ckeditor.js"></script>
   </head>
   <body>
      <!-- Left Panel Sidebar-->
      <%@ include file="/WEB-INF/handlingJsps/trainerSidebar.jsp"%>
      <div id="right-panel" class="right-panel">
         <!-- Right Panel Header-->
         <%@ include file="/WEB-INF/handlingJsps/trainerHeader.jsp"%>
         <div class="breadcrumbs">
            <div class="col-sm-4">
               <div class="page-header float-left">
                  <div class="page-title">
                     <h1>Course Management</h1>
                  </div>
               </div>
            </div>
            <div class="col-sm-8">
               <div class="page-header float-right">
                  <div class="page-title">
                     <ol class="breadcrumb text-right">
                        <li><a href="trainerCourseManagement"><b>Back</b></a></li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
         <div class="content mt-3">
            <div class="animated fadeIn">
               <div class="card">
                  <div class="card-header">
                     <strong>Add Course</strong>
                     <c:if test="${ not empty alertSuccessMessage }">
                        <div class="alert alert-success">${ alertSuccessMessage }</div>
                     </c:if>
                     <c:if test="${ not empty alertFailMessage }">
                        <div class="alert alert-danger">${ alertFailMessage }</div>
                     </c:if>
                     <c:if test="${ not empty alertFailForAddingNewCourse }">
                        <div class="alert alert-danger">
                           ${ alertFailForAddingNewCourse }
                        </div>
                     </c:if>
                     <!-- <div class="chapter-parts">
                        <button class="btn warning btn-addchapters" data-toggle="modal" data-target="#myModalChapter">
                        Add Chapter</button>
                        </div> -->
                  </div>
                  <!-- cards header -->
                  <div class="card-body card-block">
                     <form:form action="addNewCourse" method="post"
                        modelAttribute="trainerCourseWrapper"
                        enctype="multipart/form-data">
                        <div class="row">
                           <div class="col-lg-6 col-md-6 ">
                              <div class="form-group">
                                 <label for="courseName" class=" form-control-label">
                                    <fmt:message
                                       key="label.courseName" />
                                    <span class="error">*</span>
                                 </label>
                                 <form:hidden path="trainerId" value="${ trainerSession.trainerId }"/>
                                 <form:input path="courseName" class="form-control"
                                    id="company" placeholder="Enter Course Name" />
                                 <form:errors path="courseName" class="error" />
                              </div>
                           </div>
                           <div class="col-lg-6 col-md-6 ">
                              <label for="selectLg" class=" form-control-label">Course Category 
                              <span class="error">*</span></label>
                              <form:select path="categoryId" id="selectLg"
                                 class="form-control-lg form-control">
                                 <form:option value="">
                                    <fmt:message key="label.selectMainCategory" />
                                 </form:option>
                                 <c:choose>
                                    <c:when test="${not empty categoryList}">
                                       <c:forEach items="${ categoryList }" var="category">
                                          <form:option value="${ category.categoryId }">${ category.categoryName }</form:option>
                                       </c:forEach>
                                    </c:when>
                                 </c:choose>
                              </form:select>
                              <form:errors path="categoryId" class="error" />
                           </div>
                           <!-- lg 4 -->
                           <div class="col-lg-6 col-md-6 ">
                              <div class="form-group">
                                 <label for="coursePrice" class=" form-control-label">Course
                                 Price <span class="error">*</span></label>
                                 <form:input path="coursePrice" class="form-control"
                                    id="company" placeholder="Enter Course Price" />
                                 <form:errors path="coursePrice" class="error" />
                              </div>
                           </div>
                           <div class="col-lg-6 col-md-6 ">
                              <div class="form-group">
                                  <label for="coursePrice" class=" form-control-label">Course
                                 Language <span class="error">*</span>
                                 </label>
                                 <form:input path="courseLanguage" class="form-control"
                                    id="company" placeholder="Enter Course Language" />
                                 <form:errors path="courseLanguage" class="error" />
                              </div>
                           </div>
                        </div>
                        <hr>
                        <%-- <div class="row">
                           <div class="col-lg-6 col-md-6 ">
                           	<div class="form-group">
                           		<label for="courseDescription" class=" form-control-label">Course Description <span class="error">*</span></label>
                           		<form:input path="courseDescription" class="form-control"
                           			id="company" placeholder="Enter Course  Description" />
                           		
                           		<form:textarea path="courseDescription" class="form-control" rows="2"/>
                           		 <!-- <textarea class="form-control" rows="2" id=""></textarea> -->
                           		
                           		<form:errors path="courseDescription" class="error" />
                           	</div>
                           </div>
                           <div class="col-lg-6 col-md-6 ">
                           	<div class="form-group">
                           		<label for="courseContent" class=" form-control-label">Course
                           			Content <span class="error">*</span></label>
                           		<form:input path="courseContent" class="form-control"
                           			id="company" placeholder="Enter Course  Content" />
                           			<form:textarea path="courseContent" class="form-control" rows="2"/>
                           		<form:errors path="courseContent" class="error" />
                           	</div>
                           </div>
                           </div> --%>
                        <div class="row">
                           <div class="col-lg-12 col-md-12 ">
                              <label for="courseDescription" class=" form-control-label">
                                 Course Description <!-- <span class="error">*</span> -->
                              </label>
                              <textarea id="description" name="courseDescription" style="width:100%;"></textarea>
                              <script>
                                 CKEDITOR.replace( 'description', {
                                 	fullPage: true,
                                 	allowedContent: true,
                                 	extraPlugins: 'wysiwygarea',
                                 	width: '100%'
                                 });
                              </script> 
                           </div>
                           <!-- col-md-6 -->
                        </div>
                        <!-- rows -->
                        <br>
                        <div class="row">
                           <div class="col-lg-12 col-md-12 ">
                              <label for="courseContent" class=" form-control-label">
                                 Course
                                 Content <!-- <span class="error">*</span> -->
                              </label>
                              <textarea id="coursecontents" name="courseContent" style="width:100%;"></textarea>
                              <script>
                                 CKEDITOR.replace( 'coursecontents', {
                                 	fullPage: true,
                                 	allowedContent: true,
                                 	extraPlugins: 'wysiwygarea',
                                 	width: '100%'
                                 });
                              </script> 
                           </div>
                           <!-- col-md-6 -->
                        </div>
                        <!-- rows -->
                        <hr>
                        <%-- <div class="row">
                           <div class="col-lg-6 col-md-6 ">
                              <div class="form-group">
                                 <label for="vat" class=" form-control-label">Upload
                                 Cover Image <span class="error">*</span></label>
                                 <form:input type="file" accept="image/*"
                                    path="courseCoverImage" id="file-input" name="file-input"
                                    class="form-control-file" />
                                 <form:errors path="courseCoverImage" class="error" />
                              </div>
                           </div>
                           <div class="col-lg-6 col-md-6 ">
                              <div class="form-group">
                                 <label for="company" class=" form-control-label">Upload
                                 More Images <span class="error">*</span></label>
                                 <form:input type="file" path="courseOtherImages"
                                    id="file-input" name="file-input" class="form-control-file"
                                    multiple="multiple" />
                                 <form:errors path="courseOtherImages" class="error" />
                                 <!-- <input type="file" id="file-input" name="file-input"
                                    class="form-control-file"> -->
                              </div>
                           </div>
                        </div> --%>
                       <%--  <div class="row">
                           <div class="col-lg-6 col-md-6 ">
                              <div class="form-group">
                                 <label for="company" class=" form-control-label">Upload
                                 Course Preview Video <span class="error">*</span></label><br>
                                 <form:input type="file" accept="video/mp4,video/x-m4v,video/*"
                                    path="courseCoverVideo" id="file-input"
                                    name="file-input" class="form-control-file"
                                    />
                                 <form:errors path="courseCoverVideo" class="error" />
                              </div>
                           </div>
                            <div class="col-lg-6 col-md-6 ">
                              <div class="form-group">
                                 <label for="company" class=" form-control-label">Upload
                                 on-demand Videos <span class="error">*</span></label>
                                 <form:input type="file" accept="video/mp4,video/x-m4v,video/*"
                                    path="courseOtherVideos" id="file-input"
                                    name="file-input" class="form-control-file"
                                    multiple="multiple" />
                                 <form:errors path="courseOtherVideos" class="error" />
                              </div>
                              </div>
                        </div> --%>
                        <%-- <div class="row">
                           <div class="col-lg-6 col-md-6 ">
                              <div class="form-group">
                                 <label for="company" class=" form-control-label">Upload
                                 PDF /resources<span class="error">*</span></label><br>
                                 <form:input type="file" accept="application/pdf"
                                    path="courseOtherPdfs" id="file-input" name="file-input"
                                    class="form-control-file" multiple="multiple" />
                                 <form:errors path="courseOtherPdfs" class="error" />
                              </div>
                           </div>
                           </div> --%>
                        <hr>
                        <!-- <div class="progress">
					      <div id="progressBar" class="progress-bar progress-bar-success" role="progressbar"
					        aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">0%</div>
					    </div>
					
					    Alert
					    <div id="alertMsg" style="color: red;font-size: 18px;"></div>
                        <hr> -->
                        <div class="row">
                           <div class="col-md-12">
                              <button type="submit" class="btn btn-success btn-sm">Submit</button>
                           </div>
                        </div>
                     </form:form>
                  </div>
               </div>
               <!-- .animated -->
            </div>
            <!-- .content -->
         </div>
         <!-- .content -->
      </div>
      <!-- /#right-panel -->
      <%@ include file="/WEB-INF/handlingJsps/trainerLowerFooter.jsp"%>
<!--       <script type="text/javascript">
	$(function() {
		$('button[type=submit]').click(function(e) {
			e.preventDefault();
			//Disable submit button
			$(this).prop('disabled',true);
			
			
			var form = document.forms[0];
			var formData = new FormData(form);
				
			// Ajax call for file uploaling
			var ajaxReq = $.ajax({
				url : 'addNewCourse1',
				type : 'POST',
				data : formData,
				cache : false,
				contentType : false,
				processData : false,
				xhr: function(){
					//Get XmlHttpRequest object
					 var xhr = $.ajaxSettings.xhr() ;
					
					//Set onprogress event handler 
					 xhr.upload.onprogress = function(event){
						var perc = Math.round((event.loaded / event.total) * 100);
						$('#progressBar').text(perc + '%');
						$('#progressBar').css('width',perc + '%');
					 };
					 return xhr ;
				},
				beforeSend: function( xhr ) {
					//Reset alert message and progress bar
					$('#alertMsg').text('');
					$('#progressBar').text('');
					$('#progressBar').css('width','0%');
                }
			});

			// Called on success of file upload
			ajaxReq.done(function(msg) {
				/* $('#alertMsg').text(msg);
				$('input[type=file]').val('');
				$('button[type=submit]').prop('disabled',false); */
				window.location = "trainerCourseManagement";
			});
			
			// Called on failure of file upload
			ajaxReq.fail(function(jqXHR) {
				$('#alertMsg').text(jqXHR.responseText+'('+jqXHR.status+
						' - '+jqXHR.statusText+')');
				$('button[type=submit]').prop('disabled',false);
			});
		});
	});
</script> -->
      <script>
         (function($) {
         	"use strict";
         
         	jQuery('#vmap').vectorMap({
         		map : 'world_en',
         		backgroundColor : null,
         		color : '#ffffff',
         		hoverOpacity : 0.7,
         		selectedColor : '#1de9b6',
         		enableZoom : true,
         		showTooltip : true,
         		values : sample_data,
         		scaleColors : [ '#1de9b6', '#03a9f5' ],
         		normalizeFunction : 'polynomial'
         	});
         })(jQuery);
      </script>
      <script>
         $('.myDiv').click(function() { //button click class name is myDiv
         	e.stopPropagation();
         })
         
         $(function() {
         	$('.openDiv').click(function() {
         		$('.myDiv').show();
         
         	});
         	$(document).click(function() {
         		$('.myDiv').hide(); //hide the button
         
         	});
         });
      </script>
      <script>
         // Add the following code if you want the name of the file appear on select
         $(".custom-file-input").on("change", function() {
           var fileName = $(this).val().split("\\").pop();
           $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
         });
      </script>
   </body>
</html>