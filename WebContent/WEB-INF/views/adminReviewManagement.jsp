<%@ include file="/WEB-INF/handlingJsps/include.jsp"%>
<!doctype html>
<html class="no-js" lang="en">
   <head>
      <title>NovoFitness Trainer Management</title>
      <%@ include file="/WEB-INF/handlingJsps/adminTopHeader.jsp"%>
      <%@ include file="/WEB-INF/handlingJsps/adminSession.jsp"%>
      <c:choose>
         <c:when test="${ adminSession.adminRole.roleId == 2 }">
            <c:redirect url="adminDashboard" />
         </c:when>
      </c:choose>
   </head>
   <body>
      <!-- Left Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminSidebar.jsp"%>
      <!-- Left Panel -->
      <!-- Right Panel -->
      <div id="right-panel" class="right-panel">
         <!-- Header-->
         <%@ include file="/WEB-INF/handlingJsps/adminHeader.jsp"%>
         <!-- /header -->
         <div class="breadcrumbs">
            <div class="col-sm-4">
               <div class="page-header float-left">
                  <div class="page-title">
                     <h1>Manage Reviews</h1>
                  </div>
               </div>
            </div>
            <div class="col-sm-4"></div>
         </div>
         <div class="content mt-3">
            <div class="animated fadeIn">
               <div class="row">
                  <div class="col-md-12">
                     <div class="card">
                        <div class="card-header">
                           <strong class="card-title">List Of Reviews and Ratings</strong>
                          
                        </div>
                        <div class="card-body" id="userlist">
                           <table id="trainers"
                              class="table table-striped table-bordered">
                              <thead>
                                 <tr>
                                    <th>
                                       <fmt:message key="label.srNo" />
                                    </th>
                                    <th>
                                       <fmt:message key="label.firstName" />
                                    </th>
                                    <th>
                                       <fmt:message key="label.lastName" />
                                    </th>
                                     <th>Course Name</th>
                                    <th>
                                       <fmt:message key="label.email" />
                                    </th>
                                    <th>
                                       <fmt:message key="label.mobile" />
                                    </th>
                                    <th>
                                       <fmt:message key="label.userLocationStarRating" />
                                    </th>
                                    <th>
                                       <fmt:message key="label.status" />
                                    </th>
                                    <th>
                                       <fmt:message key="label.action" />
                                    </th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <c:choose>
                                    <c:when test="${ not empty reviewList }">
                                       <c:forEach items="${ reviewList }" var="review" varStatus="loop">
                                          <tr>
                                             <td>${ loop.index+1 }</td>
                                             <td>${ review.userInfo.userFName }</td>
                                             <td>${ review.userInfo.userLName }</td>
                                             <td>${ review.trainerCourseInfo.trainerCourseInfo.courseName }</td>
                                             <td>${ review.userInfo.userEmail} </td>
                                             <td>${ review.userInfo.userPhoneNo }</td>
                                             <td><span class="stars" data-rating="${ review.userCourseStarRating }" data-num-stars="5" ></span> </td>
                                             <td><label class="switch">
                                             	<input type="checkbox" 
                                                ${ review.userCourseReviewIsActive?'checked':'' } 
                                                onchange="changeReviewStatus(this,${ review.userCourseReviewId },${ review.userCourseReviewIsActive })" /> 
                                                <span class="slider round"></span></label></td>
                                              <td class="center-align">
                                                <a href="adminReviewView/${ review.userCourseReviewId }"><i class="fa fa-eye"></i></a> 
                                             </td>
                                          </tr>
                                       </c:forEach>
                                    </c:when>
                                 </c:choose>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- .animated -->
         </div>
         <!-- .content -->
      </div>
      <!-- /#right-panel -->
      <!-- Right Panel -->
      <%@ include file="/WEB-INF/handlingJsps/adminLowerFooter.jsp"%>
      <script type="text/javascript">
         $(function(){
            $('.stars').stars();
         }); 
      </script>
      <script>
         $(document).ready(function() {
         	$('#datepicker2').datepicker({
         		uiLibrary : 'bootstrap'
         	});
         
         	$('#fa-calendar2').click(function() {
         		$("#datepicker2").focus();
         	});
         });
      </script>
      <script>
         $(document).ready(function() {
         	$('#datepicker3').datepicker({
         		uiLibrary : 'bootstrap'
         	});
         
         	$('#fa-calendar3').click(function() {
         		$("#datepicker3").focus();
         	});
         });
      </script>
      
      <!-- FOR REVIEW STATUS -->
      
       <script type="text/javascript">
         function changeReviewStatus(checkboxElem,id,status) {
         	var reviewData = { 
         			reviewId : id,
         			reviewStatus : status
         		}
         	 $.ajax({
         	        type: "POST",
         	        url: "changeReviewStatus",
         	        data: reviewData,
         	        success: function (result) {
         	        	if(result=="success"){
         	        		alert("Trainer status changed successfully.");
         	        	} 
         	        	if(result=="fail"){
         	        		alert("Fail to change Trainer status. Please try again ");
         	        	} 

         	        	location.reload();
         	        },
         	        error: function (result) {
         	        	location.reload();
         	        }
         	    });
         }
      </script>
   </body>
</html>