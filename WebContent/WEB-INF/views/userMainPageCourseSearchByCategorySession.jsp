<!DOCTYPE html>
<html lang="zxx">
<head>
<!-- top header -->
<title>NovoFitness</title>
<%@ include file="/WEB-INF/handlingJsps/userTopHeader.jsp"%>
<%@ include file="/WEB-INF/handlingJsps/userSession.jsp"%>
<!-- close top header -->
</head>
<body>
	<div class="page_loader"></div>
	<!-- Top header start -->
	<%@ include file="/WEB-INF/handlingJsps/userHeader.jsp"%>
	<!-- Top header end -->
	<!-- Sub banner start -->
	<section class="sub-banner">
		<div class="container">
			<div class="row">
				<div class="breadcrumb-area">
					<div class="headet-txt">
						<h1>
							THE FUTURE OF<br> <span> FITNESS </span> and <span>HEALTH</span>
						</h1>
					</div>
					<p class="banner-txt">FIND your EXPERT now</p>
				</div>
			</div>
		</div>
	</section>
	<section id="tabs">
		<div class="about-institute content-area-6">
			<h6 class="section-title h1">Choose Your category</h6>
			<div class="row tab-details">
				<div class="navbar navbar-light bg-faded">
					<div class="col-xs-12 ">
						<nav>
							<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
								<a class="nav-item nav-link " id="nav-home-tab"
									data-toggle="tab" href="userHome" role="tab"
									aria-controls="nav-home" aria-selected="true"
									onclick="gotoUserHome()">All</a>
								<c:choose>
									<c:when test="${ not empty categoryList }">
										<c:forEach items="${ categoryList }" var="category">
											<a class="nav-item nav-link"
												id="nav-home-tab${ category.categoryId }" data-toggle="tab"
												href="#nav-All" role="tab" aria-controls="nav-home"
												aria-selected="true"
												onclick="searchByCategoty(${ category.categoryId })">${ category.categoryName }</a>
										</c:forEach>
									</c:when>
								</c:choose>
							</div>
						</nav>
					</div>
				</div>
			</div>
			<div class="tab-content">
				<div class="tab-pane active" id="nav-All">
					<div class="about-institute content-area-6">
						<div class="container" id="allCategories">
							<!-- <div class="list-heading-tabs">
                           <h3>All</h3>
                           </div> -->
							<div class="owl-carousel owl-theme">
								<c:choose>
									<c:when
										test="${ not empty trainerCourseResponseListByCategory }">
										<c:forEach items="${ trainerCourseResponseListByCategory }"
											var="trainerCourseResponse">
											<div class="item">
												<a class=""
													href="dXNlckNvdXJzZURlcnRhaWxz?dHJhaW5lckNvdXJzZUlk=${ trainerCourseResponse.trainerCourseInfo.trainerCourseId }&dHJhaW5lcklk=${ trainerCourseResponse.trainerCourseInfo.trainerInfo.trainerId }">
													<!-- <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6 course-details"> -->
													<div class="cart-over">
														<div class="card">
															<div class="course-img">
																<c:choose>
																	<c:when
																		test="${ not empty trainerCourseResponse.trainerCourseInfo.trainerCourseInfo.courseCoverImageInfo }">
																		<img
																			src="${ trainerCourseResponse.trainerCourseInfo.trainerCourseInfo.courseCoverImageInfo.courseCoverImageViewPath }"
																			class="img-responsive">
																	</c:when>
																	<c:otherwise>
																		<img src="assets/userAssets/imges/course11.jpg"
																			class="img-responsive">
																	</c:otherwise>
																</c:choose>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="course-name">
																		<h5>
																			<b>${ trainerCourseResponse.trainerCourseInfo.trainerCourseInfo.courseName }</b>
																		</h5>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="textinfo ">
																		<h5>${ trainerCourseResponse.trainerCourseInfo.trainerCourseInfo.courseDescription }</h5>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="textinfo ">
																		<h5>
																			<!-- Trainer : -->
																			<b>${ trainerCourseResponse.trainerCourseInfo.trainerInfo.trainerFName }
																				${ trainerCourseResponse.trainerCourseInfo.trainerInfo.trainerLName }</b>
																		</h5>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="star-divs">
																		<span class="stars"
																			data-rating="${ trainerCourseResponse.averageRating }"
																			data-num-stars="5"></span>
																	</div>
																</div>
															</div>
															<!--  rows -->
															<div class="row">
																<div class="col-md-12">
																	<div class="pricing-parts">
																		<p>$ ${ trainerCourseResponse.trainerCourseInfo.trainerCourseInfo.coursePrice }</p>
																	</div>
																</div>
															</div>
															<!--  rows -->
															<%-- <div class="row">
                                                <div class="col-md-12">
                                                   <div class="ratio-show">
                                                      <!--  <p>4,3/5 (20)</p> -->
                                                      <c:choose>
                                                         <c:when test="${ empty userSession }">
                                                            <fmt:message key="label.addToFav" var="addToFav"/>
                                                            <i class="fa fa-heart watch-list open-watchlistLogin"
                                                               aria-hidden="true"
                                                               data-id="${ trainerCourseResponse.trainerCourseInfo.trainerCourseId }"
                                                               title="<fmt:message key="label.addToFav"/>"
                                                            ></i>
                                                            <i class="fa fa-heart watch-list open-watchlistLogin"
                                                               aria-hidden="true"
                                                               data-id="${ trainerCourseResponse.trainerCourseInfo.trainerCourseId }"
                                                               title="<fmt:message key="label.addToFav"/>"
                                                               data-toggle="modal"
                                                               data-target="#addFavLoginModal"></i>
                                                         </c:when>
                                                         <c:otherwise>
                                                            <c:choose>
                                                               <c:when
                                                                  test="${ trainerCourseResponse.isWatchListed() }">
                                                                  <c:set var="watchListClass" value="watch-list-in"></c:set>
                                                                  <fmt:message key="label.removeFromFav"
                                                                     var="removeFromFav" />
                                                                  <c:set var="watchListTitle"
                                                                     value="${ removeFromFav }"></c:set>
                                                               </c:when>
                                                               <c:otherwise>
                                                                  <c:set var="watchListClass" value="watch-list"></c:set>
                                                                  <fmt:message key="label.addToFav" var="addToFav" />
                                                                  <c:set var="watchListTitle" value="${ addToFav }"></c:set>
                                                               </c:otherwise>
                                                            </c:choose>
                                                            <a onclick="userWatchListAddRemove(${ trainerCourseResponse.trainerCourseInfo.trainerCourseId }, ${ userSession.userId }, ${ trainerCourseResponse.isWatchListed() })">
                                                            <i class="fa fa-heart ${ watchListClass }"
                                                               aria-hidden="true" title="${ watchListTitle }"
                                                               onclick=""></i>
                                                            </a>
                                                         </c:otherwise>
                                                      </c:choose>
                                                   </div>
                                                </div>
                                             </div> --%>
														</div>
													</div> <!--  </div> -->
												</a>
											</div>
											<!---end of the col-md-3 here--->
										</c:forEach>
									</c:when>
									<c:otherwise>
										<div
											class="col-md-3 col-lg-3 col-sm-6 col-xs-6 course-details">
											<div class="cart-over">
												<div class="card">No Result Found</div>
											</div>
										</div>
									</c:otherwise>
								</c:choose>
							</div>
						</div>
						<!--end of container-fluid here--->
					</div>
				</div>
				<!--end of the nav-All here--->
			</div>
		</div>
		<!-- ./Tabs -->
		<div class="elearning-elements">
			<!--  </a> -->
			<div class="container">
				<div class="row learning-elements">
					<!-- Main title -->
					<div class="">
						<div class="sportelearning ">
							<h2>Why Novofitness ?</h2>
						</div>
						<div class="col-md-4 col-sm-12">
							<div class="first-sections">
								<img src="assets/userAssets/imges/icon2.png" alt="">
								<h3>Learning from experts</h3>
							</div>
						</div>
						<div class="col-md-4 col-sm-12">
							<div class="first-sections">
								<img src="assets/userAssets/imges/icon3.png" alt="">
								<h3>Anytime, Anywhere</h3>
							</div>
						</div>
						<div class="col-md-4 col-sm-12">
							<div class="first-sections">
								<img src="assets/userAssets/imges/icon1.png" alt="">
								<h3>Interact with teachers and students</h3>
							</div>
						</div>
					</div>
				</div>
				<!-- rows -->
			</div>
			<!-- containers -->
		</div>
	</section>
	<!---sports elearning section end here--->
	<div class="Workouts-sections">
		<div class="container">
			<div class="row">
				<div class="sportelearning-student">
					<h2>What our Student Speak</h2>
				</div>
				<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
					<div class="students-first">
						<div class="row">
							<div class="col-md-4">
								<div class="fitnessss-img">
									<img src="assets/userAssets/imges/jesica.png"
										class="img-responsive">
								</div>
							</div>
							<div class="col-md-8">
								<h6>JESSICA,24</h6>
							</div>
						</div>
						<!-- rows -->
						<div class="er-txts">
							<p>Eroritibusto cum et vendessit eostis apedica turiandus et
								volorro blam endi dis ut aut latis et quam nimustotate
								conessitia sa</p>
						</div>
					</div>
				</div>
				<!-- md-4 -->
				<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
					<div class="students-first">
						<div class="row">
							<div class="col-md-4">
								<div class="fitnessss-img">
									<img src="assets/userAssets/imges/jesica.png"
										class="img-responsive">
								</div>
							</div>
							<div class="col-md-8">
								<h6>JESSICA,24</h6>
							</div>
						</div>
						<!-- rows -->
						<div class="er-txts">
							<p>Eroritibusto cum et vendessit eostis apedica turiandus et
								volorro blam endi dis ut aut latis et quam nimustotate
								conessitia sa</p>
						</div>
					</div>
				</div>
				<!-- md-4 -->
				<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
					<div class="students-first">
						<div class="row">
							<div class="col-md-4">
								<div class="fitnessss-img">
									<img src="assets/userAssets/imges/jesica.png"
										class="img-responsive">
								</div>
							</div>
							<div class="col-md-8">
								<h6>JESSICA,24</h6>
							</div>
						</div>
						<!-- rows -->
						<div class="er-txts">
							<p>Eroritibusto cum et vendessit eostis apedica turiandus et
								volorro blam endi dis ut aut latis et quam nimustotate
								conessitia sa</p>
						</div>
					</div>
				</div>
				<!-- md-4 -->
			</div>
			<!--end of the row here--->
		</div>
	</div>
	<!-- BECOME A TEACHER start -->
	<div class="become-elements3">
		<div class="container">
			<div class="allcourses-txts">
				<h2>BECOME A TEACHER</h2>
			</div>
			<div class="earn-txtxs">
				<p>Earn money. Share your expertise. Build your following.</p>
				<button type="button" class="btn btn-default btn-laern">Learn
					More</button>
			</div>
		</div>
	</div>
	<!-- Our BECOME A TEACHER start -->
	<section class="signing-elements">
		<div class="container">
			<div class="row learning-elements">
				<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
					<div class="list-heading">
						<h2>Novofitness E-Learning - purpose is to transform access
							to sports education.</h2>
					</div>
				</div>
				<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
					<div class="sign-area">
						<div class="list-heading">
							<p>Be the first to hear about our special offers and news</p>
						</div>
						<fieldset>
							<input type="text" name="email" placeholder="Your email addresse"
								class="form-control-sign">
							<button type="submit" class="btnsign" name="submit">Sign
								up</button>
						</fieldset>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal -->
		<div class="modal fade" id="addFavLoginModal" role="dialog">
			<div class="modal-dialog modal-md">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">
							<%-- <fmt:message key="label.login" /> --%>
							Sign In to your account
						</h4>
					</div>
					<div class="modal-body">
						<div class="" id="showlog">
							<div class="details">
								<!-- Form start -->
								<div class="form-content-box ">
									<form:form action="" method="post"
										modelAttribute="userLoginWrapper">
										<div class="form-group">
											<form:hidden path="trainerCourseId" value="" />
											<fmt:message key="label.enterEmail" var="enterEmail" />
											<form:input path="userEmail" class="input-text"
												placeholder="${ enterEmail }" />
											<form:errors path="userEmail" class="error" />
										</div>
										<div class="form-group">
											<fmt:message key="label.enterPassword" var="enterPassword" />
											<form:password path="userPassword" class="input-text"
												placeholder="${ enterPassword }" />
											<form:errors path="userPassword" class="error" />
										</div>
										<div class="checkbox">
											<div class="ez-checkbox pull-left">
												<a href="userRegister"> <fmt:message
														key="label.register" />
												</a>
											</div>
											<a href="userForgetPassword"
												class="link-not-important pull-right"> Forgot password </a>
											<div class="clearfix"></div>
										</div>
										<div class="mb-0">
											<button type="submit" class="btn-md btn-theme btn-block">
												Login</button>
										</div>
									</form:form>
								</div>
								<!-- Form end -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Footer start -->
	<%@ include file="/WEB-INF/handlingJsps/userFooter.jsp"%>
	<!-- Footer end -->
	<%@ include file="/WEB-INF/handlingJsps/userLowerFooter.jsp"%>
	<script type="text/javascript">
      $(function(){
         $('.stars').stars();
      }); 
   </script>
	<script type="text/javascript">
         $(document).ready(function() {
                var owl = $('.owl-carousel');
                owl.owlCarousel({
                 	items:3,
                    margin:10,
                    autoplay:true,
                    autoplayTimeout:5000,
                    autoplayHoverPause:true
                  
                })
              });
      </script>
	<script type="text/javascript">
         $("#userLoginWrapper").submit(function(e) {
         	//alert("hi..");
         	$.ajax({
         		url : 'userLoginForFavouriteList',
         		type : 'post',
         		data : $(this).serialize(),
         		success : function(response) {
         			//alert('success::'+response);
         			if (response == 'error') {
         				alert("Please Enter valid Data");
         			} else if (response == 'fails') {
         				alert("Login Details are not matched");
         			} else if (response == 'success') {
         				alert("Location added to your Favourites")
         			}
         			/*  $('#watchlistLoginModal').dialog({modal:true});    */
         			/*  $( "#watchlistLoginModal" ).dialog( "open" ); */
         			/* location.reload(); */
         		}
         	})
         });
      </script>
	<script type="text/javascript">
         function userWatchListAddRemove(userLocationId,userId,watchListStatus) {
         	//alert("hi..");
         	var userWatchListData = { 
         			userLocationId : userLocationId,
         			userId : userId,
         			watchListStatus : watchListStatus
         		}
         	$.ajax({
                 url: 'userWatchListAddRemove',
                 type: 'post',
                 data: userWatchListData,
                 success: function(response) {
                     //alert('success::'+response);
                     if(response=='added'){
                    	 alert("Location is added to your WatchList");
                     } else if(response=='fail'){
                    	 alert("Location is fail to update. Please try again");
                     } else if(response=='removed'){
                    	 alert("Location is removed from your WatchList")
                     }
                    /*  $('#watchlistLoginModal').dialog({modal:true});    */
                   /*  $( "#watchlistLoginModal" ).dialog( "open" ); */
                   location.reload();
                   }
             });      
         }
      </script>
	<script type="text/javascript">
         $(document).on("click", ".open-watchlistLogin", function() {
         	var trainerCourseId = $(this).data('id');
         	$(".modal-body #trainerCourseId").val(trainerCourseId);
         	// As pointed out in comments, 
         	// it is unnecessary to have to manually call the modal.
         	// $('#addBookDialog').modal('show');
         	$('#categoryWiseData').hide();
         });
      </script>
	<script type="text/javascript">
         var courseCategoryId = getQueryVariable("Y291cnNlTWFpbkNhdGVnb3J5SWQ");
            
            //console.log("courseCategoryId::"+courseCategoryId);
         
            if (courseCategoryId != undefined || courseCategoryId != 0) {
            	$(".nav-item .nav-link").removeClass("active");
            	$("#nav-home-tab"+courseCategoryId).addClass("active");
         }
            
            function getQueryVariable(variable) {
          	  
            	  var query = window.location.search.substring(1);
            	  var vars = query.split("&");
            	  for (var i=0;i<vars.length;i++) {
            	    var pair = vars[i].split("=");
            	    if (pair[0] == variable) {
            	      return pair[1];
            	    }
            	  } 
            	  //alert('Query Variable ' + variable + ' not found');
            	}
      </script>
	<script type="text/javascript">
         function searchByCategoty(categoryId) {
         // alert(categoryId);
          /* var courseData = { 
         		categoryId : categoryId,
             	}
             	$.ajax({
                     type: "POST",
                     url: "searchByCategoty",
                     data: courseData,
                     success: function (result) {
                    	 console.log("sccess",result)
                    	 $('#allCategories').hide();
                    	 $('#categoryWiseData').show();
                    	 
                    	 window.location = "/NovaFitness/bWFpblBhZ2VDb3Vyc2VTZWFyY2hCeUNhdGVnb3J5?Y291cnNlTWFpbkNhdGVnb3J5SWQ="
                        		+ categoryId;
                    	 //location.reload();
                     },
                     error: function (result) {
                    	 console.log("error",result)
                    	 //location.reload();
                     }
                 });  */
          window.location = "bWFpblBhZ2VDb3Vyc2VTZWFyY2hCeUNhdGVnb3J5?Y291cnNlTWFpbkNhdGVnb3J5SWQ="
            		+ categoryId;
         }
         function gotoUserHome() {
          window.location = "userHome";
         }
      </script>
	<!-- Custom javascript -->
</body>
</html>