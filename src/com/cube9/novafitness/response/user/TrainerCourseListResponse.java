package com.cube9.novafitness.response.user;

import com.cube9.novafitness.model.trainer.TrainerCourseInfo;

public class TrainerCourseListResponse {

	private TrainerCourseInfo trainerCourseInfo;
	
	private Double averageRating;
	
	private boolean isWatchListed=false;

	public TrainerCourseInfo getTrainerCourseInfo() {
		return trainerCourseInfo;
	}

	public void setTrainerCourseInfo(TrainerCourseInfo trainerCourseInfo) {
		this.trainerCourseInfo = trainerCourseInfo;
	}

	public Double getAverageRating() {
		return averageRating;
	}

	public void setAverageRating(Double averageRating) {
		this.averageRating = averageRating;
	}

	public boolean isWatchListed() {
		return isWatchListed;
	}

	public void setWatchListed(boolean isWatchListed) {
		this.isWatchListed = isWatchListed;
	}

	@Override
	public String toString() {
		return "TrainerCourseListResponse [trainerCourseInfo=" + trainerCourseInfo + ", averageRating=" + averageRating
				+ ", isWatchListed=" + isWatchListed + "]";
	}
	
}
