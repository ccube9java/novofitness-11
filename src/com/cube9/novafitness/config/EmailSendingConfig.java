package com.cube9.novafitness.config;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailSendingConfig {

	public void sendEmail(String email, String sub, String msg) {

		 final String from = "ccube9gsinfotech@gmail.com";
		 final String pwd = "cube9@123";
		 
		// final String from = "vaibhavdes7026@gmail.com";
		// final String pwd = "mh23b.7026VVD";

		//final String from = "spyvarade.cube9@gmail.com";
		//final String pwd = "cipzdtsdyshjpzti";

		// MAIL_DRIVER=smtp
		// MAIL_HOST=smtp.gmail.com
		// MAIL_PORT=587
		// mail_username=spyvarade.cube9@gmail.com
		// MAIL_PASSWORD=cipzdtsdyshjpzti
		// MAIL_ENCRYPTION=tls
		// MAIL_FROM_NAME=Robosque
		// String to = email;

		// Get properties object
		Properties props = new Properties();

		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
		
		// get Session
		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(from, pwd);
			}
		});
		// compose message
		try {
			MimeMessage message = new MimeMessage(session);
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));

			try {
				message.setFrom(new InternetAddress(from, "NovoFitness"));
			} catch (UnsupportedEncodingException e) {
				// Auto-generated catch block
				e.printStackTrace();
			}

			message.setSubject(sub);
			// message.setText(msg);
			message.setContent("<h1>" + msg + "</h1><br>", "text/html");
			// send message
			Transport.send(message);
			System.out.println("message sent successfully");

		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
}
