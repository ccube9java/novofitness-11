package com.cube9.novafitness.config;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.cube9.novafitness.wrapper.admin.AdminChangePasswordWrapper;
import com.cube9.novafitness.wrapper.admin.AdminResetPasswordWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerChangePasswordWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerRegisterWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerResetPasswordWrapper;
import com.cube9.novafitness.wrapper.user.UserChangePasswordWrapper;
import com.cube9.novafitness.wrapper.user.UserRegisterWrapper;
import com.cube9.novafitness.wrapper.user.UserResetPasswordWrapper;

public class PasswordValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		// Auto-generated method stub
		return false;
	}
	
	@Override
	public void validate(Object obj, Errors errors) {
		TrainerRegisterWrapper password = (TrainerRegisterWrapper) obj;
		//System.out.println("vali::"+password); 
		if(!password.getTrainerPassword().equals(password.getTrainerConfirmPassword())) {
			errors.rejectValue("trainerConfirmPassword", "label.mismatchPassword"); 
		} 
	}

	public void validateAdminResetPassword(Object obj, Errors errors) {
		AdminResetPasswordWrapper password = (AdminResetPasswordWrapper) obj;
		//System.out.println("vali::"+password); 
		if(!password.getAdminPassword().equals(password.getAdminConfirmPassword())) {
			errors.rejectValue("adminConfirmPassword", "label.mismatchPassword"); 
		} 
	}
	
	public void validateAdminChangePassword(Object obj, Errors errors) {
		AdminChangePasswordWrapper password = (AdminChangePasswordWrapper) obj;
		//System.out.println("vali::"+password); \
		if(!password.getAdminPassword().equals(password.getAdminConfirmPassword())) {
			errors.rejectValue("adminConfirmPassword", "label.mismatchPassword"); 
		} 
	}

	public void validateTrainerResetPassword(Object obj, Errors errors) {
		TrainerResetPasswordWrapper password = (TrainerResetPasswordWrapper) obj;
		//System.out.println("vali::"+password); 
		if(!password.getTrainerPassword().equals(password.getTrainerConfirmPassword())) {
			errors.rejectValue("trainerConfirmPassword", "label.mismatchPassword"); 
		} 
	}

	public void validateTrainerChangePassword(Object obj, Errors errors) {
		TrainerChangePasswordWrapper password = (TrainerChangePasswordWrapper) obj;
		//System.out.println("vali::"+password); 
		if(!password.getTrainerPassword().equals(password.getTrainerConfirmPassword())) {
			errors.rejectValue("trainerConfirmPassword", "label.mismatchPassword"); 
		} 
	}

	public void validateUserRegisterWrapper(Object obj, Errors errors) {
		UserRegisterWrapper password = (UserRegisterWrapper) obj;
		//System.out.println("vali::"+password); 
		if(!password.getUserPassword().equals(password.getUserConfirmPassword())) {
			errors.rejectValue("userConfirmPassword", "label.mismatchPassword"); 
		} 
	}

	public void validateUserResetPasswordWrapper(Object obj, Errors errors) {
		UserResetPasswordWrapper password = (UserResetPasswordWrapper) obj;
		//System.out.println("vali::"+password); 
		if(!password.getUserPassword().equals(password.getUserConfirmPassword())) {
			errors.rejectValue("userConfirmPassword", "label.mismatchPassword"); 
		} 
	}

	public void validateUserChangePasswordWrapper(Object obj, Errors errors) {
		UserChangePasswordWrapper password = (UserChangePasswordWrapper) obj;
		//System.out.println("vali::"+password); 
		if(!password.getUserPassword().equals(password.getUserConfirmPassword())) {
			errors.rejectValue("userConfirmPassword", "label.mismatchPassword"); 
		} 
	}
 
	/*
	 * public boolean supports(Class<?> paramClass) { return
	 * UserWrapper.class.equals(paramClass); }
	 * 
	 * public void validate(Object obj, Errors errors) { //
	 * ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password",
	 * "valid.password"); //ValidationUtils.rejectIfEmptyOrWhitespace(errors,
	 * "passwordConf", "valid.passwordConf"); UserWrapper password = (UserWrapper)
	 * obj; //System.out.println("vali::"+password); if
	 * (!password.getUserPassword().equals(password.getUserConfirmPassword())) {
	 * errors.rejectValue("userConfirmPassword", "valid.passwordConfDiff"); } }
	 * public void validateChangePassword(Object obj, Errors errors) {
	 * UserChangePasswordWrapper password = (UserChangePasswordWrapper) obj;
	 * //System.out.println("vali::"+password); if
	 * (!password.getUserPassword().equals(password.getUserConfirmPassword())) {
	 * errors.rejectValue("userConfirmPassword", "valid.passwordConfDiff"); } }
	 * 
	 * public void validateUserResetPassword(Object obj, Errors errors) {
	 * UserResetPasswordWrapper password = (UserResetPasswordWrapper) obj;
	 * //System.out.println("vali::"+password); if
	 * (!password.getUserPassword().equals(password.getUserConfirmPassword())) {
	 * errors.rejectValue("userConfirmPassword", "valid.passwordConfDiff"); } }
	 * 
	 * 
	 * public void validateSubAdminPassword(Object obj, Errors errors) {
	 * SubAdminWrapper password = (SubAdminWrapper) obj;
	 * //System.out.println("vali::"+password); if
	 * (!password.getSubAdminPassword().equals(password.getSubAdminConfirmPassword()
	 * )) { errors.rejectValue("subAdminConfirmPassword", "valid.passwordConfDiff");
	 * } }
	 */
    
}
