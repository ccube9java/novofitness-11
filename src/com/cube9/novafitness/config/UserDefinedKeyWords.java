package com.cube9.novafitness.config;

public final class UserDefinedKeyWords {
	
	/*--- Server IP ---*/
	 //public static final String serverIP = "http://13.233.13.95:8090"; //OLD
	 public static final String serverIP = "http://13.233.151.143:8090";
	 
	 /*--- file Location for Storage ---*/        
	 //public static final String fileLocation = "/home/cube9/eclipse-workspace/novofitness/WebContent/WEB-INF/assets/";//NEW-Company-Local
	 //public static final String fileLocation = "/home/vinu/eclipse-workspace/novofitness/WebContent/WEB-INF/assets/";//NEW-Home-Local
	 public static final String fileLocation = "/opt/apache-tomcat-9.0.30/webapps/";
	
	 /*--- adminProfileImageViewPath for Storage ---*/
	 //public static final String adminProfileImageViewPath = "assets/YWRtaW5Qcm9maWxlSW1hZ2VWaWV3UGF0aA/";
	 public static final String adminProfileImageViewPath = serverIP + "/YWRtaW5Qcm9maWxlSW1hZ2VWaWV3UGF0aA/";
	 
	 /*--- trainerIDCopyViewPath for Storage ---*/
	 //public static final String trainerIDCopyViewPath = "assets/dHJhaW5lcklEQ29weVZpZXdQYXRo/";
	 public static final String trainerIDCopyViewPath = serverIP + "/dHJhaW5lcklEQ29weVZpZXdQYXRo/";
	 
	 /*--- trainerCertificateViewPath for Storage ---*/
	 //public static final String trainerCertificateViewPath = "assets/dHJhaW5lckNlcnRpZmljYXRlVmlld1BhdGg/";
	 public static final String trainerCertificateViewPath = serverIP + "/dHJhaW5lckNlcnRpZmljYXRlVmlld1BhdGg/";
	 
	 /*--- userProfileImageViewPath for Storage ---*/
	 //public static final String userProfileImageViewPath = "assets/dHJhaW5lclByb2ZpbGVJbWFnZVZpZXdQYXRo/";
	 public static final String userProfileImageViewPath = serverIP + "/dHJhaW5lclByb2ZpbGVJbWFnZVZpZXdQYXRo/";
	 
	 /*--- trainerProfileImageViewPath for Storage ---*/
	 //public static final String trainerProfileImageViewPath = "assets/dXNlclByb2ZpbGVJbWFnZVZpZXdQYXRo/";
	 public static final String trainerProfileImageViewPath = serverIP + "/dXNlclByb2ZpbGVJbWFnZVZpZXdQYXRo/";
	 
	 /*--- trainerCourseMediaInfoViewPath for Storage ---*/
	 //public static final String trainerCourseMediaInfoViewPath = "assets/dHJhaW5lckNvdXJzZU1lZGlhSW5mb1ZpZXdQYXRo/";
	 public static final String trainerCourseMediaInfoViewPath = serverIP + "/dHJhaW5lckNvdXJzZU1lZGlhSW5mb1ZpZXdQYXRo/";
	 
	 /*--- URL for Verification ---*/
	 //public static final String URL = "http://localhost:8080/NovaFitness/";
	 public static final String URL = serverIP + "/NovoFitness/";
	 
	 /*--- URLPath for Verification ---*/
	 //public static final String URL_PATH = "NovaFitness/";
	 public static final String URL_PATH = "/NovoFitness/";
	 
	 /*--- PayPal Client Id ---*/
	 public static final String PAYPAL_CLIENT_ID = "AcpXnxg6bBLxqDrvd1o74o5DcPTFZTlgISBwU5SG-iMhiLOR2rh02NUQwfFYdjmFjIw3jeBaO9UobV2Q";
	 
	 /*--- PayPal Secret Key ---*/
	 public static final String PAYPAL_SECRET_KEY = "EOs2pl_Vny9mV6BJUTV_E4vbpM6b9ru2X4XzJDWoooGZ1qACr3cfVVU7lQdU25xSSyjDFwWelA4jWJbV";
	 
	 /*--- PayPal Mode ---*/
	 public static final String PAYPAL_MODE = "sandbox";
	 //public static final String PAYPAL_MODE = "live";
	 
	 
	 
	 
	 
	 
	 /*--- loginBannerImageViewPath for Storage ---*/
	 public static final String loginBannerImageViewPath = "assets/banners/loginBanner/";
	 //public static final String loginBannerImageViewPath = "http://160.153.246.69/banners/loginBanner/";
	 
	 /*--- registerBannerImageViewPath for Storage ---*/
	 public static final String registerBannerImageViewPath = "assets/banners/registerBanner/";
	 //public static final String registerBannerImageViewPath = "http://160.153.246.69/banners/registerBanner/";
	 
	 /*--- categoryMediaViewPath for Storage ---*/
	 public static final String categoryMediaViewPath = "assets/category/";
	 //public static final String categoryMediaViewPath = "http://160.153.246.69/category/";
	 
	 /*--- coverImageViewPath for Storage ---*/
	 public static final String coverImageViewPath = "assets/bG9jYXRpb25NZWRpYQ/";
	 //public static final String coverImageViewPath = "http://160.153.246.69/bG9jYXRpb25NZWRpYQ/";
	 
	 /*--- videoViewPath for Storage ---*/
	 public static final String videoViewPath = "assets/dmlkZW9WaWV3UGF0aA/";
	 //public static final String videoViewPath = "http://160.153.246.69/dmlkZW9WaWV3UGF0aA/";
	 
	 /*--- Chat Realtime Database ---*/
	 public static final String chatDBPath = "chatLocal/";
	 //public static final String chatDBPath = "chatLive/";
	 
	 /*--- Chat Firebase API Key ---*/
	 public static final String apiKey = "";
	 
	 /*--- Chat Firebase authDomain ---*/
	 public static final String authDomain = "";
	 
	 /*--- Chat Firebase databaseURL ---*/
	 public static final String databaseURL = "";
	 
	 /*--- Payment stripePublicKey ---*/
	 public static final String stripePublicKey = "";
	 
	 /*--- Payment stripeSecretKey ---*/
	 public static final String stripeSecretKey = "";
	 
}