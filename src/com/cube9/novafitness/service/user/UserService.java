package com.cube9.novafitness.service.user;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.context.MessageSource;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cube9.novafitness.model.admin.CategoryInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseInfo;
import com.cube9.novafitness.model.user.CartTrainerCourseUserInfo;
import com.cube9.novafitness.model.user.FavouriteTrainerCourseUserInfo;
import com.cube9.novafitness.model.user.GenderInfo;
import com.cube9.novafitness.model.user.TrainerCourseUserQuestionAnswerInfo;
import com.cube9.novafitness.model.user.TrainerCourseUserQuestionInfo;
import com.cube9.novafitness.model.user.UserCoursePaymentInfo;
import com.cube9.novafitness.model.user.UserCourseReviewInfo;
import com.cube9.novafitness.model.user.UserInfo;
import com.cube9.novafitness.response.user.TrainerCourseListResponse;
import com.cube9.novafitness.wrapper.user.NewsLetterWrapper;
import com.cube9.novafitness.wrapper.user.TrainerCourseUserQuestionAnswerWrapper;
import com.cube9.novafitness.wrapper.user.TrainerCourseUserQuestionWrapper;
import com.cube9.novafitness.wrapper.user.UserChangePasswordWrapper;
import com.cube9.novafitness.wrapper.user.UserCourseReviewWrapper;
import com.cube9.novafitness.wrapper.user.UserLoginWrapper;
import com.cube9.novafitness.wrapper.user.UserProfileWrapper;
import com.cube9.novafitness.wrapper.user.UserRegisterWrapper;
import com.cube9.novafitness.wrapper.user.UserResetPasswordWrapper;
import com.cube9.novafitness.wrapper.user.payment.CouponCodePaymentWrapper;
import com.cube9.novafitness.wrapper.user.payment.UserPaymentCheckoutWraspper;
import com.paypal.api.payments.Payment;
import com.paypal.base.rest.PayPalRESTException;

public interface UserService {

	boolean checkUserDuplicateEmail(String userEmail);

	boolean userRegister(@Valid UserRegisterWrapper userRegisterWrapper, MessageSource messageSource,
			RedirectAttributes redirectAttributes, Model model);

	boolean userVerification(String userEmail, RedirectAttributes redirectAttributes, MessageSource messageSource);

	boolean userLogin(@Valid UserLoginWrapper userLoginWrapper, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session);

	boolean userForgetPassword(@Valid UserLoginWrapper userLoginWrapper, RedirectAttributes redirectAttributes,
			MessageSource messageSource);

	boolean resetUserPassword(@Valid UserResetPasswordWrapper userResetPasswordWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource);

	boolean updateUserProfile(@Valid UserProfileWrapper userProfileWrapper, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session);

	boolean uploadUserProfileImage(MultipartFile[] userProfileImage, String userEmail, Model model, HttpSession session, RedirectAttributes redirectAttributes, MessageSource messageSource);

	boolean deleteUserProfileImage(Long userProfileImageId, Long userId);

	boolean changeUserPassword(@Valid UserChangePasswordWrapper userChangePasswordWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session, UserInfo userInfo);

	List<GenderInfo> getGenderList();

	List<CategoryInfo> getActiveMainCategoryList();

	List<TrainerCourseInfo> getAllActiveCourseList();

	boolean addCourseToFavouriteList(@Valid UserLoginWrapper userLoginWrapper, HttpSession session);

	List<TrainerCourseListResponse> checkTrainerCourseIsInUsersFavList(
			List<TrainerCourseListResponse> trainerCourseListResponses, UserInfo userInfo, HttpSession session);

	List<TrainerCourseInfo> courseFilterByCategoryId(long parseLong);

	String userWatchListAddRemove(Long trainerCourseId, Long userId, boolean watchListStatus, HttpSession session);

	FavouriteTrainerCourseUserInfo getCourseFavouriteListStatus(Long trainerCourseId, Long userId);

	List<FavouriteTrainerCourseUserInfo> getUserFavouriteTrainerCourseListByUserId(Long userId);

	TrainerCourseUserQuestionInfo getTrainerCourseUserQuestionInfoByTrainerCourseId(Long trainerCourseId);

	List<TrainerCourseUserQuestionAnswerInfo> getTrainerCourseUserQuestionAnswerListByTrainerCourseUserQuestionId(
			Long trainerCourseUserQuestionId);

	boolean uploadTrainerCourseUserQuestion(@Valid TrainerCourseUserQuestionWrapper trainerCourseUserQuestionWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session);

	boolean postTrainerCourseUserQuestionAnswer(
			@Valid TrainerCourseUserQuestionAnswerWrapper trainerCourseUserQuestionAnswerWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session);

	boolean addCourseCart(@Valid UserLoginWrapper userLoginWrapper, HttpSession session);

	List<CartTrainerCourseUserInfo> getUserCartTrainerCourseListByUserId(Long userId);

	CartTrainerCourseUserInfo getCourseCartListStatus(Long trainerCourseId, Long userId);

	String userCartListAddRemove(Long trainerCourseId, Long userId, boolean watchListStatus, HttpSession session);

	String userCartListRemove(Long cartTrainerCourseUserId, boolean cartListStatus, HttpSession session);

	boolean couponCodeCheckPayment(@Valid CouponCodePaymentWrapper couponCodePaymentWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session, Model model,
			HttpServletRequest request, Locale locale);

	String authorizeUserPayment(@Valid UserPaymentCheckoutWraspper userPaymentCheckoutWraspper)
			throws PayPalRESTException;

	boolean userCoursePaymentProcess(String paymentId, String payerId, Payment coursePayment, HttpSession session,
			Model model);

	List<UserCoursePaymentInfo> getUserCoursePaymentListByUserId(Long userId);

	boolean userReviewForTrainerCourse(@Valid UserCourseReviewWrapper userCourseReviewWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session);

	List<UserCourseReviewInfo> getUserCourseReviewByTrainerCourseId(Long trainerCourseId, Model model);

	List<TrainerCourseListResponse> getTrainerCourseListResponseWithAverageRating(
			List<TrainerCourseListResponse> trainerCourseListResponses);

	boolean subscribeNewsLetter(@Valid NewsLetterWrapper newsLetterWrapper, RedirectAttributes redirectAttributes,
			MessageSource messageSource, Model model);

	UserCoursePaymentInfo getUserCoursePaymentInfoByTrainerCourseIdAndUserId(Long trainerCourseId, Long userId);

	String userBuyNow(Long trainerCourseId, Long userId, boolean watchListStatus, HttpSession session);

	UserInfo getUserInfoByUserEmail(String userEmail);

}
