package com.cube9.novafitness.service.user;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.LongSummaryStatistics;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.lang3.RandomStringUtils;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cube9.novafitness.config.EmailSendingConfig;
import com.cube9.novafitness.config.UserDefinedKeyWords;
import com.cube9.novafitness.config.UserDefinedMethods;
import com.cube9.novafitness.model.admin.CategoryInfo;
import com.cube9.novafitness.model.admin.CouponCodeInfo;
import com.cube9.novafitness.model.course.CourseContributionInfo;
import com.cube9.novafitness.model.course.CourseInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseInfo;
import com.cube9.novafitness.model.user.CartTrainerCourseUserInfo;
import com.cube9.novafitness.model.user.FavouriteTrainerCourseUserInfo;
import com.cube9.novafitness.model.user.GenderInfo;
import com.cube9.novafitness.model.user.NewsLetterInfo;
import com.cube9.novafitness.model.user.TrainerCourseUserQuestionAnswerInfo;
import com.cube9.novafitness.model.user.TrainerCourseUserQuestionInfo;
import com.cube9.novafitness.model.user.UserCoursePaymentInfo;
import com.cube9.novafitness.model.user.UserCourseReviewInfo;
import com.cube9.novafitness.model.user.UserInfo;
import com.cube9.novafitness.model.user.UserOrderInfo;
import com.cube9.novafitness.repository.admin.AdminRepository;
import com.cube9.novafitness.repository.user.UserRepository;
import com.cube9.novafitness.response.user.TrainerCourseListResponse;
import com.cube9.novafitness.wrapper.user.NewsLetterWrapper;
import com.cube9.novafitness.wrapper.user.TrainerCourseUserQuestionAnswerWrapper;
import com.cube9.novafitness.wrapper.user.TrainerCourseUserQuestionWrapper;
import com.cube9.novafitness.wrapper.user.UserChangePasswordWrapper;
import com.cube9.novafitness.wrapper.user.UserCourseReviewWrapper;
import com.cube9.novafitness.wrapper.user.UserLoginWrapper;
import com.cube9.novafitness.wrapper.user.UserProfileWrapper;
import com.cube9.novafitness.wrapper.user.UserRegisterWrapper;
import com.cube9.novafitness.wrapper.user.UserResetPasswordWrapper;
import com.cube9.novafitness.wrapper.user.payment.CouponCodePaymentWrapper;
import com.cube9.novafitness.wrapper.user.payment.UserPaymentCheckoutWraspper;
import com.paypal.api.payments.Amount;
import com.paypal.api.payments.Details;
import com.paypal.api.payments.Item;
import com.paypal.api.payments.ItemList;
import com.paypal.api.payments.Links;
import com.paypal.api.payments.Payer;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.PaymentExecution;
import com.paypal.api.payments.RedirectUrls;
import com.paypal.api.payments.Transaction;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;
import com.sun.jersey.core.util.Base64;

/**
 * @author Vaibhav Deshmane
 *
 */
@Service
public class UserServiceImp implements UserService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private AdminRepository adminRepository;

	@Transactional
	@Override
	public boolean checkUserDuplicateEmail(String UserEmail) {
		return userRepository.duplicateEmailCheck(UserEmail);
	}

	@Transactional
	@Override
	public boolean userRegister(@Valid UserRegisterWrapper UserRegisterWrapper, MessageSource messageSource,
			RedirectAttributes redirectAttributes, Model model) {
		UserInfo userInfo = new UserInfo();
		userInfo.setUserFName(UserRegisterWrapper.getUserFName());
		userInfo.setUserLName(UserRegisterWrapper.getUserLName());
		userInfo.setUserEmail(UserRegisterWrapper.getUserEmail());
		userInfo.setUserPhoneNo(UserRegisterWrapper.getUserPhoneNumber() != ""
				? Long.parseLong(UserRegisterWrapper.getUserPhoneNumber())
				: null);

		String generatedSecuredPasswordHash = BCrypt.hashpw(UserRegisterWrapper.getUserPassword(), BCrypt.gensalt(12));
		// System.out.println(generatedSecuredPasswordHash);

		userInfo.setUserPassword(generatedSecuredPasswordHash);

		// boolean matched = BCrypt.checkpw(UserRegisterWrapper.getUserPassword(),
		// generatedSecuredPasswordHash);
		// System.out.println(matched);

		userInfo = userRepository.saveUserInfo(userInfo);

		if (userInfo != null) {
			EmailSendingConfig emailSendingConfig = new EmailSendingConfig();
			byte[] encodedBytes = Base64.encode(userInfo.getUserEmail().getBytes());
			String encryptedEmail = new String(encodedBytes);
			String sub = "NovoFitness User Account Verification";

			if (UserRegisterWrapper.isUserAcceptNewsletterSubscription()) {
				//NewsLetterWrapper newsLetterWrapper = new NewsLetterWrapper();
				//newsLetterWrapper.setUserEmail(userInfo.getUserEmail());
				//newsLetterWrapper.setUserName(userInfo.getUserFName());
				//subscribeNewsLetter(newsLetterWrapper, redirectAttributes, messageSource, model);
				
				NewsLetterInfo newsLetterInfo = userRepository.getNewsLetterInfoByUserEmail(UserRegisterWrapper.getUserEmail());
				if (newsLetterInfo != null && !newsLetterInfo.isNewsLetterUserIsDeletedByAdmin()) {

					if (!newsLetterInfo.isNewsLetterUserIsActive()) {
						userRepository.updateNewsLetterInfo(newsLetterInfo.getNewsLetterId(), true,
								UserRegisterWrapper.getUserFName());
					}
				} else {
					newsLetterInfo = new NewsLetterInfo();
					newsLetterInfo.setNewsLetterUserFName(UserRegisterWrapper.getUserFName());
					newsLetterInfo.setNewsLetterUserEmail(UserRegisterWrapper.getUserEmail());
					Long newsLetterId = userRepository.saveNewsLetterSubscription(newsLetterInfo);

					if (newsLetterId != null) {

						EmailSendingConfig emailSendingConfig1 = new EmailSendingConfig();
						String sub1 = "NovoFitness NewsLetter Subscription";
						String emailHeader1 = "Hello " + UserRegisterWrapper.getUserFName();
						String emailBody1 = "Thank you for subscribing NewsLetter of NovoFitness ! <br> You will get our news, special offers by e-mailing.";
						String msg1 = UserDefinedMethods.getEmailTemplateMessageWithoutButton(sub1, emailHeader1, emailBody1);
						emailSendingConfig1.sendEmail(UserRegisterWrapper.getUserEmail(), sub1, msg1);
						
					} 
				}
			}

			// String msg = "<table width='658px' align='center'
			// style='border-collapse:collapse' border='0' cellspacing='0' cellpadding='0'>"
			// + "<tbody>"
			// + "<tr> <td width='100%'
			// style='border-top:none;border-bottom:none;border-left:none;border-right:none;text-align:left;padding:15px
			// 15px;font-size:18px;font-family:arial'>"
			// + " <span> Hello " + userInfo.getUserFName() + " " + userInfo.getUserLName()
			// + ",</span> </td> </tr>"
			// + "<tr> <td width='100%'
			// style='border-top:none;border-bottom:none;border-left:none;border-right:none;text-align:left;padding:5px
			// 15px;font-size:15px;font-family:arial'> "
			// + "<span>Thank you for your interest in registering with NovoFitness as a
			// User !</span> </td> </tr>"
			// + "<tr> <td width='100%'
			// style='border-top:none;border-bottom:none;border-left:none;border-right:none;text-align:left;padding:5px
			// 15px;font-size:15px;font-family:arial'> "
			// + "<span>To complete your registration, we need you to verify your email
			// address.</span> </td> </tr>"
			// + "<tr><td><br> </td></tr>" + "<tr> <td width='100%'> <a href=" + url
			// + " style='background:#37a000;text-decoration:none;padding:8px
			// 8px;border-radius:4px;color:#fff;font-family:arial;margin-left:
			// 150px;'>Verify Email</a> </td> </tr>"
			// + "</tbody>" + "</table>";
			
			String emailClickURL = UserDefinedKeyWords.URL + "userEmailVerification/" + encryptedEmail;
			String emailHeader = "Hello " + userInfo.getUserFName() + " " + userInfo.getUserLName();
			String emailBody = "Thank you for your interest in registering with NovoFitness as a User ! <br> To complete your registration, we need you to verify your email address.";
			String emailClickButton = "Verify Email";
			String msg = UserDefinedMethods.getEmailTemplateMessage(sub, emailHeader, emailBody, emailClickURL,
					emailClickButton);

			emailSendingConfig.sendEmail(UserRegisterWrapper.getUserEmail(), sub, msg);

			redirectAttributes.addFlashAttribute("alertSuccessMessage",
					messageSource.getMessage("label.successFullUserRegister", null, null));
			return true;
		} else {
			redirectAttributes.addFlashAttribute("alertSuccessMessage",
					messageSource.getMessage("label.registerationFailed", null, null));
			return false;
		}
	}

	@Transactional
	@Override
	public boolean userVerification(String UserEmail, RedirectAttributes redirectAttributes,
			MessageSource messageSource) {
		byte[] decodedBytes = Base64.decode(UserEmail);
		String decodedEmail = new String(decodedBytes);
		boolean flag = userRepository.userVerification(decodedEmail);
		if (flag) {
			redirectAttributes.addFlashAttribute("alertSuccessMessage",
					messageSource.getMessage("label.userAccountVerificationSuccess", null, null));
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessage",
					messageSource.getMessage("label.userAccountVerificationFailed", null, null));
		}
		return flag;
	}

	@Transactional
	@Override
	public boolean userLogin(@Valid UserLoginWrapper userLoginWrapper, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session) {
		UserInfo userInfo = userRepository.getUserInfoByUserEmail(userLoginWrapper.getUserEmail());
		if (userInfo != null) {
			if (userInfo.isUserIsDeletedByAdmin()) {
				redirectAttributes.addFlashAttribute("alertFailMessageemailNotPresent",
						messageSource.getMessage("label.emailNotPresent", null, null));
				return false;
			} else {
				if (userInfo.isUserIsActive()) {
					boolean matched = BCrypt.checkpw(userLoginWrapper.getUserPassword(), userInfo.getUserPassword());
					if (matched) {
						userRepository.updateLastUserLoginByUserId(userInfo.getUserId(), LocalDateTime.now());
						session.setAttribute("userSession", userInfo);
						return true;
					} else {
						redirectAttributes.addFlashAttribute("alertFailMessageinvalidCredentials",
								messageSource.getMessage("label.invalidCredentials", null, null));
						return false;
					}
				} else {
					redirectAttributes.addFlashAttribute("alertFailMessageUserAccountVerificationFailed",
							messageSource.getMessage("label.userAccountVerificationFailed", null, null));
					return false;
				}
			}
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessageemailNotPresent",
					messageSource.getMessage("label.emailNotPresent", null, null));
			return false;
		}
	}

	@Transactional
	@Override
	public boolean userForgetPassword(@Valid UserLoginWrapper userLoginWrapper, RedirectAttributes redirectAttributes,
			MessageSource messageSource) {
		UserInfo userInfo = userRepository.getUserInfoByUserEmail(userLoginWrapper.getUserEmail());

		if (userInfo != null) {
			if (!userInfo.isUserIsDeletedByAdmin()) {
				if (userInfo.isUserIsActive()) {
					EmailSendingConfig emailSendingConfig = new EmailSendingConfig();
					byte[] encodedBytes = Base64.encode(userInfo.getUserEmail().getBytes());
					String encryptedEmail = new String(encodedBytes);
					String sub = "NovoFitness User Reset Password";
					// String msg = "<table width='658px' align='center'
					// style='border-collapse:collapse' border='0' cellspacing='0' cellpadding='0'>"
					// + "<tbody>"
					// + "<tr> <td width='100%'
					// style='border-top:none;border-bottom:none;border-left:none;border-right:none;text-align:left;padding:15px
					// 15px;font-size:18px;font-family:arial'>"
					// + " <span> Hello " + userInfo.getUserFName() + " " + userInfo.getUserLName()
					// + ",</span> </td> </tr>"
					// + "<tr> <td width='100%'
					// style='border-top:none;border-bottom:none;border-left:none;border-right:none;text-align:left;padding:5px
					// 15px;font-size:15px;font-family:arial'> "
					// + " <span>To reset your Password, please click on below button.</span> </td>
					// </tr>"
					// + "<tr><td><br> </td></tr>" + "<tr> <td width='100%'> <a href=" + url
					// + " style='background:#37a000;text-decoration:none;padding:8px
					// 8px;border-radius:4px;color:#fff;font-family:arial;margin-left: 150px;'>Reset
					// Password</a> </td> </tr>"
					// + "</tbody>" + "</table>";
					String emailClickURL = UserDefinedKeyWords.URL + "userResetPassword/" + encryptedEmail;
					String emailHeader = "Hello " + userInfo.getUserFName() + " " + userInfo.getUserLName();
					String emailBody = "To reset your Password, please click on below button.";
					String emailClickButton = "Reset Password";
					String msg = UserDefinedMethods.getEmailTemplateMessage(sub, emailHeader, emailBody, emailClickURL,
							emailClickButton);

					emailSendingConfig.sendEmail(userInfo.getUserEmail(), sub, msg);

					redirectAttributes.addFlashAttribute("alertSuccessMessagepasswordResetLinkMessage",
							messageSource.getMessage("label.passwordResetLinkMessage", null, null));
					return true;
				} else {
					redirectAttributes.addFlashAttribute("alertFailMessageUserAccountVerificationFailed",
							messageSource.getMessage("label.userAccountVerificationFailed", null, null));
					return false;
				}
			} else {
				redirectAttributes.addFlashAttribute("alertFailMessageemailNotPresent",
						messageSource.getMessage("label.emailNotPresent", null, null));
				return false;
			}
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessageemailNotPresent",
					messageSource.getMessage("label.emailNotPresent", null, null));
			return false;
		}
	}

	@Transactional
	@Override
	public boolean resetUserPassword(@Valid UserResetPasswordWrapper userResetPasswordWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource) {
		byte[] decodedBytes = Base64.decode(userResetPasswordWrapper.getUserEmail());
		String decodedEmail = new String(decodedBytes);
		userResetPasswordWrapper.setUserEmail(decodedEmail);

		String generatedSecuredPasswordHash = BCrypt.hashpw(userResetPasswordWrapper.getUserPassword(),
				BCrypt.gensalt(12));

		boolean flag = userRepository.userResetPassword(userResetPasswordWrapper, generatedSecuredPasswordHash);
		if (flag) {
			redirectAttributes.addFlashAttribute("alertSuccessMessagepasswordResetSuccess",
					messageSource.getMessage("label.passwordResetSuccess", null, null));
			return true;
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessageUserAccountVerificationFailed",
					messageSource.getMessage("label.userAccountVerificationFailed", null, null));
			return false;
		}
	}

	@Transactional
	@Override
	public boolean updateUserProfile(@Valid UserProfileWrapper userProfileWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session) {
		boolean flag = userRepository.updateUserProfileByUserEmail(userProfileWrapper);
		if (flag) {
			UserInfo userInfo = userRepository.getUserInfoByUserEmail(userProfileWrapper.getUserEmail());
			if (userInfo != null) {
				session.setAttribute("userSession", userInfo);
				redirectAttributes.addFlashAttribute("alertSuccessMessageUserProfileUpdateSuccess",
						messageSource.getMessage("label.userProfileUpdateSuccess", null, null));
				return true;
			} else {
				redirectAttributes.addFlashAttribute("alertFailMessageUserProfileUpdateFail",
						messageSource.getMessage("label.userProfileUpdateFail", null, null));
				return false;
			}
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessageUserProfileUpdateFail",
					messageSource.getMessage("label.userProfileUpdateFail", null, null));
			return false;
		}
	}

	@Transactional
	@Override
	public boolean changeUserPassword(@Valid UserChangePasswordWrapper userChangePasswordWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session,
			UserInfo userInfo) {
		String generatedSecuredPasswordHash = BCrypt.hashpw(userChangePasswordWrapper.getUserPassword(),
				BCrypt.gensalt(12));
		boolean flag = userRepository.userChangePasswordByUserId(generatedSecuredPasswordHash, userInfo.getUserId());
		if (flag) {
			redirectAttributes.addFlashAttribute("alertSuccessMessagechangePasswordSuccess",
					messageSource.getMessage("label.changePasswordSuccess", null, null));
			return true;
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessagechangePasswordFail",
					messageSource.getMessage("label.changePasswordFail", null, null));
			return false;
		}
	}

	@Transactional
	@Override
	public boolean uploadUserProfileImage(MultipartFile[] userProfileImage, String UserEmail, Model model, HttpSession session, RedirectAttributes redirectAttributes, MessageSource messageSource) {
		
		UserInfo userInfo = userRepository.getUserInfoByUserEmail(UserEmail);

		String userIdS = userInfo.getUserFName() + String.valueOf(userInfo.getUserId()) + userInfo.getUserLName();
		byte[] encodedBytes = Base64.encode(userIdS.getBytes());
		String encryptedUser = new String(encodedBytes);
		
		
		if (userProfileImage != null) {
			// Save file on system
			String userProfileImageName = "";

			File userProfileImagePath = new File(
					UserDefinedKeyWords.fileLocation + File.separator + "dHJhaW5lclByb2ZpbGVJbWFnZVZpZXdQYXRo");
			userProfileImagePath.mkdir();

			File userProfileIdFile = new File(userProfileImagePath + File.separator + encryptedUser);
			userProfileIdFile.mkdir();

			for (MultipartFile userImage : userProfileImage) {
				userProfileImageName = userImage.getOriginalFilename();

				if (!userProfileImageName.isEmpty()) {
					Path path = Paths.get(userProfileIdFile + File.separator + userProfileImageName);
					try {
						Files.write(path, userImage.getBytes());
					} catch (IOException e) {
						e.printStackTrace();
					}
					String userProfileImageStoredPath = path.toString();
					String userProfileImageViewPath = UserDefinedKeyWords.userProfileImageViewPath + encryptedUser + "/"
							+ userProfileImageName;

					String[] bits = userProfileImageName.split("\\.");

					String userProfileImageFileType = bits[bits.length - 1];

					if (userInfo.getUserProfileImageInfo() != null) {
						userRepository.updateUserProfileImageInfoByUserProfileImageId(
								userInfo.getUserProfileImageInfo().getUserProfileImageId(), userProfileImageName,
								userProfileImageStoredPath, userProfileImageViewPath, userProfileImageFileType);
					} else {
						Long userProfileImageId = userRepository.saveAndGetUserProfileImageId(userProfileImageName,
								userProfileImageStoredPath, userProfileImageViewPath, userProfileImageFileType);
						if (userProfileImageId != null) {
							userRepository.updateUserProfileImageIdByUserId(userInfo.getUserId(), userProfileImageId);
						}
					}
				}
			}
			UserInfo userInfo1 = userRepository.getUserInfoByUserEmail(UserEmail);
			if(userInfo1!=null) {
				//session.setAttribute("userProfileImageSession", userInfo1);
				model.addAttribute("userProfileImageSession", userInfo1);
			}
			redirectAttributes.addFlashAttribute("alertSuccessMessage",
					messageSource.getMessage("label.profileImageUploadScuccess", null, null));
			return true;
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessage",
					messageSource.getMessage("label.profileImageUploadFail", null, null));
			return false;
		}
	}

	@Transactional
	@Override
	public boolean deleteUserProfileImage(Long userProfileImageId, Long UserId) {
		UserInfo userInfo = userRepository.getUserInfoByUserId(UserId);
		if (userInfo != null) {
			if (userInfo.getUserProfileImageInfo() != null) {
				Path path = Paths.get(userInfo.getUserProfileImageInfo().getUserProfileImageStoredPath());
				try {
					Files.delete(path);
				} catch (IOException e) {
					e.printStackTrace();
				}
				boolean updateUserInfo = userRepository.updateUserProfileImageInfoToNull(userInfo.getUserId());

				if (updateUserInfo) {
					boolean deleteProfileImageStatus = userRepository
							.deleteUserProfileImageByUserProfileImageId(userProfileImageId);
					if (deleteProfileImageStatus) {
						return true;
					} else {
						return false;
					}
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public List<GenderInfo> getGenderList() {
		return userRepository.getGenderList();
	}

	@Transactional
	@Override
	public List<CategoryInfo> getActiveMainCategoryList() {
		return userRepository.getActiveMainCategoryList();
	}

	@Transactional
	@Override
	public List<TrainerCourseInfo> getAllActiveCourseList() {
		return userRepository.getAllActiveCourseList();
	}

	@Transactional
	@Override
	public boolean addCourseToFavouriteList(@Valid UserLoginWrapper userLoginWrapper, HttpSession session) {
		UserInfo userInfo = userRepository.getUserInfoByUserEmail(userLoginWrapper.getUserEmail());
		if (userInfo != null) {
			boolean status = false;
			FavouriteTrainerCourseUserInfo favouriteTrainerCourseUserInfo = userRepository
					.getFavouriteListUserInfoByUserIdAndTrainerCourseId(userInfo.getUserId(),
							userLoginWrapper.getTrainerCourseId());

			if (favouriteTrainerCourseUserInfo != null) {
				if (!favouriteTrainerCourseUserInfo.isFavouriteTrainerCourseUserIsActive()) {
					boolean updateToActivate = userRepository.updateUserFavListStatus(
							favouriteTrainerCourseUserInfo.getFavouriteTrainerCourseUserId(), true);
					if (updateToActivate)
						status = true;
					else
						status = false;
				} else
					status = true;
			} else {
				TrainerCourseInfo trainerCourseInfo = new TrainerCourseInfo();
				trainerCourseInfo.setTrainerCourseId(userLoginWrapper.getTrainerCourseId());

				FavouriteTrainerCourseUserInfo favouriteTrainerCourseUserInfo2 = new FavouriteTrainerCourseUserInfo();
				favouriteTrainerCourseUserInfo2.setUserInfo(userInfo);
				favouriteTrainerCourseUserInfo2.setTrainerCourseInfo(trainerCourseInfo);
				favouriteTrainerCourseUserInfo2.setFavouriteTrainerCourseUserIsActive(true);

				Long favListId = userRepository.addTrainerCourseToFavouriteList(favouriteTrainerCourseUserInfo2);
				if (favListId != null) {
					status = true;
				} else {
					status = false;
				}
			}
			if (session.getAttribute("myFavouriteListCount") != null)
				session.removeAttribute("myFavouriteListCount");
			List<FavouriteTrainerCourseUserInfo> favouriteTrainerCourseUserInfos = userRepository
					.getFavouriteTrainerCourseUserInfobyUserId(userInfo.getUserId());
			if (favouriteTrainerCourseUserInfos != null) {
				session.setAttribute("myFavouriteListCount", favouriteTrainerCourseUserInfos.size());
			} else {
				session.setAttribute("myFavouriteListCount", 0);
			}
			return status;
		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public List<TrainerCourseListResponse> checkTrainerCourseIsInUsersFavList(
			List<TrainerCourseListResponse> trainerCourseListResponses, UserInfo userInfo, HttpSession session) {

		for (int i = 0; i < trainerCourseListResponses.size(); i++) {
			FavouriteTrainerCourseUserInfo favouriteTrainerCourseUserInfo = userRepository
					.getFavouriteListUserInfoByUserIdAndTrainerCourseId(userInfo.getUserId(),
							trainerCourseListResponses.get(i).getTrainerCourseInfo().getTrainerCourseId());

			if (favouriteTrainerCourseUserInfo != null) {
				// watchListLocationsUserInfos.add(watchListLocationsUserInfo);
				trainerCourseListResponses.get(i).setWatchListed(
						favouriteTrainerCourseUserInfo.isFavouriteTrainerCourseUserIsActive() ? true : false);
			} else {
				trainerCourseListResponses.get(i).setWatchListed(false);
			}
		}
		if (session.getAttribute("myFavouriteListCount") != null)
			session.removeAttribute("myFavouriteListCount");
		List<FavouriteTrainerCourseUserInfo> favouriteTrainerCourseUserInfos = userRepository
				.getFavouriteTrainerCourseUserInfobyUserId(userInfo.getUserId());
		if (favouriteTrainerCourseUserInfos != null) {
			session.setAttribute("myFavouriteListCount", favouriteTrainerCourseUserInfos.size());
		} else {
			session.setAttribute("myFavouriteListCount", 0);
		}

		return trainerCourseListResponses;
	}

	@Transactional
	@Override
	public List<TrainerCourseInfo> courseFilterByCategoryId(long categoryId) {

		List<CourseInfo> courseInfos = userRepository.getActiveCourseListByCategoryId(categoryId);

		if (courseInfos != null) {

			List<Long> courseIds = new ArrayList<Long>();
			for (CourseInfo courseInfo : courseInfos) {
				courseIds.add(courseInfo.getCourseId());
			}
			courseIds = courseIds.stream().distinct().collect(Collectors.toList());

			if (!courseIds.isEmpty()) {
				return userRepository.getCourseListByCourseIds(courseIds);
			} else {
				return null;
			}

		} else {
			return null;
		}
	}

	@Transactional
	@Override
	public String userWatchListAddRemove(Long trainerCourseId, Long userId, boolean watchListStatus,
			HttpSession session) {

		FavouriteTrainerCourseUserInfo favouriteTrainerCourseUserInfo = userRepository
				.getFavouriteListUserInfoByUserIdAndTrainerCourseId(userId, trainerCourseId);

		String status = "fail";

		if (favouriteTrainerCourseUserInfo != null) {
			if (!favouriteTrainerCourseUserInfo.isFavouriteTrainerCourseUserIsActive()) {
				boolean updateToActivate = userRepository.updateUserFavListStatus(
						favouriteTrainerCourseUserInfo.getFavouriteTrainerCourseUserId(), true);
				if (updateToActivate)
					status = "added";
				else
					status = "fail";
			} else {
				boolean updateToDeActivate = userRepository.updateUserFavListStatus(
						favouriteTrainerCourseUserInfo.getFavouriteTrainerCourseUserId(), false);
				if (updateToDeActivate)
					status = "removed";
				else
					status = "fail";
			}
		} else {
			TrainerCourseInfo trainerCourseInfo = new TrainerCourseInfo();
			trainerCourseInfo.setTrainerCourseId(trainerCourseId);

			UserInfo userInfo = new UserInfo();
			userInfo.setUserId(userId);

			FavouriteTrainerCourseUserInfo favouriteTrainerCourseUserInfo2 = new FavouriteTrainerCourseUserInfo();
			favouriteTrainerCourseUserInfo2.setUserInfo(userInfo);
			favouriteTrainerCourseUserInfo2.setTrainerCourseInfo(trainerCourseInfo);

			Long favListId = userRepository.addTrainerCourseToFavouriteList(favouriteTrainerCourseUserInfo2);

			if (favListId != null) {
				status = "added";
			} else {
				status = "fail";
			}
		}
		if (session.getAttribute("myFavouriteListCount") != null)
			session.removeAttribute("myFavouriteListCount");
		List<FavouriteTrainerCourseUserInfo> favouriteTrainerCourseUserInfos = userRepository
				.getFavouriteTrainerCourseUserInfobyUserId(userId);
		if (favouriteTrainerCourseUserInfos != null) {
			session.setAttribute("myFavouriteListCount", favouriteTrainerCourseUserInfos.size());
		} else {
			session.setAttribute("myFavouriteListCount", 0);
		}

		return status;

	}

	@Transactional
	@Override
	public FavouriteTrainerCourseUserInfo getCourseFavouriteListStatus(Long trainerCourseId, Long userId) {
		FavouriteTrainerCourseUserInfo favouriteTrainerCourseUserInfo = userRepository
				.getFavouriteListUserInfoByUserIdAndTrainerCourseId(userId, trainerCourseId);
		if (favouriteTrainerCourseUserInfo != null) {
			if (favouriteTrainerCourseUserInfo.isFavouriteTrainerCourseUserIsActive())
				return favouriteTrainerCourseUserInfo;
			else
				return null;
		} else
			return null;
	}

	@Transactional
	@Override
	public List<FavouriteTrainerCourseUserInfo> getUserFavouriteTrainerCourseListByUserId(Long userId) {
		return userRepository.getFavouriteTrainerCourseUserInfobyUserId(userId);
	}

	@Transactional
	@Override
	public TrainerCourseUserQuestionInfo getTrainerCourseUserQuestionInfoByTrainerCourseId(Long trainerCourseId) {
		return userRepository.getTrainerCourseUserQuestionInfoByTrainerCourseId(trainerCourseId);
	}

	@Transactional
	@Override
	public List<TrainerCourseUserQuestionAnswerInfo> getTrainerCourseUserQuestionAnswerListByTrainerCourseUserQuestionId(
			Long trainerCourseUserQuestionId) {
		return userRepository
				.getTrainerCourseUserQuestionAnswerListByTrainerCourseUserQuestionId(trainerCourseUserQuestionId);
	}

	@Transactional
	@Override
	public boolean uploadTrainerCourseUserQuestion(
			@Valid TrainerCourseUserQuestionWrapper trainerCourseUserQuestionWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session) {

		TrainerCourseUserQuestionInfo trainerCourseUserQuestionInfo = new TrainerCourseUserQuestionInfo();
		trainerCourseUserQuestionInfo.setTrainerCourseUserQuestionDetails(
				trainerCourseUserQuestionWrapper.getTrainerCourseUserQuestionDetails());

		UserInfo userInfo = new UserInfo();
		userInfo.setUserId(trainerCourseUserQuestionWrapper.getUserId());
		trainerCourseUserQuestionInfo.setUserInfo(userInfo);

		TrainerCourseInfo trainerCourseInfo = new TrainerCourseInfo();
		trainerCourseInfo.setTrainerCourseId(trainerCourseUserQuestionWrapper.getTrainerCourseId());
		trainerCourseUserQuestionInfo.setTrainerCourseInfo(trainerCourseInfo);

		trainerCourseUserQuestionInfo = userRepository.saveTrainerCourseUserQuestionInfo(trainerCourseUserQuestionInfo);

		if (trainerCourseUserQuestionInfo != null) {
			return true;
		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public boolean postTrainerCourseUserQuestionAnswer(
			@Valid TrainerCourseUserQuestionAnswerWrapper trainerCourseUserQuestionAnswerWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session) {

		TrainerCourseUserQuestionAnswerInfo trainerCourseUserQuestionAnswerInfo = new TrainerCourseUserQuestionAnswerInfo();
		trainerCourseUserQuestionAnswerInfo.setTrainerCourseUserQuestionAnswerDetails(
				trainerCourseUserQuestionAnswerWrapper.getTrainerCourseUserQuestionAnswerDetails());

		TrainerCourseUserQuestionInfo trainerCourseUserQuestionInfo = new TrainerCourseUserQuestionInfo();
		trainerCourseUserQuestionInfo.setTrainerCourseUserQuestionId(
				trainerCourseUserQuestionAnswerWrapper.getTrainerCourseUserQuestionId());
		trainerCourseUserQuestionAnswerInfo.setTrainerCourseUserQuestionInfo(trainerCourseUserQuestionInfo);

		UserInfo userInfo = new UserInfo();
		userInfo.setUserId(trainerCourseUserQuestionAnswerWrapper.getUserId());
		trainerCourseUserQuestionAnswerInfo.setAnsweredUserInfo(userInfo);

		trainerCourseUserQuestionAnswerInfo = userRepository
				.saveTrainerCourseUserQuestionAnswerInfo(trainerCourseUserQuestionAnswerInfo);

		if (trainerCourseUserQuestionAnswerInfo != null) {
			return true;
		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public boolean addCourseCart(@Valid UserLoginWrapper userLoginWrapper, HttpSession session) {
		UserInfo userInfo = userRepository.getUserInfoByUserEmail(userLoginWrapper.getUserEmail());
		if (userInfo != null) {
			boolean status = false;
			CartTrainerCourseUserInfo cartTrainerCourseUserInfo = userRepository
					.getCartListUserInfoByUserIdAndTrainerCourseId(userInfo.getUserId(),
							userLoginWrapper.getTrainerCourseId());

			if (cartTrainerCourseUserInfo != null) {
				if (!cartTrainerCourseUserInfo.isCartTrainerCourseUserIsActive()) {
					boolean updateToActivate = userRepository
							.updateUserCartListStatus(cartTrainerCourseUserInfo.getCartTrainerCourseUserId(), true);
					if (updateToActivate) {
						status = true;
					} else {
						status = false;
					}
				} else
					status = true;
			} else {
				TrainerCourseInfo trainerCourseInfo = new TrainerCourseInfo();
				trainerCourseInfo.setTrainerCourseId(userLoginWrapper.getTrainerCourseId());

				CartTrainerCourseUserInfo cartTrainerCourseUserInfo2 = new CartTrainerCourseUserInfo();
				cartTrainerCourseUserInfo2.setUserInfo(userInfo);
				cartTrainerCourseUserInfo2.setTrainerCourseInfo(trainerCourseInfo);

				Long catListId = userRepository.addTrainerCourseToCartList(cartTrainerCourseUserInfo2);
				if (catListId != null) {
					status = true;
				} else {
					status = false;
				}
			}
			return status;
		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public List<CartTrainerCourseUserInfo> getUserCartTrainerCourseListByUserId(Long userId) {
		return userRepository.getCartTrainerCourseUserInfobyUserId(userId);
	}

	@Transactional
	@Override
	public CartTrainerCourseUserInfo getCourseCartListStatus(Long trainerCourseId, Long userId) {
		CartTrainerCourseUserInfo cartTrainerCourseUserInfo = userRepository
				.getCartListUserInfoByUserIdAndTrainerCourseId(userId, trainerCourseId);
		if (cartTrainerCourseUserInfo != null) {
			if (cartTrainerCourseUserInfo.isCartTrainerCourseUserIsActive()) {
				return cartTrainerCourseUserInfo;
			} else {
				return null;
			}
		} else
			return null;
	}

	@Transactional
	@Override
	public String userCartListAddRemove(Long trainerCourseId, Long userId, boolean watchListStatus,
			HttpSession session) {

		CartTrainerCourseUserInfo catCartTrainerCourseUserInfo = userRepository
				.getCartListUserInfoByUserIdAndTrainerCourseId(userId, trainerCourseId);

		String status = "fail";

		if (catCartTrainerCourseUserInfo != null) {
			if (!catCartTrainerCourseUserInfo.isCartTrainerCourseUserIsActive()) {
				boolean updateToActivate = userRepository
						.updateUserCartListStatus(catCartTrainerCourseUserInfo.getCartTrainerCourseUserId(), true);
				if (updateToActivate) {
					status = "added";
				} else {
					status = "fail";
				}
			} else {
				boolean updateToDeActivate = userRepository
						.updateUserCartListStatus(catCartTrainerCourseUserInfo.getCartTrainerCourseUserId(), false);
				if (updateToDeActivate) {
					status = "removed";
				} else {
					status = "fail";
				}
			}
		} else {
			TrainerCourseInfo trainerCourseInfo = new TrainerCourseInfo();
			trainerCourseInfo.setTrainerCourseId(trainerCourseId);

			UserInfo userInfo = new UserInfo();
			userInfo.setUserId(userId);

			CartTrainerCourseUserInfo cartTrainerCourseUserInfo2 = new CartTrainerCourseUserInfo();
			cartTrainerCourseUserInfo2.setUserInfo(userInfo);
			cartTrainerCourseUserInfo2.setTrainerCourseInfo(trainerCourseInfo);

			Long catListId = userRepository.addTrainerCourseToCartList(cartTrainerCourseUserInfo2);
			if (catListId != null) {
				status = "added";
			} else {
				status = "fail";
			}

		}
		return status;
	}

	@Transactional
	@Override
	public String userCartListRemove(Long cartTrainerCourseUserId, boolean cartListStatus, HttpSession session) {

		String status = "fail";
		// boolean updateToDeActivate =
		// userRepository.updateUserCartListStatus(cartTrainerCourseUserId,
		// !cartListStatus);

		boolean removeFromCart = userRepository.removeFromCart(cartTrainerCourseUserId);

		if (removeFromCart) {
			status = "removed";
		} else {
			status = "fail";
		}

		return status;
	}

	@Transactional
	@Override
	public boolean couponCodeCheckPayment(@Valid CouponCodePaymentWrapper couponCodePaymentWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session, Model model,
			HttpServletRequest request, Locale locale) {

		boolean voucherCardApplystatus = false;

		CouponCodeInfo couponCodeInfo = userRepository
				.couponCodeCheckPayment(couponCodePaymentWrapper.getCouponCodeValue());

		if (couponCodeInfo != null) {

			Double totalAmountAfterVoucherApplied = couponCodePaymentWrapper.getMyCartListTotalPrice()
					- couponCodeInfo.getCouponCodeAmount();
			if (totalAmountAfterVoucherApplied < 0) {
				totalAmountAfterVoucherApplied = 0.0;
			} /* else { */
			System.out.println("amountAdmin::" + totalAmountAfterVoucherApplied);
			// Long shaAmount = subscriptionAmountAfterVoucherApplied * 100;
			// System.out.println("shaAmountAdmin::" + shaAmount);
			// model.addAttribute("myCartListTotalPriceAfterCouponCode",
			// totalAmountAfterVoucherApplied);
			session.setAttribute("couponCodeId", couponCodeInfo.getCouponCodeId());
			session.setAttribute("couponCodeAmount", couponCodeInfo.getCouponCodeAmount());
			session.setAttribute("myCartListTotalPriceAfterCouponCode", totalAmountAfterVoucherApplied);
			voucherCardApplystatus = true;

			// }
		} else {
			voucherCardApplystatus = false;
			redirectAttributes.addFlashAttribute("alertFailMessage",
					messageSource.getMessage("label.voucherCardInvalid", null, null));
			// session.setAttribute("alertFailMessageVoucherCardInvalid",
			// messageSource.getMessage("label.voucherCardInvalid", null, null));
		}
		return voucherCardApplystatus;
	}

	@Transactional
	@Override
	public boolean userCoursePaymentProcess(String paymentId, String payerId, Payment coursePayment,
			HttpSession session, Model model) {

		UserInfo userInfo = (UserInfo) session.getAttribute("userSession");
		CourseContributionInfo courseContributionInfo = adminRepository.getCourseContributionInfo();
		
		if (userInfo != null) {

			UserOrderInfo userOrderInfo = new UserOrderInfo();
			userOrderInfo.setUserInfo(userInfo);
			userOrderInfo.setPaypalPayerId(payerId);
			userOrderInfo.setPaypalPaymentId(paymentId);

			String userOrderNumber = RandomStringUtils.randomAlphanumeric(9);
			userOrderInfo.setUserOrderNumber(userOrderNumber);

			Long couponCodeId = (Long) session.getAttribute("couponCodeId");
			if (couponCodeId != null) {
				CouponCodeInfo couponCodeInfo = userRepository.getCouponCodeInfoByCouponCodeId(couponCodeId);
				if (couponCodeInfo != null) {
					userOrderInfo.setCouponCodeInfo(couponCodeInfo);
				}
			}

			userOrderInfo.setUserOrderTotal(
					Double.parseDouble(coursePayment.getTransactions().get(0).getAmount().getTotal()));
			userOrderInfo = userRepository.saveUserOder(userOrderInfo);

			if (userOrderInfo != null) {
				List<CartTrainerCourseUserInfo> cartTrainerCourseUserInfos = userRepository
						.getCartTrainerCourseUserInfobyUserId(userInfo.getUserId());
				if (cartTrainerCourseUserInfos != null) {
					for (CartTrainerCourseUserInfo cartTrainerCourseUserInfo : cartTrainerCourseUserInfos) {
						UserCoursePaymentInfo userCoursePaymentInfo = new UserCoursePaymentInfo();
						userCoursePaymentInfo.setUserOrderInfo(userOrderInfo);
						userCoursePaymentInfo.setTrainerCourseInfo(cartTrainerCourseUserInfo.getTrainerCourseInfo());
						userCoursePaymentInfo.setUserInfo(cartTrainerCourseUserInfo.getUserInfo());
						userCoursePaymentInfo.setUserPaymentAdminContribution(cartTrainerCourseUserInfo.getTrainerCourseInfo().getTrainerCourseInfo().getCourseCost()*((100-courseContributionInfo.getCourseContributionPercentage())/100));
						userCoursePaymentInfo.setUserPaymentTrainerContribution(cartTrainerCourseUserInfo.getTrainerCourseInfo().getTrainerCourseInfo().getCourseCost()*(courseContributionInfo.getCourseContributionPercentage()/100));
						if(courseContributionInfo != null) {
							userCoursePaymentInfo.setCourseContributionInfo(courseContributionInfo);
						}
						userRepository.saveUserCoursePaymentInfo(userCoursePaymentInfo);
						userRepository.removeFromCart(cartTrainerCourseUserInfo.getCartTrainerCourseUserId());
					}
				}

				EmailSendingConfig emailSendingConfig = new EmailSendingConfig();
				String sub = "NovoFitness User Order";
				String emailHeader = "Hello " + userInfo.getUserFName() + " " + userInfo.getUserLName();

				List<UserCoursePaymentInfo> userCoursePaymentInfos = userRepository
						.getUserCoursePaymentInfoByUserOderId(userOrderInfo.getUserOrderId());

				String sub1 = "NovoFitness Purchase Order for your Course";
				String emailHeader1 = "Course Purchase Information";
				// String courseName = "";
				if (userCoursePaymentInfos != null) {
					for (UserCoursePaymentInfo userCoursePaymentInfo : userCoursePaymentInfos) {
						String courseName = userCoursePaymentInfo.getTrainerCourseInfo().getTrainerCourseInfo()
								.getCourseName() + "<br>";
						String emailBody1 = courseName + "<br/> User with name " + userInfo.getUserFName() + " "
								+ userInfo.getUserLName() + " has purchased your above course.";
						String msg1 = UserDefinedMethods.getEmailTemplateMessageWithoutButton(sub1, emailHeader1,
								emailBody1);

						emailSendingConfig.sendEmail(
								userCoursePaymentInfo.getTrainerCourseInfo().getTrainerInfo().getTrainerEmail(), sub1,
								msg1);
					}
					
					String emailBody = "Thank you for purchasing Course/Courses with NovoFitness.";
					String coursePurchasedUserName = userInfo.getUserFName() + " " + userInfo.getUserLName();
					String coursePurchasedUserEmail = userInfo.getUserEmail();
					String coursePurchasedDate = LocalDateTime.now().toString();
					String coursetrainerDetails = "";
					String courseDetails = "";
					for (UserCoursePaymentInfo userCoursePaymentInfo : userCoursePaymentInfos) {
						courseDetails += "<span><b>Course:</b>" + userCoursePaymentInfo.getTrainerCourseInfo().getTrainerCourseInfo().getCourseName() + "</span>"
									   + "<span><b>List Price:</b>"	+ userCoursePaymentInfo.getTrainerCourseInfo().getTrainerCourseInfo().getCoursePrice() + "</span>"
									   + "<span><b>Discount : </b>" + userCoursePaymentInfo.getUserOrderInfo().getCouponCodeInfo().getCouponCodeAmount() + "</span>"
									   + "<span><b>Tax : </b>" + userCoursePaymentInfo.getUserOrderInfo().getUserOrderTax() + "</span>"
									   + "<span><b>Total : </b>" + userCoursePaymentInfo.getUserOrderInfo().getUserOrderTotal() + "</span> <br>";
						coursetrainerDetails += "<span> " + userCoursePaymentInfo.getTrainerCourseInfo().getTrainerInfo().getTrainerFName() + " "
									   + userCoursePaymentInfo.getTrainerCourseInfo().getTrainerInfo().getTrainerLName() + " </span>";
					}
					String msg = UserDefinedMethods.getEmailTemplateMessageForPurchasingCourse(sub, emailHeader, emailBody, coursePurchasedUserName, coursePurchasedUserEmail, coursePurchasedDate, courseDetails, coursetrainerDetails);

					emailSendingConfig.sendEmail(userInfo.getUserEmail(), sub, msg);
				}
				// session.setAttribute("userOrderDetails", userOrderInfo);
				model.addAttribute("userOrderDetails", userOrderInfo);
				model.addAttribute("userOderSummaryList", userCoursePaymentInfos);
				model.addAttribute("userOrderTotalAmount", coursePayment.getTransactions().get(0).getAmount());

				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public String authorizeUserPayment(@Valid UserPaymentCheckoutWraspper userPaymentCheckoutWraspper)
			throws PayPalRESTException {

		Payer payer = getPayerInformation();
		RedirectUrls redirectUrls = getRedirectURLs();
		List<Transaction> listTransaction = getTransactionInformation(userPaymentCheckoutWraspper);

		Payment requestPayment = new Payment();
		requestPayment.setTransactions(listTransaction);
		requestPayment.setRedirectUrls(redirectUrls);
		requestPayment.setPayer(payer);
		requestPayment.setIntent("authorize");

		APIContext apiContext = new APIContext(UserDefinedKeyWords.PAYPAL_CLIENT_ID,
				UserDefinedKeyWords.PAYPAL_SECRET_KEY, UserDefinedKeyWords.PAYPAL_MODE);

		Payment approvedPayment = requestPayment.create(apiContext);

		// System.out.println("=== CREATED PAYMENT: ====");
		// System.out.println(approvedPayment);

		return getApprovalLink(approvedPayment);
	}

	private Payer getPayerInformation() {
		Payer payer = new Payer();
		payer.setPaymentMethod("paypal");

		// PayerInfo payerInfo = new PayerInfo();
		// payerInfo.setFirstName("William")
		// .setLastName("Peterson")
		// .setEmail("william.peterson@company.com");

		// payer.setPayerInfo(payerInfo);

		return payer;
	}

	private RedirectUrls getRedirectURLs() {
		RedirectUrls redirectUrls = new RedirectUrls();
		redirectUrls.setCancelUrl(UserDefinedKeyWords.URL + "userPaymentCancel");
		// redirectUrls.setReturnUrl(UserDefinedKeyWords.URL+"userReviewPayment");
		redirectUrls.setReturnUrl(UserDefinedKeyWords.URL + "executeUserPayment");
		return redirectUrls;
	}

	private List<Transaction> getTransactionInformation(
			@Valid UserPaymentCheckoutWraspper userPaymentCheckoutWraspper) {
		Details details = new Details();
		details.setSubtotal(userPaymentCheckoutWraspper.getMyCartListTotalPrice().toString());
		// if(userPaymentCheckoutWraspper.getCouponCodeId() != null) {
		// details.setSubtotal(userPaymentCheckoutWraspper.getMyCartListTotalPrice().toString());
		// } else {
		// details.setSubtotal(userPaymentCheckoutWraspper.getMyCartListTotalPrice().toString());
		// }

		Amount amount = new Amount();
		amount.setCurrency("USD");
		amount.setTotal(userPaymentCheckoutWraspper.getMyCartListTotalPrice().toString());
		amount.setDetails(details);

		Transaction transaction = new Transaction();
		transaction.setAmount(amount);
		transaction.setDescription("My Order");

		ItemList itemList = new ItemList();
		List<Item> items = new ArrayList<>();

		// System.out.println("userId::" + userPaymentCheckoutWraspper.getUserId());
		List<CartTrainerCourseUserInfo> cartTrainerCourseUserInfos = userRepository
				.getCartTrainerCourseUserInfobyUserId(userPaymentCheckoutWraspper.getUserId());
		// System.out.println("cartTrainerCourseUserInfos::" +
		// cartTrainerCourseUserInfos);
		for (CartTrainerCourseUserInfo cartTrainerCourseUserInfo : cartTrainerCourseUserInfos) {

			Item item = new Item();
			item.setCurrency("USD");
			item.setPrice(cartTrainerCourseUserInfo.getTrainerCourseInfo().getTrainerCourseInfo().getCoursePrice()
					.toString());
			// item.setTax("0.0");
			item.setQuantity("1");
			item.setName(cartTrainerCourseUserInfo.getTrainerCourseInfo().getTrainerCourseInfo().getCourseName());
			items.add(item);
		}

		if (userPaymentCheckoutWraspper.getCouponCodeId() != null) {
			CouponCodeInfo couponCodeInfo = userRepository
					.getCouponCodeInfoByCouponCodeId(userPaymentCheckoutWraspper.getCouponCodeId());
			if (couponCodeInfo != null) {
				Item item = new Item();
				item.setCurrency("USD");
				item.setPrice("-" + couponCodeInfo.getCouponCodeAmount().toString());
				// item.setTax("0.0");
				item.setQuantity("1");
				item.setName("Discount");
				items.add(item);
			}
		}

		itemList.setItems(items);
		transaction.setItemList(itemList);
		List<Transaction> listTransaction = new ArrayList<>();
		listTransaction.add(transaction);

		return listTransaction;
	}

	private String getApprovalLink(Payment approvedPayment) {
		List<Links> links = approvedPayment.getLinks();
		String approvalLink = null;

		for (Links link : links) {
			if (link.getRel().equalsIgnoreCase("approval_url")) {
				approvalLink = link.getHref();
				break;
			}
		}

		return approvalLink;
	}

	public Payment executePayment(String paymentId, String payerId) throws PayPalRESTException {
		PaymentExecution paymentExecution = new PaymentExecution();
		paymentExecution.setPayerId(payerId);

		Payment payment = new Payment().setId(paymentId);

		APIContext apiContext = new APIContext(UserDefinedKeyWords.PAYPAL_CLIENT_ID,
				UserDefinedKeyWords.PAYPAL_SECRET_KEY, UserDefinedKeyWords.PAYPAL_MODE);

		return payment.execute(apiContext, paymentExecution);
	}

	public Payment getPaymentDetails(String paymentId) throws PayPalRESTException {
		APIContext apiContext = new APIContext(UserDefinedKeyWords.PAYPAL_CLIENT_ID,
				UserDefinedKeyWords.PAYPAL_SECRET_KEY, UserDefinedKeyWords.PAYPAL_MODE);
		return Payment.get(apiContext, paymentId);
	}

	@Transactional
	@Override
	public List<UserCoursePaymentInfo> getUserCoursePaymentListByUserId(Long userId) {
		return userRepository.getUserCoursePaymentListByUserId(userId);
	}

	@Transactional
	@Override
	public boolean userReviewForTrainerCourse(@Valid UserCourseReviewWrapper userCourseReviewWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session) {

		UserCourseReviewInfo userCourseReviewInfo = new UserCourseReviewInfo();
		userCourseReviewInfo.setUserCourseReviewTitle(userCourseReviewWrapper.getUserCourseReviewTitle());
		userCourseReviewInfo.setUserCourseReviewDescription(userCourseReviewWrapper.getUserCourseReviewDescription());
		userCourseReviewInfo
				.setUserCourseStarRating(Long.parseLong(userCourseReviewWrapper.getUserCourseReviewStarRating()));

		UserInfo reviewedUserInfo = new UserInfo();
		reviewedUserInfo.setUserId(userCourseReviewWrapper.getReviewedUserId());
		userCourseReviewInfo.setUserInfo(reviewedUserInfo);

		TrainerCourseInfo trainerCourseInfo = new TrainerCourseInfo();
		trainerCourseInfo.setTrainerCourseId(userCourseReviewWrapper.getTrainerCourseId());
		userCourseReviewInfo.setTrainerCourseInfo(trainerCourseInfo);

		Long userCourseReviewId = userRepository.saveUserCourseReviewByUser(userCourseReviewInfo);
		if (userCourseReviewId != null) {
			redirectAttributes.addFlashAttribute("alertSuccessMessagelocationPostReviewSuccess",
					messageSource.getMessage("label.locationPostReviewSuccess", null, null));
			return true;
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessagelocationPostReviewFail",
					messageSource.getMessage("label.locationPostReviewFail", null, null));
			return false;
		}
	}

	@Transactional
	@Override
	public List<UserCourseReviewInfo> getUserCourseReviewByTrainerCourseId(Long trainerCourseId, Model model) {
		List<UserCourseReviewInfo> userCourseReviewInfos = userRepository
				.getUserCourseReviewByTrainerCourseId(trainerCourseId);
		if (userCourseReviewInfos != null) {
			List<Long> reviewsCount = new ArrayList<Long>();
			for (UserCourseReviewInfo userCourseReviewInfo : userCourseReviewInfos) {
				reviewsCount.add(userCourseReviewInfo.getUserCourseStarRating());
			}
			if (reviewsCount != null) {
				LongSummaryStatistics reviewsCountStat = reviewsCount.stream().mapToLong((x) -> x).summaryStatistics();
				BigDecimal bd = new BigDecimal(Double.toString(reviewsCountStat.getAverage()));
				bd = bd.setScale(1, BigDecimal.ROUND_UP);
				model.addAttribute("reviewsCountAverage", bd.doubleValue());
			}
		}
		return userCourseReviewInfos;
	}

	@Transactional
	@Override
	public List<TrainerCourseListResponse> getTrainerCourseListResponseWithAverageRating(
			List<TrainerCourseListResponse> trainerCourseListResponses) {

		List<TrainerCourseListResponse> trainerCourseListResponsesResult = new ArrayList<TrainerCourseListResponse>();

		for (TrainerCourseListResponse trainerCourseListResponse : trainerCourseListResponses) {
			// TrainerCourseListResponse userCourseListResponse = new
			// TrainerCourseListResponse();

			List<UserCourseReviewInfo> userCourseReviewInfos = userRepository.getUserCourseReviewByTrainerCourseId(
					trainerCourseListResponse.getTrainerCourseInfo().getTrainerCourseId());
			// System.out.println("userCourseReviewInfos::"+userCourseReviewInfos);
			if (userCourseReviewInfos != null) {
				List<Long> reviewsCount = new ArrayList<Long>();
				for (UserCourseReviewInfo userCourseReviewInfo : userCourseReviewInfos) {
					// System.out.println("userCourseReviewInfo::"+userCourseReviewInfo);
					reviewsCount.add(userCourseReviewInfo.getUserCourseStarRating());
				}
				// System.out.println("reviewsCount::"+reviewsCount);
				if (!reviewsCount.isEmpty()) {
					LongSummaryStatistics reviewsCountStat = reviewsCount.stream().mapToLong((x) -> x)
							.summaryStatistics();
					// System.out.println("reviewsCountStat::"+reviewsCountStat);
					trainerCourseListResponse.setAverageRating(reviewsCountStat.getAverage());
				} else {
					trainerCourseListResponse.setAverageRating(0.0);
				}
			} else {
				trainerCourseListResponse.setAverageRating(0.0);
			}
			trainerCourseListResponsesResult.add(trainerCourseListResponse);
		}
		// System.out.println("trainerCourseListResponsesResult::"+trainerCourseListResponsesResult);
		return trainerCourseListResponsesResult;

	}

	@Transactional
	@Override
	public boolean subscribeNewsLetter(@Valid NewsLetterWrapper newsLetterWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, Model model) {

		NewsLetterInfo newsLetterInfo = userRepository.getNewsLetterInfoByUserEmail(newsLetterWrapper.getUserEmail());

		if (newsLetterInfo != null && !newsLetterInfo.isNewsLetterUserIsDeletedByAdmin()) {

			if (!newsLetterInfo.isNewsLetterUserIsActive()) {
				userRepository.updateNewsLetterInfo(newsLetterInfo.getNewsLetterId(), true,
						newsLetterWrapper.getUserName());
			}

			redirectAttributes.addFlashAttribute("alertSuccessMessagenewsletterAlreadySubscribed",
					messageSource.getMessage("label.newsletterAlreadySubscribed", null, null));

			return true;
		} else {
			newsLetterInfo = new NewsLetterInfo();
			newsLetterInfo.setNewsLetterUserFName(newsLetterWrapper.getUserName());
			newsLetterInfo.setNewsLetterUserEmail(newsLetterWrapper.getUserEmail());
			Long newsLetterId = userRepository.saveNewsLetterSubscription(newsLetterInfo);

			if (newsLetterId != null) {

				EmailSendingConfig emailSendingConfig = new EmailSendingConfig();
				String sub = "NovoFitness NewsLetter Subscription";
				String emailHeader = "Hello " + newsLetterWrapper.getUserName();
				String emailBody = "Thank you for subscribing NewsLetter of NovoFitness ! <br> You will get our news, special offers by e-mailing.";
				String msg = UserDefinedMethods.getEmailTemplateMessageWithoutButton(sub, emailHeader, emailBody);
				emailSendingConfig.sendEmail(newsLetterWrapper.getUserEmail(), sub, msg);

				redirectAttributes.addFlashAttribute("alertSuccessMessagenewsletterSubscriptionSuccess",
						messageSource.getMessage("label.newsletterSubscriptionSuccess", null, null));
				return true;
			} else {
				redirectAttributes.addFlashAttribute("alertFailMessagenewsletterSubscriptionFail",
						messageSource.getMessage("label.newsletterSubscriptionFail", null, null));
				return false;
			}
		}
	}

	@Transactional
	@Override
	public UserCoursePaymentInfo getUserCoursePaymentInfoByTrainerCourseIdAndUserId(Long trainerCourseId, Long userId) {
		return userRepository.getUserCoursePaymentInfoByTrainerCourseIdAndUserId(trainerCourseId, userId);
	}

	@Transactional
	@Override
	public String userBuyNow(Long trainerCourseId, Long userId, boolean watchListStatus, HttpSession session) {
		
		CartTrainerCourseUserInfo catCartTrainerCourseUserInfo = userRepository
				.getCartListUserInfoByUserIdAndTrainerCourseId(userId, trainerCourseId);

		String status = "fail";

		if (catCartTrainerCourseUserInfo != null) {
			if (!catCartTrainerCourseUserInfo.isCartTrainerCourseUserIsActive()) {
				boolean updateToActivate = userRepository
						.updateUserCartListStatus(catCartTrainerCourseUserInfo.getCartTrainerCourseUserId(), true);
				if (updateToActivate) {
					status = "added";
				} else {
					status = "fail";
				}
			} else {
				status = "added";
			}
		} else {
			TrainerCourseInfo trainerCourseInfo = new TrainerCourseInfo();
			trainerCourseInfo.setTrainerCourseId(trainerCourseId);

			UserInfo userInfo = new UserInfo();
			userInfo.setUserId(userId);

			CartTrainerCourseUserInfo cartTrainerCourseUserInfo2 = new CartTrainerCourseUserInfo();
			cartTrainerCourseUserInfo2.setUserInfo(userInfo);
			cartTrainerCourseUserInfo2.setTrainerCourseInfo(trainerCourseInfo);

			Long catListId = userRepository.addTrainerCourseToCartList(cartTrainerCourseUserInfo2);
			if (catListId != null) {
				status = "added";
			} else {
				status = "fail";
			}
		}
		return status;
	}

	@Transactional
	@Override
	public UserInfo getUserInfoByUserEmail(String userEmail) {
		return userRepository.getUserInfoByUserEmail(userEmail);
	}

}