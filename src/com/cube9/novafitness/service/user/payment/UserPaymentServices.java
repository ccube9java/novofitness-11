package com.cube9.novafitness.service.user.payment;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import com.cube9.novafitness.config.UserDefinedKeyWords;
import com.cube9.novafitness.model.user.CartTrainerCourseUserInfo;
import com.cube9.novafitness.repository.user.UserRepositoryImp;
import com.cube9.novafitness.wrapper.user.payment.UserPaymentCheckoutWraspper;
import com.paypal.api.payments.Amount;
import com.paypal.api.payments.Details;
import com.paypal.api.payments.Item;
import com.paypal.api.payments.ItemList;
import com.paypal.api.payments.Links;
import com.paypal.api.payments.Payer;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.PaymentExecution;
import com.paypal.api.payments.RedirectUrls;
import com.paypal.api.payments.Transaction;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;

public class UserPaymentServices {

	public String authorizeUserPayment(@Valid UserPaymentCheckoutWraspper userPaymentCheckoutWraspper)
			throws PayPalRESTException {

		Payer payer = getPayerInformation();
		RedirectUrls redirectUrls = getRedirectURLs();
		List<Transaction> listTransaction = getTransactionInformation(userPaymentCheckoutWraspper);

		Payment requestPayment = new Payment();
		requestPayment.setTransactions(listTransaction);
		requestPayment.setRedirectUrls(redirectUrls);
		requestPayment.setPayer(payer);
		requestPayment.setIntent("authorize");

		APIContext apiContext = new APIContext(UserDefinedKeyWords.PAYPAL_CLIENT_ID,
				UserDefinedKeyWords.PAYPAL_SECRET_KEY, UserDefinedKeyWords.PAYPAL_MODE);

		Payment approvedPayment = requestPayment.create(apiContext);

		System.out.println("=== CREATED PAYMENT: ====");
		System.out.println(approvedPayment);

		return getApprovalLink(approvedPayment);
	}

	private Payer getPayerInformation() {
		Payer payer = new Payer();
		payer.setPaymentMethod("paypal");
		return payer;
	}

	private RedirectUrls getRedirectURLs() {
		RedirectUrls redirectUrls = new RedirectUrls();
		redirectUrls.setCancelUrl(UserDefinedKeyWords.URL + "userPaymentCancel");
		// redirectUrls.setReturnUrl(UserDefinedKeyWords.URL+"userReviewPayment");
		redirectUrls.setReturnUrl(UserDefinedKeyWords.URL + "executeUserPayment");
		return redirectUrls;
	}

	private List<Transaction> getTransactionInformation(
			@Valid UserPaymentCheckoutWraspper userPaymentCheckoutWraspper) {
		Details details = new Details();
		details.setShipping("0.0");
		details.setSubtotal("0.0");
		details.setTax("0.0");

		Amount amount = new Amount();
		amount.setCurrency("USD");
		amount.setTotal(userPaymentCheckoutWraspper.getMyCartListTotalPrice().toString());
		amount.setDetails(details);

		Transaction transaction = new Transaction();
		transaction.setAmount(amount);
		transaction.setDescription("My Order");

		ItemList itemList = new ItemList();
		List<Item> items = new ArrayList<>();

		// System.out.println("userId::"+userPaymentCheckoutWraspper.getUserId());
		UserRepositoryImp userRepositoryImp = new UserRepositoryImp();
		List<CartTrainerCourseUserInfo> cartTrainerCourseUserInfos = userRepositoryImp
				.getCartTrainerCourseUserInfobyUserId(userPaymentCheckoutWraspper.getUserId());
		// System.out.println("cartTrainerCourseUserInfos::"+cartTrainerCourseUserInfos);
		for (CartTrainerCourseUserInfo cartTrainerCourseUserInfo : cartTrainerCourseUserInfos) {
			Item item = new Item();
			item.setCurrency("USD");
			item.setPrice(cartTrainerCourseUserInfo.getTrainerCourseInfo().getTrainerCourseInfo().getCoursePrice()
					.toString());
			// item.setTax("0.0");
			item.setQuantity("1");
			item.setName(cartTrainerCourseUserInfo.getTrainerCourseInfo().getTrainerCourseInfo().getCourseName());
			items.add(item);
		}

		itemList.setItems(items);
		transaction.setItemList(itemList);
		List<Transaction> listTransaction = new ArrayList<>();
		listTransaction.add(transaction);

		return listTransaction;
	}

	private String getApprovalLink(Payment approvedPayment) {
		List<Links> links = approvedPayment.getLinks();
		String approvalLink = null;

		for (Links link : links) {
			if (link.getRel().equalsIgnoreCase("approval_url")) {
				approvalLink = link.getHref();
				break;
			}
		}

		return approvalLink;
	}

	public Payment executePayment(String paymentId, String payerId) throws PayPalRESTException {

		PaymentExecution paymentExecution = new PaymentExecution();
		paymentExecution.setPayerId(payerId);

		Payment payment = new Payment().setId(paymentId);

		APIContext apiContext = new APIContext(UserDefinedKeyWords.PAYPAL_CLIENT_ID,
				UserDefinedKeyWords.PAYPAL_SECRET_KEY, UserDefinedKeyWords.PAYPAL_MODE);

		return payment.execute(apiContext, paymentExecution);
	}

	public Payment getPaymentDetails(String paymentId) throws PayPalRESTException {
		APIContext apiContext = new APIContext(UserDefinedKeyWords.PAYPAL_CLIENT_ID,
				UserDefinedKeyWords.PAYPAL_SECRET_KEY, UserDefinedKeyWords.PAYPAL_MODE);
		return Payment.get(apiContext, paymentId);
	}

}
