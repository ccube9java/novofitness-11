package com.cube9.novafitness.service.trainer;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.context.MessageSource;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cube9.novafitness.model.course.CourseOtherImagesInfo;
import com.cube9.novafitness.model.course.CourseOtherPDFsInfo;
import com.cube9.novafitness.model.course.CourseOtherVideosInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseChapterInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseChapterLessonInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseFAQInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseInfo;
import com.cube9.novafitness.model.trainer.TrainerInfo;
import com.cube9.novafitness.model.user.UserCoursePaymentInfo;
import com.cube9.novafitness.model.user.UserCourseReviewInfo;
import com.cube9.novafitness.model.vimeo.VimeoException;
import com.cube9.novafitness.wrapper.course.TrainerCourseWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerChangePasswordWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerCourseChapterLessonWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerCourseChapterWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerCourseFAQWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerLoginWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerProfileWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerRegisterWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerResetPasswordWrapper;

public interface TrainerService {

	boolean checkTrainerDuplicateEmail(String trainerEmail);

	boolean trainerRegister(@Valid TrainerRegisterWrapper trainerRegisterWrapper, MessageSource messageSource, RedirectAttributes redirectAttributes);

	boolean trainerVerification(String trainerEmail, RedirectAttributes redirectAttributes, MessageSource messageSource);

	boolean trainerLogin(@Valid TrainerLoginWrapper trainerLoginWrapper, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session);

	boolean trainerForgetPassword(@Valid TrainerLoginWrapper trainerLoginWrapper, RedirectAttributes redirectAttributes,
			MessageSource messageSource);

	boolean resetTrainerPassword(@Valid TrainerResetPasswordWrapper trainerResetPasswordWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource);

	boolean uploadTrainerProfileImage(MultipartFile[] trainerProfileImage, String trainerEmail, HttpSession session);

	boolean updateTrainerProfile(@Valid TrainerProfileWrapper trainerProfileWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session);

	boolean deleteTrainerProfileImage(Long trainerProfileImageId, Long trainerId);

	boolean changeTrainerPassword(@Valid TrainerChangePasswordWrapper trainerChangePasswordWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session, TrainerInfo trainerInfo);

	boolean addNewCourse(@Valid TrainerCourseWrapper trainerCourseWrapper, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session) throws IOException, VimeoException;

	List<TrainerCourseInfo> getTrainerCourseListByTrainerId(Long trainerId);

	TrainerCourseInfo getTrainerCourseInfoByTrainerCourseId(Long trainerCourseId);

	boolean updateCourseInfo(@Valid TrainerCourseWrapper trainerCourseWrapper, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session) throws IOException, VimeoException;

	List<CourseOtherImagesInfo> getCourseOtherImagesListByCourseId(Long courseId);

	List<CourseOtherVideosInfo> getCourseOtherVideosListByCourseId(Long courseId);

	List<CourseOtherPDFsInfo> getCourseOtherPDFsListByCourseId(Long courseId);

	List<UserCourseReviewInfo> getTrainerCourseListWithReviewAndRating(Long trainerId);

	List<UserCoursePaymentInfo> getTrainerCourseUserPaymentListByTrainerId(Long trainerId);

	Long getAllTrainrCourseCountByTrainerId(Long trainerId);

	boolean deleteOtherImage(Long otherImageId, RedirectAttributes redirectAttributes, MessageSource messageSource,
			HttpSession session);

	boolean deleteOtherVideo(Long otherVideoId, RedirectAttributes redirectAttributes, MessageSource messageSource,
			HttpSession session);

	boolean deleteOtherPDF(Long otherPDFId, RedirectAttributes redirectAttributes, MessageSource messageSource,
			HttpSession session);

	boolean deleteCoverImage(Long coverImageId, Long courseId);

	boolean deleteCoverVideo(Long coverVideoId, Long courseId);

	boolean addNewTrainerCourseFAQ(@Valid TrainerCourseFAQWrapper trainerCourseFAQWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session);

	List<TrainerCourseFAQInfo> getTrainerCourseFAQListByTrainerCourseId(Long trainerCourseId);

	boolean addNewTrainerCourseChapter(@Valid TrainerCourseChapterWrapper trainerCourseChapterWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session);

	List<TrainerCourseChapterInfo> getTrainerCourseChapterListByTrainerCourseId(Long trainerCourseId);

	TrainerCourseChapterInfo getTrainerCourseChapterInfoByTrainerCourseChapterId(Long trainerCourseChapterId);

	List<TrainerCourseChapterLessonInfo> getTrainerCourseChapterLessonListByTrainerCourseChapterId(
			Long trainerCourseChapterId);

	boolean addNewTrainerCourseChapterLesson(@Valid TrainerCourseChapterLessonWrapper trainerCourseChapterLessonWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session) throws IOException, VimeoException;

	TrainerCourseChapterLessonInfo getTrainerCourseChapterLessonInfoByTrainerCourseChapterLessonId(
			Long trainerCourseChapterLessonId);

	boolean updateTrainerCourseChapterLesson(@Valid TrainerCourseChapterLessonWrapper trainerCourseChapterLessonWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session) throws IOException, VimeoException;

	boolean deleteTrainersCourseChapterLesson(Long trainerCourseChapterLessonId);

	boolean updateTrainerCourseChapter(@Valid TrainerCourseChapterWrapper trainerCourseChapterWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session);

	boolean deleteTrainersCourseChapter(Long trainerCourseChapterId);

	TrainerCourseFAQInfo getTrainerCourseFAQByTrainerCourseFAQId(Long trainerCourseFAQId);

	boolean updateTrainerCourseFAQ(@Valid TrainerCourseFAQWrapper trainerCourseFAQWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session);

	boolean deleteTrainersCourseFAQ(Long trainerCourseFAQId);

	boolean uploadCourseCoverImage(MultipartFile courseCoverImage, Long trainerId, Long courseId);

	boolean uploadCourseOtherImages(MultipartFile[] courseOtherImages, Long trainerId, Long courseId);

	boolean uploadCourseCoverVideo(MultipartFile courseCoverVideo, Long trainerId, Long courseId) throws IOException, VimeoException;

	boolean uploadCourseChapterLessonPreviewVideo(MultipartFile trainerCourseChapterLessonPreviewVideo,
			Long trainerCourseChapterLessonId) throws IOException, VimeoException;

	boolean uploadCourseChapterLessonMainVideo(MultipartFile trainerCourseChapterLessonMainVideo,
			Long trainerCourseChapterLessonId) throws IOException, VimeoException;

	boolean uploadCourseChapterLessonMainPdf(MultipartFile trainerCourseChapterLessonMainPdf,
			Long trainerCourseChapterLessonId);

	boolean deletePreviewVideo(Long trainerCourseChapterLessonId);

	boolean deleteActualVideo(Long trainerCourseChapterLessonId);

	boolean deleteMainPdf(Long trainerCourseChapterLessonId);

}