package com.cube9.novafitness.service.trainer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cube9.novafitness.config.EmailSendingConfig;
import com.cube9.novafitness.config.UserDefinedKeyWords;
import com.cube9.novafitness.config.UserDefinedMethods;
import com.cube9.novafitness.model.admin.CategoryInfo;
import com.cube9.novafitness.model.course.CourseCoverImageInfo;
import com.cube9.novafitness.model.course.CourseCoverVideoInfo;
import com.cube9.novafitness.model.course.CourseImagesInfo;
import com.cube9.novafitness.model.course.CourseInfo;
import com.cube9.novafitness.model.course.CourseOtherImagesInfo;
import com.cube9.novafitness.model.course.CourseOtherPDFsInfo;
import com.cube9.novafitness.model.course.CourseOtherVideosInfo;
import com.cube9.novafitness.model.course.CoursePDFInfo;
import com.cube9.novafitness.model.course.CourseVideoInfo;
import com.cube9.novafitness.model.trainer.TrainerCertificateInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseChapterInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseChapterLessonInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseFAQInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseInfo;
import com.cube9.novafitness.model.trainer.TrainerIDCopyInfo;
import com.cube9.novafitness.model.trainer.TrainerInfo;
import com.cube9.novafitness.model.user.UserCoursePaymentInfo;
import com.cube9.novafitness.model.user.UserCourseReviewInfo;
import com.cube9.novafitness.model.vimeo.Vimeo;
import com.cube9.novafitness.model.vimeo.VimeoException;
import com.cube9.novafitness.model.vimeo.VimeoResponse;
import com.cube9.novafitness.repository.trainer.TrainerRepository;
import com.cube9.novafitness.wrapper.course.TrainerCourseWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerChangePasswordWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerCourseChapterLessonWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerCourseChapterWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerCourseFAQWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerLoginWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerProfileWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerRegisterWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerResetPasswordWrapper;
import com.sun.jersey.core.util.Base64;

/**
 * @author Vaibhav Deshmane
 *
 */
@Service
public class TrainerServiceImp implements TrainerService {

	@Autowired
	private TrainerRepository trainerRepository;

	@Transactional
	@Override
	public boolean checkTrainerDuplicateEmail(String trainerEmail) {
		return trainerRepository.duplicateEmailCheck(trainerEmail);
	}

	@Transactional
	@Override
	public boolean trainerRegister(@Valid TrainerRegisterWrapper trainerRegisterWrapper, MessageSource messageSource,
			RedirectAttributes redirectAttributes) {
		TrainerInfo trainerInfo = new TrainerInfo();
		trainerInfo.setTrainerFName(trainerRegisterWrapper.getTrainerFName());
		trainerInfo.setTrainerLName(trainerRegisterWrapper.getTrainerLName());
		trainerInfo.setTrainerEmail(trainerRegisterWrapper.getTrainerEmail());

		String generatedSecuredPasswordHash = BCrypt.hashpw(trainerRegisterWrapper.getTrainerPassword(),
				BCrypt.gensalt(12));
		// System.out.println(generatedSecuredPasswordHash);

		trainerInfo.setTrainerPassword(generatedSecuredPasswordHash);

		// boolean matched = BCrypt.checkpw(trainerRegisterWrapper.getTrainerPassword(),
		// generatedSecuredPasswordHash);
		// System.out.println(matched);

		if (!trainerRegisterWrapper.getTrainerIDCopyInfo().isEmpty()) {

			String trainerIDCopyName = trainerRegisterWrapper.getTrainerIDCopyInfo().getOriginalFilename();

			if (!trainerIDCopyName.isEmpty()) {
				String[] bits = trainerIDCopyName.split("\\.");
				String trainerIDCopyFileType = bits[bits.length - 1];
				String trainerEmail = trainerRegisterWrapper.getTrainerEmail() + bits[0] + bits[bits.length - 1];
				// System.out.println("trainerEmail before encoded::"+trainerEmail);
				byte[] encodedBytes = Base64.encode(trainerEmail.getBytes());
				String encryptedTrainerEmail = new String(encodedBytes);
				// System.out.println("encryptedTrainerEmail after
				// encoded::"+encryptedTrainerEmail);

				File trainerIDCopy = new File(
						UserDefinedKeyWords.fileLocation + File.separator + "dHJhaW5lcklEQ29weVZpZXdQYXRo");
				trainerIDCopy.mkdir();

				File trainerEmailFile = new File(trainerIDCopy + File.separator + encryptedTrainerEmail);
				trainerEmailFile.mkdir();

				Path path = Paths.get(trainerEmailFile + File.separator + trainerIDCopyName);

				try {
					Files.write(path, trainerRegisterWrapper.getTrainerIDCopyInfo().getBytes());
				} catch (IOException e) {
					e.printStackTrace();
				}

				String trainerIDCopyStoredPath = path.toString();

				String trainerIDCopyViewPath = UserDefinedKeyWords.trainerIDCopyViewPath + encryptedTrainerEmail + "/"
						+ trainerIDCopyName;

				TrainerIDCopyInfo trainerIDCopyInfo = trainerRepository.saveTrainerIDCopy(trainerIDCopyName,
						trainerIDCopyStoredPath, trainerIDCopyViewPath, trainerIDCopyFileType);

				if (trainerIDCopyInfo != null) {
					trainerInfo.setTrainerIDCopyInfo(trainerIDCopyInfo);
				}
			}
		}

		if (!trainerRegisterWrapper.getTrainerCertificateInfo().isEmpty()) {

			String trainerCertificateName = trainerRegisterWrapper.getTrainerCertificateInfo().getOriginalFilename();

			if (!trainerCertificateName.isEmpty()) {
				String[] bits = trainerCertificateName.split("\\.");
				String trainerCertificateFileType = bits[bits.length - 1];
				String trainerEmail = trainerRegisterWrapper.getTrainerEmail() + bits[0] + bits[bits.length - 1];
				// System.out.println("trainerEmail before encoded::"+trainerEmail);
				byte[] encodedBytes = Base64.encode(trainerEmail.getBytes());
				String encryptedTrainerEmail = new String(encodedBytes);
				// System.out.println("encryptedTrainerEmail after
				// encoded::"+encryptedTrainerEmail);

				File trainerCertificate = new File(
						UserDefinedKeyWords.fileLocation + File.separator + "dHJhaW5lckNlcnRpZmljYXRlVmlld1BhdGg");
				trainerCertificate.mkdir();

				File trainerEmailFile = new File(trainerCertificate + File.separator + encryptedTrainerEmail);
				trainerEmailFile.mkdir();

				Path path = Paths.get(trainerEmailFile + File.separator + trainerCertificateName);

				try {
					Files.write(path, trainerRegisterWrapper.getTrainerCertificateInfo().getBytes());
				} catch (IOException e) {
					e.printStackTrace();
				}

				String trainerCertificateStoredPath = path.toString();

				String trainerCertificateViewPath = UserDefinedKeyWords.trainerCertificateViewPath
						+ encryptedTrainerEmail + "/" + trainerCertificateName;

				TrainerCertificateInfo trainerCertificateInfo = trainerRepository.saveTrainerCertificate(
						trainerCertificateName, trainerCertificateStoredPath, trainerCertificateViewPath,
						trainerCertificateFileType);

				if (trainerCertificateInfo != null) {
					trainerInfo.setTrainerCertificateInfo(trainerCertificateInfo);
				}
			}
		}

		trainerInfo = trainerRepository.saveTrainerInfo(trainerInfo);

		if (trainerInfo != null) {
			EmailSendingConfig emailSendingConfig = new EmailSendingConfig();
			byte[] encodedBytes = Base64.encode(trainerInfo.getTrainerEmail().getBytes());
			String encryptedEmail = new String(encodedBytes);
			String sub = "NovoFitness Trainer Account Verification";
			// String msg = "<table width='658px' align='center'
			// style='border-collapse:collapse' border='0' cellspacing='0' cellpadding='0'>"
			// + "<tbody>"
			// + "<tr> <td width='100%'
			// style='border-top:none;border-bottom:none;border-left:none;border-right:none;text-align:left;padding:15px
			// 15px;font-size:18px;font-family:arial'>"
			// + " <span> Hello " + trainerInfo.getTrainerFName() + " " +
			// trainerInfo.getTrainerLName()
			// + ",</span> </td> </tr>"
			// + "<tr> <td width='100%'
			// style='border-top:none;border-bottom:none;border-left:none;border-right:none;text-align:left;padding:5px
			// 15px;font-size:15px;font-family:arial'> "
			// + "<span>Thank you for your interest in registering with NovoFitness as a
			// Trainer !</span> </td> </tr>"
			// + "<tr> <td width='100%'
			// style='border-top:none;border-bottom:none;border-left:none;border-right:none;text-align:left;padding:5px
			// 15px;font-size:15px;font-family:arial'> "
			// + "<span>To complete your registration, we need you to verify your email
			// address.</span> </td> </tr>"
			// + "<tr><td><br> </td></tr>" + "<tr> <td width='100%'> <a href=" + url
			// + " style='background:#37a000;text-decoration:none;padding:8px
			// 8px;border-radius:4px;color:#fff;font-family:arial;margin-left:
			// 150px;'>Verify Email</a> </td> </tr>"
			// + "</tbody>" + "</table>";

			String emailClickURL = UserDefinedKeyWords.URL + "trainerEmailVerification/" + encryptedEmail;
			String emailHeader = "Hello " + trainerInfo.getTrainerFName() + " " + trainerInfo.getTrainerLName();
			String emailBody = "<p>Thank you for your interest in registering with NovoFitness as a Trainer ! </p> <p> To complete your registration, we need you to verify your email address.</p>";
			String emailClickButton = "Verify Email";
			String msg = UserDefinedMethods.getEmailTemplateMessage(sub, emailHeader, emailBody, emailClickURL,
					emailClickButton);

			emailSendingConfig.sendEmail(trainerInfo.getTrainerEmail(), sub, msg);

			redirectAttributes.addFlashAttribute("alertSuccessMessage",
					messageSource.getMessage("label.successFullTrainerRegister", null, null));
			return true;
		} else {
			redirectAttributes.addFlashAttribute("alertSuccessMessage",
					messageSource.getMessage("label.registerationFailed", null, null));
			return false;
		}
	}

	@Transactional
	@Override
	public boolean trainerVerification(String trainerEmail, RedirectAttributes redirectAttributes,
			MessageSource messageSource) {
		byte[] decodedBytes = Base64.decode(trainerEmail);
		String decodedEmail = new String(decodedBytes);
		boolean flag = trainerRepository.trainerVerification(decodedEmail);
		if (flag) {
			redirectAttributes.addFlashAttribute("alertSuccessMessage",
					messageSource.getMessage("label.userAccountVerificationSuccess", null, null));
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessage",
					messageSource.getMessage("label.userAccountVerificationFailed", null, null));
		}
		return flag;
	}

	@Transactional
	@Override
	public boolean trainerLogin(@Valid TrainerLoginWrapper trainerLoginWrapper, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session) {
		TrainerInfo trainerInfo = trainerRepository.getTrainerInfoByTrainerEmail(trainerLoginWrapper.getTrainerEmail());
		if (trainerInfo != null) {
			if (trainerInfo.isTrainerSAdminDeleteStatus()) {
				redirectAttributes.addFlashAttribute("alertFailMessageemailNotPresent",
						messageSource.getMessage("label.emailNotPresent", null, null));
				return false;
			} else {
				if (trainerInfo.isTrainerStatus()) {
					boolean matched = BCrypt.checkpw(trainerLoginWrapper.getTrainerPassword(),
							trainerInfo.getTrainerPassword());
					if (matched) {
						trainerRepository.updateLastTrainerLoginByTainerId(trainerInfo.getTrainerId(),
								LocalDateTime.now());
						session.setAttribute("trainerSession", trainerInfo);
						return true;
					} else {
						redirectAttributes.addFlashAttribute("alertFailMessageinvalidCredentials",
								messageSource.getMessage("label.invalidCredentials", null, null));
						return false;
					}
				} else {
					redirectAttributes.addFlashAttribute("alertFailMessageuserAccountVerificationFailed",
							messageSource.getMessage("label.userAccountVerificationFailed", null, null));
					return false;
				}
			}
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessageemailNotPresent",
					messageSource.getMessage("label.emailNotPresent", null, null));
			return false;
		}
	}

	@Transactional
	@Override
	public boolean trainerForgetPassword(@Valid TrainerLoginWrapper trainerLoginWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource) {
		TrainerInfo trainerInfo = trainerRepository.getTrainerInfoByTrainerEmail(trainerLoginWrapper.getTrainerEmail());

		if (trainerInfo != null) {
			if (!trainerInfo.isTrainerSAdminDeleteStatus()) {
				if (trainerInfo.isTrainerStatus()) {
					EmailSendingConfig emailSendingConfig = new EmailSendingConfig();
					byte[] encodedBytes = Base64.encode(trainerInfo.getTrainerEmail().getBytes());
					String encryptedEmail = new String(encodedBytes);
					String sub = "NovoFitness Trainer Reset Password";
					// String msg = "<table width='658px' align='center'
					// style='border-collapse:collapse' border='0' cellspacing='0' cellpadding='0'>"
					// + "<tbody>"
					// + "<tr> <td width='100%'
					// style='border-top:none;border-bottom:none;border-left:none;border-right:none;text-align:left;padding:15px
					// 15px;font-size:18px;font-family:arial'>"
					// + " <span> Hello " + trainerInfo.getTrainerFName() + " " +
					// trainerInfo.getTrainerLName()
					// + ",</span> </td> </tr>"
					// + "<tr> <td width='100%'
					// style='border-top:none;border-bottom:none;border-left:none;border-right:none;text-align:left;padding:5px
					// 15px;font-size:15px;font-family:arial'> "
					// + " <span>To reset your Password, please click on below button.</span> </td>
					// </tr>"
					// + "<tr><td><br> </td></tr>" + "<tr> <td width='100%'> <a href=" + url
					// + " style='background:#37a000;text-decoration:none;padding:8px
					// 8px;border-radius:4px;color:#fff;font-family:arial;margin-left: 150px;'>Reset
					// Password</a> </td> </tr>"
					// + "</tbody>" + "</table>";
					String emailClickURL = UserDefinedKeyWords.URL + "trainerResetPassword/" + encryptedEmail;
					String emailHeader = "Hello " + trainerInfo.getTrainerFName() + " " + trainerInfo.getTrainerLName();
					String emailBody = "To reset your Password, please click on below button.";
					String emailClickButton = "Reset Password";
					String msg = UserDefinedMethods.getEmailTemplateMessage(sub, emailHeader, emailBody, emailClickURL,
							emailClickButton);

					emailSendingConfig.sendEmail(trainerInfo.getTrainerEmail(), sub, msg);
					redirectAttributes.addFlashAttribute("alertSuccessMessagepasswordResetLinkMessage",
							messageSource.getMessage("label.passwordResetLinkMessage", null, null));
					return true;
				} else {
					redirectAttributes.addFlashAttribute("alertFailMessageuserAccountVerificationFailed",
							messageSource.getMessage("label.userAccountVerificationFailed", null, null));
					return false;
				}
			} else {
				redirectAttributes.addFlashAttribute("alertFailMessageemailNotPresent",
						messageSource.getMessage("label.emailNotPresent", null, null));
				return false;
			}
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessageemailNotPresent",
					messageSource.getMessage("label.emailNotPresent", null, null));
			return false;
		}
	}

	@Transactional
	@Override
	public boolean resetTrainerPassword(@Valid TrainerResetPasswordWrapper trainerResetPasswordWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource) {
		byte[] decodedBytes = Base64.decode(trainerResetPasswordWrapper.getTrainerEmail());
		String decodedEmail = new String(decodedBytes);
		trainerResetPasswordWrapper.setTrainerEmail(decodedEmail);

		String generatedSecuredPasswordHash = BCrypt.hashpw(trainerResetPasswordWrapper.getTrainerPassword(),
				BCrypt.gensalt(12));

		boolean flag = trainerRepository.trainerResetPassword(trainerResetPasswordWrapper,
				generatedSecuredPasswordHash);
		if (flag) {
			redirectAttributes.addFlashAttribute("alertSuccessMessagepasswordResetSuccess",
					messageSource.getMessage("label.passwordResetSuccess", null, null));
			return true;
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessageuserAccountVerificationFailed",
					messageSource.getMessage("label.userAccountVerificationFailed", null, null));
			return false;
		}
	}

	@Transactional
	@Override
	public boolean updateTrainerProfile(@Valid TrainerProfileWrapper trainerProfileWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session) {

		boolean flag = trainerRepository.updateTrainerProfileByTrainerEmail(trainerProfileWrapper);
		if (flag) {
			TrainerInfo trainerInfo = trainerRepository
					.getTrainerInfoByTrainerEmail(trainerProfileWrapper.getTrainerEmail());
			if (trainerInfo != null) {
				session.setAttribute("trainerSession", trainerInfo);
				redirectAttributes.addFlashAttribute("alertSuccessMessageTrainerProfileUpdateSuccess",
						messageSource.getMessage("label.trainerProfileUpdateSuccess", null, null));
				return true;
			} else {
				redirectAttributes.addFlashAttribute("alertFailMessageTrainerProfileUpdateFail",
						messageSource.getMessage("label.trainerProfileUpdateFail", null, null));
				return false;
			}
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessageTrainerProfileUpdateFail",
					messageSource.getMessage("label.trainerProfileUpdateFail", null, null));
			return false;
		}
	}

	@Transactional
	@Override
	public boolean changeTrainerPassword(@Valid TrainerChangePasswordWrapper trainerChangePasswordWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session,
			TrainerInfo trainerInfo) {
		boolean matched = BCrypt.checkpw(trainerChangePasswordWrapper.getTrainerOldPassword(),
				trainerInfo.getTrainerPassword());
		if (!matched) {
			redirectAttributes.addFlashAttribute("alertFailMessageoldPasswordFail",
					messageSource.getMessage("label.oldPasswordFail", null, null));
			return false;
		} else {
			String generatedSecuredPasswordHash = BCrypt.hashpw(trainerChangePasswordWrapper.getTrainerPassword(),
					BCrypt.gensalt(12));
			boolean flag = trainerRepository.trainerChangePasswordByTrainerId(generatedSecuredPasswordHash,
					trainerInfo.getTrainerId());
			if (flag) {
				redirectAttributes.addFlashAttribute("alertSuccessMessagechangePasswordSuccess",
						messageSource.getMessage("label.changePasswordSuccess", null, null));
				return true;
			} else {
				redirectAttributes.addFlashAttribute("alertFailMessagechangePasswordFail",
						messageSource.getMessage("label.changePasswordFail", null, null));
				return false;
			}
		}
	}

	@Transactional
	@Override
	public boolean uploadTrainerProfileImage(MultipartFile[] trainerProfileImage, String trainerEmail,
			HttpSession session) {

		TrainerInfo trainerInfo = trainerRepository.getTrainerInfoByTrainerEmail(trainerEmail);

		String trainerS = trainerInfo.getTrainerFName() + String.valueOf(trainerInfo.getTrainerId())
				+ trainerInfo.getTrainerLName();
		byte[] encodedBytes = Base64.encode(trainerS.getBytes());
		String encryptedTrainer = new String(encodedBytes);

		if (trainerProfileImage != null) {
			// Save file on system
			String trainerProfileImageName = "";

			File trainerProfileImagePath = new File(
					UserDefinedKeyWords.fileLocation + File.separator + "dXNlclByb2ZpbGVJbWFnZVZpZXdQYXRo");
			trainerProfileImagePath.mkdir();

			File trainerProfileIdFile = new File(trainerProfileImagePath + File.separator + encryptedTrainer);
			trainerProfileIdFile.mkdir();

			for (MultipartFile trainerImage : trainerProfileImage) {
				trainerProfileImageName = trainerImage.getOriginalFilename();

				if (!trainerProfileImageName.isEmpty()) {
					Path path = Paths.get(trainerProfileIdFile + File.separator + trainerProfileImageName);
					try {
						Files.write(path, trainerImage.getBytes());
					} catch (IOException e) {
						e.printStackTrace();
					}
					String trainerProfileImageStoredPath = path.toString();
					String trainerProfileImageViewPath = UserDefinedKeyWords.trainerProfileImageViewPath
							+ encryptedTrainer + "/" + trainerProfileImageName;

					String[] bits = trainerProfileImageName.split("\\.");

					String trainerProfileImageFileType = bits[bits.length - 1];

					if (trainerInfo.getTrainerProfileImageInfo() != null) {
						trainerRepository.updateTrainerProfileImageInfoByTrainerProfileImageId(
								trainerInfo.getTrainerProfileImageInfo().getTrainerProfileImageId(),
								trainerProfileImageName, trainerProfileImageStoredPath, trainerProfileImageViewPath,
								trainerProfileImageFileType);
					} else {
						Long trainerProfileImageId = trainerRepository.saveAndGetTrainerProfileImageId(
								trainerProfileImageName, trainerProfileImageStoredPath, trainerProfileImageViewPath,
								trainerProfileImageFileType);
						if (trainerProfileImageId != null) {
							trainerRepository.updateTrainerProfileImageIdByTrainerId(trainerInfo.getTrainerId(),
									trainerProfileImageId);
						}
					}
				}
			}
			session.setAttribute("trainerSession", trainerInfo);
			return true;
		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public boolean deleteTrainerProfileImage(Long trainerProfileImageId, Long trainerId) {
		TrainerInfo trainerInfo = trainerRepository.getTrainerInfoByTrainerId(trainerId);
		if (trainerInfo != null) {
			if (trainerInfo.getTrainerProfileImageInfo() != null) {
				Path path = Paths.get(trainerInfo.getTrainerProfileImageInfo().getTrainerProfileImageStoredPath());
				try {
					Files.delete(path);
				} catch (IOException e) {
					e.printStackTrace();
				}
				boolean updateTrainerInfo = trainerRepository
						.updateTrainerProfileImageInfoToNull(trainerInfo.getTrainerId());

				if (updateTrainerInfo) {
					boolean deleteProfileImageStatus = trainerRepository
							.deleteTrainerProfileImageByTrainerProfileImageId(trainerProfileImageId);
					if (deleteProfileImageStatus) {
						return true;
					} else {
						return false;
					}
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public boolean addNewCourse(@Valid TrainerCourseWrapper trainerCourseWrapper, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session) throws IOException, VimeoException {

		boolean status = false;

		// System.out.println("courseDescription::"+trainerCourseWrapper.getCourseDescription());
		// System.out.println("courseContent::"+trainerCourseWrapper.getCourseContent());

		TrainerInfo trainerInfo = trainerRepository.getTrainerInfoByTrainerId(trainerCourseWrapper.getTrainerId());

		if (trainerInfo != null) {
			CategoryInfo courseCategoryInfo = new CategoryInfo();
			courseCategoryInfo.setCategoryId(trainerCourseWrapper.getCategoryId());

			CourseInfo courseInfo = new CourseInfo();
			courseInfo.setCourseCategoryInfo(courseCategoryInfo);
			courseInfo.setCourseName(trainerCourseWrapper.getCourseName());
			courseInfo.setCourseDescription(trainerCourseWrapper.getCourseDescription());
			courseInfo.setCourseLanguage(trainerCourseWrapper.getCourseLanguage());
			courseInfo.setCourseContent(trainerCourseWrapper.getCourseContent());
			courseInfo.setCourseIsActive(true);
			courseInfo.setCoursePrice(
					trainerCourseWrapper.getCoursePrice() != "" ? Long.parseLong(trainerCourseWrapper.getCoursePrice())
							: null);
			courseInfo.setCourseCost(trainerCourseWrapper.getCoursePrice() != ""
					? Double.parseDouble(trainerCourseWrapper.getCoursePrice())
					: null);

			courseInfo = trainerRepository.saveAndGetCourseInfo(courseInfo);

			if (courseInfo != null) {
				TrainerCourseInfo trainerCourseInfo = new TrainerCourseInfo();
				trainerCourseInfo.setTrainerCourseInfo(courseInfo);
				trainerCourseInfo.setTrainerInfo(trainerInfo);
				trainerCourseInfo.setTrainerCourseIsActive(true);
				trainerRepository.saveTrainerCourseInfo(trainerCourseInfo);

				/*
				 * CourseCoverImageInfo courseCoverImageInfo = saveCourseCoverImageInfo(
				 * trainerCourseWrapper.getCourseCoverImage(), trainerInfo, courseInfo);
				 * 
				 * if (courseCoverImageInfo != null) {
				 * 
				 * boolean updateStatus =
				 * trainerRepository.updateCourseInfoCoverImageIdByCourseId(
				 * courseInfo.getCourseId(), courseCoverImageInfo.getCourseCoverImageId());
				 * 
				 * if (updateStatus) {
				 * redirectAttributes.addFlashAttribute("alertSuccessMessageForAddingNewCourse",
				 * messageSource.getMessage("label.addNewCourseSuccess", null, null)); status =
				 * true; } else { redirectAttributes.addFlashAttribute(
				 * "alertFailMessageForAddingNewCourseOnlyCoverImageFailure",
				 * messageSource.getMessage("label.addNewCourseFailsOnlyDueToCoverImage", null,
				 * null)); status = false; }
				 * 
				 * } else { redirectAttributes.addFlashAttribute(
				 * "alertFailMessageForAddingNewCourseOnlyCoverImageFailure",
				 * messageSource.getMessage("label.addNewCourseFailsOnlyDueToCoverImage", null,
				 * null)); status = false; }
				 */

				/*
				 * CourseCoverVideoInfo courseCoverVideoInfo = saveCourseCoverVideoInfo(
				 * trainerCourseWrapper.getCourseCoverVideo(), trainerInfo, courseInfo);
				 */

				// System.out.println("courseCoverVideoInfo::"+courseCoverVideoInfo);

				/*
				 * if (courseCoverVideoInfo != null) {
				 * 
				 * boolean updateCourseVideoStatus =
				 * trainerRepository.updateCourseInfoCoverVideoIdByCourseId(
				 * courseInfo.getCourseId(), courseCoverVideoInfo.getCourseCoverVideoId());
				 * //System.out.println("updateCourseVideoStatus::"+updateCourseVideoStatus);
				 * 
				 * if (updateCourseVideoStatus) { status = true;
				 * redirectAttributes.addFlashAttribute("alertSuccessMessageForAddingNewCourse",
				 * messageSource.getMessage("label.addNewCourseSuccess", null, null)); } else {
				 * redirectAttributes.addFlashAttribute(
				 * "alertFailMessageForAddingNewCourseOnlyCoverImageFailure",
				 * messageSource.getMessage("label.addNewCourseFailsOnlyDueToCoverImage", null,
				 * null)); status = false; }
				 * 
				 * } else { redirectAttributes.addFlashAttribute(
				 * "alertFailMessageForAddingNewCourseOnlyCoverImageFailure",
				 * messageSource.getMessage("label.addNewCourseFailsOnlyDueToCoverImage", null,
				 * null)); status = false; }
				 */
				// saveCourseOtherImagesInfo(trainerCourseWrapper.getCourseOtherImages(),
				// trainerInfo, courseInfo);
				// saveCourseOtherVideosInfo(trainerCourseWrapper.getCourseOtherVideos(),
				// trainerInfo, courseInfo);
				// saveCourseOtherPDFInfo(trainerCourseWrapper.getCourseOtherPdfs(),
				// trainerInfo, courseInfo);
				status = true;
				redirectAttributes.addFlashAttribute("alertSuccessMessageForAddingNewCourse",
						messageSource.getMessage("label.addNewCourseSuccess", null, null));
			} else {
				redirectAttributes.addFlashAttribute("alertFailForAddingNewCourse",
						messageSource.getMessage("label.addNewCourseFail", null, null));
				status = false;
			}
		} else {
			redirectAttributes.addFlashAttribute("alertFailForAddingNewCourse",
					messageSource.getMessage("label.addNewCourseFail", null, null));
			status = false;
		}
		return status;
	}

	private CourseCoverVideoInfo saveCourseCoverVideoInfo(MultipartFile courseCoverVideo, TrainerInfo trainerInfo,
			CourseInfo courseInfo) throws IOException, VimeoException {

		String trainerIdCourseIdtrainerEmail = String.valueOf(trainerInfo.getTrainerId())
				+ trainerInfo.getTrainerEmail() + String.valueOf(courseInfo.getCourseId());

		Vimeo vimeo = new Vimeo("f161b3f9226918f4fafdd2331b48daa4");

		String courseCoverVideoName = courseCoverVideo.getOriginalFilename();

		if (!courseCoverVideoName.isEmpty()) {

			String[] bits = courseCoverVideoName.split("\\.");
			String courseCoverVideoFileType = bits[bits.length - 1];
			String coverVideoName = trainerIdCourseIdtrainerEmail + bits[0] + bits[bits.length - 1];
			// System.out.println("coverImageName before encoded::"+coverImageName);
			byte[] encodedBytes = Base64.encode(coverVideoName.getBytes());
			String encryptedName = new String(encodedBytes);
			// System.out.println("coverImageName after encoded::"+encryptedName);

			File trainerCourseMedia = new File(
					UserDefinedKeyWords.fileLocation + File.separator + "dHJhaW5lckNvdXJzZU1lZGlhSW5mb1ZpZXdQYXRo");
			trainerCourseMedia.mkdir();

			File trainerCourseInfo = new File(trainerCourseMedia + File.separator + encryptedName);
			trainerCourseInfo.mkdir();

			File coverVideo = new File(trainerCourseInfo + File.separator + "Y292ZXJWaWRlbw");
			coverVideo.mkdir();

			Path path = Paths.get(coverVideo + File.separator + courseCoverVideoName);

			try {
				Files.write(path, courseCoverVideo.getBytes());
			} catch (IOException e) {
				e.printStackTrace();
			}

			String courseCoverVideoStoredPath = path.toString();

			String courseCoverVideoViewPath = UserDefinedKeyWords.trainerCourseMediaInfoViewPath + encryptedName
					+ "/Y292ZXJWaWRlbw/" + courseCoverVideoName;

			// add a video
			boolean upgradeTo1080 = true;
			// String videoEndPoint = vimeo.addVideo(new
			// File("/home/cube9/Downloads/SampleVideo_1280x720_1mb.mp4"), upgradeTo1080);
			String videoEndPoint = vimeo.addVideo(new File(path.toString()), upgradeTo1080);
			// get video info
			VimeoResponse info = vimeo.getVideoInfo(videoEndPoint);
			// System.out.println("info::::::::::::::::::" + info);
			// System.out.println("link::::::::::::::::::" +
			// info.getJson().getString("link"));
			String courseCoverVideoVimeoLink = info.getJson().getString("link");

			String[] arrOfStr = courseCoverVideoVimeoLink.split("https://vimeo.com/");
			String courseCoverVideoVimeoName = arrOfStr[1];
			// System.out.println("courseVideoVimeoName::"+courseVideoVimeoName);
			// edit video
			String name = courseCoverVideo.getOriginalFilename();
			String desc = courseCoverVideo.getOriginalFilename();
			String license = ""; // see Vimeo API Documentation
			// String privacyView = "disable"; //see Vimeo API Documentation
			// String privacyEmbed = "whitelist"; //see Vimeo API Documentation
			String privacyView = "anybody"; // see Vimeo API Documentation
			String privacyEmbed = "public"; // see Vimeo API Documentation

			boolean reviewLink = false;
			vimeo.updateVideoMetadata(videoEndPoint, name, desc, license, privacyView, privacyEmbed, reviewLink);

			// add video privacy domain
			// vimeo.addVideoPrivacyDomain(videoEndPoint, "clickntap.com");

			// delete video
			// vimeo.removeVideo(videoEndPoint);

			CourseCoverVideoInfo courseCoverVideoInfo = new CourseCoverVideoInfo();
			courseCoverVideoInfo.setCourseCoverVideoName(courseCoverVideoName);
			courseCoverVideoInfo.setCourseCoverVideoStoredPath(courseCoverVideoStoredPath);
			courseCoverVideoInfo.setCourseCoverVideoViewPath(courseCoverVideoViewPath);
			courseCoverVideoInfo.setCourseCoverVideoFileType(courseCoverVideoFileType);
			courseCoverVideoInfo.setCourseCoverVideoVimeoLink(courseCoverVideoVimeoLink);
			courseCoverVideoInfo.setCourseCoverVideoVimeoName(courseCoverVideoVimeoName);

			return trainerRepository.saveCourseCoverVideoInfoAndGetCourseCoverVideoInfo(courseCoverVideoInfo);

		} else {
			return null;
		}
	}

	private CourseCoverImageInfo saveCourseCoverImageInfo(MultipartFile courseCoverImage, TrainerInfo trainerInfo,
			CourseInfo courseInfo) {

		String trainerIdCourseIdtrainerEmail = String.valueOf(trainerInfo.getTrainerId())
				+ trainerInfo.getTrainerEmail() + String.valueOf(courseInfo.getCourseId());

		if (!courseCoverImage.isEmpty()) {

			String courseCoverImageName = courseCoverImage.getOriginalFilename();

			if (!courseCoverImageName.isEmpty()) {
				String[] bits = courseCoverImageName.split("\\.");
				String courseCoverImageFileType = bits[bits.length - 1];
				String coverImageName = trainerIdCourseIdtrainerEmail + bits[0] + bits[bits.length - 1];
				// System.out.println("coverImageName before encoded::"+coverImageName);
				byte[] encodedBytes = Base64.encode(coverImageName.getBytes());
				String encryptedName = new String(encodedBytes);
				// System.out.println("coverImageName after encoded::"+encryptedName);

				File trainerCourseMedia = new File(
						UserDefinedKeyWords.fileLocation + File.separator + "dHJhaW5lckNvdXJzZU1lZGlhSW5mb1ZpZXdQYXRo");
				trainerCourseMedia.mkdir();

				File trainerCourseInfo = new File(trainerCourseMedia + File.separator + encryptedName);
				trainerCourseInfo.mkdir();

				File coverImage = new File(trainerCourseInfo + File.separator + "Y292ZXJJbWFnZQ");
				coverImage.mkdir();

				Path path = Paths.get(coverImage + File.separator + courseCoverImageName);

				try {
					Files.write(path, courseCoverImage.getBytes());
				} catch (IOException e) {
					e.printStackTrace();
				}

				String courseCoverImageStoredPath = path.toString();

				String courseCoverImageViewPath = UserDefinedKeyWords.trainerCourseMediaInfoViewPath + encryptedName
						+ "/Y292ZXJJbWFnZQ/" + courseCoverImageName;

				CourseCoverImageInfo courseCoverImageInfo = new CourseCoverImageInfo();
				courseCoverImageInfo.setCourseCoverImageName(courseCoverImageName);
				courseCoverImageInfo.setCourseCoverImageStoredPath(courseCoverImageStoredPath);
				courseCoverImageInfo.setCourseCoverImageViewPath(courseCoverImageViewPath);
				courseCoverImageInfo.setCourseCoverImageFileType(courseCoverImageFileType);

				return trainerRepository.saveCoverImageInfoAndGetCoverImageInfo(courseCoverImageInfo);

			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	private void saveCourseOtherImagesInfo(MultipartFile[] courseOtherImages, TrainerInfo trainerInfo,
			CourseInfo courseInfo) {

		String trainerIdCourseIdtrainerEmail = String.valueOf(trainerInfo.getTrainerId())
				+ trainerInfo.getTrainerEmail() + String.valueOf(courseInfo.getCourseId());

		String courseOtherImageName = "", courseImageStoredPath = "", courseImageViewPath = "",
				courseImageFileType = "";

		// System.out.println("trainerIdCourseIdtrainerEmail::"+trainerIdCourseIdtrainerEmail);

		for (MultipartFile courseOtherImage : courseOtherImages) {
			courseOtherImageName = courseOtherImage.getOriginalFilename();
			// System.out.println("courseOtherImageName::"+courseOtherImageName);
			if (!courseOtherImageName.isEmpty()) {
				String[] bits = courseOtherImageName.split("\\.");
				courseImageFileType = bits[bits.length - 1];
				String otherImageName = trainerIdCourseIdtrainerEmail + bits[0] + bits[bits.length - 1];
				// System.out.println("coverImageName before encoded::" + otherImageName);
				byte[] encodedBytes = Base64.encode(otherImageName.getBytes());
				String encryptedName = new String(encodedBytes);
				// System.out.println("coverImageName after encoded::" + encryptedName);

				File trainerCourseMedia = new File(
						UserDefinedKeyWords.fileLocation + File.separator + "dHJhaW5lckNvdXJzZU1lZGlhSW5mb1ZpZXdQYXRo");
				trainerCourseMedia.mkdir();

				File trainerCourseInfo = new File(trainerCourseMedia + File.separator + encryptedName);
				trainerCourseInfo.mkdir();

				File courseMoreImages = new File(trainerCourseInfo + File.separator + "Y291cnNlTW9yZUltYWdlcw");
				courseMoreImages.mkdir();

				Path path = Paths.get(courseMoreImages + File.separator + courseOtherImageName);

				try {
					Files.write(path, courseOtherImage.getBytes());
				} catch (IOException e) {
					e.printStackTrace();
				}

				courseImageStoredPath = path.toString();

				courseImageViewPath = UserDefinedKeyWords.trainerCourseMediaInfoViewPath + encryptedName
						+ "/Y291cnNlTW9yZUltYWdlcw/" + courseOtherImageName;

				CourseImagesInfo courseImagesInfo = new CourseImagesInfo();
				courseImagesInfo.setCourseImagesName(courseOtherImageName);
				courseImagesInfo.setCourseImagesStoredPath(courseImageStoredPath);
				courseImagesInfo.setCourseImagesViewPath(courseImageViewPath);
				courseImagesInfo.setCourseImagesFileType(courseImageFileType);

				courseImagesInfo = trainerRepository.saveCourseImagesInfoAndGetCourseImagesInfo(courseImagesInfo);

				if (courseImagesInfo != null) {
					CourseOtherImagesInfo courseOtherImagesInfo = new CourseOtherImagesInfo();
					courseOtherImagesInfo.setCourseImagesInfo(courseImagesInfo);
					courseOtherImagesInfo.setCourseInfo(courseInfo);

					trainerRepository.saveCourseOtherImagesInfoAndGetCourseOtherImagesInfo(courseOtherImagesInfo);

				}
			}
		}
	}

	private void saveCourseOtherPDFInfo(MultipartFile[] courseOtherPdfs, TrainerInfo trainerInfo,
			CourseInfo courseInfo) {

		String trainerIdCourseIdtrainerEmail = String.valueOf(trainerInfo.getTrainerId())
				+ trainerInfo.getTrainerEmail() + String.valueOf(courseInfo.getCourseId());

		String coursePdfName = "", coursePdfStoredPath = "", coursePdfViewPath = "", coursePdfFileType = "";

		for (MultipartFile coursePdf : courseOtherPdfs) {
			coursePdfName = coursePdf.getOriginalFilename();
			if (!coursePdfName.isEmpty()) {
				String[] bits = coursePdfName.split("\\.");
				coursePdfFileType = bits[bits.length - 1];
				String otherImageName = trainerIdCourseIdtrainerEmail + bits[0] + bits[bits.length - 1];
				// System.out.println("coverImageName before encoded::" + otherImageName);
				byte[] encodedBytes = Base64.encode(otherImageName.getBytes());
				String encryptedName = new String(encodedBytes);
				// System.out.println("coverImageName after encoded::" + encryptedName);

				File trainerCourseMedia = new File(
						UserDefinedKeyWords.fileLocation + File.separator + "dHJhaW5lckNvdXJzZU1lZGlhSW5mb1ZpZXdQYXRo");
				trainerCourseMedia.mkdir();

				File trainerCourseInfo = new File(trainerCourseMedia + File.separator + encryptedName);
				trainerCourseInfo.mkdir();

				File coursePdfs = new File(trainerCourseInfo + File.separator + "Y291cnNlUGRmcw");
				coursePdfs.mkdir();

				Path path = Paths.get(coursePdfs + File.separator + coursePdfName);

				try {
					Files.write(path, coursePdf.getBytes());
				} catch (IOException e) {
					e.printStackTrace();
				}

				coursePdfStoredPath = path.toString();

				coursePdfViewPath = UserDefinedKeyWords.trainerCourseMediaInfoViewPath + encryptedName
						+ "/Y291cnNlUGRmcw/" + coursePdfName;

				CoursePDFInfo coursePDFInfo = new CoursePDFInfo();
				coursePDFInfo.setCoursePDFName(coursePdfName);
				coursePDFInfo.setCoursePDFStoredPath(coursePdfStoredPath);
				coursePDFInfo.setCoursePDFViewPath(coursePdfViewPath);
				coursePDFInfo.setCoursePDFFileType(coursePdfFileType);

				coursePDFInfo = trainerRepository.saveCourseImagesInfoAndGetCourseImagesInfo(coursePDFInfo);

				if (coursePDFInfo != null) {
					CourseOtherPDFsInfo courseOtherPDFsInfo = new CourseOtherPDFsInfo();
					courseOtherPDFsInfo.setCoursePDFInfo(coursePDFInfo);
					courseOtherPDFsInfo.setCourseInfo(courseInfo);

					trainerRepository.saveCourseOtherPDFsInfoAndGetCourseOtherPDFsInfo(courseOtherPDFsInfo);
				}
			}
		}
	}

	private void saveCourseOtherVideosInfo(MultipartFile[] courseOtherVideos, TrainerInfo trainerInfo,
			CourseInfo courseInfo) throws IOException, VimeoException {

		String trainerIdCourseIdtrainerEmail = String.valueOf(trainerInfo.getTrainerId())
				+ trainerInfo.getTrainerEmail() + String.valueOf(courseInfo.getCourseId());

		String courseVideoName = "", courseVideoStoredPath = "", courseVideoViewPath = "", courseVideoFileType = "";

		Vimeo vimeo = new Vimeo("f161b3f9226918f4fafdd2331b48daa4");

		for (MultipartFile courseVideo : courseOtherVideos) {
			courseVideoName = courseVideo.getOriginalFilename();
			if (!courseVideoName.isEmpty()) {
				String[] bits = courseVideoName.split("\\.");
				courseVideoFileType = bits[bits.length - 1];
				String otherImageName = trainerIdCourseIdtrainerEmail + bits[0] + bits[bits.length - 1];
				// System.out.println("coverImageName before encoded::" + otherImageName);
				byte[] encodedBytes = Base64.encode(otherImageName.getBytes());
				String encryptedName = new String(encodedBytes);
				// System.out.println("coverImageName after encoded::" + encryptedName);

				File trainerCourseMedia = new File(
						UserDefinedKeyWords.fileLocation + File.separator + "dHJhaW5lckNvdXJzZU1lZGlhSW5mb1ZpZXdQYXRo");
				trainerCourseMedia.mkdir();

				File trainerCourseInfo = new File(trainerCourseMedia + File.separator + encryptedName);
				trainerCourseInfo.mkdir();

				File courseMoreVideos = new File(trainerCourseInfo + File.separator + "Y291cnNlTW9yZVZpZGVvcw");
				courseMoreVideos.mkdir();

				Path path = Paths.get(courseMoreVideos + File.separator + courseVideoName);

				try {
					Files.write(path, courseVideo.getBytes());
				} catch (IOException e) {
					e.printStackTrace();
				}

				courseVideoStoredPath = path.toString();

				courseVideoViewPath = UserDefinedKeyWords.trainerCourseMediaInfoViewPath + encryptedName
						+ "/Y291cnNlTW9yZVZpZGVvcw/" + courseVideoName;

				// add a video
				boolean upgradeTo1080 = true;
				// String videoEndPoint = vimeo.addVideo(new
				// File("/home/cube9/Downloads/SampleVideo_1280x720_1mb.mp4"), upgradeTo1080);
				String videoEndPoint = vimeo.addVideo(new File(path.toString()), upgradeTo1080);
				// get video info
				VimeoResponse info = vimeo.getVideoInfo(videoEndPoint);
				// System.out.println("info::::::::::::::::::" + info);
				// System.out.println("link::::::::::::::::::" +
				// info.getJson().getString("link"));
				String courseVideoVimeoLink = info.getJson().getString("link");

				String[] arrOfStr = courseVideoVimeoLink.split("https://vimeo.com/");
				String courseVideoVimeoName = arrOfStr[1];
				// System.out.println("courseVideoVimeoName::"+courseVideoVimeoName);
				// edit video
				String name = courseVideo.getOriginalFilename();
				String desc = courseVideo.getOriginalFilename();
				String license = ""; // see Vimeo API Documentation
				// String privacyView = "disable"; //see Vimeo API Documentation
				// String privacyEmbed = "whitelist"; //see Vimeo API Documentation
				String privacyView = "anybody"; // see Vimeo API Documentation
				String privacyEmbed = "public"; // see Vimeo API Documentation

				boolean reviewLink = false;
				vimeo.updateVideoMetadata(videoEndPoint, name, desc, license, privacyView, privacyEmbed, reviewLink);

				// add video privacy domain
				// vimeo.addVideoPrivacyDomain(videoEndPoint, "clickntap.com");

				// delete video
				// vimeo.removeVideo(videoEndPoint);

				CourseVideoInfo courseVideoInfo = new CourseVideoInfo();
				courseVideoInfo.setCourseVideoName(courseVideoName);
				courseVideoInfo.setCourseVideoStoredPath(courseVideoStoredPath);
				courseVideoInfo.setCourseVideoViewPath(courseVideoViewPath);
				courseVideoInfo.setCourseVideoFileType(courseVideoFileType);
				courseVideoInfo.setCourseVideoVimeoLink(courseVideoVimeoLink);
				courseVideoInfo.setCourseVideoVimeoName(courseVideoVimeoName);

				courseVideoInfo = trainerRepository.saveCourseImagesInfoAndGetCourseImagesInfo(courseVideoInfo);

				if (courseVideoInfo != null) {
					CourseOtherVideosInfo courseOtherVideosInfo = new CourseOtherVideosInfo();
					courseOtherVideosInfo.setCourseVideoInfo(courseVideoInfo);
					courseOtherVideosInfo.setCourseInfo(courseInfo);

					trainerRepository.saveCourseOtherVideosInfoAndGetCourseOtherVideosInfo(courseOtherVideosInfo);

				}

			}
		}
	}

	@Transactional
	@Override
	public List<TrainerCourseInfo> getTrainerCourseListByTrainerId(Long trainerId) {
		return trainerRepository.getTrainerCourseListByTrainerId(trainerId);
	}

	@Transactional
	@Override
	public TrainerCourseInfo getTrainerCourseInfoByTrainerCourseId(Long trainerCourseId) {
		return trainerRepository.getTrainerCourseInfoByTrainerCourseId(trainerCourseId);
	}

	@Transactional
	@Override
	public boolean updateCourseInfo(@Valid TrainerCourseWrapper trainerCourseWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session)
			throws IOException, VimeoException {

		boolean status = false;

		// CourseInfo courseInfo =
		// trainerRepository.getCourseInfoByCourseId(trainerCourseWrapper.getCourseId());

		/*
		 * if (courseInfo != null) {
		 * 
		 * TrainerInfo trainerInfo =
		 * trainerRepository.getTrainerInfoByTrainerId(trainerCourseWrapper.getTrainerId
		 * ());
		 */

		/*
		 * CourseCoverImageInfo courseCoverImageInfo = saveCourseCoverImageInfo(
		 * trainerCourseWrapper.getCourseCoverImage(), trainerInfo, courseInfo);
		 * 
		 * if (courseCoverImageInfo != null) {
		 * 
		 * status = true;
		 * redirectAttributes.addFlashAttribute("alertSuccessMessageForAddingNewCourse",
		 * messageSource.getMessage("label.addNewCourseSuccess", null, null));
		 * 
		 * } else {
		 * 
		 * redirectAttributes.addFlashAttribute(
		 * "alertFailMessageForAddingNewCourseOnlyCoverImageFailure",
		 * messageSource.getMessage("label.addNewCourseFailsOnlyDueToCoverImage", null,
		 * null)); status = false; }
		 */

		/*
		 * CourseCoverVideoInfo courseCoverVideoInfo = saveCourseCoverVideoInfo(
		 * trainerCourseWrapper.getCourseCoverVideo(), trainerInfo, courseInfo); //
		 * System.out.println("courseCoverVideoInfo::"+courseCoverVideoInfo);
		 * 
		 * if (courseCoverVideoInfo != null) {
		 * trainerRepository.updateCourseInfoCoverVideoIdByCourseId(courseInfo.
		 * getCourseId(), courseCoverVideoInfo.getCourseCoverVideoId());
		 * redirectAttributes.addFlashAttribute("alertSuccessMessageForAddingNewCourse",
		 * messageSource.getMessage("label.addNewCourseSuccess", null, null)); status =
		 * true; } else { redirectAttributes.addFlashAttribute(
		 * "alertFailMessageForAddingNewCourseOnlyCoverImageFailure",
		 * messageSource.getMessage("label.addNewCourseFailsOnlyDueToCoverImage", null,
		 * null)); status = false; }
		 */

		// saveCourseOtherImagesInfo(trainerCourseWrapper.getCourseOtherImages(),
		// trainerInfo, courseInfo);
		// saveCourseOtherVideosInfo(trainerCourseWrapper.getCourseOtherVideos(),
		// trainerInfo, courseInfo);
		// saveCourseOtherPDFInfo(trainerCourseWrapper.getCourseOtherPdfs(),
		// trainerInfo, courseInfo);

		/*
		 * } else { redirectAttributes.addFlashAttribute("alertFailForAddingNewCourse",
		 * messageSource.getMessage("label.addNewCourseFail", null, null)); status =
		 * false; }
		 */

		boolean updateStatus = trainerRepository.updateCourseInfoByCourseId(trainerCourseWrapper);

		if (updateStatus) {
			redirectAttributes.addFlashAttribute("alertSuccessMessageForUpdatingCourse",
					messageSource.getMessage("label.updateCourseSuccess", null, null));
			status = true;
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessageForUpdatingCourse",
					messageSource.getMessage("label.updateCourseFail", null, null));
			status = false;
		}
		return status;
	}

	@Transactional
	@Override
	public List<CourseOtherImagesInfo> getCourseOtherImagesListByCourseId(Long courseId) {
		return trainerRepository.getCourseOtherImagesListByCourseId(courseId);
	}

	@Transactional
	@Override
	public List<CourseOtherVideosInfo> getCourseOtherVideosListByCourseId(Long courseId) {
		return trainerRepository.getCourseOtherVideosListByCourseId(courseId);
	}

	@Transactional
	@Override
	public List<CourseOtherPDFsInfo> getCourseOtherPDFsListByCourseId(Long courseId) {
		return trainerRepository.getCourseOtherPDFsListByCourseId(courseId);
	}

	@Transactional
	@Override
	public List<UserCourseReviewInfo> getTrainerCourseListWithReviewAndRating(Long trainerId) {
		List<TrainerCourseInfo> trainerCourseInfos = trainerRepository.getTrainerCourseListByTrainerId(trainerId);
		if (trainerCourseInfos != null) {
			List<Long> trainerCourseIds = new ArrayList<Long>();
			for (TrainerCourseInfo trainerCourseInfo : trainerCourseInfos) {
				trainerCourseIds.add(trainerCourseInfo.getTrainerCourseId());
			}
			trainerCourseIds = trainerCourseIds.stream().distinct().collect(Collectors.toList());

			if (!trainerCourseIds.isEmpty()) {
				return trainerRepository.getTrainerCourseListWithReviewAndRatingByTrainerCourseId(trainerCourseIds);
			} else {
				return null;
			}
		} else {
			return null;
		}

	}

	@Transactional
	@Override
	public List<UserCoursePaymentInfo> getTrainerCourseUserPaymentListByTrainerId(Long trainerId) {
		List<TrainerCourseInfo> trainerCourseInfos = trainerRepository.getTrainerCourseListByTrainerId(trainerId);
		if (trainerCourseInfos != null) {
			List<Long> trainerCourseIds = new ArrayList<Long>();
			for (TrainerCourseInfo trainerCourseInfo : trainerCourseInfos) {
				trainerCourseIds.add(trainerCourseInfo.getTrainerCourseId());
			}
			trainerCourseIds = trainerCourseIds.stream().distinct().collect(Collectors.toList());

			if (!trainerCourseIds.isEmpty()) {
				return trainerRepository.getTrainerCourseUserPaymentListByTrainerCourseId(trainerCourseIds);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	@Transactional
	@Override
	public Long getAllTrainrCourseCountByTrainerId(Long trainerId) {
		return trainerRepository.getAllTrainrCourseCountByTrainerId(trainerId);
	}

	@Transactional
	@Override
	public boolean deleteOtherImage(Long otherImageId, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session) {

		CourseOtherImagesInfo courseOtherImagesInfo = trainerRepository
				.getCourseOtherImagesInfoByOtherImageId(otherImageId);

		if (courseOtherImagesInfo != null) {
			Path path = Paths.get(courseOtherImagesInfo.getCourseImagesInfo().getCourseImagesStoredPath());
			try {
				Files.delete(path);
			} catch (IOException e) {
				e.printStackTrace();
			}
			boolean deleteOtherImageStatus = trainerRepository.deleteOtherImage(otherImageId);
			if (deleteOtherImageStatus) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public boolean deleteOtherVideo(Long otherVideoId, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session) {
		CourseOtherVideosInfo courseOtherVideosInfo = trainerRepository
				.getCourseOtherVideosInfoByOtherVideoId(otherVideoId);
		if (courseOtherVideosInfo != null) {
			Path path = Paths.get(courseOtherVideosInfo.getCourseVideoInfo().getCourseVideoStoredPath());
			try {
				Files.delete(path);
			} catch (IOException e) {
				e.printStackTrace();
			}
			boolean deleteOtherVideoStatus = trainerRepository.deleteOtherVideo(otherVideoId);
			if (deleteOtherVideoStatus) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public boolean deleteOtherPDF(Long otherPDFId, RedirectAttributes redirectAttributes, MessageSource messageSource,
			HttpSession session) {
		CourseOtherPDFsInfo courseOtherPDFsInfo = trainerRepository.getCourseOtherPDFsInfoByOtherPDFId(otherPDFId);
		if (courseOtherPDFsInfo != null) {
			Path path = Paths.get(courseOtherPDFsInfo.getCoursePDFInfo().getCoursePDFStoredPath());
			try {
				Files.delete(path);
			} catch (IOException e) {
				e.printStackTrace();
			}
			boolean deleteOtherPDFStatus = trainerRepository.deleteOtherPDF(otherPDFId);
			if (deleteOtherPDFStatus) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public boolean deleteCoverImage(Long coverImageId, Long courseId) {
		CourseInfo courseInfo = trainerRepository.getCourseInfoByCourseId(courseId);
		if (courseInfo != null) {
			if (courseInfo.getCourseCoverImageInfo() != null) {
				Path path = Paths.get(courseInfo.getCourseCoverImageInfo().getCourseCoverImageStoredPath());
				try {
					Files.delete(path);
				} catch (IOException e) {
					e.printStackTrace();
				}
				boolean updateCourseInfo = trainerRepository.updateCourseInfoCoverImageToNull(courseInfo.getCourseId());

				if (updateCourseInfo) {
					boolean deleteCoverImageStatus = trainerRepository
							.deleteCoverImage(courseInfo.getCourseCoverImageInfo().getCourseCoverImageId());
					if (deleteCoverImageStatus) {
						return true;
					} else {
						return false;
					}
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public boolean deleteCoverVideo(Long coverVideoId, Long courseId) {
		CourseInfo courseInfo = trainerRepository.getCourseInfoByCourseId(courseId);
		if (courseInfo != null) {
			if (courseInfo.getCourseCoverVideoInfo() != null) {
				Path path = Paths.get(courseInfo.getCourseCoverVideoInfo().getCourseCoverVideoStoredPath());
				try {
					Files.delete(path);
				} catch (IOException e) {
					e.printStackTrace();
				}
				boolean updateCourseInfo = trainerRepository.updateCourseInfoCoverVideoToNull(courseInfo.getCourseId());

				if (updateCourseInfo) {
					boolean deleteCoverVideoStatus = trainerRepository
							.deleteCoverVideo(courseInfo.getCourseCoverVideoInfo().getCourseCoverVideoId());
					if (deleteCoverVideoStatus) {
						return true;
					} else {
						return false;
					}
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public boolean addNewTrainerCourseFAQ(@Valid TrainerCourseFAQWrapper trainerCourseFAQWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session) {

		TrainerCourseInfo trainerCourseInfo = new TrainerCourseInfo();
		trainerCourseInfo.setTrainerCourseId(trainerCourseFAQWrapper.getTrainerCourseId());

		TrainerCourseFAQInfo trainerCourseFAQInfo = new TrainerCourseFAQInfo();
		trainerCourseFAQInfo.setTrainerCourseFAQQuestion(trainerCourseFAQWrapper.getTrainerCourseFAQQuestion());
		trainerCourseFAQInfo.setTrainerCourseFAQAnswer(trainerCourseFAQWrapper.getTrainerCourseFAQAnswer());
		trainerCourseFAQInfo.setTrainerCourseInfo(trainerCourseInfo);

		trainerCourseFAQInfo = trainerRepository.saveAndGetTrainerCourseFAQInfo(trainerCourseFAQInfo);
		if (trainerCourseFAQInfo != null) {
			redirectAttributes.addFlashAttribute("alertSuccessMessageForAddingCourseFAQ",
					messageSource.getMessage("label.addingCourseFAQSuccess", null, null));
			return true;
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessageForAddingCourseFAQ",
					messageSource.getMessage("label.addingCourseFAQFail", null, null));
			return false;
		}
	}

	@Transactional
	@Override
	public List<TrainerCourseFAQInfo> getTrainerCourseFAQListByTrainerCourseId(Long trainerCourseId) {
		return trainerRepository.getTrainerCourseFAQListByTrainerCourseId(trainerCourseId);
	}

	@Transactional
	@Override
	public boolean addNewTrainerCourseChapter(@Valid TrainerCourseChapterWrapper trainerCourseChapterWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session) {

		TrainerCourseInfo trainerCourseInfo = new TrainerCourseInfo();
		trainerCourseInfo.setTrainerCourseId(trainerCourseChapterWrapper.getTrainerCourseId());

		TrainerCourseChapterInfo trainerCourseChapterInfo = new TrainerCourseChapterInfo();
		trainerCourseChapterInfo.setTrainerCourseChapterName(trainerCourseChapterWrapper.getTrainerCourseChapterName());
		trainerCourseChapterInfo.setTrainerCourseInfo(trainerCourseInfo);

		trainerCourseChapterInfo = trainerRepository.saveAndGetTrainerCourseChapterInfo(trainerCourseChapterInfo);
		if (trainerCourseChapterInfo != null) {
			redirectAttributes.addFlashAttribute("alertSuccessMessageForAddingCourseChapter",
					messageSource.getMessage("label.addingCourseChapterSuccess", null, null));
			return true;
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessageForAddingCourseChapter",
					messageSource.getMessage("label.addingCourseChapterFail", null, null));
			return false;
		}
	}

	@Transactional
	@Override
	public List<TrainerCourseChapterInfo> getTrainerCourseChapterListByTrainerCourseId(Long trainerCourseId) {
		return trainerRepository.getTrainerCourseChapterListByTrainerCourseId(trainerCourseId);
	}

	@Transactional
	@Override
	public TrainerCourseChapterInfo getTrainerCourseChapterInfoByTrainerCourseChapterId(Long trainerCourseChapterId) {
		return trainerRepository.getTrainerCourseChapterInfoByTrainerCourseChapterId(trainerCourseChapterId);
	}

	@Transactional
	@Override
	public List<TrainerCourseChapterLessonInfo> getTrainerCourseChapterLessonListByTrainerCourseChapterId(
			Long trainerCourseChapterId) {
		return trainerRepository.getTrainerCourseChapterLessonListByTrainerCourseChapterId(trainerCourseChapterId);
	}

	@Transactional
	@Override
	public boolean addNewTrainerCourseChapterLesson(
			@Valid TrainerCourseChapterLessonWrapper trainerCourseChapterLessonWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session)
			throws IOException, VimeoException {

		TrainerCourseChapterInfo trainerCourseChapterInfo = new TrainerCourseChapterInfo();
		trainerCourseChapterInfo
				.setTrainerCourseChapterId(trainerCourseChapterLessonWrapper.getTrainerCourseChapterId());

		TrainerCourseChapterLessonInfo trainerCourseChapterLessonInfo = new TrainerCourseChapterLessonInfo();
		trainerCourseChapterLessonInfo.setTrainerCourseChapterInfo(trainerCourseChapterInfo);
		trainerCourseChapterLessonInfo.setTrainerCourseChapterLessonName(
				trainerCourseChapterLessonWrapper.getTrainerCourseChapterLessonName());

		trainerCourseChapterLessonInfo = trainerRepository
				.saveAndGetTrainerCourseChapterLessonInfo(trainerCourseChapterLessonInfo);

		if (trainerCourseChapterLessonInfo != null) {
			redirectAttributes.addFlashAttribute("alertSuccessMessageForAddingCourseChapterLesson",
					messageSource.getMessage("label.addingCourseChapterLessonSuccess", null, null));
			return true;
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessageForAddingCourseChapterLesson",
					messageSource.getMessage("label.addingCourseChapterLessonFail", null, null));
			return false;
		}

	}

	@Transactional
	@Override
	public TrainerCourseChapterLessonInfo getTrainerCourseChapterLessonInfoByTrainerCourseChapterLessonId(
			Long trainerCourseChapterLessonId) {
		return trainerRepository
				.getTrainerCourseChapterLessonInfoByTrainerCourseChapterLessonId(trainerCourseChapterLessonId);
	}

	@Transactional
	@Override
	public boolean updateTrainerCourseChapterLesson(
			@Valid TrainerCourseChapterLessonWrapper trainerCourseChapterLessonWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session)
			throws IOException, VimeoException {

		TrainerCourseChapterLessonInfo trainerCourseChapterLessonInfo = trainerRepository
				.getTrainerCourseChapterLessonInfoByTrainerCourseChapterLessonId(
						trainerCourseChapterLessonWrapper.getTrainerCourseChapterLessonId());

		if (trainerCourseChapterLessonInfo != null) {
			trainerCourseChapterLessonInfo.setTrainerCourseChapterLessonName(
					trainerCourseChapterLessonWrapper.getTrainerCourseChapterLessonName());
			trainerRepository.updateTrainerCourseChapterLessonInfo(trainerCourseChapterLessonInfo);
			return true;

		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public boolean deleteTrainersCourseChapterLesson(Long trainerCourseChapterLessonId) {
		return trainerRepository
				.deleteTrainersCourseChapterLessonByTrainerCourseChapterLessonId(trainerCourseChapterLessonId);
	}

	@Transactional
	@Override
	public boolean updateTrainerCourseChapter(@Valid TrainerCourseChapterWrapper trainerCourseChapterWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session) {
		boolean updateTrainerCourseChapterStatus = trainerRepository
				.updateTrainerCourseChapterInfoByTrainerCourseChapterId(
						trainerCourseChapterWrapper.getTrainerCourseChapterId(),
						trainerCourseChapterWrapper.getTrainerCourseChapterName());
		if (updateTrainerCourseChapterStatus) {
			redirectAttributes.addFlashAttribute("alertSuccessMessageForUpdatingCourseChapter",
					messageSource.getMessage("label.updatingCourseChapterSuccess", null, null));
			return updateTrainerCourseChapterStatus;
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessageForUpdatingCourseChapter",
					messageSource.getMessage("label.updatingCourseChapterFail", null, null));
			return updateTrainerCourseChapterStatus;
		}
	}

	@Transactional
	@Override
	public boolean deleteTrainersCourseChapter(Long trainerCourseChapterId) {
		List<TrainerCourseChapterLessonInfo> trainerCourseChapterLessonInfos = trainerRepository
				.getTrainerCourseChapterLessonListByTrainerCourseChapterId(trainerCourseChapterId);
		if (trainerCourseChapterLessonInfos != null) {
			for (TrainerCourseChapterLessonInfo trainerCourseChapterLessonInfo : trainerCourseChapterLessonInfos) {
				trainerRepository.deleteTrainersCourseChapterLessonByTrainerCourseChapterLessonId(
						trainerCourseChapterLessonInfo.getTrainerCourseChapterLessonId());
			}
		}
		return trainerRepository.deleteTrainersCourseChapterByTrainerCourseChapterId(trainerCourseChapterId);
	}

	@Transactional
	@Override
	public TrainerCourseFAQInfo getTrainerCourseFAQByTrainerCourseFAQId(Long trainerCourseFAQId) {
		return trainerRepository.getTrainerCourseFAQByTrainerCourseFAQId(trainerCourseFAQId);
	}

	@Transactional
	@Override
	public boolean updateTrainerCourseFAQ(@Valid TrainerCourseFAQWrapper trainerCourseFAQWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session) {
		boolean updateTrainerCourseFAQStatus = trainerRepository
				.updateTrainerCourseFAQByTrainerCourseFAQId(trainerCourseFAQWrapper);
		if (updateTrainerCourseFAQStatus) {
			redirectAttributes.addFlashAttribute("alertSuccessMessageForUpdatingCourseFAQ",
					messageSource.getMessage("label.updatingCourseFAQSuccess", null, null));

		} else {
			redirectAttributes.addFlashAttribute("alertFailMessageForUpdatingCourseFAQ",
					messageSource.getMessage("label.updatingCourseFAQFail", null, null));
		}
		return updateTrainerCourseFAQStatus;
	}

	@Transactional
	@Override
	public boolean deleteTrainersCourseFAQ(Long trainerCourseFAQId) {
		return trainerRepository.deleteTrainersCourseFAQ(trainerCourseFAQId);
	}

	@Transactional
	@Override
	public boolean uploadCourseCoverImage(MultipartFile courseCoverImage, Long trainerId, Long courseId) {
		TrainerInfo trainerInfo = trainerRepository.getTrainerInfoByTrainerId(trainerId);
		if (trainerInfo != null) {
			CourseInfo courseInfo = trainerRepository.getCourseInfoByCourseId(courseId);
			if (courseInfo != null) {
				CourseCoverImageInfo courseCoverImageInfo = saveCourseCoverImageInfo(courseCoverImage, trainerInfo,
						courseInfo);
				if (courseCoverImageInfo != null) {
					trainerRepository.updateCourseInfoCoverImageIdByCourseId(courseInfo.getCourseId(),
							courseCoverImageInfo.getCourseCoverImageId());
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public boolean uploadCourseOtherImages(MultipartFile[] courseOtherImages, Long trainerId, Long courseId) {
		TrainerInfo trainerInfo = trainerRepository.getTrainerInfoByTrainerId(trainerId);
		if (trainerInfo != null) {
			CourseInfo courseInfo = trainerRepository.getCourseInfoByCourseId(courseId);
			if (courseInfo != null) {
				// System.out.println("courseInfo::"+courseInfo);
				saveCourseOtherImagesInfo(courseOtherImages, trainerInfo, courseInfo);
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public boolean uploadCourseCoverVideo(MultipartFile courseCoverVideo, Long trainerId, Long courseId)
			throws IOException, VimeoException {
		TrainerInfo trainerInfo = trainerRepository.getTrainerInfoByTrainerId(trainerId);
		if (trainerInfo != null) {
			CourseInfo courseInfo = trainerRepository.getCourseInfoByCourseId(courseId);
			if (courseInfo != null) {
				CourseCoverVideoInfo courseCoverVideoInfo = saveCourseCoverVideoInfo(courseCoverVideo, trainerInfo,
						courseInfo);
				if (courseCoverVideoInfo != null) {
					trainerRepository.updateCourseInfoCoverVideoIdByCourseId(courseInfo.getCourseId(),
							courseCoverVideoInfo.getCourseCoverVideoId());
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public boolean uploadCourseChapterLessonPreviewVideo(MultipartFile trainerCourseChapterLessonPreviewVideo,
			Long trainerCourseChapterLessonId) throws IOException, VimeoException {

		TrainerCourseChapterLessonInfo trainerCourseChapterLessonInfo = trainerRepository
				.getTrainerCourseChapterLessonInfoByTrainerCourseChapterLessonId(trainerCourseChapterLessonId);

		if (trainerCourseChapterLessonInfo != null) {

			String trainerChapterId = String.valueOf(trainerCourseChapterLessonId);

			Vimeo vimeo = new Vimeo("f161b3f9226918f4fafdd2331b48daa4");

			if (!trainerCourseChapterLessonPreviewVideo.isEmpty()) {

				String coursePreviewVideoName = trainerCourseChapterLessonPreviewVideo.getOriginalFilename();

				if (!coursePreviewVideoName.isEmpty()) {
					String[] bits = coursePreviewVideoName.split("\\.");
					String coursePreviewVideoFileType = bits[bits.length - 1];
					String previewVideoName = trainerChapterId + bits[0] + bits[bits.length - 1];
					// System.out.println("coverImageName before encoded::"+coverImageName);
					byte[] encodedBytes = Base64.encode(previewVideoName.getBytes());
					String encryptedName = new String(encodedBytes);
					// System.out.println("coverImageName after encoded::"+encryptedName);

					File trainerCourseMedia = new File(UserDefinedKeyWords.fileLocation + File.separator
							+ "dHJhaW5lckNvdXJzZU1lZGlhSW5mb1ZpZXdQYXRo");
					trainerCourseMedia.mkdir();

					File trainerCourseInfo = new File(trainerCourseMedia + File.separator + encryptedName);
					trainerCourseInfo.mkdir();

					File previewVideo = new File(trainerCourseInfo + File.separator + "cHJldmlld1ZpZGVv");
					previewVideo.mkdir();

					Path path = Paths.get(previewVideo + File.separator + coursePreviewVideoName);

					try {
						Files.write(path, trainerCourseChapterLessonPreviewVideo.getBytes());
					} catch (IOException e) {
						e.printStackTrace();
					}

					String coursePreviewVideoStoredPath = path.toString();

					String coursePreviewVideoViewPath = UserDefinedKeyWords.trainerCourseMediaInfoViewPath
							+ encryptedName + "/cHJldmlld1ZpZGVv/" + coursePreviewVideoName;

					// add a video
					boolean upgradeTo1080 = true;
					// String videoEndPoint = vimeo.addVideo(new
					// File("/home/cube9/Downloads/SampleVideo_1280x720_1mb.mp4"), upgradeTo1080);
					String videoEndPoint = vimeo.addVideo(new File(path.toString()), upgradeTo1080);
					// get video info
					VimeoResponse info = vimeo.getVideoInfo(videoEndPoint);
					// System.out.println("info::::::::::::::::::" + info);
					// System.out.println("link::::::::::::::::::" +
					// info.getJson().getString("link"));
					String trainerCourseChapterLessonpreviewVideoVimeoLink = info.getJson().getString("link");

					String[] arrOfStr = trainerCourseChapterLessonpreviewVideoVimeoLink.split("https://vimeo.com/");
					String trainerCourseChapterLessonPreviewVideoVimeoName = arrOfStr[1];
					// System.out.println("courseVideoVimeoName::"+courseVideoVimeoName);
					// edit video
					String name = trainerCourseChapterLessonPreviewVideo.getOriginalFilename();
					String desc = trainerCourseChapterLessonPreviewVideo.getOriginalFilename();
					String license = ""; // see Vimeo API Documentation
					// String privacyView = "disable"; //see Vimeo API Documentation
					// String privacyEmbed = "whitelist"; //see Vimeo API Documentation
					String privacyView = "anybody"; // see Vimeo API Documentation
					String privacyEmbed = "public"; // see Vimeo API Documentation

					boolean reviewLink = false;
					vimeo.updateVideoMetadata(videoEndPoint, name, desc, license, privacyView, privacyEmbed,
							reviewLink);

					// add video privacy domain
					// vimeo.addVideoPrivacyDomain(videoEndPoint, "clickntap.com");

					// delete video
					// vimeo.removeVideo(videoEndPoint);

					trainerCourseChapterLessonInfo
							.setTrainerCourseChapterLessonPreviewVideoStoredPath(coursePreviewVideoStoredPath);
					trainerCourseChapterLessonInfo
							.setTrainerCourseChapterLessonPreviewVideoViewPath(coursePreviewVideoViewPath);
					trainerCourseChapterLessonInfo
							.setTrainerCourseChapterLessonPreviewVideoFileType(coursePreviewVideoFileType);
					trainerCourseChapterLessonInfo.setTrainerCourseChapterLessonPreviewVideoVimeoName(
							trainerCourseChapterLessonPreviewVideoVimeoName);
					trainerCourseChapterLessonInfo.setTrainerCourseChapterLessonpreviewVideoVimeoLink(
							trainerCourseChapterLessonpreviewVideoVimeoLink);
				}
			}
			trainerRepository.updateTrainerCourseChapterLessonInfoWithPreviewVideo(trainerCourseChapterLessonInfo);
			return true;
		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public boolean uploadCourseChapterLessonMainVideo(MultipartFile trainerCourseChapterLessonMainVideo,
			Long trainerCourseChapterLessonId) throws IOException, VimeoException {

		TrainerCourseChapterLessonInfo trainerCourseChapterLessonInfo = trainerRepository
				.getTrainerCourseChapterLessonInfoByTrainerCourseChapterLessonId(trainerCourseChapterLessonId);

		if (trainerCourseChapterLessonInfo != null) {

			String trainerChapterId = String.valueOf(trainerCourseChapterLessonId);

			Vimeo vimeo = new Vimeo("f161b3f9226918f4fafdd2331b48daa4");

			if (!trainerCourseChapterLessonMainVideo.isEmpty()) {

				String courseMainVideoName = trainerCourseChapterLessonMainVideo.getOriginalFilename();

				if (!courseMainVideoName.isEmpty()) {
					String[] bits = courseMainVideoName.split("\\.");
					String courseMainVideoFileType = bits[bits.length - 1];
					String mainVideoName = trainerChapterId + bits[0] + bits[bits.length - 1];
					// System.out.println("coverImageName before encoded::"+coverImageName);
					byte[] encodedBytes = Base64.encode(mainVideoName.getBytes());
					String encryptedName = new String(encodedBytes);
					// System.out.println("coverImageName after encoded::"+encryptedName);

					File trainerCourseMedia = new File(UserDefinedKeyWords.fileLocation + File.separator
							+ "dHJhaW5lckNvdXJzZU1lZGlhSW5mb1ZpZXdQYXRo");
					trainerCourseMedia.mkdir();

					File trainerCourseInfo = new File(trainerCourseMedia + File.separator + encryptedName);
					trainerCourseInfo.mkdir();

					File mainVideo = new File(trainerCourseInfo + File.separator + "bWFpblZpZGVv");
					mainVideo.mkdir();

					Path path = Paths.get(mainVideo + File.separator + courseMainVideoName);

					try {
						Files.write(path, trainerCourseChapterLessonMainVideo.getBytes());
					} catch (IOException e) {
						e.printStackTrace();
					}

					String courseMainVideoStoredPath = path.toString();

					String courseMainVideoViewPath = UserDefinedKeyWords.trainerCourseMediaInfoViewPath + encryptedName
							+ "/bWFpblZpZGVv/" + courseMainVideoName;

					// add a video
					boolean upgradeTo1080 = true;
					// String videoEndPoint = vimeo.addVideo(new
					// File("/home/cube9/Downloads/SampleVideo_1280x720_1mb.mp4"), upgradeTo1080);
					String videoEndPoint = vimeo.addVideo(new File(path.toString()), upgradeTo1080);
					// get video info
					VimeoResponse info = vimeo.getVideoInfo(videoEndPoint);
					// System.out.println("info::::::::::::::::::" + info);
					// System.out.println("link::::::::::::::::::" +
					// info.getJson().getString("link"));
					String trainerCourseChapterLessonMainVideoVimeoLink = info.getJson().getString("link");

					String[] arrOfStr = trainerCourseChapterLessonMainVideoVimeoLink.split("https://vimeo.com/");
					String trainerCourseChapterLessonMainVideoVimeoName = arrOfStr[1];
					// System.out.println("courseVideoVimeoName::"+courseVideoVimeoName);
					// edit video
					String name = trainerCourseChapterLessonMainVideo.getOriginalFilename();
					String desc = trainerCourseChapterLessonMainVideo.getOriginalFilename();
					String license = ""; // see Vimeo API Documentation
					// String privacyView = "disable"; //see Vimeo API Documentation
					// String privacyEmbed = "whitelist"; //see Vimeo API Documentation
					String privacyView = "anybody"; // see Vimeo API Documentation
					String privacyEmbed = "public"; // see Vimeo API Documentation

					boolean reviewLink = false;
					vimeo.updateVideoMetadata(videoEndPoint, name, desc, license, privacyView, privacyEmbed,
							reviewLink);

					// add video privacy domain
					// vimeo.addVideoPrivacyDomain(videoEndPoint, "clickntap.com");

					// delete video
					// vimeo.removeVideo(videoEndPoint);

					trainerCourseChapterLessonInfo
							.setTrainerCourseChapterLessonMainVideoStoredPath(courseMainVideoStoredPath);
					trainerCourseChapterLessonInfo
							.setTrainerCourseChapterLessonMainVideoViewPath(courseMainVideoViewPath);
					trainerCourseChapterLessonInfo
							.setTrainerCourseChapterLessonMainVideoFileType(courseMainVideoFileType);
					trainerCourseChapterLessonInfo.setTrainerCourseChapterLessonMainVideoVimeoName(
							trainerCourseChapterLessonMainVideoVimeoName);
					trainerCourseChapterLessonInfo.setTrainerCourseChapterLessonMainVideoVimeoLink(
							trainerCourseChapterLessonMainVideoVimeoLink);
				}
			}
			trainerRepository.updateTrainerCourseChapterLessonInfoWithMainVideo(trainerCourseChapterLessonInfo);
			return true;
		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public boolean uploadCourseChapterLessonMainPdf(MultipartFile trainerCourseChapterLessonMainPdf,
			Long trainerCourseChapterLessonId) {

		TrainerCourseChapterLessonInfo trainerCourseChapterLessonInfo = trainerRepository
				.getTrainerCourseChapterLessonInfoByTrainerCourseChapterLessonId(trainerCourseChapterLessonId);

		if (trainerCourseChapterLessonInfo != null) {

			String trainerChapterId = String.valueOf(trainerCourseChapterLessonId);

			if (!trainerCourseChapterLessonMainPdf.isEmpty()) {

				String courseMainPdfName = trainerCourseChapterLessonMainPdf.getOriginalFilename();

				if (!courseMainPdfName.isEmpty()) {
					String[] bits = courseMainPdfName.split("\\.");
					String courseMainPdfFileType = bits[bits.length - 1];
					String mainPdfName = trainerChapterId + bits[0] + bits[bits.length - 1];
					// System.out.println("coverImageName before encoded::"+coverImageName);
					byte[] encodedBytes = Base64.encode(mainPdfName.getBytes());
					String encryptedName = new String(encodedBytes);
					// System.out.println("coverImageName after encoded::"+encryptedName);

					File trainerCourseMedia = new File(UserDefinedKeyWords.fileLocation + File.separator
							+ "dHJhaW5lckNvdXJzZU1lZGlhSW5mb1ZpZXdQYXRo");
					trainerCourseMedia.mkdir();

					File trainerCourseInfo = new File(trainerCourseMedia + File.separator + encryptedName);
					trainerCourseInfo.mkdir();

					File mainPdf = new File(trainerCourseInfo + File.separator + "bWFpblBkZg");
					mainPdf.mkdir();

					Path path = Paths.get(mainPdf + File.separator + courseMainPdfName);

					try {
						Files.write(path, trainerCourseChapterLessonMainPdf.getBytes());
					} catch (IOException e) {
						e.printStackTrace();
					}

					String courseMainPdfStoredPath = path.toString();

					String courseMainPdfViewPath = UserDefinedKeyWords.trainerCourseMediaInfoViewPath + encryptedName
							+ "/bWFpblBkZg/" + courseMainPdfName;

					trainerCourseChapterLessonInfo.setTrainerCourseChapterLessonMainPdfName(bits[0]);

					trainerCourseChapterLessonInfo
							.setTrainerCourseChapterLessonMainPdfStoredPath(courseMainPdfStoredPath);
					trainerCourseChapterLessonInfo.setTrainerCourseChapterLessonMainPdfViewPath(courseMainPdfViewPath);
					trainerCourseChapterLessonInfo.setTrainerCourseChapterLessonMainPdfFileType(courseMainPdfFileType);
				}
			}
			trainerRepository.updateTrainerCourseChapterLessonInfoWithMainPdf(trainerCourseChapterLessonInfo);
			return true;
		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public boolean deletePreviewVideo(Long trainerCourseChapterLessonId) {

		TrainerCourseChapterLessonInfo trainerCourseChapterLessonInfo = trainerRepository
				.getTrainerCourseChapterLessonInfoByTrainerCourseChapterLessonId(trainerCourseChapterLessonId);

		if (trainerCourseChapterLessonInfo != null) {

			trainerCourseChapterLessonInfo.setTrainerCourseChapterLessonPreviewVideoStoredPath(null);
			trainerCourseChapterLessonInfo.setTrainerCourseChapterLessonPreviewVideoViewPath(null);
			trainerCourseChapterLessonInfo.setTrainerCourseChapterLessonPreviewVideoFileType(null);
			trainerCourseChapterLessonInfo.setTrainerCourseChapterLessonPreviewVideoVimeoName(null);
			trainerCourseChapterLessonInfo.setTrainerCourseChapterLessonpreviewVideoVimeoLink(null);
			trainerRepository.updateTrainerCourseChapterLessonInfoWithPreviewVideo(trainerCourseChapterLessonInfo);
			return true;
		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public boolean deleteActualVideo(Long trainerCourseChapterLessonId) {

		TrainerCourseChapterLessonInfo trainerCourseChapterLessonInfo = trainerRepository
				.getTrainerCourseChapterLessonInfoByTrainerCourseChapterLessonId(trainerCourseChapterLessonId);

		if (trainerCourseChapterLessonInfo != null) {

			trainerCourseChapterLessonInfo.setTrainerCourseChapterLessonMainVideoStoredPath(null);
			trainerCourseChapterLessonInfo.setTrainerCourseChapterLessonMainVideoViewPath(null);
			trainerCourseChapterLessonInfo.setTrainerCourseChapterLessonMainVideoFileType(null);
			trainerCourseChapterLessonInfo.setTrainerCourseChapterLessonMainVideoVimeoName(null);
			trainerCourseChapterLessonInfo.setTrainerCourseChapterLessonMainVideoVimeoLink(null);
			trainerRepository.updateTrainerCourseChapterLessonInfoWithMainVideo(trainerCourseChapterLessonInfo);
			return true;
		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public boolean deleteMainPdf(Long trainerCourseChapterLessonId) {

		TrainerCourseChapterLessonInfo trainerCourseChapterLessonInfo = trainerRepository
				.getTrainerCourseChapterLessonInfoByTrainerCourseChapterLessonId(trainerCourseChapterLessonId);

		if (trainerCourseChapterLessonInfo != null) {
			trainerCourseChapterLessonInfo.setTrainerCourseChapterLessonMainPdfName(null);

			trainerCourseChapterLessonInfo.setTrainerCourseChapterLessonMainPdfStoredPath(null);
			trainerCourseChapterLessonInfo.setTrainerCourseChapterLessonMainPdfViewPath(null);
			trainerCourseChapterLessonInfo.setTrainerCourseChapterLessonMainPdfFileType(null);
			trainerRepository.updateTrainerCourseChapterLessonInfoWithMainPdf(trainerCourseChapterLessonInfo);
			return true;
		} else {
			return false;
		}
	}

}