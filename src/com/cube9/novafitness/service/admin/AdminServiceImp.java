package com.cube9.novafitness.service.admin;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cube9.novafitness.config.EmailSendingConfig;
import com.cube9.novafitness.config.UserDefinedKeyWords;
import com.cube9.novafitness.config.UserDefinedMethods;
import com.cube9.novafitness.model.admin.AdminInfo;
import com.cube9.novafitness.model.admin.CategoryInfo;
import com.cube9.novafitness.model.admin.FAQInfo;
import com.cube9.novafitness.model.admin.SubAdminAccessRoleInfo;
import com.cube9.novafitness.model.admin.SubCategoryInfo;
import com.cube9.novafitness.model.course.CourseContributionInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseInfo;
import com.cube9.novafitness.model.trainer.TrainerInfo;
import com.cube9.novafitness.model.user.NewsLetterInfo;
import com.cube9.novafitness.model.user.UserCoursePaymentInfo;
import com.cube9.novafitness.model.user.UserCourseReviewInfo;
import com.cube9.novafitness.model.user.UserInfo;
import com.cube9.novafitness.repository.admin.AdminRepository;
import com.cube9.novafitness.repository.trainer.TrainerRepository;
import com.cube9.novafitness.repository.user.UserRepository;
import com.cube9.novafitness.wrapper.admin.AdminCategoryWrapper;
import com.cube9.novafitness.wrapper.admin.AdminChangePasswordWrapper;
import com.cube9.novafitness.wrapper.admin.AdminFAQsWrapper;
import com.cube9.novafitness.wrapper.admin.AdminLoginWrapper;
import com.cube9.novafitness.wrapper.admin.AdminPaymentSettlementWrapper;
import com.cube9.novafitness.wrapper.admin.AdminProfileWrapper;
import com.cube9.novafitness.wrapper.admin.AdminResetPasswordWrapper;
import com.cube9.novafitness.wrapper.admin.AdminSubCategoryWrapper;
import com.cube9.novafitness.wrapper.admin.SubAdminWrapper;
import com.sun.jersey.core.util.Base64;

/**
 * @author Vaibhav Deshmane
 *
 */
@Service
public class AdminServiceImp implements AdminService {

	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private TrainerRepository trainerRepository;

	@Transactional
	@Override
	public boolean adminLogin(@Valid AdminLoginWrapper adminLoginWrapper, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session) {
		AdminInfo adminInfo = adminRepository.getAdminInfoByAdminEmail(adminLoginWrapper.getAdminEmail());
		if (adminInfo != null) {
			if (adminInfo.isSubAdminDeleteStatus()) {
				redirectAttributes.addFlashAttribute("alertFailMessageemailNotPresent",
						messageSource.getMessage("label.emailNotPresent", null, null));
				return false;
			} else {
				if (adminInfo.isAdminStatus()) {
					if (adminInfo.getAdminPassword().equals(adminLoginWrapper.getAdminPassword())) {
						/*
						 * if (adminInfo.getAdminRole().getRoleId() == 2) { SubAdminAccessRoleInfo
						 * subAdminAccessRoleInfo = adminRepository
						 * .getSubAdminAccessRoleInfoBySubAdminId(adminInfo.getAdminId());
						 * session.setAttribute("subAdminAccessInfo", subAdminAccessRoleInfo); }
						 */
						adminRepository.updateLastAdminLoginByAdminId(adminInfo.getAdminId(), LocalDateTime.now());
						session.setAttribute("adminSession", adminInfo);
						return true;
					} else {
						redirectAttributes.addFlashAttribute("alertFailMessageinvalidCredentials",
								messageSource.getMessage("label.invalidCredentials", null, null));
						return false;
					}
				} else {
					redirectAttributes.addFlashAttribute("alertFailMessageuserAccountVerificationFailed",
							messageSource.getMessage("label.userAccountVerificationFailed", null, null));
					return false;
				}
			}
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessageemailNotPresent",
					messageSource.getMessage("label.emailNotPresent", null, null));
			return false;
		}
	}

	@Transactional
	@Override
	public boolean adminForgetPassword(@Valid AdminLoginWrapper adminLoginWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource) {
		AdminInfo adminInfo = adminRepository.getAdminInfoByAdminEmail(adminLoginWrapper.getAdminEmail());

		if (adminInfo != null) {
			if (!adminInfo.isSubAdminDeleteStatus()) {
				if (adminInfo.isAdminStatus()) {
					EmailSendingConfig emailSendingConfig = new EmailSendingConfig();
					byte[] encodedBytes = Base64.encode(adminLoginWrapper.getAdminEmail().getBytes());
					String encryptedEmail = new String(encodedBytes);
					String sub = "NovoFitness Admin Reset Password";
					//String msg = "<table width='658px' align='center' style='border-collapse:collapse' border='0' cellspacing='0' cellpadding='0'>"
							//+ "<tbody>"
							//+ "<tr> <td width='100%' style='border-top:none;border-bottom:none;border-left:none;border-right:none;text-align:left;padding:15px 15px;font-size:18px;font-family:arial'>"
							//+ " <span> Hello " + adminInfo.getAdminFName() + ",</span> </td> </tr>"
							//+ "<tr> <td width='100%' style='border-top:none;border-bottom:none;border-left:none;border-right:none;text-align:left;padding:5px 15px;font-size:15px;font-family:arial'> "
							//+ "	<span>To reset your Password, please click on below button.</span> </td> </tr>"
							//+ "<tr><td><br> </td></tr>" + "<tr> <td width='100%'> <a href=" + url
							//+ " style='background:#37a000;text-decoration:none;padding:8px 8px;border-radius:4px;color:#fff;font-family:arial;margin-left: 150px;'>Reset Password</a> </td> </tr>"
							//+ "</tbody>" + "</table>";
					String emailClickURL = UserDefinedKeyWords.URL + "adminResetPassword/" + encryptedEmail;
					String emailHeader = "Hello " + adminInfo.getAdminFName() + " " + adminInfo.getAdminLName();
					String emailBody = "To reset your Password, please click on below button.";
					String emailClickButton = "Reset Password";
					String msg = UserDefinedMethods.getEmailTemplateMessage(sub, emailHeader, emailBody, emailClickURL, emailClickButton);
					emailSendingConfig.sendEmail(adminInfo.getAdminEmail(), sub, msg);
					redirectAttributes.addFlashAttribute("alertSuccessMessagepasswordResetLinkMessage",
							messageSource.getMessage("label.passwordResetLinkMessage", null, null));
					return true;
				} else {
					redirectAttributes.addFlashAttribute("alertFailMessageuserAccountVerificationFailed",
							messageSource.getMessage("label.userAccountVerificationFailed", null, null));
					return false;
				}
			} else {
				redirectAttributes.addFlashAttribute("alertFailMessageemailNotPresent",
						messageSource.getMessage("label.emailNotPresent", null, null));
				return false;
			}
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessageemailNotPresent",
					messageSource.getMessage("label.emailNotPresent", null, null));
			return false;
		}
	}

	@Transactional
	@Override
	public boolean adminResetPassword(@Valid AdminResetPasswordWrapper adminResetPasswordWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource) {
		byte[] decodedBytes = Base64.decode(adminResetPasswordWrapper.getAdminEmail());
		String decodedEmail = new String(decodedBytes);
		adminResetPasswordWrapper.setAdminEmail(decodedEmail);
		boolean flag = adminRepository.adminResetPassword(adminResetPasswordWrapper);
		if (flag) {
			redirectAttributes.addFlashAttribute("alertSuccessMessagepasswordResetSuccess",
					messageSource.getMessage("label.passwordResetSuccess", null, null));
			return true;
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessageuserAccountVerificationFailed",
					messageSource.getMessage("label.userAccountVerificationFailed", null, null));
			return false;
		}
	}

	@Transactional
	@Override
	public boolean updateAdminProfile(@Valid AdminProfileWrapper adminProfileWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session) {
		boolean flag = adminRepository.updateAdminProfileByAdminEmail(adminProfileWrapper);
		if (flag) {
			AdminInfo adminInfo = adminRepository.getAdminInfoByAdminEmail(adminProfileWrapper.getAdminEmail());
			if (adminInfo != null) {
				session.setAttribute("adminSession", adminInfo);
				redirectAttributes.addFlashAttribute("alertSuccessMessageadminProfileUpdateSuccess",
						messageSource.getMessage("label.adminProfileUpdateSuccess", null, null));
				return true;
			} else {
				redirectAttributes.addFlashAttribute("alertFailMessageadminProfileUpdateFail",
						messageSource.getMessage("label.adminProfileUpdateFail", null, null));
				return false;
			}
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessageadminProfileUpdateFail",
					messageSource.getMessage("label.adminProfileUpdateFail", null, null));
			return false;
		}
	}

	@Transactional
	@Override
	public boolean adminChangePassword(@Valid AdminChangePasswordWrapper adminChangePasswordWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session) {
		AdminInfo adminInfo = (AdminInfo) session.getAttribute("adminSession");

		if (adminInfo.getAdminPassword().equals(adminChangePasswordWrapper.getAdminOldPassword())) {
			boolean flag = adminRepository.adminChangePassword(adminChangePasswordWrapper, adminInfo.getAdminId());
			if (flag) {
				redirectAttributes.addFlashAttribute("alertSuccessMessagechangePasswordSuccess",
						messageSource.getMessage("label.changePasswordSuccess", null, null));
				return true;
			} else {
				redirectAttributes.addFlashAttribute("alertFailMessagechangePasswordFail",
						messageSource.getMessage("label.changePasswordFail", null, null));
				return false;
			}
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessageoldPasswordFail",
					messageSource.getMessage("label.oldPasswordFail", null, null));
			return false;
		}
	}

	@Transactional
	@Override
	public boolean uploadAdminProfileImage(MultipartFile[] adminProfileImage, String adminEmail, HttpSession session) {
		AdminInfo adminInfo = adminRepository.getAdminInfoByAdminEmail(adminEmail);
		String adminIdS = adminInfo.getAdminFName() + String.valueOf(adminInfo.getAdminId())
				+ adminInfo.getAdminLName();
		byte[] encodedBytes = Base64.encode(adminIdS.getBytes());
		String encryptedAdmin = new String(encodedBytes);

		if (adminProfileImage != null) {
			// Save file on system
			String adminProfileImageName = "";

			File adminProfileImagePath = new File(
					UserDefinedKeyWords.fileLocation + File.separator + "YWRtaW5Qcm9maWxlSW1hZ2VWaWV3UGF0aA");
			adminProfileImagePath.mkdir();

			File adminProfileIdFile = new File(adminProfileImagePath + File.separator + encryptedAdmin);
			adminProfileIdFile.mkdir();

			for (MultipartFile adminImage : adminProfileImage) {
				adminProfileImageName = adminImage.getOriginalFilename();

				if (!adminProfileImageName.isEmpty()) {
					Path path = Paths.get(adminProfileIdFile + File.separator + adminProfileImageName);
					try {
						Files.write(path, adminImage.getBytes());
					} catch (IOException e) {
						e.printStackTrace();
					}
					String adminProfileImageStoredPath = path.toString();
					String adminProfileImageViewPath = UserDefinedKeyWords.adminProfileImageViewPath + encryptedAdmin
							+ "/" + adminProfileImageName;

					String[] bits = adminProfileImageName.split("\\.");

					String adminProfileImageFileType = bits[bits.length - 1];

					if (adminInfo.getAdminProfileImageInfo() != null) {
						adminRepository.updateAdminProfileImageInfoByAdminProfileImageId(
								adminInfo.getAdminProfileImageInfo().getAdminProfileImageId(), adminProfileImageName,
								adminProfileImageStoredPath, adminProfileImageViewPath, adminProfileImageFileType);
					} else {
						Long adminProfileImageId = adminRepository.saveAndGetAdminProfileImageId(adminProfileImageName,
								adminProfileImageStoredPath, adminProfileImageViewPath, adminProfileImageFileType);
						if (adminProfileImageId != null) {
							adminRepository.updateAdminProfileImageIdByAdminId(adminInfo.getAdminId(),
									adminProfileImageId);
						}
					}
				}
			}
			session.setAttribute("adminSession", adminInfo);
			return true;
		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public boolean deleteAdminProfileImage(Long adminProfileImageId, Long adminId) {
		AdminInfo adminInfo = adminRepository.getAdminInfoByAdminId(adminId);
		if (adminInfo != null) {
			if (adminInfo.getAdminProfileImageInfo() != null) {
				Path path = Paths.get(adminInfo.getAdminProfileImageInfo().getAdminProfileImageStoredPath());
				try {
					Files.delete(path);
				} catch (IOException e) {
					e.printStackTrace();
				}
				boolean updateAdminInfo = adminRepository.updateAdminProfileImageInfoToNull(adminInfo.getAdminId());

				if (updateAdminInfo) {
					boolean deleteCoverImageStatus = adminRepository
							.deleteAdminProfileImageByAdminProfileImageId(adminProfileImageId);
					if (deleteCoverImageStatus) {
						return true;
					} else {
						return false;
					}
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	      
	
	  @Transactional
	  @Override 
	  public List<SubAdminAccessRoleInfo> getSubAdminAccessRoleInfo() {
	  List<SubAdminAccessRoleInfo> subAdminAccessRoleInfos =adminRepository.getSubAdminAccessRoleInfo(); 
	  System.out.println(subAdminAccessRoleInfos);
	  if (subAdminAccessRoleInfos != null) { 
		  subAdminAccessRoleInfos = subAdminAccessRoleInfos.stream().filter(i -> !i.getSubAdminInfo().isSubAdminDeleteStatus()).collect(Collectors.toList());
	  } 
	  return subAdminAccessRoleInfos; 
	  }
	 

	@Transactional
	@Override
	public SubAdminAccessRoleInfo getSubAdminAccessRoleInfoBySubAdminId(Long subAdminId) {
		return adminRepository.getSubAdminAccessRoleInfoBySubAdminId(subAdminId);
	}

	@Transactional
	@Override
	public List<TrainerInfo> getTrainerList() {
		return adminRepository.getTrainerList();
	}
	
	@Transactional
	@Override
	public List<TrainerInfo> getListOfTrainer(){
		return adminRepository.getListOfTrainer();
	}
	
	@Transactional
	@Override
	public List<TrainerCourseInfo> getCourseList() {
		return adminRepository.getCourseList();
	}
	
	/* Check Review */
	@Transactional
	@Override
	public List<UserCourseReviewInfo> getReviewList() {
		return adminRepository.getReviewList();
	}
	
	
	
	@Transactional
	@Override
	public List<UserInfo> getUserList() {
		return adminRepository.getUserList();
	}
	
	@Transactional
	@Override
	public List<NewsLetterInfo> getNewsLetterList(){
		return adminRepository.getNewsLetterList();
	}
	
	@Transactional
	@Override
	public List<UserInfo> getListOfUser(){
		return adminRepository.getListOfUser();
	}
	

	@Transactional
	@Override
	public List<CategoryInfo> getMainCategoryList() {
		return adminRepository.getMainCategoryList();
	}

	
	@Transactional
	@Override
	public List<SubCategoryInfo> getSubCategoryList() {
		return adminRepository.getSubCategoryList();
	}

	@Transactional
	@Override
	public List<FAQInfo> getFAQList() {
		return adminRepository.getFAQList();
	}

	@Transactional
	@Override
	public boolean createNewMainCategory(@Valid AdminCategoryWrapper adminCategoryWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session) {

		CategoryInfo categoryInfo = new CategoryInfo();
		categoryInfo.setCategoryName(adminCategoryWrapper.getCategoryName());
		categoryInfo.setCategoryDescription(adminCategoryWrapper.getCategoryDescription());

		categoryInfo = adminRepository.createNewMainCategory(categoryInfo);

		if (categoryInfo != null) {
			redirectAttributes.addFlashAttribute("alertSuccessMessagecategoryCreationSuccess",
					messageSource.getMessage("label.categoryCreationSuccess", null, null));
			return true;

		} else {
			redirectAttributes.addFlashAttribute("alertFailMessagecategoryCreationFail",
					messageSource.getMessage("label.categoryCreationFail", null, null));
			return false;
		}
	}
	
	
	@Transactional
	@Override
	public boolean createNewSubAdmin(@Valid SubAdminWrapper adminSubAdminWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session) {
		
		SubAdminAccessRoleInfo subAdminInfo = new SubAdminAccessRoleInfo();
		AdminInfo adminInfo=new AdminInfo();
		
		adminInfo.setAdminFName(adminSubAdminWrapper.getSubAdminFName());
		adminInfo.setAdminLName(adminSubAdminWrapper.getSubAdminLName());
		adminInfo.setAdminEmail(adminSubAdminWrapper.getSubAdminEmail());
		adminInfo.setAdminPhoneNo(Long.parseLong(adminSubAdminWrapper.getSubAdminPhoneNumber()));
		adminInfo.setAdminPassword(adminSubAdminWrapper.getSubAdminPassword());
		
		
		subAdminInfo.setSubAdminInfo(adminInfo);
		subAdminInfo.setSubAdminAccessForContentManagement(adminSubAdminWrapper.isSubAdminAccessForContentManagement());
		subAdminInfo.setSubAdminAccessForCategoryManagement(adminSubAdminWrapper.isSubAdminAccessForCategoryManagement());
		subAdminInfo.setSubAdminAccessForUserManagement(adminSubAdminWrapper.isSubAdminAccessForUserManagement());
		subAdminInfo.setSubAdminAccessForNewsletterManagement(adminSubAdminWrapper.isSubAdminAccessForNewsletterManagement());
		subAdminInfo.setSubAdminAccessForReviewsRatingsManagement(adminSubAdminWrapper.isSubAdminAccessForReviewsRatingsManagement());
		subAdminInfo.setSubAdminAccessForPaymentManagement(adminSubAdminWrapper.isSubAdminAccessForPaymentManagement());
		subAdminInfo.setSubAdminAccessForFAQsManagement(adminSubAdminWrapper.isSubAdminAccessForFAQsManagement());
		subAdminInfo.setSubAdminAccessForSubAdminManagement(adminSubAdminWrapper.isSubAdminAccessForSubAdminManagement());
		subAdminInfo.setSubAdminAccessForCourseManagement(adminSubAdminWrapper.isSubAdminAccessForCourseManagement());
		subAdminInfo.setSubAdminAccessForTrainerManagement(adminSubAdminWrapper.isSubAdminAccessForTrainerManagement());
		subAdminInfo.setSubAdminAccessForGeneralManagement(adminSubAdminWrapper.isSubAdminAccessForGeneralManagement());
		subAdminInfo.setSubAdminAccessForReportsManagement(adminSubAdminWrapper.isSubAdminAccessForReportsManagement());
		
		
		
		
		subAdminInfo = adminRepository.createNewSubAdmin(subAdminInfo);

		if (subAdminInfo != null) {
			redirectAttributes.addFlashAttribute("alertSuccessMessagecategoryCreationSuccess",
					messageSource.getMessage("label.subAdminAddSuccess", null, null));
			return true;

		} else {
			redirectAttributes.addFlashAttribute("alertFailMessagecategoryCreationFail",
					messageSource.getMessage("label.subAdminAddFail", null, null));
			return false;
		}		
	}
	
	

	@Transactional
	@Override
	public boolean changeMainCategoryStatus(Long categoryId, boolean categoryStatus,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session) {
		boolean changeCategoryStatus = adminRepository.changeMainCategoryStatus(categoryId, categoryStatus);
		if (changeCategoryStatus) {
			return true;
		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public boolean deleteMainCategory(Long categoryId, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session) {
		CategoryInfo categoryInfo = adminRepository.getCategoryInfoByCategoryId(categoryId);
		if (categoryInfo != null) {
			boolean deleteCategory = adminRepository.deleteCategoryByCategoryId(categoryInfo.getCategoryId(), true);

			if (deleteCategory) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public boolean deleteMainTrainerCourse(Long trainerCourseId, RedirectAttributes redirectAttributes, MessageSource messageSource,
			HttpSession session) {
		TrainerCourseInfo trainerCourseInfo = adminRepository.getTrainerCourseInfoByTrainerCourseId(trainerCourseId);
		if (trainerCourseInfo != null) {
		boolean deleteTrainerCourse=adminRepository.deleteTrainerCourseByTrainerCourseId(trainerCourseInfo.getTrainerCourseId(), true);
		
		if (deleteTrainerCourse) {
			return true;
		} else {
			return false;
		}
	} else {
		return false;
	}
	}
	
	
	
	@Transactional
	@Override
	public boolean updateMainCategory(@Valid AdminCategoryWrapper adminCategoryWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session) {
		boolean flag = adminRepository.updateMainCategory(adminCategoryWrapper);
		if (flag) {
			redirectAttributes.addFlashAttribute("alertSuccessMessagecategoryUpdateSuccess",
					messageSource.getMessage("label.categoryUpdateSuccess", null, null));
			return true;
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessagecategoryUpdateFail",
					messageSource.getMessage("label.categoryUpdateFail", null, null));
			return false;
		}
	}

	@Transactional
	@Override
	public CategoryInfo getCategoryInfoByCategoryId(Long categoryId) {
		return adminRepository.getCategoryInfoByCategoryId(categoryId);
	}

	@Transactional
	@Override
	public SubCategoryInfo getSubCategoryInfoBySubCategoryId(Long subcategoryId) {
		return adminRepository.getSubCategoryInfoBySubCategoryId(subcategoryId);
	}

	@Transactional
	@Override
	public boolean createNewSubCategory(@Valid AdminSubCategoryWrapper adminSubCategoryWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session) {
		CategoryInfo categoryInfo = new CategoryInfo();
		categoryInfo.setCategoryId(adminSubCategoryWrapper.getMainCategoryId());

		SubCategoryInfo subCategoryInfo = new SubCategoryInfo();
		subCategoryInfo.setSubcategoryName(adminSubCategoryWrapper.getSubCategoryName());
		subCategoryInfo.setSubcategoryDescription(adminSubCategoryWrapper.getSubCategoryDescription());
		subCategoryInfo.setCategoryInfo(categoryInfo);

		subCategoryInfo = adminRepository.createNewSubCategory(subCategoryInfo);

		if (subCategoryInfo != null) {
			redirectAttributes.addFlashAttribute("alertSuccessMessageaddNewSubCategorySuccess",
					messageSource.getMessage("label.addNewSubCategorySuccess", null, null));
			return true;
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessageaddNewSubCategoryFail",
					messageSource.getMessage("label.addNewSubCategoryFail", null, null));
			return false;
		}

	}

	@Transactional
	@Override
	public boolean changeSubCategoryStatus(Long subcategoryId, boolean subcategoryStatus,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session) {
		boolean changeSubCategoryStatus = adminRepository.changeSubCategoryStatus(subcategoryId, subcategoryStatus);
		if (changeSubCategoryStatus) {
			return true;
		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public boolean deleteSubCategory(Long subcategoryId, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session) {
		boolean deleteSubCategory = adminRepository.deleteSubCategoryBySubcategoryId(subcategoryId, true);
		if (deleteSubCategory) {
			redirectAttributes.addFlashAttribute("alertSuccessMessage",
					messageSource.getMessage("admin.deleteSubCategorySuccess", null, null));
			return true;
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessage",
					messageSource.getMessage("admin.deleteSubCategoryFail", null, null));
			return false;
		}
	}

	@Transactional
	@Override
	public boolean deleteAdminSubAdmin(Long subAdminAccessRoleId, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session) {
		
		boolean deleteSubAdmin=adminRepository.deleteSubSubAdminBySubAdminAccessRoleId(subAdminAccessRoleId, true);
		if (deleteSubAdmin) {
			redirectAttributes.addFlashAttribute("alertSuccessMessage",
					messageSource.getMessage("admin.deleteSubAdminSuccess", null, null));
			return true;
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessage",
					messageSource.getMessage("admin.deleteSubAdminFail", null, null));
			return false;
		}
		
	}
	
	
	@Transactional
	@Override
	public boolean updateSubCategory(@Valid AdminSubCategoryWrapper adminSubCategoryWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session) {
		boolean flag = adminRepository.updateSubCategory(adminSubCategoryWrapper);
		if (flag) {
			redirectAttributes.addFlashAttribute("alertSuccessMessagecategoryUpdateSuccess",
					messageSource.getMessage("label.categoryUpdateSuccess", null, null));
			return true;
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessagecategoryUpdateFail",
					messageSource.getMessage("label.categoryUpdateFail", null, null));
			return false;
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Transactional
	@Override
	public boolean updateSubAdmin(@Valid SubAdminWrapper subAdminWrapper, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session){
		
				
		boolean flag = adminRepository.updateSubAdmin(subAdminWrapper);
		
		
		if (flag) {
			AdminInfo adminInfo = adminRepository.getAdminInfoByAdminId(subAdminWrapper.getSubAdminId());
			if (adminInfo != null) {
				session.setAttribute("adminSession", adminInfo);
				
			redirectAttributes.addFlashAttribute("alertSuccessMessagecategoryUpdateSuccess",
					messageSource.getMessage("label.subAdminProfileUpdateSuccess", null, null));
			return true;
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessagecategoryUpdateFail",
					messageSource.getMessage("label.subAdminCreationFail", null, null));
			return false;
		}
	}return false;

	}
	
	
	
	
	
	
	
	
	
	
	
	
	@Transactional
	@Override
	public FAQInfo getFAQInfoByFAQId(Long faqId) {
		return adminRepository.getFAQInfoByFAQId(faqId);
	}

	
	@Transactional
	@Override
	public SubAdminAccessRoleInfo getSubAdminInfoBySubAdminId(Long subAdminAccessRoleId){
		return adminRepository.getSubAdminInfoBySubAdminId(subAdminAccessRoleId);
	}

	
	@Transactional
	@Override
	public boolean addNewFAQs(@Valid AdminFAQsWrapper adminFAQsWrapper, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session) {
		Long faqId = adminRepository.addNewFAQs(adminFAQsWrapper);

		if (faqId != null) {
			redirectAttributes.addFlashAttribute("alertSuccessMessageaddNewFAQsSuccess",
					messageSource.getMessage("label.addNewFAQsSuccess", null, null));
			return true;
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessageaddNewFAQsFail",
					messageSource.getMessage("label.addNewFAQsFail", null, null));
			return false;
		}
	}

	@Transactional
	@Override
	public boolean changeFAQsStatus(Long faqsId, boolean faqsStatus, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session) {
		boolean changeFAQsStatus = adminRepository.changeFAQsStatus(faqsId, faqsStatus);
		if (changeFAQsStatus) {
			return true;
		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public boolean deleteFAQs(Long faqsId, RedirectAttributes redirectAttributes, MessageSource messageSource,
			HttpSession session) {
		boolean deleteFAQs = adminRepository.deleteFAQs(faqsId, true);
		if (deleteFAQs) {
			return true;
		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public boolean updateFAQs(@Valid AdminFAQsWrapper adminFAQsWrapper, RedirectAttributes redirectAttributes,
			MessageSource messageSource) {
		boolean updateFAQs = adminRepository.updateFAQs(adminFAQsWrapper);
		if (updateFAQs) {
			redirectAttributes.addFlashAttribute("alertSuccessMessageupdateFAQsSuccess",
					messageSource.getMessage("label.updateFAQsSuccess", null, null));
			return true;
		} else {
			redirectAttributes.addFlashAttribute("alertFailMessageupdateFAQsFail",
					messageSource.getMessage("label.updateFAQsFail", null, null));
			return false;
		}
	}
	
	@Transactional
	@Override
	public boolean changeUserStatus(Long userId, boolean userStatus, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session) {
		boolean changeUserStatus = adminRepository.changeUserStatus(userId, userStatus);
		if (changeUserStatus) {
			return true;
		} else {
			return false;
		}
	}
	
	@Transactional
	@Override
	public boolean changeNewsLetterStatus(Long newsLetterId, boolean newsLetterStatus, RedirectAttributes redirectAttributes, 
			MessageSource messageSource, HttpSession session) {
		boolean changeNewsLetterStatus = adminRepository.changeNewsLetterStatus(newsLetterId, newsLetterStatus);
		if (changeNewsLetterStatus) {
			return true;
		} else {
			return false;
		}
	}
	
	@Transactional
	@Override
	public boolean changeSubAdminStatus(Long subAdminAccessRoleId, boolean subAdminStatus, RedirectAttributes redirectAttributes, 
			MessageSource messageSource, HttpSession session){
		boolean changeSubAdminStatus = adminRepository.changeSubAdminStatus(subAdminAccessRoleId, subAdminStatus);
		if (changeSubAdminStatus) {
			return true;
		} else {
			return false;
		}
	}
	
	@Transactional
	@Override
	public boolean changeReviewStatus(Long reviewId, boolean reviewStatus, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session){
		boolean changeReviewStatus = adminRepository.changeReviewStatus(reviewId, reviewStatus);
		if (changeReviewStatus) {
			return true;
		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public boolean changeTrainerStatus(Long trainerId, boolean trainerStatus, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session) {
		boolean changeTrainerStatus = adminRepository.changeTrainerStatus(trainerId, trainerStatus);
		if (changeTrainerStatus) {
			return true;
		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public UserInfo getUserInfoByUserId(Long userId) {
		return userRepository.getUserInfoByUserId(userId);
	}

	
	
	@Transactional
	@Override
	public UserCourseReviewInfo getReviewInfoByReviewId(Long reviewId){
		//return userRepository.getReviewInfoByReviewId(reviewId);
		return userRepository.getReviewInfoByReviewId(reviewId);
	}
	
	@Transactional
	@Override
	public TrainerInfo getTrainierInfoByTrainerId(Long trainerId) {
		return trainerRepository.getTrainerInfoByTrainerId(trainerId);
	}
	
	@Transactional
	@Override
	public TrainerCourseInfo getTrainierCourseInfoByTrainerCourseId(Long trainerCourseId) {
		return trainerRepository.getTrainerCourseInfoByTrainerCourseId(trainerCourseId);
	}

	@Transactional
	@Override
	public boolean changeTrainerAdminApproveStatus(Long trainerId, boolean trainerAdminApproveStatus,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session) {
		boolean changeTrainerAdminApproveStatus = adminRepository.changeTrainerAdminApproveStatus(trainerId, trainerAdminApproveStatus);
		if (changeTrainerAdminApproveStatus) {
			TrainerInfo trainerInfo = trainerRepository.getTrainerInfoByTrainerId(trainerId);
			
			EmailSendingConfig emailSendingConfig = new EmailSendingConfig();
			String sub = "NovoFitness Admin Approval";
			String emailHeader = "Hello " + trainerInfo.getTrainerFName() + " " + trainerInfo.getTrainerLName();
			String emailBody = "Admin approved your account successfully. <br> Now you can add courses of your choice. <br> "
					+ "To add new courses login to your NovoFitness Trainer Account";
			if (!trainerInfo.isTrainerIsApprovedByAdmin()) {
				sub = "NovoFitness Admin Dispproval";
				emailBody = "Your account is disapproved. <br> Please Contact to Admin.";
			}
			
			String msg = UserDefinedMethods.getEmailTemplateMessageWithoutButton(sub, emailHeader, emailBody);

			emailSendingConfig.sendEmail(trainerInfo.getTrainerEmail(), sub, msg);
			return true;
		} else {
			return false;
		}
	}

	@Transactional
	@Override
	public List<UserCoursePaymentInfo> getUserCoursePaymentListByTrainerCourseId(Long trainerCourseId) {
		return adminRepository.getUserCoursePaymentListByTrainerCourseId(trainerCourseId);
	}

	@Transactional
	@Override
	public List<UserCoursePaymentInfo> getUserCoursePaymentList() {
		return adminRepository.getUserCoursePaymentList();
	}

	@Transactional
	@Override
	public UserCoursePaymentInfo getUserCoursePaymentInfoByUserCoursePaymentId(Long userCoursePaymentId) {
		return adminRepository.getUserCoursePaymentInfoByUserCoursePaymentId(userCoursePaymentId);
	}

	@Transactional
	@Override
	public Long getAllUserCount() {
		return adminRepository.getAllUserCount();
	}

	@Transactional
	@Override
	public Long getAllTrainerCount() {
		return adminRepository.getAllTrainerCount();
	}

	@Transactional
	@Override
	public Long getAllTrainerCourseCount() {
		return adminRepository.getAllTrainerCourseCount();
	}

	@Transactional
	@Override
	public boolean payTrainerCoursePaymentContribution(Long userCoursePaymentId, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session) {
		return adminRepository.changeTrainerCoursePaymentContributionStatus(userCoursePaymentId, true);
	}

	@Transactional
	@Override
	public boolean addPaymentSettlementContribution(@Valid AdminPaymentSettlementWrapper adminPaymentSettlementWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session) {
		
		CourseContributionInfo courseContributionInfo = adminRepository.getCourseContributionInfo();
		
		if(courseContributionInfo != null) {
			boolean status = adminRepository.updateCourseContributionInfoByCourseContributionId(courseContributionInfo, adminPaymentSettlementWrapper.getPercentageContribution());
			if(status) {
				redirectAttributes.addFlashAttribute("alertSuccessMessagePaymentContribution",
						messageSource.getMessage("label.paymentContributionUpdateSuccess", null, null));
				return true;
			} else {
				redirectAttributes.addFlashAttribute("alertFailPaymentContribution",
						messageSource.getMessage("label.paymentContributionUpdateFail", null, null));
				return false;
			}
		} else {
			CourseContributionInfo courseContributionInfo1 = new CourseContributionInfo();
			courseContributionInfo1.setCourseContributionPercentage(Double.parseDouble(adminPaymentSettlementWrapper.getPercentageContribution()));
			
			courseContributionInfo1 = adminRepository.saveCourseContributionInfo(courseContributionInfo1);
			if(courseContributionInfo1 != null) {
				redirectAttributes.addFlashAttribute("alertSuccessMessagePaymentContribution",
						messageSource.getMessage("label.paymentContributionAddSuccess", null, null));
				return true;
			} else {
				redirectAttributes.addFlashAttribute("alertFailPaymentContribution",
						messageSource.getMessage("label.paymentContributionAddFail", null, null));
				return false;
			}
		}
	}

	@Transactional
	@Override
	public List<CourseContributionInfo> getCourseContributionList() {
		return adminRepository.getCourseContributionList();
	}

}