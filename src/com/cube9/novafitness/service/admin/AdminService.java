package com.cube9.novafitness.service.admin;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.context.MessageSource;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cube9.novafitness.model.admin.CategoryInfo;
import com.cube9.novafitness.model.admin.FAQInfo;
import com.cube9.novafitness.model.admin.SubAdminAccessRoleInfo;
import com.cube9.novafitness.model.admin.SubCategoryInfo;
import com.cube9.novafitness.model.course.CourseContributionInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseInfo;
import com.cube9.novafitness.model.trainer.TrainerInfo;
import com.cube9.novafitness.model.user.NewsLetterInfo;
import com.cube9.novafitness.model.user.UserCoursePaymentInfo;
import com.cube9.novafitness.model.user.UserCourseReviewInfo;
import com.cube9.novafitness.model.user.UserInfo;
import com.cube9.novafitness.wrapper.admin.AdminCategoryWrapper;
import com.cube9.novafitness.wrapper.admin.AdminChangePasswordWrapper;
import com.cube9.novafitness.wrapper.admin.AdminFAQsWrapper;
import com.cube9.novafitness.wrapper.admin.AdminLoginWrapper;
import com.cube9.novafitness.wrapper.admin.AdminPaymentSettlementWrapper;
import com.cube9.novafitness.wrapper.admin.AdminProfileWrapper;
import com.cube9.novafitness.wrapper.admin.AdminResetPasswordWrapper;
import com.cube9.novafitness.wrapper.admin.AdminSubCategoryWrapper;
import com.cube9.novafitness.wrapper.admin.SubAdminWrapper;

public interface AdminService {

	boolean adminLogin(@Valid AdminLoginWrapper adminLoginWrapper, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session);

	boolean adminForgetPassword(@Valid AdminLoginWrapper adminLoginWrapper, RedirectAttributes redirectAttributes,
			MessageSource messageSource);

	boolean adminResetPassword(@Valid AdminResetPasswordWrapper adminResetPasswordWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource);

	boolean updateAdminProfile(@Valid AdminProfileWrapper adminProfileWrapper, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session);

	boolean adminChangePassword(@Valid AdminChangePasswordWrapper adminChangePasswordWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session);

	boolean uploadAdminProfileImage(MultipartFile[] adminProfileImage, String adminEmail, HttpSession session);

	boolean deleteAdminProfileImage(Long adminProfileImageId, Long adminId);

	 List<SubAdminAccessRoleInfo> getSubAdminAccessRoleInfo(); 

	SubAdminAccessRoleInfo getSubAdminAccessRoleInfoBySubAdminId(Long subAdminId);

	List<TrainerInfo> getTrainerList();
	
	List<TrainerInfo> getListOfTrainer();
	
	List<TrainerCourseInfo> getCourseList();
	
	List<UserCourseReviewInfo> getReviewList();

	List<UserInfo> getUserList();
	
	List<NewsLetterInfo> getNewsLetterList();
	
	List<UserInfo> getListOfUser();

	List<CategoryInfo> getMainCategoryList();
	
	

	List<SubCategoryInfo> getSubCategoryList();

	List<FAQInfo> getFAQList();

	boolean createNewMainCategory(@Valid AdminCategoryWrapper adminCategoryWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session);
	
	boolean createNewSubAdmin(@Valid SubAdminWrapper adminSubAdminWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session);

	
	boolean changeMainCategoryStatus(Long categoryId, boolean categoryStatus, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session);

	boolean deleteMainCategory(Long categoryId, RedirectAttributes redirectAttributes, MessageSource messageSource,
			HttpSession session);

	boolean deleteMainTrainerCourse(Long trainerCourseId, RedirectAttributes redirectAttributes, MessageSource messageSource,
			HttpSession session);
	
	
	boolean updateMainCategory(@Valid AdminCategoryWrapper adminCategoryWrapper, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session);

	CategoryInfo getCategoryInfoByCategoryId(Long categoryId);

	SubCategoryInfo getSubCategoryInfoBySubCategoryId(Long subcategoryId);

	boolean createNewSubCategory(@Valid AdminSubCategoryWrapper adminSubCategoryWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session);

	boolean changeSubCategoryStatus(Long subcategoryId, boolean subcategoryStatus,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session);

	boolean deleteSubCategory(Long subcategoryId, RedirectAttributes redirectAttributes, MessageSource messageSource,
			HttpSession session);
	

	boolean deleteAdminSubAdmin(Long subAdminAccessRoleId, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session);



	boolean updateSubCategory(@Valid AdminSubCategoryWrapper adminSubCategoryWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session);
	
	boolean updateSubAdmin(@Valid SubAdminWrapper subAdminWrapper, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session);

	FAQInfo getFAQInfoByFAQId(Long faqId);

	boolean addNewFAQs(@Valid AdminFAQsWrapper adminFAQsWrapper, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session);

	boolean changeFAQsStatus(Long faqsId, boolean faqsStatus, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session);

	boolean deleteFAQs(Long faqsId, RedirectAttributes redirectAttributes, MessageSource messageSource,
			HttpSession session);

	boolean updateFAQs(@Valid AdminFAQsWrapper adminFAQsWrapper, RedirectAttributes redirectAttributes,
			MessageSource messageSource);

	boolean changeUserStatus(Long userId, boolean userStatus, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session);
	
	boolean changeNewsLetterStatus(Long newsLetterId, boolean newsLetterStatus, RedirectAttributes redirectAttributes, 
			MessageSource messageSource, HttpSession session);
	
	
	boolean changeSubAdminStatus(Long subAdminAccessRoleId, boolean subAdminStatus, RedirectAttributes redirectAttributes, MessageSource messageSource,
			HttpSession session);

	boolean changeReviewStatus(Long reviewId, boolean reviewStatus, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session);
	
	boolean changeTrainerStatus(Long trainerId, boolean trainerStatus, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session);

	UserInfo getUserInfoByUserId(Long userId);
	
	SubAdminAccessRoleInfo getSubAdminInfoBySubAdminId(Long subAdminAccessRoleId);
	
	UserCourseReviewInfo getReviewInfoByReviewId(Long reviewId);

	TrainerInfo getTrainierInfoByTrainerId(Long trainerId);
	
	TrainerCourseInfo getTrainierCourseInfoByTrainerCourseId(Long trainerCourseId);

	boolean changeTrainerAdminApproveStatus(Long trainerId, boolean trainerAdminApproveStatus,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session);

	List<UserCoursePaymentInfo> getUserCoursePaymentListByTrainerCourseId(Long trainerCourseId);

	List<UserCoursePaymentInfo> getUserCoursePaymentList();

	UserCoursePaymentInfo getUserCoursePaymentInfoByUserCoursePaymentId(Long userCoursePaymentId);

	Long getAllUserCount();

	Long getAllTrainerCount();

	Long getAllTrainerCourseCount();

	boolean payTrainerCoursePaymentContribution(Long userCoursePaymentId, RedirectAttributes redirectAttributes,
			MessageSource messageSource, HttpSession session);

	boolean addPaymentSettlementContribution(@Valid AdminPaymentSettlementWrapper adminPaymentSettlementWrapper,
			RedirectAttributes redirectAttributes, MessageSource messageSource, HttpSession session);

	List<CourseContributionInfo> getCourseContributionList();

}
