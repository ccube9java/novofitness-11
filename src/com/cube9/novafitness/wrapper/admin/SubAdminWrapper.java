package com.cube9.novafitness.wrapper.admin;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class SubAdminWrapper {

	private Long subAdminAccessRoleId;
	
	private Long subAdminId;
	
	@NotEmpty(message = "{label.requiredField}")
	private String subAdminFName;

	@NotEmpty(message = "{label.requiredField}")
	private String subAdminLName;

	@NotEmpty(message = "{label.requiredField}")
	private String subAdminEmail;

	@NotEmpty(message = "{label.requiredField}")
	private String subAdminConfirmPassword;
	
	@NotEmpty(message = "{label.requiredField}")
	@Size(max = 20, min = 6)
	private String subAdminPassword;
	
	private String subAdminPhoneNumber;
	
		private boolean subAdminAccessForContentManagement;
	
		private boolean subAdminAccessForCategoryManagement;
	
		private boolean subAdminAccessForUserManagement;
	
		private boolean subAdminAccessForNewsletterManagement;
	
		private boolean subAdminAccessForReviewsRatingsManagement;
	
		private boolean subAdminAccessForPaymentManagement;
	
		private boolean subAdminAccessForFAQsManagement;
	
		private boolean subAdminAccessForSubAdminManagement;
	
		private boolean subAdminAccessForCourseManagement;
	
		private boolean subAdminAccessForTrainerManagement;
		
		private boolean subAdminAccessForGeneralManagement;
	
		private boolean subAdminAccessForReportsManagement;

		public Long getSubAdminAccessRoleId() {
			return subAdminAccessRoleId;
		}

		public Long getSubAdminId() {
			return subAdminId;
		}

		public String getSubAdminFName() {
			return subAdminFName;
		}

		public String getSubAdminLName() {
			return subAdminLName;
		}

		public String getSubAdminEmail() {
			return subAdminEmail;
		}

		public String getSubAdminConfirmPassword() {
			return subAdminConfirmPassword;
		}

		public String getSubAdminPassword() {
			return subAdminPassword;
		}

		public String getSubAdminPhoneNumber() {
			return subAdminPhoneNumber;
		}

		public boolean isSubAdminAccessForContentManagement() {
			return subAdminAccessForContentManagement;
		}

		public boolean isSubAdminAccessForCategoryManagement() {
			return subAdminAccessForCategoryManagement;
		}

		public boolean isSubAdminAccessForUserManagement() {
			return subAdminAccessForUserManagement;
		}

		public boolean isSubAdminAccessForNewsletterManagement() {
			return subAdminAccessForNewsletterManagement;
		}

		public boolean isSubAdminAccessForReviewsRatingsManagement() {
			return subAdminAccessForReviewsRatingsManagement;
		}

		public boolean isSubAdminAccessForPaymentManagement() {
			return subAdminAccessForPaymentManagement;
		}

		public boolean isSubAdminAccessForFAQsManagement() {
			return subAdminAccessForFAQsManagement;
		}

		public boolean isSubAdminAccessForSubAdminManagement() {
			return subAdminAccessForSubAdminManagement;
		}

		public boolean isSubAdminAccessForCourseManagement() {
			return subAdminAccessForCourseManagement;
		}

		public boolean isSubAdminAccessForTrainerManagement() {
			return subAdminAccessForTrainerManagement;
		}

		public boolean isSubAdminAccessForGeneralManagement() {
			return subAdminAccessForGeneralManagement;
		}

		public boolean isSubAdminAccessForReportsManagement() {
			return subAdminAccessForReportsManagement;
		}

		public void setSubAdminAccessRoleId(Long subAdminAccessRoleId) {
			this.subAdminAccessRoleId = subAdminAccessRoleId;
		}

		public void setSubAdminId(Long subAdminId) {
			this.subAdminId = subAdminId;
		}

		public void setSubAdminFName(String subAdminFName) {
			this.subAdminFName = subAdminFName;
		}

		public void setSubAdminLName(String subAdminLName) {
			this.subAdminLName = subAdminLName;
		}

		public void setSubAdminEmail(String subAdminEmail) {
			this.subAdminEmail = subAdminEmail;
		}

		public void setSubAdminConfirmPassword(String subAdminConfirmPassword) {
			this.subAdminConfirmPassword = subAdminConfirmPassword;
		}

		public void setSubAdminPassword(String subAdminPassword) {
			this.subAdminPassword = subAdminPassword;
		}

		public void setSubAdminPhoneNumber(String subAdminPhoneNumber) {
			this.subAdminPhoneNumber = subAdminPhoneNumber;
		}

		public void setSubAdminAccessForContentManagement(boolean subAdminAccessForContentManagement) {
			this.subAdminAccessForContentManagement = subAdminAccessForContentManagement;
		}

		public void setSubAdminAccessForCategoryManagement(boolean subAdminAccessForCategoryManagement) {
			this.subAdminAccessForCategoryManagement = subAdminAccessForCategoryManagement;
		}

		public void setSubAdminAccessForUserManagement(boolean subAdminAccessForUserManagement) {
			this.subAdminAccessForUserManagement = subAdminAccessForUserManagement;
		}

		public void setSubAdminAccessForNewsletterManagement(boolean subAdminAccessForNewsletterManagement) {
			this.subAdminAccessForNewsletterManagement = subAdminAccessForNewsletterManagement;
		}

		public void setSubAdminAccessForReviewsRatingsManagement(boolean subAdminAccessForReviewsRatingsManagement) {
			this.subAdminAccessForReviewsRatingsManagement = subAdminAccessForReviewsRatingsManagement;
		}

		public void setSubAdminAccessForPaymentManagement(boolean subAdminAccessForPaymentManagement) {
			this.subAdminAccessForPaymentManagement = subAdminAccessForPaymentManagement;
		}

		public void setSubAdminAccessForFAQsManagement(boolean subAdminAccessForFAQsManagement) {
			this.subAdminAccessForFAQsManagement = subAdminAccessForFAQsManagement;
		}

		public void setSubAdminAccessForSubAdminManagement(boolean subAdminAccessForSubAdminManagement) {
			this.subAdminAccessForSubAdminManagement = subAdminAccessForSubAdminManagement;
		}

		public void setSubAdminAccessForCourseManagement(boolean subAdminAccessForCourseManagement) {
			this.subAdminAccessForCourseManagement = subAdminAccessForCourseManagement;
		}

		public void setSubAdminAccessForTrainerManagement(boolean subAdminAccessForTrainerManagement) {
			this.subAdminAccessForTrainerManagement = subAdminAccessForTrainerManagement;
		}

		public void setSubAdminAccessForGeneralManagement(boolean subAdminAccessForGeneralManagement) {
			this.subAdminAccessForGeneralManagement = subAdminAccessForGeneralManagement;
		}

		public void setSubAdminAccessForReportsManagement(boolean subAdminAccessForReportsManagement) {
			this.subAdminAccessForReportsManagement = subAdminAccessForReportsManagement;
		}

		@Override
		public String toString() {
			return "SubAdminWrapper [subAdminAccessRoleId=" + subAdminAccessRoleId + ", subAdminId=" + subAdminId
					+ ", subAdminFName=" + subAdminFName + ", subAdminLName=" + subAdminLName + ", subAdminEmail="
					+ subAdminEmail + ", subAdminConfirmPassword=" + subAdminConfirmPassword + ", subAdminPassword="
					+ subAdminPassword + ", subAdminPhoneNumber=" + subAdminPhoneNumber
					+ ", subAdminAccessForContentManagement=" + subAdminAccessForContentManagement
					+ ", subAdminAccessForCategoryManagement=" + subAdminAccessForCategoryManagement
					+ ", subAdminAccessForUserManagement=" + subAdminAccessForUserManagement
					+ ", subAdminAccessForNewsletterManagement=" + subAdminAccessForNewsletterManagement
					+ ", subAdminAccessForReviewsRatingsManagement=" + subAdminAccessForReviewsRatingsManagement
					+ ", subAdminAccessForPaymentManagement=" + subAdminAccessForPaymentManagement
					+ ", subAdminAccessForFAQsManagement=" + subAdminAccessForFAQsManagement
					+ ", subAdminAccessForSubAdminManagement=" + subAdminAccessForSubAdminManagement
					+ ", subAdminAccessForCourseManagement=" + subAdminAccessForCourseManagement
					+ ", subAdminAccessForTrainerManagement=" + subAdminAccessForTrainerManagement
					+ ", subAdminAccessForGeneralManagement=" + subAdminAccessForGeneralManagement
					+ ", subAdminAccessForReportsManagement=" + subAdminAccessForReportsManagement + "]";
		}
		

		
		
	
	
	
}
