package com.cube9.novafitness.wrapper.admin;

import javax.validation.constraints.NotEmpty;

public class AdminCityWrapper {

	@NotEmpty(message = "{label.requiredField}")
	private String cityName;

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	@Override
	public String toString() {
		return "AdminCityWrapper [cityName=" + cityName + "]";
	}
	
}
