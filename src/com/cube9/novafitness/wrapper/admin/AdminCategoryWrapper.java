package com.cube9.novafitness.wrapper.admin;

import javax.validation.constraints.NotEmpty;

public class AdminCategoryWrapper {

	private Long categoryId;
	
	@NotEmpty(message = "{label.requiredField}")
	private String categoryName;

	@NotEmpty(message = "{label.requiredField}")
	private String categoryDescription;
	
	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryDescription() {
		return categoryDescription;
	}

	public void setCategoryDescription(String categoryDescription) {
		this.categoryDescription = categoryDescription;
	}

	@Override
	public String toString() {
		return "AdminCategoryWrapper [categoryId=" + categoryId + ", categoryName=" + categoryName
				+ ", categoryDescription=" + categoryDescription + "]";
	}
	
}
