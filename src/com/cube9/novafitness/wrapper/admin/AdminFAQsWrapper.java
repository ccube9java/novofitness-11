package com.cube9.novafitness.wrapper.admin;

import javax.validation.constraints.NotEmpty;

public class AdminFAQsWrapper {

	private Long faqId;
	
	@NotEmpty(message = "{label.requiredField}")
	private String faqsQuestion;

	@NotEmpty(message = "{label.requiredField}")
	private String faqsAnswer;

	public Long getFaqId() {
		return faqId;
	}

	public void setFaqId(Long faqId) {
		this.faqId = faqId;
	}

	public String getFaqsQuestion() {
		return faqsQuestion;
	}

	public void setFaqsQuestion(String faqsQuestion) {
		this.faqsQuestion = faqsQuestion;
	}

	public String getFaqsAnswer() {
		return faqsAnswer;
	}

	public void setFaqsAnswer(String faqsAnswer) {
		this.faqsAnswer = faqsAnswer;
	}

	@Override
	public String toString() {
		return "AdminFAQsWrapper [faqId=" + faqId + ", faqsQuestion=" + faqsQuestion + ", faqsAnswer=" + faqsAnswer
				+ "]";
	}
	
}
