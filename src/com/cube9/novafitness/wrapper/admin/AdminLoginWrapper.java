package com.cube9.novafitness.wrapper.admin;

import javax.validation.constraints.NotEmpty;

public class AdminLoginWrapper {

	@NotEmpty(message = "{label.requiredField}")
	private String adminEmail;
	
	private String adminPassword;

	public String getAdminEmail() {
		return adminEmail;
	}

	public void setAdminEmail(String adminEmail) {
		this.adminEmail = adminEmail;
	}

	public String getAdminPassword() {
		return adminPassword;
	}

	public void setAdminPassword(String adminPassword) {
		this.adminPassword = adminPassword;
	}

	@Override
	public String toString() {
		return "AdminLoginWrapper [adminEmail=" + adminEmail + ", adminPassword=" + adminPassword + "]";
	}
	
}
