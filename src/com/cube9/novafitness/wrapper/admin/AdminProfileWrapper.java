package com.cube9.novafitness.wrapper.admin;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class AdminProfileWrapper {

	@NotEmpty(message = "{label.requiredField}")
	@Size(max = 20, min = 3)
	private String adminFName;

	@NotEmpty(message = "{label.requiredField}")
	@Size(max = 20, min = 3)
	private String adminLName;

	@NotEmpty(message = "{label.requiredField}")
	@Email
	private String adminEmail;

	//@NotNull(message = "{label.requiredField}")
	//private Long adminPhoneNumber;
	
	private String adminPhoneNumber;

	public String getAdminFName() {
		return adminFName;
	}

	public void setAdminFName(String adminFName) {
		this.adminFName = adminFName;
	}

	public String getAdminLName() {
		return adminLName;
	}

	public void setAdminLName(String adminLName) {
		this.adminLName = adminLName;
	}

	public String getAdminEmail() {
		return adminEmail;
	}

	public void setAdminEmail(String adminEmail) {
		this.adminEmail = adminEmail;
	}

	
	/*
	 * public Long getAdminPhoneNumber() { return adminPhoneNumber; }
	 * 
	 * public void setAdminPhoneNumber(Long adminPhoneNumber) {
	 * this.adminPhoneNumber = adminPhoneNumber; }
	 */

	public String getAdminPhoneNumber() {
		return adminPhoneNumber;
	}

	public void setAdminPhoneNumber(String adminPhoneNumber) {
		this.adminPhoneNumber = adminPhoneNumber;
	}

	@Override
	public String toString() {
		return "AdminProfileWrapper [adminFName=" + adminFName + ", adminLName=" + adminLName + ", adminEmail="
				+ adminEmail + ", adminPhoneNumber=" + adminPhoneNumber + "]";
	}
	
}
