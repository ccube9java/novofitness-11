package com.cube9.novafitness.wrapper.admin;

import javax.validation.constraints.NotEmpty;

public class AdminChangePasswordWrapper {

	@NotEmpty(message = "{label.requiredField}")
	private String adminConfirmPassword;
	
	@NotEmpty(message = "{label.requiredField}")
	private String adminPassword;
	
	@NotEmpty(message = "{label.requiredField}")
	private String adminOldPassword;

	public String getAdminConfirmPassword() {
		return adminConfirmPassword;
	}

	public void setAdminConfirmPassword(String adminConfirmPassword) {
		this.adminConfirmPassword = adminConfirmPassword;
	}

	public String getAdminPassword() {
		return adminPassword;
	}

	public void setAdminPassword(String adminPassword) {
		this.adminPassword = adminPassword;
	}

	public String getAdminOldPassword() {
		return adminOldPassword;
	}

	public void setAdminOldPassword(String adminOldPassword) {
		this.adminOldPassword = adminOldPassword;
	}

	@Override
	public String toString() {
		return "AdminChangePasswordWrapper [adminConfirmPassword=" + adminConfirmPassword + ", adminPassword="
				+ adminPassword + ", adminOldPassword=" + adminOldPassword + "]";
	}

}
