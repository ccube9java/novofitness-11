package com.cube9.novafitness.wrapper.admin;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class AdminSubCategoryWrapper {

	private Long subcategoryId;
	
	@NotEmpty(message = "{label.requiredField}")
	private String subCategoryName;
	
	@NotEmpty(message = "{label.requiredField}")
	private String subCategoryDescription;

	@NotNull(message = "{label.requiredField}")
	private Long mainCategoryId;

	public Long getSubcategoryId() {
		return subcategoryId;
	}

	public void setSubcategoryId(Long subcategoryId) {
		this.subcategoryId = subcategoryId;
	}

	public String getSubCategoryName() {
		return subCategoryName;
	}

	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}

	public String getSubCategoryDescription() {
		return subCategoryDescription;
	}

	public void setSubCategoryDescription(String subCategoryDescription) {
		this.subCategoryDescription = subCategoryDescription;
	}

	public Long getMainCategoryId() {
		return mainCategoryId;
	}

	public void setMainCategoryId(Long mainCategoryId) {
		this.mainCategoryId = mainCategoryId;
	}

	@Override
	public String toString() {
		return "AdminSubCategoryWrapper [subcategoryId=" + subcategoryId + ", subCategoryName=" + subCategoryName
				+ ", subCategoryDescription=" + subCategoryDescription + ", mainCategoryId=" + mainCategoryId + "]";
	}
	
}
