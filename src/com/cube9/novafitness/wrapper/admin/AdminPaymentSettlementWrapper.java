package com.cube9.novafitness.wrapper.admin;

import javax.validation.constraints.NotEmpty;

public class AdminPaymentSettlementWrapper {

	@NotEmpty(message = "{label.requiredField}")
	private String percentageContribution;

	public String getPercentageContribution() {
		return percentageContribution;
	}

	public void setPercentageContribution(String percentageContribution) {
		this.percentageContribution = percentageContribution;
	}

	@Override
	public String toString() {
		return "AdminPaymentSettlementWrapper [percentageContribution=" + percentageContribution + "]";
	}
	
}
