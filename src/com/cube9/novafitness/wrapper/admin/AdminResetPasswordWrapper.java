package com.cube9.novafitness.wrapper.admin;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class AdminResetPasswordWrapper {
	
	private String adminEmail;

	@NotEmpty(message = "{label.requiredField}")
	@Size(max = 20, min = 6)
	private String adminConfirmPassword;
	
	@NotEmpty(message = "{label.requiredField}")
	@Size(max = 20, min = 6)
	private String adminPassword;

	public String getAdminEmail() {
		return adminEmail;
	}

	public void setAdminEmail(String adminEmail) {
		this.adminEmail = adminEmail;
	}

	public String getAdminConfirmPassword() {
		return adminConfirmPassword;
	}

	public void setAdminConfirmPassword(String adminConfirmPassword) {
		this.adminConfirmPassword = adminConfirmPassword;
	}

	public String getAdminPassword() {
		return adminPassword;
	}

	public void setAdminPassword(String adminPassword) {
		this.adminPassword = adminPassword;
	}

	@Override
	public String toString() {
		return "AdminResetPasswordWrapper [adminEmail=" + adminEmail + ", adminConfirmPassword=" + adminConfirmPassword
				+ ", adminPassword=" + adminPassword + "]";
	}
	
}
