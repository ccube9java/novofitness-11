package com.cube9.novafitness.wrapper.course;

import java.util.Arrays;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.web.multipart.MultipartFile;

public class TrainerCourseWrapper {

	private Long trainerId;
	
	private Long trainerCourseId;
	
	private Long courseId;
	
	@NotEmpty(message = "{label.requiredField}")
	private String courseName;
	
	@NotNull(message = "{label.requiredField}")
	private Long categoryId;
	@NotEmpty(message = "{label.requiredField}")
	private String courseLanguage;
	
	//@NotEmpty(message = "{label.requiredField}")
	private String courseDescription;
	
	//@NotEmpty(message = "{label.requiredField}")
	private String courseContent;
	
	@NotEmpty(message = "{label.requiredField}")
	private String coursePrice;
	
	//@NotNull(message = "{label.requiredField}")
	private MultipartFile courseCoverImage;
	
	//@NotNull(message = "{label.requiredField}")
	private MultipartFile[] courseOtherImages;
	
	//@NotNull(message = "{label.requiredField}")
	private MultipartFile courseCoverVideo;
		
	//@NotNull(message = "{label.requiredField}")
	//private MultipartFile[] courseOtherVideos;
	
	//@NotNull(message = "{label.requiredField}")
	//private MultipartFile[] courseOtherPdfs;

	public Long getTrainerId() {
		return trainerId;
	}

	public void setTrainerId(Long trainerId) {
		this.trainerId = trainerId;
	}

	public Long getTrainerCourseId() {
		return trainerCourseId;
	}

	public void setTrainerCourseId(Long trainerCourseId) {
		this.trainerCourseId = trainerCourseId;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getCourseDescription() {
		return courseDescription;
	}

	public void setCourseDescription(String courseDescription) {
		this.courseDescription = courseDescription;
	}

	public String getCourseContent() {
		return courseContent;
	}

	public void setCourseContent(String courseContent) {
		this.courseContent = courseContent;
	}

	public String getCoursePrice() {
		return coursePrice;
	}

	public void setCoursePrice(String coursePrice) {
		this.coursePrice = coursePrice;
	}

	public MultipartFile getCourseCoverImage() {
		return courseCoverImage;
	}

	public void setCourseCoverImage(MultipartFile courseCoverImage) {
		this.courseCoverImage = courseCoverImage;
	}

	public MultipartFile[] getCourseOtherImages() {
		return courseOtherImages;
	}

	public void setCourseOtherImages(MultipartFile[] courseOtherImages) {
		this.courseOtherImages = courseOtherImages;
	}

	public MultipartFile getCourseCoverVideo() {
		return courseCoverVideo;
	}

	public void setCourseCoverVideo(MultipartFile courseCoverVideo) {
		this.courseCoverVideo = courseCoverVideo;
	}
	
	/*
	 * public MultipartFile[] getCourseOtherVideos() { return courseOtherVideos; }
	 * 
	 * public void setCourseOtherVideos(MultipartFile[] courseOtherVideos) {
	 * this.courseOtherVideos = courseOtherVideos; }
	 * 
	 * public MultipartFile[] getCourseOtherPdfs() { return courseOtherPdfs; }
	 * 
	 * public void setCourseOtherPdfs(MultipartFile[] courseOtherPdfs) {
	 * this.courseOtherPdfs = courseOtherPdfs; }
	 */

	public String getCourseLanguage() {
		return courseLanguage;
	}

	public void setCourseLanguage(String courseLanguage) {
		this.courseLanguage = courseLanguage;
	}

	@Override
	public String toString() {
		return "TrainerCourseWrapper [trainerId=" + trainerId + ", trainerCourseId=" + trainerCourseId + ", courseId="
				+ courseId + ", courseName=" + courseName + ", categoryId=" + categoryId + ", courseLanguage="
				+ courseLanguage + ", courseDescription=" + courseDescription + ", courseContent=" + courseContent
				+ ", coursePrice=" + coursePrice + ", courseCoverImage=" + courseCoverImage + ", courseOtherImages="
				+ Arrays.toString(courseOtherImages) + ", courseCoverVideo=" + courseCoverVideo + "]";
	}

}
