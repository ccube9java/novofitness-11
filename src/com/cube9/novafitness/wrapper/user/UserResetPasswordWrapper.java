package com.cube9.novafitness.wrapper.user;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class UserResetPasswordWrapper {

	private String userEmail;

	@NotEmpty(message = "{label.requiredField}")
	@Size(max = 20, min = 6)
	private String userConfirmPassword;
	
	@NotEmpty(message = "{label.requiredField}")
	@Size(max = 20, min = 6)
	private String userPassword;

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserConfirmPassword() {
		return userConfirmPassword;
	}

	public void setUserConfirmPassword(String userConfirmPassword) {
		this.userConfirmPassword = userConfirmPassword;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	@Override
	public String toString() {
		return "UserResetPasswordWrapper [userEmail=" + userEmail + ", userConfirmPassword=" + userConfirmPassword
				+ ", userPassword=" + userPassword + "]";
	}
	
}
