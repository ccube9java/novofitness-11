package com.cube9.novafitness.wrapper.user;

import javax.validation.constraints.NotEmpty;

public class TrainerCourseUserQuestionWrapper {

	@NotEmpty(message = "{label.requiredField}")
	private String trainerCourseUserQuestionDetails;
	
	private Long trainerCourseId;
	
	private Long trainerId;
	
	private Long userId;
	
	private Long trainerCourseUserQuestionId;
	
	private Long trainerCourseUserQuestionAnswerId;

	public String getTrainerCourseUserQuestionDetails() {
		return trainerCourseUserQuestionDetails;
	}

	public void setTrainerCourseUserQuestionDetails(String trainerCourseUserQuestionDetails) {
		this.trainerCourseUserQuestionDetails = trainerCourseUserQuestionDetails;
	}

	public Long getTrainerId() {
		return trainerId;
	}

	public void setTrainerId(Long trainerId) {
		this.trainerId = trainerId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getTrainerCourseId() {
		return trainerCourseId;
	}

	public void setTrainerCourseId(Long trainerCourseId) {
		this.trainerCourseId = trainerCourseId;
	}

	public Long getTrainerCourseUserQuestionId() {
		return trainerCourseUserQuestionId;
	}

	public void setTrainerCourseUserQuestionId(Long trainerCourseUserQuestionId) {
		this.trainerCourseUserQuestionId = trainerCourseUserQuestionId;
	}

	public Long getTrainerCourseUserQuestionAnswerId() {
		return trainerCourseUserQuestionAnswerId;
	}

	public void setTrainerCourseUserQuestionAnswerId(Long trainerCourseUserQuestionAnswerId) {
		this.trainerCourseUserQuestionAnswerId = trainerCourseUserQuestionAnswerId;
	}

	@Override
	public String toString() {
		return "TrainerCourseUserQuestionWrapper [trainerCourseUserQuestionDetails=" + trainerCourseUserQuestionDetails
				+ ", trainerCourseId=" + trainerCourseId + ", trainerCourseUserQuestionId="
				+ trainerCourseUserQuestionId + ", trainerCourseUserQuestionAnswerId="
				+ trainerCourseUserQuestionAnswerId + "]";
	}
	
}
