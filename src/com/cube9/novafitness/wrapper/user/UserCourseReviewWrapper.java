package com.cube9.novafitness.wrapper.user;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class UserCourseReviewWrapper {
	
	@NotNull
	private Long trainerCourseId;
	
	@NotNull
	private Long trainerId;
	
	private Long reviewedUserId;
	
	@NotEmpty(message = "{label.requiredField}")
	private String userCourseReviewStarRating;
	
	@NotEmpty(message = "{label.requiredField}")
	private String userCourseReviewTitle;
	
	@NotEmpty(message = "{label.requiredField}")
	private String userCourseReviewDescription;

	public Long getTrainerCourseId() {
		return trainerCourseId;
	}

	public void setTrainerCourseId(Long trainerCourseId) {
		this.trainerCourseId = trainerCourseId;
	}

	public Long getTrainerId() {
		return trainerId;
	}

	public void setTrainerId(Long trainerId) {
		this.trainerId = trainerId;
	}

	public Long getReviewedUserId() {
		return reviewedUserId;
	}

	public void setReviewedUserId(Long reviewedUserId) {
		this.reviewedUserId = reviewedUserId;
	}

	public String getUserCourseReviewStarRating() {
		return userCourseReviewStarRating;
	}

	public void setUserCourseReviewStarRating(String userCourseReviewStarRating) {
		this.userCourseReviewStarRating = userCourseReviewStarRating;
	}

	public String getUserCourseReviewTitle() {
		return userCourseReviewTitle;
	}

	public void setUserCourseReviewTitle(String userCourseReviewTitle) {
		this.userCourseReviewTitle = userCourseReviewTitle;
	}

	public String getUserCourseReviewDescription() {
		return userCourseReviewDescription;
	}

	public void setUserCourseReviewDescription(String userCourseReviewDescription) {
		this.userCourseReviewDescription = userCourseReviewDescription;
	}

	@Override
	public String toString() {
		return "UserCourseReviewWrapper [trainerCourseId=" + trainerCourseId + ", trainerId=" + trainerId
				+ ", reviewedUserId=" + reviewedUserId + ", userCourseReviewStarRating=" + userCourseReviewStarRating
				+ ", userCourseReviewTitle=" + userCourseReviewTitle + ", userCourseReviewDescription="
				+ userCourseReviewDescription + "]";
	}
	
}
