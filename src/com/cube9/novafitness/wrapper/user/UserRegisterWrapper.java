package com.cube9.novafitness.wrapper.user;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserRegisterWrapper {

	@NotEmpty(message = "{label.requiredField}")
	private String userFName;

	@NotEmpty(message = "{label.requiredField}")
	private String userLName;

	@NotEmpty(message = "{label.requiredField}")
	@Email(message = "{label.invalidEmail}")
	private String userEmail;

	@NotEmpty(message = "{label.requiredField}")
	private String userConfirmPassword;

	@NotEmpty(message = "{label.requiredField}")
	@Size(max = 20, min = 6)
	private String userPassword;

	@NotNull(message = "{label.requiredField}")
	private boolean userAcceptTermsCondition;

	private boolean userAcceptNewsletterSubscription;

	@NotEmpty(message = "{label.requiredField}")
	private String userPhoneNumber;

	public String getUserFName() {
		return userFName;
	}

	public void setUserFName(String userFName) {
		this.userFName = userFName;
	}

	public String getUserLName() {
		return userLName;
	}

	public void setUserLName(String userLName) {
		this.userLName = userLName;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserConfirmPassword() {
		return userConfirmPassword;
	}

	public void setUserConfirmPassword(String userConfirmPassword) {
		this.userConfirmPassword = userConfirmPassword;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserPhoneNumber() {
		return userPhoneNumber;
	}

	public void setUserPhoneNumber(String userPhoneNumber) {
		this.userPhoneNumber = userPhoneNumber;
	}

	public boolean isUserAcceptTermsCondition() {
		return userAcceptTermsCondition;
	}

	public void setUserAcceptTermsCondition(boolean userAcceptTermsCondition) {
		this.userAcceptTermsCondition = userAcceptTermsCondition;
	}

	public boolean isUserAcceptNewsletterSubscription() {
		return userAcceptNewsletterSubscription;
	}

	public void setUserAcceptNewsletterSubscription(boolean userAcceptNewsletterSubscription) {
		this.userAcceptNewsletterSubscription = userAcceptNewsletterSubscription;
	}

	@Override
	public String toString() {
		return "UserRegisterWrapper [userFName=" + userFName + ", userLName=" + userLName + ", userEmail=" + userEmail
				+ ", userConfirmPassword=" + userConfirmPassword + ", userPassword=" + userPassword
				+ ", userAcceptTermsCondition=" + userAcceptTermsCondition + ", userAcceptNewsletterSubscription="
				+ userAcceptNewsletterSubscription + ", userPhoneNumber=" + userPhoneNumber + "]";
	}

}
