package com.cube9.novafitness.wrapper.user;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class UserProfileWrapper {

	@NotEmpty(message = "{label.requiredField}")
	private String userFName;

	@NotEmpty(message = "{label.requiredField}")
	private String userLName;

	@NotEmpty(message = "{label.requiredField}")
	@Email
	private String userEmail;

	/* @NotEmpty(message = "{label.requiredField}") */
	private String userPhoneNumber;
	
	@NotEmpty(message = "{label.requiredField}")
	private String userLocation;

	@NotEmpty(message = "{label.requiredField}")
	private String userCity;
	
	@NotEmpty(message = "{label.requiredField}")
	private String userState;
	
	@NotEmpty(message = "{label.requiredField}")
	private String userCountry;
	
	@NotNull(message = "{label.requiredField}")
	private Long genderId;

	public String getUserFName() {
		return userFName;
	}

	public void setUserFName(String userFName) {
		this.userFName = userFName;
	}

	public String getUserLName() {
		return userLName;
	}

	public void setUserLName(String userLName) {
		this.userLName = userLName;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserPhoneNumber() {
		return userPhoneNumber;
	}

	public void setUserPhoneNumber(String userPhoneNumber) {
		this.userPhoneNumber = userPhoneNumber;
	}

	public String getUserLocation() {
		return userLocation;
	}

	public void setUserLocation(String userLocation) {
		this.userLocation = userLocation;
	}

	public String getUserCity() {
		return userCity;
	}

	public void setUserCity(String userCity) {
		this.userCity = userCity;
	}

	public String getUserState() {
		return userState;
	}

	public void setUserState(String userState) {
		this.userState = userState;
	}

	public String getUserCountry() {
		return userCountry;
	}

	public void setUserCountry(String userCountry) {
		this.userCountry = userCountry;
	}

	public Long getGenderId() {
		return genderId;
	}

	public void setGenderId(Long genderId) {
		this.genderId = genderId;
	}

	@Override
	public String toString() {
		return "UserProfileWrapper [userFName=" + userFName + ", userLName=" + userLName + ", userEmail=" + userEmail
				+ ", userPhoneNumber=" + userPhoneNumber + ", userLocation=" + userLocation + ", userCity=" + userCity
				+ ", userState=" + userState + ", userCountry=" + userCountry + ", genderId=" + genderId + "]";
	}
	
}
