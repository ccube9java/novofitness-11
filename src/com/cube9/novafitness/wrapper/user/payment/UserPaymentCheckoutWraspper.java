package com.cube9.novafitness.wrapper.user.payment;

import java.util.List;

import com.cube9.novafitness.model.user.CartTrainerCourseUserInfo;

public class UserPaymentCheckoutWraspper {

	private Double myCartListTotalPrice = 0.0;
	
	private Long couponCodeId;
	
	private Long userId;
	
	private String userOrderNumber;
	
	private List<CartTrainerCourseUserInfo> myCartCourseList;

	public Double getMyCartListTotalPrice() {
		return myCartListTotalPrice;
	}

	public void setMyCartListTotalPrice(Double myCartListTotalPrice) {
		this.myCartListTotalPrice = myCartListTotalPrice;
	}

	public Long getCouponCodeId() {
		return couponCodeId;
	}

	public void setCouponCodeId(Long couponCodeId) {
		this.couponCodeId = couponCodeId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserOrderNumber() {
		return userOrderNumber;
	}

	public void setUserOrderNumber(String userOrderNumber) {
		this.userOrderNumber = userOrderNumber;
	}

	public List<CartTrainerCourseUserInfo> getMyCartCourseList() {
		return myCartCourseList;
	}

	public void setMyCartCourseList(List<CartTrainerCourseUserInfo> myCartCourseList) {
		this.myCartCourseList = myCartCourseList;
	}

	@Override
	public String toString() {
		return "UserPaymentCheckoutWraspper [myCartListTotalPrice=" + myCartListTotalPrice + ", couponCodeId="
				+ couponCodeId + ", userId=" + userId + ", userOrderNumber=" + userOrderNumber + ", myCartCourseList="
				+ myCartCourseList + "]";
	}
	
}
