package com.cube9.novafitness.wrapper.user.payment;

public class CouponCodePaymentWrapper {
	
	//@NotEmpty(message = "{label.requiredField}")
	private String couponCodeValue;
	
	private Long userId;

	private Double myCartListTotalPrice = 0.0;
	
	public String getCouponCodeValue() {
		return couponCodeValue;
	}

	public void setCouponCodeValue(String couponCodeValue) {
		this.couponCodeValue = couponCodeValue;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Double getMyCartListTotalPrice() {
		return myCartListTotalPrice;
	}

	public void setMyCartListTotalPrice(Double myCartListTotalPrice) {
		this.myCartListTotalPrice = myCartListTotalPrice;
	}

	@Override
	public String toString() {
		return "CouponCodePaymentWrapper [couponCodeValue=" + couponCodeValue + ", userId=" + userId
				+ ", myCartListTotalPrice=" + myCartListTotalPrice + "]";
	}
	
}
