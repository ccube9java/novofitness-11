package com.cube9.novafitness.wrapper.user;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

public class NewsLetterWrapper {

	@NotEmpty(message = "{label.requiredField}")
	@Email(message = "{valid.email}")
	//@Email
	private String userEmail;
	
	@NotEmpty(message = "{valid.required}")
	private String userName;
	
	//@NotNull(message = "{valid.required}")
	//private boolean acceptTermsCondition;
	
	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String toString() {
		return "NewsLetterWrapper [userEmail=" + userEmail + ", userName=" + userName + "]";
	}

}
