package com.cube9.novafitness.wrapper.user;

import javax.validation.constraints.NotEmpty;

public class UserChangePasswordWrapper {

	public UserChangePasswordWrapper() {
		super();
	}

	@NotEmpty(message = "{label.requiredField}")
	private String userConfirmPassword;
	
	@NotEmpty(message = "{label.requiredField}")
	private String userPassword;
	
	@NotEmpty(message = "{label.requiredField}")
	private String userOldPassword;

	public String getUserConfirmPassword() {
		return userConfirmPassword;
	}

	public void setUserConfirmPassword(String userConfirmPassword) {
		this.userConfirmPassword = userConfirmPassword;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserOldPassword() {
		return userOldPassword;
	}

	public void setUserOldPassword(String userOldPassword) {
		this.userOldPassword = userOldPassword;
	}

	public UserChangePasswordWrapper(@NotEmpty(message = "{valid.required}") String userPassword) {
		super();
		this.userPassword = userPassword;
	}

	@Override
	public String toString() {
		return "UserChangePasswordWrapper [userConfirmPassword=" + userConfirmPassword + ", userPassword="
				+ userPassword + ", userOldPassword=" + userOldPassword + "]";
	}
	
}
