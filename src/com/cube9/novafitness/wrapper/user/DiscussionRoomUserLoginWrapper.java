package com.cube9.novafitness.wrapper.user;

import javax.validation.constraints.NotEmpty;

public class DiscussionRoomUserLoginWrapper {

	@NotEmpty(message = "{label.requiredField}")
	private String userEmail;
	
	private String userPassword;
	
	private Long trainerCourseId;
	
	private Long trainerCourseUserQuestionId;
	
	private Long trainerCourseUserQuestionAnswerId;

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public Long getTrainerCourseId() {
		return trainerCourseId;
	}

	public void setTrainerCourseId(Long trainerCourseId) {
		this.trainerCourseId = trainerCourseId;
	}

	public Long getTrainerCourseUserQuestionId() {
		return trainerCourseUserQuestionId;
	}

	public void setTrainerCourseUserQuestionId(Long trainerCourseUserQuestionId) {
		this.trainerCourseUserQuestionId = trainerCourseUserQuestionId;
	}

	public Long getTrainerCourseUserQuestionAnswerId() {
		return trainerCourseUserQuestionAnswerId;
	}

	public void setTrainerCourseUserQuestionAnswerId(Long trainerCourseUserQuestionAnswerId) {
		this.trainerCourseUserQuestionAnswerId = trainerCourseUserQuestionAnswerId;
	}

	@Override
	public String toString() {
		return "DiscussionRoomUserLoginWrapper [userEmail=" + userEmail + ", userPassword=" + userPassword
				+ ", trainerCourseId=" + trainerCourseId + ", trainerCourseUserQuestionId="
				+ trainerCourseUserQuestionId + ", trainerCourseUserQuestionAnswerId="
				+ trainerCourseUserQuestionAnswerId + "]";
	}
	
}
