package com.cube9.novafitness.wrapper.user;

import javax.validation.constraints.NotEmpty;

public class UserLoginWrapper {

	@NotEmpty(message = "{label.requiredField}")
	private String userEmail;
	
	private String userPassword;
	
	private Long trainerCourseId;

	public UserLoginWrapper() {
		super();
	}

	public UserLoginWrapper(@NotEmpty(message = "{label.requiredField}") String userEmail, String userPassword) {
		super();
		this.userEmail = userEmail;
		this.userPassword = userPassword;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public Long getTrainerCourseId() {
		return trainerCourseId;
	}

	public void setTrainerCourseId(Long trainerCourseId) {
		this.trainerCourseId = trainerCourseId;
	}

	@Override
	public String toString() {
		return "UserLoginWrapper [userEmail=" + userEmail + ", userPassword=" + userPassword + ", trainerCourseId="
				+ trainerCourseId + "]";
	}
	
}
