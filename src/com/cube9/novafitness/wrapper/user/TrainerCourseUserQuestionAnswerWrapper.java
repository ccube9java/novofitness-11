package com.cube9.novafitness.wrapper.user;

import javax.validation.constraints.NotEmpty;

public class TrainerCourseUserQuestionAnswerWrapper {

	@NotEmpty(message = "{label.requiredField}")
	private String trainerCourseUserQuestionAnswerDetails;
	
	private Long trainerCourseId;
	
	private Long trainerId;
	
	private Long userId;
	
	private Long trainerCourseUserQuestionId;
	
	private Long trainerCourseUserQuestionAnswerId;

	public String getTrainerCourseUserQuestionAnswerDetails() {
		return trainerCourseUserQuestionAnswerDetails;
	}

	public void setTrainerCourseUserQuestionAnswerDetails(String trainerCourseUserQuestionAnswerDetails) {
		this.trainerCourseUserQuestionAnswerDetails = trainerCourseUserQuestionAnswerDetails;
	}

	public Long getTrainerCourseId() {
		return trainerCourseId;
	}

	public void setTrainerCourseId(Long trainerCourseId) {
		this.trainerCourseId = trainerCourseId;
	}

	public Long getTrainerId() {
		return trainerId;
	}

	public void setTrainerId(Long trainerId) {
		this.trainerId = trainerId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getTrainerCourseUserQuestionId() {
		return trainerCourseUserQuestionId;
	}

	public void setTrainerCourseUserQuestionId(Long trainerCourseUserQuestionId) {
		this.trainerCourseUserQuestionId = trainerCourseUserQuestionId;
	}

	public Long getTrainerCourseUserQuestionAnswerId() {
		return trainerCourseUserQuestionAnswerId;
	}

	public void setTrainerCourseUserQuestionAnswerId(Long trainerCourseUserQuestionAnswerId) {
		this.trainerCourseUserQuestionAnswerId = trainerCourseUserQuestionAnswerId;
	}

	@Override
	public String toString() {
		return "TrainerCourseUserQuestionAnswerWrapper [trainerCourseUserQuestionAnswerDetails="
				+ trainerCourseUserQuestionAnswerDetails + ", trainerCourseId=" + trainerCourseId + ", trainerId="
				+ trainerId + ", userId=" + userId + ", trainerCourseUserQuestionId=" + trainerCourseUserQuestionId
				+ ", trainerCourseUserQuestionAnswerId=" + trainerCourseUserQuestionAnswerId + "]";
	}
	
}
