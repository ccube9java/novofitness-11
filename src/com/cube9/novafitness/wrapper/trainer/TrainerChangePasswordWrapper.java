package com.cube9.novafitness.wrapper.trainer;

import javax.validation.constraints.NotEmpty;

public class TrainerChangePasswordWrapper {

	@NotEmpty(message = "{label.requiredField}")
	private String trainerConfirmPassword;
	
	@NotEmpty(message = "{label.requiredField}")
	private String trainerPassword;
	
	@NotEmpty(message = "{label.requiredField}")
	private String trainerOldPassword;

	public String getTrainerConfirmPassword() {
		return trainerConfirmPassword;
	}

	public void setTrainerConfirmPassword(String trainerConfirmPassword) {
		this.trainerConfirmPassword = trainerConfirmPassword;
	}

	public String getTrainerPassword() {
		return trainerPassword;
	}

	public void setTrainerPassword(String trainerPassword) {
		this.trainerPassword = trainerPassword;
	}

	public String getTrainerOldPassword() {
		return trainerOldPassword;
	}

	public void setTrainerOldPassword(String trainerOldPassword) {
		this.trainerOldPassword = trainerOldPassword;
	}

	@Override
	public String toString() {
		return "TrainerChangePasswordWrapper [trainerConfirmPassword=" + trainerConfirmPassword + ", trainerPassword="
				+ trainerPassword + ", trainerOldPassword=" + trainerOldPassword + "]";
	}
	
}
