package com.cube9.novafitness.wrapper.trainer;

import javax.validation.constraints.NotEmpty;

public class TrainerCourseFAQWrapper {

	private Long trainerCourseId;
	
	private Long trainerCourseFAQId;
	
	@NotEmpty(message = "{label.requiredField}")
	private String trainerCourseFAQQuestion;

	@NotEmpty(message = "{label.requiredField}")
	private String trainerCourseFAQAnswer;

	public Long getTrainerCourseId() {
		return trainerCourseId;
	}

	public void setTrainerCourseId(Long trainerCourseId) {
		this.trainerCourseId = trainerCourseId;
	}

	public Long getTrainerCourseFAQId() {
		return trainerCourseFAQId;
	}

	public void setTrainerCourseFAQId(Long trainerCourseFAQId) {
		this.trainerCourseFAQId = trainerCourseFAQId;
	}

	public String getTrainerCourseFAQQuestion() {
		return trainerCourseFAQQuestion;
	}

	public void setTrainerCourseFAQQuestion(String trainerCourseFAQQuestion) {
		this.trainerCourseFAQQuestion = trainerCourseFAQQuestion;
	}

	public String getTrainerCourseFAQAnswer() {
		return trainerCourseFAQAnswer;
	}

	public void setTrainerCourseFAQAnswer(String trainerCourseFAQAnswer) {
		this.trainerCourseFAQAnswer = trainerCourseFAQAnswer;
	}

	@Override
	public String toString() {
		return "TrainerCourseFAQWrapper [trainerCourseId=" + trainerCourseId + ", trainerCourseFAQId="
				+ trainerCourseFAQId + ", trainerCourseFAQQuestion=" + trainerCourseFAQQuestion
				+ ", trainerCourseFAQAnswer=" + trainerCourseFAQAnswer + "]";
	}
	
}
