package com.cube9.novafitness.wrapper.trainer;

import javax.validation.constraints.NotEmpty;

public class TrainerCourseChapterWrapper {
	
	private Long trainerCourseId;
	
	private Long trainerCourseChapterId;
	
	@NotEmpty(message = "{label.requiredField}")
	private String trainerCourseChapterName;

	public Long getTrainerCourseId() {
		return trainerCourseId;
	}

	public void setTrainerCourseId(Long trainerCourseId) {
		this.trainerCourseId = trainerCourseId;
	}

	public String getTrainerCourseChapterName() {
		return trainerCourseChapterName;
	}

	public void setTrainerCourseChapterName(String trainerCourseChapterName) {
		this.trainerCourseChapterName = trainerCourseChapterName;
	}

	public Long getTrainerCourseChapterId() {
		return trainerCourseChapterId;
	}

	public void setTrainerCourseChapterId(Long trainerCourseChapterId) {
		this.trainerCourseChapterId = trainerCourseChapterId;
	}

	@Override
	public String toString() {
		return "TrainerCourseChapterWrapper [trainerCourseId=" + trainerCourseId + ", trainerCourseChapterId="
				+ trainerCourseChapterId + ", trainerCourseChapterName=" + trainerCourseChapterName + "]";
	}
	
}