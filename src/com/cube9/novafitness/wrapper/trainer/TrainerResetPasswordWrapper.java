package com.cube9.novafitness.wrapper.trainer;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class TrainerResetPasswordWrapper {

	private String trainerEmail;

	@NotEmpty(message = "{label.requiredField}")
	@Size(max = 20, min = 6)
	private String trainerConfirmPassword;
	
	@NotEmpty(message = "{label.requiredField}")
	@Size(max = 20, min = 6)
	private String trainerPassword;

	public String getTrainerEmail() {
		return trainerEmail;
	}

	public void setTrainerEmail(String trainerEmail) {
		this.trainerEmail = trainerEmail;
	}

	public String getTrainerConfirmPassword() {
		return trainerConfirmPassword;
	}

	public void setTrainerConfirmPassword(String trainerConfirmPassword) {
		this.trainerConfirmPassword = trainerConfirmPassword;
	}

	public String getTrainerPassword() {
		return trainerPassword;
	}

	public void setTrainerPassword(String trainerPassword) {
		this.trainerPassword = trainerPassword;
	}

	@Override
	public String toString() {
		return "TrainerResetPasswordWrapper [trainerEmail=" + trainerEmail + ", trainerConfirmPassword="
				+ trainerConfirmPassword + ", trainerPassword=" + trainerPassword + "]";
	}
	
}
