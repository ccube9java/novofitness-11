package com.cube9.novafitness.wrapper.trainer;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.web.multipart.MultipartFile;

public class TrainerRegisterWrapper {

	@NotEmpty(message = "{label.requiredField}")
	private String trainerFName;

	@NotEmpty(message = "{label.requiredField}")
	private String trainerLName;

	@NotEmpty(message = "{label.requiredField}")
	@Email(message = "{label.invalidEmail}")
	private String trainerEmail;
	
	@NotEmpty(message = "{label.requiredField}")
	private String trainerConfirmPassword;
	
	@NotEmpty(message = "{label.requiredField}")
	@Size(max = 20, min = 6)
	private String trainerPassword;
	
	@NotNull(message = "{label.requiredField}")
	private MultipartFile trainerCertificateInfo;
	
	@NotNull(message = "{label.requiredField}")
	private MultipartFile trainerIDCopyInfo;
	
	@NotNull(message = "{label.requiredField}")
	private boolean trainerAcceptTermsCondition;

	public String getTrainerFName() {
		return trainerFName;
	}

	public void setTrainerFName(String trainerFName) {
		this.trainerFName = trainerFName;
	}

	public String getTrainerLName() {
		return trainerLName;
	}

	public void setTrainerLName(String trainerLName) {
		this.trainerLName = trainerLName;
	}

	public String getTrainerEmail() {
		return trainerEmail;
	}

	public void setTrainerEmail(String trainerEmail) {
		this.trainerEmail = trainerEmail;
	}

	public String getTrainerConfirmPassword() {
		return trainerConfirmPassword;
	}

	public void setTrainerConfirmPassword(String trainerConfirmPassword) {
		this.trainerConfirmPassword = trainerConfirmPassword;
	}

	public String getTrainerPassword() {
		return trainerPassword;
	}

	public void setTrainerPassword(String trainerPassword) {
		this.trainerPassword = trainerPassword;
	}

	public MultipartFile getTrainerCertificateInfo() {
		return trainerCertificateInfo;
	}

	public void setTrainerCertificateInfo(MultipartFile trainerCertificateInfo) {
		this.trainerCertificateInfo = trainerCertificateInfo;
	}

	public MultipartFile getTrainerIDCopyInfo() {
		return trainerIDCopyInfo;
	}

	public void setTrainerIDCopyInfo(MultipartFile trainerIDCopyInfo) {
		this.trainerIDCopyInfo = trainerIDCopyInfo;
	}

	public boolean isTrainerAcceptTermsCondition() {
		return trainerAcceptTermsCondition;
	}

	public void setTrainerAcceptTermsCondition(boolean trainerAcceptTermsCondition) {
		this.trainerAcceptTermsCondition = trainerAcceptTermsCondition;
	}

	@Override
	public String toString() {
		return "TrainerRegisterWrapper [trainerFName=" + trainerFName + ", trainerLName=" + trainerLName
				+ ", trainerEmail=" + trainerEmail + ", trainerConfirmPassword=" + trainerConfirmPassword
				+ ", trainerPassword=" + trainerPassword + ", trainerCertificateInfo=" + trainerCertificateInfo
				+ ", trainerIDCopyInfo=" + trainerIDCopyInfo + ", trainerAcceptTermsCondition="
				+ trainerAcceptTermsCondition + "]";
	}
	
}
