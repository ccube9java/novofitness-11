package com.cube9.novafitness.wrapper.trainer;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

public class TrainerProfileWrapper {

	@NotEmpty(message = "{label.requiredField}")
	private String trainerFName;

	@NotEmpty(message = "{label.requiredField}")
	private String trainerLName;

	@NotEmpty(message = "{label.requiredField}")
	@Email(message = "{label.invalidEmail}")
	private String trainerEmail;
	
	//@NotEmpty(message = "{label.requiredField}")
	private String trainerDescription;
	
	private String trainerWebsiteLink;
	
	private String trainerPayPalId;
	
	private String trainerFacebookLink;
	
	private String trainerInstagramLink;
	
	private String trainerYoutubeLink;

	public String getTrainerFName() {
		return trainerFName;
	}

	public void setTrainerFName(String trainerFName) {
		this.trainerFName = trainerFName;
	}

	public String getTrainerLName() {
		return trainerLName;
	}

	public void setTrainerLName(String trainerLName) {
		this.trainerLName = trainerLName;
	}

	public String getTrainerEmail() {
		return trainerEmail;
	}

	public void setTrainerEmail(String trainerEmail) {
		this.trainerEmail = trainerEmail;
	}

	public String getTrainerDescription() {
		return trainerDescription;
	}

	public void setTrainerDescription(String trainerDescription) {
		this.trainerDescription = trainerDescription;
	}

	public String getTrainerWebsiteLink() {
		return trainerWebsiteLink;
	}

	public void setTrainerWebsiteLink(String trainerWebsiteLink) {
		this.trainerWebsiteLink = trainerWebsiteLink;
	}

	public String getTrainerFacebookLink() {
		return trainerFacebookLink;
	}

	public void setTrainerFacebookLink(String trainerFacebookLink) {
		this.trainerFacebookLink = trainerFacebookLink;
	}

	public String getTrainerInstagramLink() {
		return trainerInstagramLink;
	}

	public void setTrainerInstagramLink(String trainerInstagramLink) {
		this.trainerInstagramLink = trainerInstagramLink;
	}

	public String getTrainerYoutubeLink() {
		return trainerYoutubeLink;
	}

	public void setTrainerYoutubeLink(String trainerYoutubeLink) {
		this.trainerYoutubeLink = trainerYoutubeLink;
	}

	public String getTrainerPayPalId() {
		return trainerPayPalId;
	}

	public void setTrainerPayPalId(String trainerPayPalId) {
		this.trainerPayPalId = trainerPayPalId;
	}

	@Override
	public String toString() {
		return "TrainerProfileWrapper [trainerFName=" + trainerFName + ", trainerLName=" + trainerLName
				+ ", trainerEmail=" + trainerEmail + ", trainerDescription=" + trainerDescription
				+ ", trainerWebsiteLink=" + trainerWebsiteLink + ", trainerPayPalId=" + trainerPayPalId
				+ ", trainerFacebookLink=" + trainerFacebookLink + ", trainerInstagramLink=" + trainerInstagramLink
				+ ", trainerYoutubeLink=" + trainerYoutubeLink + "]";
	}

}
