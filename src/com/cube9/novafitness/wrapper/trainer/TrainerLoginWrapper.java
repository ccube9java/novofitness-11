package com.cube9.novafitness.wrapper.trainer;

import javax.validation.constraints.NotEmpty;

public class TrainerLoginWrapper {

	@NotEmpty(message = "{label.requiredField}")
	private String trainerEmail;
	
	private String trainerPassword;

	public String getTrainerEmail() {
		return trainerEmail;
	}

	public void setTrainerEmail(String trainerEmail) {
		this.trainerEmail = trainerEmail;
	}

	public String getTrainerPassword() {
		return trainerPassword;
	}

	public void setTrainerPassword(String trainerPassword) {
		this.trainerPassword = trainerPassword;
	}

	@Override
	public String toString() {
		return "TrainerLoginWrapper [trainerEmail=" + trainerEmail + ", trainerPassword=" + trainerPassword + "]";
	}
	
}
