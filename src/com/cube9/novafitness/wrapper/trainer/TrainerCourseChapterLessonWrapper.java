package com.cube9.novafitness.wrapper.trainer;

import javax.validation.constraints.NotEmpty;

import org.springframework.web.multipart.MultipartFile;

public class TrainerCourseChapterLessonWrapper {
	
	private Long trainerCourseChapterLessonId;
	
	private Long trainerCourseChapterId;

	//@NotEmpty(message = "{label.requiredField}")
	private String trainerCourseChapterName;
	
	@NotEmpty(message = "{label.requiredField}")
	private String trainerCourseChapterLessonName;

	// @NotNull(message = "{label.requiredField}")
	private MultipartFile trainerCourseChapterLessonPreviewVideo;

	// @NotNull(message = "{label.requiredField}")
	private MultipartFile trainerCourseChapterLessonMainVideo;
	
	private MultipartFile trainerCourseChapterLessonMainPdf;

	public Long getTrainerCourseChapterLessonId() {
		return trainerCourseChapterLessonId;
	}

	public void setTrainerCourseChapterLessonId(Long trainerCourseChapterLessonId) {
		this.trainerCourseChapterLessonId = trainerCourseChapterLessonId;
	}

	public Long getTrainerCourseChapterId() {
		return trainerCourseChapterId;
	}

	public void setTrainerCourseChapterId(Long trainerCourseChapterId) {
		this.trainerCourseChapterId = trainerCourseChapterId;
	}

	public String getTrainerCourseChapterName() {
		return trainerCourseChapterName;
	}

	public void setTrainerCourseChapterName(String trainerCourseChapterName) {
		this.trainerCourseChapterName = trainerCourseChapterName;
	}

	public String getTrainerCourseChapterLessonName() {
		return trainerCourseChapterLessonName;
	}

	public void setTrainerCourseChapterLessonName(String trainerCourseChapterLessonName) {
		this.trainerCourseChapterLessonName = trainerCourseChapterLessonName;
	}

	public MultipartFile getTrainerCourseChapterLessonPreviewVideo() {
		return trainerCourseChapterLessonPreviewVideo;
	}

	public void setTrainerCourseChapterLessonPreviewVideo(MultipartFile trainerCourseChapterLessonPreviewVideo) {
		this.trainerCourseChapterLessonPreviewVideo = trainerCourseChapterLessonPreviewVideo;
	}

	public MultipartFile getTrainerCourseChapterLessonMainVideo() {
		return trainerCourseChapterLessonMainVideo;
	}

	public void setTrainerCourseChapterLessonMainVideo(MultipartFile trainerCourseChapterLessonMainVideo) {
		this.trainerCourseChapterLessonMainVideo = trainerCourseChapterLessonMainVideo;
	}

	public MultipartFile getTrainerCourseChapterLessonMainPdf() {
		return trainerCourseChapterLessonMainPdf;
	}

	public void setTrainerCourseChapterLessonMainPdf(MultipartFile trainerCourseChapterLessonMainPdf) {
		this.trainerCourseChapterLessonMainPdf = trainerCourseChapterLessonMainPdf;
	}

	@Override
	public String toString() {
		return "TrainerCourseChapterLessonWrapper [trainerCourseChapterLessonId=" + trainerCourseChapterLessonId
				+ ", trainerCourseChapterId=" + trainerCourseChapterId + ", trainerCourseChapterName="
				+ trainerCourseChapterName + ", trainerCourseChapterLessonName=" + trainerCourseChapterLessonName
				+ ", trainerCourseChapterLessonPreviewVideo=" + trainerCourseChapterLessonPreviewVideo
				+ ", trainerCourseChapterLessonMainVideo=" + trainerCourseChapterLessonMainVideo
				+ ", trainerCourseChapterLessonMainPdf=" + trainerCourseChapterLessonMainPdf + "]";
	}

}