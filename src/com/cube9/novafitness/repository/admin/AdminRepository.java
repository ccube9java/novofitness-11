package com.cube9.novafitness.repository.admin;

import java.time.LocalDateTime;
import java.util.List;

import javax.validation.Valid;

import com.cube9.novafitness.model.admin.AdminInfo;
import com.cube9.novafitness.model.admin.CategoryInfo;
import com.cube9.novafitness.model.admin.FAQInfo;
import com.cube9.novafitness.model.admin.SubAdminAccessRoleInfo;
import com.cube9.novafitness.model.admin.SubCategoryInfo;
import com.cube9.novafitness.model.course.CourseContributionInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseInfo;
import com.cube9.novafitness.model.trainer.TrainerInfo;
import com.cube9.novafitness.model.user.NewsLetterInfo;
import com.cube9.novafitness.model.user.UserCoursePaymentInfo;
import com.cube9.novafitness.model.user.UserCourseReviewInfo;
import com.cube9.novafitness.model.user.UserInfo;
import com.cube9.novafitness.wrapper.admin.AdminCategoryWrapper;
import com.cube9.novafitness.wrapper.admin.AdminChangePasswordWrapper;
import com.cube9.novafitness.wrapper.admin.AdminFAQsWrapper;
import com.cube9.novafitness.wrapper.admin.AdminProfileWrapper;
import com.cube9.novafitness.wrapper.admin.AdminResetPasswordWrapper;
import com.cube9.novafitness.wrapper.admin.AdminSubCategoryWrapper;
import com.cube9.novafitness.wrapper.admin.SubAdminWrapper;

public interface AdminRepository {

	AdminInfo getAdminInfoByAdminEmail(String adminEmail);

	boolean updateLastAdminLoginByAdminId(Long adminId, LocalDateTime now);

	boolean adminResetPassword(@Valid AdminResetPasswordWrapper adminResetPasswordWrapper);

	boolean updateAdminProfileByAdminEmail(@Valid AdminProfileWrapper adminProfileWrapper);

	boolean adminChangePassword(@Valid AdminChangePasswordWrapper adminChangePasswordWrapper, Long adminId);

	Long saveAndGetAdminProfileImageId(String adminProfileImageName, String adminProfileImageStoredPath,
			String adminProfileImageViewPath, String adminProfileImageFileType);

	boolean updateAdminProfileImageIdByAdminId(Long adminId, Long adminProfileImageId);

	boolean updateAdminProfileImageInfoByAdminProfileImageId(Long adminProfileImageId, String adminProfileImageName,
			String adminProfileImageStoredPath, String adminProfileImageViewPath, String adminProfileImageFileType);

	AdminInfo getAdminInfoByAdminId(Long adminId);

	boolean updateAdminProfileImageInfoToNull(Long adminId);

	boolean deleteAdminProfileImageByAdminProfileImageId(Long adminProfileImageId);

	 List<SubAdminAccessRoleInfo> getSubAdminAccessRoleInfo(); 

	SubAdminAccessRoleInfo getSubAdminAccessRoleInfoBySubAdminId(Long subAdminId);

	List<TrainerInfo> getTrainerList();
	
	List<TrainerInfo> getListOfTrainer();
	
	List<TrainerCourseInfo> getCourseList();
	
	/* Check Review */
	
	List<UserCourseReviewInfo> getReviewList();
	
	List<UserInfo> getUserList();
	
	List<NewsLetterInfo> getNewsLetterList();
	
	List<UserInfo> getListOfUser();

	List<CategoryInfo> getMainCategoryList();
	
	

	List<SubCategoryInfo> getSubCategoryList();

	List<FAQInfo> getFAQList();

	CategoryInfo createNewMainCategory(CategoryInfo categoryInfo);
	
	SubAdminAccessRoleInfo createNewSubAdmin(SubAdminAccessRoleInfo subAdminInfo);
	
	boolean changeMainCategoryStatus(Long categoryId, boolean categoryStatus);

	CategoryInfo getCategoryInfoByCategoryId(Long categoryId);
	
	TrainerCourseInfo getTrainerCourseInfoByTrainerCourseId(Long trainerCourseId);
	
	

	boolean deleteCategoryByCategoryId(Long categoryId, boolean categoryDeleteStatus);
	
	boolean deleteTrainerCourseByTrainerCourseId(Long trainerCourseId, boolean trainerCourseDeleteStatus);

	
	
	boolean updateMainCategory(@Valid AdminCategoryWrapper adminCategoryWrapper);

	SubCategoryInfo getSubCategoryInfoBySubCategoryId(Long subcategoryId);

	SubCategoryInfo createNewSubCategory(SubCategoryInfo subCategoryInfo);

	boolean changeSubCategoryStatus(Long subcategoryId, boolean subcategoryStatus);

	boolean deleteSubCategoryBySubcategoryId(Long subcategoryId, boolean subcategoryStatus);
	
	boolean deleteSubSubAdminBySubAdminAccessRoleId(Long subAdminAccessRoleId,boolean subAdminStatus);

	boolean updateSubCategory(@Valid AdminSubCategoryWrapper adminSubCategoryWrapper);
	
	boolean updateSubAdmin(@Valid SubAdminWrapper subAdminWrapper);

	FAQInfo getFAQInfoByFAQId(Long faqId);
	
	SubAdminAccessRoleInfo getSubAdminInfoBySubAdminId(Long subAdminAccessRoleId);

	Long addNewFAQs(@Valid AdminFAQsWrapper adminFAQsWrapper);

	boolean changeFAQsStatus(Long faqsId, boolean faqsStatus);

	boolean deleteFAQs(Long faqsId, boolean faqsDeleteStatus);

	boolean updateFAQs(@Valid AdminFAQsWrapper adminFAQsWrapper);

	boolean changeUserStatus(Long userId, boolean userStatus);
	
	boolean changeNewsLetterStatus(Long newsLetterId, boolean newsLetterStatus);
	
	boolean changeSubAdminStatus(Long subAdminAccessRoleId, boolean subAdminStatus);
	
	boolean changeReviewStatus(Long reviewId, boolean reviewStatus); 

	boolean changeTrainerStatus(Long trainerId, boolean trainerStatus);

	boolean changeTrainerAdminApproveStatus(Long trainerId, boolean trainerAdminApproveStatus);

	List<UserCoursePaymentInfo> getUserCoursePaymentListByTrainerCourseId(Long trainerCourseId);

	List<UserCoursePaymentInfo> getUserCoursePaymentList();

	UserCoursePaymentInfo getUserCoursePaymentInfoByUserCoursePaymentId(Long userCoursePaymentId);

	Long getAllUserCount();

	Long getAllTrainerCount();

	Long getAllTrainerCourseCount();

	boolean changeTrainerCoursePaymentContributionStatus(Long userCoursePaymentId, boolean status);

	CourseContributionInfo getCourseContributionInfo();

	boolean updateCourseContributionInfoByCourseContributionId(CourseContributionInfo courseContributionInfo,
			String percentageContribution);

	CourseContributionInfo saveCourseContributionInfo(CourseContributionInfo courseContributionInfo1);

	List<CourseContributionInfo> getCourseContributionList();

}
