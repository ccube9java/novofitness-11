package com.cube9.novafitness.repository.admin;

import java.time.LocalDateTime;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cube9.novafitness.model.admin.AdminInfo;
import com.cube9.novafitness.model.admin.AdminProfileImageInfo;
import com.cube9.novafitness.model.admin.CategoryInfo;
import com.cube9.novafitness.model.admin.FAQInfo;
import com.cube9.novafitness.model.admin.SubAdminAccessRoleInfo;
import com.cube9.novafitness.model.admin.SubCategoryInfo;
import com.cube9.novafitness.model.course.CourseContributionInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseInfo;
import com.cube9.novafitness.model.trainer.TrainerInfo;
import com.cube9.novafitness.model.user.NewsLetterInfo;
import com.cube9.novafitness.model.user.UserCoursePaymentInfo;
import com.cube9.novafitness.model.user.UserCourseReviewInfo;
import com.cube9.novafitness.model.user.UserInfo;
import com.cube9.novafitness.wrapper.admin.AdminCategoryWrapper;
import com.cube9.novafitness.wrapper.admin.AdminChangePasswordWrapper;
import com.cube9.novafitness.wrapper.admin.AdminFAQsWrapper;
import com.cube9.novafitness.wrapper.admin.AdminProfileWrapper;
import com.cube9.novafitness.wrapper.admin.AdminResetPasswordWrapper;
import com.cube9.novafitness.wrapper.admin.AdminSubCategoryWrapper;
import com.cube9.novafitness.wrapper.admin.SubAdminWrapper;

/**
 * @author Vaibhav Deshmane
 */
@Repository
public class AdminRepositoryImp implements AdminRepository {

	@Autowired
	private SessionFactory sessionFactory;

	@Transactional
	@Override
	public AdminInfo getAdminInfoByAdminEmail(String adminEmail) {
		String hql = "from admin_info where adminEmail='" + adminEmail + "'";
		@SuppressWarnings("unchecked")
		Query<AdminInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<AdminInfo> adminInfos = query.list();
		return !adminInfos.isEmpty() ? adminInfos.get(0) : null;
	}

	@Transactional
	@Override
	public boolean updateLastAdminLoginByAdminId(Long adminId, LocalDateTime now) {
		Query<?> admin = sessionFactory.getCurrentSession()
				.createQuery("Update admin_info set adminLastLoginDateTime=:alldt " + "where adminId=:aid");
		admin.setParameter("alldt", now);
		admin.setParameter("aid", adminId);
		int adminStatus = admin.executeUpdate();
		return adminStatus > 0 ? true : false;
	}

	@Transactional
	@Override
	public boolean adminResetPassword(@Valid AdminResetPasswordWrapper adminResetPasswordWrapper) {
		@SuppressWarnings("rawtypes")
		Query q = sessionFactory.getCurrentSession()
				.createQuery("update admin_info set adminPassword=:p where adminEmail=:e");
		q.setParameter("p", adminResetPasswordWrapper.getAdminPassword());
		q.setParameter("e", adminResetPasswordWrapper.getAdminEmail());
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public boolean updateAdminProfileByAdminEmail(@Valid AdminProfileWrapper adminProfileWrapper) {
		Query<?> admin = sessionFactory.getCurrentSession().createQuery(
				"Update admin_info set adminFName=:fn, adminLName=:ln, adminPhoneNo=:pn "
				+ "where adminEmail=:ae");
		admin.setParameter("fn", adminProfileWrapper.getAdminFName());
		admin.setParameter("ln", adminProfileWrapper.getAdminLName());
		admin.setParameter("pn", Long.parseLong(adminProfileWrapper.getAdminPhoneNumber()));
		admin.setParameter("ae", adminProfileWrapper.getAdminEmail());
		int adminStatus = admin.executeUpdate();
		return adminStatus > 0 ? true : false;
	}

	@Transactional
	@Override
	public boolean adminChangePassword(@Valid AdminChangePasswordWrapper adminChangePasswordWrapper, Long adminId) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("update admin_info set adminPassword=:p where adminId=:i");
		q.setParameter("p", adminChangePasswordWrapper.getAdminPassword());
		q.setParameter("i", adminId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public Long saveAndGetAdminProfileImageId(String adminProfileImageName, String adminProfileImageStoredPath,
			String adminProfileImageViewPath, String adminProfileImageFileType) {
		AdminProfileImageInfo adminProfileImageInfo = new AdminProfileImageInfo();
		adminProfileImageInfo.setAdminProfileImageName(adminProfileImageName);
		adminProfileImageInfo.setAdminProfileImageStoredPath(adminProfileImageStoredPath);
		adminProfileImageInfo.setAdminProfileImageViewPath(adminProfileImageViewPath);
		adminProfileImageInfo.setAdminProfileImageFileType(adminProfileImageFileType);
		adminProfileImageInfo.setAdminProfileImageLastUpdatedDateTime(LocalDateTime.now());
		
		sessionFactory.getCurrentSession().save(adminProfileImageInfo);
		
		return adminProfileImageInfo.getAdminProfileImageId();
	}

	@Transactional
	@Override
	public boolean updateAdminProfileImageIdByAdminId(Long adminId, Long adminProfileImageId) {
		Query<?> q = sessionFactory.getCurrentSession().createQuery(
				"update admin_info set admin_profile_image_id=:apid where adminId=:aid");
		q.setParameter("apid", adminProfileImageId);
		q.setParameter("aid", adminId);
		int status1 = q.executeUpdate();
		return status1 > 0 ? true : false;
	}

	@Transactional
	@Override
	public boolean updateAdminProfileImageInfoByAdminProfileImageId(Long adminProfileImageId,
			String adminProfileImageName, String adminProfileImageStoredPath, String adminProfileImageViewPath,
			String adminProfileImageFileType) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("update admin_profile_image_info set adminProfileImageName=:apin, "
						+ "adminProfileImageStoredPath=:apisp, adminProfileImageViewPath=:apivp, "
						+ "adminProfileImageFileType=:apift, adminProfileImageLastUpdatedDateTime=:apiludt "
						+ "where adminProfileImageId=:apid");
		q.setParameter("apin", adminProfileImageName);
		q.setParameter("apisp", adminProfileImageStoredPath);
		q.setParameter("apivp", adminProfileImageViewPath);
		q.setParameter("apift", adminProfileImageFileType);
		q.setParameter("apiludt", LocalDateTime.now());
		q.setParameter("apid", adminProfileImageId);
		int status1 = q.executeUpdate();
		return status1 > 0 ? true : false;
	}

	@Transactional
	@Override
	public AdminInfo getAdminInfoByAdminId(Long adminId) {
		String hql = "from admin_info where adminId='" + adminId + "'";
		@SuppressWarnings("unchecked")
		Query<AdminInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<AdminInfo> adminInfos = query.list();
		return !adminInfos.isEmpty() ? adminInfos.get(0) : null;
	}

	@Transactional
	@Override
	public boolean updateAdminProfileImageInfoToNull(Long adminId) {
		@SuppressWarnings("rawtypes")
		Query q = sessionFactory.getCurrentSession()
				.createQuery("update admin_info set admin_profile_image_id=:apid "
						+ "where adminId=:aid");
		q.setParameter("apid", null);
		q.setParameter("aid", adminId);

		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public boolean deleteAdminProfileImageByAdminProfileImageId(Long adminProfileImageId) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("delete from admin_profile_image_info where adminProfileImageId=:apid");
		q.setParameter("apid", adminProfileImageId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	                      
	
	  @Transactional
	  @Override 
	  public List<SubAdminAccessRoleInfo> getSubAdminAccessRoleInfo() {
	  String hql = "from subAdmin_access_role_info";
	  
	  @SuppressWarnings("unchecked") 
	  Query<SubAdminAccessRoleInfo> query =sessionFactory.getCurrentSession().createQuery(hql);
	  List<SubAdminAccessRoleInfo> subAdminAccessRoleInfos = query.list(); 
	  return !subAdminAccessRoleInfos.isEmpty() ? subAdminAccessRoleInfos : null; 
	  }
	 
	 

	@Transactional
	@Override
	public SubAdminAccessRoleInfo getSubAdminAccessRoleInfoBySubAdminId(Long subAdminId) {
		String hql = "from subAdmin_access_role_info where subAdminAccessRoleId='" + subAdminId + "'";
		@SuppressWarnings("unchecked")
		Query<SubAdminAccessRoleInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<SubAdminAccessRoleInfo> subAdminAccessRoleInfos = query.list();
		return !subAdminAccessRoleInfos.isEmpty() ? subAdminAccessRoleInfos.get(0) : null;	
	}

	@Transactional
	@Override
	public List<TrainerInfo> getTrainerList() {
		String hql = "from trainer_info where trainerSAdminDeleteStatus=false order by trainerCurrentDateTime desc";
		@SuppressWarnings("unchecked")
		Query<TrainerInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<TrainerInfo> trainerInfos = query.list();
		return !trainerInfos.isEmpty() ? trainerInfos: null;	
	}
	
	@Transactional
	@Override
	public List<TrainerInfo> getListOfTrainer(){
		String hql = "from trainer_info where trainerSAdminDeleteStatus=false";
		@SuppressWarnings("unchecked")
		Query<TrainerInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<TrainerInfo> trainerListInfos = query.list();
		return !trainerListInfos.isEmpty() ? trainerListInfos: null;	
	}
	
	@Transactional
	@Override
	public List<TrainerCourseInfo> getCourseList() {
		String hql = "from trainer_course_info where trainerCourseIsDeletedByAdmin=false order by trainerCourseId desc";
		@SuppressWarnings("unchecked")
		Query<TrainerCourseInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<TrainerCourseInfo> trainerCourseInfos = query.list();
		return !trainerCourseInfos.isEmpty() ? trainerCourseInfos: null;	
	}
	
	/* Check Review */
	
	@Transactional
	@Override
	public List<UserCourseReviewInfo> getReviewList() {
		String hql = "from user_course_review_info where userCourseReviewIsDeletedByAdmin=false order by userCourseReviewId desc";
		@SuppressWarnings("unchecked")
		Query<UserCourseReviewInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<UserCourseReviewInfo> courseReview = query.list();
		return !courseReview.isEmpty() ? courseReview: null;	
	}
	

	@Transactional
	@Override
	public List<UserInfo> getUserList() {
		String hql = "from user_info where userIsDeletedByAdmin=false order by userId desc";
		@SuppressWarnings("unchecked")
		Query<UserInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<UserInfo> userInfos = query.list();
		return !userInfos.isEmpty() ? userInfos: null;	
	}
	
	@Transactional
	@Override
	public List<NewsLetterInfo> getNewsLetterList(){
		String hql = "from news_letter_info where newsLetterUserIsDeletedByAdmin=false order by newsLetterId desc";
		@SuppressWarnings("unchecked")
		Query<NewsLetterInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<NewsLetterInfo> userNewsLetterInfos = query.list();
		return !userNewsLetterInfos.isEmpty() ? userNewsLetterInfos: null;	
	}

	@Transactional
	@Override
	public List<UserInfo> getListOfUser(){
		String hql = "from user_info where userIsDeletedByAdmin=false";
		@SuppressWarnings("unchecked")
		Query<UserInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<UserInfo> listOfUser = query.list();
		return !listOfUser.isEmpty() ? listOfUser: null;	
	}
	
	@Transactional
	@Override
	public List<CategoryInfo> getMainCategoryList() {
		String hql = "from category_info where categoryIsDeletedByAdmin=false order by categoryCreatedDateTime desc";
		@SuppressWarnings("unchecked")
		Query<CategoryInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<CategoryInfo> categoryInfos = query.list();
		return !categoryInfos.isEmpty() ? categoryInfos: null;	
	}
	
	

	@Transactional
	@Override
	public List<SubCategoryInfo> getSubCategoryList() {
		String hql = "from subcategory_info where subcategoryIsDeletedByAdmin=false order by subcategoryCreatedDateTime desc";
		@SuppressWarnings("unchecked")
		Query<SubCategoryInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<SubCategoryInfo> subCategoryInfos = query.list();
		return !subCategoryInfos.isEmpty() ? subCategoryInfos: null;	
	}

	@Transactional
	@Override
	public List<FAQInfo> getFAQList() {
		String hql = "from faq_info where faqIsDeletedByAdmin=false order by faqCreatedDateTime desc";
		@SuppressWarnings("unchecked")
		Query<FAQInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<FAQInfo> faqInfos = query.list();
		return !faqInfos.isEmpty() ? faqInfos: null;
	}

	@Transactional
	@Override
	public CategoryInfo createNewMainCategory(CategoryInfo categoryInfo) {
		sessionFactory.getCurrentSession().save(categoryInfo);
		return categoryInfo;
	}

	@Transactional
	@Override
	public SubAdminAccessRoleInfo createNewSubAdmin(SubAdminAccessRoleInfo subAdminInfo){
		sessionFactory.getCurrentSession().save(subAdminInfo);
		return subAdminInfo;
	}
	
	
	@Transactional
	@Override
	public boolean changeMainCategoryStatus(Long categoryId, boolean categoryStatus) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("update category_info set categoryIsActive=:s where categoryId=:i");
		q.setParameter("s", !categoryStatus);
		q.setParameter("i", categoryId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public CategoryInfo getCategoryInfoByCategoryId(Long categoryId) {
		String hql = "from category_info where categoryId = '" + categoryId + "'";
		@SuppressWarnings("unchecked")
		Query<CategoryInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<CategoryInfo> categoryInfos = query.list();
		return !categoryInfos.isEmpty() ? categoryInfos.get(0) : null;
	}
	
	@Transactional
	@Override
	public TrainerCourseInfo getTrainerCourseInfoByTrainerCourseId(Long trainerCourseId) {
		String hql = "from trainer_course_info where trainerCourseId = '" + trainerCourseId + "'";
		@SuppressWarnings("unchecked")
		Query<TrainerCourseInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<TrainerCourseInfo> trainerCourseInfos = query.list();
		return !trainerCourseInfos.isEmpty() ? trainerCourseInfos.get(0) : null;
	}
	
	
	@Transactional
	@Override
	public boolean deleteCategoryByCategoryId(Long categoryId, boolean categoryDeleteStatus) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("update category_info set categoryIsDeletedByAdmin=:s where categoryId=:i");
		q.setParameter("s", categoryDeleteStatus);
		q.setParameter("i", categoryId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}
	
	@Transactional
	@Override
	public boolean deleteTrainerCourseByTrainerCourseId(Long trainerCourseId, boolean trainerCourseDeleteStatus) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("update trainer_course_info set trainerCourseIsDeletedByAdmin=:s where trainerCourseId=:i");
		q.setParameter("s", trainerCourseDeleteStatus);
		q.setParameter("i", trainerCourseId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	
	
	@Transactional
	@Override
	public boolean updateMainCategory(@Valid AdminCategoryWrapper adminCategoryWrapper) {
		Query<?> category = sessionFactory.getCurrentSession()
				.createQuery("Update category_info set categoryName=:n, categoryDescription=:d where categoryId=:i");
		category.setParameter("n", adminCategoryWrapper.getCategoryName());
		category.setParameter("d", adminCategoryWrapper.getCategoryDescription());
		category.setParameter("i", adminCategoryWrapper.getCategoryId());
		int categoryStatus = category.executeUpdate();
		return categoryStatus > 0 ? true : false;
	}

	@Transactional
	@Override
	public SubCategoryInfo getSubCategoryInfoBySubCategoryId(Long subcategoryId) {
		String hql = "from subcategory_info where subcategoryId = '" + subcategoryId + "'";
		@SuppressWarnings("unchecked")
		Query<SubCategoryInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<SubCategoryInfo> subCategoryInfos = query.list();
		return !subCategoryInfos.isEmpty() ? subCategoryInfos.get(0) : null;
	}

	@Transactional
	@Override
	public SubCategoryInfo createNewSubCategory(SubCategoryInfo subCategoryInfo) {
		sessionFactory.getCurrentSession().save(subCategoryInfo);
		return subCategoryInfo;
	}

	@Transactional
	@Override
	public boolean changeSubCategoryStatus(Long subcategoryId, boolean subcategoryStatus) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("update subcategory_info set subcategoryIsActive=:s where subcategoryId=:i");
		q.setParameter("s", !subcategoryStatus);
		q.setParameter("i", subcategoryId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public boolean deleteSubCategoryBySubcategoryId(Long subcategoryId, boolean subcategoryStatus) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("update subcategory_info set subcategoryIsDeletedByAdmin=:s where subcategoryId=:i");
		q.setParameter("s", subcategoryStatus);
		q.setParameter("i", subcategoryId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	
	@Transactional
	@Override
	public boolean deleteSubSubAdminBySubAdminAccessRoleId(Long subAdminAccessRoleId,boolean subAdminStatus){
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("update subAdmin_access_role_info set subAdminDeleteStatus=:s where subAdminAccessRoleId=:i");
		q.setParameter("s", subAdminStatus);
		q.setParameter("i", subAdminAccessRoleId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}
	
	@Transactional
	@Override
	public boolean updateSubCategory(@Valid AdminSubCategoryWrapper adminSubCategoryWrapper) {
		Query<?> category = sessionFactory.getCurrentSession()
				.createQuery("Update subcategory_info set subcategoryName=:n, subcategoryDescription=:d, "
								+ "main_category_id=:mcid where subcategoryId=:i");
		category.setParameter("n", adminSubCategoryWrapper.getSubCategoryName());
		category.setParameter("d", adminSubCategoryWrapper.getSubCategoryDescription());
		category.setParameter("mcid", adminSubCategoryWrapper.getMainCategoryId());
		category.setParameter("i", adminSubCategoryWrapper.getSubcategoryId());
		int categoryStatus = category.executeUpdate();
		return categoryStatus > 0 ? true : false;
	}
	
	
	
	
	
	
	
	@Transactional
	@Override
	public boolean updateSubAdmin(@Valid SubAdminWrapper subAdminWrapper){
		Query<?> subAdmin = sessionFactory.getCurrentSession()
				.createQuery("Update subAdmin_access_role_info set subAdminAccessForUserManagement=:us, "
						+ "subAdminAccessForTrainerManagement=:tr, subAdminAccessForCourseManagement=:crs"
						+ "subAdminAccessForCategoryManagement=:cat, subAdminAccessForContentManagement=:cn"
						+ "subAdminAccessForFAQsManagement=:faq, subAdminAccessForGeneralManagement=:ge"
						+ "subAdminAccessForReviewsRatingsManagement=:re, subAdminAccessForNewsletterManagement=:nw"
						+ "subAdminAccessForPaymentManagement=:pay, subAdminAccessForReportsManagement=:rp"
						+ "subAdminAccessForSubAdminManagement=:sub"
								+ " where subAdminAccessRoleId=:id");
		
		
		
		subAdmin.setParameter("cn",subAdminWrapper.isSubAdminAccessForContentManagement());
		subAdmin.setParameter("cat",subAdminWrapper.isSubAdminAccessForCategoryManagement());
		subAdmin.setParameter("us",subAdminWrapper.isSubAdminAccessForUserManagement());
		subAdmin.setParameter("nw",subAdminWrapper.isSubAdminAccessForNewsletterManagement());
		subAdmin.setParameter("re",subAdminWrapper.isSubAdminAccessForReviewsRatingsManagement());
		subAdmin.setParameter("pay",subAdminWrapper.isSubAdminAccessForPaymentManagement());
		subAdmin.setParameter("faq",subAdminWrapper.isSubAdminAccessForFAQsManagement());
		subAdmin.setParameter("sub",subAdminWrapper.isSubAdminAccessForSubAdminManagement());
		subAdmin.setParameter("crs",subAdminWrapper.isSubAdminAccessForCourseManagement());
		subAdmin.setParameter("tr",subAdminWrapper.isSubAdminAccessForTrainerManagement());
		subAdmin.setParameter("ge",subAdminWrapper.isSubAdminAccessForGeneralManagement());
		subAdmin.setParameter("rp",subAdminWrapper.isSubAdminAccessForReportsManagement());
		
		subAdmin.setParameter("id", subAdminWrapper.getSubAdminAccessRoleId());
		
		int subAdminStatus = subAdmin.executeUpdate();
		return subAdminStatus > 0 ? true : false;
	}
	
	
	
	
	
	
	@Transactional
	@Override
	public Long addNewFAQs(@Valid AdminFAQsWrapper adminFAQsWrapper) {
		FAQInfo faqInfo = new FAQInfo();
		faqInfo.setFaqQuestion(adminFAQsWrapper.getFaqsQuestion());
		faqInfo.setFaqAnswer(adminFAQsWrapper.getFaqsAnswer());

		sessionFactory.getCurrentSession().save(faqInfo);
		return faqInfo.getFaqId();
	}

	@Transactional
	@Override
	public boolean changeFAQsStatus(Long faqsId, boolean faqsStatus) {
		Query<?> q = sessionFactory.getCurrentSession().createQuery("update faq_info set faqIsActive=:s where faqId=:i");
		q.setParameter("s", !faqsStatus);
		q.setParameter("i", faqsId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public boolean deleteFAQs(Long faqsId, boolean faqsDeleteStatus) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("update faq_info set faqIsDeletedByAdmin=:s where faqId=:i");
		q.setParameter("s", faqsDeleteStatus);
		q.setParameter("i", faqsId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public FAQInfo getFAQInfoByFAQId(Long faqId) {
		String hql = "from faq_info where faqId='" + faqId + "'";
		@SuppressWarnings("unchecked")
		Query<FAQInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<FAQInfo> faqInfos = query.list();
		return !faqInfos.isEmpty() ? faqInfos.get(0) : null;
	}
	
	@Transactional
	@Override
	public SubAdminAccessRoleInfo getSubAdminInfoBySubAdminId(Long subAdminAccessRoleId){
		String hql = "from  subAdmin_access_role_info where subAdminAccessRoleId='" + subAdminAccessRoleId + "'";
		@SuppressWarnings("unchecked")
		Query<SubAdminAccessRoleInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<SubAdminAccessRoleInfo> subAdminInfos = query.list();
		return !subAdminInfos.isEmpty() ? subAdminInfos.get(0) : null;
	}	


	@Transactional
	@Override
	public boolean updateFAQs(@Valid AdminFAQsWrapper adminFAQsWrapper) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("update faq_info set faqQuestion=:fq, faqAnswer=:fa where faqId=:fid");
		q.setParameter("fq", adminFAQsWrapper.getFaqsQuestion());
		q.setParameter("fa", adminFAQsWrapper.getFaqsAnswer());
		q.setParameter("fid", adminFAQsWrapper.getFaqId());
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}
	
	@Transactional
	@Override
	public boolean changeUserStatus(Long userId, boolean userStatus) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("update user_info set userIsActive=:s where userId=:i");
		q.setParameter("s", !userStatus);
		q.setParameter("i", userId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public boolean changeNewsLetterStatus(Long newsLetterId, boolean newsLetterStatus){
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("update news_letter_info set newsLetterUserIsActive=:s where newsLetterId=:i");
		q.setParameter("s", !newsLetterStatus);
		q.setParameter("i", newsLetterId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}
	
	
	
	@Transactional
	@Override
	public boolean changeSubAdminStatus(Long subAdminAccessRoleId, boolean subAdminStatus){
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("update subAdmin_access_role_info set subAdminStatus=:s where subAdminAccessRoleId=:i");
		q.setParameter("s", !subAdminStatus);
		q.setParameter("i", subAdminAccessRoleId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}
	
	
	
	@Transactional
	@Override
	public boolean changeReviewStatus(Long reviewId, boolean reviewStatus){
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("update user_review_info set reviewIsActive=:s where reviewId=:i");
		q.setParameter("s", !reviewStatus);
		q.setParameter("i", reviewId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}
	
	@Transactional
	@Override
	public boolean changeTrainerStatus(Long trainerId, boolean trainerStatus) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("update trainer_info set trainerStatus=:s where trainerId=:i");
		q.setParameter("s", !trainerStatus);
		q.setParameter("i", trainerId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public boolean changeTrainerAdminApproveStatus(Long trainerId, boolean trainerAdminApproveStatus) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("update trainer_info set trainerIsApprovedByAdmin=:s where trainerId=:i");
		q.setParameter("s", !trainerAdminApproveStatus);
		q.setParameter("i", trainerId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public List<UserCoursePaymentInfo> getUserCoursePaymentListByTrainerCourseId(Long trainerCourseId) {
		String hql = "from user_course_payment_info where trainer_course_id='" + trainerCourseId + "' ";
		@SuppressWarnings("unchecked")
		Query<UserCoursePaymentInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<UserCoursePaymentInfo> userCoursePaymentInfos = query.list();
		return !userCoursePaymentInfos.isEmpty() ? userCoursePaymentInfos : null;
	}
	
	@Transactional
	@Override
	public List<UserCoursePaymentInfo> getUserCoursePaymentList() {
		String hql = "from user_course_payment_info where userCoursePaymentIsDeletedByAdmin=false order by userCoursePaymentId desc";
		@SuppressWarnings("unchecked")
		Query<UserCoursePaymentInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<UserCoursePaymentInfo> userCoursePaymentInfos = query.list();
		return !userCoursePaymentInfos.isEmpty() ? userCoursePaymentInfos : null;
	}

	@Transactional
	@Override
	public UserCoursePaymentInfo getUserCoursePaymentInfoByUserCoursePaymentId(Long userCoursePaymentId) {
		String hql = "from user_course_payment_info where userCoursePaymentId='"+ userCoursePaymentId +"' and userCoursePaymentIsDeletedByAdmin=false ";
		@SuppressWarnings("unchecked")
		Query<UserCoursePaymentInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<UserCoursePaymentInfo> userCoursePaymentInfos = query.list();
		return !userCoursePaymentInfos.isEmpty() ? userCoursePaymentInfos.get(0) : null;
	}

	@Transactional
	@Override
	public Long getAllUserCount() {
		return (Long) sessionFactory.getCurrentSession().createQuery("select count(*) from user_info where userIsDeletedByAdmin=false")
				.getSingleResult();
	}

	@Transactional
	@Override
	public Long getAllTrainerCount() {
		return (Long) sessionFactory.getCurrentSession().createQuery("select count(*) from trainer_info where trainerSAdminDeleteStatus=false")
				.getSingleResult();
	}

	@Transactional
	@Override
	public Long getAllTrainerCourseCount() {
		return (Long) sessionFactory.getCurrentSession().createQuery("select count(*) from trainer_course_info where trainerCourseIsDeletedByAdmin=false")
				.getSingleResult();
	}

	@Transactional
	@Override
	public boolean changeTrainerCoursePaymentContributionStatus(Long userCoursePaymentId, boolean paymentStatus) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("update user_course_payment_info set userCoursePaymentIsAdminPaidTrainerContrubution=:s where userCoursePaymentId=:i");
		q.setParameter("s", paymentStatus);
		q.setParameter("i", userCoursePaymentId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public CourseContributionInfo getCourseContributionInfo() {
		String hql = "from course_contribution_info";
		@SuppressWarnings("unchecked")
		Query<CourseContributionInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<CourseContributionInfo> contributionInfos = query.list();
		return !contributionInfos.isEmpty() ? contributionInfos.get(0) : null;
	}

	@Transactional
	@Override
	public boolean updateCourseContributionInfoByCourseContributionId(CourseContributionInfo courseContributionInfo,
			String percentageContribution) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("update course_contribution_info set courseContributionPercentage=:s where courseContributionId=:i");
		q.setParameter("s", Double.parseDouble(percentageContribution));
		q.setParameter("i", courseContributionInfo.getCourseContributionId());
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public CourseContributionInfo saveCourseContributionInfo(CourseContributionInfo courseContributionInfo1) {
		sessionFactory.getCurrentSession().save(courseContributionInfo1);
		return courseContributionInfo1;
	}

	@Transactional
	@Override
	public List<CourseContributionInfo> getCourseContributionList() {
		String hql = "from course_contribution_info";
		@SuppressWarnings("unchecked")
		Query<CourseContributionInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<CourseContributionInfo> contributionInfos = query.list();
		return !contributionInfos.isEmpty() ? contributionInfos : null;
	}
	
}