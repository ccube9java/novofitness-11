package com.cube9.novafitness.repository.user;

import java.time.LocalDateTime;
import java.util.List;

import javax.validation.Valid;

import com.cube9.novafitness.model.admin.CategoryInfo;
import com.cube9.novafitness.model.admin.CouponCodeInfo;
import com.cube9.novafitness.model.course.CourseInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseInfo;
import com.cube9.novafitness.model.user.CartTrainerCourseUserInfo;
import com.cube9.novafitness.model.user.FavouriteTrainerCourseUserInfo;
import com.cube9.novafitness.model.user.GenderInfo;
import com.cube9.novafitness.model.user.NewsLetterInfo;
import com.cube9.novafitness.model.user.TrainerCourseUserQuestionAnswerInfo;
import com.cube9.novafitness.model.user.TrainerCourseUserQuestionInfo;
import com.cube9.novafitness.model.user.UserCoursePaymentInfo;
import com.cube9.novafitness.model.user.UserCourseReviewInfo;
import com.cube9.novafitness.model.user.UserInfo;
import com.cube9.novafitness.model.user.UserOrderInfo;
import com.cube9.novafitness.wrapper.user.UserProfileWrapper;
import com.cube9.novafitness.wrapper.user.UserResetPasswordWrapper;

public interface UserRepository {

	boolean duplicateEmailCheck(String userEmail);

	UserInfo saveUserInfo(UserInfo userInfo);

	boolean userVerification(String decodedEmail);

	UserInfo getUserInfoByUserEmail(String userEmail);

	boolean updateLastUserLoginByUserId(Long userId, LocalDateTime now);

	boolean userResetPassword(@Valid UserResetPasswordWrapper userResetPasswordWrapper,
			String generatedSecuredPasswordHash);

	boolean updateUserProfileByUserEmail(@Valid UserProfileWrapper userProfileWrapper);

	boolean userChangePasswordByUserId(String generatedSecuredPasswordHash, Long userId);

	boolean updateUserProfileImageInfoByUserProfileImageId(Long userProfileImageId, String userProfileImageName,
			String userProfileImageStoredPath, String userProfileImageViewPath, String userProfileImageFileType);

	Long saveAndGetUserProfileImageId(String userProfileImageName, String userProfileImageStoredPath,
			String userProfileImageViewPath, String userProfileImageFileType);

	boolean updateUserProfileImageIdByUserId(Long userId, Long userProfileImageId);

	UserInfo getUserInfoByUserId(Long userId);
	
	
	
	UserCourseReviewInfo getReviewInfoByReviewId(Long reviewId);

	boolean updateUserProfileImageInfoToNull(Long userId);

	boolean deleteUserProfileImageByUserProfileImageId(Long userProfileImageId);

	List<GenderInfo> getGenderList();

	List<CategoryInfo> getActiveMainCategoryList();

	List<TrainerCourseInfo> getAllActiveCourseList();

	List<CourseInfo> getActiveCourseListByCategoryId(long categoryId);

	List<TrainerCourseInfo> getCourseListByCourseIds(List<Long> courseIds);

	FavouriteTrainerCourseUserInfo getFavouriteListUserInfoByUserIdAndTrainerCourseId(Long userId,
			Long trainerCourseId);

	boolean updateUserFavListStatus(Long favouriteTrainerCourseUserId, boolean favouriteTrainerCourseUserIsActive);

	Long addTrainerCourseToFavouriteList(FavouriteTrainerCourseUserInfo favouriteTrainerCourseUserInfo2);

	List<FavouriteTrainerCourseUserInfo> getFavouriteTrainerCourseUserInfobyUserId(Long userId);

	TrainerCourseUserQuestionInfo getTrainerCourseUserQuestionInfoByTrainerCourseId(Long trainerCourseId);

	List<TrainerCourseUserQuestionAnswerInfo> getTrainerCourseUserQuestionAnswerListByTrainerCourseUserQuestionId(
			Long trainerCourseUserQuestionId);

	TrainerCourseUserQuestionInfo saveTrainerCourseUserQuestionInfo(
			TrainerCourseUserQuestionInfo trainerCourseUserQuestionInfo);

	TrainerCourseUserQuestionAnswerInfo saveTrainerCourseUserQuestionAnswerInfo(
			TrainerCourseUserQuestionAnswerInfo trainerCourseUserQuestionAnswerInfo);

	boolean updateUserCartListStatus(Long cartTrainerCourseUserId, boolean b);

	CartTrainerCourseUserInfo getCartListUserInfoByUserIdAndTrainerCourseId(Long userId, Long trainerCourseId);

	Long addTrainerCourseToCartList(CartTrainerCourseUserInfo cartTrainerCourseUserInfo2);

	List<CartTrainerCourseUserInfo> getCartTrainerCourseUserInfobyUserId(Long userId);

	boolean removeFromCart(Long cartTrainerCourseUserId);

	CouponCodeInfo couponCodeCheckPayment(String couponCodeValue);

	CouponCodeInfo getCouponCodeInfoByCouponCodeId(Long couponCodeId);

	UserOrderInfo saveUserOder(UserOrderInfo userOrderInfo);

	UserOrderInfo getUserOrderInfoByUserOrderNumber(String userOrderNumber);

	List<UserCoursePaymentInfo> getUserCoursePaymentInfoByUserOderId(Long userOrderId);

	void saveUserCoursePaymentInfo(UserCoursePaymentInfo userCoursePaymentInfo);

	List<UserCoursePaymentInfo> getUserCoursePaymentListByUserId(Long userId);

	Long saveUserCourseReviewByUser(UserCourseReviewInfo userCourseReviewInfo);

	List<UserCourseReviewInfo> getUserCourseReviewByTrainerCourseId(Long trainerCourseId);

	NewsLetterInfo getNewsLetterInfoByUserEmail(String userEmail);

	Long saveNewsLetterSubscription(NewsLetterInfo newsLetterInfo);

	boolean updateNewsLetterInfo(Long newsLetterId, boolean b, String userName);

	UserCoursePaymentInfo getUserCoursePaymentInfoByTrainerCourseIdAndUserId(Long trainerCourseId, Long userId);

}
