package com.cube9.novafitness.repository.user;

import java.time.LocalDateTime;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cube9.novafitness.model.admin.CategoryInfo;
import com.cube9.novafitness.model.admin.CouponCodeInfo;
import com.cube9.novafitness.model.course.CourseInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseInfo;
import com.cube9.novafitness.model.user.CartTrainerCourseUserInfo;
import com.cube9.novafitness.model.user.FavouriteTrainerCourseUserInfo;
import com.cube9.novafitness.model.user.GenderInfo;
import com.cube9.novafitness.model.user.NewsLetterInfo;
import com.cube9.novafitness.model.user.TrainerCourseUserQuestionAnswerInfo;
import com.cube9.novafitness.model.user.TrainerCourseUserQuestionInfo;
import com.cube9.novafitness.model.user.UserCoursePaymentInfo;
import com.cube9.novafitness.model.user.UserCourseReviewInfo;
import com.cube9.novafitness.model.user.UserInfo;
import com.cube9.novafitness.model.user.UserOrderInfo;
import com.cube9.novafitness.model.user.UserProfileImageInfo;
import com.cube9.novafitness.wrapper.user.UserProfileWrapper;
import com.cube9.novafitness.wrapper.user.UserResetPasswordWrapper;

/**
 * @author Vaibhav Deshmane
 */
@Repository
public class UserRepositoryImp implements UserRepository {

	@Autowired
	private SessionFactory sessionFactory;

	@Transactional
	@Override
	public boolean duplicateEmailCheck(String userEmail) {
		@SuppressWarnings("unchecked")
		Query<UserInfo> query = sessionFactory.getCurrentSession()
				.createQuery("from  user_info WHERE userEmail ='" + userEmail + "'");
		List<UserInfo> userInfos = query.list();
		return !userInfos.isEmpty() ? true : false;
	}

	@Transactional
	@Override
	public UserInfo saveUserInfo(UserInfo userInfo) {
		sessionFactory.getCurrentSession().save(userInfo);
		return userInfo != null ? userInfo : null;
	}

	@Transactional
	@Override
	public boolean userVerification(String userEmail) {
		@SuppressWarnings("rawtypes")
		Query q = sessionFactory.getCurrentSession()
				.createQuery("update user_info set userIsActive=:s where userEmail=:e");
		q.setParameter("s", true);
		q.setParameter("e", userEmail);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public UserInfo getUserInfoByUserEmail(String userEmail) {
		String hql = "from  user_info where userEmail='" + userEmail + "'";
		@SuppressWarnings("unchecked")
		Query<UserInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<UserInfo> userInfos = query.list();
		return !userInfos.isEmpty() ? userInfos.get(0) : null;
	}

	@Transactional
	@Override
	public boolean updateLastUserLoginByUserId(Long userId, LocalDateTime now) {
		Query<?> User = sessionFactory.getCurrentSession()
				.createQuery("update user_info set userLastLoginDateTime=:ulldt " + "where userId=:uid");
		User.setParameter("ulldt", now);
		User.setParameter("uid", userId);
		int tainerStatus = User.executeUpdate();
		return tainerStatus > 0 ? true : false;
	}

	@Transactional
	@Override
	public boolean userResetPassword(@Valid UserResetPasswordWrapper UserResetPasswordWrapper,
			String generatedSecuredPasswordHash) {
		@SuppressWarnings("rawtypes")
		Query q = sessionFactory.getCurrentSession()
				.createQuery("update user_info set userPassword =:p where userEmail=:e");
		q.setParameter("p", generatedSecuredPasswordHash);
		q.setParameter("e", UserResetPasswordWrapper.getUserEmail());
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public boolean updateUserProfileByUserEmail(@Valid UserProfileWrapper userProfileWrapper) {
		Query<?> admin = sessionFactory.getCurrentSession()
				.createQuery("update user_info set userFName=:fn, userLName=:ln, userLocation=:ul, userCity=:uc, userState=:us, userCountry=:ucon, gender_id=:gid " 
						+ "where userEmail=:ue");
		admin.setParameter("fn", userProfileWrapper.getUserFName());
		admin.setParameter("ln", userProfileWrapper.getUserLName());
		admin.setParameter("ul", userProfileWrapper.getUserLocation());
		admin.setParameter("uc", userProfileWrapper.getUserCity());
		admin.setParameter("us", userProfileWrapper.getUserState());
		admin.setParameter("ucon", userProfileWrapper.getUserCountry());
		admin.setParameter("gid", userProfileWrapper.getGenderId());
		admin.setParameter("ue", userProfileWrapper.getUserEmail());
		int status = admin.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public boolean userChangePasswordByUserId(String generatedSecuredPasswordHash, Long userId) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("update user_info set userPassword =:p where userId=:i");
		q.setParameter("p", generatedSecuredPasswordHash);
		q.setParameter("i", userId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public Long saveAndGetUserProfileImageId(String UserProfileImageName, String UserProfileImageStoredPath,
			String UserProfileImageViewPath, String UserProfileImageFileType) {
		UserProfileImageInfo userProfileImageInfo = new UserProfileImageInfo();
		userProfileImageInfo.setUserProfileImageName(UserProfileImageName);
		userProfileImageInfo.setUserProfileImageStoredPath(UserProfileImageStoredPath);
		userProfileImageInfo.setUserProfileImageViewPath(UserProfileImageViewPath);
		userProfileImageInfo.setUserProfileImageFileType(UserProfileImageFileType);

		sessionFactory.getCurrentSession().save(userProfileImageInfo);

		return userProfileImageInfo.getUserProfileImageId();
	}

	@Transactional
	@Override
	public boolean updateUserProfileImageIdByUserId(Long userId, Long userProfileImageId) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("update user_info set user_profile_image_id=:upid where userId=:uid");
		q.setParameter("upid", userProfileImageId);
		q.setParameter("uid", userId);
		int status1 = q.executeUpdate();
		return status1 > 0 ? true : false;
	}

	@Transactional
	@Override
	public boolean updateUserProfileImageInfoByUserProfileImageId(Long userProfileImageId, String userProfileImageName,
			String userProfileImageStoredPath, String userProfileImageViewPath, String userProfileImageFileType) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("update user_profile_image_info set userProfileImageName=:upin, "
						+ "userProfileImageStoredPath=:upisp, userProfileImageViewPath=:upivp, "
						+ "userProfileImageFileType=:upift, userProfileImageLastUpdatedDateTime=:upiludt "
						+ "where userProfileImageId=:upid");
		q.setParameter("upin", userProfileImageName);
		q.setParameter("upisp", userProfileImageStoredPath);
		q.setParameter("upivp", userProfileImageViewPath);
		q.setParameter("upift", userProfileImageFileType);
		q.setParameter("upiludt", LocalDateTime.now());
		q.setParameter("upid", userProfileImageId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public UserInfo getUserInfoByUserId(Long userId) {
		String hql = "from  user_info where userId='" + userId + "'";
		@SuppressWarnings("unchecked")
		Query<UserInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<UserInfo> userInfos = query.list();
		return !userInfos.isEmpty() ? userInfos.get(0) : null;
	}
	
	@Transactional
	@Override
	public UserCourseReviewInfo getReviewInfoByReviewId(Long reviewId) {
		String hql = "from user_course_review_info where userCourseReviewId='" + reviewId + "'";
		@SuppressWarnings("unchecked")
		Query<UserCourseReviewInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<UserCourseReviewInfo> reviewInfos = query.list();
		return !reviewInfos.isEmpty() ? reviewInfos.get(0) : null;
	}
	
	@Transactional
	@Override
	public boolean updateUserProfileImageInfoToNull(Long userId) {
		@SuppressWarnings("rawtypes")
		Query q = sessionFactory.getCurrentSession()
				.createQuery("update user_info set user_profile_image_id=:upid " 
								+ "where userId=:uid");
		q.setParameter("upid", null);
		q.setParameter("uid", userId);

		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public boolean deleteUserProfileImageByUserProfileImageId(Long userProfileImageId) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("delete from user_profile_image_info where userProfileImageId=:upid");
		q.setParameter("upid", userProfileImageId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}
	
	@Transactional
	@Override
	public List<GenderInfo> getGenderList() {
		String hql = "from  gender_info where genderIsActive=true and genderIsDeletedByAdmin=false";
		@SuppressWarnings("unchecked")
		Query<GenderInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<GenderInfo> genderInfos = query.list();
		return !genderInfos.isEmpty() ? genderInfos : null;
	}

	@Transactional
	@Override
	public List<CategoryInfo> getActiveMainCategoryList() {
		String hql = "from category_info where categoryIsDeletedByAdmin=false and categoryIsActive=true order by categoryCreatedDateTime desc";
		@SuppressWarnings("unchecked")
		Query<CategoryInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<CategoryInfo> categoryInfos = query.list();
		return !categoryInfos.isEmpty() ? categoryInfos: null;	
	}

	@Transactional
	@Override
	public List<TrainerCourseInfo> getAllActiveCourseList() {
		String hql = "from trainer_course_info where trainerCourseIsDeletedByAdmin=false order by trainerCourseCreatedDateTime desc";
		@SuppressWarnings("unchecked")
		Query<TrainerCourseInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<TrainerCourseInfo> trainerCourseInfos = query.list();
		return !trainerCourseInfos.isEmpty() ? trainerCourseInfos: null;
	}

	@Transactional
	@Override
	public List<CourseInfo> getActiveCourseListByCategoryId(long categoryId) {
		String hql = "from course_info where courseIsDeletedByAdmin=false and courseIsActive=true and category_id='"+ categoryId +"'";
		@SuppressWarnings("unchecked")
		Query<CourseInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<CourseInfo> courseInfos = query.list();
		return !courseInfos.isEmpty() ? courseInfos: null;
	}

	@Transactional
	@Override
	public List<TrainerCourseInfo> getCourseListByCourseIds(List<Long> courseIds) {
		String hql = "from trainer_course_info where course_id in ("
				+ courseIds.toString().replace("[", "").replace("]", "") + ") "
				+ "and trainerCourseIsDeletedByAdmin=false and trainerCourseIsActive=true";
		@SuppressWarnings("unchecked")
		Query<TrainerCourseInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<TrainerCourseInfo> trainerCourseInfos = query.list();
		return !trainerCourseInfos.isEmpty() ? trainerCourseInfos : null;
	}

	@Transactional
	@Override
	public FavouriteTrainerCourseUserInfo getFavouriteListUserInfoByUserIdAndTrainerCourseId(Long userId,
			Long trainerCourseId) {
		String hql = "from favourite_trainer_course_user_info where trainer_course_id='" + trainerCourseId + "' and user_id='"
				+ userId + "'";
		@SuppressWarnings("unchecked")
		Query<FavouriteTrainerCourseUserInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<FavouriteTrainerCourseUserInfo> favouriteTrainerCourseUserInfos = query.list();
		return !favouriteTrainerCourseUserInfos.isEmpty() ? favouriteTrainerCourseUserInfos.get(0) : null;
	}

	@Transactional
	@Override
	public boolean updateUserFavListStatus(Long favouriteTrainerCourseUserId,
			boolean favouriteTrainerCourseUserIsActive) {
		@SuppressWarnings("rawtypes")
		Query q = sessionFactory.getCurrentSession()
				.createQuery("update favourite_trainer_course_user_info set favouriteTrainerCourseUserIsActive=:ftcuia " 
								+ "where favouriteTrainerCourseUserId=:ftcuid");
		q.setParameter("ftcuia", favouriteTrainerCourseUserIsActive);
		q.setParameter("ftcuid", favouriteTrainerCourseUserId);

		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public Long addTrainerCourseToFavouriteList(FavouriteTrainerCourseUserInfo favouriteTrainerCourseUserInfo2) {
		sessionFactory.getCurrentSession().save(favouriteTrainerCourseUserInfo2);
		return favouriteTrainerCourseUserInfo2.getFavouriteTrainerCourseUserId();
	}

	@Transactional
	@Override
	public List<FavouriteTrainerCourseUserInfo> getFavouriteTrainerCourseUserInfobyUserId(Long userId) {
		String hql = "from favourite_trainer_course_user_info where user_id='"+userId+"' and favouriteTrainerCourseUserIsActive = true";
		@SuppressWarnings("unchecked")
		Query<FavouriteTrainerCourseUserInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<FavouriteTrainerCourseUserInfo> favouriteTrainerCourseUserInfos = query.list();
		return !favouriteTrainerCourseUserInfos.isEmpty() ? favouriteTrainerCourseUserInfos : null;
	}

	@Transactional
	@Override
	public TrainerCourseUserQuestionInfo getTrainerCourseUserQuestionInfoByTrainerCourseId(Long trainerCourseId) {
		String hql = "from trainer_course_user_question_info where trainer_course_id='"+trainerCourseId+"' and trainerCourseUserQuestionIsActive = true";
		@SuppressWarnings("unchecked")
		Query<TrainerCourseUserQuestionInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<TrainerCourseUserQuestionInfo> trainerCourseUserQuestionInfos = query.list();
		return !trainerCourseUserQuestionInfos.isEmpty() ? trainerCourseUserQuestionInfos.get(0) : null;	
	}

	@Transactional
	@Override
	public List<TrainerCourseUserQuestionAnswerInfo> getTrainerCourseUserQuestionAnswerListByTrainerCourseUserQuestionId(
			Long trainerCourseUserQuestionId) {
		String hql = "from trainer_course_user_question_answer_info where trainer_course_user_question_id='"+trainerCourseUserQuestionId+"' and trainerCourseUserQuestionAnswerIsActive = true";
		@SuppressWarnings("unchecked")
		Query<TrainerCourseUserQuestionAnswerInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<TrainerCourseUserQuestionAnswerInfo> trainerCourseUserQuestionAnswerInfos = query.list();
		return !trainerCourseUserQuestionAnswerInfos.isEmpty() ? trainerCourseUserQuestionAnswerInfos : null;
	}

	@Transactional
	@Override
	public TrainerCourseUserQuestionInfo saveTrainerCourseUserQuestionInfo(
			TrainerCourseUserQuestionInfo trainerCourseUserQuestionInfo) {
			sessionFactory.getCurrentSession().save(trainerCourseUserQuestionInfo);
		return trainerCourseUserQuestionInfo;
	}

	@Transactional
	@Override
	public TrainerCourseUserQuestionAnswerInfo saveTrainerCourseUserQuestionAnswerInfo(
			TrainerCourseUserQuestionAnswerInfo trainerCourseUserQuestionAnswerInfo) {
		sessionFactory.getCurrentSession().save(trainerCourseUserQuestionAnswerInfo);
		return trainerCourseUserQuestionAnswerInfo;
	}

	@Transactional
	@Override
	public boolean updateUserCartListStatus(Long cartTrainerCourseUserId, boolean cartTrainerCourseUserIsActive) {
		@SuppressWarnings("rawtypes")
		Query q = sessionFactory.getCurrentSession()
				.createQuery("update cart_trainer_course_user_info set cartTrainerCourseUserIsActive=:ctcuia " 
								+ "where cartTrainerCourseUserId=:ctcuid");
		q.setParameter("ctcuia", cartTrainerCourseUserIsActive);
		q.setParameter("ctcuid", cartTrainerCourseUserId);

		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public CartTrainerCourseUserInfo getCartListUserInfoByUserIdAndTrainerCourseId(Long userId, Long trainerCourseId) {
		String hql = "from cart_trainer_course_user_info where trainer_course_id='" + trainerCourseId + "' and user_id='"
				+ userId + "'";
		@SuppressWarnings("unchecked")
		Query<CartTrainerCourseUserInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<CartTrainerCourseUserInfo> cartTrainerCourseUserInfos = query.list();
		return !cartTrainerCourseUserInfos.isEmpty() ? cartTrainerCourseUserInfos.get(0) : null;
	}

	@Transactional
	@Override
	public Long addTrainerCourseToCartList(CartTrainerCourseUserInfo cartTrainerCourseUserInfo2) {
		sessionFactory.getCurrentSession().save(cartTrainerCourseUserInfo2);
		return cartTrainerCourseUserInfo2.getCartTrainerCourseUserId();
	}

	@Transactional
	@Override
	public List<CartTrainerCourseUserInfo> getCartTrainerCourseUserInfobyUserId(Long userId) {
		String hql = "from cart_trainer_course_user_info where user_id='"
				+ userId + "' and cartTrainerCourseUserIsActive=true";
		@SuppressWarnings("unchecked")
		Query<CartTrainerCourseUserInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<CartTrainerCourseUserInfo> cartTrainerCourseUserInfos = query.list();
		return !cartTrainerCourseUserInfos.isEmpty() ? cartTrainerCourseUserInfos : null;
	}

	@Transactional
	@Override
	public boolean removeFromCart(Long cartTrainerCourseUserId) {
		@SuppressWarnings("rawtypes")
		Query q = sessionFactory.getCurrentSession()
				.createQuery("delete cart_trainer_course_user_info " 
								+ "where cartTrainerCourseUserId=:ctcuid");
		q.setParameter("ctcuid", cartTrainerCourseUserId);

		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public CouponCodeInfo couponCodeCheckPayment(String couponCodeValue) {
		String hql = "from coupon_code_info where couponCodeValue='" + couponCodeValue + "' "
				+ "and couponCodeIsActive=true and couponCodeIsDeletedByAdmin=false";
		@SuppressWarnings("unchecked")
		Query<CouponCodeInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<CouponCodeInfo> voucherCardInfos = query.list();
		return !voucherCardInfos.isEmpty() ? voucherCardInfos.get(0) : null;
	}

	@Transactional
	@Override
	public CouponCodeInfo getCouponCodeInfoByCouponCodeId(Long couponCodeId) {
		String hql = "from coupon_code_info where couponCodeId='" + couponCodeId + "' ";
		@SuppressWarnings("unchecked")
		Query<CouponCodeInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<CouponCodeInfo> couponCodeInfos = query.list();
		return !couponCodeInfos.isEmpty() ? couponCodeInfos.get(0) : null;
	}

	@Transactional
	@Override
	public UserOrderInfo saveUserOder(UserOrderInfo userOrderInfo) {
		sessionFactory.getCurrentSession().save(userOrderInfo);
		return userOrderInfo;
	}

	@Transactional
	@Override
	public UserOrderInfo getUserOrderInfoByUserOrderNumber(String userOrderNumber) {
		String hql = "from user_order_info where userOrderNumber='" + userOrderNumber + "' ";
		@SuppressWarnings("unchecked")
		Query<UserOrderInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<UserOrderInfo> userOrderInfos = query.list();
		return !userOrderInfos.isEmpty() ? userOrderInfos.get(0) : null;
	}

	@Transactional
	@Override
	public List<UserCoursePaymentInfo> getUserCoursePaymentInfoByUserOderId(Long userOrderId) {
		String hql = "from user_course_payment_info where user_order_id='" + userOrderId + "' ";
		@SuppressWarnings("unchecked")
		Query<UserCoursePaymentInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<UserCoursePaymentInfo> userCoursePaymentInfos = query.list();
		return !userCoursePaymentInfos.isEmpty() ? userCoursePaymentInfos : null;
	}

	@Transactional
	@Override
	public void saveUserCoursePaymentInfo(UserCoursePaymentInfo userCoursePaymentInfo) {
		sessionFactory.getCurrentSession().save(userCoursePaymentInfo);
	}

	@Transactional
	@Override
	public List<UserCoursePaymentInfo> getUserCoursePaymentListByUserId(Long userId) {
		String hql = "from user_course_payment_info where user_id='" + userId + "' ";
		@SuppressWarnings("unchecked")
		Query<UserCoursePaymentInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<UserCoursePaymentInfo> userCoursePaymentInfos = query.list();
		return !userCoursePaymentInfos.isEmpty() ? userCoursePaymentInfos : null;
	}

	@Transactional
	@Override
	public Long saveUserCourseReviewByUser(UserCourseReviewInfo userCourseReviewInfo) {
		sessionFactory.getCurrentSession().save(userCourseReviewInfo);
		return userCourseReviewInfo.getUserCourseReviewId();
	}

	@Transactional
	@Override
	public List<UserCourseReviewInfo> getUserCourseReviewByTrainerCourseId(Long trainerCourseId) {
		String hql = "from user_course_review_info where trainer_course_id='"+trainerCourseId+"'";
		@SuppressWarnings("unchecked")
		Query<UserCourseReviewInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<UserCourseReviewInfo> userCourseReviewInfos = query.list();
		return !userCourseReviewInfos.isEmpty() ? userCourseReviewInfos : null;
	}

	@Transactional
	@Override
	public NewsLetterInfo getNewsLetterInfoByUserEmail(String userEmail) {
		String hql = "from news_letter_info where newsLetterUserEmail='"+userEmail+"'";
		@SuppressWarnings("unchecked")
		Query<NewsLetterInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<NewsLetterInfo> newsLetterInfos = query.list();
		return !newsLetterInfos.isEmpty()?newsLetterInfos.get(0):null;
	}

	@Transactional
	@Override
	public Long saveNewsLetterSubscription(NewsLetterInfo newsLetterInfo) {
		sessionFactory.getCurrentSession().save(newsLetterInfo);
		return newsLetterInfo.getNewsLetterId();
	}

	@Transactional
	@Override
	public boolean updateNewsLetterInfo(Long newsLetterId, boolean b, String userName) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("update news_letter_info set newsLetterUserFName=:nlun,"
						+ "newsLetterUserIsActive=:nlss "
						+ "where newsLetterId=:nlid");
		
		q.setParameter("nlun", userName);
		q.setParameter("nlss", b);
		q.setParameter("nlid", newsLetterId);
		int updateNewsLetterInfo = q.executeUpdate();
		return updateNewsLetterInfo > 0 ? true : false;
	}

	@Transactional
	@Override
	public UserCoursePaymentInfo getUserCoursePaymentInfoByTrainerCourseIdAndUserId(Long trainerCourseId, Long userId) {
		String hql = "from user_course_payment_info where trainer_course_id='"+trainerCourseId+"' and user_id='"+userId+"'";
		@SuppressWarnings("unchecked")
		Query<UserCoursePaymentInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<UserCoursePaymentInfo> userCoursePaymentInfos = query.list();
		return !userCoursePaymentInfos.isEmpty() ? userCoursePaymentInfos.get(0) : null;
	}

}