package com.cube9.novafitness.repository.trainer;

import java.time.LocalDateTime;
import java.util.List;

import javax.validation.Valid;

import com.cube9.novafitness.model.course.CourseCoverImageInfo;
import com.cube9.novafitness.model.course.CourseCoverVideoInfo;
import com.cube9.novafitness.model.course.CourseImagesInfo;
import com.cube9.novafitness.model.course.CourseInfo;
import com.cube9.novafitness.model.course.CourseOtherImagesInfo;
import com.cube9.novafitness.model.course.CourseOtherPDFsInfo;
import com.cube9.novafitness.model.course.CourseOtherVideosInfo;
import com.cube9.novafitness.model.course.CoursePDFInfo;
import com.cube9.novafitness.model.course.CourseVideoInfo;
import com.cube9.novafitness.model.trainer.TrainerCertificateInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseChapterInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseChapterLessonInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseFAQInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseInfo;
import com.cube9.novafitness.model.trainer.TrainerIDCopyInfo;
import com.cube9.novafitness.model.trainer.TrainerInfo;
import com.cube9.novafitness.model.user.UserCoursePaymentInfo;
import com.cube9.novafitness.model.user.UserCourseReviewInfo;
import com.cube9.novafitness.wrapper.course.TrainerCourseWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerCourseFAQWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerProfileWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerResetPasswordWrapper;

public interface TrainerRepository {

	boolean duplicateEmailCheck(String trainerEmail);

	TrainerIDCopyInfo saveTrainerIDCopy(String trainerIDCopyName, String trainerIDCopyStoredPath,
			String trainerIDCopyViewPath, String trainerIDCopyFileType);

	TrainerCertificateInfo saveTrainerCertificate(String trainerCertificateName, String trainerCertificateStoredPath,
			String trainerCertificateViewPath, String trainerCertificateFileType);

	TrainerInfo saveTrainerInfo(TrainerInfo trainerInfo);

	boolean trainerVerification(String trainerEmail);

	TrainerInfo getTrainerInfoByTrainerEmail(String trainerEmail);

	boolean updateLastTrainerLoginByTainerId(Long trainerId, LocalDateTime now);

	boolean trainerResetPassword(@Valid TrainerResetPasswordWrapper trainerResetPasswordWrapper, String generatedSecuredPasswordHash);

	boolean updateTrainerProfileByTrainerEmail(@Valid TrainerProfileWrapper trainerProfileWrapper);

	boolean trainerChangePasswordByTrainerId(String generatedSecuredPasswordHash, Long trainerId);

	boolean updateTrainerProfileImageInfoByTrainerProfileImageId(Long trainerProfileImageId,
			String trainerProfileImageName, String trainerProfileImageStoredPath, String trainerProfileImageViewPath,
			String trainerProfileImageFileType);

	Long saveAndGetTrainerProfileImageId(String trainerProfileImageName, String trainerProfileImageStoredPath,
			String trainerProfileImageViewPath, String trainerProfileImageFileType);

	boolean updateTrainerProfileImageIdByTrainerId(Long trainerId, Long trainerProfileImageId);

	TrainerInfo getTrainerInfoByTrainerId(Long trainerId);

	boolean updateTrainerProfileImageInfoToNull(Long trainerId);

	boolean deleteTrainerProfileImageByTrainerProfileImageId(Long trainerProfileImageId);

	CourseInfo saveAndGetCourseInfo(CourseInfo courseInfo);

	CourseCoverImageInfo saveCoverImageInfoAndGetCoverImageInfo(CourseCoverImageInfo courseCoverImageInfo);

	boolean updateCourseInfoCoverImageIdByCourseId(Long courseId, Long courseCoverImageId);

	CourseImagesInfo saveCourseImagesInfoAndGetCourseImagesInfo(CourseImagesInfo courseImagesInfo);

	void saveCourseOtherImagesInfoAndGetCourseOtherImagesInfo(CourseOtherImagesInfo courseOtherImagesInfo);

	CoursePDFInfo saveCourseImagesInfoAndGetCourseImagesInfo(CoursePDFInfo coursePDFInfo);

	void saveCourseOtherPDFsInfoAndGetCourseOtherPDFsInfo(CourseOtherPDFsInfo courseOtherPDFsInfo);

	CourseVideoInfo saveCourseImagesInfoAndGetCourseImagesInfo(CourseVideoInfo courseVideoInfo);

	void saveCourseOtherVideosInfoAndGetCourseOtherVideosInfo(CourseOtherVideosInfo courseOtherVideosInfo);

	List<TrainerCourseInfo> getTrainerCourseListByTrainerId(Long trainerId);

	void saveTrainerCourseInfo(TrainerCourseInfo trainerCourseInfo);

	TrainerCourseInfo getTrainerCourseInfoByTrainerCourseId(Long trainerCourseId);

	boolean updateCourseInfoByCourseId(@Valid TrainerCourseWrapper trainerCourseWrapper);

	CourseInfo getCourseInfoByCourseId(Long courseId);

	List<CourseOtherImagesInfo> getCourseOtherImagesListByCourseId(Long courseId);

	List<CourseOtherVideosInfo> getCourseOtherVideosListByCourseId(Long courseId);

	List<CourseOtherPDFsInfo> getCourseOtherPDFsListByCourseId(Long courseId);

	List<UserCourseReviewInfo> getTrainerCourseListWithReviewAndRatingByTrainerCourseId(List<Long> trainerCourseIds);

	List<UserCoursePaymentInfo> getTrainerCourseUserPaymentListByTrainerCourseId(List<Long> trainerCourseIds);

	Long getAllTrainrCourseCountByTrainerId(Long trainerId);

	CourseCoverVideoInfo saveCourseCoverVideoInfoAndGetCourseCoverVideoInfo(CourseCoverVideoInfo courseCoverVideoInfo);

	boolean updateCourseInfoCoverVideoIdByCourseId(Long courseId, Long courseCoverVideoId);

	CourseOtherImagesInfo getCourseOtherImagesInfoByOtherImageId(Long otherImageId);

	boolean deleteOtherImage(Long otherImageId);

	CourseOtherVideosInfo getCourseOtherVideosInfoByOtherVideoId(Long otherVideoId);

	boolean deleteOtherVideo(Long otherVideoId);

	CourseOtherPDFsInfo getCourseOtherPDFsInfoByOtherPDFId(Long otherPDFId);

	boolean deleteOtherPDF(Long otherPDFId);

	boolean updateCourseInfoCoverImageToNull(Long courseId);

	boolean deleteCoverImage(Long courseCoverImageId);

	boolean updateCourseInfoCoverVideoToNull(Long courseId);

	boolean deleteCoverVideo(Long courseCoverVideoId);

	TrainerCourseFAQInfo saveAndGetTrainerCourseFAQInfo(TrainerCourseFAQInfo trainerCourseFAQInfo);

	List<TrainerCourseFAQInfo> getTrainerCourseFAQListByTrainerCourseId(Long trainerCourseId);

	TrainerCourseChapterInfo saveAndGetTrainerCourseChapterInfo(TrainerCourseChapterInfo trainerCourseChapterInfo);

	List<TrainerCourseChapterInfo> getTrainerCourseChapterListByTrainerCourseId(Long trainerCourseId);

	TrainerCourseChapterInfo getTrainerCourseChapterInfoByTrainerCourseChapterId(Long trainerCourseChapterId);

	List<TrainerCourseChapterLessonInfo> getTrainerCourseChapterLessonListByTrainerCourseChapterId(
			Long trainerCourseChapterId);

	TrainerCourseChapterLessonInfo saveAndGetTrainerCourseChapterLessonInfo(
			TrainerCourseChapterLessonInfo trainerCourseChapterLessonInfo);

	TrainerCourseChapterLessonInfo getTrainerCourseChapterLessonInfoByTrainerCourseChapterLessonId(
			Long trainerCourseChapterLessonId);

	boolean updateTrainerCourseChapterLessonInfo(TrainerCourseChapterLessonInfo trainerCourseChapterLessonInfo);

	boolean deleteTrainersCourseChapterLessonByTrainerCourseChapterLessonId(Long trainerCourseChapterLessonId);

	boolean updateTrainerCourseChapterInfoByTrainerCourseChapterId(Long trainerCourseChapterId, String trainerCourseChapterName);

	boolean deleteTrainersCourseChapterByTrainerCourseChapterId(Long trainerCourseChapterId);

	TrainerCourseFAQInfo getTrainerCourseFAQByTrainerCourseFAQId(Long trainerCourseFAQId);

	boolean updateTrainerCourseFAQByTrainerCourseFAQId(@Valid TrainerCourseFAQWrapper trainerCourseFAQWrapper);

	boolean deleteTrainersCourseFAQ(Long trainerCourseFAQId);

	boolean updateTrainerCourseChapterLessonInfoWithPreviewVideo(
			TrainerCourseChapterLessonInfo trainerCourseChapterLessonInfo);

	boolean updateTrainerCourseChapterLessonInfoWithMainVideo(
			TrainerCourseChapterLessonInfo trainerCourseChapterLessonInfo);

	boolean updateTrainerCourseChapterLessonInfoWithMainPdf(TrainerCourseChapterLessonInfo trainerCourseChapterLessonInfo);

}