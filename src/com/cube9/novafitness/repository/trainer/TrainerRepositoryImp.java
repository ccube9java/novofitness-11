package com.cube9.novafitness.repository.trainer;

import java.time.LocalDateTime;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cube9.novafitness.model.course.CourseCoverImageInfo;
import com.cube9.novafitness.model.course.CourseCoverVideoInfo;
import com.cube9.novafitness.model.course.CourseImagesInfo;
import com.cube9.novafitness.model.course.CourseInfo;
import com.cube9.novafitness.model.course.CourseOtherImagesInfo;
import com.cube9.novafitness.model.course.CourseOtherPDFsInfo;
import com.cube9.novafitness.model.course.CourseOtherVideosInfo;
import com.cube9.novafitness.model.course.CoursePDFInfo;
import com.cube9.novafitness.model.course.CourseVideoInfo;
import com.cube9.novafitness.model.trainer.TrainerCertificateInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseChapterInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseChapterLessonInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseFAQInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseInfo;
import com.cube9.novafitness.model.trainer.TrainerIDCopyInfo;
import com.cube9.novafitness.model.trainer.TrainerInfo;
import com.cube9.novafitness.model.trainer.TrainerProfileImageInfo;
import com.cube9.novafitness.model.user.UserCoursePaymentInfo;
import com.cube9.novafitness.model.user.UserCourseReviewInfo;
import com.cube9.novafitness.wrapper.course.TrainerCourseWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerCourseFAQWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerProfileWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerResetPasswordWrapper;

/**
 * @author Vaibhav Deshmane
 */
@Repository
public class TrainerRepositoryImp implements TrainerRepository {

	@Autowired
	private SessionFactory sessionFactory;

	@Transactional
	@Override
	public boolean duplicateEmailCheck(String trainerEmail) {
		@SuppressWarnings("unchecked")
		Query<TrainerInfo> query = sessionFactory.getCurrentSession()
				.createQuery("from  trainer_info WHERE trainerEmail ='" + trainerEmail + "'");
		List<TrainerInfo> trainerInfos = query.list();
		return !trainerInfos.isEmpty() ? true : false;
	}

	@Transactional
	@Override
	public TrainerIDCopyInfo saveTrainerIDCopy(String trainerIDCopyName, String trainerIDCopyStoredPath,
			String trainerIDCopyViewPath, String trainerIDCopyFileType) {
		TrainerIDCopyInfo traninerIdCopyInfo = new TrainerIDCopyInfo();
		traninerIdCopyInfo.setTrainerIDCopyName(trainerIDCopyName);
		traninerIdCopyInfo.setTrainerIDCopyStoredPath(trainerIDCopyStoredPath);
		traninerIdCopyInfo.setTrainerIDCopyViewPath(trainerIDCopyViewPath);
		traninerIdCopyInfo.setTrainerIDCopyFileType(trainerIDCopyFileType);

		sessionFactory.getCurrentSession().save(traninerIdCopyInfo);
		return traninerIdCopyInfo != null ? traninerIdCopyInfo : null;
	}

	@Transactional
	@Override
	public TrainerCertificateInfo saveTrainerCertificate(String trainerCertificateName,
			String trainerCertificateStoredPath, String trainerCertificateViewPath, String trainerCertificateFileType) {
		TrainerCertificateInfo trainerCertificateInfo = new TrainerCertificateInfo();
		trainerCertificateInfo.setTrainerCertificateName(trainerCertificateName);
		trainerCertificateInfo.setTrainerCertificateStoredPath(trainerCertificateStoredPath);
		trainerCertificateInfo.setTrainerCertificateViewPath(trainerCertificateViewPath);
		trainerCertificateInfo.setTrainerCertificateFileType(trainerCertificateFileType);

		sessionFactory.getCurrentSession().save(trainerCertificateInfo);
		return trainerCertificateInfo != null ? trainerCertificateInfo : null;
	}

	@Transactional
	@Override
	public TrainerInfo saveTrainerInfo(TrainerInfo trainerInfo) {
		sessionFactory.getCurrentSession().save(trainerInfo);
		return trainerInfo != null ? trainerInfo : null;
	}

	@Transactional
	@Override
	public boolean trainerVerification(String trainerEmail) {
		@SuppressWarnings("rawtypes")
		Query q = sessionFactory.getCurrentSession()
				.createQuery("update trainer_info set trainerStatus=:s where trainerEmail=:e");
		q.setParameter("s", true);
		q.setParameter("e", trainerEmail);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public TrainerInfo getTrainerInfoByTrainerEmail(String trainerEmail) {
		String hql = "from  trainer_info where trainerEmail='" + trainerEmail + "'";
		@SuppressWarnings("unchecked")
		Query<TrainerInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<TrainerInfo> trainerInfos = query.list();
		return !trainerInfos.isEmpty() ? trainerInfos.get(0) : null;
	}

	@Transactional
	@Override
	public boolean updateLastTrainerLoginByTainerId(Long trainerId, LocalDateTime now) {
		Query<?> trainer = sessionFactory.getCurrentSession()
				.createQuery("update trainer_info set trainerLastLoginDateTime=:tlldt "
							+ "where trainerId=:tid");
		trainer.setParameter("tlldt", now);
		trainer.setParameter("tid", trainerId);
		int tainerStatus = trainer.executeUpdate();
		return tainerStatus > 0 ? true : false;
	}

	@Transactional
	@Override
	public boolean trainerResetPassword(@Valid TrainerResetPasswordWrapper trainerResetPasswordWrapper, String generatedSecuredPasswordHash) {
		@SuppressWarnings("rawtypes")
		Query q = sessionFactory.getCurrentSession()
				.createQuery("update trainer_info set trainerPassword=:p where trainerEmail=:e");
		q.setParameter("p", generatedSecuredPasswordHash);
		q.setParameter("e", trainerResetPasswordWrapper.getTrainerEmail());
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public boolean updateTrainerProfileByTrainerEmail(@Valid TrainerProfileWrapper trainerProfileWrapper) {
		Query<?> admin = sessionFactory.getCurrentSession().createQuery(
				"update trainer_info set trainerFName=:fn, trainerLName=:ln, trainerDescription=:tdesc, trainerWebsiteLink=:twl, trainerFacebookLink=:tfbl,"
				+ " trainerInstagramLink=:til, trainerYoutubeLink=:tyl, trainerPayPalId=:tppid "
				+ "where trainerEmail=:ae");
		admin.setParameter("fn", trainerProfileWrapper.getTrainerFName());
		admin.setParameter("ln", trainerProfileWrapper.getTrainerLName());
		admin.setParameter("tdesc", trainerProfileWrapper.getTrainerDescription());
		admin.setParameter("twl", trainerProfileWrapper.getTrainerWebsiteLink());
		admin.setParameter("tfbl", trainerProfileWrapper.getTrainerFacebookLink());
		admin.setParameter("til", trainerProfileWrapper.getTrainerInstagramLink());
		admin.setParameter("tyl", trainerProfileWrapper.getTrainerYoutubeLink());
		admin.setParameter("tppid", trainerProfileWrapper.getTrainerPayPalId());
		admin.setParameter("ae", trainerProfileWrapper.getTrainerEmail());
		int status = admin.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public boolean trainerChangePasswordByTrainerId(String generatedSecuredPasswordHash, Long trainerId) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("update trainer_info set trainerPassword=:p where trainerId=:i");
		q.setParameter("p", generatedSecuredPasswordHash);
		q.setParameter("i", trainerId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public Long saveAndGetTrainerProfileImageId(String trainerProfileImageName, String trainerProfileImageStoredPath,
			String trainerProfileImageViewPath, String trainerProfileImageFileType) {
		TrainerProfileImageInfo trainerProfileImageInfo = new TrainerProfileImageInfo();
		trainerProfileImageInfo.setTrainerProfileImageName(trainerProfileImageName);
		trainerProfileImageInfo.setTrainerProfileImageStoredPath(trainerProfileImageStoredPath);
		trainerProfileImageInfo.setTrainerProfileImageViewPath(trainerProfileImageViewPath);
		trainerProfileImageInfo.setTrainerProfileImageFileType(trainerProfileImageFileType);
		
		sessionFactory.getCurrentSession().save(trainerProfileImageInfo);
		
		return trainerProfileImageInfo.getTrainerProfileImageId();
	}

	@Transactional
	@Override
	public boolean updateTrainerProfileImageIdByTrainerId(Long trainerId, Long trainerProfileImageId) {
		Query<?> q = sessionFactory.getCurrentSession().createQuery(
				"update trainer_info set trainer_profile_image_id=:tpid where trainerId=:tid");
		q.setParameter("tpid", trainerProfileImageId);
		q.setParameter("tid", trainerId);
		int status1 = q.executeUpdate();
		return status1 > 0 ? true : false;
	}

	@Transactional
	@Override
	public boolean updateTrainerProfileImageInfoByTrainerProfileImageId(Long trainerProfileImageId,
			String trainerProfileImageName, String trainerProfileImageStoredPath, String trainerProfileImageViewPath,
			String trainerProfileImageFileType) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("update trainer_profile_image_info set trainerProfileImageName=:tpin, "
						+ "trainerProfileImageStoredPath=:tpisp, trainerProfileImageViewPath=:tpivp, "
						+ "trainerProfileImageFileType=:tpift, trainerProfileImageLastUpdatedDateTime=:tpiludt "
						+ "where trainerProfileImageId=:tpid");
		q.setParameter("tpin", trainerProfileImageName);
		q.setParameter("tpisp", trainerProfileImageStoredPath);
		q.setParameter("tpivp", trainerProfileImageViewPath);
		q.setParameter("tpift", trainerProfileImageFileType);
		q.setParameter("tpiludt", LocalDateTime.now());
		q.setParameter("tpid", trainerProfileImageId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}
	
	@Transactional
	@Override
	public TrainerInfo getTrainerInfoByTrainerId(Long trainerId) {
		String hql = "from  trainer_info where trainerId='" + trainerId + "'";
		@SuppressWarnings("unchecked")
		Query<TrainerInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<TrainerInfo> trainerInfos = query.list();
		return !trainerInfos.isEmpty() ? trainerInfos.get(0) : null;
	}

	@Transactional
	@Override
	public boolean updateTrainerProfileImageInfoToNull(Long trainerId) {
		@SuppressWarnings("rawtypes")
		Query q = sessionFactory.getCurrentSession()
				.createQuery("update trainer_info set trainer_profile_image_id=:tpid "
						+ "where trainerId=:tid");
		q.setParameter("tpid", null);
		q.setParameter("tid", trainerId);

		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public boolean deleteTrainerProfileImageByTrainerProfileImageId(Long trainerProfileImageId) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("delete from trainer_profile_image_info where trainerProfileImageId=:tpid");
		q.setParameter("tpid", trainerProfileImageId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public CourseInfo saveAndGetCourseInfo(CourseInfo courseInfo) {
		sessionFactory.getCurrentSession().save(courseInfo);
		return courseInfo;
	}

	@Transactional
	@Override
	public CourseCoverImageInfo saveCoverImageInfoAndGetCoverImageInfo(CourseCoverImageInfo courseCoverImageInfo) {
		sessionFactory.getCurrentSession().save(courseCoverImageInfo);
		return courseCoverImageInfo;
	}

	@Transactional
	@Override
	public boolean updateCourseInfoCoverImageIdByCourseId(Long courseId, Long courseCoverImageId) {
		@SuppressWarnings("rawtypes")
		Query q = sessionFactory.getCurrentSession()
				.createQuery("update course_info set course_cover_image_id=:cciid "
						+ "where courseId=:cid");
		q.setParameter("cciid", courseCoverImageId);
		q.setParameter("cid", courseId);

		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public CourseImagesInfo saveCourseImagesInfoAndGetCourseImagesInfo(CourseImagesInfo courseImagesInfo) {
		sessionFactory.getCurrentSession().save(courseImagesInfo);
		return courseImagesInfo;
	}

	@Transactional
	@Override
	public void saveCourseOtherImagesInfoAndGetCourseOtherImagesInfo(CourseOtherImagesInfo courseOtherImagesInfo) {
		sessionFactory.getCurrentSession().save(courseOtherImagesInfo);
	}

	@Transactional
	@Override
	public CoursePDFInfo saveCourseImagesInfoAndGetCourseImagesInfo(CoursePDFInfo coursePDFInfo) {
		sessionFactory.getCurrentSession().save(coursePDFInfo);
		return coursePDFInfo;
	}

	@Transactional
	@Override
	public void saveCourseOtherPDFsInfoAndGetCourseOtherPDFsInfo(CourseOtherPDFsInfo courseOtherPDFsInfo) {
		sessionFactory.getCurrentSession().save(courseOtherPDFsInfo);
	}

	@Transactional
	@Override
	public CourseVideoInfo saveCourseImagesInfoAndGetCourseImagesInfo(CourseVideoInfo courseVideoInfo) {
		sessionFactory.getCurrentSession().save(courseVideoInfo);
		return courseVideoInfo;
	}

	@Transactional
	@Override
	public void saveCourseOtherVideosInfoAndGetCourseOtherVideosInfo(CourseOtherVideosInfo courseOtherVideosInfo) {
		sessionFactory.getCurrentSession().save(courseOtherVideosInfo);
	}

	@Transactional
	@Override
	public List<TrainerCourseInfo> getTrainerCourseListByTrainerId(Long trainerId) {
		String hql = "from  trainer_course_info where trainer_id='" + trainerId + "' and trainerCourseIsDeletedByAdmin=false";
		@SuppressWarnings("unchecked")
		Query<TrainerCourseInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<TrainerCourseInfo> trainerCourseInfos = query.list();
		return !trainerCourseInfos.isEmpty() ? trainerCourseInfos : null;
	}

	@Transactional
	@Override
	public void saveTrainerCourseInfo(TrainerCourseInfo trainerCourseInfo) {
		sessionFactory.getCurrentSession().save(trainerCourseInfo);
	}

	@Transactional
	@Override
	public TrainerCourseInfo getTrainerCourseInfoByTrainerCourseId(Long trainerCourseId) {
		String hql = "from trainer_course_info where trainerCourseId='" + trainerCourseId + "'";
		@SuppressWarnings("unchecked")
		Query<TrainerCourseInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<TrainerCourseInfo> trainerCourseInfos = query.list();
		return !trainerCourseInfos.isEmpty() ? trainerCourseInfos.get(0) : null;
	}

	@Transactional
	@Override
	public boolean updateCourseInfoByCourseId(@Valid TrainerCourseWrapper trainerCourseWrapper) {
		@SuppressWarnings("rawtypes")
		Query q = sessionFactory.getCurrentSession()
				.createQuery("update course_info set courseName=:cn, courseDescription=:cd, courseContent=:cc, "
						+ "category_id=:catid, courseLanguage=:cl, coursePrice=:cp where courseId=:cid");
		q.setParameter("cn", trainerCourseWrapper.getCourseName());
		q.setParameter("cd", trainerCourseWrapper.getCourseDescription());
		q.setParameter("cc", trainerCourseWrapper.getCourseContent());
		q.setParameter("catid", trainerCourseWrapper.getCategoryId());
		q.setParameter("cl", trainerCourseWrapper.getCourseLanguage());
		q.setParameter("cp", trainerCourseWrapper.getCoursePrice()!=""?Long.parseLong(trainerCourseWrapper.getCoursePrice()):null);
		q.setParameter("cid", trainerCourseWrapper.getCourseId());

		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public CourseInfo getCourseInfoByCourseId(Long courseId) {
		String hql = "from course_info where courseId='" + courseId + "'";
		@SuppressWarnings("unchecked")
		Query<CourseInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<CourseInfo> courseInfos = query.list();
		return !courseInfos.isEmpty() ? courseInfos.get(0) : null;
	}

	@Transactional
	@Override
	public List<CourseOtherImagesInfo> getCourseOtherImagesListByCourseId(Long courseId) {
		String hql = "from course_other_images_info where course_id='" + courseId + "'";
		@SuppressWarnings("unchecked")
		Query<CourseOtherImagesInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<CourseOtherImagesInfo> courseOtherImagesInfos = query.list();
		return !courseOtherImagesInfos.isEmpty() ? courseOtherImagesInfos : null;
	}

	@Transactional
	@Override
	public List<CourseOtherVideosInfo> getCourseOtherVideosListByCourseId(Long courseId) {
		String hql = "from course_other_videos_info where course_id='" + courseId + "'";
		@SuppressWarnings("unchecked")
		Query<CourseOtherVideosInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<CourseOtherVideosInfo> courseOtherVideosInfos = query.list();
		return !courseOtherVideosInfos.isEmpty() ? courseOtherVideosInfos : null;
	}

	@Transactional
	@Override
	public List<CourseOtherPDFsInfo> getCourseOtherPDFsListByCourseId(Long courseId) {
		String hql = "from course_other_pdfs_info where course_id='" + courseId + "'";
		@SuppressWarnings("unchecked")
		Query<CourseOtherPDFsInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<CourseOtherPDFsInfo> courseOtherPDFsInfos = query.list();
		return !courseOtherPDFsInfos.isEmpty() ? courseOtherPDFsInfos : null;
	}

	@Transactional
	@Override
	public List<UserCourseReviewInfo> getTrainerCourseListWithReviewAndRatingByTrainerCourseId(
			List<Long> trainerCourseIds) {
		String hql = "from user_course_review_info where trainer_course_id in ("
				+ trainerCourseIds.toString().replace("[", "").replace("]", "") + ") "
				+ "and userCourseReviewIsDeletedByAdmin=false and userCourseReviewIsActive=true order by userCourseReviewId desc";
		@SuppressWarnings("unchecked")
		Query<UserCourseReviewInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<UserCourseReviewInfo> userCourseReviewInfos = query.list();
		return !userCourseReviewInfos.isEmpty() ? userCourseReviewInfos : null;
	}

	@Transactional
	@Override
	public List<UserCoursePaymentInfo> getTrainerCourseUserPaymentListByTrainerCourseId(List<Long> trainerCourseIds) {
		String hql = "from user_course_payment_info where trainer_course_id in ("
				+ trainerCourseIds.toString().replace("[", "").replace("]", "") + ") "
				+ "and userCoursePaymentIsDeletedByAdmin=false and userCoursePaymentIsActive=true order by userCoursePaymentId desc";
		@SuppressWarnings("unchecked")
		Query<UserCoursePaymentInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<UserCoursePaymentInfo> userCoursePaymentInfos = query.list();
		return !userCoursePaymentInfos.isEmpty() ? userCoursePaymentInfos : null;
	}

	@Transactional
	@Override
	public Long getAllTrainrCourseCountByTrainerId(Long trainerId) {
		return (Long) sessionFactory.getCurrentSession().createQuery("select count(*) from trainer_course_info where trainer_id = '"+ trainerId +"' and trainerCourseIsDeletedByAdmin=false")
				.getSingleResult();
	}

	@Transactional
	@Override
	public CourseCoverVideoInfo saveCourseCoverVideoInfoAndGetCourseCoverVideoInfo(
			CourseCoverVideoInfo courseCoverVideoInfo) {
		sessionFactory.getCurrentSession().save(courseCoverVideoInfo);
		return courseCoverVideoInfo;
	}

	@Transactional
	@Override
	public boolean updateCourseInfoCoverVideoIdByCourseId(Long courseId, Long courseCoverVideoId) {
		@SuppressWarnings("rawtypes")
		Query q = sessionFactory.getCurrentSession()
				.createQuery("update course_info set course_cover_video_id=:ccvid "
						+ "where courseId=:cid");
		q.setParameter("ccvid", courseCoverVideoId);
		q.setParameter("cid", courseId);

		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public CourseOtherImagesInfo getCourseOtherImagesInfoByOtherImageId(Long otherImageId) {
		String hql = "from course_other_images_info where courseOtherImagesId='" + otherImageId + "'";
		@SuppressWarnings("unchecked")
		Query<CourseOtherImagesInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<CourseOtherImagesInfo> courseOtherImagesInfos = query.list();
		return !courseOtherImagesInfos.isEmpty() ? courseOtherImagesInfos.get(0) : null;
	}
	
	@Transactional
	@Override
	public boolean deleteOtherImage(Long otherImageId) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("delete from course_other_images_info where courseOtherImagesId=:i");
		q.setParameter("i", otherImageId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public CourseOtherVideosInfo getCourseOtherVideosInfoByOtherVideoId(Long otherVideoId) {
		String hql = "from course_other_videos_info where courseOtherVideosId='" + otherVideoId + "'";
		@SuppressWarnings("unchecked")
		Query<CourseOtherVideosInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<CourseOtherVideosInfo> courseOtherVideosInfos = query.list();
		return !courseOtherVideosInfos.isEmpty() ? courseOtherVideosInfos.get(0) : null;
	}

	@Transactional
	@Override
	public boolean deleteOtherVideo(Long otherVideoId) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("delete from course_other_videos_info where courseOtherVideosId=:i");
		q.setParameter("i", otherVideoId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public CourseOtherPDFsInfo getCourseOtherPDFsInfoByOtherPDFId(Long otherPDFId) {
		String hql = "from course_other_pdfs_info where courseOtherPDFsId='" + otherPDFId + "'";
		@SuppressWarnings("unchecked")
		Query<CourseOtherPDFsInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<CourseOtherPDFsInfo> courseOtherPDFsInfos = query.list();
		return !courseOtherPDFsInfos.isEmpty() ? courseOtherPDFsInfos.get(0) : null;
	}

	@Transactional
	@Override
	public boolean deleteOtherPDF(Long otherPDFId) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("delete from course_other_pdfs_info where courseOtherPDFsId=:i");
		q.setParameter("i", otherPDFId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public boolean updateCourseInfoCoverImageToNull(Long courseId) {
		@SuppressWarnings("rawtypes")
		Query q = sessionFactory.getCurrentSession().createQuery(
				"update course_info set course_cover_image_id=:upcid " + "where courseId=:upid");
		q.setParameter("upcid", null);
		q.setParameter("upid", courseId);

		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public boolean deleteCoverImage(Long courseCoverImageId) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("delete from course_cover_image_info where courseCoverImageId=:i");
		q.setParameter("i", courseCoverImageId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public boolean updateCourseInfoCoverVideoToNull(Long courseId) {
		@SuppressWarnings("rawtypes")
		Query q = sessionFactory.getCurrentSession().createQuery(
				"update course_info set course_cover_video_id=:upcid " + "where courseId=:upid");
		q.setParameter("upcid", null);
		q.setParameter("upid", courseId);

		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public boolean deleteCoverVideo(Long courseCoverVideoId) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("delete from course_cover_video_info where courseCoverVideoId=:i");
		q.setParameter("i", courseCoverVideoId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public TrainerCourseFAQInfo saveAndGetTrainerCourseFAQInfo(TrainerCourseFAQInfo trainerCourseFAQInfo) {
		sessionFactory.getCurrentSession().save(trainerCourseFAQInfo);
		return trainerCourseFAQInfo;
	}

	@Transactional
	@Override
	public List<TrainerCourseFAQInfo> getTrainerCourseFAQListByTrainerCourseId(Long trainerCourseId) {
		String hql = "from trainer_course_faq_info where trainer_course_id='" + trainerCourseId + "'";
		@SuppressWarnings("unchecked")
		Query<TrainerCourseFAQInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<TrainerCourseFAQInfo> trainerCourseFAQInfos = query.list();
		return !trainerCourseFAQInfos.isEmpty() ? trainerCourseFAQInfos : null;
	}

	@Transactional
	@Override
	public TrainerCourseChapterInfo saveAndGetTrainerCourseChapterInfo(
			TrainerCourseChapterInfo trainerCourseChapterInfo) {
		sessionFactory.getCurrentSession().save(trainerCourseChapterInfo);
		return trainerCourseChapterInfo;
	}

	@Transactional
	@Override
	public List<TrainerCourseChapterInfo> getTrainerCourseChapterListByTrainerCourseId(Long trainerCourseId) {
		String hql = "from trainer_course_chapter_info where trainer_course_id='" + trainerCourseId + "'";
		@SuppressWarnings("unchecked")
		Query<TrainerCourseChapterInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<TrainerCourseChapterInfo> tranCourseChapterInfos = query.list();
		return !tranCourseChapterInfos.isEmpty() ? tranCourseChapterInfos : null;
	}

	@Transactional
	@Override
	public TrainerCourseChapterInfo getTrainerCourseChapterInfoByTrainerCourseChapterId(Long trainerCourseChapterId) {
		String hql = "from trainer_course_chapter_info where trainerCourseChapterId='" + trainerCourseChapterId + "'";
		@SuppressWarnings("unchecked")
		Query<TrainerCourseChapterInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<TrainerCourseChapterInfo> tranCourseChapterInfos = query.list();
		return !tranCourseChapterInfos.isEmpty() ? tranCourseChapterInfos.get(0) : null;
	}

	@Transactional
	@Override
	public List<TrainerCourseChapterLessonInfo> getTrainerCourseChapterLessonListByTrainerCourseChapterId(
			Long trainerCourseChapterId) {
		String hql = "from trainer_course_chapter_lesson_info where trainer_course_chapter_id='" + trainerCourseChapterId + "'";
		@SuppressWarnings("unchecked")
		Query<TrainerCourseChapterLessonInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<TrainerCourseChapterLessonInfo> trainerCourseChapterLessonInfos = query.list();
		return !trainerCourseChapterLessonInfos.isEmpty() ? trainerCourseChapterLessonInfos : null;
	}

	@Transactional
	@Override
	public TrainerCourseChapterLessonInfo saveAndGetTrainerCourseChapterLessonInfo(
			TrainerCourseChapterLessonInfo trainerCourseChapterLessonInfo) {
		sessionFactory.getCurrentSession().save(trainerCourseChapterLessonInfo);
		return trainerCourseChapterLessonInfo;
	}

	@Transactional
	@Override
	public TrainerCourseChapterLessonInfo getTrainerCourseChapterLessonInfoByTrainerCourseChapterLessonId(
			Long trainerCourseChapterLessonId) {
		String hql = "from trainer_course_chapter_lesson_info where trainerCourseChapterLessonId='" + trainerCourseChapterLessonId + "'";
		@SuppressWarnings("unchecked")
		Query<TrainerCourseChapterLessonInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<TrainerCourseChapterLessonInfo> trainerCourseChapterLessonInfos = query.list();
		return !trainerCourseChapterLessonInfos.isEmpty() ? trainerCourseChapterLessonInfos.get(0) : null;
	}

	@Transactional
	@Override
	public boolean updateTrainerCourseChapterLessonInfo(TrainerCourseChapterLessonInfo trainerCourseChapterLessonInfo) {
		@SuppressWarnings("rawtypes")
		Query q = sessionFactory.getCurrentSession().createQuery(
				"update trainer_course_chapter_lesson_info set trainerCourseChapterLessonName=:tccln "
				+ "where trainerCourseChapterLessonId=:tcclid");
		
		q.setParameter("tccln", trainerCourseChapterLessonInfo.getTrainerCourseChapterLessonName());
		q.setParameter("tcclid", trainerCourseChapterLessonInfo.getTrainerCourseChapterLessonId());

		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public boolean deleteTrainersCourseChapterLessonByTrainerCourseChapterLessonId(Long trainerCourseChapterLessonId) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("delete from trainer_course_chapter_lesson_info where trainerCourseChapterLessonId=:i");
		q.setParameter("i", trainerCourseChapterLessonId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public boolean updateTrainerCourseChapterInfoByTrainerCourseChapterId(Long trainerCourseChapterId, String trainerCourseChapterName) {
		@SuppressWarnings("rawtypes")
		Query q = sessionFactory.getCurrentSession().createQuery(
					"update trainer_course_chapter_info set trainerCourseChapterName=:tccn " 
					+ "where trainerCourseChapterId=:tccid");
		q.setParameter("tccn", trainerCourseChapterName);
		q.setParameter("tccid", trainerCourseChapterId);

		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public boolean deleteTrainersCourseChapterByTrainerCourseChapterId(Long trainerCourseChapterId) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("delete from trainer_course_chapter_info where trainerCourseChapterId=:i");
		q.setParameter("i", trainerCourseChapterId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public TrainerCourseFAQInfo getTrainerCourseFAQByTrainerCourseFAQId(Long trainerCourseFAQId) {
		String hql = "from trainer_course_faq_info where trainerCourseFAQId='" + trainerCourseFAQId + "'";
		@SuppressWarnings("unchecked")
		Query<TrainerCourseFAQInfo> query = sessionFactory.getCurrentSession().createQuery(hql);
		List<TrainerCourseFAQInfo> trainerCourseFAQInfos = query.list();
		return !trainerCourseFAQInfos.isEmpty() ? trainerCourseFAQInfos.get(0) : null;
	}

	@Transactional
	@Override
	public boolean updateTrainerCourseFAQByTrainerCourseFAQId(@Valid TrainerCourseFAQWrapper trainerCourseFAQWrapper) {
		@SuppressWarnings("rawtypes")
		Query q = sessionFactory.getCurrentSession().createQuery(
					"update trainer_course_faq_info set trainerCourseFAQQuestion=:tcfq, trainerCourseFAQAnswer=:tcfa " 
					+ "where trainerCourseFAQId=:tcfid");
		q.setParameter("tcfq", trainerCourseFAQWrapper.getTrainerCourseFAQQuestion());
		q.setParameter("tcfa", trainerCourseFAQWrapper.getTrainerCourseFAQAnswer());
		q.setParameter("tcfid", trainerCourseFAQWrapper.getTrainerCourseFAQId());

		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public boolean deleteTrainersCourseFAQ(Long trainerCourseFAQId) {
		Query<?> q = sessionFactory.getCurrentSession()
				.createQuery("delete from trainer_course_faq_info where trainerCourseFAQId=:i");
		q.setParameter("i", trainerCourseFAQId);
		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public boolean updateTrainerCourseChapterLessonInfoWithPreviewVideo(
			TrainerCourseChapterLessonInfo trainerCourseChapterLessonInfo) {
		@SuppressWarnings("rawtypes")
		Query q = sessionFactory.getCurrentSession().createQuery(
				"update trainer_course_chapter_lesson_info set "
				+ "trainerCourseChapterLessonPreviewVideoStoredPath=:tcclpvsp, trainerCourseChapterLessonPreviewVideoViewPath=:tcclpvvp, "
				+ "trainerCourseChapterLessonPreviewVideoFileType=:tcclpvft, trainerCourseChapterLessonpreviewVideoVimeoLink=:tcclpvvl, trainerCourseChapterLessonPreviewVideoVimeoName=:tcclpvvn "
				+ "where trainerCourseChapterLessonId=:tcclid");
		
		q.setParameter("tcclpvsp", trainerCourseChapterLessonInfo.getTrainerCourseChapterLessonPreviewVideoStoredPath());
		q.setParameter("tcclpvvp", trainerCourseChapterLessonInfo.getTrainerCourseChapterLessonPreviewVideoViewPath());
		q.setParameter("tcclpvft", trainerCourseChapterLessonInfo.getTrainerCourseChapterLessonPreviewVideoFileType());
		q.setParameter("tcclpvvl", trainerCourseChapterLessonInfo.getTrainerCourseChapterLessonpreviewVideoVimeoLink());
		q.setParameter("tcclpvvn", trainerCourseChapterLessonInfo.getTrainerCourseChapterLessonPreviewVideoVimeoName());
		q.setParameter("tcclid", trainerCourseChapterLessonInfo.getTrainerCourseChapterLessonId());

		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public boolean updateTrainerCourseChapterLessonInfoWithMainVideo(
			TrainerCourseChapterLessonInfo trainerCourseChapterLessonInfo) {
		@SuppressWarnings("rawtypes")
		Query q = sessionFactory.getCurrentSession().createQuery(
				"update trainer_course_chapter_lesson_info set trainerCourseChapterLessonMainVideoStoredPath=:tcclmvsp, trainerCourseChapterLessonMainVideoViewPath=:tcclmvvp, "
				+ "trainerCourseChapterLessonMainVideoFileType=:tcclmvft, trainerCourseChapterLessonMainVideoVimeoLink=:tcclmvvl, trainerCourseChapterLessonMainVideoVimeoName=:tcclmvvn "
				+ "where trainerCourseChapterLessonId=:tcclid");
		
		q.setParameter("tcclmvsp", trainerCourseChapterLessonInfo.getTrainerCourseChapterLessonMainVideoStoredPath());
		q.setParameter("tcclmvvp", trainerCourseChapterLessonInfo.getTrainerCourseChapterLessonMainVideoViewPath());
		q.setParameter("tcclmvft", trainerCourseChapterLessonInfo.getTrainerCourseChapterLessonMainVideoFileType());
		q.setParameter("tcclmvvl", trainerCourseChapterLessonInfo.getTrainerCourseChapterLessonMainVideoVimeoLink());
		q.setParameter("tcclmvvn", trainerCourseChapterLessonInfo.getTrainerCourseChapterLessonMainVideoVimeoName());
		q.setParameter("tcclid", trainerCourseChapterLessonInfo.getTrainerCourseChapterLessonId());

		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

	@Transactional
	@Override
	public boolean updateTrainerCourseChapterLessonInfoWithMainPdf(
			TrainerCourseChapterLessonInfo trainerCourseChapterLessonInfo) {
		
		@SuppressWarnings("rawtypes")
		Query q = sessionFactory.getCurrentSession().createQuery(
				"update trainer_course_chapter_lesson_info set trainerCourseChapterLessonMainPdfStoredPath=:tcclmpsp, trainerCourseChapterLessonMainPdfViewPath=:tcclmpvp,"
				+ "trainerCourseChapterLessonMainPdfFileType=:tcclmpft "
				+ "where trainerCourseChapterLessonId=:tcclid");
		
		q.setParameter("tcclmpsp", trainerCourseChapterLessonInfo.getTrainerCourseChapterLessonMainPdfStoredPath());
		q.setParameter("tcclmpvp", trainerCourseChapterLessonInfo.getTrainerCourseChapterLessonMainPdfViewPath());
		q.setParameter("tcclmpft", trainerCourseChapterLessonInfo.getTrainerCourseChapterLessonMainPdfFileType());
		q.setParameter("tcclid", trainerCourseChapterLessonInfo.getTrainerCourseChapterLessonId());

		int status = q.executeUpdate();
		return status > 0 ? true : false;
	}

}