package com.cube9.novafitness.model.trainer;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.CreationTimestamp;

//@Entity(name = "course_payment_contribution_info")
public class CoursePaymentContributionInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "admin_trainer_course_payment_contribution_id")
	private Long adminTrainerCoursePaymentContributionId;

	@Column(name = "admin_trainer_course_payment_contribution_percentage")
	private Double adminTrainerCoursePaymentContributionPercentage;

	@Column(name = "is_Active", columnDefinition = "boolean default true")
	private boolean adminTrainerCoursePaymentContributionIsActive = true;

	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean adminTrainerCoursePaymentContributionIsDeletedByAdmin = false;

	@Column(name = "admin_trainer_course_payment_contribution_percentage_last_updated_dateTime")
	private LocalDateTime adminTrainerCoursePaymentContributionLastUpdatedDateTime;

	@Column(name = "admin_trainer_course_payment_contribution_percentage_created_dateTime")
	@CreationTimestamp
	private LocalDateTime adminTrainerCoursePaymentContributionCreatedDateTime;

	public Long getAdminTrainerCoursePaymentContributionId() {
		return adminTrainerCoursePaymentContributionId;
	}

	public void setAdminTrainerCoursePaymentContributionId(Long adminTrainerCoursePaymentContributionId) {
		this.adminTrainerCoursePaymentContributionId = adminTrainerCoursePaymentContributionId;
	}

	public Double getAdminTrainerCoursePaymentContributionPercentage() {
		return adminTrainerCoursePaymentContributionPercentage;
	}

	public void setAdminTrainerCoursePaymentContributionPercentage(Double adminTrainerCoursePaymentContributionPercentage) {
		this.adminTrainerCoursePaymentContributionPercentage = adminTrainerCoursePaymentContributionPercentage;
	}

	public boolean isAdminTrainerCoursePaymentContributionIsActive() {
		return adminTrainerCoursePaymentContributionIsActive;
	}

	public void setAdminTrainerCoursePaymentContributionIsActive(boolean adminTrainerCoursePaymentContributionIsActive) {
		this.adminTrainerCoursePaymentContributionIsActive = adminTrainerCoursePaymentContributionIsActive;
	}

	public boolean isAdminTrainerCoursePaymentContributionIsDeletedByAdmin() {
		return adminTrainerCoursePaymentContributionIsDeletedByAdmin;
	}

	public void setAdminTrainerCoursePaymentContributionIsDeletedByAdmin(
			boolean adminTrainerCoursePaymentContributionIsDeletedByAdmin) {
		this.adminTrainerCoursePaymentContributionIsDeletedByAdmin = adminTrainerCoursePaymentContributionIsDeletedByAdmin;
	}

	public LocalDateTime getAdminTrainerCoursePaymentContributionLastUpdatedDateTime() {
		return adminTrainerCoursePaymentContributionLastUpdatedDateTime;
	}

	public void setAdminTrainerCoursePaymentContributionLastUpdatedDateTime(
			LocalDateTime adminTrainerCoursePaymentContributionLastUpdatedDateTime) {
		this.adminTrainerCoursePaymentContributionLastUpdatedDateTime = adminTrainerCoursePaymentContributionLastUpdatedDateTime;
	}

	public LocalDateTime getAdminTrainerCoursePaymentContributionCreatedDateTime() {
		return adminTrainerCoursePaymentContributionCreatedDateTime;
	}

	public void setAdminTrainerCoursePaymentContributionCreatedDateTime(
			LocalDateTime adminTrainerCoursePaymentContributionCreatedDateTime) {
		this.adminTrainerCoursePaymentContributionCreatedDateTime = adminTrainerCoursePaymentContributionCreatedDateTime;
	}

	@Override
	public String toString() {
		return "CoursePaymentContributionInfo [adminTrainerCoursePaymentContributionId="
				+ adminTrainerCoursePaymentContributionId + ", adminTrainerCoursePaymentContributionPercentage="
				+ adminTrainerCoursePaymentContributionPercentage + ", adminTrainerCoursePaymentContributionIsActive="
				+ adminTrainerCoursePaymentContributionIsActive
				+ ", adminTrainerCoursePaymentContributionIsDeletedByAdmin="
				+ adminTrainerCoursePaymentContributionIsDeletedByAdmin
				+ ", adminTrainerCoursePaymentContributionLastUpdatedDateTime="
				+ adminTrainerCoursePaymentContributionLastUpdatedDateTime
				+ ", adminTrainerCoursePaymentContributionCreatedDateTime="
				+ adminTrainerCoursePaymentContributionCreatedDateTime + "]";
	}
	
}
