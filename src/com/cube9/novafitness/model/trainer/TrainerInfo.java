package com.cube9.novafitness.model.trainer;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

@Entity(name = "trainer_info")
public class TrainerInfo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "trainer_id")
	private Long trainerId;

	@Column(name = "trainer_title")
	private String trainerTitle;

	@Column(name = "trainer_fname")
	private String trainerFName;

	@Column(name = "trainer_lname")
	private String trainerLName;

	@Column(name = "trainer_email", unique = true)
	private String trainerEmail;

	@Column(name = "trainer_password", columnDefinition = "VARCHAR(5000)" )
	private String trainerPassword;
	
	@Column(name = "trainer_description", columnDefinition = "VARCHAR(5000)" )
	private String trainerDescription;
	
	@Column(name = "trainer_website_link", columnDefinition = "VARCHAR(5000)" )
	private String trainerWebsiteLink;
	
	@Column(name = "trainer_facebook_link", columnDefinition = "VARCHAR(5000)" )
	private String trainerFacebookLink;
	
	@Column(name = "trainer_instagram_link", columnDefinition = "VARCHAR(5000)" )
	private String trainerInstagramLink;
	
	@Column(name = "trainer_youtube_link", columnDefinition = "VARCHAR(5000)" )
	private String trainerYoutubeLink;
	
	@Column(name = "trainer_paypal_id", columnDefinition = "VARCHAR(500)" )
	private String trainerPayPalId;

	@Column(name = "trainer_phone_no")
	private Long trainerPhoneNo;

	@OneToOne
	@JoinColumn(name = "trainer_profile_image_id")
	private TrainerProfileImageInfo trainerProfileImageInfo;
	
	@OneToOne
	@JoinColumn(name = "trainer_certificate_id")
	private TrainerCertificateInfo trainerCertificateInfo;
	
	@OneToOne
	@JoinColumn(name = "trainer_idCopy_id")
	private TrainerIDCopyInfo trainerIDCopyInfo;
	
	@Column(name = "is_Active", columnDefinition = "boolean default false")
	private boolean trainerStatus=false;
	
	@Column(name = "is_Approved_by_admin", columnDefinition = "boolean default false")
	private boolean trainerIsApprovedByAdmin=false;
	
	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean trainerSAdminDeleteStatus=false;

	@Column(name = "trainer_last_login_dateTime")
	private LocalDateTime trainerLastLoginDateTime;
	
	@Column(name = "trainer_current_dateTime")
	@CreationTimestamp
	private LocalDateTime trainerCurrentDateTime;

	public Long getTrainerId() {
		return trainerId;
	}

	public void setTrainerId(Long trainerId) {
		this.trainerId = trainerId;
	}

	public String getTrainerTitle() {
		return trainerTitle;
	}

	public void setTrainerTitle(String trainerTitle) {
		this.trainerTitle = trainerTitle;
	}

	public String getTrainerFName() {
		return trainerFName;
	}

	public void setTrainerFName(String trainerFName) {
		this.trainerFName = trainerFName;
	}

	public String getTrainerLName() {
		return trainerLName;
	}

	public void setTrainerLName(String trainerLName) {
		this.trainerLName = trainerLName;
	}

	public String getTrainerEmail() {
		return trainerEmail;
	}

	public void setTrainerEmail(String trainerEmail) {
		this.trainerEmail = trainerEmail;
	}

	public String getTrainerPassword() {
		return trainerPassword;
	}

	public void setTrainerPassword(String trainerPassword) {
		this.trainerPassword = trainerPassword;
	}

	public String getTrainerWebsiteLink() {
		return trainerWebsiteLink;
	}

	public void setTrainerWebsiteLink(String trainerWebsiteLink) {
		this.trainerWebsiteLink = trainerWebsiteLink;
	}

	public String getTrainerFacebookLink() {
		return trainerFacebookLink;
	}

	public void setTrainerFacebookLink(String trainerFacebookLink) {
		this.trainerFacebookLink = trainerFacebookLink;
	}

	public String getTrainerInstagramLink() {
		return trainerInstagramLink;
	}

	public void setTrainerInstagramLink(String trainerInstagramLink) {
		this.trainerInstagramLink = trainerInstagramLink;
	}

	public String getTrainerYoutubeLink() {
		return trainerYoutubeLink;
	}

	public void setTrainerYoutubeLink(String trainerYoutubeLink) {
		this.trainerYoutubeLink = trainerYoutubeLink;
	}

	public String getTrainerDescription() {
		return trainerDescription;
	}

	public void setTrainerDescription(String trainerDescription) {
		this.trainerDescription = trainerDescription;
	}

	public Long getTrainerPhoneNo() {
		return trainerPhoneNo;
	}

	public void setTrainerPhoneNo(Long trainerPhoneNo) {
		this.trainerPhoneNo = trainerPhoneNo;
	}

	public TrainerProfileImageInfo getTrainerProfileImageInfo() {
		return trainerProfileImageInfo;
	}

	public void setTrainerProfileImageInfo(TrainerProfileImageInfo trainerProfileImageInfo) {
		this.trainerProfileImageInfo = trainerProfileImageInfo;
	}

	public TrainerCertificateInfo getTrainerCertificateInfo() {
		return trainerCertificateInfo;
	}

	public void setTrainerCertificateInfo(TrainerCertificateInfo trainerCertificateInfo) {
		this.trainerCertificateInfo = trainerCertificateInfo;
	}

	public TrainerIDCopyInfo getTrainerIDCopyInfo() {
		return trainerIDCopyInfo;
	}

	public void setTrainerIDCopyInfo(TrainerIDCopyInfo trainerIDCopyInfo) {
		this.trainerIDCopyInfo = trainerIDCopyInfo;
	}

	public boolean isTrainerStatus() {
		return trainerStatus;
	}

	public void setTrainerStatus(boolean trainerStatus) {
		this.trainerStatus = trainerStatus;
	}

	public boolean isTrainerIsApprovedByAdmin() {
		return trainerIsApprovedByAdmin;
	}

	public void setTrainerIsApprovedByAdmin(boolean trainerIsApprovedByAdmin) {
		this.trainerIsApprovedByAdmin = trainerIsApprovedByAdmin;
	}

	public boolean isTrainerSAdminDeleteStatus() {
		return trainerSAdminDeleteStatus;
	}

	public void setTrainerSAdminDeleteStatus(boolean trainerSAdminDeleteStatus) {
		this.trainerSAdminDeleteStatus = trainerSAdminDeleteStatus;
	}

	public LocalDateTime getTrainerLastLoginDateTime() {
		return trainerLastLoginDateTime;
	}

	public void setTrainerLastLoginDateTime(LocalDateTime trainerLastLoginDateTime) {
		this.trainerLastLoginDateTime = trainerLastLoginDateTime;
	}

	public LocalDateTime getTrainerCurrentDateTime() {
		return trainerCurrentDateTime;
	}

	public void setTrainerCurrentDateTime(LocalDateTime trainerCurrentDateTime) {
		this.trainerCurrentDateTime = trainerCurrentDateTime;
	}

	public String getTrainerPayPalId() {
		return trainerPayPalId;
	}

	public void setTrainerPayPalId(String trainerPayPalId) {
		this.trainerPayPalId = trainerPayPalId;
	}

	@Override
	public String toString() {
		return "TrainerInfo [trainerId=" + trainerId + ", trainerTitle=" + trainerTitle + ", trainerFName="
				+ trainerFName + ", trainerLName=" + trainerLName + ", trainerEmail=" + trainerEmail
				+ ", trainerPassword=" + trainerPassword + ", trainerDescription=" + trainerDescription
				+ ", trainerWebsiteLink=" + trainerWebsiteLink + ", trainerFacebookLink=" + trainerFacebookLink
				+ ", trainerInstagramLink=" + trainerInstagramLink + ", trainerYoutubeLink=" + trainerYoutubeLink
				+ ", trainerPayPalId=" + trainerPayPalId + ", trainerPhoneNo=" + trainerPhoneNo
				+ ", trainerProfileImageInfo=" + trainerProfileImageInfo + ", trainerCertificateInfo="
				+ trainerCertificateInfo + ", trainerIDCopyInfo=" + trainerIDCopyInfo + ", trainerStatus="
				+ trainerStatus + ", trainerIsApprovedByAdmin=" + trainerIsApprovedByAdmin
				+ ", trainerSAdminDeleteStatus=" + trainerSAdminDeleteStatus + ", trainerLastLoginDateTime="
				+ trainerLastLoginDateTime + ", trainerCurrentDateTime=" + trainerCurrentDateTime + "]";
	}
	
}
