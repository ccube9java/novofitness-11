package com.cube9.novafitness.model.trainer;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

@Entity(name = "trainer_course_chapter_lesson_info")
public class TrainerCourseChapterLessonInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "trainer_course_chapter_lesson_id")
	private Long trainerCourseChapterLessonId;
	
	@Column(name = "trainer_course_chapter_lesson_name", columnDefinition = "VARCHAR(500)")
	private String trainerCourseChapterLessonName;

	@Column(name = "trainer_course_chapter_lesson_main_video_name")
	private String trainerCourseChapterLessonVideoName;
	
	@Column(name = "trainer_course_chapter_lesson_main_video_stored_path")
	private String trainerCourseChapterLessonMainVideoStoredPath;
	
	@Column(name = "trainer_course_chapter_lesson_main_video_view_path")
	private String trainerCourseChapterLessonMainVideoViewPath;
	
	@Column(name = "trainer_course_chapter_lesson_main_video_file_type")
	private String trainerCourseChapterLessonMainVideoFileType;
	
	@Column(name = "trainer_course_chapter_lesson_main_video_vimeo_link")
	private String trainerCourseChapterLessonMainVideoVimeoLink;
	
	@Column(name = "trainer_course_chapter_lesson_main_video_vimeo_name")
	private String trainerCourseChapterLessonMainVideoVimeoName;
	
	@Column(name = "trainer_course_chapter_lesson_preview_video_stored_path")
	private String trainerCourseChapterLessonPreviewVideoStoredPath;
	
	@Column(name = "trainer_course_chapter_lesson_preview_video_view_path")
	private String trainerCourseChapterLessonPreviewVideoViewPath;
	
	@Column(name = "trainer_course_chapter_lesson_preview_video_file_type")
	private String trainerCourseChapterLessonPreviewVideoFileType;
	
	@Column(name = "trainer_course_chapter_lesson_preview_video_vimeo_link")
	private String trainerCourseChapterLessonpreviewVideoVimeoLink;
	
	@Column(name = "trainer_course_chapter_lesson_preview_video_vimeo_name")
	private String trainerCourseChapterLessonPreviewVideoVimeoName;
	
	@Column(name = "trainer_course_chapter_lesson_main_pdf_name")
	private String trainerCourseChapterLessonMainPdfName;
	
	@Column(name = "trainer_course_chapter_lesson_main_pdf_stored_path")
	private String trainerCourseChapterLessonMainPdfStoredPath;
	
	@Column(name = "trainer_course_chapter_lesson_main_pdf_view_path")
	private String trainerCourseChapterLessonMainPdfViewPath;
	
	@Column(name = "trainer_course_chapter_lesson_main_pdf_file_type")
	private String trainerCourseChapterLessonMainPdfFileType;
	
	@OneToOne
	@JoinColumn(name="trainer_course_chapter_id")
	private TrainerCourseChapterInfo trainerCourseChapterInfo;
	
	@Column(name = "is_Active", columnDefinition = "boolean default true")
	private boolean trainerCourseChapterLessonIsActive=true;
	
	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean trainerCourseChapterLessonIsDeletedByAdmin=false;
	
	@Column(name = "trainer_course_chapter_lesson_last_updated_dateTime")
	private LocalDateTime trainerCourseChapterLessonLastUpdatedDateTime;
	
	@Column(name = "trainer_course_chapter_lesson_created_dateTime")
	@CreationTimestamp
	private LocalDateTime trainerCourseChapterLessonCreatedDateTime;

	public Long getTrainerCourseChapterLessonId() {
		return trainerCourseChapterLessonId;
	}

	public void setTrainerCourseChapterLessonId(Long trainerCourseChapterLessonId) {
		this.trainerCourseChapterLessonId = trainerCourseChapterLessonId;
	}

	public String getTrainerCourseChapterLessonName() {
		return trainerCourseChapterLessonName;
	}

	public void setTrainerCourseChapterLessonName(String trainerCourseChapterLessonName) {
		this.trainerCourseChapterLessonName = trainerCourseChapterLessonName;
	}

	public TrainerCourseChapterInfo getTrainerCourseChapterInfo() {
		return trainerCourseChapterInfo;
	}

	public void setTrainerCourseChapterInfo(TrainerCourseChapterInfo trainerCourseChapterInfo) {
		this.trainerCourseChapterInfo = trainerCourseChapterInfo;
	}

	public String getTrainerCourseChapterLessonVideoName() {
		return trainerCourseChapterLessonVideoName;
	}

	public void setTrainerCourseChapterLessonVideoName(String trainerCourseChapterLessonVideoName) {
		this.trainerCourseChapterLessonVideoName = trainerCourseChapterLessonVideoName;
	}

	public String getTrainerCourseChapterLessonMainVideoStoredPath() {
		return trainerCourseChapterLessonMainVideoStoredPath;
	}

	public void setTrainerCourseChapterLessonMainVideoStoredPath(String trainerCourseChapterLessonMainVideoStoredPath) {
		this.trainerCourseChapterLessonMainVideoStoredPath = trainerCourseChapterLessonMainVideoStoredPath;
	}

	public String getTrainerCourseChapterLessonMainVideoViewPath() {
		return trainerCourseChapterLessonMainVideoViewPath;
	}

	public void setTrainerCourseChapterLessonMainVideoViewPath(String trainerCourseChapterLessonMainVideoViewPath) {
		this.trainerCourseChapterLessonMainVideoViewPath = trainerCourseChapterLessonMainVideoViewPath;
	}

	public String getTrainerCourseChapterLessonMainVideoFileType() {
		return trainerCourseChapterLessonMainVideoFileType;
	}

	public void setTrainerCourseChapterLessonMainVideoFileType(String trainerCourseChapterLessonMainVideoFileType) {
		this.trainerCourseChapterLessonMainVideoFileType = trainerCourseChapterLessonMainVideoFileType;
	}

	public String getTrainerCourseChapterLessonMainVideoVimeoLink() {
		return trainerCourseChapterLessonMainVideoVimeoLink;
	}

	public void setTrainerCourseChapterLessonMainVideoVimeoLink(String trainerCourseChapterLessonMainVideoVimeoLink) {
		this.trainerCourseChapterLessonMainVideoVimeoLink = trainerCourseChapterLessonMainVideoVimeoLink;
	}

	public String getTrainerCourseChapterLessonMainVideoVimeoName() {
		return trainerCourseChapterLessonMainVideoVimeoName;
	}

	public void setTrainerCourseChapterLessonMainVideoVimeoName(String trainerCourseChapterLessonMainVideoVimeoName) {
		this.trainerCourseChapterLessonMainVideoVimeoName = trainerCourseChapterLessonMainVideoVimeoName;
	}

	public String getTrainerCourseChapterLessonPreviewVideoStoredPath() {
		return trainerCourseChapterLessonPreviewVideoStoredPath;
	}

	public void setTrainerCourseChapterLessonPreviewVideoStoredPath(
			String trainerCourseChapterLessonPreviewVideoStoredPath) {
		this.trainerCourseChapterLessonPreviewVideoStoredPath = trainerCourseChapterLessonPreviewVideoStoredPath;
	}

	public String getTrainerCourseChapterLessonPreviewVideoViewPath() {
		return trainerCourseChapterLessonPreviewVideoViewPath;
	}

	public void setTrainerCourseChapterLessonPreviewVideoViewPath(String trainerCourseChapterLessonPreviewVideoViewPath) {
		this.trainerCourseChapterLessonPreviewVideoViewPath = trainerCourseChapterLessonPreviewVideoViewPath;
	}

	public String getTrainerCourseChapterLessonPreviewVideoFileType() {
		return trainerCourseChapterLessonPreviewVideoFileType;
	}

	public void setTrainerCourseChapterLessonPreviewVideoFileType(String trainerCourseChapterLessonPreviewVideoFileType) {
		this.trainerCourseChapterLessonPreviewVideoFileType = trainerCourseChapterLessonPreviewVideoFileType;
	}

	public String getTrainerCourseChapterLessonpreviewVideoVimeoLink() {
		return trainerCourseChapterLessonpreviewVideoVimeoLink;
	}

	public void setTrainerCourseChapterLessonpreviewVideoVimeoLink(String trainerCourseChapterLessonpreviewVideoVimeoLink) {
		this.trainerCourseChapterLessonpreviewVideoVimeoLink = trainerCourseChapterLessonpreviewVideoVimeoLink;
	}

	public String getTrainerCourseChapterLessonPreviewVideoVimeoName() {
		return trainerCourseChapterLessonPreviewVideoVimeoName;
	}

	public void setTrainerCourseChapterLessonPreviewVideoVimeoName(String trainerCourseChapterLessonPreviewVideoVimeoName) {
		this.trainerCourseChapterLessonPreviewVideoVimeoName = trainerCourseChapterLessonPreviewVideoVimeoName;
	}

	public String getTrainerCourseChapterLessonMainPdfName() {
		return trainerCourseChapterLessonMainPdfName;
	}

	public void setTrainerCourseChapterLessonMainPdfName(String trainerCourseChapterLessonMainPdfName) {
		this.trainerCourseChapterLessonMainPdfName = trainerCourseChapterLessonMainPdfName;
	}

	public String getTrainerCourseChapterLessonMainPdfStoredPath() {
		return trainerCourseChapterLessonMainPdfStoredPath;
	}

	public void setTrainerCourseChapterLessonMainPdfStoredPath(String trainerCourseChapterLessonMainPdfStoredPath) {
		this.trainerCourseChapterLessonMainPdfStoredPath = trainerCourseChapterLessonMainPdfStoredPath;
	}

	public String getTrainerCourseChapterLessonMainPdfViewPath() {
		return trainerCourseChapterLessonMainPdfViewPath;
	}

	public void setTrainerCourseChapterLessonMainPdfViewPath(String trainerCourseChapterLessonMainPdfViewPath) {
		this.trainerCourseChapterLessonMainPdfViewPath = trainerCourseChapterLessonMainPdfViewPath;
	}

	public String getTrainerCourseChapterLessonMainPdfFileType() {
		return trainerCourseChapterLessonMainPdfFileType;
	}

	public void setTrainerCourseChapterLessonMainPdfFileType(String trainerCourseChapterLessonMainPdfFileType) {
		this.trainerCourseChapterLessonMainPdfFileType = trainerCourseChapterLessonMainPdfFileType;
	}

	public boolean isTrainerCourseChapterLessonIsActive() {
		return trainerCourseChapterLessonIsActive;
	}

	public void setTrainerCourseChapterLessonIsActive(boolean trainerCourseChapterLessonIsActive) {
		this.trainerCourseChapterLessonIsActive = trainerCourseChapterLessonIsActive;
	}

	public boolean isTrainerCourseChapterLessonIsDeletedByAdmin() {
		return trainerCourseChapterLessonIsDeletedByAdmin;
	}

	public void setTrainerCourseChapterLessonIsDeletedByAdmin(boolean trainerCourseChapterLessonIsDeletedByAdmin) {
		this.trainerCourseChapterLessonIsDeletedByAdmin = trainerCourseChapterLessonIsDeletedByAdmin;
	}

	public LocalDateTime getTrainerCourseChapterLessonLastUpdatedDateTime() {
		return trainerCourseChapterLessonLastUpdatedDateTime;
	}

	public void setTrainerCourseChapterLessonLastUpdatedDateTime(
			LocalDateTime trainerCourseChapterLessonLastUpdatedDateTime) {
		this.trainerCourseChapterLessonLastUpdatedDateTime = trainerCourseChapterLessonLastUpdatedDateTime;
	}

	public LocalDateTime getTrainerCourseChapterLessonCreatedDateTime() {
		return trainerCourseChapterLessonCreatedDateTime;
	}

	public void setTrainerCourseChapterLessonCreatedDateTime(LocalDateTime trainerCourseChapterLessonCreatedDateTime) {
		this.trainerCourseChapterLessonCreatedDateTime = trainerCourseChapterLessonCreatedDateTime;
	}

	@Override
	public String toString() {
		return "TrainerCourseChapterLessonInfo [trainerCourseChapterLessonId=" + trainerCourseChapterLessonId
				+ ", trainerCourseChapterLessonName=" + trainerCourseChapterLessonName
				+ ", trainerCourseChapterLessonVideoName=" + trainerCourseChapterLessonVideoName
				+ ", trainerCourseChapterLessonMainVideoStoredPath=" + trainerCourseChapterLessonMainVideoStoredPath
				+ ", trainerCourseChapterLessonMainVideoViewPath=" + trainerCourseChapterLessonMainVideoViewPath
				+ ", trainerCourseChapterLessonMainVideoFileType=" + trainerCourseChapterLessonMainVideoFileType
				+ ", trainerCourseChapterLessonMainVideoVimeoLink=" + trainerCourseChapterLessonMainVideoVimeoLink
				+ ", trainerCourseChapterLessonMainVideoVimeoName=" + trainerCourseChapterLessonMainVideoVimeoName
				+ ", trainerCourseChapterLessonPreviewVideoStoredPath="
				+ trainerCourseChapterLessonPreviewVideoStoredPath + ", trainerCourseChapterLessonPreviewVideoViewPath="
				+ trainerCourseChapterLessonPreviewVideoViewPath + ", trainerCourseChapterLessonPreviewVideoFileType="
				+ trainerCourseChapterLessonPreviewVideoFileType + ", trainerCourseChapterLessonpreviewVideoVimeoLink="
				+ trainerCourseChapterLessonpreviewVideoVimeoLink + ", trainerCourseChapterLessonPreviewVideoVimeoName="
				+ trainerCourseChapterLessonPreviewVideoVimeoName + ", trainerCourseChapterLessonMainPdfName="
				+ trainerCourseChapterLessonMainPdfName + ", trainerCourseChapterLessonMainPdfStoredPath="
				+ trainerCourseChapterLessonMainPdfStoredPath + ", trainerCourseChapterLessonMainPdfViewPath="
				+ trainerCourseChapterLessonMainPdfViewPath + ", trainerCourseChapterLessonMainPdfFileType="
				+ trainerCourseChapterLessonMainPdfFileType + ", trainerCourseChapterInfo=" + trainerCourseChapterInfo
				+ ", trainerCourseChapterLessonIsActive=" + trainerCourseChapterLessonIsActive
				+ ", trainerCourseChapterLessonIsDeletedByAdmin=" + trainerCourseChapterLessonIsDeletedByAdmin
				+ ", trainerCourseChapterLessonLastUpdatedDateTime=" + trainerCourseChapterLessonLastUpdatedDateTime
				+ ", trainerCourseChapterLessonCreatedDateTime=" + trainerCourseChapterLessonCreatedDateTime + "]";
	}
	
}
