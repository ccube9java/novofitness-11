package com.cube9.novafitness.model.trainer;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

import com.cube9.novafitness.model.course.CourseInfo;

@Entity(name = "trainer_course_info")
public class TrainerCourseInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "trainer_course_id")
	private Long trainerCourseId;

	@OneToOne
	@JoinColumn(name = "trainer_id")
	private TrainerInfo trainerInfo;

	@OneToOne
	@JoinColumn(name = "course_id")
	private CourseInfo trainerCourseInfo;

	@Column(name = "is_Active", columnDefinition = "boolean default true")
	private boolean trainerCourseIsActive = true;

	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean trainerCourseIsDeletedByAdmin = false;

	@Column(name = "trainer_course_last_updated_dateTime")
	private LocalDateTime trainerCourseLastUpdatedDateTime;

	@Column(name = "trainer_course_created_dateTime")
	@CreationTimestamp
	private LocalDateTime trainerCourseCreatedDateTime;

	public Long getTrainerCourseId() {
		return trainerCourseId;
	}

	public void setTrainerCourseId(Long trainerCourseId) {
		this.trainerCourseId = trainerCourseId;
	}

	public TrainerInfo getTrainerInfo() {
		return trainerInfo;
	}

	public void setTrainerInfo(TrainerInfo trainerInfo) {
		this.trainerInfo = trainerInfo;
	}

	public CourseInfo getTrainerCourseInfo() {
		return trainerCourseInfo;
	}

	public void setTrainerCourseInfo(CourseInfo trainerCourseInfo) {
		this.trainerCourseInfo = trainerCourseInfo;
	}

	public boolean isTrainerCourseIsActive() {
		return trainerCourseIsActive;
	}

	public void setTrainerCourseIsActive(boolean trainerCourseIsActive) {
		this.trainerCourseIsActive = trainerCourseIsActive;
	}

	public boolean isTrainerCourseIsDeletedByAdmin() {
		return trainerCourseIsDeletedByAdmin;
	}

	public void setTrainerCourseIsDeletedByAdmin(boolean trainerCourseIsDeletedByAdmin) {
		this.trainerCourseIsDeletedByAdmin = trainerCourseIsDeletedByAdmin;
	}

	public LocalDateTime getTrainerCourseLastUpdatedDateTime() {
		return trainerCourseLastUpdatedDateTime;
	}

	public void setTrainerCourseLastUpdatedDateTime(LocalDateTime trainerCourseLastUpdatedDateTime) {
		this.trainerCourseLastUpdatedDateTime = trainerCourseLastUpdatedDateTime;
	}

	public LocalDateTime getTrainerCourseCreatedDateTime() {
		return trainerCourseCreatedDateTime;
	}

	public void setTrainerCourseCreatedDateTime(LocalDateTime trainerCourseCreatedDateTime) {
		this.trainerCourseCreatedDateTime = trainerCourseCreatedDateTime;
	}

	@Override
	public String toString() {
		return "TrainerCourseInfo [trainerCourseId=" + trainerCourseId + ", trainerInfo=" + trainerInfo
				+ ", trainerCourseInfo=" + trainerCourseInfo + ", trainerCourseIsActive=" + trainerCourseIsActive
				+ ", trainerCourseIsDeletedByAdmin=" + trainerCourseIsDeletedByAdmin
				+ ", trainerCourseLastUpdatedDateTime=" + trainerCourseLastUpdatedDateTime
				+ ", trainerCourseCreatedDateTime=" + trainerCourseCreatedDateTime + "]";
	}

}
