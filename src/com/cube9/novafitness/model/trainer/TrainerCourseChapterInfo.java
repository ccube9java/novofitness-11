package com.cube9.novafitness.model.trainer;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

@Entity(name = "trainer_course_chapter_info")
public class TrainerCourseChapterInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "trainer_course_chapter_id")
	private Long trainerCourseChapterId;
	
	@Column(name = "trainer_course_chapter_name", columnDefinition = "VARCHAR(500)")
	private String trainerCourseChapterName;

	@OneToOne
	@JoinColumn(name="trainer_course_id")
	private TrainerCourseInfo trainerCourseInfo;
	
	@Column(name = "is_Active", columnDefinition = "boolean default true")
	private boolean trainerCourseChapterIsActive=true;
	
	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean trainerCourseChapterIsDeletedByAdmin=false;
	
	@Column(name = "trainer_course_chapter_last_updated_dateTime")
	private LocalDateTime trainerCourseChapterLastUpdatedDateTime;
	
	@Column(name = "trainer_course_chapter_created_dateTime")
	@CreationTimestamp
	private LocalDateTime trainerCourseChapterCreatedDateTime;

	public Long getTrainerCourseChapterId() {
		return trainerCourseChapterId;
	}

	public void setTrainerCourseChapterId(Long trainerCourseChapterId) {
		this.trainerCourseChapterId = trainerCourseChapterId;
	}

	public String getTrainerCourseChapterName() {
		return trainerCourseChapterName;
	}

	public void setTrainerCourseChapterName(String trainerCourseChapterName) {
		this.trainerCourseChapterName = trainerCourseChapterName;
	}

	public TrainerCourseInfo getTrainerCourseInfo() {
		return trainerCourseInfo;
	}

	public void setTrainerCourseInfo(TrainerCourseInfo trainerCourseInfo) {
		this.trainerCourseInfo = trainerCourseInfo;
	}

	public boolean isTrainerCourseChapterIsActive() {
		return trainerCourseChapterIsActive;
	}

	public void setTrainerCourseChapterIsActive(boolean trainerCourseChapterIsActive) {
		this.trainerCourseChapterIsActive = trainerCourseChapterIsActive;
	}

	public boolean isTrainerCourseChapterIsDeletedByAdmin() {
		return trainerCourseChapterIsDeletedByAdmin;
	}

	public void setTrainerCourseChapterIsDeletedByAdmin(boolean trainerCourseChapterIsDeletedByAdmin) {
		this.trainerCourseChapterIsDeletedByAdmin = trainerCourseChapterIsDeletedByAdmin;
	}

	public LocalDateTime getTrainerCourseChapterLastUpdatedDateTime() {
		return trainerCourseChapterLastUpdatedDateTime;
	}

	public void setTrainerCourseChapterLastUpdatedDateTime(LocalDateTime trainerCourseChapterLastUpdatedDateTime) {
		this.trainerCourseChapterLastUpdatedDateTime = trainerCourseChapterLastUpdatedDateTime;
	}

	public LocalDateTime getTrainerCourseChapterCreatedDateTime() {
		return trainerCourseChapterCreatedDateTime;
	}

	public void setTrainerCourseChapterCreatedDateTime(LocalDateTime trainerCourseChapterCreatedDateTime) {
		this.trainerCourseChapterCreatedDateTime = trainerCourseChapterCreatedDateTime;
	}

	@Override
	public String toString() {
		return "TrainerCourseChapterInfo [trainerCourseChapterId=" + trainerCourseChapterId
				+ ", trainerCourseChapterName=" + trainerCourseChapterName + ", trainerCourseInfo=" + trainerCourseInfo
				+ ", trainerCourseChapterIsActive=" + trainerCourseChapterIsActive
				+ ", trainerCourseChapterIsDeletedByAdmin=" + trainerCourseChapterIsDeletedByAdmin
				+ ", trainerCourseChapterLastUpdatedDateTime=" + trainerCourseChapterLastUpdatedDateTime
				+ ", trainerCourseChapterCreatedDateTime=" + trainerCourseChapterCreatedDateTime + "]";
	}
	
}
