package com.cube9.novafitness.model.trainer;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;

@Entity(name="trainer_idCopy_info")
public class TrainerIDCopyInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "trainer_idCopy_id")
	private Long trainerIDCopyId;
	
	@Column(name = "trainer_idCopy_name")
	private String trainerIDCopyName;
	
	@Column(name = "trainer_idCopy_number")
	private String trainerIDCopyNumber;
	
	@Column(name = "trainer_idCopy_stored_path")
	private String trainerIDCopyStoredPath;
	
	@Column(name = "trainer_idCopy_view_path")
	private String trainerIDCopyViewPath;
	
	@Column(name = "trainer_idCopy_file_type")
	private String trainerIDCopyFileType;
	
	@Column(name = "is_Active", columnDefinition = "boolean default true")
	private boolean trainerIDCopyStatus=true;
	
	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean trainerIDCopyAdminDeleteStatus=false;
	
	@Column(name = "trainer_idCopy_last_updated_dateTime")
	private LocalDateTime trainerIDCopyLastUpdatedDateTime;
	
	@Column(name = "trainer_idCopy_created_dateTime")
	@CreationTimestamp
	private LocalDateTime trainerIDCopyCreatedDateTime;

	public Long getTrainerIDCopyId() {
		return trainerIDCopyId;
	}

	public void setTrainerIDCopyId(Long trainerIDCopyId) {
		this.trainerIDCopyId = trainerIDCopyId;
	}

	public String getTrainerIDCopyName() {
		return trainerIDCopyName;
	}

	public void setTrainerIDCopyName(String trainerIDCopyName) {
		this.trainerIDCopyName = trainerIDCopyName;
	}

	public String getTrainerIDCopyNumber() {
		return trainerIDCopyNumber;
	}

	public void setTrainerIDCopyNumber(String trainerIDCopyNumber) {
		this.trainerIDCopyNumber = trainerIDCopyNumber;
	}

	public String getTrainerIDCopyStoredPath() {
		return trainerIDCopyStoredPath;
	}

	public void setTrainerIDCopyStoredPath(String trainerIDCopyStoredPath) {
		this.trainerIDCopyStoredPath = trainerIDCopyStoredPath;
	}

	public String getTrainerIDCopyViewPath() {
		return trainerIDCopyViewPath;
	}

	public void setTrainerIDCopyViewPath(String trainerIDCopyViewPath) {
		this.trainerIDCopyViewPath = trainerIDCopyViewPath;
	}

	public String getTrainerIDCopyFileType() {
		return trainerIDCopyFileType;
	}

	public void setTrainerIDCopyFileType(String trainerIDCopyFileType) {
		this.trainerIDCopyFileType = trainerIDCopyFileType;
	}

	public boolean isTrainerIDCopyStatus() {
		return trainerIDCopyStatus;
	}

	public void setTrainerIDCopyStatus(boolean trainerIDCopyStatus) {
		this.trainerIDCopyStatus = trainerIDCopyStatus;
	}

	public boolean isTrainerIDCopyAdminDeleteStatus() {
		return trainerIDCopyAdminDeleteStatus;
	}

	public void setTrainerIDCopyAdminDeleteStatus(boolean trainerIDCopyAdminDeleteStatus) {
		this.trainerIDCopyAdminDeleteStatus = trainerIDCopyAdminDeleteStatus;
	}

	public LocalDateTime getTrainerIDCopyLastUpdatedDateTime() {
		return trainerIDCopyLastUpdatedDateTime;
	}

	public void setTrainerIDCopyLastUpdatedDateTime(LocalDateTime trainerIDCopyLastUpdatedDateTime) {
		this.trainerIDCopyLastUpdatedDateTime = trainerIDCopyLastUpdatedDateTime;
	}

	public LocalDateTime getTrainerIDCopyCreatedDateTime() {
		return trainerIDCopyCreatedDateTime;
	}

	public void setTrainerIDCopyCreatedDateTime(LocalDateTime trainerIDCopyCreatedDateTime) {
		this.trainerIDCopyCreatedDateTime = trainerIDCopyCreatedDateTime;
	}

	@Override
	public String toString() {
		return "TrainerIDCopyInfo [trainerIDCopyId=" + trainerIDCopyId + ", trainerIDCopyName=" + trainerIDCopyName
				+ ", trainerIDCopyNumber=" + trainerIDCopyNumber + ", trainerIDCopyStoredPath="
				+ trainerIDCopyStoredPath + ", trainerIDCopyViewPath=" + trainerIDCopyViewPath
				+ ", trainerIDCopyFileType=" + trainerIDCopyFileType + ", trainerIDCopyStatus=" + trainerIDCopyStatus
				+ ", trainerIDCopyAdminDeleteStatus=" + trainerIDCopyAdminDeleteStatus
				+ ", trainerIDCopyLastUpdatedDateTime=" + trainerIDCopyLastUpdatedDateTime
				+ ", trainerIDCopyCreatedDateTime=" + trainerIDCopyCreatedDateTime + "]";
	}
	
}
