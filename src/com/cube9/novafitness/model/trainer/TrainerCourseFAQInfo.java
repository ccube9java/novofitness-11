package com.cube9.novafitness.model.trainer;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

@Entity(name = "trainer_course_faq_info")
public class TrainerCourseFAQInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "trainer_course_faq_id")
	private Long trainerCourseFAQId;
	
	@Column(name = "trainer_course_faq_question", columnDefinition = "VARCHAR(500)")
	private String trainerCourseFAQQuestion;

	@Column(name = "trainer_course_faq_answer", columnDefinition = "VARCHAR(5000)")
	private String trainerCourseFAQAnswer;
	
	@OneToOne
	@JoinColumn(name="trainer_course_id")
	private TrainerCourseInfo trainerCourseInfo;
	
	@Column(name = "is_Active", columnDefinition = "boolean default true")
	private boolean trainerCourseFAQIsActive=true;
	
	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean trainerCourseFAQIsDeletedByAdmin=false;
	
	@Column(name = "trainer_course_faq_last_updated_dateTime")
	private LocalDateTime trainerCourseFAQLastUpdatedDateTime;
	
	@Column(name = "trainer_course_faq_created_dateTime")
	@CreationTimestamp
	private LocalDateTime trainerCourseFAQCreatedDateTime;

	public Long getTrainerCourseFAQId() {
		return trainerCourseFAQId;
	}

	public void setTrainerCourseFAQId(Long trainerCourseFAQId) {
		this.trainerCourseFAQId = trainerCourseFAQId;
	}

	public String getTrainerCourseFAQQuestion() {
		return trainerCourseFAQQuestion;
	}

	public void setTrainerCourseFAQQuestion(String trainerCourseFAQQuestion) {
		this.trainerCourseFAQQuestion = trainerCourseFAQQuestion;
	}

	public String getTrainerCourseFAQAnswer() {
		return trainerCourseFAQAnswer;
	}

	public void setTrainerCourseFAQAnswer(String trainerCourseFAQAnswer) {
		this.trainerCourseFAQAnswer = trainerCourseFAQAnswer;
	}

	public TrainerCourseInfo getTrainerCourseInfo() {
		return trainerCourseInfo;
	}

	public void setTrainerCourseInfo(TrainerCourseInfo trainerCourseInfo) {
		this.trainerCourseInfo = trainerCourseInfo;
	}

	public boolean isTrainerCourseFAQIsActive() {
		return trainerCourseFAQIsActive;
	}

	public void setTrainerCourseFAQIsActive(boolean trainerCourseFAQIsActive) {
		this.trainerCourseFAQIsActive = trainerCourseFAQIsActive;
	}

	public boolean isTrainerCourseFAQIsDeletedByAdmin() {
		return trainerCourseFAQIsDeletedByAdmin;
	}

	public void setTrainerCourseFAQIsDeletedByAdmin(boolean trainerCourseFAQIsDeletedByAdmin) {
		this.trainerCourseFAQIsDeletedByAdmin = trainerCourseFAQIsDeletedByAdmin;
	}

	public LocalDateTime getTrainerCourseFAQLastUpdatedDateTime() {
		return trainerCourseFAQLastUpdatedDateTime;
	}

	public void setTrainerCourseFAQLastUpdatedDateTime(LocalDateTime trainerCourseFAQLastUpdatedDateTime) {
		this.trainerCourseFAQLastUpdatedDateTime = trainerCourseFAQLastUpdatedDateTime;
	}

	public LocalDateTime getTrainerCourseFAQCreatedDateTime() {
		return trainerCourseFAQCreatedDateTime;
	}

	public void setTrainerCourseFAQCreatedDateTime(LocalDateTime trainerCourseFAQCreatedDateTime) {
		this.trainerCourseFAQCreatedDateTime = trainerCourseFAQCreatedDateTime;
	}

	@Override
	public String toString() {
		return "TrainerCourseFAQInfo [trainerCourseFAQId=" + trainerCourseFAQId + ", trainerCourseFAQQuestion="
				+ trainerCourseFAQQuestion + ", trainerCourseFAQAnswer=" + trainerCourseFAQAnswer
				+ ", trainerCourseInfo=" + trainerCourseInfo + ", trainerCourseFAQIsActive=" + trainerCourseFAQIsActive
				+ ", trainerCourseFAQIsDeletedByAdmin=" + trainerCourseFAQIsDeletedByAdmin
				+ ", trainerCourseFAQLastUpdatedDateTime=" + trainerCourseFAQLastUpdatedDateTime
				+ ", trainerCourseFAQCreatedDateTime=" + trainerCourseFAQCreatedDateTime + "]";
	}
	
}
