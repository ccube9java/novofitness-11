package com.cube9.novafitness.model.trainer;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;

@Entity(name="trainer_profile_image_info")
public class TrainerProfileImageInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "trainer_profile_image_id")
	private Long trainerProfileImageId;
	
	@Column(name = "trainer_profile_image_name")
	private String trainerProfileImageName;
	
	@Column(name = "trainer_profile_image_stored_path")
	private String trainerProfileImageStoredPath;
	
	@Column(name = "trainer_profile_image_view_path")
	private String trainerProfileImageViewPath;
	
	@Column(name = "trainer_profile_image_file_type")
	private String trainerProfileImageFileType;
	
	@Column(name = "is_Active", columnDefinition = "boolean default true")
	private boolean trainerProfileImageStatus=true;
	
	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean trainerProfileImageAdminDeleteStatus=false;
	
	@Column(name = "trainer_profile_image_last_updated_dateTime")
	private LocalDateTime trainerProfileImageLastUpdatedDateTime;
	
	@Column(name = "trainer_profile_image_created_dateTime")
	@CreationTimestamp
	private LocalDateTime trainerProfileImageCreatedDateTime;

	public Long getTrainerProfileImageId() {
		return trainerProfileImageId;
	}

	public void setTrainerProfileImageId(Long trainerProfileImageId) {
		this.trainerProfileImageId = trainerProfileImageId;
	}

	public String getTrainerProfileImageName() {
		return trainerProfileImageName;
	}

	public void setTrainerProfileImageName(String trainerProfileImageName) {
		this.trainerProfileImageName = trainerProfileImageName;
	}

	public String getTrainerProfileImageStoredPath() {
		return trainerProfileImageStoredPath;
	}

	public void setTrainerProfileImageStoredPath(String trainerProfileImageStoredPath) {
		this.trainerProfileImageStoredPath = trainerProfileImageStoredPath;
	}

	public String getTrainerProfileImageViewPath() {
		return trainerProfileImageViewPath;
	}

	public void setTrainerProfileImageViewPath(String trainerProfileImageViewPath) {
		this.trainerProfileImageViewPath = trainerProfileImageViewPath;
	}

	public String getTrainerProfileImageFileType() {
		return trainerProfileImageFileType;
	}

	public void setTrainerProfileImageFileType(String trainerProfileImageFileType) {
		this.trainerProfileImageFileType = trainerProfileImageFileType;
	}

	public boolean isTrainerProfileImageStatus() {
		return trainerProfileImageStatus;
	}

	public void setTrainerProfileImageStatus(boolean trainerProfileImageStatus) {
		this.trainerProfileImageStatus = trainerProfileImageStatus;
	}

	public boolean isTrainerProfileImageAdminDeleteStatus() {
		return trainerProfileImageAdminDeleteStatus;
	}

	public void setTrainerProfileImageAdminDeleteStatus(boolean trainerProfileImageAdminDeleteStatus) {
		this.trainerProfileImageAdminDeleteStatus = trainerProfileImageAdminDeleteStatus;
	}

	public LocalDateTime getTrainerProfileImageLastUpdatedDateTime() {
		return trainerProfileImageLastUpdatedDateTime;
	}

	public void setTrainerProfileImageLastUpdatedDateTime(LocalDateTime trainerProfileImageLastUpdatedDateTime) {
		this.trainerProfileImageLastUpdatedDateTime = trainerProfileImageLastUpdatedDateTime;
	}

	public LocalDateTime getTrainerProfileImageCreatedDateTime() {
		return trainerProfileImageCreatedDateTime;
	}

	public void setTrainerProfileImageCreatedDateTime(LocalDateTime trainerProfileImageCreatedDateTime) {
		this.trainerProfileImageCreatedDateTime = trainerProfileImageCreatedDateTime;
	}

	@Override
	public String toString() {
		return "TrainerProfileImageInfo [trainerProfileImageId=" + trainerProfileImageId + ", trainerProfileImageName="
				+ trainerProfileImageName + ", trainerProfileImageStoredPath=" + trainerProfileImageStoredPath
				+ ", trainerProfileImageViewPath=" + trainerProfileImageViewPath + ", trainerProfileImageFileType="
				+ trainerProfileImageFileType + ", trainerProfileImageStatus=" + trainerProfileImageStatus
				+ ", trainerProfileImageAdminDeleteStatus=" + trainerProfileImageAdminDeleteStatus
				+ ", trainerProfileImageLastUpdatedDateTime=" + trainerProfileImageLastUpdatedDateTime
				+ ", trainerProfileImageCreatedDateTime=" + trainerProfileImageCreatedDateTime + "]";
	}
	
}
