package com.cube9.novafitness.model.trainer;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;

@Entity(name="trainer_certificate_info")
public class TrainerCertificateInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "trainer_certificate_id")
	private Long trainerCertificateId;
	
	@Column(name = "trainer_certificate_name")
	private String trainerCertificateName;
	
	@Column(name = "trainer_certificate_number")
	private String trainerCertificateNumber;
	
	@Column(name = "trainer_certificate_stored_path")
	private String trainerCertificateStoredPath;
	
	@Column(name = "trainer_certificate_view_path")
	private String trainerCertificateViewPath;
	
	@Column(name = "trainer_certificate_file_type")
	private String trainerCertificateFileType;
	
	@Column(name = "is_Active", columnDefinition = "boolean default true")
	private boolean trainerCertificateStatus=true;
	
	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean trainerCertificateAdminDeleteStatus=false;
	
	@Column(name = "trainer_certificate_last_updated_dateTime")
	private LocalDateTime trainerCertificateLastUpdatedDateTime;
	
	@Column(name = "trainer_certificate_created_dateTime")
	@CreationTimestamp
	private LocalDateTime trainerCertificateCreatedDateTime;

	public Long getTrainerCertificateId() {
		return trainerCertificateId;
	}

	public void setTrainerCertificateId(Long trainerCertificateId) {
		this.trainerCertificateId = trainerCertificateId;
	}

	public String getTrainerCertificateName() {
		return trainerCertificateName;
	}

	public void setTrainerCertificateName(String trainerCertificateName) {
		this.trainerCertificateName = trainerCertificateName;
	}

	public String getTrainerCertificateNumber() {
		return trainerCertificateNumber;
	}

	public void setTrainerCertificateNumber(String trainerCertificateNumber) {
		this.trainerCertificateNumber = trainerCertificateNumber;
	}

	public String getTrainerCertificateStoredPath() {
		return trainerCertificateStoredPath;
	}

	public void setTrainerCertificateStoredPath(String trainerCertificateStoredPath) {
		this.trainerCertificateStoredPath = trainerCertificateStoredPath;
	}

	public String getTrainerCertificateViewPath() {
		return trainerCertificateViewPath;
	}

	public void setTrainerCertificateViewPath(String trainerCertificateViewPath) {
		this.trainerCertificateViewPath = trainerCertificateViewPath;
	}

	public String getTrainerCertificateFileType() {
		return trainerCertificateFileType;
	}

	public void setTrainerCertificateFileType(String trainerCertificateFileType) {
		this.trainerCertificateFileType = trainerCertificateFileType;
	}

	public boolean isTrainerCertificateStatus() {
		return trainerCertificateStatus;
	}

	public void setTrainerCertificateStatus(boolean trainerCertificateStatus) {
		this.trainerCertificateStatus = trainerCertificateStatus;
	}

	public boolean isTrainerCertificateAdminDeleteStatus() {
		return trainerCertificateAdminDeleteStatus;
	}

	public void setTrainerCertificateAdminDeleteStatus(boolean trainerCertificateAdminDeleteStatus) {
		this.trainerCertificateAdminDeleteStatus = trainerCertificateAdminDeleteStatus;
	}

	public LocalDateTime getTrainerCertificateLastUpdatedDateTime() {
		return trainerCertificateLastUpdatedDateTime;
	}

	public void setTrainerCertificateLastUpdatedDateTime(LocalDateTime trainerCertificateLastUpdatedDateTime) {
		this.trainerCertificateLastUpdatedDateTime = trainerCertificateLastUpdatedDateTime;
	}

	public LocalDateTime getTrainerCertificateCreatedDateTime() {
		return trainerCertificateCreatedDateTime;
	}

	public void setTrainerCertificateCreatedDateTime(LocalDateTime trainerCertificateCreatedDateTime) {
		this.trainerCertificateCreatedDateTime = trainerCertificateCreatedDateTime;
	}

	@Override
	public String toString() {
		return "TrainerCertificateInfo [trainerCertificateId=" + trainerCertificateId + ", trainerCertificateName="
				+ trainerCertificateName + ", trainerCertificateNumber=" + trainerCertificateNumber
				+ ", trainerCertificateStoredPath=" + trainerCertificateStoredPath + ", trainerCertificateViewPath="
				+ trainerCertificateViewPath + ", trainerCertificateFileType=" + trainerCertificateFileType
				+ ", trainerCertificateStatus=" + trainerCertificateStatus + ", trainerCertificateAdminDeleteStatus="
				+ trainerCertificateAdminDeleteStatus + ", trainerCertificateLastUpdatedDateTime="
				+ trainerCertificateLastUpdatedDateTime + ", trainerCertificateCreatedDateTime="
				+ trainerCertificateCreatedDateTime + "]";
	}
	
}
