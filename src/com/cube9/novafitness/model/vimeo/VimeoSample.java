package com.cube9.novafitness.model.vimeo;

	import java.io.File;

	public class VimeoSample {

	  public static void main(String[] args) throws Exception {
	    //Vimeo vimeo = new Vimeo("[6dc54e39c92a6f9e1432789ad84588a9]"); 
		  Vimeo vimeo = new Vimeo("f161b3f9226918f4fafdd2331b48daa4");   
	    //add a video
	    boolean upgradeTo1080 = true;
	    String videoEndPoint = vimeo.addVideo(new File("/home/cube9/Downloads/SampleVideo_1280x720_1mb.mp4"), upgradeTo1080);
	    //String videoEndPoint = vimeo.addVideo(new File("/home/cube9/Downloads/VID_20200312_103049.mp4"), upgradeTo1080);
	    
	    //get video info
	    VimeoResponse info = vimeo.getVideoInfo(videoEndPoint);
	   // System.out.println("info::::::::::::::::::"+info.getJson().getJSONObject("link"));
	    System.out.println("info::::::::::::::::::"+info);
	    System.out.println("link::::::::::::::::::"+info.getJson().getString("link"));
	    //edit video
	    String name = "my.mp4";
	    String desc = "Description";
	    String license = ""; //see Vimeo API Documentation
	    //String privacyView = "disable"; //see Vimeo API Documentation
	    //String privacyEmbed = "whitelist"; //see Vimeo API Documentation
	    String privacyView = "anybody"; //see Vimeo API Documentation
	    String privacyEmbed = "public"; //see Vimeo API Documentation
	    
	    boolean reviewLink = false;
	    vimeo.updateVideoMetadata(videoEndPoint, name, desc, license, privacyView, privacyEmbed, reviewLink);
	    
	    //add video privacy domain
	    //vimeo.addVideoPrivacyDomain(videoEndPoint, "clickntap.com");
	   
	    //delete video
	   // vimeo.removeVideo(videoEndPoint);
	    
	  }

	}
