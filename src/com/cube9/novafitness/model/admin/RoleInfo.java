package com.cube9.novafitness.model.admin;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;

@Entity(name = "role_info")
public class RoleInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "role_id")
	private Long roleId;

	@Column(name = "role_name")
	private String roleName;

	@Column(name = "role_last_updated_dateTime")
	private LocalDateTime roleLastUpdatedDateTime;
	
	@Column(name = "role_current_dateTime")
	@CreationTimestamp
	private LocalDateTime roleCurrentDateTime;

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public LocalDateTime getRoleLastUpdatedDateTime() {
		return roleLastUpdatedDateTime;
	}

	public void setRoleLastUpdatedDateTime(LocalDateTime roleLastUpdatedDateTime) {
		this.roleLastUpdatedDateTime = roleLastUpdatedDateTime;
	}

	public LocalDateTime getRoleCurrentDateTime() {
		return roleCurrentDateTime;
	}

	public void setRoleCurrentDateTime(LocalDateTime roleCurrentDateTime) {
		this.roleCurrentDateTime = roleCurrentDateTime;
	}

	@Override
	public String toString() {
		return "RoleInfo [roleId=" + roleId + ", roleName=" + roleName + ", roleLastUpdatedDateTime="
				+ roleLastUpdatedDateTime + ", roleCurrentDateTime=" + roleCurrentDateTime + "]";
	}

}
