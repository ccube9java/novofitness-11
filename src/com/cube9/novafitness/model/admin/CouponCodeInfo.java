package com.cube9.novafitness.model.admin;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;

@Entity(name = "coupon_code_info")
public class CouponCodeInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "coupon_code_id")
	private Long couponCodeId;

	@Column(name = "coupon_code_value", unique = true)
	private String couponCodeValue;
	
	@Column(name = "coupon_code_amount")
	private Long couponCodeAmount;

	@Column(name = "coupon_code_description", columnDefinition = "VARCHAR(5000)")
	private String couponCodeDescription;

	@Column(name = "is_used", columnDefinition = "boolean default false")
	private boolean couponCodeUsedStatus=false;
	
	@Column(name = "is_Active", columnDefinition = "boolean default true")
	private boolean couponCodeIsActive=true;
	
	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean couponCodeIsDeletedByAdmin=false;
	
	@Column(name = "coupon_code_last_updated_dateTime")
	private LocalDateTime couponCodeLastUpdatedDateTime;
	
	@Column(name = "coupon_code_current_dateTime")
	@CreationTimestamp
	private LocalDateTime couponCodeCurrentDateTime;

	public Long getCouponCodeId() {
		return couponCodeId;
	}

	public void setCouponCodeId(Long couponCodeId) {
		this.couponCodeId = couponCodeId;
	}

	public String getCouponCodeValue() {
		return couponCodeValue;
	}

	public void setCouponCodeValue(String couponCodeValue) {
		this.couponCodeValue = couponCodeValue;
	}

	public Long getCouponCodeAmount() {
		return couponCodeAmount;
	}

	public void setCouponCodeAmount(Long couponCodeAmount) {
		this.couponCodeAmount = couponCodeAmount;
	}

	public String getCouponCodeDescription() {
		return couponCodeDescription;
	}

	public void setCouponCodeDescription(String couponCodeDescription) {
		this.couponCodeDescription = couponCodeDescription;
	}

	public boolean isCouponCodeUsedStatus() {
		return couponCodeUsedStatus;
	}

	public void setCouponCodeUsedStatus(boolean couponCodeUsedStatus) {
		this.couponCodeUsedStatus = couponCodeUsedStatus;
	}

	public boolean isCouponCodeIsActive() {
		return couponCodeIsActive;
	}

	public void setCouponCodeIsActive(boolean couponCodeIsActive) {
		this.couponCodeIsActive = couponCodeIsActive;
	}

	public boolean isCouponCodeIsDeletedByAdmin() {
		return couponCodeIsDeletedByAdmin;
	}

	public void setCouponCodeIsDeletedByAdmin(boolean couponCodeIsDeletedByAdmin) {
		this.couponCodeIsDeletedByAdmin = couponCodeIsDeletedByAdmin;
	}

	public LocalDateTime getCouponCodeLastUpdatedDateTime() {
		return couponCodeLastUpdatedDateTime;
	}

	public void setCouponCodeLastUpdatedDateTime(LocalDateTime couponCodeLastUpdatedDateTime) {
		this.couponCodeLastUpdatedDateTime = couponCodeLastUpdatedDateTime;
	}

	public LocalDateTime getCouponCodeCurrentDateTime() {
		return couponCodeCurrentDateTime;
	}

	public void setCouponCodeCurrentDateTime(LocalDateTime couponCodeCurrentDateTime) {
		this.couponCodeCurrentDateTime = couponCodeCurrentDateTime;
	}

	@Override
	public String toString() {
		return "CouponCodeInfo [couponCodeId=" + couponCodeId + ", couponCodeValue=" + couponCodeValue
				+ ", couponCodeAmount=" + couponCodeAmount + ", couponCodeDescription=" + couponCodeDescription
				+ ", couponCodeUsedStatus=" + couponCodeUsedStatus + ", couponCodeIsActive=" + couponCodeIsActive
				+ ", couponCodeIsDeletedByAdmin=" + couponCodeIsDeletedByAdmin + ", couponCodeLastUpdatedDateTime="
				+ couponCodeLastUpdatedDateTime + ", couponCodeCurrentDateTime=" + couponCodeCurrentDateTime + "]";
	}

	
}
