package com.cube9.novafitness.model.admin;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;

//@Entity(name = "login_page_banner_info")
public class LoginPageBannerInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "login_page_banner_id")
	private Long loginPageBannerId;
	
	@Column(name = "login_page_banner_name")
	private String loginPageBannerName;
	
	@Column(name = "login_page_banner_stored_path")
	private String loginPageBannerStoredPath;
	
	@Column(name = "login_page_banner_view_path")
	private String loginPageBannerViewPath;
	
	@Column(name = "login_page_banner_type")
	private String loginPageBannerType;
	
	@Column(name = "login_page_banner_status", columnDefinition = "boolean default false")
	private boolean loginPageBannerStatus=false;

	@Column(name = "login_page_banner_current_dateTime")
	@CreationTimestamp
	private LocalDateTime loginPageBannerCurrentDateTime;

	public Long getLoginPageBannerId() {
		return loginPageBannerId;
	}

	public void setLoginPageBannerId(Long loginPageBannerId) {
		this.loginPageBannerId = loginPageBannerId;
	}

	public String getLoginPageBannerName() {
		return loginPageBannerName;
	}

	public void setLoginPageBannerName(String loginPageBannerName) {
		this.loginPageBannerName = loginPageBannerName;
	}

	public String getLoginPageBannerStoredPath() {
		return loginPageBannerStoredPath;
	}

	public void setLoginPageBannerStoredPath(String loginPageBannerStoredPath) {
		this.loginPageBannerStoredPath = loginPageBannerStoredPath;
	}

	public String getLoginPageBannerViewPath() {
		return loginPageBannerViewPath;
	}

	public void setLoginPageBannerViewPath(String loginPageBannerViewPath) {
		this.loginPageBannerViewPath = loginPageBannerViewPath;
	}

	public String getLoginPageBannerType() {
		return loginPageBannerType;
	}

	public void setLoginPageBannerType(String loginPageBannerType) {
		this.loginPageBannerType = loginPageBannerType;
	}

	public boolean isLoginPageBannerStatus() {
		return loginPageBannerStatus;
	}

	public void setLoginPageBannerStatus(boolean loginPageBannerStatus) {
		this.loginPageBannerStatus = loginPageBannerStatus;
	}

	public LocalDateTime getLoginPageBannerCurrentDateTime() {
		return loginPageBannerCurrentDateTime;
	}

	public void setLoginPageBannerCurrentDateTime(LocalDateTime loginPageBannerCurrentDateTime) {
		this.loginPageBannerCurrentDateTime = loginPageBannerCurrentDateTime;
	}

	@Override
	public String toString() {
		return "LoginPageBannerInfo [loginPageBannerId=" + loginPageBannerId + ", loginPageBannerName="
				+ loginPageBannerName + ", loginPageBannerStoredPath=" + loginPageBannerStoredPath
				+ ", loginPageBannerViewPath=" + loginPageBannerViewPath + ", loginPageBannerType="
				+ loginPageBannerType + ", loginPageBannerStatus=" + loginPageBannerStatus
				+ ", loginPageBannerCurrentDateTime=" + loginPageBannerCurrentDateTime + "]";
	}
	
}
