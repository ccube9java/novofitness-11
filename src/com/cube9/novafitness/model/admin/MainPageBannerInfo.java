package com.cube9.novafitness.model.admin;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;

//@Entity(name = "main_page_banner_info")
public class MainPageBannerInfo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "main_page_banner_id")
	private Long mainPageBannerId;
	
	@Column(name = "main_page_banner_name")
	private String mainPageBannerName;
	
	@Column(name = "main_page_banner_stored_path")
	private String mainPageBannerStoredPath;
	
	@Column(name = "main_page_banner_view_path")
	private String mainPageBannerViewPath;
	
	@Column(name = "main_page_banner_type")
	private String mainPageBannerType;
	
	@Column(name = "main_page_banner_status", columnDefinition = "boolean default false")
	private boolean mainPageBannerStatus=false;

	@Column(name = "main_page_banner_current_dateTime")
	@CreationTimestamp
	private LocalDateTime mainPageBannerCurrentDateTime;

	public Long getMainPageBannerId() {
		return mainPageBannerId;
	}

	public void setMainPageBannerId(Long mainPageBannerId) {
		this.mainPageBannerId = mainPageBannerId;
	}

	public String getMainPageBannerName() {
		return mainPageBannerName;
	}

	public void setMainPageBannerName(String mainPageBannerName) {
		this.mainPageBannerName = mainPageBannerName;
	}

	public String getMainPageBannerStoredPath() {
		return mainPageBannerStoredPath;
	}

	public void setMainPageBannerStoredPath(String mainPageBannerStoredPath) {
		this.mainPageBannerStoredPath = mainPageBannerStoredPath;
	}

	public String getMainPageBannerViewPath() {
		return mainPageBannerViewPath;
	}

	public void setMainPageBannerViewPath(String mainPageBannerViewPath) {
		this.mainPageBannerViewPath = mainPageBannerViewPath;
	}

	public String getMainPageBannerType() {
		return mainPageBannerType;
	}

	public void setMainPageBannerType(String mainPageBannerType) {
		this.mainPageBannerType = mainPageBannerType;
	}

	public boolean isMainPageBannerStatus() {
		return mainPageBannerStatus;
	}

	public void setMainPageBannerStatus(boolean mainPageBannerStatus) {
		this.mainPageBannerStatus = mainPageBannerStatus;
	}

	public LocalDateTime getMainPageBannerCurrentDateTime() {
		return mainPageBannerCurrentDateTime;
	}

	public void setMainPageBannerCurrentDateTime(LocalDateTime mainPageBannerCurrentDateTime) {
		this.mainPageBannerCurrentDateTime = mainPageBannerCurrentDateTime;
	}

	@Override
	public String toString() {
		return "MainPageBannerInfo [mainPageBannerId=" + mainPageBannerId + ", mainPageBannerName=" + mainPageBannerName
				+ ", mainPageBannerStoredPath=" + mainPageBannerStoredPath + ", mainPageBannerViewPath="
				+ mainPageBannerViewPath + ", mainPageBannerType=" + mainPageBannerType + ", mainPageBannerStatus="
				+ mainPageBannerStatus + ", mainPageBannerCurrentDateTime=" + mainPageBannerCurrentDateTime + "]";
	}
	
}
