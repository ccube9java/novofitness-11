package com.cube9.novafitness.model.admin;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.CreationTimestamp;

@Entity(name = "faq_info")
public class FAQInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "faq_id")
	private Long faqId;

	@Column(name = "faq_question")
	private String faqQuestion;

	@Column(name = "faq_answer" , columnDefinition = "VARCHAR(5000)")
	private String faqAnswer;
	
	@Column(name = "is_Active", columnDefinition = "boolean default false")
	private boolean faqIsActive=false;
	
	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean faqIsDeletedByAdmin=false;
	
	@Column(name = "faq_last_updated_dateTime")
	private LocalDateTime faqLastUpdatedDateTime;
	
	@Column(name = "faq_created_dateTime")
	@CreationTimestamp
	private LocalDateTime faqCreatedDateTime;

	public Long getFaqId() {
		return faqId;
	}

	public void setFaqId(Long faqId) {
		this.faqId = faqId;
	}

	public String getFaqQuestion() {
		return faqQuestion;
	}

	public void setFaqQuestion(String faqQuestion) {
		this.faqQuestion = faqQuestion;
	}

	public String getFaqAnswer() {
		return faqAnswer;
	}

	public void setFaqAnswer(String faqAnswer) {
		this.faqAnswer = faqAnswer;
	}

	public boolean isFaqIsActive() {
		return faqIsActive;
	}

	public void setFaqIsActive(boolean faqIsActive) {
		this.faqIsActive = faqIsActive;
	}

	public boolean isFaqIsDeletedByAdmin() {
		return faqIsDeletedByAdmin;
	}

	public void setFaqIsDeletedByAdmin(boolean faqIsDeletedByAdmin) {
		this.faqIsDeletedByAdmin = faqIsDeletedByAdmin;
	}

	public LocalDateTime getFaqLastUpdatedDateTime() {
		return faqLastUpdatedDateTime;
	}

	public void setFaqLastUpdatedDateTime(LocalDateTime faqLastUpdatedDateTime) {
		this.faqLastUpdatedDateTime = faqLastUpdatedDateTime;
	}

	public LocalDateTime getFaqCreatedDateTime() {
		return faqCreatedDateTime;
	}

	public void setFaqCreatedDateTime(LocalDateTime faqCreatedDateTime) {
		this.faqCreatedDateTime = faqCreatedDateTime;
	}

	@Override
	public String toString() {
		return "FAQInfo [faqId=" + faqId + ", faqQuestion=" + faqQuestion + ", faqAnswer=" + faqAnswer
				+ ", faqIsActive=" + faqIsActive + ", faqIsDeletedByAdmin=" + faqIsDeletedByAdmin
				+ ", faqLastUpdatedDateTime=" + faqLastUpdatedDateTime + ", faqCreatedDateTime=" + faqCreatedDateTime
				+ "]";
	}
}
