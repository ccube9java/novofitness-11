package com.cube9.novafitness.model.admin;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;

@Entity(name="admin_profile_image_info")
public class AdminProfileImageInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "admin_profile_image_id")
	private Long adminProfileImageId;
	
	@Column(name = "admin_profile_image_name")
	private String adminProfileImageName;
	
	@Column(name = "admin_profile_image_stored_path")
	private String adminProfileImageStoredPath;
	
	@Column(name = "admin_profile_image_view_path")
	private String adminProfileImageViewPath;
	
	@Column(name = "admin_profile_image_file_type")
	private String adminProfileImageFileType;
	
	@Column(name = "is_Active", columnDefinition = "boolean default true")
	private boolean adminProfileImageStatus=true;
	
	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean subAdminProfileImageAdminDeleteStatus=false;
	
	@Column(name = "admin_profile_image_last_updated_dateTime")
	private LocalDateTime adminProfileImageLastUpdatedDateTime;
	
	@Column(name = "admin_profile_image_created_dateTime")
	@CreationTimestamp
	private LocalDateTime adminProfileImageCreatedDateTime;

	public Long getAdminProfileImageId() {
		return adminProfileImageId;
	}

	public void setAdminProfileImageId(Long adminProfileImageId) {
		this.adminProfileImageId = adminProfileImageId;
	}

	public String getAdminProfileImageName() {
		return adminProfileImageName;
	}

	public void setAdminProfileImageName(String adminProfileImageName) {
		this.adminProfileImageName = adminProfileImageName;
	}

	public String getAdminProfileImageStoredPath() {
		return adminProfileImageStoredPath;
	}

	public void setAdminProfileImageStoredPath(String adminProfileImageStoredPath) {
		this.adminProfileImageStoredPath = adminProfileImageStoredPath;
	}

	public String getAdminProfileImageViewPath() {
		return adminProfileImageViewPath;
	}

	public void setAdminProfileImageViewPath(String adminProfileImageViewPath) {
		this.adminProfileImageViewPath = adminProfileImageViewPath;
	}

	public String getAdminProfileImageFileType() {
		return adminProfileImageFileType;
	}

	public void setAdminProfileImageFileType(String adminProfileImageFileType) {
		this.adminProfileImageFileType = adminProfileImageFileType;
	}

	public boolean isAdminProfileImageStatus() {
		return adminProfileImageStatus;
	}

	public void setAdminProfileImageStatus(boolean adminProfileImageStatus) {
		this.adminProfileImageStatus = adminProfileImageStatus;
	}

	public boolean isSubAdminProfileImageAdminDeleteStatus() {
		return subAdminProfileImageAdminDeleteStatus;
	}

	public void setSubAdminProfileImageAdminDeleteStatus(boolean subAdminProfileImageAdminDeleteStatus) {
		this.subAdminProfileImageAdminDeleteStatus = subAdminProfileImageAdminDeleteStatus;
	}

	public LocalDateTime getAdminProfileImageLastUpdatedDateTime() {
		return adminProfileImageLastUpdatedDateTime;
	}

	public void setAdminProfileImageLastUpdatedDateTime(LocalDateTime adminProfileImageLastUpdatedDateTime) {
		this.adminProfileImageLastUpdatedDateTime = adminProfileImageLastUpdatedDateTime;
	}

	public LocalDateTime getAdminProfileImageCreatedDateTime() {
		return adminProfileImageCreatedDateTime;
	}

	public void setAdminProfileImageCreatedDateTime(LocalDateTime adminProfileImageCreatedDateTime) {
		this.adminProfileImageCreatedDateTime = adminProfileImageCreatedDateTime;
	}

	@Override
	public String toString() {
		return "AdminProfileImageInfo [adminProfileImageId=" + adminProfileImageId + ", adminProfileImageName="
				+ adminProfileImageName + ", adminProfileImageStoredPath=" + adminProfileImageStoredPath
				+ ", adminProfileImageViewPath=" + adminProfileImageViewPath + ", adminProfileImageFileType="
				+ adminProfileImageFileType + ", adminProfileImageStatus=" + adminProfileImageStatus
				+ ", subAdminProfileImageAdminDeleteStatus=" + subAdminProfileImageAdminDeleteStatus
				+ ", adminProfileImageLastUpdatedDateTime=" + adminProfileImageLastUpdatedDateTime
				+ ", adminProfileImageCreatedDateTime=" + adminProfileImageCreatedDateTime + "]";
	}
	
}
