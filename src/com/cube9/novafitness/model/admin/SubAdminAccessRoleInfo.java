package com.cube9.novafitness.model.admin;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

@Entity(name = "subAdmin_access_role_info")
public class SubAdminAccessRoleInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "subAdmin_access_role_id")
	private Long subAdminAccessRoleId;
	
	@OneToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "sub_admin_id")
	private AdminInfo subAdminInfo;
	
	@Column(name = "accessing_user_management", columnDefinition = "boolean default false")
	private boolean subAdminAccessForUserManagement=false;
	
	@Column(name = "accessing_trainer_management", columnDefinition = "boolean default false")
	private boolean subAdminAccessForTrainerManagement=false;
	
	@Column(name = "accessing_course_management", columnDefinition = "boolean default false")
	private boolean subAdminAccessForCourseManagement=false;
	
	@Column(name = "accessing_category_management", columnDefinition = "boolean default false")
	private boolean subAdminAccessForCategoryManagement=false;
	
	@Column(name = "accessing_content_management", columnDefinition = "boolean default false")
	private boolean subAdminAccessForContentManagement=false;
	
	@Column(name = "accessing_faqs_management", columnDefinition = "boolean default false")
	private boolean subAdminAccessForFAQsManagement=false;
	
	@Column(name = "accessing_general_management", columnDefinition = "boolean default false")
	private boolean subAdminAccessForGeneralManagement=false;
	
	@Column(name = "accessing_reviews_ratings_management", columnDefinition = "boolean default false")
	private boolean subAdminAccessForReviewsRatingsManagement=false;
	
	@Column(name = "accessing_newsletter_management", columnDefinition = "boolean default false")
	private boolean subAdminAccessForNewsletterManagement=false;
	
	@Column(name = "accessing_payment_management", columnDefinition = "boolean default false")
	private boolean subAdminAccessForPaymentManagement=false;
	
	@Column(name = "accessing_reports_management", columnDefinition = "boolean default false")
	private boolean subAdminAccessForReportsManagement=false;
	
	@Column(name = "accessing_subAdmin_management", columnDefinition = "boolean default false")
	private boolean subAdminAccessForSubAdminManagement=false;
	
	@Column(name = "is_Approved_by_admin", columnDefinition = "boolean default false")
	private boolean subAdminIsApprovedByAdmin=false;
	
	@Column(name = "subAdmin_access_role_current_dateTime")
	@CreationTimestamp
	private LocalDateTime subAdminAccessRoleCurrentDateTime;	
	
	@Column(name = "is_Active", columnDefinition = "boolean default false")
	private boolean subAdminStatus=false;
	
	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean subAdminDeleteStatus=false;
	

	public Long getSubAdminAccessRoleId() {
		return subAdminAccessRoleId;
	}

	public AdminInfo getSubAdminInfo() {
		return subAdminInfo;
	}

	public boolean isSubAdminAccessForUserManagement() {
		return subAdminAccessForUserManagement;
	}

	public boolean isSubAdminAccessForTrainerManagement() {
		return subAdminAccessForTrainerManagement;
	}

	public boolean isSubAdminAccessForCourseManagement() {
		return subAdminAccessForCourseManagement;
	}

	public boolean isSubAdminAccessForCategoryManagement() {
		return subAdminAccessForCategoryManagement;
	}

	public boolean isSubAdminAccessForContentManagement() {
		return subAdminAccessForContentManagement;
	}

	public boolean isSubAdminAccessForFAQsManagement() {
		return subAdminAccessForFAQsManagement;
	}

	public boolean isSubAdminAccessForGeneralManagement() {
		return subAdminAccessForGeneralManagement;
	}

	public boolean isSubAdminAccessForReviewsRatingsManagement() {
		return subAdminAccessForReviewsRatingsManagement;
	}

	public boolean isSubAdminAccessForNewsletterManagement() {
		return subAdminAccessForNewsletterManagement;
	}

	public boolean isSubAdminAccessForPaymentManagement() {
		return subAdminAccessForPaymentManagement;
	}

	public boolean isSubAdminAccessForReportsManagement() {
		return subAdminAccessForReportsManagement;
	}

	public boolean isSubAdminAccessForSubAdminManagement() {
		return subAdminAccessForSubAdminManagement;
	}

	public LocalDateTime getSubAdminAccessRoleCurrentDateTime() {
		return subAdminAccessRoleCurrentDateTime;
	}

	public boolean isSubAdminDeleteStatus() {
		return subAdminDeleteStatus;
	}

	public boolean isSubAdminStatus() {
		return subAdminStatus;
	}

	public boolean isSubAdminIsApprovedByAdmin() {
		return subAdminIsApprovedByAdmin;
	}

	public void setSubAdminAccessRoleId(Long subAdminAccessRoleId) {
		this.subAdminAccessRoleId = subAdminAccessRoleId;
	}

	public void setSubAdminInfo(AdminInfo subAdminInfo) {
		this.subAdminInfo = subAdminInfo;
	}

	public void setSubAdminAccessForUserManagement(boolean subAdminAccessForUserManagement) {
		this.subAdminAccessForUserManagement = subAdminAccessForUserManagement;
	}

	public void setSubAdminAccessForTrainerManagement(boolean subAdminAccessForTrainerManagement) {
		this.subAdminAccessForTrainerManagement = subAdminAccessForTrainerManagement;
	}

	public void setSubAdminAccessForCourseManagement(boolean subAdminAccessForCourseManagement) {
		this.subAdminAccessForCourseManagement = subAdminAccessForCourseManagement;
	}

	public void setSubAdminAccessForCategoryManagement(boolean subAdminAccessForCategoryManagement) {
		this.subAdminAccessForCategoryManagement = subAdminAccessForCategoryManagement;
	}

	public void setSubAdminAccessForContentManagement(boolean subAdminAccessForContentManagement) {
		this.subAdminAccessForContentManagement = subAdminAccessForContentManagement;
	}

	public void setSubAdminAccessForFAQsManagement(boolean subAdminAccessForFAQsManagement) {
		this.subAdminAccessForFAQsManagement = subAdminAccessForFAQsManagement;
	}

	public void setSubAdminAccessForGeneralManagement(boolean subAdminAccessForGeneralManagement) {
		this.subAdminAccessForGeneralManagement = subAdminAccessForGeneralManagement;
	}

	public void setSubAdminAccessForReviewsRatingsManagement(boolean subAdminAccessForReviewsRatingsManagement) {
		this.subAdminAccessForReviewsRatingsManagement = subAdminAccessForReviewsRatingsManagement;
	}

	public void setSubAdminAccessForNewsletterManagement(boolean subAdminAccessForNewsletterManagement) {
		this.subAdminAccessForNewsletterManagement = subAdminAccessForNewsletterManagement;
	}

	public void setSubAdminAccessForPaymentManagement(boolean subAdminAccessForPaymentManagement) {
		this.subAdminAccessForPaymentManagement = subAdminAccessForPaymentManagement;
	}

	public void setSubAdminAccessForReportsManagement(boolean subAdminAccessForReportsManagement) {
		this.subAdminAccessForReportsManagement = subAdminAccessForReportsManagement;
	}

	public void setSubAdminAccessForSubAdminManagement(boolean subAdminAccessForSubAdminManagement) {
		this.subAdminAccessForSubAdminManagement = subAdminAccessForSubAdminManagement;
	}

	public void setSubAdminAccessRoleCurrentDateTime(LocalDateTime subAdminAccessRoleCurrentDateTime) {
		this.subAdminAccessRoleCurrentDateTime = subAdminAccessRoleCurrentDateTime;
	}

	public void setSubAdminDeleteStatus(boolean subAdminDeleteStatus) {
		this.subAdminDeleteStatus = subAdminDeleteStatus;
	}

	public void setSubAdminStatus(boolean subAdminStatus) {
		this.subAdminStatus = subAdminStatus;
	}

	public void setSubAdminIsApprovedByAdmin(boolean subAdminIsApprovedByAdmin) {
		this.subAdminIsApprovedByAdmin = subAdminIsApprovedByAdmin;
	}

	@Override
	public String toString() {
		return "SubAdminAccessRoleInfo [subAdminAccessRoleId=" + subAdminAccessRoleId + ", subAdminInfo=" + subAdminInfo
				+ ", subAdminAccessForUserManagement=" + subAdminAccessForUserManagement
				+ ", subAdminAccessForTrainerManagement=" + subAdminAccessForTrainerManagement
				+ ", subAdminAccessForCourseManagement=" + subAdminAccessForCourseManagement
				+ ", subAdminAccessForCategoryManagement=" + subAdminAccessForCategoryManagement
				+ ", subAdminAccessForContentManagement=" + subAdminAccessForContentManagement
				+ ", subAdminAccessForFAQsManagement=" + subAdminAccessForFAQsManagement
				+ ", subAdminAccessForGeneralManagement=" + subAdminAccessForGeneralManagement
				+ ", subAdminAccessForReviewsRatingsManagement=" + subAdminAccessForReviewsRatingsManagement
				+ ", subAdminAccessForNewsletterManagement=" + subAdminAccessForNewsletterManagement
				+ ", subAdminAccessForPaymentManagement=" + subAdminAccessForPaymentManagement
				+ ", subAdminAccessForReportsManagement=" + subAdminAccessForReportsManagement
				+ ", subAdminAccessForSubAdminManagement=" + subAdminAccessForSubAdminManagement
				+ ", subAdminAccessRoleCurrentDateTime=" + subAdminAccessRoleCurrentDateTime + ", subAdminDeleteStatus="
				+ subAdminDeleteStatus + ", subAdminStatus=" + subAdminStatus + ", subAdminIsApprovedByAdmin="
				+ subAdminIsApprovedByAdmin + "]";
	}
	
	
	

}
