package com.cube9.novafitness.model.admin;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.CreationTimestamp;

@Entity(name = "category_info")
public class CategoryInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "category_id")
	private Long categoryId;

	@Column(name = "category_description", columnDefinition = "VARCHAR(5000)" )
	private String categoryDescription;
	
	@Column(name = "is_Active", columnDefinition = "boolean default false")
	private boolean categoryIsActive=false;
	
	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean categoryIsDeletedByAdmin=false;
	
	@Column(name = "category_last_updated_dateTime")
	private LocalDateTime categoryLastUpdatedDateTime;
	
	@Column(name = "category_created_dateTime")
	@CreationTimestamp
	private LocalDateTime categoryCreatedDateTime;
	
	@Column(name = "category_name")
	private String categoryName;


	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryDescription() {
		return categoryDescription;
	}

	public void setCategoryDescription(String categoryDescription) {
		this.categoryDescription = categoryDescription;
	}

	public boolean isCategoryIsActive() {
		return categoryIsActive;
	}

	public void setCategoryIsActive(boolean categoryIsActive) {
		this.categoryIsActive = categoryIsActive;
	}

	public boolean isCategoryIsDeletedByAdmin() {
		return categoryIsDeletedByAdmin;
	}

	public void setCategoryIsDeletedByAdmin(boolean categoryIsDeletedByAdmin) {
		this.categoryIsDeletedByAdmin = categoryIsDeletedByAdmin;
	}

	public LocalDateTime getCategoryLastUpdatedDateTime() {
		return categoryLastUpdatedDateTime;
	}

	public void setCategoryLastUpdatedDateTime(LocalDateTime categoryLastUpdatedDateTime) {
		this.categoryLastUpdatedDateTime = categoryLastUpdatedDateTime;
	}

	public LocalDateTime getCategoryCreatedDateTime() {
		return categoryCreatedDateTime;
	}

	public void setCategoryCreatedDateTime(LocalDateTime categoryCreatedDateTime) {
		this.categoryCreatedDateTime = categoryCreatedDateTime;
	}

	@Override
	public String toString() {
		return "CategoryInfo [categoryId=" + categoryId + ", categoryName=" + categoryName + ", categoryDescription="
				+ categoryDescription + ", categoryIsActive=" + categoryIsActive + ", categoryIsDeletedByAdmin="
				+ categoryIsDeletedByAdmin + ", categoryLastUpdatedDateTime=" + categoryLastUpdatedDateTime
				+ ", categoryCreatedDateTime=" + categoryCreatedDateTime + "]";
	}
	
}
