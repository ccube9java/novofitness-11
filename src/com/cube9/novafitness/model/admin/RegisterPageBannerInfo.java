package com.cube9.novafitness.model.admin;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;

//@Entity(name = "register_page_banner_info")
public class RegisterPageBannerInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "register_page_banner_id")
	private Long registerPageBannerId;
	
	@Column(name = "register_page_banner_name")
	private String registerPageBannerName;
	
	@Column(name = "register_page_banner_stored_path")
	private String registerPageBannerStoredPath;
	
	@Column(name = "register_page_banner_view_path")
	private String registerPageBannerViewPath;
	
	@Column(name = "register_page_banner_type")
	private String registerPageBannerType;
	
	@Column(name = "register_page_banner_status", columnDefinition = "boolean default false")
	private boolean registerPageBannerStatus=false;

	@Column(name = "register_page_banner_current_dateTime")
	@CreationTimestamp
	private LocalDateTime registerPageBannerCurrentDateTime;

	public Long getRegisterPageBannerId() {
		return registerPageBannerId;
	}

	public void setRegisterPageBannerId(Long registerPageBannerId) {
		this.registerPageBannerId = registerPageBannerId;
	}

	public String getRegisterPageBannerName() {
		return registerPageBannerName;
	}

	public void setRegisterPageBannerName(String registerPageBannerName) {
		this.registerPageBannerName = registerPageBannerName;
	}

	public String getRegisterPageBannerStoredPath() {
		return registerPageBannerStoredPath;
	}

	public void setRegisterPageBannerStoredPath(String registerPageBannerStoredPath) {
		this.registerPageBannerStoredPath = registerPageBannerStoredPath;
	}

	public String getRegisterPageBannerViewPath() {
		return registerPageBannerViewPath;
	}

	public void setRegisterPageBannerViewPath(String registerPageBannerViewPath) {
		this.registerPageBannerViewPath = registerPageBannerViewPath;
	}

	public String getRegisterPageBannerType() {
		return registerPageBannerType;
	}

	public void setRegisterPageBannerType(String registerPageBannerType) {
		this.registerPageBannerType = registerPageBannerType;
	}

	public boolean isRegisterPageBannerStatus() {
		return registerPageBannerStatus;
	}

	public void setRegisterPageBannerStatus(boolean registerPageBannerStatus) {
		this.registerPageBannerStatus = registerPageBannerStatus;
	}

	public LocalDateTime getRegisterPageBannerCurrentDateTime() {
		return registerPageBannerCurrentDateTime;
	}

	public void setRegisterPageBannerCurrentDateTime(LocalDateTime registerPageBannerCurrentDateTime) {
		this.registerPageBannerCurrentDateTime = registerPageBannerCurrentDateTime;
	}

	@Override
	public String toString() {
		return "RegisterPageBannerInfo [registerPageBannerId=" + registerPageBannerId + ", registerPageBannerName="
				+ registerPageBannerName + ", registerPageBannerStoredPath=" + registerPageBannerStoredPath
				+ ", registerPageBannerViewPath=" + registerPageBannerViewPath + ", registerPageBannerType="
				+ registerPageBannerType + ", registerPageBannerStatus=" + registerPageBannerStatus
				+ ", registerPageBannerCurrentDateTime=" + registerPageBannerCurrentDateTime + "]";
	}
	
}
