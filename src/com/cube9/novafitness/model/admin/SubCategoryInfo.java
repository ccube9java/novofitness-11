package com.cube9.novafitness.model.admin;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

@Entity(name = "subcategory_info")
public class SubCategoryInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "subcategory_id")
	private Long subcategoryId;

	@Column(name = "subcategory_name")
	private String subcategoryName;

	@Column(name = "subcategory_description")
	private String subcategoryDescription;
	
	@OneToOne
	@JoinColumn(name="main_category_id")
	private CategoryInfo categoryInfo;
	
	@Column(name = "is_Active", columnDefinition = "boolean default false")
	private boolean subcategoryIsActive=false;
	
	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean subcategoryIsDeletedByAdmin=false;
	
	@Column(name = "subcategory_last_updated_dateTime")
	private LocalDateTime subcategoryLastUpdatedDateTime;
	
	@Column(name = "subcategory_created_dateTime")
	@CreationTimestamp
	private LocalDateTime subcategoryCreatedDateTime;

	public Long getSubcategoryId() {
		return subcategoryId;
	}

	public void setSubcategoryId(Long subcategoryId) {
		this.subcategoryId = subcategoryId;
	}

	public String getSubcategoryName() {
		return subcategoryName;
	}

	public void setSubcategoryName(String subcategoryName) {
		this.subcategoryName = subcategoryName;
	}

	public String getSubcategoryDescription() {
		return subcategoryDescription;
	}

	public void setSubcategoryDescription(String subcategoryDescription) {
		this.subcategoryDescription = subcategoryDescription;
	}

	public CategoryInfo getCategoryInfo() {
		return categoryInfo;
	}

	public void setCategoryInfo(CategoryInfo categoryInfo) {
		this.categoryInfo = categoryInfo;
	}

	public boolean isSubcategoryIsActive() {
		return subcategoryIsActive;
	}

	public void setSubcategoryIsActive(boolean subcategoryIsActive) {
		this.subcategoryIsActive = subcategoryIsActive;
	}

	public boolean isSubcategoryIsDeletedByAdmin() {
		return subcategoryIsDeletedByAdmin;
	}

	public void setSubcategoryIsDeletedByAdmin(boolean subcategoryIsDeletedByAdmin) {
		this.subcategoryIsDeletedByAdmin = subcategoryIsDeletedByAdmin;
	}

	public LocalDateTime getSubcategoryLastUpdatedDateTime() {
		return subcategoryLastUpdatedDateTime;
	}

	public void setSubcategoryLastUpdatedDateTime(LocalDateTime subcategoryLastUpdatedDateTime) {
		this.subcategoryLastUpdatedDateTime = subcategoryLastUpdatedDateTime;
	}

	public LocalDateTime getSubcategoryCreatedDateTime() {
		return subcategoryCreatedDateTime;
	}

	public void setSubcategoryCreatedDateTime(LocalDateTime subcategoryCreatedDateTime) {
		this.subcategoryCreatedDateTime = subcategoryCreatedDateTime;
	}

	@Override
	public String toString() {
		return "SubCategoryInfo [subcategoryId=" + subcategoryId + ", subcategoryName=" + subcategoryName
				+ ", subcategoryDescription=" + subcategoryDescription + ", categoryInfo=" + categoryInfo
				+ ", subcategoryIsActive=" + subcategoryIsActive + ", subcategoryIsDeletedByAdmin="
				+ subcategoryIsDeletedByAdmin + ", subcategoryLastUpdatedDateTime=" + subcategoryLastUpdatedDateTime
				+ ", subcategoryCreatedDateTime=" + subcategoryCreatedDateTime + "]";
	}

}
