package com.cube9.novafitness.model.admin;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

@Entity(name = "admin_info")
public class AdminInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "admin_id")
	private Long adminId;

	@Column(name = "admin_title")
	private String adminTitle;

	@Column(name = "admin_fname")
	private String adminFName;

	@Column(name = "admin_lname")
	private String adminLName;

	@Column(name = "admin_email", unique = true)
	private String adminEmail;

	@Column(name = "admin_password")
	private String adminPassword;
	
	
	/*
	 * @Column(name = "admin_phone_number") private String adminPhoneNumber;
	 */
	
	@Column(name = "admin_phone_no")
	private Long adminPhoneNo;

	@OneToOne
	@JoinColumn(name = "admin_role_id")
	private RoleInfo adminRole;

	@OneToOne
	@JoinColumn(name = "admin_profile_image_id")
	private AdminProfileImageInfo adminProfileImageInfo;
	
	@Column(name = "is_Active", columnDefinition = "boolean default false")
	private boolean adminStatus=false;
	
	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean subAdminDeleteStatus=false;

	@Column(name = "admin_last_login_dateTime")
	private LocalDateTime adminLastLoginDateTime;
	
	@Column(name = "admin_current_dateTime")
	@CreationTimestamp
	private LocalDateTime adminCurrentDateTime;

	public Long getAdminId() {
		return adminId;
	}

	public String getAdminTitle() {
		return adminTitle;
	}

	public String getAdminFName() {
		return adminFName;
	}

	public String getAdminLName() {
		return adminLName;
	}

	public String getAdminEmail() {
		return adminEmail;
	}

	public String getAdminPassword() {
		return adminPassword;
	}

	public Long getAdminPhoneNo() {
		return adminPhoneNo;
	}

	public RoleInfo getAdminRole() {
		return adminRole;
	}

	public AdminProfileImageInfo getAdminProfileImageInfo() {
		return adminProfileImageInfo;
	}

	public boolean isAdminStatus() {
		return adminStatus;
	}

	public boolean isSubAdminDeleteStatus() {
		return subAdminDeleteStatus;
	}

	public LocalDateTime getAdminLastLoginDateTime() {
		return adminLastLoginDateTime;
	}

	public LocalDateTime getAdminCurrentDateTime() {
		return adminCurrentDateTime;
	}

	public void setAdminId(Long adminId) {
		this.adminId = adminId;
	}

	public void setAdminTitle(String adminTitle) {
		this.adminTitle = adminTitle;
	}

	public void setAdminFName(String adminFName) {
		this.adminFName = adminFName;
	}

	public void setAdminLName(String adminLName) {
		this.adminLName = adminLName;
	}

	public void setAdminEmail(String adminEmail) {
		this.adminEmail = adminEmail;
	}

	public void setAdminPassword(String adminPassword) {
		this.adminPassword = adminPassword;
	}

	public void setAdminPhoneNo(Long adminPhoneNo) {
		this.adminPhoneNo = adminPhoneNo;
	}

	public void setAdminRole(RoleInfo adminRole) {
		this.adminRole = adminRole;
	}

	public void setAdminProfileImageInfo(AdminProfileImageInfo adminProfileImageInfo) {
		this.adminProfileImageInfo = adminProfileImageInfo;
	}

	public void setAdminStatus(boolean adminStatus) {
		this.adminStatus = adminStatus;
	}

	public void setSubAdminDeleteStatus(boolean subAdminDeleteStatus) {
		this.subAdminDeleteStatus = subAdminDeleteStatus;
	}

	public void setAdminLastLoginDateTime(LocalDateTime adminLastLoginDateTime) {
		this.adminLastLoginDateTime = adminLastLoginDateTime;
	}

	public void setAdminCurrentDateTime(LocalDateTime adminCurrentDateTime) {
		this.adminCurrentDateTime = adminCurrentDateTime;
	}

	@Override
	public String toString() {
		return "AdminInfo [adminId=" + adminId + ", adminTitle=" + adminTitle + ", adminFName=" + adminFName
				+ ", adminLName=" + adminLName + ", adminEmail=" + adminEmail + ", adminPassword=" + adminPassword
				+ ", adminPhoneNo=" + adminPhoneNo + ", adminRole=" + adminRole + ", adminProfileImageInfo="
				+ adminProfileImageInfo + ", adminStatus=" + adminStatus + ", subAdminDeleteStatus="
				+ subAdminDeleteStatus + ", adminLastLoginDateTime=" + adminLastLoginDateTime
				+ ", adminCurrentDateTime=" + adminCurrentDateTime + "]";
	}

	
	
	
}