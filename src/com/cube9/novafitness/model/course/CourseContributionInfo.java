package com.cube9.novafitness.model.course;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;

@Entity(name = "course_contribution_info")
public class CourseContributionInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "course_contribution_id")
	private Long courseContributionId;

	@Column(name = "course_contribution_percentage")
	private Double courseContributionPercentage;

	@Column(name = "is_Active", columnDefinition = "boolean default true")
	private boolean courseContributionIsActive = true;

	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean courseContributionIsDeletedByAdmin = false;

	@Column(name = "course_contribution_percentage_last_updated_dateTime")
	private LocalDateTime courseContributionLastUpdatedDateTime;

	@Column(name = "course_contribution_percentage_created_dateTime")
	@CreationTimestamp
	private LocalDateTime courseContributionCreatedDateTime;

	public Long getCourseContributionId() {
		return courseContributionId;
	}

	public void setCourseContributionId(Long courseContributionId) {
		this.courseContributionId = courseContributionId;
	}

	public Double getCourseContributionPercentage() {
		return courseContributionPercentage;
	}

	public void setCourseContributionPercentage(Double courseContributionPercentage) {
		this.courseContributionPercentage = courseContributionPercentage;
	}

	public boolean isCourseContributionIsActive() {
		return courseContributionIsActive;
	}

	public void setCourseContributionIsActive(boolean courseContributionIsActive) {
		this.courseContributionIsActive = courseContributionIsActive;
	}

	public boolean isCourseContributionIsDeletedByAdmin() {
		return courseContributionIsDeletedByAdmin;
	}

	public void setCourseContributionIsDeletedByAdmin(
			boolean courseContributionIsDeletedByAdmin) {
		this.courseContributionIsDeletedByAdmin = courseContributionIsDeletedByAdmin;
	}

	public LocalDateTime getCourseContributionLastUpdatedDateTime() {
		return courseContributionLastUpdatedDateTime;
	}

	public void setCourseContributionLastUpdatedDateTime(
			LocalDateTime courseContributionLastUpdatedDateTime) {
		this.courseContributionLastUpdatedDateTime = courseContributionLastUpdatedDateTime;
	}

	public LocalDateTime getCourseContributionCreatedDateTime() {
		return courseContributionCreatedDateTime;
	}

	public void setCourseContributionCreatedDateTime(
			LocalDateTime courseContributionCreatedDateTime) {
		this.courseContributionCreatedDateTime = courseContributionCreatedDateTime;
	}

	@Override
	public String toString() {
		return "CourseContributionInfo [courseContributionId="
				+ courseContributionId + ", courseContributionPercentage="
				+ courseContributionPercentage + ", courseContributionIsActive="
				+ courseContributionIsActive
				+ ", courseContributionIsDeletedByAdmin="
				+ courseContributionIsDeletedByAdmin
				+ ", courseContributionLastUpdatedDateTime="
				+ courseContributionLastUpdatedDateTime
				+ ", courseContributionCreatedDateTime="
				+ courseContributionCreatedDateTime + "]";
	}
	
}
