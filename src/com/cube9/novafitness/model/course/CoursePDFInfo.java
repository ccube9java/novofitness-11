package com.cube9.novafitness.model.course;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;

@Entity(name="course_pdf_info")
public class CoursePDFInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "course_pdf_id")
	private Long coursePDFId;
	
	@Column(name = "course_pdf_name")
	private String coursePDFName;
	
	@Column(name = "course_pdf_stored_path")
	private String coursePDFStoredPath;
	
	@Column(name = "course_pdf_view_path")
	private String coursePDFViewPath;
	
	@Column(name = "course_pdf_file_type")
	private String coursePDFFileType;
	
	@Column(name = "is_Active", columnDefinition = "boolean default true")
	private boolean coursePDFIsActive=true;
	
	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean coursePDFIsDeletedByAdmin=false;
	
	@Column(name = "course_pdf_last_updated_dateTime")
	private LocalDateTime coursePDFLastUpdatedDateTime;
	
	@Column(name = "course_pdf_created_dateTime")
	@CreationTimestamp
	private LocalDateTime coursePDFCreatedDateTime;

	public Long getCoursePDFId() {
		return coursePDFId;
	}

	public void setCoursePDFId(Long coursePDFId) {
		this.coursePDFId = coursePDFId;
	}

	public String getCoursePDFName() {
		return coursePDFName;
	}

	public void setCoursePDFName(String coursePDFName) {
		this.coursePDFName = coursePDFName;
	}

	public String getCoursePDFStoredPath() {
		return coursePDFStoredPath;
	}

	public void setCoursePDFStoredPath(String coursePDFStoredPath) {
		this.coursePDFStoredPath = coursePDFStoredPath;
	}

	public String getCoursePDFViewPath() {
		return coursePDFViewPath;
	}

	public void setCoursePDFViewPath(String coursePDFViewPath) {
		this.coursePDFViewPath = coursePDFViewPath;
	}

	public String getCoursePDFFileType() {
		return coursePDFFileType;
	}

	public void setCoursePDFFileType(String coursePDFFileType) {
		this.coursePDFFileType = coursePDFFileType;
	}

	public boolean isCoursePDFIsActive() {
		return coursePDFIsActive;
	}

	public void setCoursePDFIsActive(boolean coursePDFIsActive) {
		this.coursePDFIsActive = coursePDFIsActive;
	}

	public boolean isCoursePDFIsDeletedByAdmin() {
		return coursePDFIsDeletedByAdmin;
	}

	public void setCoursePDFIsDeletedByAdmin(boolean coursePDFIsDeletedByAdmin) {
		this.coursePDFIsDeletedByAdmin = coursePDFIsDeletedByAdmin;
	}

	public LocalDateTime getCoursePDFLastUpdatedDateTime() {
		return coursePDFLastUpdatedDateTime;
	}

	public void setCoursePDFLastUpdatedDateTime(LocalDateTime coursePDFLastUpdatedDateTime) {
		this.coursePDFLastUpdatedDateTime = coursePDFLastUpdatedDateTime;
	}

	public LocalDateTime getCoursePDFCreatedDateTime() {
		return coursePDFCreatedDateTime;
	}

	public void setCoursePDFCreatedDateTime(LocalDateTime coursePDFCreatedDateTime) {
		this.coursePDFCreatedDateTime = coursePDFCreatedDateTime;
	}

	@Override
	public String toString() {
		return "CoursePDFInfo [coursePDFId=" + coursePDFId + ", coursePDFName=" + coursePDFName
				+ ", coursePDFStoredPath=" + coursePDFStoredPath + ", coursePDFViewPath=" + coursePDFViewPath
				+ ", coursePDFFileType=" + coursePDFFileType + ", coursePDFIsActive=" + coursePDFIsActive
				+ ", coursePDFIsDeletedByAdmin=" + coursePDFIsDeletedByAdmin + ", coursePDFLastUpdatedDateTime="
				+ coursePDFLastUpdatedDateTime + ", coursePDFCreatedDateTime=" + coursePDFCreatedDateTime + "]";
	}
	
}
