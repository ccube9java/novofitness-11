package com.cube9.novafitness.model.course;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;

@Entity(name = "course_cover_video_info")
public class CourseCoverVideoInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "course_cover_video_id")
	private Long courseCoverVideoId;

	@Column(name = "course_cover_video_name")
	private String courseCoverVideoName;

	@Column(name = "course_cover_video_stored_path")
	private String courseCoverVideoStoredPath;

	@Column(name = "course_cover_video_view_path")
	private String courseCoverVideoViewPath;

	@Column(name = "course_cover_video_file_type")
	private String courseCoverVideoFileType;

	@Column(name = "course_video_vimeo_link")
	private String courseCoverVideoVimeoLink;
	
	@Column(name = "course_video_vimeo_name")
	private String courseCoverVideoVimeoName;
	
	@Column(name = "is_Active", columnDefinition = "boolean default true")
	private boolean courseCoverVideoIsActive = true;

	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean courseCoverVideoIsDeletedByAdmin = false;

	@Column(name = "course_cover_video_last_updated_dateTime")
	private LocalDateTime courseCoverVideoLastUpdatedDateTime;

	@Column(name = "course_cover_video_created_dateTime")
	@CreationTimestamp
	private LocalDateTime courseCoverVideoCreatedDateTime;

	public Long getCourseCoverVideoId() {
		return courseCoverVideoId;
	}

	public void setCourseCoverVideoId(Long courseCoverVideoId) {
		this.courseCoverVideoId = courseCoverVideoId;
	}

	public String getCourseCoverVideoName() {
		return courseCoverVideoName;
	}

	public void setCourseCoverVideoName(String courseCoverVideoName) {
		this.courseCoverVideoName = courseCoverVideoName;
	}

	public String getCourseCoverVideoStoredPath() {
		return courseCoverVideoStoredPath;
	}

	public void setCourseCoverVideoStoredPath(String courseCoverVideoStoredPath) {
		this.courseCoverVideoStoredPath = courseCoverVideoStoredPath;
	}

	public String getCourseCoverVideoViewPath() {
		return courseCoverVideoViewPath;
	}

	public void setCourseCoverVideoViewPath(String courseCoverVideoViewPath) {
		this.courseCoverVideoViewPath = courseCoverVideoViewPath;
	}

	public String getCourseCoverVideoFileType() {
		return courseCoverVideoFileType;
	}

	public void setCourseCoverVideoFileType(String courseCoverVideoFileType) {
		this.courseCoverVideoFileType = courseCoverVideoFileType;
	}

	public String getCourseCoverVideoVimeoLink() {
		return courseCoverVideoVimeoLink;
	}

	public void setCourseCoverVideoVimeoLink(String courseCoverVideoVimeoLink) {
		this.courseCoverVideoVimeoLink = courseCoverVideoVimeoLink;
	}

	public String getCourseCoverVideoVimeoName() {
		return courseCoverVideoVimeoName;
	}

	public void setCourseCoverVideoVimeoName(String courseCoverVideoVimeoName) {
		this.courseCoverVideoVimeoName = courseCoverVideoVimeoName;
	}

	public boolean isCourseCoverVideoIsActive() {
		return courseCoverVideoIsActive;
	}

	public void setCourseCoverVideoIsActive(boolean courseCoverVideoIsActive) {
		this.courseCoverVideoIsActive = courseCoverVideoIsActive;
	}

	public boolean isCourseCoverVideoIsDeletedByAdmin() {
		return courseCoverVideoIsDeletedByAdmin;
	}

	public void setCourseCoverVideoIsDeletedByAdmin(boolean courseCoverVideoIsDeletedByAdmin) {
		this.courseCoverVideoIsDeletedByAdmin = courseCoverVideoIsDeletedByAdmin;
	}

	public LocalDateTime getCourseCoverVideoLastUpdatedDateTime() {
		return courseCoverVideoLastUpdatedDateTime;
	}

	public void setCourseCoverVideoLastUpdatedDateTime(LocalDateTime courseCoverVideoLastUpdatedDateTime) {
		this.courseCoverVideoLastUpdatedDateTime = courseCoverVideoLastUpdatedDateTime;
	}

	public LocalDateTime getCourseCoverVideoCreatedDateTime() {
		return courseCoverVideoCreatedDateTime;
	}

	public void setCourseCoverVideoCreatedDateTime(LocalDateTime courseCoverVideoCreatedDateTime) {
		this.courseCoverVideoCreatedDateTime = courseCoverVideoCreatedDateTime;
	}

	@Override
	public String toString() {
		return "CourseCoverVideoInfo [courseCoverVideoId=" + courseCoverVideoId + ", courseCoverVideoName="
				+ courseCoverVideoName + ", courseCoverVideoStoredPath=" + courseCoverVideoStoredPath
				+ ", courseCoverVideoViewPath=" + courseCoverVideoViewPath + ", courseCoverVideoFileType="
				+ courseCoverVideoFileType + ", courseCoverVideoVimeoLink=" + courseCoverVideoVimeoLink
				+ ", courseCoverVideoVimeoName=" + courseCoverVideoVimeoName + ", courseCoverVideoIsActive="
				+ courseCoverVideoIsActive + ", courseCoverVideoIsDeletedByAdmin=" + courseCoverVideoIsDeletedByAdmin
				+ ", courseCoverVideoLastUpdatedDateTime=" + courseCoverVideoLastUpdatedDateTime
				+ ", courseCoverVideoCreatedDateTime=" + courseCoverVideoCreatedDateTime + "]";
	}
	
}
