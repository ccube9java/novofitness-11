package com.cube9.novafitness.model.course;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

@Entity(name = "course_other_videos_info")
public class CourseOtherVideosInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "course_other_videos_id")
	private Long courseOtherVideosId;
	
	@OneToOne
	@JoinColumn(name="course_id")
	private CourseInfo courseInfo;
	
	@OneToOne
	@JoinColumn(name="course_video_id")
	private CourseVideoInfo courseVideoInfo;
	
	@Column(name = "is_Active", columnDefinition = "boolean default false")
	private boolean courseOtherVideosIsActive=false;
	
	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean courseOtherVideosIsDeletedByAdmin=false;
	
	@Column(name = "course_other_videos_last_updated_dateTime")
	private LocalDateTime courseOtherVideosLastUpdatedDateTime;
	
	@Column(name = "course_other_videos_created_dateTime")
	@CreationTimestamp
	private LocalDateTime courseOtherVideosCreatedDateTime;

	public Long getCourseOtherVideosId() {
		return courseOtherVideosId;
	}

	public void setCourseOtherVideosId(Long courseOtherVideosId) {
		this.courseOtherVideosId = courseOtherVideosId;
	}

	public CourseInfo getCourseInfo() {
		return courseInfo;
	}

	public void setCourseInfo(CourseInfo courseInfo) {
		this.courseInfo = courseInfo;
	}

	public CourseVideoInfo getCourseVideoInfo() {
		return courseVideoInfo;
	}

	public void setCourseVideoInfo(CourseVideoInfo courseVideoInfo) {
		this.courseVideoInfo = courseVideoInfo;
	}

	public boolean isCourseOtherVideosIsActive() {
		return courseOtherVideosIsActive;
	}

	public void setCourseOtherVideosIsActive(boolean courseOtherVideosIsActive) {
		this.courseOtherVideosIsActive = courseOtherVideosIsActive;
	}

	public boolean isCourseOtherVideosIsDeletedByAdmin() {
		return courseOtherVideosIsDeletedByAdmin;
	}

	public void setCourseOtherVideosIsDeletedByAdmin(boolean courseOtherVideosIsDeletedByAdmin) {
		this.courseOtherVideosIsDeletedByAdmin = courseOtherVideosIsDeletedByAdmin;
	}

	public LocalDateTime getCourseOtherVideosLastUpdatedDateTime() {
		return courseOtherVideosLastUpdatedDateTime;
	}

	public void setCourseOtherVideosLastUpdatedDateTime(LocalDateTime courseOtherVideosLastUpdatedDateTime) {
		this.courseOtherVideosLastUpdatedDateTime = courseOtherVideosLastUpdatedDateTime;
	}

	public LocalDateTime getCourseOtherVideosCreatedDateTime() {
		return courseOtherVideosCreatedDateTime;
	}

	public void setCourseOtherVideosCreatedDateTime(LocalDateTime courseOtherVideosCreatedDateTime) {
		this.courseOtherVideosCreatedDateTime = courseOtherVideosCreatedDateTime;
	}

	@Override
	public String toString() {
		return "CourseOtherVideosInfo [courseOtherVideosId=" + courseOtherVideosId + ", courseInfo=" + courseInfo
				+ ", courseVideoInfo=" + courseVideoInfo + ", courseOtherVideosIsActive=" + courseOtherVideosIsActive
				+ ", courseOtherVideosIsDeletedByAdmin=" + courseOtherVideosIsDeletedByAdmin
				+ ", courseOtherVideosLastUpdatedDateTime=" + courseOtherVideosLastUpdatedDateTime
				+ ", courseOtherVideosCreatedDateTime=" + courseOtherVideosCreatedDateTime + "]";
	}
	
}
