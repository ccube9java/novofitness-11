package com.cube9.novafitness.model.course;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;

@Entity(name = "course_cover_image_info")
public class CourseCoverImageInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "course_cover_image_id")
	private Long courseCoverImageId;

	@Column(name = "course_cover_image_name")
	private String courseCoverImageName;

	@Column(name = "course_cover_image_stored_path")
	private String courseCoverImageStoredPath;

	@Column(name = "course_cover_image_view_path")
	private String courseCoverImageViewPath;

	@Column(name = "course_cover_image_file_type")
	private String courseCoverImageFileType;

	@Column(name = "is_Active", columnDefinition = "boolean default true")
	private boolean courseCoverImageIsActive = true;

	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean courseCoverImageIsDeletedByAdmin = false;

	@Column(name = "course_cover_image_last_updated_dateTime")
	private LocalDateTime courseCoverImageLastUpdatedDateTime;

	@Column(name = "course_cover_image_created_dateTime")
	@CreationTimestamp
	private LocalDateTime courseCoverImageCreatedDateTime;

	public Long getCourseCoverImageId() {
		return courseCoverImageId;
	}

	public void setCourseCoverImageId(Long courseCoverImageId) {
		this.courseCoverImageId = courseCoverImageId;
	}

	public String getCourseCoverImageName() {
		return courseCoverImageName;
	}

	public void setCourseCoverImageName(String courseCoverImageName) {
		this.courseCoverImageName = courseCoverImageName;
	}

	public String getCourseCoverImageStoredPath() {
		return courseCoverImageStoredPath;
	}

	public void setCourseCoverImageStoredPath(String courseCoverImageStoredPath) {
		this.courseCoverImageStoredPath = courseCoverImageStoredPath;
	}

	public String getCourseCoverImageViewPath() {
		return courseCoverImageViewPath;
	}

	public void setCourseCoverImageViewPath(String courseCoverImageViewPath) {
		this.courseCoverImageViewPath = courseCoverImageViewPath;
	}

	public String getCourseCoverImageFileType() {
		return courseCoverImageFileType;
	}

	public void setCourseCoverImageFileType(String courseCoverImageFileType) {
		this.courseCoverImageFileType = courseCoverImageFileType;
	}

	public boolean isCourseCoverImageIsActive() {
		return courseCoverImageIsActive;
	}

	public void setCourseCoverImageIsActive(boolean courseCoverImageIsActive) {
		this.courseCoverImageIsActive = courseCoverImageIsActive;
	}

	public boolean isCourseCoverImageIsDeletedByAdmin() {
		return courseCoverImageIsDeletedByAdmin;
	}

	public void setCourseCoverImageIsDeletedByAdmin(boolean courseCoverImageIsDeletedByAdmin) {
		this.courseCoverImageIsDeletedByAdmin = courseCoverImageIsDeletedByAdmin;
	}

	public LocalDateTime getCourseCoverImageLastUpdatedDateTime() {
		return courseCoverImageLastUpdatedDateTime;
	}

	public void setCourseCoverImageLastUpdatedDateTime(LocalDateTime courseCoverImageLastUpdatedDateTime) {
		this.courseCoverImageLastUpdatedDateTime = courseCoverImageLastUpdatedDateTime;
	}

	public LocalDateTime getCourseCoverImageCreatedDateTime() {
		return courseCoverImageCreatedDateTime;
	}

	public void setCourseCoverImageCreatedDateTime(LocalDateTime courseCoverImageCreatedDateTime) {
		this.courseCoverImageCreatedDateTime = courseCoverImageCreatedDateTime;
	}

	@Override
	public String toString() {
		return "CourseCoverImageInfo [courseCoverImageId=" + courseCoverImageId + ", courseCoverImageName="
				+ courseCoverImageName + ", courseCoverImageStoredPath=" + courseCoverImageStoredPath
				+ ", courseCoverImageViewPath=" + courseCoverImageViewPath + ", courseCoverImageFileType="
				+ courseCoverImageFileType + ", courseCoverImageIsActive=" + courseCoverImageIsActive
				+ ", courseCoverImageIsDeletedByAdmin=" + courseCoverImageIsDeletedByAdmin
				+ ", courseCoverImageLastUpdatedDateTime=" + courseCoverImageLastUpdatedDateTime
				+ ", courseCoverImageCreatedDateTime=" + courseCoverImageCreatedDateTime + "]";
	}

}
