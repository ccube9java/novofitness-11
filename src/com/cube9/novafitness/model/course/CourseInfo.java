package com.cube9.novafitness.model.course;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

import com.cube9.novafitness.model.admin.CategoryInfo;

@Entity(name = "course_info")
public class CourseInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "course_id")
	private Long courseId;

	@Column(name = "course_name")
	private String courseName;

	@Column(name = "course_description", columnDefinition = "VARCHAR(5000)")
	private String courseDescription;

	@Column(name = "course_content", columnDefinition = "VARCHAR(5000)")
	private String courseContent;
	
	@Column(name = "course_language")
	private String courseLanguage;

	@OneToOne
	@JoinColumn(name = "category_id")
	private CategoryInfo courseCategoryInfo;

	@Column(name = "course_price")
	private Long coursePrice;

	@Column(name = "course_cost")
	private Double courseCost;
	
	@OneToOne
	@JoinColumn(name = "course_cover_image_id")
	private CourseCoverImageInfo courseCoverImageInfo;
	
	@OneToOne
	@JoinColumn(name = "course_cover_video_id")
	private CourseCoverVideoInfo courseCoverVideoInfo;

	@Column(name = "is_Active", columnDefinition = "boolean default true")
	private boolean courseIsActive = true;

	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean courseIsDeletedByAdmin = false;

	@Column(name = "course_last_updated_dateTime")
	private LocalDateTime courseLastUpdatedDateTime;

	@Column(name = "course_created_dateTime")
	@CreationTimestamp
	private LocalDateTime courseCreatedDateTime;

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCourseDescription() {
		return courseDescription;
	}

	public void setCourseDescription(String courseDescription) {
		this.courseDescription = courseDescription;
	}

	public String getCourseContent() {
		return courseContent;
	}

	public void setCourseContent(String courseContent) {
		this.courseContent = courseContent;
	}

	public String getCourseLanguage() {
		return courseLanguage;
	}

	public void setCourseLanguage(String courseLanguage) {
		this.courseLanguage = courseLanguage;
	}

	public CategoryInfo getCourseCategoryInfo() {
		return courseCategoryInfo;
	}

	public void setCourseCategoryInfo(CategoryInfo courseCategoryInfo) {
		this.courseCategoryInfo = courseCategoryInfo;
	}

	public Long getCoursePrice() {
		return coursePrice;
	}

	public void setCoursePrice(Long coursePrice) {
		this.coursePrice = coursePrice;
	}

	public Double getCourseCost() {
		return courseCost;
	}

	public void setCourseCost(Double courseCost) {
		this.courseCost = courseCost;
	}

	public CourseCoverImageInfo getCourseCoverImageInfo() {
		return courseCoverImageInfo;
	}

	public void setCourseCoverImageInfo(CourseCoverImageInfo courseCoverImageInfo) {
		this.courseCoverImageInfo = courseCoverImageInfo;
	}

	public CourseCoverVideoInfo getCourseCoverVideoInfo() {
		return courseCoverVideoInfo;
	}

	public void setCourseCoverVideoInfo(CourseCoverVideoInfo courseCoverVideoInfo) {
		this.courseCoverVideoInfo = courseCoverVideoInfo;
	}

	public boolean isCourseIsActive() {
		return courseIsActive;
	}

	public void setCourseIsActive(boolean courseIsActive) {
		this.courseIsActive = courseIsActive;
	}

	public boolean isCourseIsDeletedByAdmin() {
		return courseIsDeletedByAdmin;
	}

	public void setCourseIsDeletedByAdmin(boolean courseIsDeletedByAdmin) {
		this.courseIsDeletedByAdmin = courseIsDeletedByAdmin;
	}

	public LocalDateTime getCourseLastUpdatedDateTime() {
		return courseLastUpdatedDateTime;
	}

	public void setCourseLastUpdatedDateTime(LocalDateTime courseLastUpdatedDateTime) {
		this.courseLastUpdatedDateTime = courseLastUpdatedDateTime;
	}

	public LocalDateTime getCourseCreatedDateTime() {
		return courseCreatedDateTime;
	}

	public void setCourseCreatedDateTime(LocalDateTime courseCreatedDateTime) {
		this.courseCreatedDateTime = courseCreatedDateTime;
	}

	@Override
	public String toString() {
		return "CourseInfo [courseId=" + courseId + ", courseName=" + courseName + ", courseDescription="
				+ courseDescription + ", courseContent=" + courseContent + ", courseLanguage=" + courseLanguage
				+ ", courseCategoryInfo=" + courseCategoryInfo + ", coursePrice=" + coursePrice + ", courseCost="
				+ courseCost + ", courseCoverImageInfo=" + courseCoverImageInfo + ", courseCoverVideoInfo="
				+ courseCoverVideoInfo + ", courseIsActive=" + courseIsActive + ", courseIsDeletedByAdmin="
				+ courseIsDeletedByAdmin + ", courseLastUpdatedDateTime=" + courseLastUpdatedDateTime
				+ ", courseCreatedDateTime=" + courseCreatedDateTime + "]";
	}

}