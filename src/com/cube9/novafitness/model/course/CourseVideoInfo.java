package com.cube9.novafitness.model.course;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;

@Entity(name="course_video_info")
public class CourseVideoInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "course_video_id")
	private Long courseVideoId;
	
	@Column(name = "course_video_name")
	private String courseVideoName;
	
	@Column(name = "course_video_stored_path")
	private String courseVideoStoredPath;
	
	@Column(name = "course_video_view_path")
	private String courseVideoViewPath;
	
	@Column(name = "course_video_file_type")
	private String courseVideoFileType;
	
	@Column(name = "course_video_vimeo_link")
	private String courseVideoVimeoLink;
	
	@Column(name = "course_video_vimeo_name")
	private String courseVideoVimeoName;
	
	@Column(name = "is_Active", columnDefinition = "boolean default true")
	private boolean courseVideoIsActive=true;
	
	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean courseVideoIsDeletedByAdmin=false;
	
	@Column(name = "course_video_last_updated_dateTime")
	private LocalDateTime courseVideoLastUpdatedDateTime;
	
	@Column(name = "course_video_created_dateTime")
	@CreationTimestamp
	private LocalDateTime courseVideoCreatedDateTime;

	public Long getCourseVideoId() {
		return courseVideoId;
	}

	public void setCourseVideoId(Long courseVideoId) {
		this.courseVideoId = courseVideoId;
	}

	public String getCourseVideoName() {
		return courseVideoName;
	}

	public void setCourseVideoName(String courseVideoName) {
		this.courseVideoName = courseVideoName;
	}

	public String getCourseVideoStoredPath() {
		return courseVideoStoredPath;
	}

	public void setCourseVideoStoredPath(String courseVideoStoredPath) {
		this.courseVideoStoredPath = courseVideoStoredPath;
	}

	public String getCourseVideoViewPath() {
		return courseVideoViewPath;
	}

	public void setCourseVideoViewPath(String courseVideoViewPath) {
		this.courseVideoViewPath = courseVideoViewPath;
	}

	public String getCourseVideoFileType() {
		return courseVideoFileType;
	}

	public String getCourseVideoVimeoLink() {
		return courseVideoVimeoLink;
	}

	public void setCourseVideoVimeoLink(String courseVideoVimeoLink) {
		this.courseVideoVimeoLink = courseVideoVimeoLink;
	}

	public String getCourseVideoVimeoName() {
		return courseVideoVimeoName;
	}

	public void setCourseVideoVimeoName(String courseVideoVimeoName) {
		this.courseVideoVimeoName = courseVideoVimeoName;
	}

	public void setCourseVideoFileType(String courseVideoFileType) {
		this.courseVideoFileType = courseVideoFileType;
	}

	public boolean isCourseVideoIsActive() {
		return courseVideoIsActive;
	}

	public void setCourseVideoIsActive(boolean courseVideoIsActive) {
		this.courseVideoIsActive = courseVideoIsActive;
	}

	public boolean isCourseVideoIsDeletedByAdmin() {
		return courseVideoIsDeletedByAdmin;
	}

	public void setCourseVideoIsDeletedByAdmin(boolean courseVideoIsDeletedByAdmin) {
		this.courseVideoIsDeletedByAdmin = courseVideoIsDeletedByAdmin;
	}

	public LocalDateTime getCourseVideoLastUpdatedDateTime() {
		return courseVideoLastUpdatedDateTime;
	}

	public void setCourseVideoLastUpdatedDateTime(LocalDateTime courseVideoLastUpdatedDateTime) {
		this.courseVideoLastUpdatedDateTime = courseVideoLastUpdatedDateTime;
	}

	public LocalDateTime getCourseVideoCreatedDateTime() {
		return courseVideoCreatedDateTime;
	}

	public void setCourseVideoCreatedDateTime(LocalDateTime courseVideoCreatedDateTime) {
		this.courseVideoCreatedDateTime = courseVideoCreatedDateTime;
	}

	@Override
	public String toString() {
		return "CourseVideoInfo [courseVideoId=" + courseVideoId + ", courseVideoName=" + courseVideoName
				+ ", courseVideoStoredPath=" + courseVideoStoredPath + ", courseVideoViewPath=" + courseVideoViewPath
				+ ", courseVideoFileType=" + courseVideoFileType + ", courseVideoVimeoLink=" + courseVideoVimeoLink
				+ ", courseVideoVimeoName=" + courseVideoVimeoName + ", courseVideoIsActive=" + courseVideoIsActive
				+ ", courseVideoIsDeletedByAdmin=" + courseVideoIsDeletedByAdmin + ", courseVideoLastUpdatedDateTime="
				+ courseVideoLastUpdatedDateTime + ", courseVideoCreatedDateTime=" + courseVideoCreatedDateTime + "]";
	}
	
}
