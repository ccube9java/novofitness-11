package com.cube9.novafitness.model.course;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;

@Entity(name = "course_images_info")
public class CourseImagesInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "course_images_id")
	private Long courseImagesId;

	@Column(name = "course_images_name")
	private String courseImagesName;

	@Column(name = "course_images_stored_path")
	private String courseImagesStoredPath;

	@Column(name = "course_images_view_path")
	private String courseImagesViewPath;

	@Column(name = "course_images_file_type")
	private String courseImagesFileType;

	@Column(name = "is_Active", columnDefinition = "boolean default true")
	private boolean courseImagesIsActive = true;

	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean courseImagesIsDeletedByAdmin = false;

	@Column(name = "course_images_last_updated_dateTime")
	private LocalDateTime courseImagesLastUpdatedDateTime;

	@Column(name = "course_images_created_dateTime")
	@CreationTimestamp
	private LocalDateTime courseImagesCreatedDateTime;

	public Long getCourseImagesId() {
		return courseImagesId;
	}

	public void setCourseImagesId(Long courseImagesId) {
		this.courseImagesId = courseImagesId;
	}

	public String getCourseImagesName() {
		return courseImagesName;
	}

	public void setCourseImagesName(String courseImagesName) {
		this.courseImagesName = courseImagesName;
	}

	public String getCourseImagesStoredPath() {
		return courseImagesStoredPath;
	}

	public void setCourseImagesStoredPath(String courseImagesStoredPath) {
		this.courseImagesStoredPath = courseImagesStoredPath;
	}

	public String getCourseImagesViewPath() {
		return courseImagesViewPath;
	}

	public void setCourseImagesViewPath(String courseImagesViewPath) {
		this.courseImagesViewPath = courseImagesViewPath;
	}

	public String getCourseImagesFileType() {
		return courseImagesFileType;
	}

	public void setCourseImagesFileType(String courseImagesFileType) {
		this.courseImagesFileType = courseImagesFileType;
	}

	public boolean isCourseImagesIsActive() {
		return courseImagesIsActive;
	}

	public void setCourseImagesIsActive(boolean courseImagesIsActive) {
		this.courseImagesIsActive = courseImagesIsActive;
	}

	public boolean isCourseImagesIsDeletedByAdmin() {
		return courseImagesIsDeletedByAdmin;
	}

	public void setCourseImagesIsDeletedByAdmin(boolean courseImagesIsDeletedByAdmin) {
		this.courseImagesIsDeletedByAdmin = courseImagesIsDeletedByAdmin;
	}

	public LocalDateTime getCourseImagesLastUpdatedDateTime() {
		return courseImagesLastUpdatedDateTime;
	}

	public void setCourseImagesLastUpdatedDateTime(LocalDateTime courseImagesLastUpdatedDateTime) {
		this.courseImagesLastUpdatedDateTime = courseImagesLastUpdatedDateTime;
	}

	public LocalDateTime getCourseImagesCreatedDateTime() {
		return courseImagesCreatedDateTime;
	}

	public void setCourseImagesCreatedDateTime(LocalDateTime courseImagesCreatedDateTime) {
		this.courseImagesCreatedDateTime = courseImagesCreatedDateTime;
	}

	@Override
	public String toString() {
		return "CourseImagesInfo [courseImagesId=" + courseImagesId + ", courseImagesName=" + courseImagesName
				+ ", courseImagesStoredPath=" + courseImagesStoredPath + ", courseImagesViewPath="
				+ courseImagesViewPath + ", courseImagesFileType=" + courseImagesFileType + ", courseImagesIsActive="
				+ courseImagesIsActive + ", courseImagesIsDeletedByAdmin=" + courseImagesIsDeletedByAdmin
				+ ", courseImagesLastUpdatedDateTime=" + courseImagesLastUpdatedDateTime
				+ ", courseImagesCreatedDateTime=" + courseImagesCreatedDateTime + "]";
	}

}
