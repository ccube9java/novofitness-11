package com.cube9.novafitness.model.course;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

@Entity(name = "course_other_images_info")
public class CourseOtherImagesInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "course_other_images_id")
	private Long courseOtherImagesId;
	
	@OneToOne
	@JoinColumn(name="course_id")
	private CourseInfo courseInfo;
	
	@OneToOne
	@JoinColumn(name="course_images_id")
	private CourseImagesInfo courseImagesInfo;
	
	@Column(name = "is_Active", columnDefinition = "boolean default false")
	private boolean courseOtherImagesIsActive=false;
	
	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean courseOtherImagesIsDeletedByAdmin=false;
	
	@Column(name = "course_other_images_last_updated_dateTime")
	private LocalDateTime courseOtherImagesLastUpdatedDateTime;
	
	@Column(name = "course_other_images_created_dateTime")
	@CreationTimestamp
	private LocalDateTime courseOtherImagesCreatedDateTime;

	public Long getCourseOtherImagesId() {
		return courseOtherImagesId;
	}

	public void setCourseOtherImagesId(Long courseOtherImagesId) {
		this.courseOtherImagesId = courseOtherImagesId;
	}

	public CourseInfo getCourseInfo() {
		return courseInfo;
	}

	public void setCourseInfo(CourseInfo courseInfo) {
		this.courseInfo = courseInfo;
	}

	public CourseImagesInfo getCourseImagesInfo() {
		return courseImagesInfo;
	}

	public void setCourseImagesInfo(CourseImagesInfo courseImagesInfo) {
		this.courseImagesInfo = courseImagesInfo;
	}

	public boolean isCourseOtherImagesIsActive() {
		return courseOtherImagesIsActive;
	}

	public void setCourseOtherImagesIsActive(boolean courseOtherImagesIsActive) {
		this.courseOtherImagesIsActive = courseOtherImagesIsActive;
	}

	public boolean isCourseOtherImagesIsDeletedByAdmin() {
		return courseOtherImagesIsDeletedByAdmin;
	}

	public void setCourseOtherImagesIsDeletedByAdmin(boolean courseOtherImagesIsDeletedByAdmin) {
		this.courseOtherImagesIsDeletedByAdmin = courseOtherImagesIsDeletedByAdmin;
	}

	public LocalDateTime getCourseOtherImagesLastUpdatedDateTime() {
		return courseOtherImagesLastUpdatedDateTime;
	}

	public void setCourseOtherImagesLastUpdatedDateTime(LocalDateTime courseOtherImagesLastUpdatedDateTime) {
		this.courseOtherImagesLastUpdatedDateTime = courseOtherImagesLastUpdatedDateTime;
	}

	public LocalDateTime getCourseOtherImagesCreatedDateTime() {
		return courseOtherImagesCreatedDateTime;
	}

	public void setCourseOtherImagesCreatedDateTime(LocalDateTime courseOtherImagesCreatedDateTime) {
		this.courseOtherImagesCreatedDateTime = courseOtherImagesCreatedDateTime;
	}

	@Override
	public String toString() {
		return "CourseOtherImagesInfo [courseOtherImagesId=" + courseOtherImagesId + ", courseInfo=" + courseInfo
				+ ", courseImagesInfo=" + courseImagesInfo + ", courseOtherImagesIsActive=" + courseOtherImagesIsActive
				+ ", courseOtherImagesIsDeletedByAdmin=" + courseOtherImagesIsDeletedByAdmin
				+ ", courseOtherImagesLastUpdatedDateTime=" + courseOtherImagesLastUpdatedDateTime
				+ ", courseOtherImagesCreatedDateTime=" + courseOtherImagesCreatedDateTime + "]";
	}
	
}
