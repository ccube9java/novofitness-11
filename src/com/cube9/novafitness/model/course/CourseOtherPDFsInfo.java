package com.cube9.novafitness.model.course;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

@Entity(name = "course_other_pdfs_info")
public class CourseOtherPDFsInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "course_other_pdfs_id")
	private Long courseOtherPDFsId;
	
	@OneToOne
	@JoinColumn(name="course_id")
	private CourseInfo courseInfo;
	
	@OneToOne
	@JoinColumn(name="course_pdf_id")
	private CoursePDFInfo coursePDFInfo;
	
	@Column(name = "is_Active", columnDefinition = "boolean default false")
	private boolean courseOtherPDFsIsActive=false;
	
	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean courseOtherPDFsIsDeletedByAdmin=false;
	
	@Column(name = "course_other_pdfs_last_updated_dateTime")
	private LocalDateTime courseOtherPDFsLastUpdatedDateTime;
	
	@Column(name = "course_other_pdfs_created_dateTime")
	@CreationTimestamp
	private LocalDateTime courseOtherPDFsCreatedDateTime;

	public Long getCourseOtherPDFsId() {
		return courseOtherPDFsId;
	}

	public void setCourseOtherPDFsId(Long courseOtherPDFsId) {
		this.courseOtherPDFsId = courseOtherPDFsId;
	}

	public CourseInfo getCourseInfo() {
		return courseInfo;
	}

	public void setCourseInfo(CourseInfo courseInfo) {
		this.courseInfo = courseInfo;
	}

	public CoursePDFInfo getCoursePDFInfo() {
		return coursePDFInfo;
	}

	public void setCoursePDFInfo(CoursePDFInfo coursePDFInfo) {
		this.coursePDFInfo = coursePDFInfo;
	}

	public boolean isCourseOtherPDFsIsActive() {
		return courseOtherPDFsIsActive;
	}

	public void setCourseOtherPDFsIsActive(boolean courseOtherPDFsIsActive) {
		this.courseOtherPDFsIsActive = courseOtherPDFsIsActive;
	}

	public boolean isCourseOtherPDFsIsDeletedByAdmin() {
		return courseOtherPDFsIsDeletedByAdmin;
	}

	public void setCourseOtherPDFsIsDeletedByAdmin(boolean courseOtherPDFsIsDeletedByAdmin) {
		this.courseOtherPDFsIsDeletedByAdmin = courseOtherPDFsIsDeletedByAdmin;
	}

	public LocalDateTime getCourseOtherPDFsLastUpdatedDateTime() {
		return courseOtherPDFsLastUpdatedDateTime;
	}

	public void setCourseOtherPDFsLastUpdatedDateTime(LocalDateTime courseOtherPDFsLastUpdatedDateTime) {
		this.courseOtherPDFsLastUpdatedDateTime = courseOtherPDFsLastUpdatedDateTime;
	}

	public LocalDateTime getCourseOtherPDFsCreatedDateTime() {
		return courseOtherPDFsCreatedDateTime;
	}

	public void setCourseOtherPDFsCreatedDateTime(LocalDateTime courseOtherPDFsCreatedDateTime) {
		this.courseOtherPDFsCreatedDateTime = courseOtherPDFsCreatedDateTime;
	}

	@Override
	public String toString() {
		return "CourseOtherPDFsInfo [courseOtherPDFsId=" + courseOtherPDFsId + ", courseInfo=" + courseInfo
				+ ", coursePDFInfo=" + coursePDFInfo + ", courseOtherPDFsIsActive=" + courseOtherPDFsIsActive
				+ ", courseOtherPDFsIsDeletedByAdmin=" + courseOtherPDFsIsDeletedByAdmin
				+ ", courseOtherPDFsLastUpdatedDateTime=" + courseOtherPDFsLastUpdatedDateTime
				+ ", courseOtherPDFsCreatedDateTime=" + courseOtherPDFsCreatedDateTime + "]";
	}
	
}
