package com.cube9.novafitness.model.user;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

import com.cube9.novafitness.model.admin.CouponCodeInfo;

@Entity(name = "user_order_info")
public class UserOrderInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_order_id")
	private Long userOrderId;

	@Column(name = "user_oder_number", columnDefinition = "VARCHAR(5000)")
	private String userOrderNumber;

	@Column(name = "paypal_payment_id", columnDefinition = "VARCHAR(500)")
	private String paypalPaymentId;

	@Column(name = "paypal_payer_id", columnDefinition = "VARCHAR(500)")
	private String paypalPayerId;

	@OneToOne
	@JoinColumn(name = "user_id")
	private UserInfo userInfo;

	@OneToOne
	@JoinColumn(name = "coupon_code_id")
	private CouponCodeInfo couponCodeInfo;

	@Column(name = "user_order_subtotal")
	private Double userOrderSubTotal;

	@Column(name = "user_order_shipping")
	private Double userOrderShipping;

	@Column(name = "user_order_tax")
	private Double userOrderTax;

	@Column(name = "user_order_total")
	private Double userOrderTotal;

	@Column(name = "is_Active", columnDefinition = "boolean default true")
	private boolean userOrderIsActive = true;

	@Column(name = "is_order_complete", columnDefinition = "boolean default false")
	private boolean userOrderIsComplete = false;

	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean userOrderIsDeletedByAdmin = false;

	@Column(name = "user_order_last_updated_dateTime")
	private LocalDateTime userOrderLastUpdatedDateTime;

	@Column(name = "user_order_created_dateTime")
	@CreationTimestamp
	private LocalDateTime userOrderCreatedDateTime;

	public Long getUserOrderId() {
		return userOrderId;
	}

	public void setUserOrderId(Long userOrderId) {
		this.userOrderId = userOrderId;
	}

	public String getUserOrderNumber() {
		return userOrderNumber;
	}

	public void setUserOrderNumber(String userOrderNumber) {
		this.userOrderNumber = userOrderNumber;
	}

	public CouponCodeInfo getCouponCodeInfo() {
		return couponCodeInfo;
	}

	public void setCouponCodeInfo(CouponCodeInfo couponCodeInfo) {
		this.couponCodeInfo = couponCodeInfo;
	}

	public String getPaypalPaymentId() {
		return paypalPaymentId;
	}

	public void setPaypalPaymentId(String paypalPaymentId) {
		this.paypalPaymentId = paypalPaymentId;
	}

	public String getPaypalPayerId() {
		return paypalPayerId;
	}

	public void setPaypalPayerId(String paypalPayerId) {
		this.paypalPayerId = paypalPayerId;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public Double getUserOrderSubTotal() {
		return userOrderSubTotal;
	}

	public void setUserOrderSubTotal(Double userOrderSubTotal) {
		this.userOrderSubTotal = userOrderSubTotal;
	}

	public Double getUserOrderShipping() {
		return userOrderShipping;
	}

	public void setUserOrderShipping(Double userOrderShipping) {
		this.userOrderShipping = userOrderShipping;
	}

	public Double getUserOrderTax() {
		return userOrderTax;
	}

	public void setUserOrderTax(Double userOrderTax) {
		this.userOrderTax = userOrderTax;
	}

	public Double getUserOrderTotal() {
		return userOrderTotal;
	}

	public void setUserOrderTotal(Double userOrderTotal) {
		this.userOrderTotal = userOrderTotal;
	}

	public boolean isUserOrderIsActive() {
		return userOrderIsActive;
	}

	public void setUserOrderIsActive(boolean userOrderIsActive) {
		this.userOrderIsActive = userOrderIsActive;
	}

	public boolean isUserOrderIsComplete() {
		return userOrderIsComplete;
	}

	public void setUserOrderIsComplete(boolean userOrderIsComplete) {
		this.userOrderIsComplete = userOrderIsComplete;
	}

	public boolean isUserOrderIsDeletedByAdmin() {
		return userOrderIsDeletedByAdmin;
	}

	public void setUserOrderIsDeletedByAdmin(boolean userOrderIsDeletedByAdmin) {
		this.userOrderIsDeletedByAdmin = userOrderIsDeletedByAdmin;
	}

	public LocalDateTime getUserOrderLastUpdatedDateTime() {
		return userOrderLastUpdatedDateTime;
	}

	public void setUserOrderLastUpdatedDateTime(LocalDateTime userOrderLastUpdatedDateTime) {
		this.userOrderLastUpdatedDateTime = userOrderLastUpdatedDateTime;
	}

	public LocalDateTime getUserOrderCreatedDateTime() {
		return userOrderCreatedDateTime;
	}

	public void setUserOrderCreatedDateTime(LocalDateTime userOrderCreatedDateTime) {
		this.userOrderCreatedDateTime = userOrderCreatedDateTime;
	}

	@Override
	public String toString() {
		return "UserOrderInfo [userOrderId=" + userOrderId + ", userOrderNumber=" + userOrderNumber
				+ ", paypalPaymentId=" + paypalPaymentId + ", paypalPayerId=" + paypalPayerId + ", userInfo=" + userInfo
				+ ", couponCodeInfo=" + couponCodeInfo + ", userOrderSubTotal=" + userOrderSubTotal
				+ ", userOrderShipping=" + userOrderShipping + ", userOrderTax=" + userOrderTax + ", userOrderTotal="
				+ userOrderTotal + ", userOrderIsActive=" + userOrderIsActive + ", userOrderIsComplete="
				+ userOrderIsComplete + ", userOrderIsDeletedByAdmin=" + userOrderIsDeletedByAdmin
				+ ", userOrderLastUpdatedDateTime=" + userOrderLastUpdatedDateTime + ", userOrderCreatedDateTime="
				+ userOrderCreatedDateTime + "]";
	}

}
