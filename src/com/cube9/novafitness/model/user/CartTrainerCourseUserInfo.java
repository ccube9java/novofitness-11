package com.cube9.novafitness.model.user;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

import com.cube9.novafitness.model.trainer.TrainerCourseInfo;

@Entity(name = "cart_trainer_course_user_info")
public class CartTrainerCourseUserInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cart_trainer_course_user_id")
	private Long cartTrainerCourseUserId;
	
	@OneToOne
	@JoinColumn(name="user_id")
	private UserInfo userInfo;
	
	@OneToOne
	@JoinColumn(name="trainer_course_id")
	private TrainerCourseInfo trainerCourseInfo;
	
	@Column(name = "is_Active", columnDefinition = "boolean default true")
	private boolean cartTrainerCourseUserIsActive=true;
	
	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean cartTrainerCourseUserIsDeletedByAdmin=false;
	
	@Column(name = "cart_trainer_course_user_last_updated_dateTime")
	private LocalDateTime cartTrainerCourseUserLastUpdatedDateTime;
	
	@Column(name = "cart_trainer_course_user_created_dateTime")
	@CreationTimestamp
	private LocalDateTime cartTrainerCourseUserCreatedDateTime;

	public Long getCartTrainerCourseUserId() {
		return cartTrainerCourseUserId;
	}

	public void setCartTrainerCourseUserId(Long cartTrainerCourseUserId) {
		this.cartTrainerCourseUserId = cartTrainerCourseUserId;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public TrainerCourseInfo getTrainerCourseInfo() {
		return trainerCourseInfo;
	}

	public void setTrainerCourseInfo(TrainerCourseInfo trainerCourseInfo) {
		this.trainerCourseInfo = trainerCourseInfo;
	}

	public boolean isCartTrainerCourseUserIsActive() {
		return cartTrainerCourseUserIsActive;
	}

	public void setCartTrainerCourseUserIsActive(boolean cartTrainerCourseUserIsActive) {
		this.cartTrainerCourseUserIsActive = cartTrainerCourseUserIsActive;
	}

	public boolean isCartTrainerCourseUserIsDeletedByAdmin() {
		return cartTrainerCourseUserIsDeletedByAdmin;
	}

	public void setCartTrainerCourseUserIsDeletedByAdmin(boolean cartTrainerCourseUserIsDeletedByAdmin) {
		this.cartTrainerCourseUserIsDeletedByAdmin = cartTrainerCourseUserIsDeletedByAdmin;
	}

	public LocalDateTime getCartTrainerCourseUserLastUpdatedDateTime() {
		return cartTrainerCourseUserLastUpdatedDateTime;
	}

	public void setCartTrainerCourseUserLastUpdatedDateTime(LocalDateTime cartTrainerCourseUserLastUpdatedDateTime) {
		this.cartTrainerCourseUserLastUpdatedDateTime = cartTrainerCourseUserLastUpdatedDateTime;
	}

	public LocalDateTime getCartTrainerCourseUserCreatedDateTime() {
		return cartTrainerCourseUserCreatedDateTime;
	}

	public void setCartTrainerCourseUserCreatedDateTime(LocalDateTime cartTrainerCourseUserCreatedDateTime) {
		this.cartTrainerCourseUserCreatedDateTime = cartTrainerCourseUserCreatedDateTime;
	}

	@Override
	public String toString() {
		return "CartTrainerCourseUserInfo [cartTrainerCourseUserId=" + cartTrainerCourseUserId + ", userInfo="
				+ userInfo + ", trainerCourseInfo=" + trainerCourseInfo + ", cartTrainerCourseUserIsActive="
				+ cartTrainerCourseUserIsActive + ", cartTrainerCourseUserIsDeletedByAdmin="
				+ cartTrainerCourseUserIsDeletedByAdmin + ", cartTrainerCourseUserLastUpdatedDateTime="
				+ cartTrainerCourseUserLastUpdatedDateTime + ", cartTrainerCourseUserCreatedDateTime="
				+ cartTrainerCourseUserCreatedDateTime + "]";
	}
	
}
