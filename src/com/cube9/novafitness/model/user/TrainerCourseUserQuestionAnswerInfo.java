package com.cube9.novafitness.model.user;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

@Entity(name = "trainer_course_user_question_answer_info")
public class TrainerCourseUserQuestionAnswerInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "trainer_course_user_question_answer_id")
	private Long trainerCourseUserQuestionAnswerId;
	
	@Column(name = "trainer_course_user_question_answer_details", columnDefinition = "VARCHAR(5000)" )
	private String trainerCourseUserQuestionAnswerDetails;
	
	@OneToOne
	@JoinColumn(name="answered_user_id")
	private UserInfo answeredUserInfo;
	
	@OneToOne
	@JoinColumn(name="trainer_course_user_question_id")
	private TrainerCourseUserQuestionInfo trainerCourseUserQuestionInfo;
	
	//@ManyToOne(cascade={CascadeType.ALL})
	//@JoinColumn(name="parent_trainer_course_user_question_answer_id", insertable = false, updatable = false)
	//private TrainerCourseUserQuestionAnswerInfo parentTrainerCourseUserQuestionAnswerInfo;

	//@OneToMany(mappedBy="parentTrainerCourseUserQuestionAnswerInfo")
	//private Set<TrainerCourseUserQuestionAnswerInfo> childTrainerCourseUserQuestionAnswerInfo = new HashSet<TrainerCourseUserQuestionAnswerInfo>();

	@OneToOne
	@JoinColumn(name="parent_trainer_course_user_question_answer_id")
	private TrainerCourseUserQuestionAnswerInfo trainerCourseUserQuestionAnswerInfo;
	
	@Column(name = "is_Active", columnDefinition = "boolean default true")
	private boolean trainerCourseUserQuestionAnswerIsActive=true;
	
	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean trainerCourseUserQuestionAnswerIsDeletedByAdmin=false;
	
	@Column(name = "trainer_course_user_question_answer_last_updated_dateTime")
	private LocalDateTime trainerCourseUserQuestionAnswerLastUpdatedDateTime;
	
	@Column(name = "trainer_course_user_question_answer_created_dateTime")
	@CreationTimestamp
	private LocalDateTime trainerCourseUserQuestionAnswerCreatedDateTime;

	public Long getTrainerCourseUserQuestionAnswerId() {
		return trainerCourseUserQuestionAnswerId;
	}

	public void setTrainerCourseUserQuestionAnswerId(Long trainerCourseUserQuestionAnswerId) {
		this.trainerCourseUserQuestionAnswerId = trainerCourseUserQuestionAnswerId;
	}

	public String getTrainerCourseUserQuestionAnswerDetails() {
		return trainerCourseUserQuestionAnswerDetails;
	}

	public void setTrainerCourseUserQuestionAnswerDetails(String trainerCourseUserQuestionAnswerDetails) {
		this.trainerCourseUserQuestionAnswerDetails = trainerCourseUserQuestionAnswerDetails;
	}

	public UserInfo getAnsweredUserInfo() {
		return answeredUserInfo;
	}

	public void setAnsweredUserInfo(UserInfo answeredUserInfo) {
		this.answeredUserInfo = answeredUserInfo;
	}

	public TrainerCourseUserQuestionInfo getTrainerCourseUserQuestionInfo() {
		return trainerCourseUserQuestionInfo;
	}

	public void setTrainerCourseUserQuestionInfo(TrainerCourseUserQuestionInfo trainerCourseUserQuestionInfo) {
		this.trainerCourseUserQuestionInfo = trainerCourseUserQuestionInfo;
	}

	public TrainerCourseUserQuestionAnswerInfo getTrainerCourseUserQuestionAnswerInfo() {
		return trainerCourseUserQuestionAnswerInfo;
	}

	public void setTrainerCourseUserQuestionAnswerInfo(
			TrainerCourseUserQuestionAnswerInfo trainerCourseUserQuestionAnswerInfo) {
		this.trainerCourseUserQuestionAnswerInfo = trainerCourseUserQuestionAnswerInfo;
	}

	public boolean isTrainerCourseUserQuestionAnswerIsActive() {
		return trainerCourseUserQuestionAnswerIsActive;
	}

	public void setTrainerCourseUserQuestionAnswerIsActive(boolean trainerCourseUserQuestionAnswerIsActive) {
		this.trainerCourseUserQuestionAnswerIsActive = trainerCourseUserQuestionAnswerIsActive;
	}

	public boolean isTrainerCourseUserQuestionAnswerIsDeletedByAdmin() {
		return trainerCourseUserQuestionAnswerIsDeletedByAdmin;
	}

	public void setTrainerCourseUserQuestionAnswerIsDeletedByAdmin(
			boolean trainerCourseUserQuestionAnswerIsDeletedByAdmin) {
		this.trainerCourseUserQuestionAnswerIsDeletedByAdmin = trainerCourseUserQuestionAnswerIsDeletedByAdmin;
	}

	public LocalDateTime getTrainerCourseUserQuestionAnswerLastUpdatedDateTime() {
		return trainerCourseUserQuestionAnswerLastUpdatedDateTime;
	}

	public void setTrainerCourseUserQuestionAnswerLastUpdatedDateTime(
			LocalDateTime trainerCourseUserQuestionAnswerLastUpdatedDateTime) {
		this.trainerCourseUserQuestionAnswerLastUpdatedDateTime = trainerCourseUserQuestionAnswerLastUpdatedDateTime;
	}

	public LocalDateTime getTrainerCourseUserQuestionAnswerCreatedDateTime() {
		return trainerCourseUserQuestionAnswerCreatedDateTime;
	}

	public void setTrainerCourseUserQuestionAnswerCreatedDateTime(
			LocalDateTime trainerCourseUserQuestionAnswerCreatedDateTime) {
		this.trainerCourseUserQuestionAnswerCreatedDateTime = trainerCourseUserQuestionAnswerCreatedDateTime;
	}

	@Override
	public String toString() {
		return "TrainerCourseUserQuestionAnswerInfo [trainerCourseUserQuestionAnswerId="
				+ trainerCourseUserQuestionAnswerId + ", trainerCourseUserQuestionAnswerDetails="
				+ trainerCourseUserQuestionAnswerDetails + ", answeredUserInfo=" + answeredUserInfo
				+ ", trainerCourseUserQuestionInfo=" + trainerCourseUserQuestionInfo
				+ ", trainerCourseUserQuestionAnswerInfo=" + trainerCourseUserQuestionAnswerInfo
				+ ", trainerCourseUserQuestionAnswerIsActive=" + trainerCourseUserQuestionAnswerIsActive
				+ ", trainerCourseUserQuestionAnswerIsDeletedByAdmin=" + trainerCourseUserQuestionAnswerIsDeletedByAdmin
				+ ", trainerCourseUserQuestionAnswerLastUpdatedDateTime="
				+ trainerCourseUserQuestionAnswerLastUpdatedDateTime
				+ ", trainerCourseUserQuestionAnswerCreatedDateTime=" + trainerCourseUserQuestionAnswerCreatedDateTime
				+ "]";
	}
	
}
