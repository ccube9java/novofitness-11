package com.cube9.novafitness.model.user;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;

@Entity(name = "gender_info")
public class GenderInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "gender_id")
	private Long genderId;

	@Column(name = "gender_title")
	private String genderTitle;

	@Column(name = "is_Active", columnDefinition = "boolean default true")
	private boolean genderIsActive = true;
	
	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean genderIsDeletedByAdmin = false;
	
	@Column(name = "gender_last_updated_dateTime")
	private LocalDateTime genderLastUpdatedDateTime;
	
	@Column(name = "gender_created_dateTime")
	@CreationTimestamp
	private LocalDateTime genderCreatedDateTime;

	public Long getGenderId() {
		return genderId;
	}

	public void setGenderId(Long genderId) {
		this.genderId = genderId;
	}

	public String getGenderTitle() {
		return genderTitle;
	}

	public void setGenderTitle(String genderTitle) {
		this.genderTitle = genderTitle;
	}

	public boolean isGenderIsActive() {
		return genderIsActive;
	}

	public void setGenderIsActive(boolean genderIsActive) {
		this.genderIsActive = genderIsActive;
	}

	public boolean isGenderIsDeletedByAdmin() {
		return genderIsDeletedByAdmin;
	}

	public void setGenderIsDeletedByAdmin(boolean genderIsDeletedByAdmin) {
		this.genderIsDeletedByAdmin = genderIsDeletedByAdmin;
	}

	public LocalDateTime getGenderLastUpdatedDateTime() {
		return genderLastUpdatedDateTime;
	}

	public void setGenderLastUpdatedDateTime(LocalDateTime genderLastUpdatedDateTime) {
		this.genderLastUpdatedDateTime = genderLastUpdatedDateTime;
	}

	public LocalDateTime getGenderCreatedDateTime() {
		return genderCreatedDateTime;
	}

	public void setGenderCreatedDateTime(LocalDateTime genderCreatedDateTime) {
		this.genderCreatedDateTime = genderCreatedDateTime;
	}

	@Override
	public String toString() {
		return "GenderInfo [genderId=" + genderId + ", genderTitle=" + genderTitle + ", genderIsActive="
				+ genderIsActive + ", genderIsDeletedByAdmin=" + genderIsDeletedByAdmin + ", genderLastUpdatedDateTime="
				+ genderLastUpdatedDateTime + ", genderCreatedDateTime=" + genderCreatedDateTime + "]";
	}
	
}
