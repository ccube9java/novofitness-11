package com.cube9.novafitness.model.user;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

import com.cube9.novafitness.model.course.CourseContributionInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseInfo;

@Entity(name = "user_course_payment_info")
public class UserCoursePaymentInfo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_course_payment_id")
	private Long userCoursePaymentId;

	@OneToOne
	@JoinColumn(name="trainer_course_id")
	private TrainerCourseInfo trainerCourseInfo;
	
	@OneToOne
	@JoinColumn(name = "user_id")
	private UserInfo userInfo;
	
	@OneToOne
	@JoinColumn(name = "user_order_id")
	private UserOrderInfo userOrderInfo;
	
	@OneToOne
	@JoinColumn(name = "course_contribution_id")
	private CourseContributionInfo courseContributionInfo;
	
	@Column(name = "trainer_contribution")
	private Double userPaymentTrainerContribution;
	
	@Column(name = "admin_contribution")
	private Double userPaymentAdminContribution;
	
	@Column(name = "is_admin_paid_trainer_contrubution", columnDefinition = "boolean default false")
	private boolean userCoursePaymentIsAdminPaidTrainerContrubution = false;
	
	@Column(name = "is_Active", columnDefinition = "boolean default true")
	private boolean userCoursePaymentIsActive = true;
	
	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean userCoursePaymentIsDeletedByAdmin = false;
	
	@Column(name = "user_course_payment_last_updated_dateTime")
	private LocalDateTime userCoursePaymentLastUpdatedDateTime;
	
	@Column(name = "user_course_payment_created_dateTime")
	@CreationTimestamp
	private LocalDateTime userCoursePaymentCreatedDateTime;

	public Long getUserCoursePaymentId() {
		return userCoursePaymentId;
	}

	public void setUserCoursePaymentId(Long userCoursePaymentId) {
		this.userCoursePaymentId = userCoursePaymentId;
	}

	public TrainerCourseInfo getTrainerCourseInfo() {
		return trainerCourseInfo;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public void setTrainerCourseInfo(TrainerCourseInfo trainerCourseInfo) {
		this.trainerCourseInfo = trainerCourseInfo;
	}

	public UserOrderInfo getUserOrderInfo() {
		return userOrderInfo;
	}

	public void setUserOrderInfo(UserOrderInfo userOrderInfo) {
		this.userOrderInfo = userOrderInfo;
	}

	public Double getUserPaymentTrainerContribution() {
		return userPaymentTrainerContribution;
	}

	public void setUserPaymentTrainerContribution(Double userPaymentTrainerContribution) {
		this.userPaymentTrainerContribution = userPaymentTrainerContribution;
	}

	public Double getUserPaymentAdminContribution() {
		return userPaymentAdminContribution;
	}

	public void setUserPaymentAdminContribution(Double userPaymentAdminContribution) {
		this.userPaymentAdminContribution = userPaymentAdminContribution;
	}

	public CourseContributionInfo getCourseContributionInfo() {
		return courseContributionInfo;
	}

	public void setCourseContributionInfo(CourseContributionInfo courseContributionInfo) {
		this.courseContributionInfo = courseContributionInfo;
	}

	public boolean isUserCoursePaymentIsAdminPaidTrainerContrubution() {
		return userCoursePaymentIsAdminPaidTrainerContrubution;
	}

	public void setUserCoursePaymentIsAdminPaidTrainerContrubution(
			boolean userCoursePaymentIsAdminPaidTrainerContrubution) {
		this.userCoursePaymentIsAdminPaidTrainerContrubution = userCoursePaymentIsAdminPaidTrainerContrubution;
	}

	public boolean isUserCoursePaymentIsActive() {
		return userCoursePaymentIsActive;
	}

	public void setUserCoursePaymentIsActive(boolean userCoursePaymentIsActive) {
		this.userCoursePaymentIsActive = userCoursePaymentIsActive;
	}

	public boolean isUserCoursePaymentIsDeletedByAdmin() {
		return userCoursePaymentIsDeletedByAdmin;
	}	

	public void setUserCoursePaymentIsDeletedByAdmin(boolean userCoursePaymentIsDeletedByAdmin) {
		this.userCoursePaymentIsDeletedByAdmin = userCoursePaymentIsDeletedByAdmin;
	}

	public LocalDateTime getUserCoursePaymentLastUpdatedDateTime() {
		return userCoursePaymentLastUpdatedDateTime;
	}

	public void setUserCoursePaymentLastUpdatedDateTime(LocalDateTime userCoursePaymentLastUpdatedDateTime) {
		this.userCoursePaymentLastUpdatedDateTime = userCoursePaymentLastUpdatedDateTime;
	}

	public LocalDateTime getUserCoursePaymentCreatedDateTime() {
		return userCoursePaymentCreatedDateTime;
	}

	public void setUserCoursePaymentCreatedDateTime(LocalDateTime userCoursePaymentCreatedDateTime) {
		this.userCoursePaymentCreatedDateTime = userCoursePaymentCreatedDateTime;
	}

	@Override
	public String toString() {
		return "UserCoursePaymentInfo [userCoursePaymentId=" + userCoursePaymentId + ", trainerCourseInfo="
				+ trainerCourseInfo + ", userInfo=" + userInfo + ", userOrderInfo=" + userOrderInfo
				+ ", courseContributionInfo=" + courseContributionInfo + ", userPaymentTrainerContribution="
				+ userPaymentTrainerContribution + ", userPaymentAdminContribution=" + userPaymentAdminContribution
				+ ", userCoursePaymentIsAdminPaidTrainerContrubution=" + userCoursePaymentIsAdminPaidTrainerContrubution
				+ ", userCoursePaymentIsActive=" + userCoursePaymentIsActive + ", userCoursePaymentIsDeletedByAdmin="
				+ userCoursePaymentIsDeletedByAdmin + ", userCoursePaymentLastUpdatedDateTime="
				+ userCoursePaymentLastUpdatedDateTime + ", userCoursePaymentCreatedDateTime="
				+ userCoursePaymentCreatedDateTime + "]";
	}

}
