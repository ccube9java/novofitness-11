package com.cube9.novafitness.model.user;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

@Entity(name = "user_info")
public class UserInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id")
	private Long userId;

	@Column(name = "user_title")
	private String userTitle;

	@Column(name = "user_fname")
	private String userFName;

	@Column(name = "user_lname")
	private String userLName;

	@Column(name = "user_email", unique = true)
	private String userEmail;

	@Column(name = "user_password", columnDefinition = "VARCHAR(5000)" )
	private String userPassword;

	@Column(name = "user_phone_no")
	private Long userPhoneNo;

	@Column(name = "user_location", columnDefinition = "VARCHAR(500)")
	private String userLocation;

	@Column(name = "user_city", columnDefinition = "VARCHAR(500)")
	private String userCity;
	
	@Column(name = "user_state", columnDefinition = "VARCHAR(500)")
	private String userState;
	
	@Column(name = "user_country", columnDefinition = "VARCHAR(500)")
	private String userCountry;
	
	@OneToOne
	@JoinColumn(name = "gender_id")
	private GenderInfo genderInfo;
	
	@OneToOne
	@JoinColumn(name = "user_profile_image_id")
	private UserProfileImageInfo userProfileImageInfo;
	
	@Column(name = "is_Active", columnDefinition = "boolean default false")
	private boolean userIsActive=false;
	
	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean userIsDeletedByAdmin=false;

	@Column(name = "user_last_login_dateTime")
	private LocalDateTime userLastLoginDateTime;
	
	@Column(name = "user_current_dateTime")
	@CreationTimestamp
	private LocalDateTime userCurrentDateTime;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserTitle() {
		return userTitle;
	}

	public void setUserTitle(String userTitle) {
		this.userTitle = userTitle;
	}

	public String getUserFName() {
		return userFName;
	}

	public void setUserFName(String userFName) {
		this.userFName = userFName;
	}

	public String getUserLName() {
		return userLName;
	}

	public void setUserLName(String userLName) {
		this.userLName = userLName;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public Long getUserPhoneNo() {
		return userPhoneNo;
	}

	public void setUserPhoneNo(Long userPhoneNo) {
		this.userPhoneNo = userPhoneNo;
	}

	public UserProfileImageInfo getUserProfileImageInfo() {
		return userProfileImageInfo;
	}

	public void setUserProfileImageInfo(UserProfileImageInfo userProfileImageInfo) {
		this.userProfileImageInfo = userProfileImageInfo;
	}

	public boolean isUserIsActive() {
		return userIsActive;
	}

	public void setUserIsActive(boolean userIsActive) {
		this.userIsActive = userIsActive;
	}

	public String getUserLocation() {
		return userLocation;
	}

	public void setUserLocation(String userLocation) {
		this.userLocation = userLocation;
	}

	public String getUserCity() {
		return userCity;
	}

	public void setUserCity(String userCity) {
		this.userCity = userCity;
	}

	public String getUserState() {
		return userState;
	}

	public void setUserState(String userState) {
		this.userState = userState;
	}

	public String getUserCountry() {
		return userCountry;
	}

	public void setUserCountry(String userCountry) {
		this.userCountry = userCountry;
	}

	public GenderInfo getGenderInfo() {
		return genderInfo;
	}

	public void setGenderInfo(GenderInfo genderInfo) {
		this.genderInfo = genderInfo;
	}

	public boolean isUserIsDeletedByAdmin() {
		return userIsDeletedByAdmin;
	}

	public void setUserIsDeletedByAdmin(boolean userIsDeletedByAdmin) {
		this.userIsDeletedByAdmin = userIsDeletedByAdmin;
	}

	public LocalDateTime getUserLastLoginDateTime() {
		return userLastLoginDateTime;
	}

	public void setUserLastLoginDateTime(LocalDateTime userLastLoginDateTime) {
		this.userLastLoginDateTime = userLastLoginDateTime;
	}

	public LocalDateTime getUserCurrentDateTime() {
		return userCurrentDateTime;
	}

	public void setUserCurrentDateTime(LocalDateTime userCurrentDateTime) {
		this.userCurrentDateTime = userCurrentDateTime;
	}

	@Override
	public String toString() {
		return "UserInfo [userId=" + userId + ", userTitle=" + userTitle + ", userFName=" + userFName + ", userLName="
				+ userLName + ", userEmail=" + userEmail + ", userPassword=" + userPassword + ", userPhoneNo="
				+ userPhoneNo + ", userProfileImageInfo=" + userProfileImageInfo + ", userIsActive=" + userIsActive
				+ ", userIsDeletedByAdmin=" + userIsDeletedByAdmin + ", userLastLoginDateTime=" + userLastLoginDateTime
				+ ", userCurrentDateTime=" + userCurrentDateTime + "]";
	}
	
}
