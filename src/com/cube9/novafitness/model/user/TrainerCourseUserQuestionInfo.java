package com.cube9.novafitness.model.user;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

import com.cube9.novafitness.model.trainer.TrainerCourseInfo;

@Entity(name = "trainer_course_user_question_info")
public class TrainerCourseUserQuestionInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "trainer_course_user_question_id")
	private Long trainerCourseUserQuestionId;
	
	@Column(name = "trainer_course_user_question_details", columnDefinition = "VARCHAR(500)" )
	private String trainerCourseUserQuestionDetails;
	
	@OneToOne
	@JoinColumn(name="user_id")
	private UserInfo userInfo;
	
	@OneToOne
	@JoinColumn(name="trainer_course_id")
	private TrainerCourseInfo trainerCourseInfo;
	
	@Column(name = "is_Active", columnDefinition = "boolean default true")
	private boolean trainerCourseUserQuestionIsActive=true;
	
	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean trainerCourseUserQuestionIsDeletedByAdmin=false;
	
	@Column(name = "trainer_course_user_question_last_updated_dateTime")
	private LocalDateTime trainerCourseUserQuestionLastUpdatedDateTime;
	
	@Column(name = "trainer_course_user_question_created_dateTime")
	@CreationTimestamp
	private LocalDateTime trainerCourseUserQuestionCreatedDateTime;

	public Long getTrainerCourseUserQuestionId() {
		return trainerCourseUserQuestionId;
	}

	public void setTrainerCourseUserQuestionId(Long trainerCourseUserQuestionId) {
		this.trainerCourseUserQuestionId = trainerCourseUserQuestionId;
	}

	public String getTrainerCourseUserQuestionDetails() {
		return trainerCourseUserQuestionDetails;
	}

	public void setTrainerCourseUserQuestionDetails(String trainerCourseUserQuestionDetails) {
		this.trainerCourseUserQuestionDetails = trainerCourseUserQuestionDetails;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public TrainerCourseInfo getTrainerCourseInfo() {
		return trainerCourseInfo;
	}

	public void setTrainerCourseInfo(TrainerCourseInfo trainerCourseInfo) {
		this.trainerCourseInfo = trainerCourseInfo;
	}

	public boolean isTrainerCourseUserQuestionIsActive() {
		return trainerCourseUserQuestionIsActive;
	}

	public void setTrainerCourseUserQuestionIsActive(boolean trainerCourseUserQuestionIsActive) {
		this.trainerCourseUserQuestionIsActive = trainerCourseUserQuestionIsActive;
	}

	public boolean isTrainerCourseUserQuestionIsDeletedByAdmin() {
		return trainerCourseUserQuestionIsDeletedByAdmin;
	}

	public void setTrainerCourseUserQuestionIsDeletedByAdmin(boolean trainerCourseUserQuestionIsDeletedByAdmin) {
		this.trainerCourseUserQuestionIsDeletedByAdmin = trainerCourseUserQuestionIsDeletedByAdmin;
	}

	public LocalDateTime getTrainerCourseUserQuestionLastUpdatedDateTime() {
		return trainerCourseUserQuestionLastUpdatedDateTime;
	}

	public void setTrainerCourseUserQuestionLastUpdatedDateTime(
			LocalDateTime trainerCourseUserQuestionLastUpdatedDateTime) {
		this.trainerCourseUserQuestionLastUpdatedDateTime = trainerCourseUserQuestionLastUpdatedDateTime;
	}

	public LocalDateTime getTrainerCourseUserQuestionCreatedDateTime() {
		return trainerCourseUserQuestionCreatedDateTime;
	}

	public void setTrainerCourseUserQuestionCreatedDateTime(LocalDateTime trainerCourseUserQuestionCreatedDateTime) {
		this.trainerCourseUserQuestionCreatedDateTime = trainerCourseUserQuestionCreatedDateTime;
	}

	@Override
	public String toString() {
		return "TrainerCourseUserQuestionInfo [trainerCourseUserQuestionId=" + trainerCourseUserQuestionId
				+ ", trainerCourseUserQuestionDetails=" + trainerCourseUserQuestionDetails + ", userInfo=" + userInfo
				+ ", trainerCourseInfo=" + trainerCourseInfo + ", trainerCourseUserQuestionIsActive="
				+ trainerCourseUserQuestionIsActive + ", trainerCourseUserQuestionIsDeletedByAdmin="
				+ trainerCourseUserQuestionIsDeletedByAdmin + ", trainerCourseUserQuestionLastUpdatedDateTime="
				+ trainerCourseUserQuestionLastUpdatedDateTime + ", trainerCourseUserQuestionCreatedDateTime="
				+ trainerCourseUserQuestionCreatedDateTime + "]";
	}
	
}
