package com.cube9.novafitness.model.user;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

@Entity(name = "user_referal_link_info")
public class UserReferalLinkInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_referal_link_id")
	private Long userReferalLinkId;

	@Column(name = "user_referal_link_email")
	private String userReferalLinkEmail;

	@Column(name = "user_referal_link_code", columnDefinition = "VARCHAR(500)")
	private String userReferalLinkCode;

	@OneToOne
	@JoinColumn(name = "user_id")
	private UserInfo userInfo;
	
	@Column(name = "is_Active", columnDefinition = "boolean default false")
	private boolean userReferalLinkIsActive=false;
	
	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean userReferalLinkIsDeletedByAdmin=false;

	@Column(name = "user_referal_link_last_updated_dateTime")
	private LocalDateTime userReferalLinkLastUpdatedDateTime;
	
	@Column(name = "user_referal_link_current_dateTime")
	@CreationTimestamp
	private LocalDateTime userReferalLinkCurrentDateTime;

	public Long getUserReferalLinkId() {
		return userReferalLinkId;
	}

	public void setUserReferalLinkId(Long userReferalLinkId) {
		this.userReferalLinkId = userReferalLinkId;
	}

	public String getUserReferalLinkEmail() {
		return userReferalLinkEmail;
	}

	public void setUserReferalLinkEmail(String userReferalLinkEmail) {
		this.userReferalLinkEmail = userReferalLinkEmail;
	}

	public String getUserReferalLinkCode() {
		return userReferalLinkCode;
	}

	public void setUserReferalLinkCode(String userReferalLinkCode) {
		this.userReferalLinkCode = userReferalLinkCode;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public boolean isUserReferalLinkIsActive() {
		return userReferalLinkIsActive;
	}

	public void setUserReferalLinkIsActive(boolean userReferalLinkIsActive) {
		this.userReferalLinkIsActive = userReferalLinkIsActive;
	}

	public boolean isUserReferalLinkIsDeletedByAdmin() {
		return userReferalLinkIsDeletedByAdmin;
	}

	public void setUserReferalLinkIsDeletedByAdmin(boolean userReferalLinkIsDeletedByAdmin) {
		this.userReferalLinkIsDeletedByAdmin = userReferalLinkIsDeletedByAdmin;
	}

	public LocalDateTime getUserReferalLinkLastUpdatedDateTime() {
		return userReferalLinkLastUpdatedDateTime;
	}

	public void setUserReferalLinkLastUpdatedDateTime(LocalDateTime userReferalLinkLastUpdatedDateTime) {
		this.userReferalLinkLastUpdatedDateTime = userReferalLinkLastUpdatedDateTime;
	}

	public LocalDateTime getUserReferalLinkCurrentDateTime() {
		return userReferalLinkCurrentDateTime;
	}

	public void setUserReferalLinkCurrentDateTime(LocalDateTime userReferalLinkCurrentDateTime) {
		this.userReferalLinkCurrentDateTime = userReferalLinkCurrentDateTime;
	}

	@Override
	public String toString() {
		return "UserReferalLinkInfo [userReferalLinkId=" + userReferalLinkId + ", userReferalLinkEmail="
				+ userReferalLinkEmail + ", userReferalLinkCode=" + userReferalLinkCode + ", userInfo=" + userInfo
				+ ", userReferalLinkIsActive=" + userReferalLinkIsActive + ", userReferalLinkIsDeletedByAdmin="
				+ userReferalLinkIsDeletedByAdmin + ", userReferalLinkLastUpdatedDateTime="
				+ userReferalLinkLastUpdatedDateTime + ", userReferalLinkCurrentDateTime="
				+ userReferalLinkCurrentDateTime + "]";
	}
	
}
