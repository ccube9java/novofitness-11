package com.cube9.novafitness.model.user;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

import com.cube9.novafitness.model.trainer.TrainerCourseInfo;


@Entity(name = "user_course_review_info")
public class UserCourseReviewInfo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_course_review_id")
	private Long userCourseReviewId;
	
	@Column(name = "user_location_review_title")
	private String userCourseReviewTitle;
	
	@Column(name = "user_location_review_description" , columnDefinition = "VARCHAR(5000)")
	private String userCourseReviewDescription;
	
	@Column(name = "user_course_review_star_rating")
	private Long userCourseStarRating;

	@OneToOne
	@JoinColumn(name = "user_id")
	private UserInfo userInfo;
	
	@OneToOne
	@JoinColumn(name = "trainer_course_id")
	private TrainerCourseInfo trainerCourseInfo;

	@Column(name = "is_Active", columnDefinition = "boolean default true")
	private boolean userCourseReviewIsActive = true;
	
	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean userCourseReviewIsDeletedByAdmin = false;
	
	@Column(name = "user_course_review_last_updated_dateTime")
	private LocalDateTime userCourseReviewLastUpdatedDateTime;
	
	@Column(name = "user_course_review_created_dateTime")
	@CreationTimestamp
	private LocalDateTime userCourseReviewCreatedDateTime;

	public Long getUserCourseReviewId() {
		return userCourseReviewId;
	}

	public void setUserCourseReviewId(Long userCourseReviewId) {
		this.userCourseReviewId = userCourseReviewId;
	}

	public String getUserCourseReviewTitle() {
		return userCourseReviewTitle;
	}

	public void setUserCourseReviewTitle(String userCourseReviewTitle) {
		this.userCourseReviewTitle = userCourseReviewTitle;
	}

	public String getUserCourseReviewDescription() {
		return userCourseReviewDescription;
	}

	public void setUserCourseReviewDescription(String userCourseReviewDescription) {
		this.userCourseReviewDescription = userCourseReviewDescription;
	}

	public Long getUserCourseStarRating() {
		return userCourseStarRating;
	}

	public void setUserCourseStarRating(Long userCourseStarRating) {
		this.userCourseStarRating = userCourseStarRating;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public TrainerCourseInfo getTrainerCourseInfo() {
		return trainerCourseInfo;
	}

	public void setTrainerCourseInfo(TrainerCourseInfo trainerCourseInfo) {
		this.trainerCourseInfo = trainerCourseInfo;
	}

	public boolean isUserCourseReviewIsActive() {
		return userCourseReviewIsActive;
	}

	public void setUserCourseReviewIsActive(boolean userCourseReviewIsActive) {
		this.userCourseReviewIsActive = userCourseReviewIsActive;
	}

	public boolean isUserCourseReviewIsDeletedByAdmin() {
		return userCourseReviewIsDeletedByAdmin;
	}

	public void setUserCourseReviewIsDeletedByAdmin(boolean userCourseReviewIsDeletedByAdmin) {
		this.userCourseReviewIsDeletedByAdmin = userCourseReviewIsDeletedByAdmin;
	}

	public LocalDateTime getUserCourseReviewLastUpdatedDateTime() {
		return userCourseReviewLastUpdatedDateTime;
	}

	public void setUserCourseReviewLastUpdatedDateTime(LocalDateTime userCourseReviewLastUpdatedDateTime) {
		this.userCourseReviewLastUpdatedDateTime = userCourseReviewLastUpdatedDateTime;
	}

	public LocalDateTime getUserCourseReviewCreatedDateTime() {
		return userCourseReviewCreatedDateTime;
	}

	public void setUserCourseReviewCreatedDateTime(LocalDateTime userCourseReviewCreatedDateTime) {
		this.userCourseReviewCreatedDateTime = userCourseReviewCreatedDateTime;
	}

	@Override
	public String toString() {
		return "UserCourseReviewInfo [userCourseReviewId=" + userCourseReviewId + ", userCourseReviewTitle="
				+ userCourseReviewTitle + ", userCourseReviewDescription=" + userCourseReviewDescription
				+ ", userCourseStarRating=" + userCourseStarRating + ", userInfo=" + userInfo + ", trainerCourseInfo="
				+ trainerCourseInfo + ", userCourseReviewIsActive=" + userCourseReviewIsActive
				+ ", userCourseReviewIsDeletedByAdmin=" + userCourseReviewIsDeletedByAdmin
				+ ", userCourseReviewLastUpdatedDateTime=" + userCourseReviewLastUpdatedDateTime
				+ ", userCourseReviewCreatedDateTime=" + userCourseReviewCreatedDateTime + "]";
	}

}
