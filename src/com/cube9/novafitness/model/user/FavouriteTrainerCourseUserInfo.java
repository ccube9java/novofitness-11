package com.cube9.novafitness.model.user;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

import com.cube9.novafitness.model.trainer.TrainerCourseInfo;

@Entity(name = "favourite_trainer_course_user_info")
public class FavouriteTrainerCourseUserInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "favourite_trainer_course_user_id")
	private Long favouriteTrainerCourseUserId;
	
	@OneToOne
	@JoinColumn(name="user_id")
	private UserInfo userInfo;
	
	@OneToOne
	@JoinColumn(name="trainer_course_id")
	private TrainerCourseInfo trainerCourseInfo;
	
	@Column(name = "is_Active", columnDefinition = "boolean default true")
	private boolean favouriteTrainerCourseUserIsActive=true;
	
	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean favouriteTrainerCourseUserIsDeletedByAdmin=false;
	
	@Column(name = "favourite_trainer_course_user_last_updated_dateTime")
	private LocalDateTime favouriteTrainerCourseUserLastUpdatedDateTime;
	
	@Column(name = "favourite_trainer_course_user_created_dateTime")
	@CreationTimestamp
	private LocalDateTime favouriteTrainerCourseUserCreatedDateTime;

	public Long getFavouriteTrainerCourseUserId() {
		return favouriteTrainerCourseUserId;
	}

	public void setFavouriteTrainerCourseUserId(Long favouriteTrainerCourseUserId) {
		this.favouriteTrainerCourseUserId = favouriteTrainerCourseUserId;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public TrainerCourseInfo getTrainerCourseInfo() {
		return trainerCourseInfo;
	}

	public void setTrainerCourseInfo(TrainerCourseInfo trainerCourseInfo) {
		this.trainerCourseInfo = trainerCourseInfo;
	}

	public boolean isFavouriteTrainerCourseUserIsActive() {
		return favouriteTrainerCourseUserIsActive;
	}

	public void setFavouriteTrainerCourseUserIsActive(boolean favouriteTrainerCourseUserIsActive) {
		this.favouriteTrainerCourseUserIsActive = favouriteTrainerCourseUserIsActive;
	}

	public boolean isFavouriteTrainerCourseUserIsDeletedByAdmin() {
		return favouriteTrainerCourseUserIsDeletedByAdmin;
	}

	public void setFavouriteTrainerCourseUserIsDeletedByAdmin(boolean favouriteTrainerCourseUserIsDeletedByAdmin) {
		this.favouriteTrainerCourseUserIsDeletedByAdmin = favouriteTrainerCourseUserIsDeletedByAdmin;
	}

	public LocalDateTime getFavouriteTrainerCourseUserLastUpdatedDateTime() {
		return favouriteTrainerCourseUserLastUpdatedDateTime;
	}

	public void setFavouriteTrainerCourseUserLastUpdatedDateTime(
			LocalDateTime favouriteTrainerCourseUserLastUpdatedDateTime) {
		this.favouriteTrainerCourseUserLastUpdatedDateTime = favouriteTrainerCourseUserLastUpdatedDateTime;
	}

	public LocalDateTime getFavouriteTrainerCourseUserCreatedDateTime() {
		return favouriteTrainerCourseUserCreatedDateTime;
	}

	public void setFavouriteTrainerCourseUserCreatedDateTime(LocalDateTime favouriteTrainerCourseUserCreatedDateTime) {
		this.favouriteTrainerCourseUserCreatedDateTime = favouriteTrainerCourseUserCreatedDateTime;
	}

	@Override
	public String toString() {
		return "FavouriteTrainerCourseUserInfo [favouriteTrainerCourseUserId=" + favouriteTrainerCourseUserId
				+ ", userInfo=" + userInfo + ", trainerCourseInfo=" + trainerCourseInfo
				+ ", favouriteTrainerCourseUserIsActive=" + favouriteTrainerCourseUserIsActive
				+ ", favouriteTrainerCourseUserIsDeletedByAdmin=" + favouriteTrainerCourseUserIsDeletedByAdmin
				+ ", favouriteTrainerCourseUserLastUpdatedDateTime=" + favouriteTrainerCourseUserLastUpdatedDateTime
				+ ", favouriteTrainerCourseUserCreatedDateTime=" + favouriteTrainerCourseUserCreatedDateTime + "]";
	}
	
}
