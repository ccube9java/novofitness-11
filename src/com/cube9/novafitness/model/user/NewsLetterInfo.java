package com.cube9.novafitness.model.user;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.CreationTimestamp;

@Entity(name = "news_letter_info")
public class NewsLetterInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "news_letter_id")
	private Long newsLetterId;

	@Column(name = "news_letter_user_fname")
	private String newsLetterUserFName;
	
	@Column(name = "news_letter_user_lname")
	private String newsLetterUserLName;
	
	@Column(name = "news_letter_user_email", unique = true)
	private String newsLetterUserEmail;
	
	@Column(name = "news_letter_user_phone_no")
	private Long newsLetterUserPhoneNo;

	@Column(name = "is_Active", columnDefinition = "boolean default true")
	private boolean newsLetterUserIsActive=true;
	
	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean newsLetterUserIsDeletedByAdmin=false;

	@Column(name = "news_letter_created_dateTime")
	@CreationTimestamp
	private LocalDateTime newsLetterUserCreatedDateTime;

	public Long getNewsLetterId() {
		return newsLetterId;
	}

	public void setNewsLetterId(Long newsLetterId) {
		this.newsLetterId = newsLetterId;
	}

	public String getNewsLetterUserFName() {
		return newsLetterUserFName;
	}

	public void setNewsLetterUserFName(String newsLetterUserFName) {
		this.newsLetterUserFName = newsLetterUserFName;
	}

	public String getNewsLetterUserLName() {
		return newsLetterUserLName;
	}

	public void setNewsLetterUserLName(String newsLetterUserLName) {
		this.newsLetterUserLName = newsLetterUserLName;
	}

	public String getNewsLetterUserEmail() {
		return newsLetterUserEmail;
	}

	public void setNewsLetterUserEmail(String newsLetterUserEmail) {
		this.newsLetterUserEmail = newsLetterUserEmail;
	}

	public Long getNewsLetterUserPhoneNo() {
		return newsLetterUserPhoneNo;
	}

	public void setNewsLetterUserPhoneNo(Long newsLetterUserPhoneNo) {
		this.newsLetterUserPhoneNo = newsLetterUserPhoneNo;
	}

	public LocalDateTime getNewsLetterUserCreatedDateTime() {
		return newsLetterUserCreatedDateTime;
	}

	public void setNewsLetterUserCreatedDateTime(LocalDateTime newsLetterUserCreatedDateTime) {
		this.newsLetterUserCreatedDateTime = newsLetterUserCreatedDateTime;
	}

	public boolean isNewsLetterUserIsActive() {
		return newsLetterUserIsActive;
	}

	public void setNewsLetterUserIsActive(boolean newsLetterUserIsActive) {
		this.newsLetterUserIsActive = newsLetterUserIsActive;
	}

	public boolean isNewsLetterUserIsDeletedByAdmin() {
		return newsLetterUserIsDeletedByAdmin;
	}

	public void setNewsLetterUserIsDeletedByAdmin(boolean newsLetterUserIsDeletedByAdmin) {
		this.newsLetterUserIsDeletedByAdmin = newsLetterUserIsDeletedByAdmin;
	}

	@Override
	public String toString() {
		return "NewsLetterInfo [newsLetterId=" + newsLetterId + ", newsLetterUserFName=" + newsLetterUserFName
				+ ", newsLetterUserLName=" + newsLetterUserLName + ", newsLetterUserEmail=" + newsLetterUserEmail
				+ ", newsLetterUserPhoneNo=" + newsLetterUserPhoneNo + ", newsLetterUserIsActive="
				+ newsLetterUserIsActive + ", newsLetterUserIsDeletedByAdmin=" + newsLetterUserIsDeletedByAdmin
				+ ", newsLetterUserCreatedDateTime=" + newsLetterUserCreatedDateTime + "]";
	}
	
}
