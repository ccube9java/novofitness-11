package com.cube9.novafitness.model.user;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;

@Entity(name="user_profile_image_info")
public class UserProfileImageInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_profile_image_id")
	private Long userProfileImageId;
	
	@Column(name = "user_profile_image_name")
	private String userProfileImageName;
	
	@Column(name = "user_profile_image_stored_path")
	private String userProfileImageStoredPath;
	
	@Column(name = "user_profile_image_view_path")
	private String userProfileImageViewPath;
	
	@Column(name = "user_profile_image_file_type")
	private String userProfileImageFileType;
	
	@Column(name = "is_Active", columnDefinition = "boolean default true")
	private boolean userProfileImageIsActive=true;
	
	@Column(name = "is_deleted_by_admin", columnDefinition = "boolean default false")
	private boolean userProfileImageIsDeletedByAdmin=false;
	
	@Column(name = "user_profile_image_last_updated_dateTime")
	private LocalDateTime userProfileImageLastUpdatedDateTime;
	
	@Column(name = "user_profile_image_created_dateTime")
	@CreationTimestamp
	private LocalDateTime userProfileImageCreatedDateTime;

	public Long getUserProfileImageId() {
		return userProfileImageId;
	}

	public void setUserProfileImageId(Long userProfileImageId) {
		this.userProfileImageId = userProfileImageId;
	}

	public String getUserProfileImageName() {
		return userProfileImageName;
	}

	public void setUserProfileImageName(String userProfileImageName) {
		this.userProfileImageName = userProfileImageName;
	}

	public String getUserProfileImageStoredPath() {
		return userProfileImageStoredPath;
	}

	public void setUserProfileImageStoredPath(String userProfileImageStoredPath) {
		this.userProfileImageStoredPath = userProfileImageStoredPath;
	}

	public String getUserProfileImageViewPath() {
		return userProfileImageViewPath;
	}

	public void setUserProfileImageViewPath(String userProfileImageViewPath) {
		this.userProfileImageViewPath = userProfileImageViewPath;
	}

	public String getUserProfileImageFileType() {
		return userProfileImageFileType;
	}

	public void setUserProfileImageFileType(String userProfileImageFileType) {
		this.userProfileImageFileType = userProfileImageFileType;
	}

	public boolean isUserProfileImageIsActive() {
		return userProfileImageIsActive;
	}

	public void setUserProfileImageIsActive(boolean userProfileImageIsActive) {
		this.userProfileImageIsActive = userProfileImageIsActive;
	}

	public boolean isUserProfileImageIsDeletedByAdmin() {
		return userProfileImageIsDeletedByAdmin;
	}

	public void setUserProfileImageIsDeletedByAdmin(boolean userProfileImageIsDeletedByAdmin) {
		this.userProfileImageIsDeletedByAdmin = userProfileImageIsDeletedByAdmin;
	}

	public LocalDateTime getUserProfileImageLastUpdatedDateTime() {
		return userProfileImageLastUpdatedDateTime;
	}

	public void setUserProfileImageLastUpdatedDateTime(LocalDateTime userProfileImageLastUpdatedDateTime) {
		this.userProfileImageLastUpdatedDateTime = userProfileImageLastUpdatedDateTime;
	}

	public LocalDateTime getUserProfileImageCreatedDateTime() {
		return userProfileImageCreatedDateTime;
	}

	public void setUserProfileImageCreatedDateTime(LocalDateTime userProfileImageCreatedDateTime) {
		this.userProfileImageCreatedDateTime = userProfileImageCreatedDateTime;
	}

	@Override
	public String toString() {
		return "UserProfileImageInfo [userProfileImageId=" + userProfileImageId + ", userProfileImageName="
				+ userProfileImageName + ", userProfileImageStoredPath=" + userProfileImageStoredPath
				+ ", userProfileImageViewPath=" + userProfileImageViewPath + ", userProfileImageFileType="
				+ userProfileImageFileType + ", userProfileImageIsActive=" + userProfileImageIsActive
				+ ", userProfileImageIsDeletedByAdmin=" + userProfileImageIsDeletedByAdmin
				+ ", userProfileImageLastUpdatedDateTime=" + userProfileImageLastUpdatedDateTime
				+ ", userProfileImageCreatedDateTime=" + userProfileImageCreatedDateTime + "]";
	}
	
}
