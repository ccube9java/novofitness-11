package com.cube9.novafitness.controller.trainer;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cube9.novafitness.config.PasswordValidator;
import com.cube9.novafitness.model.admin.CategoryInfo;
import com.cube9.novafitness.model.course.CourseOtherImagesInfo;
import com.cube9.novafitness.model.course.CourseOtherPDFsInfo;
import com.cube9.novafitness.model.course.CourseOtherVideosInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseChapterInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseChapterLessonInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseFAQInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseInfo;
import com.cube9.novafitness.model.trainer.TrainerInfo;
import com.cube9.novafitness.model.user.UserCoursePaymentInfo;
import com.cube9.novafitness.model.user.UserCourseReviewInfo;
import com.cube9.novafitness.model.vimeo.VimeoException;
import com.cube9.novafitness.service.admin.AdminService;
import com.cube9.novafitness.service.trainer.TrainerService;
import com.cube9.novafitness.wrapper.course.TrainerCourseWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerChangePasswordWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerCourseChapterLessonWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerCourseChapterWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerCourseFAQWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerLoginWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerProfileWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerRegisterWrapper;
import com.cube9.novafitness.wrapper.trainer.TrainerResetPasswordWrapper;
import com.sun.jersey.core.util.Base64;

/**
 * @author Vaibhav Deshmane
 */

@Controller
public class TrainerWebController {

	@Autowired
	private TrainerService trainerService;

	@Autowired
	private AdminService adminService;

	@Autowired
	private MessageSource messageSource;

	/*--- getCategoryInfo ---*/
	public void getCategoryInfo(Model model) {
		List<CategoryInfo> categoryInfos = adminService.getMainCategoryList();
		model.addAttribute("categoryList", categoryInfos);
	}

	/*--- On starting Application ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/trainer", produces = "application/json;charset=UTF-8")
	public String trainer(Locale locale, Model model, HttpSession session) {
		return "trainerLogin";
	}

	/*--- On starting Application ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/trainerLogin", produces = "application/json;charset=UTF-8")
	public String trainerLogin(Locale locale, Model model, HttpSession session) {
		return "trainerLogin";
	}

	/*--- On starting Application ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/trainerRegister", produces = "application/json;charset=UTF-8")
	public String trainerRegister(Locale locale, Model model, HttpSession session) {
		return "trainerRegister";
	}

	/*--- On Trainer Reset Password ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/trainerResetPassword", produces = "application/json;charset=UTF-8")
	public String trainerResetPassword(Locale locale, Model model, HttpSession session) {
		return "trainerResetPassword";
	}

	/*--- On Trainer Profile ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/trainerProfile", produces = "application/json;charset=UTF-8")
	public String trainerProfile(Locale locale, Model model, HttpSession session) {
		return "trainerProfile";
	}

	/*--- On Trainer Add Course Management ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/trainerAddCourseManagement", produces = "application/json;charset=UTF-8")
	public String trainerAddCourseManagement(Locale locale, Model model, HttpSession session) {
		getCategoryInfo(model);
		return "trainerAddCourseManagement";
	}

	/*--- On Trainer Reset Password Page ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/trainerResetPassword/{trainerEmail}", produces = "application/json;charset=UTF-8")
	public String trainerResetPassword(@PathVariable(value = "trainerEmail") String trainerEmail, Locale locale,
			Model model, final RedirectAttributes redirectAttributes, HttpSession session) {
		session.setAttribute("trainerEmail", trainerEmail);
		return "redirect:/trainerResetPassword";
	}

	/*--- On Trainer Dashboard ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/trainerDashboard", produces = "application/json;charset=UTF-8")
	public String trainerDashboard(Locale locale, Model model, HttpSession session) {

		TrainerInfo trainerInfo = (TrainerInfo) session.getAttribute("trainerSession");
		if (trainerInfo != null) {
			Long courseCount = trainerService.getAllTrainrCourseCountByTrainerId(trainerInfo.getTrainerId());
			model.addAttribute("courseCount", courseCount);
			List<UserCourseReviewInfo> userCourseReviewInfos = trainerService
					.getTrainerCourseListWithReviewAndRating(trainerInfo.getTrainerId());
			if (userCourseReviewInfos != null) {
				model.addAttribute("trainerCourseReviewCount", userCourseReviewInfos.size());
			} else {
				model.addAttribute("trainerCourseReviewCount", 0);
			}
			List<UserCoursePaymentInfo> userCoursePaymentInfos = trainerService
					.getTrainerCourseUserPaymentListByTrainerId(trainerInfo.getTrainerId());
			if (userCourseReviewInfos != null) {
				model.addAttribute("trainerCoursePaymentCount", userCoursePaymentInfos.size());
			} else {
				model.addAttribute("trainerCoursePaymentCount", 0);
			}
		}

		return "trainerDashboard";
	}

	/*--- On Trainer Course Management ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/trainerCourseManagement", produces = "application/json;charset=UTF-8")
	public String trainerCourseManagement(Locale locale, Model model, HttpSession session) {
		TrainerInfo trainerInfo = (TrainerInfo) session.getAttribute("trainerSession");
		if (trainerInfo != null) {
			List<TrainerCourseInfo> trainerCourseInfos = trainerService
					.getTrainerCourseListByTrainerId(trainerInfo.getTrainerId());
			if (trainerCourseInfos != null) {
				model.addAttribute("trainerCourseList", trainerCourseInfos);
			}
		}
		return "trainerCourseManagement";
	}

	/*--- On Trainer Purchased Course Management ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/trainerPurchasedCourseManagement", produces = "application/json;charset=UTF-8")
	public String trainerPurchasedCourseManagement(Locale locale, Model model, HttpSession session) {

		TrainerInfo trainerInfo = (TrainerInfo) session.getAttribute("trainerSession");
		if (trainerInfo != null) {
			List<UserCoursePaymentInfo> userCoursePaymentInfos = trainerService
					.getTrainerCourseUserPaymentListByTrainerId(trainerInfo.getTrainerId());
			if (userCoursePaymentInfos != null) {
				model.addAttribute("trainerCourseUserPaymentList", userCoursePaymentInfos);
			}
		}

		return "trainerPurchasedCourseManagement";
	}

	/*--- On Trainer Course Review Rating Management ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/trainerCourseReviewRatingManagement", produces = "application/json;charset=UTF-8")
	public String trainerCourseReviewRatingManagement(Locale locale, Model model, HttpSession session) {

		TrainerInfo trainerInfo = (TrainerInfo) session.getAttribute("trainerSession");
		if (trainerInfo != null) {
			List<UserCourseReviewInfo> userCourseReviewInfos = trainerService
					.getTrainerCourseListWithReviewAndRating(trainerInfo.getTrainerId());
			if (userCourseReviewInfos != null) {
				model.addAttribute("trainerCourseListWithReviewAndRating", userCourseReviewInfos);
			}
		}

		return "trainerCourseReviewRatingManagement";
	}

	/*--- On Trainer Course User Payment Settlement Management ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/trainerCourseUserPaymentSettlementManagement", produces = "application/json;charset=UTF-8")
	public String trainerCourseUserPaymentSettlementManagement(Locale locale, Model model, HttpSession session) {

		TrainerInfo trainerInfo = (TrainerInfo) session.getAttribute("trainerSession");
		if (trainerInfo != null) {
			List<UserCoursePaymentInfo> userCoursePaymentInfos = trainerService
					.getTrainerCourseUserPaymentListByTrainerId(trainerInfo.getTrainerId());
			if (userCoursePaymentInfos != null) {
				model.addAttribute("trainerCourseUserPaymentList", userCoursePaymentInfos);
			}
		}
		return "trainerCourseUserPaymentSettlementManagement";
	}

	/*--- Trainer Course Management View ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/trainersCourseView/{trainerCourseId}", produces = "application/json;charset=UTF-8")
	public String trainersCourseView(@PathVariable(value = "trainerCourseId") Long trainerCourseId, Locale locale,
			Model model, final RedirectAttributes redirectAttributes, HttpSession session) {
		byte[] encodedBytes = Base64.encode(trainerCourseId.toString().getBytes());
		String encryptedTrainerCourseId = new String(encodedBytes);

		return "redirect:/dHJhaW5lcnNDb3Vyc2VWaWV3UHJvY2Vzcw?ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUlk="
				+ encryptedTrainerCourseId;
	}

	@RequestMapping("/dHJhaW5lcnNDb3Vyc2VWaWV3UHJvY2Vzcw")
	public String trainersCourseViewProcess(
			@RequestParam(name = "ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUlk") String encryptedTrainerCourseId, Locale locale,
			Model model, final RedirectAttributes redirectAttributes, HttpSession session) {

		getCategoryInfo(model);
		byte[] decodedBytes = Base64.decode(encryptedTrainerCourseId);
		String decodedTrainerCourseId = new String(decodedBytes);
		Long trainerCourseId = Long.parseLong(decodedTrainerCourseId);

		TrainerCourseInfo trainerCourseInfo = trainerService.getTrainerCourseInfoByTrainerCourseId(trainerCourseId);

		if (trainerCourseInfo != null) {
			List<CourseOtherImagesInfo> courseOtherImagesInfos = trainerService
					.getCourseOtherImagesListByCourseId(trainerCourseInfo.getTrainerCourseInfo().getCourseId());
			model.addAttribute("trainerCourseOtherImagesList", courseOtherImagesInfos);

			List<CourseOtherVideosInfo> courseOtherVideosInfos = trainerService
					.getCourseOtherVideosListByCourseId(trainerCourseInfo.getTrainerCourseInfo().getCourseId());
			model.addAttribute("trainerCourseOtherVideosList", courseOtherVideosInfos);

			List<CourseOtherPDFsInfo> courseOtherPDFsInfos = trainerService
					.getCourseOtherPDFsListByCourseId(trainerCourseInfo.getTrainerCourseInfo().getCourseId());
			model.addAttribute("trainerCourseOtherPDFsList", courseOtherPDFsInfos);

			List<TrainerCourseFAQInfo> trainerCourseFAQInfos = trainerService
					.getTrainerCourseFAQListByTrainerCourseId(trainerCourseInfo.getTrainerCourseId());
			model.addAttribute("trainerCourseFAQList", trainerCourseFAQInfos);

			List<TrainerCourseChapterInfo> trainerCourseChapterInfos = trainerService
					.getTrainerCourseChapterListByTrainerCourseId(trainerCourseInfo.getTrainerCourseId());

			model.addAttribute("trainerCourseChapterList", trainerCourseChapterInfos);
		}
		model.addAttribute("trainerCourseDetails", trainerCourseInfo);

		return "trainerCourseViewUpdateManagement";
	}

	/*--- Trainer Course Chapter Management Update ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/trainersCourseChapterUpdate/{trainerCourseChapterId}", produces = "application/json;charset=UTF-8")
	public String trainersCourseChapterUpdate(
			@PathVariable(value = "trainerCourseChapterId") Long trainerCourseChapterId, Locale locale, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) {
		byte[] encodedBytes = Base64.encode(trainerCourseChapterId.toString().getBytes());
		String encryptedTrainerCourseChapterId = new String(encodedBytes);

		return "redirect:/dHJhaW5lcnNDb3Vyc2VDaGFwdGVyVXBkYXRlUHJvY2Vzcw?ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUNoYXB0ZXJJZA="
				+ encryptedTrainerCourseChapterId;
	}

	@RequestMapping("/dHJhaW5lcnNDb3Vyc2VDaGFwdGVyVXBkYXRlUHJvY2Vzcw")
	public String trainersCourseChapterUpdateProcess(
			@RequestParam(name = "ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUNoYXB0ZXJJZA") String encryptedTrainerCourseChapterId,
			Locale locale, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {

		getCategoryInfo(model);
		byte[] decodedBytes = Base64.decode(encryptedTrainerCourseChapterId);
		String decodedTrainerCourseChapterId = new String(decodedBytes);
		Long trainerCourseChapterId = Long.parseLong(decodedTrainerCourseChapterId);

		TrainerCourseChapterInfo trainerCourseChapterInfo = trainerService
				.getTrainerCourseChapterInfoByTrainerCourseChapterId(trainerCourseChapterId);

		model.addAttribute("trainerCourseChapterDetails", trainerCourseChapterInfo);

		return "trainerCourseChapterUpdateManagement";
	}

	/*--- Trainer Course Chapter Management View ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/trainersCourseChapterView/{trainerCourseChapterId}", produces = "application/json;charset=UTF-8")
	public String trainersCourseChapterView(@PathVariable(value = "trainerCourseChapterId") Long trainerCourseChapterId,
			Locale locale, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {
		byte[] encodedBytes = Base64.encode(trainerCourseChapterId.toString().getBytes());
		String encryptedTrainerCourseChapterId = new String(encodedBytes);

		return "redirect:/dHJhaW5lcnNDb3Vyc2VDaGFwdGVyVmlld1Byb2Nlc3M?ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUNoYXB0ZXJJZA="
				+ encryptedTrainerCourseChapterId;
	}

	@RequestMapping("/dHJhaW5lcnNDb3Vyc2VDaGFwdGVyVmlld1Byb2Nlc3M")
	public String trainersCourseChapterViewProcess(
			@RequestParam(name = "ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUNoYXB0ZXJJZA") String encryptedTrainerCourseChapterId,
			Locale locale, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {

		getCategoryInfo(model);
		byte[] decodedBytes = Base64.decode(encryptedTrainerCourseChapterId);
		String decodedTrainerCourseChapterId = new String(decodedBytes);
		Long trainerCourseChapterId = Long.parseLong(decodedTrainerCourseChapterId);

		TrainerCourseChapterInfo trainerCourseChapterInfo = trainerService
				.getTrainerCourseChapterInfoByTrainerCourseChapterId(trainerCourseChapterId);

		if (trainerCourseChapterInfo != null) {

			List<TrainerCourseChapterLessonInfo> trainerCourseChapterLessonInfos = trainerService
					.getTrainerCourseChapterLessonListByTrainerCourseChapterId(
							trainerCourseChapterInfo.getTrainerCourseChapterId());

			model.addAttribute("trainerCourseChapterLessonList", trainerCourseChapterLessonInfos);
		}
		model.addAttribute("trainerCourseChapterDetails", trainerCourseChapterInfo);
		return "trainerCourseChapterViewManagement";
	}

	/*--- Trainer Course Chapter Lesson Update ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/trainersCourseChapterLessonUpdate/{trainerCourseChapterLessonId}", produces = "application/json;charset=UTF-8")
	public String trainersCourseChapterLessonUpdate(
			@PathVariable(value = "trainerCourseChapterLessonId") Long trainerCourseChapterLessonId, Locale locale,
			Model model, final RedirectAttributes redirectAttributes, HttpSession session) {
		byte[] encodedBytes = Base64.encode(trainerCourseChapterLessonId.toString().getBytes());
		String encryptedTrainerCourseChapterLessonId = new String(encodedBytes);

		return "redirect:/dHJhaW5lcnNDb3Vyc2VDaGFwdGVyTGVzc29uVXBkYXRlUHJvY2Vzcw?ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUNoYXB0ZXJMZXNzb25JZA="
				+ encryptedTrainerCourseChapterLessonId;
	}

	@RequestMapping("/dHJhaW5lcnNDb3Vyc2VDaGFwdGVyTGVzc29uVXBkYXRlUHJvY2Vzcw")
	public String trainersCourseChapterLessonUpdateProcess(
			@RequestParam(name = "ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUNoYXB0ZXJMZXNzb25JZA") String encryptedTrainerCourseChapterLessonId,
			Locale locale, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {

		getCategoryInfo(model);
		byte[] decodedBytes = Base64.decode(encryptedTrainerCourseChapterLessonId);
		String decodedTrainerCourseChapterLessonId = new String(decodedBytes);
		Long trainerCourseChapterLessonId = Long.parseLong(decodedTrainerCourseChapterLessonId);

		TrainerCourseChapterLessonInfo trainerCourseChapterLessonInfo = trainerService
				.getTrainerCourseChapterLessonInfoByTrainerCourseChapterLessonId(trainerCourseChapterLessonId);

		if (trainerCourseChapterLessonInfo != null) {
			model.addAttribute("trainerCourseChapterLessonDetails", trainerCourseChapterLessonInfo);
		}
		return "trainerCourseChapterLessonUpdate";
	}

	/* --- Trainer Review Management View --- */
	@RequestMapping(method = RequestMethod.GET, value = "/trainerCourseReviewViewManagement/{reviewId}", produces = "application/json;charset=UTF-8")
	public String adminReviewView(@PathVariable(value = "reviewId") Long reviewId, Locale locale, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) {
		byte[] encodedBytes = Base64.encode(reviewId.toString().getBytes());
		String encryptedReviewId = new String(encodedBytes);

		return "redirect:/dHJhaW5lckNvdXJzZVJldmlld1ZpZXdNYW5hZ2VtZW50?ZW5jcnlwdGVkUmV2aWV3SWQ=" + encryptedReviewId;
	}

	@RequestMapping("/dHJhaW5lckNvdXJzZVJldmlld1ZpZXdNYW5hZ2VtZW50")
	public String adminReviewViewProcess(@RequestParam(name = "ZW5jcnlwdGVkUmV2aWV3SWQ") String encryptedReviewId,
			Locale locale, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {
		byte[] decodedBytes = Base64.decode(encryptedReviewId);
		String decodedReviewId = new String(decodedBytes);
		Long reviewId = Long.parseLong(decodedReviewId);
		UserCourseReviewInfo reviewInfo = adminService.getReviewInfoByReviewId(reviewId);
		model.addAttribute("reviewInfo", reviewInfo);
		return "trainerCourseReviewViewManagement";
	}

	/*--- Trainer Course User Payment View Management --- */
	@RequestMapping(method = RequestMethod.GET, value = "/trainerCourseUserPaymentViewManagement/{userCoursePaymentId}", produces = "application/json;charset=UTF-8")
	public String trainerCourseUserPaymentViewManagement(
			@PathVariable(value = "userCoursePaymentId") Long userCoursePaymentId, Locale locale, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) {
		byte[] encodedBytes = Base64.encode(userCoursePaymentId.toString().getBytes());
		String encryptedUserCoursePaymentId = new String(encodedBytes);

		return "redirect:/dHJhaW5lckNvdXJzZVVzZXJQYXltZW50Vmlld01hbmFnZW1lbnQ?ZW5jcnlwdGVkVXNlckNvdXJzZVBheW1lbnRJZA="
				+ encryptedUserCoursePaymentId;
	}

	@RequestMapping("/dHJhaW5lckNvdXJzZVVzZXJQYXltZW50Vmlld01hbmFnZW1lbnQ")
	public String trainerCourseUserPaymentViewManagementPage(
			@RequestParam(name = "ZW5jcnlwdGVkVXNlckNvdXJzZVBheW1lbnRJZA") String encryptedUserCoursePaymentId,
			Locale locale, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {
		byte[] decodedBytes = Base64.decode(encryptedUserCoursePaymentId);
		String decodedUserCoursePaymentId = new String(decodedBytes);
		Long userCoursePaymentId = Long.parseLong(decodedUserCoursePaymentId);
		UserCoursePaymentInfo userCoursePaymentInfo = adminService
				.getUserCoursePaymentInfoByUserCoursePaymentId(userCoursePaymentId);
		model.addAttribute("userCoursePaymentInfo", userCoursePaymentInfo);
		return "trainerCourseUserPaymentViewManagement";
	}

	/*--- Trainer Course User Payment Settlement View Management ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/trainerCourseUserPaymentSettlementViewManagement/{userCoursePaymentId}", produces = "application/json;charset=UTF-8")
	public String trainerCourseUserPaymentSettlementViewManagement(@PathVariable(value = "userCoursePaymentId") Long userCoursePaymentId,
			Locale locale, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {
		byte[] encodedBytes = Base64.encode(userCoursePaymentId.toString().getBytes());
		String encryptedUserCoursePaymentId = new String(encodedBytes);

		return "redirect:/dHJhaW5lckNvdXJzZVVzZXJQYXltZW50U2V0dGxlbWVudFZpZXdNYW5hZ2VtZW50?ZW5jcnlwdGVkVXNlckNvdXJzZVBheW1lbnRJZA="
				+ encryptedUserCoursePaymentId;
	}

	@RequestMapping("/dHJhaW5lckNvdXJzZVVzZXJQYXltZW50U2V0dGxlbWVudFZpZXdNYW5hZ2VtZW50")
	public String trainerCourseUserPaymentSettlementViewManagementProcess(
			@RequestParam(name = "ZW5jcnlwdGVkVXNlckNvdXJzZVBheW1lbnRJZA") String encryptedUserCoursePaymentId,
			Locale locale, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {

		getCategoryInfo(model);
		byte[] decodedBytes = Base64.decode(encryptedUserCoursePaymentId);
		String decodedUserCoursePaymentId = new String(decodedBytes);
		Long userCoursePaymentId = Long.parseLong(decodedUserCoursePaymentId);

		UserCoursePaymentInfo userCoursePaymentInfo = adminService
				.getUserCoursePaymentInfoByUserCoursePaymentId(userCoursePaymentId);
		model.addAttribute("userCoursePaymentInfo", userCoursePaymentInfo);
		return "trainerCourseUserPaymentSettlementViewManagement";
	}
	
	/*--- Trainer Course FAQ Update ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/trainersCourseFAQUpdate/{trainerCourseFAQId}", produces = "application/json;charset=UTF-8")
	public String trainersCourseFAQUpdate(@PathVariable(value = "trainerCourseFAQId") Long trainerCourseFAQId,
			Locale locale, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {
		byte[] encodedBytes = Base64.encode(trainerCourseFAQId.toString().getBytes());
		String encryptedTrainerCourseFAQId = new String(encodedBytes);

		return "redirect:/dHJhaW5lcnNDb3Vyc2VGQVFVcGRhdGVQcm9jZXNz?ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUZBUUlk="
				+ encryptedTrainerCourseFAQId;
	}

	@RequestMapping("/dHJhaW5lcnNDb3Vyc2VGQVFVcGRhdGVQcm9jZXNz")
	public String trainersCourseFAQUpdateProcess(
			@RequestParam(name = "ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUZBUUlk") String encryptedTrainerCourseFAQId,
			Locale locale, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {

		getCategoryInfo(model);
		byte[] decodedBytes = Base64.decode(encryptedTrainerCourseFAQId);
		String decodedTrainerCourseFAQId = new String(decodedBytes);
		Long trainerCourseFAQId = Long.parseLong(decodedTrainerCourseFAQId);

		TrainerCourseFAQInfo trainCourseFAQInfo = trainerService
				.getTrainerCourseFAQByTrainerCourseFAQId(trainerCourseFAQId);

		if (trainCourseFAQInfo != null) {
			model.addAttribute("trainerCourseFAQDetails", trainCourseFAQInfo);
		}
		return "trainerCourseFAQUpdate";
	}
	
	/*--- On starting initialization of TrainerRegisterWrapper ---*/
	@ModelAttribute("trainerRegisterWrapper")
	public TrainerRegisterWrapper setUpTrainerRegisterWrapper() {
		return new TrainerRegisterWrapper();
	}

	/*--- On starting initialization of TrainerLoginWrapper ---*/
	@ModelAttribute("trainerLoginWrapper")
	public TrainerLoginWrapper setUpTrainerLoginWrapper() {
		return new TrainerLoginWrapper();
	}

	/*--- On starting initialization of TrainerResetPasswordWrapper ---*/
	@ModelAttribute("trainerResetPasswordWrapper")
	public TrainerResetPasswordWrapper setUpTrainerResetPasswordWrapper() {
		return new TrainerResetPasswordWrapper();
	}

	/*--- On starting initialization of TrainerProfileWrapper ---*/
	@ModelAttribute("trainerProfileWrapper")
	public TrainerProfileWrapper setUpTrainerProfileWrapper() {
		return new TrainerProfileWrapper();
	}

	/*--- On starting initialization of TrainerChangePasswordWrapper ---*/
	@ModelAttribute("trainerChangePasswordWrapper")
	public TrainerChangePasswordWrapper setUpTrainerChangePasswordWrapper() {
		return new TrainerChangePasswordWrapper();
	}

	/*--- On starting initialization of TrainerCourseWrapper ---*/
	@ModelAttribute("trainerCourseWrapper")
	public TrainerCourseWrapper setUpTrainerCourseWrapper() {
		return new TrainerCourseWrapper();
	}

	/*--- On starting initialization of TrainerCourseFAQWrapper ---*/
	@ModelAttribute("trainerCourseFAQWrapper")
	public TrainerCourseFAQWrapper setUpTrainerCourseFAQWrapper() {
		return new TrainerCourseFAQWrapper();
	}

	/*--- On starting initialization of TrainerCourseChapterWrapper ---*/
	@ModelAttribute("trainerCourseChapterWrapper")
	public TrainerCourseChapterWrapper setUpTrainerCourseChapterWrapper() {
		return new TrainerCourseChapterWrapper();
	}

	/*--- On starting initialization of TrainerCourseChapterLessonWrapper ---*/
	@ModelAttribute("trainerCourseChapterLessonWrapper")
	public TrainerCourseChapterLessonWrapper setUpTrainerCourseChapterLessonWrapper() {
		return new TrainerCourseChapterLessonWrapper();
	}

	/*--- Register New Trainer ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/registerTrainer")
	public String registerTrainer(
			@ModelAttribute("trainerRegisterWrapper") @Valid TrainerRegisterWrapper trainerRegisterWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes) {

		PasswordValidator validator = new PasswordValidator();
		validator.validate(trainerRegisterWrapper, result);

		boolean duplicateEmail = trainerService.checkTrainerDuplicateEmail(trainerRegisterWrapper.getTrainerEmail());

		if (duplicateEmail) {
			result.rejectValue("trainerEmail", "label.userEmailAlreadyPresent");
			return "trainerRegister";
		} else if (trainerRegisterWrapper.getTrainerIDCopyInfo().isEmpty()) {
			result.rejectValue("trainerIDCopyInfo", "label.requiredField");
			return "trainerRegister";
		} else if (trainerRegisterWrapper.getTrainerCertificateInfo().isEmpty()) {
			result.rejectValue("trainerCertificateInfo", "label.requiredField");
			return "trainerRegister";
		} else if (!trainerRegisterWrapper.isTrainerAcceptTermsCondition()) {
			result.rejectValue("trainerAcceptTermsCondition", "label.validAcceptanceTermsAndConditions");
			return "trainerRegister";
		} else if (result.hasErrors()) {
			return "trainerRegister";
		} else {
			boolean registerTrainerStatus = trainerService.trainerRegister(trainerRegisterWrapper, messageSource,
					redirectAttributes);
			if (registerTrainerStatus) {
				return "redirect:/trainerLogin";
			} else {
				return "redirect:/trainerRegister";
			}
		}
	}

	/*--- Trainer Email Verification Process Page---*/
	@RequestMapping(method = RequestMethod.GET, value = "/trainerEmailVerification/{trainerEmail}", produces = "application/json;charset=UTF-8")
	public String trainerEmailVerification(@PathVariable(value = "trainerEmail") String trainerEmail, Locale locale,
			Model model, final RedirectAttributes redirectAttributes) {
		trainerService.trainerVerification(trainerEmail, redirectAttributes, messageSource);
		return "redirect:/trainerLogin";
	}

	/* --- Login Trainer --- */
	@RequestMapping(method = RequestMethod.POST, value = "/trainerLogin")
	public String trainerLogin(@ModelAttribute("trainerLoginWrapper") @Valid TrainerLoginWrapper trainerLoginWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {
		if (result.hasErrors()) {
			return "trainerLogin";
		} else {
			boolean trainerLogin = trainerService.trainerLogin(trainerLoginWrapper, redirectAttributes, messageSource,
					session);
			if (trainerLogin) {
				return "redirect:/trainerDashboard";
			} else {
				return "redirect:/trainerLogin";
			}
		}
	}

	/*--- Trainer Logout ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/trainerLogout", produces = "application/json;charset=UTF-8")
	public String trainerLogout(HttpSession session) {
		session.invalidate();
		return "redirect:/trainerLogin";
	}

	/*--- Trainer Forget Password ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/trainerForgetPassword")
	public String trainerForgetPassword(@ModelAttribute("trainerLogin") @Valid TrainerLoginWrapper trainerLoginWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			return "trainerLogin";
		} else {
			boolean trainerForgetPassword = trainerService.trainerForgetPassword(trainerLoginWrapper,
					redirectAttributes, messageSource);
			if (trainerForgetPassword) {
				return "redirect:/trainerLogin";
			} else {
				return "redirect:/trainerLogin";
			}
		}
	}

	/*--- Trainer Reset Password ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/resetTrainerPassword")
	public String resetTrainerPassword(
			@ModelAttribute("trainerResetPasswordWrapper") @Valid TrainerResetPasswordWrapper trainerResetPasswordWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {
		PasswordValidator validator = new PasswordValidator();
		validator.validateTrainerResetPassword(trainerResetPasswordWrapper, result);
		trainerResetPasswordWrapper.setTrainerEmail((String) session.getAttribute("trainerEmail"));
		if (result.hasErrors()) {
			return "trainerResetPassword";
		} else {
			boolean resetTrainerPassword = trainerService.resetTrainerPassword(trainerResetPasswordWrapper,
					redirectAttributes, messageSource);
			session.removeAttribute("trainerEmail");
			if (resetTrainerPassword) {
				return "redirect:/trainerLogin";
			} else {
				return "redirect:/trainerResetPassword";
			}
		}
	}

	/*--- Upload Trainer Profile Image ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/uploadTrainerProfileImage")
	public String uploadTrainerProfileImage(@RequestParam("trainerProfileImage") MultipartFile[] trainerProfileImage,
			@RequestParam("trainerEmail") String trainerEmail, Model model, final RedirectAttributes redirectAttributes,
			HttpSession session) {
		boolean uploadTrainerProfileImage = trainerService.uploadTrainerProfileImage(trainerProfileImage, trainerEmail,
				session);
		if (uploadTrainerProfileImage) {
			return "redirect:/trainerProfile";
		} else {
			return "redirect:/trainerProfile";
		}
	}

	/*--- Delete Trainer Profile Image ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/deleteTrainerProfileImage")
	public String deleteTrainerProfileImage(@RequestParam("trainerProfileImageId") Long trainerProfileImageId,
			@RequestParam("trainerId") Long trainerId, Model model, final RedirectAttributes redirectAttributes,
			HttpSession session) throws IOException {
		boolean deleteTrainerProfileImageStatus = trainerService.deleteTrainerProfileImage(trainerProfileImageId,
				trainerId);
		if (deleteTrainerProfileImageStatus) {
			return "success";
		} else {
			return "fail";
		}
	}

	/*--- Update Trainer Profile ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/updateTrainerProfile")
	public String updateTrainerProfile(
			@ModelAttribute("trainerProfileWrapper") @Valid TrainerProfileWrapper trainerProfileWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {
		// System.out.println("des::"+trainerProfileWrapper.getTrainerDescription());
		/*
		 * if (!(trainerProfileWrapper.get).matches("\\d{10}")) {
		 * result.rejectValue("adminPhoneNumber", "label.validPhone"); return
		 * "trainerProfile"; } else
		 */if (result.hasErrors()) {
			return "trainerProfile";
		} else {
			boolean updateTrainerProfile = trainerService.updateTrainerProfile(trainerProfileWrapper,
					redirectAttributes, messageSource, session);
			if (updateTrainerProfile) {
				return "redirect:/trainerProfile";
			} else {
				return "redirect:/trainerProfile";
			}
		}
	}

	/*--- Change Trainer Password ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/changeTrainerPassword")
	public String changeTrainerPassword(
			@ModelAttribute("trainerChangePasswordWrapper") @Valid TrainerChangePasswordWrapper trainerChangePasswordWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {
		TrainerInfo trainerInfo = (TrainerInfo) session.getAttribute("trainerSession");
		// boolean matched =
		// BCrypt.checkpw(trainerChangePasswordWrapper.getTrainerOldPassword(),
		// trainerInfo.getTrainerPassword());
		// if (!matched) {
		// result.rejectValue("trainerOldPassword", "label.oldPasswordFail");
		// return "trainerProfile";
		// } else {
		PasswordValidator validator = new PasswordValidator();
		validator.validateTrainerChangePassword(trainerChangePasswordWrapper, result);
		if (result.hasErrors()) {
			return "trainerProfile";
		} else {
			boolean changeTrainerPasswordStatus = trainerService.changeTrainerPassword(trainerChangePasswordWrapper,
					redirectAttributes, messageSource, session, trainerInfo);
			if (changeTrainerPasswordStatus) {
				return "redirect:/trainerProfile";
			} else {
				return "redirect:/trainerProfile";
			}
		}
		// }
	}

	/*--- Add New Course ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/addNewCourse")
	public String addNewCourse(@ModelAttribute("trainerCourseWrapper") @Valid TrainerCourseWrapper trainerCourseWrapper,
			BindingResult result, Locale locale, Model model, final RedirectAttributes redirectAttributes,
			HttpSession session) throws IOException, VimeoException {

		getCategoryInfo(model);
		if (!trainerCourseWrapper.getCoursePrice().matches("\\d*")) {
			result.rejectValue("coursePrice", "label.coursePrice");
			return "trainerAddCourseManagement";
		} else if (result.hasErrors()) {
			return "trainerAddCourseManagement";
		} else {
			boolean addNewCourse = trainerService.addNewCourse(trainerCourseWrapper, redirectAttributes, messageSource,
					session);
			if (addNewCourse) {
				return "redirect:/trainerCourseManagement";
			} else {
				return "redirect:/trainerAddCourseManagement";
			}
		}
	}

	/*--- Add New Course ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/addNewCourse1")
	public ResponseEntity<Object> addNewCourse1(
			@ModelAttribute("trainerCourseWrapper") @Valid TrainerCourseWrapper trainerCourseWrapper,
			BindingResult result, Locale locale, Model model, final RedirectAttributes redirectAttributes,
			HttpSession session) throws IOException, VimeoException {

		getCategoryInfo(model);

		if (!trainerCourseWrapper.getCoursePrice().matches("\\d*")) {
			result.rejectValue("coursePrice", "label.coursePrice");
			return new ResponseEntity<>("Invalid file.", HttpStatus.BAD_REQUEST);
		} else if (result.hasErrors()) {
			return new ResponseEntity<>("Invalid file.", HttpStatus.BAD_REQUEST);
		} else {
			boolean addNewCourse = trainerService.addNewCourse(trainerCourseWrapper, redirectAttributes, messageSource,
					session);
			if (addNewCourse) {
				return new ResponseEntity<>("File Uploaded Successfully.", HttpStatus.OK);
			} else {
				return new ResponseEntity<>("Invalid file.", HttpStatus.BAD_REQUEST);
			}
		}
	}

	// Handling file upload request
	@PostMapping("/uploadCourseCoverImage")
	public ResponseEntity<Object> uploadCourseCoverImage(
			@RequestParam("courseCoverImage") MultipartFile courseCoverImage, @RequestParam("trainerId") Long trainerId,
			@RequestParam("courseId") Long courseId) throws IOException {

		System.out.println("courseId::" + courseId);
		System.out.println("trainerId::" + trainerId);
		boolean uploadCourseCoverImageStatus = trainerService.uploadCourseCoverImage(courseCoverImage, trainerId,
				courseId);

		// Save file on system
		// if (!file.getOriginalFilename().isEmpty()) {
		// BufferedOutputStream outputStream = new BufferedOutputStream(
		// new FileOutputStream(
		// new
		// File("/home/vinu/Documents/novofitness/spring-mvc-fileupload-example3/src/main/webapp/WEB-INF/views/",
		// file.getOriginalFilename())));
		// outputStream.write(file.getBytes());
		// outputStream.flush();
		// outputStream.close();
		/// } else {
		// return new ResponseEntity<>("Invalid file.",HttpStatus.BAD_REQUEST);
		// }
		if (uploadCourseCoverImageStatus) {
			return new ResponseEntity<>("File Uploaded Successfully.", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Invalid file.", HttpStatus.BAD_REQUEST);
		}

	}

	// Handling file upload request
	@PostMapping("/uploadCourseCoverVideo")
	public ResponseEntity<Object> uploadCourseCoverVideo(
			@RequestParam("courseCoverVideo") MultipartFile courseCoverVideo, @RequestParam("trainerId") Long trainerId,
			@RequestParam("courseId") Long courseId) throws IOException, VimeoException {

		System.out.println("courseId::" + courseId);
		System.out.println("trainerId::" + trainerId);
		boolean uploadCourseCoverVideoStatus = trainerService.uploadCourseCoverVideo(courseCoverVideo, trainerId,
				courseId);

		// Save file on system
		// if (!file.getOriginalFilename().isEmpty()) {
		// BufferedOutputStream outputStream = new BufferedOutputStream(
		// new FileOutputStream(
		// new
		// File("/home/vinu/Documents/novofitness/spring-mvc-fileupload-example3/src/main/webapp/WEB-INF/views/",
		// file.getOriginalFilename())));
		// outputStream.write(file.getBytes());
		// outputStream.flush();
		// outputStream.close();
		/// } else {
		// return new ResponseEntity<>("Invalid file.",HttpStatus.BAD_REQUEST);
		// }
		if (uploadCourseCoverVideoStatus) {
			return new ResponseEntity<>("Video Uploaded Successfully.", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Invalid file.", HttpStatus.BAD_REQUEST);
		}

	}

	// Handling file upload request
	@PostMapping("/uploadCourseOtherImages")
	public ResponseEntity<Object> uploadCourseOtherImages(
			@RequestParam("courseOtherImages") MultipartFile[] courseOtherImages,
			@RequestParam("trainerId") Long trainerId, @RequestParam("courseId") Long courseId) throws IOException {

		System.out.println("courseId::" + courseId);
		System.out.println("trainerId::" + trainerId);
		boolean uploadCourseOtherImagesStatus = trainerService.uploadCourseOtherImages(courseOtherImages, trainerId,
				courseId);

		// Save file on system
		// if (!file.getOriginalFilename().isEmpty()) {
		// BufferedOutputStream outputStream = new BufferedOutputStream(
		// new FileOutputStream(
		// new
		// File("/home/vinu/Documents/novofitness/spring-mvc-fileupload-example3/src/main/webapp/WEB-INF/views/",
		// file.getOriginalFilename())));
		// outputStream.write(file.getBytes());
		// outputStream.flush();
		// outputStream.close();
		/// } else {
		// return new ResponseEntity<>("Invalid file.",HttpStatus.BAD_REQUEST);
		// }
		if (uploadCourseOtherImagesStatus) {
			return new ResponseEntity<>("Files Uploaded Successfully.", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Invalid file.", HttpStatus.BAD_REQUEST);
		}

	}

	/*--- Update Course Info ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/updateCourseInfo")
	public String updateCourseInfo(
			@ModelAttribute("trainerCourseWrapper") @Valid TrainerCourseWrapper trainerCourseWrapper,
			BindingResult result, Locale locale, Model model, final RedirectAttributes redirectAttributes,
			HttpSession session) throws IOException, VimeoException {

		getCategoryInfo(model);

		TrainerCourseInfo trainerCourseInfo = trainerService
				.getTrainerCourseInfoByTrainerCourseId(trainerCourseWrapper.getTrainerCourseId());

		if (trainerCourseInfo != null) {
			List<CourseOtherImagesInfo> courseOtherImagesInfos = trainerService
					.getCourseOtherImagesListByCourseId(trainerCourseInfo.getTrainerCourseInfo().getCourseId());
			model.addAttribute("trainerCourseOtherImagesList", courseOtherImagesInfos);

			List<CourseOtherVideosInfo> courseOtherVideosInfos = trainerService
					.getCourseOtherVideosListByCourseId(trainerCourseInfo.getTrainerCourseInfo().getCourseId());
			model.addAttribute("trainerCourseOtherVideosList", courseOtherVideosInfos);

			List<CourseOtherPDFsInfo> courseOtherPDFsInfos = trainerService
					.getCourseOtherPDFsListByCourseId(trainerCourseInfo.getTrainerCourseInfo().getCourseId());
			model.addAttribute("trainerCourseOtherPDFsList", courseOtherPDFsInfos);

			List<TrainerCourseFAQInfo> trainerCourseFAQInfos = trainerService
					.getTrainerCourseFAQListByTrainerCourseId(trainerCourseInfo.getTrainerCourseId());
			model.addAttribute("trainerCourseFAQList", trainerCourseFAQInfos);

			List<TrainerCourseChapterInfo> trainerCourseChapterInfos = trainerService
					.getTrainerCourseChapterListByTrainerCourseId(trainerCourseInfo.getTrainerCourseId());

			model.addAttribute("trainerCourseChapterList", trainerCourseChapterInfos);
		}
		model.addAttribute("trainerCourseDetails", trainerCourseInfo);

		if (!trainerCourseWrapper.getCoursePrice().matches("\\d*")) {
			result.rejectValue("coursePrice", "label.coursePrice");
			return "trainerCourseViewUpdateManagement";
		} else if (result.hasErrors()) {
			return "trainerCourseViewUpdateManagement";
		} else {
			boolean addNewCourse = trainerService.updateCourseInfo(trainerCourseWrapper, redirectAttributes,
					messageSource, session);
			byte[] encodedBytes = Base64.encode(trainerCourseWrapper.getTrainerCourseId().toString().getBytes());
			String encryptedTrainerCourseId = new String(encodedBytes);

			if (addNewCourse) {
				return "redirect:/dHJhaW5lcnNDb3Vyc2VWaWV3UHJvY2Vzcw?ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUlk="
						+ encryptedTrainerCourseId;
			} else {
				return "redirect:/dHJhaW5lcnNDb3Vyc2VWaWV3UHJvY2Vzcw?ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUlk="
						+ encryptedTrainerCourseId;
			}
		}
	}

	/*--- Delete Course Other Images ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/deleteOtherImage")
	public @ResponseBody String deleteOtherImage(@RequestParam("otherImageId") Long otherImageId, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) throws IOException {
		boolean deleteOtherImageStatus = trainerService.deleteOtherImage(otherImageId, redirectAttributes,
				messageSource, session);
		if (deleteOtherImageStatus) {
			return "success";
		} else {
			return "fail";
		}
	}

	/*--- Delete Course Other Videos ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/deleteOtherVideo")
	public String deleteOtherVideo(@RequestParam("otherVideoId") Long otherVideoId, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) throws IOException {
		boolean deleteOtherVideoStatus = trainerService.deleteOtherVideo(otherVideoId, redirectAttributes,
				messageSource, session);
		if (deleteOtherVideoStatus) {
			return "success";
		} else {
			return "fail";
		}
	}

	/*--- Delete Course Other PDFs ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/deleteOtherPDF")
	public String deleteOtherPDF(@RequestParam("otherPDFId") Long otherPDFId, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) throws IOException {
		boolean deleteOtherPDFStatus = trainerService.deleteOtherPDF(otherPDFId, redirectAttributes, messageSource,
				session);
		if (deleteOtherPDFStatus) {
			return "success";
		} else {
			return "fail";
		}
	}

	/*--- Delete Course Cover Image ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/deleteCoverImage")
	public @ResponseBody String deleteCoverImage(@RequestParam("coverImageId") Long coverImageId,
			@RequestParam("courseId") Long courseId, Model model, final RedirectAttributes redirectAttributes,
			HttpSession session) throws IOException {
		boolean deleteCoverImageStatus = trainerService.deleteCoverImage(coverImageId, courseId);
		if (deleteCoverImageStatus) {
			return "success";
		} else {
			return "fail";
		}
	}

	/*--- Delete Course Cover Video ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/deleteCoverVideo")
	public @ResponseBody String deleteCoverVideo(@RequestParam("coverVideoId") Long coverVideoId,
			@RequestParam("courseId") Long courseId, Model model, final RedirectAttributes redirectAttributes,
			HttpSession session) throws IOException {
		boolean deleteCoverVideoStatus = trainerService.deleteCoverVideo(coverVideoId, courseId);
		if (deleteCoverVideoStatus) {
			return "success";
		} else {
			return "fail";
		}
	}

	/*--- Add New Trainer Course FAQ ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/addNewTrainerCourseFAQ")
	public String addNewTrainerCourseFAQ(
			@ModelAttribute("trainerCourseFAQWrapper") @Valid TrainerCourseFAQWrapper trainerCourseFAQWrapper,
			BindingResult result, Locale locale, Model model, final RedirectAttributes redirectAttributes,
			HttpSession session) throws IOException, VimeoException {

		getCategoryInfo(model);

		TrainerCourseInfo trainerCourseInfo = trainerService
				.getTrainerCourseInfoByTrainerCourseId(trainerCourseFAQWrapper.getTrainerCourseId());

		if (trainerCourseInfo != null) {
			List<CourseOtherImagesInfo> courseOtherImagesInfos = trainerService
					.getCourseOtherImagesListByCourseId(trainerCourseInfo.getTrainerCourseInfo().getCourseId());
			model.addAttribute("trainerCourseOtherImagesList", courseOtherImagesInfos);

			List<CourseOtherVideosInfo> courseOtherVideosInfos = trainerService
					.getCourseOtherVideosListByCourseId(trainerCourseInfo.getTrainerCourseInfo().getCourseId());
			model.addAttribute("trainerCourseOtherVideosList", courseOtherVideosInfos);

			List<CourseOtherPDFsInfo> courseOtherPDFsInfos = trainerService
					.getCourseOtherPDFsListByCourseId(trainerCourseInfo.getTrainerCourseInfo().getCourseId());
			model.addAttribute("trainerCourseOtherPDFsList", courseOtherPDFsInfos);

			List<TrainerCourseFAQInfo> trainerCourseFAQInfos = trainerService
					.getTrainerCourseFAQListByTrainerCourseId(trainerCourseInfo.getTrainerCourseId());
			model.addAttribute("trainerCourseFAQList", trainerCourseFAQInfos);

			List<TrainerCourseChapterInfo> trainerCourseChapterInfos = trainerService
					.getTrainerCourseChapterListByTrainerCourseId(trainerCourseInfo.getTrainerCourseId());

			model.addAttribute("trainerCourseChapterList", trainerCourseChapterInfos);
		}
		model.addAttribute("trainerCourseDetails", trainerCourseInfo);
		if (result.hasErrors()) {

			return "trainerCourseViewUpdateManagement";

		} else {

			boolean addNewTrainerCourseFAQStatus = trainerService.addNewTrainerCourseFAQ(trainerCourseFAQWrapper,
					redirectAttributes, messageSource, session);

			byte[] encodedBytes = Base64.encode(trainerCourseFAQWrapper.getTrainerCourseId().toString().getBytes());
			String encryptedTrainerCourseId = new String(encodedBytes);

			if (addNewTrainerCourseFAQStatus) {
				return "redirect:/dHJhaW5lcnNDb3Vyc2VWaWV3UHJvY2Vzcw?ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUlk="
						+ encryptedTrainerCourseId;
			} else {
				return "redirect:/dHJhaW5lcnNDb3Vyc2VWaWV3UHJvY2Vzcw?ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUlk="
						+ encryptedTrainerCourseId;
			}
		}
	}

	/*--- Update Trainer Course FAQ ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/updateTrainerCourseFAQ")
	public String updateTrainerCourseFAQ(
			@ModelAttribute("trainerCourseFAQWrapper") @Valid TrainerCourseFAQWrapper trainerCourseFAQWrapper,
			BindingResult result, Locale locale, Model model, final RedirectAttributes redirectAttributes,
			HttpSession session) throws IOException, VimeoException {

		TrainerCourseFAQInfo trainCourseFAQInfo = trainerService
				.getTrainerCourseFAQByTrainerCourseFAQId(trainerCourseFAQWrapper.getTrainerCourseFAQId());

		if (trainCourseFAQInfo != null) {
			model.addAttribute("trainerCourseFAQDetails", trainCourseFAQInfo);
		}
		getCategoryInfo(model);

		if (result.hasErrors()) {

			return "trainerCourseFAQUpdate";

		} else {

			boolean updateTrainerCourseFAQStatus = trainerService.updateTrainerCourseFAQ(trainerCourseFAQWrapper,
					redirectAttributes, messageSource, session);

			byte[] encodedBytes = Base64.encode(trainerCourseFAQWrapper.getTrainerCourseFAQId().toString().getBytes());
			String encryptedTrainerCourseFAQId = new String(encodedBytes);

			if (updateTrainerCourseFAQStatus) {
				return "redirect:/dHJhaW5lcnNDb3Vyc2VGQVFVcGRhdGVQcm9jZXNz?ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUZBUUlk="
						+ encryptedTrainerCourseFAQId;
			} else {
				return "redirect:/dHJhaW5lcnNDb3Vyc2VGQVFVcGRhdGVQcm9jZXNz?ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUZBUUlk="
						+ encryptedTrainerCourseFAQId;
			}
		}
	}

	/*--- Delete Course FAQ ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/deleteTrainersCourseFAQ")
	public @ResponseBody String deleteTrainersCourseFAQ(@RequestParam("trainerCourseFAQId") Long trainerCourseFAQId,
			Model model, final RedirectAttributes redirectAttributes, HttpSession session) throws IOException {

		boolean deleteTrainersCourseFAQStatus = trainerService.deleteTrainersCourseFAQ(trainerCourseFAQId);
		if (deleteTrainersCourseFAQStatus) {
			return "success";
		} else {
			return "fail";
		}
	}

	/*--- Add New Trainer Course Chapter ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/addNewTrainerCourseChapter")
	public String addNewTrainerCourseChapter(
			@ModelAttribute("trainerCourseChapterWrapper") @Valid TrainerCourseChapterWrapper trainerCourseChapterWrapper,
			BindingResult result, Locale locale, Model model, final RedirectAttributes redirectAttributes,
			HttpSession session) throws IOException, VimeoException {

		TrainerCourseInfo trainerCourseInfo = trainerService
				.getTrainerCourseInfoByTrainerCourseId(trainerCourseChapterWrapper.getTrainerCourseId());

		if (trainerCourseInfo != null) {
			List<CourseOtherImagesInfo> courseOtherImagesInfos = trainerService
					.getCourseOtherImagesListByCourseId(trainerCourseInfo.getTrainerCourseInfo().getCourseId());
			model.addAttribute("trainerCourseOtherImagesList", courseOtherImagesInfos);

			List<CourseOtherVideosInfo> courseOtherVideosInfos = trainerService
					.getCourseOtherVideosListByCourseId(trainerCourseInfo.getTrainerCourseInfo().getCourseId());
			model.addAttribute("trainerCourseOtherVideosList", courseOtherVideosInfos);

			List<CourseOtherPDFsInfo> courseOtherPDFsInfos = trainerService
					.getCourseOtherPDFsListByCourseId(trainerCourseInfo.getTrainerCourseInfo().getCourseId());
			model.addAttribute("trainerCourseOtherPDFsList", courseOtherPDFsInfos);

			List<TrainerCourseFAQInfo> trainerCourseFAQInfos = trainerService
					.getTrainerCourseFAQListByTrainerCourseId(trainerCourseInfo.getTrainerCourseId());
			model.addAttribute("trainerCourseFAQList", trainerCourseFAQInfos);

			List<TrainerCourseChapterInfo> trainerCourseChapterInfos = trainerService
					.getTrainerCourseChapterListByTrainerCourseId(trainerCourseInfo.getTrainerCourseId());

			model.addAttribute("trainerCourseChapterList", trainerCourseChapterInfos);
		}
		model.addAttribute("trainerCourseDetails", trainerCourseInfo);

		getCategoryInfo(model);

		if (result.hasErrors()) {

			return "trainerCourseViewUpdateManagement";

		} else {

			boolean addNewTrainerCourseChapterStatus = trainerService.addNewTrainerCourseChapter(
					trainerCourseChapterWrapper, redirectAttributes, messageSource, session);

			byte[] encodedBytes = Base64.encode(trainerCourseChapterWrapper.getTrainerCourseId().toString().getBytes());
			String encryptedTrainerCourseId = new String(encodedBytes);

			if (addNewTrainerCourseChapterStatus) {
				return "redirect:/dHJhaW5lcnNDb3Vyc2VWaWV3UHJvY2Vzcw?ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUlk="
						+ encryptedTrainerCourseId;
			} else {
				return "redirect:/dHJhaW5lcnNDb3Vyc2VWaWV3UHJvY2Vzcw?ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUlk="
						+ encryptedTrainerCourseId;
			}
		}
	}

	/*--- Update Trainer Course Chapter ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/updateTrainerCourseChapter")
	public String updateTrainerCourseChapter(
			@ModelAttribute("trainerCourseChapterWrapper") @Valid TrainerCourseChapterWrapper trainerCourseChapterWrapper,
			BindingResult result, Locale locale, Model model, final RedirectAttributes redirectAttributes,
			HttpSession session) throws IOException, VimeoException {

		TrainerCourseChapterInfo trainerCourseChapterInfo = trainerService
				.getTrainerCourseChapterInfoByTrainerCourseChapterId(trainerCourseChapterWrapper.getTrainerCourseChapterId());

		model.addAttribute("trainerCourseChapterDetails", trainerCourseChapterInfo);
		
		getCategoryInfo(model);

		if (result.hasErrors()) {

			return "trainerCourseChapterUpdateManagement";

		} else {

			boolean updateTrainerCourseChapterLessonStatus = trainerService.updateTrainerCourseChapter(
					trainerCourseChapterWrapper, redirectAttributes, messageSource, session);

			byte[] encodedBytes = Base64
					.encode(trainerCourseChapterWrapper.getTrainerCourseChapterId().toString().getBytes());
			String encryptedTrainerCourseChapterId = new String(encodedBytes);

			if (updateTrainerCourseChapterLessonStatus) {
				return "redirect:/dHJhaW5lcnNDb3Vyc2VDaGFwdGVyVXBkYXRlUHJvY2Vzcw?ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUNoYXB0ZXJJZA="
						+ encryptedTrainerCourseChapterId;
			} else {
				return "redirect:/dHJhaW5lcnNDb3Vyc2VDaGFwdGVyVXBkYXRlUHJvY2Vzcw?ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUNoYXB0ZXJJZA="
						+ encryptedTrainerCourseChapterId;
			}
		}
	}

	/*--- Delete Course Chapter ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/deleteTrainersCourseChapter")
	public @ResponseBody String deleteTrainersCourseChapter(
			@RequestParam("trainerCourseChapterId") Long trainerCourseChapterId, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) throws IOException {

		boolean deleteTrainersCourseChapterStatus = trainerService.deleteTrainersCourseChapter(trainerCourseChapterId);
		if (deleteTrainersCourseChapterStatus) {
			return "success";
		} else {
			return "fail";
		}
	}

	/*--- Add New Trainer Course Chapter Lesson ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/addNewTrainerCourseChapterLesson")
	public String addNewTrainerCourseChapterLesson(
			@ModelAttribute("trainerCourseChapterLessonWrapper") @Valid TrainerCourseChapterLessonWrapper trainerCourseChapterLessonWrapper,
			BindingResult result, Locale locale, Model model, final RedirectAttributes redirectAttributes,
			HttpSession session) throws IOException, VimeoException {

		TrainerCourseChapterInfo trainerCourseChapterInfo = trainerService
				.getTrainerCourseChapterInfoByTrainerCourseChapterId(trainerCourseChapterLessonWrapper.getTrainerCourseChapterId());

		if (trainerCourseChapterInfo != null) {

			List<TrainerCourseChapterLessonInfo> trainerCourseChapterLessonInfos = trainerService
					.getTrainerCourseChapterLessonListByTrainerCourseChapterId(
							trainerCourseChapterInfo.getTrainerCourseChapterId());

			model.addAttribute("trainerCourseChapterLessonList", trainerCourseChapterLessonInfos);
		}
		model.addAttribute("trainerCourseChapterDetails", trainerCourseChapterInfo);
		
		getCategoryInfo(model);

		if (result.hasErrors()) {

			return "trainerCourseChapterViewManagement";

		} else {

			boolean addNewTrainerCourseChapterLessonStatus = trainerService.addNewTrainerCourseChapterLesson(
					trainerCourseChapterLessonWrapper, redirectAttributes, messageSource, session);

			byte[] encodedBytes = Base64
					.encode(trainerCourseChapterLessonWrapper.getTrainerCourseChapterId().toString().getBytes());
			String encryptedTrainerCourseChapterId = new String(encodedBytes);

			if (addNewTrainerCourseChapterLessonStatus) {
				return "redirect:/dHJhaW5lcnNDb3Vyc2VDaGFwdGVyVmlld1Byb2Nlc3M?ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUNoYXB0ZXJJZA="
						+ encryptedTrainerCourseChapterId;
			} else {
				return "redirect:/dHJhaW5lcnNDb3Vyc2VDaGFwdGVyVmlld1Byb2Nlc3M?ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUNoYXB0ZXJJZA="
						+ encryptedTrainerCourseChapterId;
			}
		}
	}

	/*--- Update Trainer Course Chapter Lesson ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/updateTrainerCourseChapterLesson")
	public String updateTrainerCourseChapterLesson(
			@ModelAttribute("trainerCourseChapterLessonWrapper") @Valid TrainerCourseChapterLessonWrapper trainerCourseChapterLessonWrapper,
			BindingResult result, Locale locale, Model model, final RedirectAttributes redirectAttributes,
			HttpSession session) throws IOException, VimeoException {

		TrainerCourseChapterLessonInfo trainerCourseChapterLessonInfo = trainerService
				.getTrainerCourseChapterLessonInfoByTrainerCourseChapterLessonId(trainerCourseChapterLessonWrapper.getTrainerCourseChapterLessonId());

		if (trainerCourseChapterLessonInfo != null) {
			model.addAttribute("trainerCourseChapterLessonDetails", trainerCourseChapterLessonInfo);
		}
		
		getCategoryInfo(model);

		if (result.hasErrors()) {

			return "trainerCourseChapterLessonUpdate";

		} else {

			boolean updateTrainerCourseChapterLessonStatus = trainerService.updateTrainerCourseChapterLesson(
					trainerCourseChapterLessonWrapper, redirectAttributes, messageSource, session);

			byte[] encodedBytes = Base64
					.encode(trainerCourseChapterLessonWrapper.getTrainerCourseChapterLessonId().toString().getBytes());
			String encryptedTrainerCourseChapterLessonId = new String(encodedBytes);

			if (updateTrainerCourseChapterLessonStatus) {
				return "redirect:/dHJhaW5lcnNDb3Vyc2VDaGFwdGVyTGVzc29uVXBkYXRlUHJvY2Vzcw?ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUNoYXB0ZXJMZXNzb25JZA="
						+ encryptedTrainerCourseChapterLessonId;
			} else {
				return "redirect:/dHJhaW5lcnNDb3Vyc2VDaGFwdGVyTGVzc29uVXBkYXRlUHJvY2Vzcw?ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUNoYXB0ZXJMZXNzb25JZA="
						+ encryptedTrainerCourseChapterLessonId;
			}
		}
	}

	/*--- Delete Course Course Chapter Lesson ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/deleteTrainersCourseChapterLesson")
	public @ResponseBody String deleteTrainersCourseChapterLesson(
			@RequestParam("trainerCourseChapterLessonId") Long trainerCourseChapterLessonId, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) throws IOException {

		boolean trainerCourseChapterLessonStatus = trainerService
				.deleteTrainersCourseChapterLesson(trainerCourseChapterLessonId);
		if (trainerCourseChapterLessonStatus) {
			return "success";
		} else {
			return "fail";
		}
	}

	// Handling file upload request
	@PostMapping("/uploadCourseChapterLessonPreviewVideo")
	public ResponseEntity<Object> uploadCourseChapterLessonPreviewVideo(
			@RequestParam("trainerCourseChapterLessonPreviewVideo") MultipartFile trainerCourseChapterLessonPreviewVideo,
			@RequestParam("trainerCourseChapterLessonId") Long trainerCourseChapterLessonId)
			throws IOException, VimeoException {

		System.out.println("trainerCourseChapterLessonId::" + trainerCourseChapterLessonId);
		boolean uploadCourseChapterLessonPreviewVideoStatus = trainerService.uploadCourseChapterLessonPreviewVideo(
				trainerCourseChapterLessonPreviewVideo, trainerCourseChapterLessonId);

		if (uploadCourseChapterLessonPreviewVideoStatus) {
			return new ResponseEntity<>("Video Uploaded Successfully.", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Invalid file.", HttpStatus.BAD_REQUEST);
		}

	}

	// Handling file upload request
	@PostMapping("/uploadCourseChapterLessonMainVideo")
	public ResponseEntity<Object> uploadCourseChapterLessonMainVideo(
			@RequestParam("trainerCourseChapterLessonMainVideo") MultipartFile trainerCourseChapterLessonMainVideo,
			@RequestParam("trainerCourseChapterLessonId") Long trainerCourseChapterLessonId)
			throws IOException, VimeoException {

		System.out.println("trainerCourseChapterLessonId::" + trainerCourseChapterLessonId);
		boolean uploadCourseChapterLessonMainVideoStatus = trainerService
				.uploadCourseChapterLessonMainVideo(trainerCourseChapterLessonMainVideo, trainerCourseChapterLessonId);

		if (uploadCourseChapterLessonMainVideoStatus) {
			return new ResponseEntity<>("Video Uploaded Successfully.", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Invalid file.", HttpStatus.BAD_REQUEST);
		}

	}

	// Handling file upload request
	@PostMapping("/uploadCourseChapterLessonMainPdf")
	public ResponseEntity<Object> uploadCourseChapterLessonMainPdf(
			@RequestParam("trainerCourseChapterLessonMainPdf") MultipartFile trainerCourseChapterLessonMainPdf,
			@RequestParam("trainerCourseChapterLessonId") Long trainerCourseChapterLessonId)
			throws IOException, VimeoException {

		System.out.println("trainerCourseChapterLessonId::" + trainerCourseChapterLessonId);
		boolean uploadCourseChapterLessonMainPdfStatus = trainerService
				.uploadCourseChapterLessonMainPdf(trainerCourseChapterLessonMainPdf, trainerCourseChapterLessonId);

		if (uploadCourseChapterLessonMainPdfStatus) {
			return new ResponseEntity<>("Video Uploaded Successfully.", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Invalid file.", HttpStatus.BAD_REQUEST);
		}
	}

	/*--- Delete Preview Video ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/deletePreviewVideo")
	public @ResponseBody String deletePreviewVideo(
			@RequestParam("trainerCourseChapterLessonId") Long trainerCourseChapterLessonId, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) throws IOException {
		boolean deletePreviewVideoStatus = trainerService.deletePreviewVideo(trainerCourseChapterLessonId);
		if (deletePreviewVideoStatus) {
			return "success";
		} else {
			return "fail";
		}
	}

	/*--- Delete Actual Video ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/deleteActualVideo")
	public @ResponseBody String deleteActualVideo(
			@RequestParam("trainerCourseChapterLessonId") Long trainerCourseChapterLessonId, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) throws IOException {
		boolean deleteActualVideoStatus = trainerService.deleteActualVideo(trainerCourseChapterLessonId);
		if (deleteActualVideoStatus) {
			return "success";
		} else {
			return "fail";
		}
	}

	/*--- Delete Main Pdf ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/deleteMainPdf")
	public @ResponseBody String deleteMainPdf(
			@RequestParam("trainerCourseChapterLessonId") Long trainerCourseChapterLessonId, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) throws IOException {
		boolean deleteMainPdfStatus = trainerService.deleteMainPdf(trainerCourseChapterLessonId);
		if (deleteMainPdfStatus) {
			return "success";
		} else {
			return "fail";
		}
	}
}