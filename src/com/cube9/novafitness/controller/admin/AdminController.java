package com.cube9.novafitness.controller.admin;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cube9.novafitness.config.PasswordValidator;
import com.cube9.novafitness.model.admin.AdminInfo;
import com.cube9.novafitness.model.admin.CategoryInfo;
import com.cube9.novafitness.model.admin.FAQInfo;
import com.cube9.novafitness.model.admin.SubAdminAccessRoleInfo;
import com.cube9.novafitness.model.admin.SubCategoryInfo;
import com.cube9.novafitness.model.course.CourseContributionInfo;
import com.cube9.novafitness.model.course.CourseOtherImagesInfo;
import com.cube9.novafitness.model.course.CourseOtherPDFsInfo;
import com.cube9.novafitness.model.course.CourseOtherVideosInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseInfo;
import com.cube9.novafitness.model.trainer.TrainerInfo;
import com.cube9.novafitness.model.user.NewsLetterInfo;
import com.cube9.novafitness.model.user.UserCoursePaymentInfo;
import com.cube9.novafitness.model.user.UserCourseReviewInfo;
import com.cube9.novafitness.model.user.UserInfo;
import com.cube9.novafitness.service.admin.AdminService;
import com.cube9.novafitness.service.trainer.TrainerService;
import com.cube9.novafitness.wrapper.admin.AdminCategoryWrapper;
import com.cube9.novafitness.wrapper.admin.AdminChangePasswordWrapper;
import com.cube9.novafitness.wrapper.admin.AdminFAQsWrapper;
import com.cube9.novafitness.wrapper.admin.AdminLoginWrapper;
import com.cube9.novafitness.wrapper.admin.AdminPaymentSettlementWrapper;
import com.cube9.novafitness.wrapper.admin.AdminProfileWrapper;
import com.cube9.novafitness.wrapper.admin.AdminResetPasswordWrapper;
import com.cube9.novafitness.wrapper.admin.AdminSubCategoryWrapper;
import com.cube9.novafitness.wrapper.admin.SubAdminWrapper;
import com.sun.jersey.core.util.Base64;

/**
 * @author Vaibhav Deshmane
 */

@Controller
public class AdminController {

	@Autowired
	private AdminService adminService;

	@Autowired
	private TrainerService trainerService;

	// @Autowired
	// private UserService userService;

	@Autowired
	private MessageSource messageSource;

	/*--- getCategoryInfo ---*/
	public void getCategoryInfo(Locale locale, Model model) {
		List<CategoryInfo> categoryInfos = adminService.getMainCategoryList();
		model.addAttribute("categoryList", categoryInfos);
	}

	/*--- On starting Application ---*/
	/*
	 * @RequestMapping(method = RequestMethod.GET, value = "/", produces =
	 * "application/json;charset=UTF-8") public String adminLogin(Locale locale,
	 * Model model, HttpSession session) { return "adminLogin"; }
	 */

	/*--- On starting Application ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/admin", produces = "application/json;charset=UTF-8")
	public String admin(Locale locale, Model model, HttpSession session) {
		return "adminLogin";
	}

	/*--- On starting Application ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/adminLogin", produces = "application/json;charset=UTF-8")
	public String adminL(Locale locale, Model model, HttpSession session) {
		return "adminLogin";
	}

	/*--- On Admin Reset Password ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/adminsResetPassword", produces = "application/json;charset=UTF-8")
	public String adminsResetPassword(Locale locale, Model model, HttpSession session) {
		return "adminResetPassword";
	}

	/*--- On Admin Dashboard ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/adminDashboard", produces = "application/json;charset=UTF-8")
	public String adminDashboard(Locale locale, Model model, HttpSession session) {
		
		Long userCount = adminService.getAllUserCount();
		model.addAttribute("userCount", userCount);
		Long trainerCount = adminService.getAllTrainerCount();
		model.addAttribute("trainerCount", trainerCount);
		Long trainerCourseCount = adminService.getAllTrainerCourseCount();
		model.addAttribute("trainerCourseCount", trainerCourseCount);
		return "adminDashboard";
	}

	/*--- On Admin Profile ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/adminProfile", produces = "application/json;charset=UTF-8")
	public String adminsProfile(Locale locale, Model model, HttpSession session) {
		return "adminProfile";
	}

	/*--- On Admin Reset Password Page ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/adminResetPassword/{email}", produces = "application/json;charset=UTF-8")
	public String adminResetPassword(@PathVariable(value = "email") String adminEmail, Locale locale, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) {
		session.setAttribute("adminEmail", adminEmail);
		return "redirect:/adminsResetPassword";
	}

	/* --- On Sub-Admin Management --- */
	@RequestMapping(method = RequestMethod.GET, value = "/adminSubAdminManagement", produces = "application/json;charset=UTF-8")
	public String adminSubAdminManagement(Locale locale, Model model, HttpSession session) {
		List<SubAdminAccessRoleInfo> subAdminAccessRoleInfos = adminService.getSubAdminAccessRoleInfo();
		model.addAttribute("subAdminAccessRoleList", subAdminAccessRoleInfos);
		return "adminSubAdminManagement";
	}

	/*--- On Trainer Management ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/adminTrainerManagement", produces = "application/json;charset=UTF-8")
	public String adminTrainerManagement(Locale locale, Model model, HttpSession session) {
		List<TrainerInfo> trainerInfos = adminService.getTrainerList();
		model.addAttribute("trainerList", trainerInfos);
		return "adminTrainerManagement";
	}

	/*--- On Admin List of User/Learner Management --- */
	@RequestMapping(method = RequestMethod.GET, value = "/adminUserListManagement", produces = "application/json;charset=UTF-8")
	public String adminListOfUser(Locale locale, Model model, HttpSession session) {
		List<UserInfo> userListInfos = adminService.getListOfUser();
		model.addAttribute("adminUserList", userListInfos);
		return "adminUserListManagement";
	}

	/*--- On Admin List of Trainer Management --- */
	@RequestMapping(method = RequestMethod.GET, value = "/adminTrainerListManagent", produces = "application/json;charset=UTF-8")
	public String adminListOfTrainerTrainerManagement(Locale locale, Model model, HttpSession session) {
		List<TrainerInfo> trainerListInfos = adminService.getListOfTrainer();
		model.addAttribute("adminTrainerList", trainerListInfos);
		return "adminTrainerListManagent";
	}

	/*--- On Course Management --- */
	@RequestMapping(method = RequestMethod.GET, value = "/adminCourseManagement", produces = "application/json;charset=UTF-8")
	public String adminCourseManagement(Locale locale, Model model, HttpSession session) {
		List<TrainerCourseInfo> courseInfos = adminService.getCourseList();
		model.addAttribute("courseList", courseInfos);
		return "adminCourseManagement";
	}

	/*--- On admin Review Management --- */
	@RequestMapping(method = RequestMethod.GET, value = "/adminReviewManagement", produces = "application/json;charset=UTF-8")
	public String adminReviewManagement(Locale locale, Model model, HttpSession session) {
		List<UserCourseReviewInfo> courseReview = adminService.getReviewList();
		model.addAttribute("reviewList", courseReview);
		return "adminReviewManagement";
	}

	/*--- Admin Users Management View ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/adminTrainersView/{trainerId}", produces = "application/json;charset=UTF-8")
	public String adminTrainersView(@PathVariable(value = "trainerId") Long trainerId, Locale locale, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) {
		byte[] encodedBytes = Base64.encode(trainerId.toString().getBytes());
		String encryptedTrainerId = new String(encodedBytes);

		return "redirect:/YWRtaW5UcmFpbmVyVmlld1Byb2Nlc3M?ZW5jcnlwdGVkVHJhaW5lcklk=" + encryptedTrainerId;
	}

	@RequestMapping("/YWRtaW5UcmFpbmVyVmlld1Byb2Nlc3M")
	public String adminTrainerViewProcess(@RequestParam(name = "ZW5jcnlwdGVkVHJhaW5lcklk") String encryptedTrainerId,
			Locale locale, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {
		byte[] decodedBytes = Base64.decode(encryptedTrainerId);
		String decodedTrainerId = new String(decodedBytes);
		Long trainerId = Long.parseLong(decodedTrainerId);
		TrainerInfo trainerInfo = adminService.getTrainierInfoByTrainerId(trainerId);
		if (trainerInfo != null) {
			List<TrainerCourseInfo> trainerCourseInfos = trainerService
					.getTrainerCourseListByTrainerId(trainerInfo.getTrainerId());
			if (trainerCourseInfos != null) {
				model.addAttribute("trainerCourseList", trainerCourseInfos);
			}
			model.addAttribute("trainerInfo", trainerInfo);
		}
		
		return "adminTrainerViewManagement";
	}

	/*--- Admin Course Management View --- */
	@RequestMapping(method = RequestMethod.GET, value = "/adminCourseView/{trainerCourseId}", produces = "application/json;charset=UTF-8")
	public String adminCourseView(@PathVariable(value = "trainerCourseId") Long trainerCourseId, Locale locale,
			Model model, final RedirectAttributes redirectAttributes, HttpSession session) {
		byte[] encodedBytes = Base64.encode(trainerCourseId.toString().getBytes());
		String encryptedTrainerCourseId = new String(encodedBytes);

		return "redirect:/YWRtaW5Db3Vyc2VWaWV3UHJvY2Vzcw?ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUlk=" + encryptedTrainerCourseId;
	}

	@RequestMapping("/YWRtaW5Db3Vyc2VWaWV3UHJvY2Vzcw")
	public String adminCourseViewProcess(
			@RequestParam(name = "ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUlk") String encryptedTrainerCourseId, Locale locale,
			Model model, final RedirectAttributes redirectAttributes, HttpSession session) {
		byte[] decodedBytes = Base64.decode(encryptedTrainerCourseId);
		String decodedTrainerCourseId = new String(decodedBytes);
		Long trainerCourseId = Long.parseLong(decodedTrainerCourseId);
		TrainerCourseInfo trainerCourseInfo = adminService.getTrainierCourseInfoByTrainerCourseId(trainerCourseId);
		if (trainerCourseInfo != null) {
			model.addAttribute("trainerCourseInfo", trainerCourseInfo);
			List<CourseOtherImagesInfo> courseOtherImagesInfos = trainerService
					.getCourseOtherImagesListByCourseId(trainerCourseInfo.getTrainerCourseInfo().getCourseId());
			model.addAttribute("courseOtherImagesList", courseOtherImagesInfos);
			List<CourseOtherVideosInfo> courseOtherVideosInfos = trainerService
					.getCourseOtherVideosListByCourseId(trainerCourseInfo.getTrainerCourseInfo().getCourseId());
			model.addAttribute("courseOtherVideosList", courseOtherVideosInfos);
			List<CourseOtherPDFsInfo> courseOtherPDFsInfos = trainerService
					.getCourseOtherPDFsListByCourseId(trainerCourseInfo.getTrainerCourseInfo().getCourseId());
			model.addAttribute("courseOtherPDFsList", courseOtherPDFsInfos);
			List<UserCoursePaymentInfo> userCoursePaymentInfos = adminService
					.getUserCoursePaymentListByTrainerCourseId(trainerCourseInfo.getTrainerCourseId());
			model.addAttribute("coursePurchasedUserList", userCoursePaymentInfos);
		}

		return "adminCourseViewManagement";
	}

	/*--- On User Management ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/adminUserManagement", produces = "application/json;charset=UTF-8")
	public String adminUserManagement(Locale locale, Model model, HttpSession session) {
		List<UserInfo> userInfos = adminService.getUserList();
		model.addAttribute("userList", userInfos);
		return "adminUserManagement";
	}

	/*--- On News Letter Management --- */
	@RequestMapping(method = RequestMethod.GET, value = "/adminNewsLetterManagement", produces = "application/json;charset=UTF-8")
	public String adminNewsLetterManagement(Locale locale, Model model, HttpSession session) {
		List<NewsLetterInfo> newsLetterInfos = adminService.getNewsLetterList();
		model.addAttribute("newsLetterList", newsLetterInfos);
		return "adminNewsLetterManagement";
	}

	/*--- On Admin Trainer Course User Payment Management --- */
	@RequestMapping(method = RequestMethod.GET, value = "/adminTrainerCourseUserPaymentManagement", produces = "application/json;charset=UTF-8")
	public String adminTrainerCourseUserPaymentManagement(Locale locale, Model model, HttpSession session) {
		List<UserCoursePaymentInfo> userCoursePaymentInfos = adminService.getUserCoursePaymentList();
		model.addAttribute("trainerCourseUserPaymentList", userCoursePaymentInfos);
		return "adminTrainerCourseUserPaymentManagement";
	}

	/*--- Admin Users Management View ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/adminUsersView/{userId}", produces = "application/json;charset=UTF-8")
	public String adminUsersView(@PathVariable(value = "userId") Long userId, Locale locale, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) {
		byte[] encodedBytes = Base64.encode(userId.toString().getBytes());
		String encryptedUserId = new String(encodedBytes);

		return "redirect:/YWRtaW5Vc2VyVmlld1Byb2Nlc3M?ZW5jcnlwdGVkVXNlcklk=" + encryptedUserId;
	}

	@RequestMapping("/YWRtaW5Vc2VyVmlld1Byb2Nlc3M")
	public String adminUserViewProcess(@RequestParam(name = "ZW5jcnlwdGVkVXNlcklk") String encryptedUserId,
			Locale locale, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {
		byte[] decodedBytes = Base64.decode(encryptedUserId);
		String decodedUserId = new String(decodedBytes);
		Long userId = Long.parseLong(decodedUserId);
		UserInfo userInfo = adminService.getUserInfoByUserId(userId);
		model.addAttribute("userInfo", userInfo);
		return "adminUserViewManagement";
	}

	/*--- Admin Sub-Admin Management View ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/adminSubAdminView/{subAdminAccessRoleId}", produces = "application/json;charset=UTF-8")
	public String adminSubAdminView(@PathVariable(value = "subAdminAccessRoleId") Long subAdminAccessRoleId,
			Locale locale, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {
		byte[] encodedBytes = Base64.encode(subAdminAccessRoleId.toString().getBytes());
		String encryptedSubAdminId = new String(encodedBytes);

		return "redirect:/YWRtaW5TdWJBZG1pblZpZXdQcm9jZXNz?ZW5jcnlwdGVkU3ViQWRtaW5JZA=" + encryptedSubAdminId;
	}

	@RequestMapping("/YWRtaW5TdWJBZG1pblZpZXdQcm9jZXNz")
	public String adminSubAdminViewProcess(
			@RequestParam(name = "ZW5jcnlwdGVkU3ViQWRtaW5JZA") String encryptedSubAdminId, Locale locale, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) {
		byte[] decodedBytes = Base64.decode(encryptedSubAdminId);
		String decodedSubAdminId = new String(decodedBytes);
		Long subAdminId = Long.parseLong(decodedSubAdminId);
		SubAdminAccessRoleInfo subAdminInfo = adminService.getSubAdminInfoBySubAdminId(subAdminId);
		model.addAttribute("subAdminInfo", subAdminInfo);
		return "adminSubAdminViewManagement";
	}

	/* --- Admin Review Management View --- */
	@RequestMapping(method = RequestMethod.GET, value = "/adminReviewView/{reviewId}", produces = "application/json;charset=UTF-8")
	public String adminReviewView(@PathVariable(value = "reviewId") Long reviewId, Locale locale, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) {
		byte[] encodedBytes = Base64.encode(reviewId.toString().getBytes());
		String encryptedReviewId = new String(encodedBytes);

		return "redirect:/YWRtaW5SZXZpZXdWaWV3UHJvY2Vzcw?ZW5jcnlwdGVkUmV2aWV3SWQ=" + encryptedReviewId;
	}

	@RequestMapping("/YWRtaW5SZXZpZXdWaWV3UHJvY2Vzcw")
	public String adminReviewViewProcess(@RequestParam(name = "ZW5jcnlwdGVkUmV2aWV3SWQ") String encryptedReviewId,
			Locale locale, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {
		byte[] decodedBytes = Base64.decode(encryptedReviewId);
		String decodedReviewId = new String(decodedBytes);
		Long reviewId = Long.parseLong(decodedReviewId);
		UserCourseReviewInfo reviewInfo = adminService.getReviewInfoByReviewId(reviewId);
		model.addAttribute("reviewInfo", reviewInfo);
		return "adminReviewViewManagement";
	}

	/* --- Admin Trainer Course User Payment View Management --- */
	@RequestMapping(method = RequestMethod.GET, value = "/adminTrainerCourseUserPaymentViewManagement/{userCoursePaymentId}", produces = "application/json;charset=UTF-8")
	public String adminTrainerCourseUserPaymentViewManagement(
			@PathVariable(value = "userCoursePaymentId") Long userCoursePaymentId, Locale locale, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) {
		byte[] encodedBytes = Base64.encode(userCoursePaymentId.toString().getBytes());
		String encryptedUserCoursePaymentId = new String(encodedBytes);

		return "redirect:/YWRtaW5UcmFpbmVyQ291cnNlVXNlclBheW1lbnRWaWV3TWFuYWdlbWVudA?ZW5jcnlwdGVkVXNlckNvdXJzZVBheW1lbnRJZA="
				+ encryptedUserCoursePaymentId;
	}

	@RequestMapping("/YWRtaW5UcmFpbmVyQ291cnNlVXNlclBheW1lbnRWaWV3TWFuYWdlbWVudA")
	public String adminTrainerCourseUserPaymentViewManagementPage(
			@RequestParam(name = "ZW5jcnlwdGVkVXNlckNvdXJzZVBheW1lbnRJZA") String encryptedUserCoursePaymentId,
			Locale locale, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {
		byte[] decodedBytes = Base64.decode(encryptedUserCoursePaymentId);
		String decodedUserCoursePaymentId = new String(decodedBytes);
		Long userCoursePaymentId = Long.parseLong(decodedUserCoursePaymentId);
		UserCoursePaymentInfo userCoursePaymentInfo = adminService
				.getUserCoursePaymentInfoByUserCoursePaymentId(userCoursePaymentId);
		model.addAttribute("userCoursePaymentInfo", userCoursePaymentInfo);
		return "adminTrainerCourseUserPaymentViewManagement";
	}

	/*--- On Main Category Management ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/adminMainCategoryManagement", produces = "application/json;charset=UTF-8")
	public String adminMainCategoryManagement(Locale locale, Model model, HttpSession session) {
		getCategoryInfo(locale, model);
		return "adminMainCategoryManagement";
	}

	/*--- On Add New Main Category Management ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/addNewMainCategoryManagement", produces = "application/json;charset=UTF-8")
	public String addNewMainCategoryManagement(Locale locale, Model model, HttpSession session) {
		return "addNewMainCategoryManagement";
	}
	
	/*--- On Add New Admin Payment Settlement Management ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/adminPaymentSettlementManagement", produces = "application/json;charset=UTF-8")
	public String adminPaymentSettlementManagement(Locale locale, Model model, HttpSession session) {
		List<CourseContributionInfo> courseContributionInfos = adminService.getCourseContributionList();
		if (courseContributionInfos != null) {
			model.addAttribute("courseContributionList", courseContributionInfos);
		}
		return "adminPaymentSettlementManagement";
	}

	/*--- On Add New Sub Admin Management --- */
	@RequestMapping(method = RequestMethod.GET, value = "/addNewSubAdminManagement", produces = "application/json;charset=UTF-8")
	public String addNewSubAdminManagement(Locale locale, Model model, HttpSession session) {
		return "addNewSubAdminManagement";
	}

	/*--- On Update Main Category Management ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/adminMainCategoryUpdate", produces = "application/json;charset=UTF-8")
	public String adminMainCategoryUpdatePage(Locale locale, Model model, HttpSession session) {
		return "adminMainCategoryUpdate";
	}

	/*--- On Admin Main Category Update Page ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/adminMainCategoryUpdate/{categoryId}", produces = "application/json;charset=UTF-8")
	public String adminMainCategoryUpdate(@PathVariable(value = "categoryId") Long categoryId, Locale locale,
			Model model, final RedirectAttributes redirectAttributes, HttpSession session) {

		byte[] encodedBytes = Base64.encode(categoryId.toString().getBytes());
		String encryptedMainCategoryId = new String(encodedBytes);

		return "redirect:/YWRtaW5NYWluQ2F0ZWdvcnlVcGRhdGU?ZW5jcnlwdGVkTWFpbkNhdGVnb3J5SWQ=" + encryptedMainCategoryId;
	}
	/*
	 * @RequestMapping(method = RequestMethod.GET, value =
	 * "/adminMainCategoryUpdate/{categoryId}/{categoryName}", produces =
	 * "application/json;charset=UTF-8") public String
	 * adminMainCategoryUpdate(@PathVariable(value = "categoryId") Long categoryId,
	 * 
	 * @PathVariable(value = "categoryName") String categoryName, Locale locale,
	 * Model model, final RedirectAttributes redirectAttributes, HttpSession
	 * session) {
	 * 
	 * CategoryInfo categoryInfo =
	 * adminService.getCategoryInfoByCategoryId(categoryId); if (categoryInfo !=
	 * null) {
	 * 
	 * byte[] encodedBytes = Base64.encode(categoryId.toString().getBytes()); String
	 * encryptedMainCategoryId = new String(encodedBytes);
	 * 
	 * encodedBytes = Base64.encode(categoryName.getBytes()); String
	 * encryptedMainCategoryName = new String(encodedBytes);
	 * 
	 * return
	 * "redirect:/YWRtaW5NYWluQ2F0ZWdvcnlVcGRhdGU?ZW5jcnlwdGVkTWFpbkNhdGVnb3J5SWQ="
	 * + encryptedMainCategoryId + "&ZW5jcnlwdGVkTWFpbkNhdGVnb3J5TmFtZQ=" +
	 * encryptedMainCategoryName;
	 * 
	 * } else { return "redirect:/adminMainCategoryManagement"; }
	 * 
	 * }
	 */

	@RequestMapping("/YWRtaW5NYWluQ2F0ZWdvcnlVcGRhdGU")
	public String adminMainCategoryUpdateViewPage(
			@RequestParam(name = "ZW5jcnlwdGVkTWFpbkNhdGVnb3J5SWQ") String encryptedMainCategoryId,
			// @RequestParam(name = "ZW5jcnlwdGVkTWFpbkNhdGVnb3J5TmFtZQ") String
			// encryptedMainCategoryName,
			Locale locale, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {

		byte[] decodedBytes = Base64.decode(encryptedMainCategoryId);
		String decodedCategoryId = new String(decodedBytes);
		Long categoryId = Long.parseLong(decodedCategoryId);

		// decodedBytes = Base64.decode(encryptedMainCategoryName);
		// String categoryName = new String(decodedBytes);

		CategoryInfo categoryInfo = adminService.getCategoryInfoByCategoryId(categoryId);
		if (categoryInfo != null) {
			model.addAttribute("categoryInfo", categoryInfo);
		}

		return "adminMainCategoryUpdate";
	}

	/*--- On Sub Category Management ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/adminSubCategoryManagement", produces = "application/json;charset=UTF-8")
	public String adminSubCategoryManagement(Locale locale, Model model, HttpSession session) {
		List<SubCategoryInfo> subCategoryInfos = adminService.getSubCategoryList();
		model.addAttribute("subCategoryList", subCategoryInfos);
		return "adminSubCategoryManagement";
	}

	/*--- On Add New Sub Category Management ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/addNewSubCategoryManagement", produces = "application/json;charset=UTF-8")
	public String addNewSubCategoryManagement(Locale locale, Model model, HttpSession session) {
		getCategoryInfo(locale, model);
		return "addNewSubCategoryManagement";
	}

	/*--- On Update Sub-Admin Management ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/adminSubAdminUpdate", produces = "application/json;charset=UTF-8")
	public String adminSubAdminUpdatePage(Locale locale, Model model, HttpSession session) {
		return "adminSubAdminUpdate";
	}

	/*--- On Admin Sub-Admin  Update  Page---*/
	@RequestMapping(method = RequestMethod.GET, value = "/adminSubAdminUpdate/{subAdminAccessRoleId}", produces = "application/json;charset=UTF-8")
	public String adminSubAdminUpdate(@PathVariable(value = "subAdminAccessRoleId") Long subAdminAccessRoleId,
			Locale locale, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {
		byte[] encodedBytes = Base64.encode(subAdminAccessRoleId.toString().getBytes());
		String encryptedSubAdminId = new String(encodedBytes);

		return "redirect:/YWRtaW5TdWJBZG1pblVwZGF0ZVZpZXdQYWdl?ZW5jcnlwdGVkU3ViQWRtaW5JZA=" + encryptedSubAdminId;
	}

	@RequestMapping("/YWRtaW5TdWJBZG1pblVwZGF0ZVZpZXdQYWdl")
	public String adminSubAdminUpdateViewPage(
			@RequestParam(name = "ZW5jcnlwdGVkU3ViQWRtaW5JZA") String encryptedSubAdminId, Locale locale, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) {
		byte[] decodedBytes = Base64.decode(encryptedSubAdminId);
		String decodedSubAdminId = new String(decodedBytes);
		Long subAdminId = Long.parseLong(decodedSubAdminId);
		SubAdminAccessRoleInfo subAdminInfo = adminService.getSubAdminAccessRoleInfoBySubAdminId(subAdminId);
		model.addAttribute("subAdminInfo", subAdminInfo);
		return "adminSubAdminUpdate";
	}

	/*--- Update Sub-Admin ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/updateSubAdmin")
	public String updateSubAdmin(@ModelAttribute("subAdminWrapper") @Valid SubAdminWrapper subAdminWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes, HttpSession session)
			throws IOException {
		if (result.hasErrors()) {
			return "adminSubAdminUpdate";
		} else {
			boolean updateSubAdminStatus = adminService.updateSubAdmin(subAdminWrapper, redirectAttributes,
					messageSource, session);
			byte[] encodedBytes = Base64.encode(subAdminWrapper.getSubAdminAccessRoleId().toString().getBytes());
			String encryptedSubAdminId = new String(encodedBytes);

			if (updateSubAdminStatus) {
				return "redirect:/YWRtaW5TdWJBZG1pblVwZGF0ZVZpZXdQYWdl?ZW5jcnlwdGVkU3ViQWRtaW5JZA="
						+ encryptedSubAdminId;
			} else {
				return "redirect:/YWRtaW5TdWJBZG1pblVwZGF0ZVZpZXdQYWdl?ZW5jcnlwdGVkU3ViQWRtaW5JZA="
						+ encryptedSubAdminId;
			}
		}
	}

	/*--- On Update Sub Category Management ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/adminSubCategoryUpdate", produces = "application/json;charset=UTF-8")
	public String adminSubCategoryUpdatePage(Locale locale, Model model, HttpSession session) {
		getCategoryInfo(locale, model);
		return "adminSubCategoryUpdate";
	}

	/*--- On Admin Sub Category Update Page ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/adminSubCategoryUpdate/{subcategoryId}", produces = "application/json;charset=UTF-8")
	public String adminSubCategoryUpdate(@PathVariable(value = "subcategoryId") Long subcategoryId, Locale locale,
			Model model, final RedirectAttributes redirectAttributes, HttpSession session) {

		byte[] encodedBytes = Base64.encode(subcategoryId.toString().getBytes());
		String encryptedSubcategoryId = new String(encodedBytes);

		return "redirect:/YWRtaW5TdWJDYXRlZ29yeVVwZGF0ZVZpZXdQYWdl?ZW5jcnlwdGVkU3ViY2F0ZWdvcnlJZA="
				+ encryptedSubcategoryId;
	}

	@RequestMapping("/YWRtaW5TdWJDYXRlZ29yeVVwZGF0ZVZpZXdQYWdl")
	public String adminSubCategoryUpdateViewPage(
			@RequestParam(name = "ZW5jcnlwdGVkU3ViY2F0ZWdvcnlJZA") String encryptedSubcategoryId, Locale locale,
			Model model, final RedirectAttributes redirectAttributes, HttpSession session) {

		byte[] decodedBytes = Base64.decode(encryptedSubcategoryId);
		String decodedCategoryId = new String(decodedBytes);
		Long subcategoryId = Long.parseLong(decodedCategoryId);

		SubCategoryInfo subCategoryInfo = adminService.getSubCategoryInfoBySubCategoryId(subcategoryId);
		if (subCategoryInfo != null) {
			model.addAttribute("subCategoryInfo", subCategoryInfo);
		}
		getCategoryInfo(locale, model);
		return "adminSubCategoryUpdate";
	}

	/*--- Update Sub-Category ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/updateSubCategory")
	public String updateSubCategory(
			@ModelAttribute("adminSubCategoryWrapper") @Valid AdminSubCategoryWrapper adminSubCategoryWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes, HttpSession session)
			throws IOException {
		if (result.hasErrors()) {
			return "adminSubCategoryUpdate";
		} else {
			boolean updateCategoryStatus = adminService.updateSubCategory(adminSubCategoryWrapper, redirectAttributes,
					messageSource, session);
			byte[] encodedBytes = Base64.encode(adminSubCategoryWrapper.getSubcategoryId().toString().getBytes());
			String encryptedSubcategoryId = new String(encodedBytes);

			if (updateCategoryStatus) {
				return "redirect:/YWRtaW5TdWJDYXRlZ29yeVVwZGF0ZVZpZXdQYWdl?ZW5jcnlwdGVkU3ViY2F0ZWdvcnlJZA="
						+ encryptedSubcategoryId;
			} else {
				return "redirect:/YWRtaW5TdWJDYXRlZ29yeVVwZGF0ZVZpZXdQYWdl?ZW5jcnlwdGVkU3ViY2F0ZWdvcnlJZA="
						+ encryptedSubcategoryId;
			}
		}
	}

	/*--- On FAQ Management ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/adminFAQManagement", produces = "application/json;charset=UTF-8")
	public String adminFAQManagement(Locale locale, Model model, HttpSession session) {
		List<FAQInfo> faqInfos = adminService.getFAQList();
		model.addAttribute("faqList", faqInfos);
		return "adminFAQManagement";
	}

	/*--- On Add New FAQ Management ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/addNewFAQManagement", produces = "application/json;charset=UTF-8")
	public String addNewFAQManagement(Locale locale, Model model, HttpSession session) {
		getCategoryInfo(locale, model);
		return "addNewFAQManagement";
	}

	/*--- Add New FAQs ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/addNewFAQs")
	public String addNewFAQs(@ModelAttribute("adminFAQsWrapper") @Valid AdminFAQsWrapper adminFAQsWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes, HttpSession session)
			throws IOException {
		if (result.hasErrors()) {
			return "addNewFAQManagement";
		} else {
			boolean addNewFAQs = adminService.addNewFAQs(adminFAQsWrapper, redirectAttributes, messageSource, session);
			if (addNewFAQs) {
				return "redirect:/adminFAQManagement";
			} else {
				return "redirect:/addNewFAQManagement";
			}
		}
	}

	/*--- Admin FAQs Management View Update ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/adminFAQsViewUpdate/{faqId}", produces = "application/json;charset=UTF-8")
	public String adminFAQsViewUpdate(@PathVariable(value = "faqId") Long faqId, Locale locale, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) {
		byte[] encodedBytes = Base64.encode(faqId.toString().getBytes());
		String encryptedFAQId = new String(encodedBytes);

		return "redirect:/YWRtaW5GQVFzVmlld1VwZGF0ZQ?ZW5jcnlwdGVkRmFxSWQ=" + encryptedFAQId;
	}

	@RequestMapping("/YWRtaW5GQVFzVmlld1VwZGF0ZQ")
	public String adminFAQsViewUpdateProcess(@RequestParam(name = "ZW5jcnlwdGVkRmFxSWQ") String encryptedFAQId,
			Locale locale, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {
		byte[] decodedBytes = Base64.decode(encryptedFAQId);
		String decodedFAQId = new String(decodedBytes);
		Long faqId = Long.parseLong(decodedFAQId);
		FAQInfo faqInfo = adminService.getFAQInfoByFAQId(faqId);
		model.addAttribute("faqInfo", faqInfo);
		return "adminFAQsViewUpdate";
	}

	/*--- On Promotion Offers Management ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/adminPromotionalOffersManagement", produces = "application/json;charset=UTF-8")
	public String adminPromotionalOffersManagement(Locale locale, Model model, HttpSession session) {
		List<FAQInfo> faqInfos = adminService.getFAQList();
		model.addAttribute("promotionOfferList", faqInfos);
		return "adminPromotionalOffersManagement";
	}

	/*--- On starting initialization of AdminLoginWrapper ---*/
	@ModelAttribute("adminLogin")
	public AdminLoginWrapper setUpAdminLogin() {
		return new AdminLoginWrapper();
	}

	/*--- On starting initialization of AdminResetPasswordWrapper ---*/
	@ModelAttribute("adminResetPassword")
	public AdminResetPasswordWrapper setUpResetAdminPassword() {
		return new AdminResetPasswordWrapper();
	}

	/*--- On starting initialization of AdminProfileWrapper ---*/
	@ModelAttribute("adminProfile")
	public AdminProfileWrapper setUpAdminProfile() {
		return new AdminProfileWrapper();
	}

	/*--- On starting initialization of AdminChangePasswordWrapper ---*/
	@ModelAttribute("adminChangePassword")
	public AdminChangePasswordWrapper setUpAdminChangePassword() {
		return new AdminChangePasswordWrapper();
	}

	/*--- On starting initialization of AdminCategoryWrapper ---*/
	@ModelAttribute("adminCreateNewMainCategoryWrapper")
	public AdminCategoryWrapper setUpAdminCategoryWrapper() {
		return new AdminCategoryWrapper();
	}

	/*--- On starting initialization of SubAdminWrapper ---*/
	@ModelAttribute("adminCreateNewSubAdminWrapper")
	public SubAdminWrapper setUpSubAdminWrapper() {
		return new SubAdminWrapper();
	}

	/*--- On starting initialization of AdminSubCategoryWrapper ---*/
	@ModelAttribute("adminSubCategoryWrapper")
	public AdminSubCategoryWrapper setUpAdminSubCategoryWrapper() {
		return new AdminSubCategoryWrapper();
	}

	/*--- On starting initialization of AdminFAQsWrapper ---*/
	@ModelAttribute("adminFAQsWrapper")
	public AdminFAQsWrapper setUpAdminFAQsWrapper() {
		return new AdminFAQsWrapper();
	}
	
	/*--- On starting initialization of AdminPaymentSettlementWrapper ---*/
	@ModelAttribute("adminPaymentSettlementWrapper")
	public AdminPaymentSettlementWrapper setUpAdminPaymentSettlementWrapper() {
		return new AdminPaymentSettlementWrapper();
	}

	/* --- Login Admin --- */
	@RequestMapping(method = RequestMethod.POST, value = "/adminLogin")
	public String adminLogin(@ModelAttribute("adminLogin") @Valid AdminLoginWrapper adminLoginWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {
		if (result.hasErrors()) {
			return "adminLogin";
		} else {
			boolean adminLogin = adminService.adminLogin(adminLoginWrapper, redirectAttributes, messageSource, session);
			if (adminLogin) {
				return "redirect:/adminDashboard";
			} else {
				return "redirect:/adminLogin";
			}
		}
	}

	/*--- Admin Logout ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/adminLogout", produces = "application/json;charset=UTF-8")
	public String adminLogout(HttpSession session) {
		session.invalidate();
		return "redirect:/adminLogin";
	}

	/*--- Admin Forget Password ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/adminForgetPassword")
	public String adminForgetPassword(@ModelAttribute("adminLogin") @Valid AdminLoginWrapper adminLoginWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			return "adminLogin";
		} else {
			boolean adminForgetPassword = adminService.adminForgetPassword(adminLoginWrapper, redirectAttributes,
					messageSource);
			if (adminForgetPassword) {
				return "redirect:/adminLogin";
			} else {
				return "redirect:/adminLogin";
			}
		}
	}

	/*--- Admin Reset Password ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/resetAdminPassword")
	public String resetAdminPassword(
			@ModelAttribute("adminResetPassword") @Valid AdminResetPasswordWrapper adminResetPasswordWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {
		PasswordValidator validator = new PasswordValidator();
		validator.validateAdminResetPassword(adminResetPasswordWrapper, result);
		adminResetPasswordWrapper.setAdminEmail((String) session.getAttribute("adminEmail"));
		if (result.hasErrors()) {
			return "adminResetPassword";
		} else {
			boolean admimResetPassword = adminService.adminResetPassword(adminResetPasswordWrapper, redirectAttributes,
					messageSource);
			session.removeAttribute("adminEmail");
			if (admimResetPassword) {
				return "redirect:/adminLogin";
			} else {
				return "redirect:/adminResetPassword";
			}
		}
	}

	/*--- Upload Admin Profile Image ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/uploadAdminProfileImage")
	public String uploadAdminProfileImage(@RequestParam("adminProfileImage") MultipartFile[] adminProfileImage,
			@RequestParam("adminEmail") String adminEmail, Model model, final RedirectAttributes redirectAttributes,
			HttpSession session) {
		/// System.out.println("adminProfileImage::" + adminProfileImage.length);
		boolean uploadAdminProfileImageStatus = adminService.uploadAdminProfileImage(adminProfileImage, adminEmail,
				session);
		if (uploadAdminProfileImageStatus) {
			return "redirect:/adminProfile";
		} else {
			return "redirect:/adminProfile";
		}
	}

	/*--- Update Admin Profile ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/updateAdminProfile")
	public String updateUserProfile(@ModelAttribute("adminProfile") @Valid AdminProfileWrapper adminProfileWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {
		if (!(adminProfileWrapper.getAdminPhoneNumber()).matches("\\d{10}")) {
			result.rejectValue("adminPhoneNumber", "label.validPhone");
			return "adminProfile";
		} else if (result.hasErrors()) {
			return "adminProfile";
		} else {
			boolean adminprofile = adminService.updateAdminProfile(adminProfileWrapper, redirectAttributes,
					messageSource, session);
			if (adminprofile) {
				return "redirect:/adminProfile";
			} else {
				return "redirect:/adminProfile";
			}
		}
	}

	/*--- Delete Admin Profile Image ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/deleteAdminProfileImage")
	public String deleteAdminProfileImage(@RequestParam("adminProfileImageId") Long adminProfileImageId,
			@RequestParam("adminId") Long adminId, Model model, final RedirectAttributes redirectAttributes,
			HttpSession session) throws IOException {
		boolean deleteCoverImageStatus = adminService.deleteAdminProfileImage(adminProfileImageId, adminId);
		if (deleteCoverImageStatus) {
			return "success";
		} else {
			return "fail";
		}
	}

	/*--- Change Admin Password ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/changeAdminPassword")
	public String changeUserPassword(
			@ModelAttribute("adminChangePassword") @Valid AdminChangePasswordWrapper adminChangePasswordWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {
		AdminInfo adminInfo = (AdminInfo) session.getAttribute("adminSession");

		if (!adminInfo.getAdminPassword().equals(adminChangePasswordWrapper.getAdminOldPassword())) {
			result.rejectValue("adminOldPassword", "label.oldPasswordFail");
			return "adminProfile";
		} else {
			PasswordValidator validator = new PasswordValidator();
			validator.validateAdminChangePassword(adminChangePasswordWrapper, result);
			if (result.hasErrors()) {
				return "adminProfile";
			} else {
				boolean adminProfile = adminService.adminChangePassword(adminChangePasswordWrapper, redirectAttributes,
						messageSource, session);
				if (adminProfile) {
					return "redirect:/adminProfile";
				} else {
					return "redirect:/adminProfile";
				}
			}
		}
	}

	/*--- Create New Category for Main Page ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/createNewMainCategory")
	public String createNewMainCategory(
			@ModelAttribute("adminCreateNewMainCategoryWrapper") @Valid AdminCategoryWrapper adminCategoryWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes, HttpSession session)
			throws IOException {
		// System.out.println("error::"+result.hasErrors());
		// System.out.println("error::"+result.hasErrors());
		if (result.hasErrors()) {
			return "addNewMainCategoryManagement";
		} else {
			boolean createNewCategoryStatus = adminService.createNewMainCategory(adminCategoryWrapper,
					redirectAttributes, messageSource, session);
			if (createNewCategoryStatus) {
				return "redirect:/adminMainCategoryManagement";
			} else {
				return "redirect:/addNewMainCategoryManagement";
			}
		}
	}

	/** --- Create New sub admin for Main Page --- */
	@RequestMapping(method = RequestMethod.POST, value = "/createNewSubAdmin")
	public String createNewSubAdmin(
			@ModelAttribute("adminCreateNewSubAdminWrapper") @Valid SubAdminWrapper adminSubAdminWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes, HttpSession session)
			throws IOException {
		// System.out.println("error::"+result.hasErrors());
		// System.out.println("error::"+result.hasErrors());
		if (result.hasErrors()) {
			return "addNewSubAdminManagement";
		} else {
			boolean createNewSubAdminStatus = adminService.createNewSubAdmin(adminSubAdminWrapper, redirectAttributes,
					messageSource, session);
			if (createNewSubAdminStatus) {
				return "redirect:/adminSubAdminManagement";
			} else {
				return "redirect:/addNewSubAdminManagement";
			}
		}
	}

	/*--- Change Category Status for Main Page ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/changeMainCategoryStatus")
	public @ResponseBody String changeCategoryStatus(@RequestParam("categoryId") Long categoryId,
			@RequestParam("categoryStatus") boolean categoryStatus, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) throws IOException {
		boolean changeCategoryStatus = adminService.changeMainCategoryStatus(categoryId, categoryStatus,
				redirectAttributes, messageSource, session);
		if (changeCategoryStatus) {
			return "success";
		} else {
			return "fail";
		}
	}

	/*--- Delete Category for Main Page ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/deleteMainCategory")
	public @ResponseBody String deleteCategory(@RequestParam("categoryId") Long categoryId, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) throws IOException {
		boolean deleteCategoryStatus = adminService.deleteMainCategory(categoryId, redirectAttributes, messageSource,
				session);
		if (deleteCategoryStatus) {
			return "success";
		} else {
			return "fail";
		}
	}

	/** --- Delete Course by admin --- */
	@RequestMapping(method = RequestMethod.POST, value = "/deleteTrainerCourse")
	public @ResponseBody String deleteTrainerCourse(@RequestParam("trainerCourseId") Long trainerCourseId, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) throws IOException {
		boolean deleteTrainerCourseStatus = adminService.deleteMainTrainerCourse(trainerCourseId, redirectAttributes,
				messageSource, session);
		if (deleteTrainerCourseStatus) {
			return "success";
		} else {
			return "fail";
		}
	}

	/*--- Create New Sub Category ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/createNewSubCategory")
	public String createNewSubCategory(
			@ModelAttribute("adminSubCategoryWrapper") @Valid AdminSubCategoryWrapper adminSubCategoryWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes, HttpSession session)
			throws IOException {
		if (result.hasErrors()) {
			return "addNewSubCategoryManagement";
		} else {
			boolean createNewCategoryStatus = adminService.createNewSubCategory(adminSubCategoryWrapper,
					redirectAttributes, messageSource, session);
			if (createNewCategoryStatus) {
				return "redirect:/adminSubCategoryManagement";
			} else {
				return "redirect:/addNewSubCategoryManagement";
			}
		}
	}

	/*--- Change Sub-Category Status ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/changeSubCategoryStatus")
	public @ResponseBody String changeSubCategoryStatus(@RequestParam("subcategoryId") Long subcategoryId,
			@RequestParam("subcategoryStatus") boolean subcategoryStatus, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) throws IOException {
		boolean changeCategoryStatus = adminService.changeSubCategoryStatus(subcategoryId, subcategoryStatus,
				redirectAttributes, messageSource, session);
		if (changeCategoryStatus) {
			return "success";
		} else {
			return "fail";
		}
	}

	/*--- Delete Sub-Category ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/deleteSubCategory")
	public @ResponseBody String deleteSubCategory(@RequestParam("subcategoryId") Long subcategoryId, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) throws IOException {
		boolean deleteCategoryStatus = adminService.deleteSubCategory(subcategoryId, redirectAttributes, messageSource,
				session);
		if (deleteCategoryStatus) {
			return "success";
		} else {
			return "fail";
		}
	}

	/** --- Delete sub-admin by admin --- */
	@RequestMapping(method = RequestMethod.POST, value = "/deleteAdminSubAdmin")
	public @ResponseBody String deleteAdminSubAdmin(@RequestParam("subAdminAccessRoleId") Long subAdminAccessRoleId,
			Model model, final RedirectAttributes redirectAttributes, HttpSession session) throws IOException {
		boolean deleteAdminSubAdminStatus = adminService.deleteAdminSubAdmin(subAdminAccessRoleId, redirectAttributes,
				messageSource, session);
		if (deleteAdminSubAdminStatus) {
			return "success";
		} else {
			return "fail";
		}
	}

	/*--- Update Category for Main Page ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/updateMainCategory")
	public String updateMainCategory(
			@ModelAttribute("adminCreateNewMainCategoryWrapper") @Valid AdminCategoryWrapper adminCategoryWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes, HttpSession session)
			throws IOException {
		if (result.hasErrors()) {
			return "adminMainCategoryUpdate";
		} else {
			boolean updateCategoryStatus = adminService.updateMainCategory(adminCategoryWrapper, redirectAttributes,
					messageSource, session);
			byte[] encodedBytes = Base64.encode(adminCategoryWrapper.getCategoryId().toString().getBytes());
			String encryptedMainCategoryId = new String(encodedBytes);

			// encodedBytes =
			// Base64.encode(adminCategoryWrapper.getCategoryName().getBytes());
			// String encryptedMainCategoryName = new String(encodedBytes);

			if (updateCategoryStatus) {
				return "redirect:/YWRtaW5NYWluQ2F0ZWdvcnlVcGRhdGU?ZW5jcnlwdGVkTWFpbkNhdGVnb3J5SWQ="
						+ encryptedMainCategoryId;
			} else {
				return "redirect:/YWRtaW5NYWluQ2F0ZWdvcnlVcGRhdGU?ZW5jcnlwdGVkTWFpbkNhdGVnb3J5SWQ="
						+ encryptedMainCategoryId;
			}
		}
	}

	/*--- Change FAQs Status for Main Page ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/changeFAQsStatus")
	public @ResponseBody String changeFAQsStatus(@RequestParam("faqsId") Long faqsId,
			@RequestParam("faqsStatus") boolean faqsStatus, Model model, final RedirectAttributes redirectAttributes,
			HttpSession session) throws IOException {
		boolean changeFAQsStatus = adminService.changeFAQsStatus(faqsId, faqsStatus, redirectAttributes, messageSource,
				session);
		if (changeFAQsStatus) {
			return "success";
		} else {
			return "fail";
		}
	}

	/*--- Delete FAQs for Main Page ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/deleteFAQs")
	public @ResponseBody String deleteFAQs(@RequestParam("faqsId") Long faqsId, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) throws IOException {
		boolean deleteFAQs = adminService.deleteFAQs(faqsId, redirectAttributes, messageSource, session);
		if (deleteFAQs) {
			return "success";
		} else {
			return "fail";
		}
	}

	/*--- Update FAQs for Main Page ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/updateFAQs")
	public String updateFAQs(@ModelAttribute("adminFAQsWrapper") @Valid AdminFAQsWrapper adminFAQsWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes, HttpSession session)
			throws IOException {
		byte[] encodedBytes = Base64.encode(adminFAQsWrapper.getFaqId().toString().getBytes());
		String encryptedFAQId = new String(encodedBytes);

		if (result.hasErrors()) {
			return "adminFAQsViewUpdate";
		} else {
			boolean updateFAQs = adminService.updateFAQs(adminFAQsWrapper, redirectAttributes, messageSource);
			if (updateFAQs) {
				return "redirect:/YWRtaW5GQVFzVmlld1VwZGF0ZQ?ZW5jcnlwdGVkRmFxSWQ=" + encryptedFAQId;
			} else {
				return "redirect:/YWRtaW5GQVFzVmlld1VwZGF0ZQ?ZW5jcnlwdGVkRmFxSWQ=" + encryptedFAQId;
			}
		}
	}

	/*--- Change User Status In Admin Dashboard ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/changeUserStatus")
	public @ResponseBody String changeUserStatus(@RequestParam("userId") Long userId,
			@RequestParam("userStatus") boolean userStatus, Model model, final RedirectAttributes redirectAttributes,
			HttpSession session) throws IOException {
		boolean changeUserStatus = adminService.changeUserStatus(userId, userStatus, redirectAttributes, messageSource,
				session);
		if (changeUserStatus) {
			return "success";
		} else {
			return "fail";
		}
	}

	/** --- Change News Letter Status In Admin Dashboard --- */
	@RequestMapping(method = RequestMethod.POST, value = "/changeNewsLetterStatus")
	public @ResponseBody String changeNewsLetterStatus(@RequestParam("newsLetterId") Long newsLetterId,
			@RequestParam("newsLetterStatus") boolean newsLetterStatus, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) throws IOException {
		boolean changeNewsLetterStatus = adminService.changeNewsLetterStatus(newsLetterId, newsLetterStatus,
				redirectAttributes, messageSource, session);
		if (changeNewsLetterStatus) {
			return "success";
		} else {
			return "fail";
		}
	}

	/** --- Change Sub Admin Status In Admin Dashboard --- */
	@RequestMapping(method = RequestMethod.POST, value = "/changeSubAdminStatus")
	public @ResponseBody String changeSubAdminStatus(@RequestParam("subAdminAccessRoleId") Long subAdminAccessRoleId,
			@RequestParam("subAdminStatus") boolean subAdminStatus, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) throws IOException {
		boolean changeSubAdminStatus = adminService.changeSubAdminStatus(subAdminAccessRoleId, subAdminStatus,
				redirectAttributes, messageSource, session);
		if (changeSubAdminStatus) {
			return "success";
		} else {
			return "fail";
		}
	}

	/** --- Change Review Status In Admin Dashboard --- */
	@RequestMapping(method = RequestMethod.POST, value = "/changeReviewStatus")
	public @ResponseBody String changeReviewStatus(@RequestParam("reviewId") Long reviewId,
			@RequestParam("reviewStatus") boolean reviewStatus, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) throws IOException {
		boolean changeReviewStatus = adminService.changeReviewStatus(reviewId, reviewStatus, redirectAttributes,
				messageSource, session);
		if (changeReviewStatus) {
			return "success";
		} else {
			return "fail";
		}
	}

	/*--- Change Trainer Status In Admin Dashboard ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/changeTrainerStatus")
	public @ResponseBody String changeTrainerStatus(@RequestParam("trainerId") Long trainerId,
			@RequestParam("trainerStatus") boolean trainerStatus, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) throws IOException {
		boolean changeTrainerStatus = adminService.changeTrainerStatus(trainerId, trainerStatus, redirectAttributes,
				messageSource, session);
		if (changeTrainerStatus) {
			return "success";
		} else {
			return "fail";
		}
	}

	/*--- Change Trainer Admin Approve Status In Admin Dashboard ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/changeTrainerAdminApproveStatus")
	public @ResponseBody String changeTrainerAdminApproveStatus(@RequestParam("trainerId") Long trainerId,
			@RequestParam("trainerAdminApproveStatus") boolean trainerAdminApproveStatus, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) throws IOException {
		boolean changeTrainerAdminApproveStatus = adminService.changeTrainerAdminApproveStatus(trainerId,
				trainerAdminApproveStatus, redirectAttributes, messageSource, session);
		if (changeTrainerAdminApproveStatus) {
			if (trainerAdminApproveStatus) {
				return "false";
			} else if (!trainerAdminApproveStatus) {
				return "true";
			} else {
				return "success";
			}
		} else {
			return "fail";
		}
	}
	
	/*--- Admin Pay Trainer Course Payment Contribution ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/payTrainerCoursePaymentContribution")
	public @ResponseBody String payTrainerCoursePaymentContribution(@RequestParam("userCoursePaymentId") Long userCoursePaymentId,
			Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) {
		boolean payTrainerCoursePaymentContributionStatus = adminService.payTrainerCoursePaymentContribution(userCoursePaymentId, redirectAttributes, messageSource, session);
		if (payTrainerCoursePaymentContributionStatus) {
			return "success";
		} else {
			return "fail";
		}
	}
	
	/*--- addPaymentSettlementContribution ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/addPaymentSettlementContribution")
	public String addPaymentSettlementContribution(
			@ModelAttribute("adminPaymentSettlementWrapper") @Valid AdminPaymentSettlementWrapper adminPaymentSettlementWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes, HttpSession session)
			throws IOException {
		if (!adminPaymentSettlementWrapper.getPercentageContribution().matches("\\d{2}")) {
			result.rejectValue("percentageContribution", "label.validPercentageContribution");
			return "adminPaymentSettlementManagement";
		} else if (result.hasErrors()) {
			return "adminPaymentSettlementManagement";
		} else {
			boolean addPaymentSettlementContributionStatus = adminService.addPaymentSettlementContribution(adminPaymentSettlementWrapper, redirectAttributes,
					messageSource, session);
			if (addPaymentSettlementContributionStatus) {
				return "redirect:/adminPaymentSettlementManagement";
			} else {
				return "redirect:/adminPaymentSettlementManagement";
			}
		}
	}
}
