package com.cube9.novafitness.controller.user;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.cube9.novafitness.config.PasswordValidator;
import com.cube9.novafitness.model.admin.CategoryInfo;
import com.cube9.novafitness.model.course.CourseOtherImagesInfo;
import com.cube9.novafitness.model.course.CourseOtherPDFsInfo;
import com.cube9.novafitness.model.course.CourseOtherVideosInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseChapterInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseChapterLessonInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseFAQInfo;
import com.cube9.novafitness.model.trainer.TrainerCourseInfo;
import com.cube9.novafitness.model.trainer.TrainerInfo;
import com.cube9.novafitness.model.user.CartTrainerCourseUserInfo;
import com.cube9.novafitness.model.user.FavouriteTrainerCourseUserInfo;
import com.cube9.novafitness.model.user.GenderInfo;
import com.cube9.novafitness.model.user.TrainerCourseUserQuestionAnswerInfo;
import com.cube9.novafitness.model.user.TrainerCourseUserQuestionInfo;
import com.cube9.novafitness.model.user.UserCoursePaymentInfo;
import com.cube9.novafitness.model.user.UserCourseReviewInfo;
import com.cube9.novafitness.model.user.UserInfo;
import com.cube9.novafitness.response.user.TrainerCourseListResponse;
import com.cube9.novafitness.service.admin.AdminService;
import com.cube9.novafitness.service.trainer.TrainerService;
import com.cube9.novafitness.service.user.UserService;
import com.cube9.novafitness.service.user.payment.UserPaymentServices;
import com.cube9.novafitness.wrapper.trainer.TrainerRegisterWrapper;
import com.cube9.novafitness.wrapper.user.DiscussionRoomUserLoginWrapper;
import com.cube9.novafitness.wrapper.user.NewsLetterWrapper;
import com.cube9.novafitness.wrapper.user.TrainerCourseUserQuestionAnswerWrapper;
import com.cube9.novafitness.wrapper.user.TrainerCourseUserQuestionWrapper;
import com.cube9.novafitness.wrapper.user.UserChangePasswordWrapper;
import com.cube9.novafitness.wrapper.user.UserCourseReviewWrapper;
import com.cube9.novafitness.wrapper.user.UserLoginWrapper;
import com.cube9.novafitness.wrapper.user.UserProfileWrapper;
import com.cube9.novafitness.wrapper.user.UserRegisterWrapper;
import com.cube9.novafitness.wrapper.user.UserResetPasswordWrapper;
import com.cube9.novafitness.wrapper.user.payment.CouponCodePaymentWrapper;
import com.cube9.novafitness.wrapper.user.payment.UserPaymentCheckoutWraspper;
import com.paypal.api.payments.PayerInfo;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.ShippingAddress;
import com.paypal.api.payments.Transaction;
import com.paypal.base.rest.PayPalRESTException;
import com.sun.jersey.core.util.Base64;

/**
 * @author Vaibhav Deshmane
 */

@Controller
public class UserWebController {

	@Autowired
	private UserService userService;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private TrainerService trainerService;

	@Autowired
	private AdminService adminService;

	/*--- getCategoryInfo ---*/
	public void getCategoryInfo(Model model) {
		List<CategoryInfo> categoryInfos = userService.getActiveMainCategoryList();
		model.addAttribute("categoryList", categoryInfos);
	}

	/*--- getGenderList ---*/
	public void getGenderList(Model model) {
		List<GenderInfo> genderInfos = userService.getGenderList();
		model.addAttribute("genderList", genderInfos);
	}

	/*--- getAllActiveCourseList ---*/
	public void getAllActiveCourseList(Model model) {
		List<TrainerCourseInfo> trainerCourseInfos = userService.getAllActiveCourseList();
		model.addAttribute("trainerCourseList", trainerCourseInfos);
	}

	/*--- getAllActiveUserCartList ---*/
	public void getAllActiveUserCartList(Model model, UserInfo userInfo) {
		// if (session.getAttribute("myFavouriteListCount") != null)
		// session.removeAttribute("myFavouriteListCount");

		// if (session.getAttribute("myCartListTotalPriceAfterCouponCode") != null)

		Double totalPrice = 0.0;
		List<CartTrainerCourseUserInfo> cartTrainerCourseUserInfos = userService
				.getUserCartTrainerCourseListByUserId(userInfo.getUserId());
		if (cartTrainerCourseUserInfos != null) {
			// session.setAttribute("myCartListCount",
			// favouriteTrainerCourseUserInfos.size());
			for (CartTrainerCourseUserInfo cartTrainerCourseUserInfo : cartTrainerCourseUserInfos) {
				totalPrice = cartTrainerCourseUserInfo.getTrainerCourseInfo().getTrainerCourseInfo().getCoursePrice()
						+ totalPrice;
			}
			model.addAttribute("myCartCourseList", cartTrainerCourseUserInfos);
			model.addAttribute("myCartListCount", cartTrainerCourseUserInfos.size());
			model.addAttribute("myCartListTotalPrice", totalPrice);
		} else {
			// session.setAttribute("myCartListCount", 0);
			model.addAttribute("myCartListCount", 0);
			model.addAttribute("myCartListTotalPrice", 0.0);
		}
	}

	/*--- On starting Application ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/", produces = "application/json;charset=UTF-8")
	public String user(Locale locale, Model model, HttpSession session) {
		getCategoryInfo(model);
		removeCouponInfoFromSession(session);
		List<TrainerCourseInfo> trainerCourseInfos = userService.getAllActiveCourseList();
		if (trainerCourseInfos != null) {
			List<TrainerCourseListResponse> trainerCourseListResponses = new ArrayList<TrainerCourseListResponse>();

			for (TrainerCourseInfo trainerCourseInfo : trainerCourseInfos) {
				TrainerCourseListResponse trainerCourseListResponse = new TrainerCourseListResponse();
				trainerCourseListResponse.setTrainerCourseInfo(trainerCourseInfo);
				trainerCourseListResponses.add(trainerCourseListResponse);
			}

			if(!trainerCourseListResponses.isEmpty()) {
				trainerCourseListResponses = userService
						.getTrainerCourseListResponseWithAverageRating(trainerCourseListResponses);
			}
			
			UserInfo userInfo = (UserInfo) session.getAttribute("userSession");
			if (userInfo != null) {

				getAllActiveUserCartList(model, userInfo);

				if (trainerCourseListResponses != null) {
					trainerCourseListResponses = userService
							.checkTrainerCourseIsInUsersFavList(trainerCourseListResponses, userInfo, session);
				}
			}

			model.addAttribute("trainerCourseResponseList", trainerCourseListResponses);
		}
		return "userHome";
	}

	/*--- On starting Application ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/userHome", produces = "application/json;charset=UTF-8")
	public String userHome(Locale locale, Model model, HttpSession session) {
		getCategoryInfo(model);
		removeCouponInfoFromSession(session);
		List<TrainerCourseInfo> trainerCourseInfos = userService.getAllActiveCourseList();
		if (trainerCourseInfos != null) {
			List<TrainerCourseListResponse> trainerCourseListResponses = new ArrayList<TrainerCourseListResponse>();

			for (TrainerCourseInfo trainerCourseInfo : trainerCourseInfos) {
				TrainerCourseListResponse trainerCourseListResponse = new TrainerCourseListResponse();
				trainerCourseListResponse.setTrainerCourseInfo(trainerCourseInfo);
				trainerCourseListResponses.add(trainerCourseListResponse);
			}

			trainerCourseListResponses = userService
					.getTrainerCourseListResponseWithAverageRating(trainerCourseListResponses);

			UserInfo userInfo = (UserInfo) session.getAttribute("userSession");
			if (userInfo != null) {

				getAllActiveUserCartList(model, userInfo);

				if (trainerCourseListResponses != null) {
					trainerCourseListResponses = userService
							.checkTrainerCourseIsInUsersFavList(trainerCourseListResponses, userInfo, session);
				}
			}

			model.addAttribute("trainerCourseResponseList", trainerCourseListResponses);
		}
		return "userHome";
	}
	
	/*--- On starting Application ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/userHomeSession", produces = "application/json;charset=UTF-8")
	public String userHomeSession(Locale locale, Model model, HttpSession session) {
		getCategoryInfo(model);
		removeCouponInfoFromSession(session);
		List<TrainerCourseInfo> trainerCourseInfos = userService.getAllActiveCourseList();
		if (trainerCourseInfos != null) {
			List<TrainerCourseListResponse> trainerCourseListResponses = new ArrayList<TrainerCourseListResponse>();

			for (TrainerCourseInfo trainerCourseInfo : trainerCourseInfos) {
				TrainerCourseListResponse trainerCourseListResponse = new TrainerCourseListResponse();
				trainerCourseListResponse.setTrainerCourseInfo(trainerCourseInfo);
				trainerCourseListResponses.add(trainerCourseListResponse);
			}

			trainerCourseListResponses = userService
					.getTrainerCourseListResponseWithAverageRating(trainerCourseListResponses);

			UserInfo userInfo = (UserInfo) session.getAttribute("userSession");
			if (userInfo != null) {

				getAllActiveUserCartList(model, userInfo);

				if (trainerCourseListResponses != null) {
					trainerCourseListResponses = userService
							.checkTrainerCourseIsInUsersFavList(trainerCourseListResponses, userInfo, session);
				}
			}

			model.addAttribute("trainerCourseResponseList", trainerCourseListResponses);
		}
		return "userHomeSession";
	}

	/*--- On starting Application ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/userCourseDetails111", produces = "application/json;charset=UTF-8")
	public String userCourseDetails1(Locale locale, Model model, HttpSession session) {
		getCategoryInfo(model);
		removeCouponInfoFromSession(session);
		return "userCourseDetails1";
	}
	
	/*--- On starting Application ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/userLogin", produces = "application/json;charset=UTF-8")
	public String userLogin(Locale locale, Model model, HttpSession session) {
		getCategoryInfo(model);
		removeCouponInfoFromSession(session);
		return "userLogin";
	}

	/*--- On starting Application ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/becomeTrainer", produces = "application/json;charset=UTF-8")
	public String becomeTrainer(Locale locale, Model model, HttpSession session) {
		getCategoryInfo(model);
		removeCouponInfoFromSession(session);
		return "userBecomeTrainer";
	}

	/*--- On starting Application ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/userRegister", produces = "application/json;charset=UTF-8")
	public String userRegister(Locale locale, Model model, HttpSession session) {
		getCategoryInfo(model);
		removeCouponInfoFromSession(session);
		return "userRegister";
	}

	/*--- On user Reset Password ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/userResetPassword", produces = "application/json;charset=UTF-8")
	public String userResetPassword(Locale locale, Model model, HttpSession session) {
		getCategoryInfo(model);
		removeCouponInfoFromSession(session);
		return "userResetPassword";
	}

	/*--- On user Forgot Password ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/userForgetPassword", produces = "application/json;charset=UTF-8")
	public String userForgetPassword(Locale locale, Model model, HttpSession session) {
		getCategoryInfo(model);
		removeCouponInfoFromSession(session);
		return "userForgetPassword";
	}

	/*--- On user Dashboard ---*/
	/*
	 * @RequestMapping(method = RequestMethod.GET, value = "/userDashboard",
	 * produces = "application/json;charset=UTF-8") public String
	 * userDashboard(Locale locale, Model model, HttpSession session) { return
	 * "userDashboard"; }
	 */

	/*--- On user Profile ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/userProfile", produces = "application/json;charset=UTF-8")
	public String userProfile(Locale locale, Model model, HttpSession session) {
		getGenderList(model);
		getCategoryInfo(model);
		removeCouponInfoFromSession(session);
		UserInfo userInfo = (UserInfo) session.getAttribute("userSession");
		if (userInfo != null) {
			UserInfo userInfo1 = userService.getUserInfoByUserEmail(userInfo.getUserEmail());
			getAllActiveUserCartList(model, userInfo);
			//session.setAttribute("userProfileImageSession", userInfo);
			model.addAttribute("userProfileImageSession", userInfo1);
		}
		return "userProfile";
	}

	/*--- On user Reset Password Page ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/userResetPassword/{userEmail}", produces = "application/json;charset=UTF-8")
	public String userResetPassword(@PathVariable(value = "userEmail") String userEmail, Locale locale, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) {
		getCategoryInfo(model);
		removeCouponInfoFromSession(session);
		session.setAttribute("userEmail", userEmail);
		return "redirect:/userResetPassword";
	}

	/*--- On User Favourite List Page ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/userFavouriteList", produces = "application/json;charset=UTF-8")
	public String userFavouriteList(Locale locale, Model model, HttpSession session) {

		getCategoryInfo(model);
		removeCouponInfoFromSession(session);

		// System.out.println("courseCategotyId::"+courseCategotyId);
		UserInfo userInfo = (UserInfo) session.getAttribute("userSession");
		if (userInfo != null) {
			List<FavouriteTrainerCourseUserInfo> favouriteTrainerCourseUserInfos = userService
					.getUserFavouriteTrainerCourseListByUserId(userInfo.getUserId());
			if (favouriteTrainerCourseUserInfos != null) {
				/*
				 * for (TrainerCourseInfo trainerCourseInfo : trainerCourseInfos) {
				 * TrainerCourseListResponse trainerCourseListResponse = new
				 * TrainerCourseListResponse();
				 * trainerCourseListResponse.setTrainerCourseInfo(trainerCourseInfo);
				 * trainerCourseListResponses.add(trainerCourseListResponse); }
				 * 
				 * trainerCourseListResponses = userService
				 * .checkTrainerCourseIsInUsersFavList(trainerCourseListResponses, userInfo,
				 * session);
				 */

				model.addAttribute("userfavouriteTrainerCourseList", favouriteTrainerCourseUserInfos);
			}
			getAllActiveUserCartList(model, userInfo);
		}
		return "userFavouriteList";
	}

	/*--- On User Shopping Cart List Page ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/userShopingCartList", produces = "application/json;charset=UTF-8")
	public String userShopingCartList(Locale locale, Model model, HttpSession session) {

		getCategoryInfo(model);
		// removeCouponInfoFromSession(session);

		// System.out.println("courseCategotyId::"+courseCategotyId);
		UserInfo userInfo = (UserInfo) session.getAttribute("userSession");
		if (userInfo != null) {
			getAllActiveUserCartList(model, userInfo);
		}
		return "userShopingCartList";
	}

	/*--- On User Payment Receipt Page ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/userPaymentReceipt", produces = "application/json;charset=UTF-8")
	public String userPaymentReceipt(Locale locale, Model model, HttpSession session) {

		getCategoryInfo(model);
		// removeCouponInfoFromSession(session);

		// System.out.println("courseCategotyId::"+courseCategotyId);
		UserInfo userInfo = (UserInfo) session.getAttribute("userSession");
		if (userInfo != null) {
			getAllActiveUserCartList(model, userInfo);
		}
		return "userPaymentReceipt";
	}

	/*--- On User Referal Invitation Page ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/userReferalInvitation", produces = "application/json;charset=UTF-8")
	public String userReferalInvitation(Locale locale, Model model, HttpSession session) {

		getCategoryInfo(model);
		// removeCouponInfoFromSession(session);

		// System.out.println("courseCategotyId::"+courseCategotyId);
		UserInfo userInfo = (UserInfo) session.getAttribute("userSession");
		if (userInfo != null) {
			getAllActiveUserCartList(model, userInfo);
		}
		return "userReferalInvitation";
	}

	/*--- On User Referal Invitation Page ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/userPaymentError", produces = "application/json;charset=UTF-8")
	public String userPaymentError(Locale locale, Model model, HttpSession session) {

		getCategoryInfo(model);
		// removeCouponInfoFromSession(session);

		// System.out.println("courseCategotyId::"+courseCategotyId);
		UserInfo userInfo = (UserInfo) session.getAttribute("userSession");
		if (userInfo != null) {
			getAllActiveUserCartList(model, userInfo);
		}
		return "userPaymentError";
	}

	/*--- On User Referal Invitation Page ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/userPaymentCancel", produces = "application/json;charset=UTF-8")
	public String userPaymentCancel(Locale locale, Model model, HttpSession session) {

		getCategoryInfo(model);
		// removeCouponInfoFromSession(session);

		// System.out.println("courseCategotyId::"+courseCategotyId);
		UserInfo userInfo = (UserInfo) session.getAttribute("userSession");
		if (userInfo != null) {
			getAllActiveUserCartList(model, userInfo);
		}
		return "userPaymentCancel";
	}

	/*--- On User Shopping Cart List Page ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/userPaymentCheckout", produces = "application/json;charset=UTF-8")
	public String userPaymentCheckout(Locale locale, Model model, HttpSession session) {

		getCategoryInfo(model);
		// removeCouponInfoFromSession(session);

		// System.out.println("courseCategotyId::"+courseCategotyId);
		UserInfo userInfo = (UserInfo) session.getAttribute("userSession");
		if (userInfo != null) {
			getAllActiveUserCartList(model, userInfo);
		}
		return "userPaymentCheckout";
	}

	/*--- On User Purchased Course List Page ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/userPurchasedCourseList", produces = "application/json;charset=UTF-8")
	public String userPurchasedCourseList(Locale locale, Model model, HttpSession session) {

		getCategoryInfo(model);
		removeCouponInfoFromSession(session);

		UserInfo userInfo = (UserInfo) session.getAttribute("userSession");
		if (userInfo != null) {
			List<UserCoursePaymentInfo> userCoursePaymentInfos = userService
					.getUserCoursePaymentListByUserId(userInfo.getUserId());
			if (userCoursePaymentInfos != null) {
				model.addAttribute("userPurchasedCourseList", userCoursePaymentInfos);
			}
			getAllActiveUserCartList(model, userInfo);
		}
		return "userPurchasedCourseList";
	}

	/*--- On starting initialization of UserRegisterWrapper ---*/
	@ModelAttribute("userRegisterWrapper")
	public UserRegisterWrapper setUpUserRegisterWrapper() {
		return new UserRegisterWrapper();
	}

	/*--- On starting initialization of UserLoginWrapper ---*/
	@ModelAttribute("userLoginWrapper")
	public UserLoginWrapper setUpUserLoginWrapper() {
		return new UserLoginWrapper();
	}

	/*--- On starting initialization of UserResetPasswordWrapper ---*/
	@ModelAttribute("userResetPasswordWrapper")
	public UserResetPasswordWrapper setUpUserResetPasswordWrapper() {
		return new UserResetPasswordWrapper();
	}

	/*--- On starting initialization of UserProfileWrapper ---*/
	@ModelAttribute("userProfileWrapper")
	public UserProfileWrapper setUpUserProfileWrapper() {
		return new UserProfileWrapper();
	}

	/*--- On starting initialization of UserChangePasswordWrapper ---*/
	@ModelAttribute("userChangePasswordWrapper")
	public UserChangePasswordWrapper setUpUserChangePasswordWrapper() {
		return new UserChangePasswordWrapper();
	}

	/*--- On starting initialization of TrainerRegisterWrapper ---*/
	@ModelAttribute("trainerRegisterWrapper")
	public TrainerRegisterWrapper setUpTrainerRegisterWrapper() {
		return new TrainerRegisterWrapper();
	}

	/*--- On starting initialization of DiscussionRoomUserLoginWrapper ---*/
	@ModelAttribute("discussionRoomUserLoginWrapper")
	public DiscussionRoomUserLoginWrapper setUpDiscussionRoomUserLoginWrapper() {
		return new DiscussionRoomUserLoginWrapper();
	}

	/*--- On starting initialization of TrainerCourseUserQuestionWrapper ---*/
	@ModelAttribute("trainerCourseUserQuestionWrapper")
	public TrainerCourseUserQuestionWrapper setUpTrainerCourseUserQuestionWrapper() {
		return new TrainerCourseUserQuestionWrapper();
	}

	/*--- On starting initialization of TrainerCourseUserQuestionAnswerWrapper ---*/
	@ModelAttribute("trainerCourseUserQuestionAnswerWrapper")
	public TrainerCourseUserQuestionAnswerWrapper setUpTrainerCourseUserQuestionAnswerWrapper() {
		return new TrainerCourseUserQuestionAnswerWrapper();
	}

	/*--- On starting initialization of UserPaymentCheckoutWraspper ---*/
	@ModelAttribute("userPaymentCheckoutWraspper")
	public UserPaymentCheckoutWraspper setUpUserPaymentCheckoutWraspper() {
		return new UserPaymentCheckoutWraspper();
	}

	/*--- On starting initialization of CouponCodePaymentWrapper ---*/
	@ModelAttribute("couponCodePaymentWrapper")
	public CouponCodePaymentWrapper setUpCouponCodePaymentWrapper() {
		return new CouponCodePaymentWrapper();
	}

	/*--- On starting initialization of UserCourseReviewWrapper ---*/
	@ModelAttribute("userCourseReviewWrapper")
	public UserCourseReviewWrapper setUpUserCourseReviewWrapper() {
		return new UserCourseReviewWrapper();
	}

	/*--- On starting initialization of NewsLetterWrapper ---*/
	@ModelAttribute("newsLetterWrapper")
	public NewsLetterWrapper setUpNewsLetterWrapper() {
		return new NewsLetterWrapper();
	}

	/*--- Register New user ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/registerUser")
	public String registerUser(@ModelAttribute("userRegisterWrapper") @Valid UserRegisterWrapper userRegisterWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes) {

		PasswordValidator validator = new PasswordValidator();
		validator.validateUserRegisterWrapper(userRegisterWrapper, result);

		boolean duplicateEmail = userService.checkUserDuplicateEmail(userRegisterWrapper.getUserEmail());

		if (!userRegisterWrapper.getUserPhoneNumber().matches("\\d+") ) {
			result.rejectValue("userPhoneNumber", "label.validPhone");
			return "userRegister";
		} else if (duplicateEmail) {
			result.rejectValue("userEmail", "label.userEmailAlreadyPresent");
			return "userRegister";
		} else if (!userRegisterWrapper.isUserAcceptTermsCondition()) {
			result.rejectValue("userAcceptTermsCondition", "label.validAcceptanceTermsAndConditions");
			return "userRegister";
		} else if (result.hasErrors()) {
			return "userRegister";
		} else {
			boolean registerUserStatus = userService.userRegister(userRegisterWrapper, messageSource,
					redirectAttributes, model);
			if (registerUserStatus) {
				return "redirect:/userLogin";
			} else {
				return "redirect:/userRegister";
			}
		}
	}

	/*--- user Email Verification Process Page---*/
	@RequestMapping(method = RequestMethod.GET, value = "/userEmailVerification/{userEmail}", produces = "application/json;charset=UTF-8")
	public String userEmailVerification(@PathVariable(value = "userEmail") String userEmail, Locale locale, Model model,
			final RedirectAttributes redirectAttributes) {

		userService.userVerification(userEmail, redirectAttributes, messageSource);
		return "redirect:/userLogin";
	}

	/* --- Login user --- */
	@RequestMapping(method = RequestMethod.POST, value = "/userLogin")
	public String userLogin(@ModelAttribute("userLoginWrapper") @Valid UserLoginWrapper userLoginWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {

		if (result.hasErrors()) {
			return "userLogin";
		} else {
			boolean userLogin = userService.userLogin(userLoginWrapper, redirectAttributes, messageSource, session);
			if (userLogin) {
				return "redirect:/userHomeSession";
			} else {
				return "redirect:/userLogin";
			}
		}
	}

	/*--- Login User for FavouriteList---*/
	@RequestMapping(method = RequestMethod.POST, value = "/userLoginForFavouriteList")
	public @ResponseBody String userLoginForFavouriteList(
			@ModelAttribute("userLoginWrapper") @Valid UserLoginWrapper userLoginWrapper, BindingResult result,
			Model model, final RedirectAttributes redirectAttributes, HttpSession session) {

		// System.out.println("hi.......");
		if (result.hasErrors()) {
			return "error";
		} else {
			boolean loginStatus = userService.userLogin(userLoginWrapper, redirectAttributes, messageSource, session);
			if (loginStatus) {
				boolean favStatus = userService.addCourseToFavouriteList(userLoginWrapper, session);
				if (favStatus) {
					return "success";
				} else {
					return "fails";
				}
			} else {
				return "fails";
			}
		}
	}

	/*--- Login User for Discussion Room ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/userLoginForDiscussionRoom")
	public @ResponseBody String userLoginForDiscussionRoom(
			@ModelAttribute("discussionRoomUserLoginWrapper") @Valid DiscussionRoomUserLoginWrapper discussionRoomUserLoginWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {

		// System.out.println("hi.......");
		if (result.hasErrors()) {
			return "error";
		} else {
			UserLoginWrapper userLoginWrapper = new UserLoginWrapper(discussionRoomUserLoginWrapper.getUserEmail(),
					discussionRoomUserLoginWrapper.getUserPassword());

			boolean loginStatus = userService.userLogin(userLoginWrapper, redirectAttributes, messageSource, session);
			if (loginStatus) {
				// boolean favStatus = userService.addCourseToFavouriteList(userLoginWrapper,
				// session);
				// if (favStatus) {
				// return "success";
				// } else {
				// return "fails";
				// }
				System.out.println("loginStatus::" + loginStatus);
				return "success1";
			} else {
				return "fails";
			}
		}
	}

	/*--- Login User for Add to Cart---*/
	@RequestMapping(method = RequestMethod.POST, value = "/userLoginForAddToCart")
	public @ResponseBody String userLoginForAddToCart(
			@ModelAttribute("userLoginWrapper") @Valid UserLoginWrapper userLoginWrapper, BindingResult result,
			Model model, final RedirectAttributes redirectAttributes, HttpSession session) {

		// System.out.println("hi.......");
		if (result.hasErrors()) {
			return "error";
		} else {
			boolean loginStatus = userService.userLogin(userLoginWrapper, redirectAttributes, messageSource, session);
			if (loginStatus) {
				boolean addtoCartStatus = userService.addCourseCart(userLoginWrapper, session);
				if (addtoCartStatus) {
					return "success";
				} else {
					return "fails";
				}
			} else {
				return "fails";
			}
		}
	}

	/*--- user Logout ---*/
	@RequestMapping(method = RequestMethod.GET, value = "/userLogout", produces = "application/json;charset=UTF-8")
	public String userLogout(HttpSession session) {
		session.invalidate();
		return "redirect:/userLogin";
	}

	/*--- user Forget Password ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/forgotUserPassword")
	public String userForgetPassword(@ModelAttribute("userLoginWrapper") @Valid UserLoginWrapper userLoginWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			return "userForgetPassword";
		} else {
			boolean userForgetPassword = userService.userForgetPassword(userLoginWrapper, redirectAttributes,
					messageSource);
			if (userForgetPassword) {
				return "redirect:/userLogin";
			} else {
				return "redirect:/userForgetPassword";
			}
		}
	}

	/*--- user Reset Password ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/resetUserPassword")
	public String resetuserPassword(
			@ModelAttribute("userResetPasswordWrapper") @Valid UserResetPasswordWrapper userResetPasswordWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {

		PasswordValidator validator = new PasswordValidator();
		validator.validateUserResetPasswordWrapper(userResetPasswordWrapper, result);
		userResetPasswordWrapper.setUserEmail((String) session.getAttribute("userEmail"));
		if (result.hasErrors()) {
			return "userResetPassword";
		} else {
			boolean resetuserPassword = userService.resetUserPassword(userResetPasswordWrapper, redirectAttributes,
					messageSource);
			session.removeAttribute("userEmail");
			if (resetuserPassword) {
				return "redirect:/userLogin";
			} else {
				return "redirect:/userResetPassword";
			}
		}
	}

	/*--- Update user Profile ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/updateUserProfile")
	public String updateUserProfile(@ModelAttribute("userProfileWrapper") @Valid UserProfileWrapper userProfileWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {

		getGenderList(model);
		/*
		 * if (!(userProfileWrapper.get).matches("\\d{10}")) {
		 * result.rejectValue("adminPhoneNumber", "label.validPhone"); return
		 * "userProfile"; } else
		 */
		if (result.hasErrors()) {
			return "userProfile";
		} else {
			boolean updateUserProfileStatus = userService.updateUserProfile(userProfileWrapper, redirectAttributes,
					messageSource, session);
			if (updateUserProfileStatus) {
				return "redirect:/userProfile";
			} else {
				return "redirect:/userProfile";
			}
		}
	}

	/*--- Upload user Profile Image ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/uploadUserProfileImage")
	public String uploadUserProfileImage(@RequestParam("userProfileImage") MultipartFile[] userProfileImage,
			@RequestParam("userEmail") String userEmail, Model model, final RedirectAttributes redirectAttributes,
			HttpSession session) {
		boolean uploaduserProfileImageStatus = userService.uploadUserProfileImage(userProfileImage, userEmail, model, session, redirectAttributes, messageSource);
		if (uploaduserProfileImageStatus) {
			//session.removeAttribute("userSession");
			//UserInfo userInfo = userService.getUserInfoByUserEmail(userEmail);
			//session.setAttribute("userSession", userInfo);
			return "redirect:/userProfile";
		} else {
			return "redirect:/userProfile";
		}
	}

	/*--- Delete user Profile Image ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/deleteUserProfileImage")
	public @ResponseBody String deleteUserProfileImage(@RequestParam("userProfileImageId") Long userProfileImageId,
			@RequestParam("userId") Long userId, Model model, final RedirectAttributes redirectAttributes,
			HttpSession session) throws IOException {
		boolean deleteUserProfileImageStatus = userService.deleteUserProfileImage(userProfileImageId, userId);
		if (deleteUserProfileImageStatus) {
			//session.removeAttribute("userSession");
			UserInfo userInfo = adminService.getUserInfoByUserId(userId);
			//session.setAttribute("userSession", userInfo);
			model.addAttribute("userProfileImageSession", userInfo);
			return "success";
		} else {
			return "fail";
		}
	}

	/*--- Change user Password ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/changeUserPassword")
	public String changeUserPassword(
			@ModelAttribute("userChangePasswordWrapper") @Valid UserChangePasswordWrapper userChangePasswordWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {

		getGenderList(model);

		UserInfo userInfo = (UserInfo) session.getAttribute("userSession");
		boolean matched = BCrypt.checkpw(userChangePasswordWrapper.getUserOldPassword(), userInfo.getUserPassword());

		if (!matched) {
			result.rejectValue("userOldPassword", "label.oldPasswordFail");
			return "userProfile";
		} else {
			PasswordValidator validator = new PasswordValidator();
			validator.validateUserChangePasswordWrapper(userChangePasswordWrapper, result);
			if (result.hasErrors()) {
				return "userProfile";
			} else {
				boolean changeUserPasswordStatus = userService.changeUserPassword(userChangePasswordWrapper,
						redirectAttributes, messageSource, session, userInfo);
				if (changeUserPasswordStatus) {
					return "redirect:/userProfile";
				} else {
					return "redirect:/userProfile";
				}
			}
		}
	}

	/*--- On User Course Details Page ---*/
	@RequestMapping("/dXNlckNvdXJzZURlcnRhaWxz")
	public String courseDetailsFrom(@RequestParam(name = "dHJhaW5lckNvdXJzZUlk") Long trainerCourseId,
			@RequestParam(name = "dHJhaW5lcklk") Long trainerId, Locale locale, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) {

		byte[] encodedBytes = Base64.encode(trainerCourseId.toString().getBytes());
		String encryptedTrainerCourseId = new String(encodedBytes);
		encodedBytes = Base64.encode(trainerId.toString().getBytes());
		String encryptedTrainerId = new String(encodedBytes);
		return "redirect:/cHJvY2Vzc2luZ09uQ291cnNlRGV0YWlsc1BhZ2U?ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUlk="
				+ encryptedTrainerCourseId + "&ZW5jcnlwdGVkVHJhaW5lcklk=" + encryptedTrainerId;

	}

	@RequestMapping("/cHJvY2Vzc2luZ09uQ291cnNlRGV0YWlsc1BhZ2U")
	public String courseDetailsPage(
			@RequestParam(name = "ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUlk") String encryptedTrainerCourseId,
			@RequestParam(name = "ZW5jcnlwdGVkVHJhaW5lcklk") String encryptedTrainerId, Locale locale, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) {

		getCategoryInfo(model);
		removeCouponInfoFromSession(session);

		byte[] decodedBytes = Base64.decode(encryptedTrainerCourseId);
		String decodedTrainerCourseId = new String(decodedBytes);
		// System.out.println("decodedTrainerCourseId::" + decodedTrainerCourseId);
		decodedBytes = Base64.decode(encryptedTrainerId);
		// String decodedTrainerId = new String(decodedBytes);
		// System.out.println("decodedTrainerId::" + decodedTrainerId);
		Long trainerCourseId = Long.parseLong(decodedTrainerCourseId);

		boolean isCoursePurchased = false;
		UserInfo userInfoSession = (UserInfo) session.getAttribute("userSession"); 
		TrainerCourseInfo trainerCourseInfo = trainerService.getTrainerCourseInfoByTrainerCourseId(trainerCourseId);
		if (trainerCourseInfo != null) {
			model.addAttribute("trainerCourseDetails", trainerCourseInfo);

			List<CourseOtherImagesInfo> courseOtherImagesInfos = trainerService
					.getCourseOtherImagesListByCourseId(trainerCourseInfo.getTrainerCourseInfo().getCourseId());
			/* System.out.println("courseOtherImagesInfos::"+courseOtherImagesInfos); */
			if (courseOtherImagesInfos != null) {
				model.addAttribute("myCourseImageList", courseOtherImagesInfos);
			}
			
			boolean favouriteListStatus = false;
			boolean cartListStatus = false;
			if (userInfoSession != null) {
				FavouriteTrainerCourseUserInfo favouriteTrainerCourseUserInfo = userService
						.getCourseFavouriteListStatus(trainerCourseInfo.getTrainerCourseId(), userInfoSession.getUserId());
				if (favouriteTrainerCourseUserInfo != null) {
					favouriteListStatus = true;
				}
				if (session.getAttribute("myFavouriteListCount") != null)
					session.removeAttribute("myFavouriteListCount");

				List<FavouriteTrainerCourseUserInfo> favouriteTrainerCourseUserInfos = userService
						.getUserFavouriteTrainerCourseListByUserId(userInfoSession.getUserId());
				if (favouriteTrainerCourseUserInfos != null) {
					session.setAttribute("myFavouriteListCount", favouriteTrainerCourseUserInfos.size());
				} else {
					session.setAttribute("myFavouriteListCount", 0);
				}
				getAllActiveUserCartList(model, userInfoSession);
				CartTrainerCourseUserInfo cartTrainerCourseUserInfo = userService
						.getCourseCartListStatus(trainerCourseInfo.getTrainerCourseId(), userInfoSession.getUserId());
				if (cartTrainerCourseUserInfo != null) {
					cartListStatus = true;
				}

				UserCoursePaymentInfo userCoursePaymentInfo = userService
						.getUserCoursePaymentInfoByTrainerCourseIdAndUserId(trainerCourseInfo.getTrainerCourseId(),
								userInfoSession.getUserId());
				if (userCoursePaymentInfo != null) {
					model.addAttribute("myPurchasedCourseInfo", userCoursePaymentInfo);
					isCoursePurchased = true;
					List<CourseOtherPDFsInfo> courseOtherPDFsInfos = trainerService
							.getCourseOtherPDFsListByCourseId(trainerCourseInfo.getTrainerCourseInfo().getCourseId());
					if (courseOtherPDFsInfos != null) {
						model.addAttribute("myPurchasedCoursePdfList", courseOtherPDFsInfos);
					}
					List<CourseOtherVideosInfo> courseOtherVideosInfos = trainerService
							.getCourseOtherVideosListByCourseId(trainerCourseInfo.getTrainerCourseInfo().getCourseId());
					if (courseOtherVideosInfos != null) {
						model.addAttribute("myPurchasedCourseVideoList", courseOtherVideosInfos);
					}
				}
			}
			model.addAttribute("favouriteListStatusCourseDetails", favouriteListStatus);
			model.addAttribute("cartListStatusCourseDetails", cartListStatus);

			TrainerCourseUserQuestionInfo trainerCourseUserQuestionInfo = userService
					.getTrainerCourseUserQuestionInfoByTrainerCourseId(trainerCourseInfo.getTrainerCourseId());
			// System.out.println("trainerCourseUserQuestionInfo::" +
			// trainerCourseUserQuestionInfo);
			if (trainerCourseUserQuestionInfo != null) {
				model.addAttribute("trainerCourseUserQuestionInfo", trainerCourseUserQuestionInfo);

				List<TrainerCourseUserQuestionAnswerInfo> trainerCourseUserQuestionAnswerInfos = userService
						.getTrainerCourseUserQuestionAnswerListByTrainerCourseUserQuestionId(
								trainerCourseUserQuestionInfo.getTrainerCourseUserQuestionId());
				// System.out.println("trainerCourseUserQuestionAnswerInfos::" +
				// trainerCourseUserQuestionAnswerInfos);
				if (trainerCourseUserQuestionAnswerInfos != null) {
					model.addAttribute("trainerCourseUserQuestionAnswerList", trainerCourseUserQuestionAnswerInfos);
				}
			}

			List<UserCourseReviewInfo> userCourseReviewInfos = userService
					.getUserCourseReviewByTrainerCourseId(trainerCourseInfo.getTrainerCourseId(), model);
			model.addAttribute("userCourseReviews", userCourseReviewInfos);
			
			List<TrainerCourseFAQInfo> trainerCourseFAQInfos = trainerService
					.getTrainerCourseFAQListByTrainerCourseId(trainerCourseInfo.getTrainerCourseId());
			//System.out.println("trainerCourseFAQInfos::"+trainerCourseFAQInfos);
			model.addAttribute("trainerCourseUserFAQList", trainerCourseFAQInfos);
			
			List<TrainerCourseChapterInfo> trainerCourseChapterInfos = trainerService
					.getTrainerCourseChapterListByTrainerCourseId(trainerCourseInfo.getTrainerCourseId());
			if (trainerCourseChapterInfos != null) {
				model.addAttribute("trainerCourseUserChapterList", trainerCourseChapterInfos);
				
				List<TrainerCourseChapterLessonInfo> trainerCourseChapterLessonInfos = new ArrayList<TrainerCourseChapterLessonInfo>();
				for (TrainerCourseChapterInfo trainerCourseChapterInfo : trainerCourseChapterInfos) {
					List<TrainerCourseChapterLessonInfo> trainerCourseChapterLessonInfos1 = trainerService
							.getTrainerCourseChapterLessonListByTrainerCourseChapterId(trainerCourseChapterInfo.getTrainerCourseChapterId());
					if (trainerCourseChapterLessonInfos1 != null) {
						trainerCourseChapterLessonInfos.addAll(trainerCourseChapterLessonInfos1);
					}
					
				}

				model.addAttribute("trainerCourseUserChapterLessonList", trainerCourseChapterLessonInfos);
			}
		}
		if(isCoursePurchased) {
			return "userCourseDetails1";
		} else {
			return userInfoSession!=null?"userCourseDetailsSession":"userCourseDetails";
		}
	}

	/*--- Filter User Course from Main Page ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/filterByCategoty")
	public String filterByCategoty(@RequestParam("categoryId") Long categoryId, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) {

		if (categoryId != null) {
			return "redirect:/dHJhaW5lckNvdXJzZUJ5RmlsdGVyU2VhcmNo?Y291cnNlTWFpbkNhdGVnb3J5SWQ=" + categoryId
					+ "&Y291cnNlU3RhclJhdGluZ3M=" + "&Y291cnNlTWluUHJpY2U=" + "&Y291cnNlTWF4UHJpY2U="
					+ "&Y291cnNlU3ViQ2F0ZWdvcmllcw=";
		} else {
			return "redirect:/dHJhaW5lckNvdXJzZUJ5RmlsdGVyU2VhcmNo?Y291cnNlTWFpbkNhdGVnb3J5SWQ="
					+ "&Y291cnNlU3RhclJhdGluZ3M=" + "&Y291cnNlTWluUHJpY2U=" + "&Y291cnNlTWF4UHJpY2U="
					+ "&Y291cnNlU3ViQ2F0ZWdvcmllcw=";
		}
	}

	@RequestMapping("/dHJhaW5lckNvdXJzZUJ5RmlsdGVyU2VhcmNo")
	public String filterByCategotyPage(@RequestParam(name = "Y291cnNlTWFpbkNhdGVnb3J5SWQ") String courseCategotyId,
			@RequestParam(name = "Y291cnNlU3RhclJhdGluZ3M") String courseStarRating,
			@RequestParam(name = "Y291cnNlTWluUHJpY2U") String courseMinPrice,
			@RequestParam(name = "Y291cnNlTWF4UHJpY2U") String courseMaxPrice,
			@RequestParam(name = "Y291cnNlU3ViQ2F0ZWdvcmllcw") String courseSubCategories, Locale locale, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) {

		getCategoryInfo(model);
		removeCouponInfoFromSession(session);

		// System.out.println("courseCategotyId::"+courseCategotyId);
		// System.out.println("courseStarRating::"+courseStarRating);
		// System.out.println("courseMinPrice::"+courseMinPrice);
		// System.out.println("courseMaxPrice::"+courseMaxPrice);
		// System.out.println("courseSubCategories::"+courseSubCategories);

		// List<Long> newlySelectedCourseSubcategories = new ArrayList<>();
		// newlySelectedCourseSubcategories =
		// (Arrays.asList(courseSubCategories)).stream().map(Long::valueOf)
		// .collect(Collectors.toList());
		// System.out.println("newlySelectedCourseSubcategories::"+newlySelectedCourseSubcategories);

		List<TrainerCourseInfo> trainerCourseInfos = new ArrayList<TrainerCourseInfo>();
		if (courseCategotyId != "") {
			trainerCourseInfos = userService.courseFilterByCategoryId(Long.parseLong(courseCategotyId));
			CategoryInfo categoryInfo = adminService.getCategoryInfoByCategoryId(Long.parseLong(courseCategotyId));
			model.addAttribute("categoryName", categoryInfo.getCategoryName());
		}

		if (trainerCourseInfos != null) {
			List<TrainerCourseListResponse> trainerCourseListResponses = new ArrayList<TrainerCourseListResponse>();

			for (TrainerCourseInfo trainerCourseInfo : trainerCourseInfos) {
				TrainerCourseListResponse trainerCourseListResponse = new TrainerCourseListResponse();
				trainerCourseListResponse.setTrainerCourseInfo(trainerCourseInfo);
				trainerCourseListResponses.add(trainerCourseListResponse);
			}

			UserInfo userInfo = (UserInfo) session.getAttribute("userSession");
			if (userInfo != null) {
				trainerCourseListResponses = userService.checkTrainerCourseIsInUsersFavList(trainerCourseListResponses,
						userInfo, session);
				getAllActiveUserCartList(model, userInfo);
			}

			if(!trainerCourseListResponses.isEmpty()) {
				trainerCourseListResponses = userService
						.getTrainerCourseListResponseWithAverageRating(trainerCourseListResponses);
			}
			
			model.addAttribute("trainerCourseFilteredList", trainerCourseListResponses);
		}
		model.addAttribute("courseCategotyId", courseCategotyId); 
		UserInfo userInfoSession = (UserInfo) session.getAttribute("userSession"); 
		
		return userInfoSession!=null?"userCourseFilterSession":"userCourseFilter";
	}

	/*--- Search User Course from Main Page ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/searchByCategoty")
	public @ResponseBody String searchByCategoty(@RequestParam("categoryId") Long categoryId, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) {

		List<TrainerCourseInfo> trainerCourseInfos = new ArrayList<TrainerCourseInfo>();
		if (categoryId != null) {
			trainerCourseInfos = userService.courseFilterByCategoryId(categoryId);
			if (trainerCourseInfos != null) {
				List<TrainerCourseListResponse> trainerCourseListResponses = new ArrayList<TrainerCourseListResponse>();

				for (TrainerCourseInfo trainerCourseInfo : trainerCourseInfos) {
					TrainerCourseListResponse trainerCourseListResponse = new TrainerCourseListResponse();
					trainerCourseListResponse.setTrainerCourseInfo(trainerCourseInfo);
					trainerCourseListResponses.add(trainerCourseListResponse);
				}

				UserInfo userInfo = (UserInfo) session.getAttribute("userSession");
				if (userInfo != null) {
					trainerCourseListResponses = userService
							.checkTrainerCourseIsInUsersFavList(trainerCourseListResponses, userInfo, session);
				}

				if(!trainerCourseListResponses.isEmpty()) {
					trainerCourseListResponses = userService
							.getTrainerCourseListResponseWithAverageRating(trainerCourseListResponses);
				}
				
				model.addAttribute("trainerCourseResponseListByCategory", trainerCourseListResponses);
			}
			return "success";
		} else {
			return "fail";
		}
	}

	@RequestMapping("/bWFpblBhZ2VDb3Vyc2VTZWFyY2hCeUNhdGVnb3J5")
	public String mainPageCourseSearchByCategory(
			@RequestParam(name = "Y291cnNlTWFpbkNhdGVnb3J5SWQ") Long courseCategotyId, Locale locale, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) {

		getCategoryInfo(model);
		removeCouponInfoFromSession(session);

		// System.out.println("courseCategotyId::"+courseCategotyId);
		UserInfo userInfoSession = (UserInfo) session.getAttribute("userSession"); 
		List<TrainerCourseInfo> trainerCourseInfos = new ArrayList<TrainerCourseInfo>();
		if (courseCategotyId != null) {
			trainerCourseInfos = userService.courseFilterByCategoryId(courseCategotyId);
			if (trainerCourseInfos != null) {
				List<TrainerCourseListResponse> trainerCourseListResponses = new ArrayList<TrainerCourseListResponse>();

				for (TrainerCourseInfo trainerCourseInfo : trainerCourseInfos) {
					TrainerCourseListResponse trainerCourseListResponse = new TrainerCourseListResponse();
					trainerCourseListResponse.setTrainerCourseInfo(trainerCourseInfo);
					trainerCourseListResponses.add(trainerCourseListResponse);
				}

				UserInfo userInfo = (UserInfo) session.getAttribute("userSession");
				if (userInfo != null) {

					getAllActiveUserCartList(model, userInfo);

					if (trainerCourseListResponses != null) {
						trainerCourseListResponses = userService
								.checkTrainerCourseIsInUsersFavList(trainerCourseListResponses, userInfo, session);
						trainerCourseListResponses = userService
								.getTrainerCourseListResponseWithAverageRating(trainerCourseListResponses);
					}
				}
				
				model.addAttribute("trainerCourseResponseListByCategory", trainerCourseListResponses);
			}
			return userInfoSession!=null?"userMainPageCourseSearchByCategorySession":"userMainPageCourseSearchByCategory";
		} else {
			return userInfoSession!=null?"userHomeSession":"userHome";
		}
	}

	/*--- WatchList Add Remove UserCourse ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/userWatchListAddRemove")
	public @ResponseBody String userWatchListAddRemove(@RequestParam(name = "trainerCourseId") Long trainerCourseId,
			@RequestParam(name = "userId") Long userId, @RequestParam(name = "watchListStatus") boolean watchListStatus,
			Model model, final RedirectAttributes redirectAttributes, HttpSession session) {

		String userWatchListAddRemove = userService.userWatchListAddRemove(trainerCourseId, userId, watchListStatus,
				session);
		return userWatchListAddRemove;
	}

	/*--- Cart Add Remove UserCourse ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/userCartListAddRemove")
	public @ResponseBody String userCartListAddRemove(@RequestParam(name = "trainerCourseId") Long trainerCourseId,
			@RequestParam(name = "userId") Long userId, @RequestParam(name = "watchListStatus") boolean watchListStatus,
			Model model, final RedirectAttributes redirectAttributes, HttpSession session) {

		String userCartListAddRemove = userService.userCartListAddRemove(trainerCourseId, userId, watchListStatus,
				session);
		return userCartListAddRemove;
	}

	/*--- Buy now UserCourse ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/userBuyNow")
	public @ResponseBody String userBuyNow(@RequestParam(name = "trainerCourseId") Long trainerCourseId,
			@RequestParam(name = "userId") Long userId, @RequestParam(name = "watchListStatus") boolean watchListStatus,
			Model model, final RedirectAttributes redirectAttributes, HttpSession session) {

		String userBuyNow = userService.userBuyNow(trainerCourseId, userId, watchListStatus,
				session);
		return userBuyNow;
	}
	
	/*--- Cart Remove UserCourse ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/userCartListRemove")
	public @ResponseBody String userCartListRemove(
			@RequestParam(name = "cartTrainerCourseUserId") Long cartTrainerCourseUserId,
			@RequestParam(name = "cartListStatus") boolean cartListStatus, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) {
		removeCouponInfoFromSession(session);
		String userCartListRemove = userService.userCartListRemove(cartTrainerCourseUserId, cartListStatus, session);

		return userCartListRemove;
	}

	/*--- User Trainer Course Question ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/uploadTrainerCourseUserQuestion")
	public String uploadTrainerCourseUserQuestion(
			@ModelAttribute("trainerCourseUserQuestionWrapper") @Valid TrainerCourseUserQuestionWrapper trainerCourseUserQuestionWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {

		getGenderList(model);

		if (result.hasErrors()) {
			return "userCourseDetailsSession";
		} else {
			boolean uploadTrainerCourseUserQuestionStatus = userService.uploadTrainerCourseUserQuestion(
					trainerCourseUserQuestionWrapper, redirectAttributes, messageSource, session);

			byte[] encodedBytes = Base64
					.encode(trainerCourseUserQuestionWrapper.getTrainerCourseId().toString().getBytes());
			String encryptedTrainerCourseId = new String(encodedBytes);
			encodedBytes = Base64.encode(trainerCourseUserQuestionWrapper.getTrainerId().toString().getBytes());
			String encryptedTrainerId = new String(encodedBytes);

			if (uploadTrainerCourseUserQuestionStatus) {
				return "redirect:/cHJvY2Vzc2luZ09uQ291cnNlRGV0YWlsc1BhZ2U?ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUlk="
						+ encryptedTrainerCourseId + "&ZW5jcnlwdGVkVHJhaW5lcklk=" + encryptedTrainerId;
			} else {
				return "redirect:/cHJvY2Vzc2luZ09uQ291cnNlRGV0YWlsc1BhZ2U?ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUlk="
						+ encryptedTrainerCourseId + "&ZW5jcnlwdGVkVHJhaW5lcklk=" + encryptedTrainerId;
			}
		}
	}

	/*--- User Trainer Course Question Post Answer---*/
	@RequestMapping(method = RequestMethod.POST, value = "/postTrainerCourseUserQuestionAnswer")
	public String postTrainerCourseUserQuestionAnswer(
			@ModelAttribute("trainerCourseUserQuestionAnswerWrapper") @Valid TrainerCourseUserQuestionAnswerWrapper trainerCourseUserQuestionAnswerWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {

		getGenderList(model);

		if (result.hasErrors()) {
			return "userCourseDetailsSession";
		} else {
			boolean postTrainerCourseUserQuestionAnswerStatus = userService.postTrainerCourseUserQuestionAnswer(
					trainerCourseUserQuestionAnswerWrapper, redirectAttributes, messageSource, session);

			byte[] encodedBytes = Base64
					.encode(trainerCourseUserQuestionAnswerWrapper.getTrainerCourseId().toString().getBytes());
			String encryptedTrainerCourseId = new String(encodedBytes);
			encodedBytes = Base64.encode(trainerCourseUserQuestionAnswerWrapper.getTrainerId().toString().getBytes());
			String encryptedTrainerId = new String(encodedBytes);

			if (postTrainerCourseUserQuestionAnswerStatus) {
				return "redirect:/cHJvY2Vzc2luZ09uQ291cnNlRGV0YWlsc1BhZ2U?ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUlk="
						+ encryptedTrainerCourseId + "&ZW5jcnlwdGVkVHJhaW5lcklk=" + encryptedTrainerId;
			} else {
				return "redirect:/cHJvY2Vzc2luZ09uQ291cnNlRGV0YWlsc1BhZ2U?ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUlk="
						+ encryptedTrainerCourseId + "&ZW5jcnlwdGVkVHJhaW5lcklk=" + encryptedTrainerId;
			}
		}
	}

	/*--- On User Trainer Details Page ---*/
	@RequestMapping("/dHJhaW5lckRldGFpbHNieVRyYWluZXJJZA")
	public String trainerDetail(@RequestParam(name = "dHJhaW5lcklk") Long trainerId, Locale locale, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session) {

		byte[] encodedBytes = Base64.encode(trainerId.toString().getBytes());
		String encryptedTrainerId = new String(encodedBytes);
		return "redirect:/dHJhaW5lckRldGFpbHNQYWdl?ZW5jcnlwdGVkVHJhaW5lcklk=" + encryptedTrainerId;

	}

	@RequestMapping("/dHJhaW5lckRldGFpbHNQYWdl")
	public String trainerDetailsPage(@RequestParam(name = "ZW5jcnlwdGVkVHJhaW5lcklk") String encryptedTrainerId,
			Locale locale, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {

		getCategoryInfo(model);
		removeCouponInfoFromSession(session);

		byte[] decodedBytes = Base64.decode(encryptedTrainerId);
		String decodedTrainerId = new String(decodedBytes);
		// System.out.println("decodedTrainerId::" + decodedTrainerId);
		Long trainerId = Long.parseLong(decodedTrainerId);

		TrainerInfo trainerInfo = adminService.getTrainierInfoByTrainerId(trainerId);
		if (trainerInfo != null) {
			model.addAttribute("trainerDetails", trainerInfo);
			List<TrainerCourseInfo> trainerCourseInfos = trainerService.getTrainerCourseListByTrainerId(trainerId);
			if (trainerCourseInfos != null) {
				List<TrainerCourseListResponse> trainerCourseListResponses = new ArrayList<TrainerCourseListResponse>();

				for (TrainerCourseInfo trainerCourseInfo : trainerCourseInfos) {
					TrainerCourseListResponse trainerCourseListResponse = new TrainerCourseListResponse();
					trainerCourseListResponse.setTrainerCourseInfo(trainerCourseInfo);
					trainerCourseListResponses.add(trainerCourseListResponse);
				}

				trainerCourseListResponses = userService
						.getTrainerCourseListResponseWithAverageRating(trainerCourseListResponses);
				model.addAttribute("trainerSCourseList", trainerCourseListResponses);
			}
		}
		
		UserInfo userInfoSession = (UserInfo) session.getAttribute("userSession"); 
		
		return userInfoSession!=null?"userTrainerDetailsSession":"userTrainerDetails";
	}

	/* --- Move User to Payment Checkout --- */
	@RequestMapping(method = RequestMethod.POST, value = "/moveUserToPaymentCheckout")
	public String moveUserToPaymentCheckout(
			@ModelAttribute("userPaymentCheckoutWraspper") @Valid UserPaymentCheckoutWraspper userPaymentCheckoutWraspper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes, HttpSession session,
			HttpServletRequest request) throws PayPalRESTException {

		// boolean userPaymentCheckotStatus =
		// userService.moveUserToPaymentCheckoutWithUserOder(userPaymentCheckoutWraspper,
		// model, session);

		// if(true) {
		// return "redirect:/userPaymentCheckout";
		// } else {
		return "redirect:/userShopingCartList";
		// }

	}

	/* --- Authorize User Payment --- */
	@RequestMapping(method = RequestMethod.POST, value = "/authorizeUserPayment")
	public String authorizeUserPayment(
			@ModelAttribute("userPaymentCheckoutWraspper") @Valid UserPaymentCheckoutWraspper userPaymentCheckoutWraspper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes, HttpSession session,
			HttpServletRequest request) throws PayPalRESTException {

		try {
			// boolean userPaymentStatus =
			// userService.userCoursePaymentProcess(userPaymentCheckoutWraspper, session,
			// model);
			// UserPaymentServices userPaymentServices = new UserPaymentServices();
			String approvalLink = userService.authorizeUserPayment(userPaymentCheckoutWraspper);

			return "redirect:" + approvalLink;
			// response.sendRedirect(approvalLink);

		} catch (PayPalRESTException ex) {
			// request.setAttribute("errorMessage", ex.getMessage());
			model.addAttribute("userPaymentErrorMessage", ex.getMessage());
			ex.printStackTrace();
			return "userPaymentError";
		}
	}

	/* --- Review User Payment --- */
	@RequestMapping(method = RequestMethod.POST, value = "/userReviewPayment")
	public String userReviewPayment(Model model, final RedirectAttributes redirectAttributes, HttpSession session,
			HttpServletRequest request, HttpServletResponse response) throws PayPalRESTException {

		String paymentId = request.getParameter("paymentId");
		String payerId = request.getParameter("PayerID");

		try {
			UserPaymentServices userPaymentServices = new UserPaymentServices();
			Payment payment = userPaymentServices.getPaymentDetails(paymentId);

			PayerInfo payerInfo = payment.getPayer().getPayerInfo();
			Transaction transaction = payment.getTransactions().get(0);
			ShippingAddress shippingAddress = transaction.getItemList().getShippingAddress();

			model.addAttribute("payer", payerInfo);
			model.addAttribute("transaction", transaction);
			model.addAttribute("shippingAddress", shippingAddress);

			// request.setAttribute("payer", payerInfo);
			// request.setAttribute("transaction", transaction);
			// request.setAttribute("shippingAddress", shippingAddress);

			String url = "userPaymentCheckout?paymentId=" + paymentId + "&PayerID=" + payerId;

			return "redirect:" + url;
			// request.getRequestDispatcher(url).forward(request, response);

		} catch (PayPalRESTException ex) {
			// request.setAttribute("errorMessage", ex.getMessage());
			model.addAttribute("userPaymentErrorMessage", ex.getMessage());
			ex.printStackTrace();
			return "userPaymentError";
		}
	}

	/* --- Review User Payment --- */
	@RequestMapping("/userReviewPayment")
	public String userPaymentCheckout(@RequestParam(name = "paymentId") String paymentId,
			@RequestParam(name = "PayerID") String payerId, Locale locale, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session, HttpServletRequest request,
			HttpServletResponse response) {

		getCategoryInfo(model);

		// System.out.println("paymentId::" + paymentId);
		// System.out.println("payerId::" + payerId);

		// String paymentId = request.getParameter("paymentId");
		// String payerId = request.getParameter("PayerID");

		try {
			UserPaymentServices userPaymentServices = new UserPaymentServices();
			Payment payment = userPaymentServices.getPaymentDetails(paymentId);

			PayerInfo payerInfo = payment.getPayer().getPayerInfo();
			Transaction transaction = payment.getTransactions().get(0);
			ShippingAddress shippingAddress = transaction.getItemList().getShippingAddress();

			model.addAttribute("payer", payerInfo);
			model.addAttribute("transaction", transaction);
			model.addAttribute("shippingAddress", shippingAddress);

			// request.setAttribute("payer", payerInfo);
			// request.setAttribute("transaction", transaction);
			// request.setAttribute("shippingAddress", shippingAddress);
			// String url = "userPaymentCheckout?paymentId=" + paymentId + "&PayerID=" +
			// payerId;
			return "userPaymentCheckout";
			// return "redirect:"+url;
			// request.getRequestDispatcher(url).forward(request, response);

		} catch (PayPalRESTException ex) {
			// request.setAttribute("errorMessage", ex.getMessage());
			model.addAttribute("userPaymentErrorMessage", ex.getMessage());
			ex.printStackTrace();
			return "userPaymentError";
		}
	}

	/* --- Execute User Payment --- */
	@RequestMapping("/executeUserPayment")
	public String executeUserPayment(@RequestParam(name = "paymentId") String paymentId,
			@RequestParam(name = "PayerID") String payerId, Locale locale, Model model,
			final RedirectAttributes redirectAttributes, HttpSession session, HttpServletRequest request,
			HttpServletResponse response) {

		getCategoryInfo(model);
		removeCouponInfoFromSession(session);

		// System.out.println("paymentId::" + paymentId);
		// System.out.println("payerId::" + payerId);

		// String paymentId = request.getParameter("paymentId");
		// String payerId = request.getParameter("PayerID");
		try {
			UserPaymentServices userPaymentServices = new UserPaymentServices();
			Payment coursePayment = userPaymentServices.getPaymentDetails(paymentId);
			boolean userPaymentStatus = userService.userCoursePaymentProcess(paymentId, payerId, coursePayment, session,
					model);
			if (userPaymentStatus) {

				Payment payment = userPaymentServices.executePayment(paymentId, payerId);

				PayerInfo payerInfo = payment.getPayer().getPayerInfo();
				Transaction transaction = payment.getTransactions().get(0);

				model.addAttribute("payer", payerInfo);
				model.addAttribute("transaction", transaction);
				// request.setAttribute("payer", payerInfo);
				// request.setAttribute("transaction", transaction);

				// request.getRequestDispatcher("receipt.jsp").forward(request, response);
				return "userPaymentReceipt";
			} else {
				return "userShopingCartList";
			}

		} catch (PayPalRESTException ex) {
			// request.setAttribute("errorMessage", ex.getMessage());
			model.addAttribute("userPaymentErrorMessage", ex.getMessage());
			ex.printStackTrace();
			return "userPaymentError";
		}

	}

	/* --- Cancel User Payment --- */
	@RequestMapping("/userPaymentCancel")
	public String userPaymentCancel(Locale locale, Model model, final RedirectAttributes redirectAttributes,
			HttpSession session, HttpServletRequest request, HttpServletResponse response) {

		getCategoryInfo(model);
		removeCouponInfoFromSession(session);
		return "userPaymentCancel";
	}

	/*--- Coupon Code Apply---*/
	@RequestMapping(method = RequestMethod.POST, value = "/applyCouponCode", produces = "application/json;charset=UTF-8")
	public String applyVoucherCard(
			@ModelAttribute("couponCodePaymentWrapper") @Valid CouponCodePaymentWrapper couponCodePaymentWrapper,
			BindingResult result, Locale locale, Model model, final RedirectAttributes redirectAttributes,
			HttpSession session, HttpServletRequest request) {

		/*
		 * if (result.hasErrors()) { getActiveSubscriptionPlanList(locale, model);
		 * session.setAttribute("displayStatus", true); return "userQuestionnaire"; }
		 * else {
		 */
		// UserInfo userInfo =
		// adminService.getUserInfoByUserId(voucherCardPaymentWrapper.getUserId());

		boolean couponCodeStatus = userService.couponCodeCheckPayment(couponCodePaymentWrapper, redirectAttributes,
				messageSource, session, model, request, locale);
		if (couponCodeStatus) {
			return "redirect:/userShopingCartList";
		} else {
			return "redirect:/userShopingCartList";
		}
	}

	/*--- User Review for Trainer Course ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/userReviewForTrainerCourse")
	public String userReviewForTrainerCourse(
			@ModelAttribute("userCourseReviewWrapper") @Valid UserCourseReviewWrapper userCourseReviewWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {

		if (result.hasErrors()) {
			return "userCourseDetailsSession";
		} else {
			boolean userReviewForTrainerCourseStatus = userService.userReviewForTrainerCourse(userCourseReviewWrapper,
					redirectAttributes, messageSource, session);
			byte[] encodedBytes = Base64.encode(userCourseReviewWrapper.getTrainerCourseId().toString().getBytes());
			String encryptedTrainerCourseId = new String(encodedBytes);
			encodedBytes = Base64.encode(userCourseReviewWrapper.getTrainerId().toString().getBytes());
			String encryptedTrainerId = new String(encodedBytes);

			if (userReviewForTrainerCourseStatus) {
				return "redirect:/cHJvY2Vzc2luZ09uQ291cnNlRGV0YWlsc1BhZ2U?ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUlk="
						+ encryptedTrainerCourseId + "&ZW5jcnlwdGVkVHJhaW5lcklk=" + encryptedTrainerId;
			} else {
				return "redirect:/cHJvY2Vzc2luZ09uQ291cnNlRGV0YWlsc1BhZ2U?ZW5jcnlwdGVkVHJhaW5lckNvdXJzZUlk="
						+ encryptedTrainerCourseId + "&ZW5jcnlwdGVkVHJhaW5lcklk=" + encryptedTrainerId;
			}
		}
	}

	/*--- Subscribe NewsLetter ---*/
	@RequestMapping(method = RequestMethod.POST, value = "/subscribeNewsLetter")
	public String subscribeNewsLetter(@ModelAttribute("newsLetterWrapper") @Valid NewsLetterWrapper newsLetterWrapper,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes, HttpSession session) {
		if (result.hasErrors()) {
			return "userHome";
		} else {
			boolean subscribeNewsLetter = userService.subscribeNewsLetter(newsLetterWrapper, redirectAttributes,
					messageSource, model);
			if (subscribeNewsLetter) {
				return "redirect:/userHome";
			} else {
				return "redirect:/userHome";
			}
		}
	}

	/*--- Removing removeCouponInfoFromSession ---*/
	public void removeCouponInfoFromSession(HttpSession session) {
		if (session.getAttribute("myCartListTotalPriceAfterCouponCode") != null)
			session.removeAttribute("myCartListTotalPriceAfterCouponCode");

		if (session.getAttribute("couponCodeAmount") != null)
			session.removeAttribute("couponCodeAmount");

		if (session.getAttribute("couponCodeId") != null)
			session.removeAttribute("couponCodeId");

		if (session.getAttribute("userOrderDetails") != null)
			session.removeAttribute("userOrderDetails");
	}

}
