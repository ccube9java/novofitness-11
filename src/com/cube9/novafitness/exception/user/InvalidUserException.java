package com.cube9.novafitness.exception.user;

public class InvalidUserException extends RuntimeException {
	
	public InvalidUserException(String exceptionMessage) {
		super(exceptionMessage);
	}
	
}
